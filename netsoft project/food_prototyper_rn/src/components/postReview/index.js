import React, { Component } from 'react';
import { View, StyleSheet, ScrollView, TouchableOpacity, FlatList } from 'react-native';
import { Text, ListItem, Left, Body, Right, Button, Thumbnail, List, Item, Textarea, Form } from 'native-base';
import ANT from 'react-native-vector-icons/AntDesign';
import OCT from 'react-native-vector-icons/Octicons';



class PostReview extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    const data = this.props.navigation.state.params.data;
    const navigateActions = this.props.navigation.navigate;
    console.log('data----PostReview', data)
    return (
      <View style={styles.container}>
        <ScrollView>
          <List style={{ backgroundColor: '#fff', marginTop: 10, }} >
            <ListItem avatar style={{ paddingBottom: 15, marginLeft: 10 }}>
              <Left style={{ borderBottomWidth: 0 }}>
                <Thumbnail square source={{ uri: data.img }} style={{ width: 80, height: 80 }} />
              </Left>
              <Body style={{ borderBottomWidth: 0 }}>
                <Text>{data.item}</Text>
                <Text note style={{ marginTop: 5 }}>{data.name}</Text>
                {/* <View style={{ flexDirection: 'row', marginTop: 5 }}>
                  <ANT name="star" color="#FFD700" size={15} style={{ paddingRight: 5 }} />
                  <Text note style={{ color: '#000' }}>{data.star}</Text>
                </View> */}
                <View style={{ flexDirection: 'row', marginTop: 10, justifyContent: 'space-between' }}>
                  <Text style={{ fontWeight: 'bold' }}>RM {data.price}</Text>
                </View>
              </Body>
            </ListItem>
            <ListItem style={{ borderTopWidth: .5, paddingLeft: 0, marginLeft: 0, borderTopColor: '#eee' }}>
              <Body style={{ borderBottomWidth: 0 }}>
                <View style={{ flexDirection: 'row', paddingLeft: 10, justifyContent: 'space-between' }}>
                  <View style={{ flexDirection: 'row', }}>
                    <OCT name="package" size={18} style={{ color: '#f48120', }} />
                    <Text note>[{data.date}] Item {data.status}</Text>
                  </View>
                  <View style={[styles.statusColor, { backgroundColor: '#3ee063', }]} />
                </View>
              </Body>
            </ListItem>
          </List>

          <View style={{ padding: 20, marginTop: 10, alignItems: 'center' }}>
            <View style={{marginBottom: 20}}>
              <Text note>Please give us a review to improve our quality.</Text>
            </View>
            <Form style={{ width: '100%' }}>
              <Textarea rowSpan={7} bordered placeholder="Write your review..." placeholderTextColor='#ccc' />
            </Form>
            {/************************** button submit ****************************/}
            <View style={{ width: '80%', }}>
              <Button full dark style={styles.btnwarp}  onPress={() => navigateActions('review')}>
                <Text style={styles.btntext}>POST</Text>
              </Button>
            </View>
          </View>


        </ScrollView>
      </View>
    );
  }
}


const styles = StyleSheet.create({
  container: { flex: 1, backgroundColor: '#fff', },
  title: { marginTop: 15, paddingBottom: 10, paddingLeft: 10, paddingRight: 10 },
  statusColor: { borderRadius: 50, width: 12, height: 12, marginTop: 3 },
  btnwarp: { marginTop: 50, borderRadius: 5, height: 50, },

})

export default PostReview;
