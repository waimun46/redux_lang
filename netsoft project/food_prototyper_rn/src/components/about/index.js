import React, { Component } from 'react';
import { View, Text, Image, StyleSheet, ScrollView, } from 'react-native';
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import about_img from '../../assest/about.jpg';
import award1 from '../../assest/award/a1.jpg';
import award2 from '../../assest/award/a2.jpg';
import award3 from '../../assest/award/a3.jpg';

class AboutUs extends Component {
  // static navigationOptions = {
  //   title: 'About Us',
  // };
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <View style={styles.container}>
        <ScrollView>
          <View style={{ width: '100%', height: 400 }}>
            <Image source={about_img} style={{ width: '100%', height: '100%' }} />
          </View>
          <View style={{ padding: 20 }}>
            <Text style={{ marginBottom: 5, fontWeight: 'bold', fontSize: RFPercentage('4')  }}>About Us</Text>
            <View style={{height: 3, backgroundColor: '#f48120', width: '40%', marginBottom: 20}} />
            <Text style={{ marginBottom: 10 }}>
              Singapore, a cosmopolitan city that brings together the cuisines of various countries. In 1993,
              a young man, Alan Voo, with full ambition, began his first step to chase his dreams by followed
              Master Yan (Hong Kong famous roasting master) to Hong Kong to learn all the skills of Hong Kong
              cuisine. Alan Voo start up his first restaurant in Kuala Lumpur name [Hong Kong Kitchen] and get
              famous in short time.
          </Text>
            <Text style={{ marginBottom: 10, }}>[Delicious, is just nice]</Text>
            <Text style={{}}>
              [Gam Tong]--The molasses should be mellow. To control the fire and time of the fried sugar,
              stir up the golden sweet and not blackened molasses. Therefore, the golden sugar is the pursuit
              and persistence of quality, without chemical and artificial flavors. Ingredients, unique recipes,
              inherited authentic Hong Kong-style tea restaurant specialties and local cooking techniques
          </Text>
          </View>
          <View style={{ padding: 20, }}>
            <Text style={{ marginBottom: 5, fontWeight: 'bold', fontSize: RFPercentage('4') }}>Award</Text>
            <View style={{height: 3, backgroundColor: '#f48120', width: '40%', marginBottom: 20}} />
            <View >
              <View style={{ height: 500, marginBottom: 30,}}>
                <Image source={award1} style={{ width: '100%', height: '100%', resizeMode: 'contain' }} />
              </View>
              <View style={{ height:500,marginBottom: 30}}>
                <Image source={award2} style={{ width: '100%', height: '100%', resizeMode: 'contain' }} />
              </View>
              <View style={{ height: 300}}>
                <Image source={award3} style={{ width: '100%', height: '100%', resizeMode: 'contain' }} />
              </View>

            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: { flex: 1, backgroundColor: '#fff' },
})

export default AboutUs;
