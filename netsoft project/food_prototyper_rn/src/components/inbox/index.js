import React, { Component } from 'react';
import { View, FlatList, StyleSheet, TouchableOpacity } from 'react-native';
import { List, ListItem, Left, Body, Right, Thumbnail, Text } from 'native-base';
import ANT from 'react-native-vector-icons/AntDesign';



const dataInbox = [
  {
    name: 'User 1',
    description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry s standard dummy text ever since the 1500s,',
    time: '3:43 pm', img: 'https://cdn.nohat.cc/thumb/f/720/0fec843cd5c84a2d9514.jpg', star: 4.6
  },
  {
    name: 'User 2',
    description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry s standard dummy text ever since the 1500s,',
    time: '3:43 pm', img: 'https://cdn.nohat.cc/thumb/f/720/0fec843cd5c84a2d9514.jpg', star: 4.6
  },
  {
    name: 'User 3',
    description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry s standard dummy text ever since the 1500s,',
    time: '3:43 pm', img: 'https://cdn.nohat.cc/thumb/f/720/0fec843cd5c84a2d9514.jpg', star: 4.6
  },
  {
    name: 'User 4',
    description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry s standard dummy text ever since the 1500s,',
    time: '3:43 pm', img: 'https://cdn.nohat.cc/thumb/f/720/0fec843cd5c84a2d9514.jpg', star: 4.6
  },
]


class InboxPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  /**************************************** _renderInboxList ************************************/
  _renderInboxList = ({ item, index }) => {
    const navigateActions = this.props.navigation.navigate;
    return (
      <ListItem thumbnail style={{paddingLeft: 0, marginLeft: 0,}}>
        {/* <Left>
          <Thumbnail square source={{ uri: item.img }} />
        </Left> */}
        <Body style={{ height: 70 ,marginLeft: 0,  paddingLeft: 10}}>
          <Text>{item.name}</Text>
          <Text note numberOfLines={1}>{item.description}</Text>
        </Body>
        <Right style={{ height: 70 , }}>
          <Text note style={{ marginBottom: 10 }}>{item.time}</Text>
          <TouchableOpacity onPress={() => navigateActions('inboxContent', { data: item })} style={{ padding: 5, paddingTop: 5, paddingLeft: 20, paddingBottom: 0, marginTop: -5 }}>
            <ANT name="right" color={"#FF6347"} size={15} style={{ marginBottom: 10, color: '#ccc', fontWeight: '500' }} />
          </TouchableOpacity>
        </Right>
      </ListItem>
    );
  }

  render() {

    return (
      <View style={styles.container}>
        <List>
          <FlatList
            data={dataInbox}
            renderItem={this._renderInboxList}
            keyExtractor={(item, index) => index.toString()}
          />
        </List>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: { backgroundColor: '#fff', flex: 1 },
})

export default InboxPage;
