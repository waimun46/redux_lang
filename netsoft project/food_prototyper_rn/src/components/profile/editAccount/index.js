import React, { Component } from 'react';
import { View, StyleSheet, ScrollView, TouchableOpacity, TextInput} from 'react-native';
import { Form, Item, Input, Label, Text, Button, CardItem, Thumbnail, Picker } from 'native-base';
import ANT from 'react-native-vector-icons/AntDesign';
import imgEmpty from '../../../assest/empty.png';
import ImagePicker from 'react-native-image-picker';

const stateData = [
  { state: 'Kuala Lumpur', }, { state: 'Negeri Sembilan', }, { state: 'Selangor', }, { state: 'Kuantan', }
  , { state: 'Perak', }, { state: 'Johor', }
]


class EditAccount extends Component {
  constructor(props) {
    super(props);
    this.state = {
      phone: '0123456789',
      first_name: 'Wei Kong',
      last_name: 'Tan',
      email: 'wei@gmail.com',
      address_1: 'KK Times Square, Phase 2',
      address_2: 'Off Coastal Highway',
      postcode: '88100',
      state: '',
      srcImg: null,
      imgUri: '',
      fileName: '',
      imgData: null,
      selected: ""
    };
  }

  onValueChange(value: string) {
    this.setState({
      selected: value
    });
  }


  /****************************************** onChangeTextInput ********************************************/
  onChangeTextInput(text, field) {
    //console.log(text)
    if (field === 'phone') { this.setState({ phone: text }) }
    if (field === 'email') { this.setState({ email: text }) }
    if (field === 'first_name') { this.setState({ first_name: text }) }
    if (field === 'last_name') { this.setState({ last_name: text }) }
    if (field === 'address_1') { this.setState({ address_1: text }) }
    if (field === 'address_2') { this.setState({ address_2: text }) }
    if (field === 'postcode') { this.setState({ postcode: text }) }
    if (field === 'state') { this.setState({ state: text }) }
    if (field === 'taccode') { this.setState({ taccode: text }) }
    if (field === 'referral_id') { this.setState({ referral_id: text }) }
  }


  /******************************************************* choosePicture *********************************************************/
  choosePicture = () => {
    const options = {
      title: 'Select Photo',
      maxWidth: 1000,
      maxHeight: 1000,
      quality: 0.7,
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);
      if (response.didCancel) {
        //console.log('User cancelled image picker');
      }
      else if (response.error) {
        //console.log('ImagePicker Error: ', response.error);
      }
      else if (response.customButton) {
        // console.log('User tapped custom button: ', response.customButton);
      }
      else {
        //console.log(response.fileName);
        //console.log(response, '----------response');
        this.setState({
          srcImg: { uri: response.uri },
          imgUri: response.uri,
          fileName: response.fileName,
          imgData: response.data
        });
      }
    });
  };

  render() {
    const { phone, email, first_name, last_name, address_2, address_1, postcode, state, srcImg } = this.state;
    const navigateActions = this.props.navigation.navigate;
    return (
      <ScrollView>
      <View style={styles.container}>
    
          <View style={{ backgroundColor: '#fff', alignItems: 'center', }}>

            {/************************** Update Photo ****************************/}
            <CardItem style={styles.headerImg} >
              <View>
                <View style={styles.imgWarpProfile}>
                  {
                    srcImg === null ?
                      <Thumbnail source={imgEmpty} style={styles.thumbnailImg} />
                      :
                      <Thumbnail source={srcImg} style={styles.thumbnailImg} />
                  }
                </View>
                <View style={styles.update}>
                  <TouchableOpacity onPress={this.choosePicture}>
                    <Text style={styles.blueText}>Update Photo</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </CardItem>

            <View style={styles.contentWarp}>
              {/************************** Frist Name ****************************/}
              <View style={{ marginBottom: 15 }}>
                <Text note style={styles.labelsty}>First Name</Text>
                <Item >
                  <TextInput
                    value={first_name}
                    placeholder=""
                    placeholderTextColor="#ccc"
                    style={styles.inputsty}
                    onChangeText={(text) => this.onChangeTextInput(text, 'first_name')}
                    autoCorrect={false}
                    autoCapitalize="none"
                  // keyboardType={'numeric'}
                  />
                </Item>
              </View>

              {/************************** Last Name ****************************/}
              <View style={{ marginBottom: 15 }}>
                <Text note style={styles.labelsty}>Last Name</Text>
                <Item >
                  <TextInput
                    value={last_name}
                    placeholder=""
                    placeholderTextColor="#ccc"
                    style={styles.inputsty}
                    onChangeText={(text) => this.onChangeTextInput(text, 'last_name')}
                    autoCorrect={false}
                    autoCapitalize="none"
                  // keyboardType={'numeric'}
                  />
                </Item>
              </View>


              {/************************** Email ****************************/}
              <View style={{ marginBottom: 15 }}>
                <Text note style={styles.labelsty}>Email</Text>
                <Item>
                  <TextInput
                    value={email}
                    placeholder="EXP: abc@mail.com"
                    placeholderTextColor="#ccc"
                    style={styles.inputsty}
                    onChangeText={(text) => this.onChangeTextInput(text, 'email')}
                    autoCorrect={false}
                    autoCapitalize="none"
                  // keyboardType={'numeric'}
                  />
                </Item>
              </View>

              {/************************** Address ****************************/}
              <View style={{ marginBottom: 15 }}>
                <Text note style={styles.labelsty}>Address</Text>
                <Item>
                  <TextInput
                    value={address_1}
                    placeholder="Address line 1"
                    placeholderTextColor="#ccc"
                    style={styles.inputsty}
                    onChangeText={(text) => this.onChangeTextInput(text, 'address_1')}
                    autoCorrect={false}
                    autoCapitalize="none"
                  // keyboardType={'numeric'}
                  />
                </Item>
                <Item style={{ marginTop: 10 }}>
                  <TextInput
                    value={address_2}
                    placeholder="Address line 2"
                    placeholderTextColor="#ccc"
                    style={styles.inputsty}
                    onChangeText={(text) => this.onChangeTextInput(text, 'address_2')}
                    autoCorrect={false}
                    autoCapitalize="none"
                  // keyboardType={'numeric'}
                  />
                </Item>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 15 }}>
                  <View style={{ width: '50%', paddingRight: 15 }}>
                    <View>
                      <Text note style={styles.labelsty}>State (Sabah)</Text>
                      <Item >
                        <Picker
                          mode="dropdown"
                          iosHeader="Select State"
                          headerTitleStyle={{ color: '#fff' }}
                          headerStyle={{ backgroundColor: "#f48120" }}
                          headerBackButtonTextStyle={{ color: "#fff" }}
                          headerBackButtonText={<ANT name="left" size={22} />}
                          iosIcon={<ANT name="down" size={10} style={styles.iconstyPicker} />}
                          style={{ width: undefined, height: 45, }}
                          selectedValue={this.state.selected}
                          onValueChange={this.onValueChange.bind(this)}
                          textStyle={{ fontSize: 15, paddingLeft: 0 }}
                          placeholder='Select State'
                          placeholderStyle={{ color: '#ccc' }}
                        >
                          {
                            stateData.map((item, index) => {
                              return (
                                <Picker.Item label={item.state} value={item.state} key={index} />
                              )
                            })
                          }
                        </Picker>
                      </Item>
                    </View>
                  </View>
                  <View style={{ width: '50%', paddingLeft: 15 }}>
                    <View>
                      <Text note style={styles.labelsty}>Postcode</Text>
                      <Item >
                        <TextInput
                          value={postcode}
                          placeholder="EXP: 12345"
                          placeholderTextColor="#ccc"
                          onChangeText={(text) => this.onChangeTextInput(text, 'postcode')}
                          autoCorrect={false}
                          autoCapitalize="none"
                          //secureTextEntry={true}
                          keyboardType={'numeric'}
                          style={styles.inputsty}
                        />
                      </Item>
                    </View>
                  </View>
                </View>
              </View>


              {/************************** Phone Number ****************************/}
              <View style={{ marginBottom: 5 }}>
                <Text note style={styles.labelsty}>Phone Number</Text>
                <Item>
                  <TextInput
                    value={phone}
                    placeholder="EXP: 0120000000"
                    placeholderTextColor="#ccc"
                    style={styles.inputsty}
                    onChangeText={(text) => this.onChangeTextInput(text, 'phone')}
                    autoCorrect={false}
                    autoCapitalize="none"
                    keyboardType={'numeric'}
                    editable={false}
                  />
                </Item>
              </View>


              <View style={{ marginBottom: 15 }}>
                <Item inlineLabel last style={{ marginLeft: 0, paddingLeft: 0 }}>
                  <TouchableOpacity onPress={() => navigateActions('reset_pass')} style={{ padding: 15, width: '100%', paddingLeft: 0, }}>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center',paddingBottom: 10  }}>
                      <Text style={{ marginTop: 5, color: '#575757' }}>Reset Password</Text>
                      <ANT name="right" size={20} style={{ color: '#ccc' }} />
                    </View>
                  </TouchableOpacity>
                </Item>
              </View>
            </View>

            <View style={{ width: '100%', alignItems: 'center', marginTop: 30, marginBottom: 50}}>
              <View style={{ width: '80%', }}>
                <Button full dark >
                  <Text >SUBMIT</Text>
                </Button>
              </View>

            </View>

          </View>

      </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: { backgroundColor: '#fff', flex: 1, },
  iconsty: { color: '#fff' },
  headerImg: { justifyContent: 'center', backgroundColor: 'transparent', marginTop: 10 },
  imgWarpProfile: {
    width: '100%', alignItems: 'center', shadowColor: "#000", shadowOffset: { width: 0, height: 5, }, shadowOpacity: 0.34,
    shadowRadius: 6.27, elevation: 1,
  },
  thumbnailImg: { width: 120, height: 120, borderRadius: 100 },
  update: { alignItems: 'center', marginTop: 30, marginBottom: 15 },
  blueText: { color: '#2979FF' },
  inputsty: { marginBottom: 5, paddingLeft: 0, fontSize: 15, height: 40 },
  labelsty: { color: '#000' },
  contentWarp: { marginTop: 20, width: '85%', },
  iconstyPicker: { color: '#ccc', fontSize: 16, },

})

export default EditAccount;
