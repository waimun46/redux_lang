import React, { Component } from 'react';
import { View, ScrollView , StyleSheet, TextInput} from 'react-native';
import { Text, Button } from 'native-base';
import FA from 'react-native-vector-icons/FontAwesome';
import { showMessage, hideMessage } from "react-native-flash-message";


class SmsLogin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      phone: '',
      isSubmit: false
    };
  }


  /****************************************** onChangeTextInput ********************************************/
  onChangeTextInput(text, field) {
    //console.log(text)
    if (field === 'phone') { this.setState({ phone: text }) }
  }



  render() {
    const { phone, isSubmit } = this.state;
    const navigateActions = this.props.navigation.navigate;

    return (
      <ScrollView>

      <View style={styles.container}>
        <View style={styles.iconContainer}>
          <View style={styles.iconWarp}>
            <FA name="envelope" size={75} color='#fff' />
          </View>
        </View>

        {/************************** Phone input ****************************/}
        <View style={styles.contentWarp}>
          <TextInput
            value={phone}
            placeholder="Mobile Number (EXP: 0120000000)"
            placeholderTextColor="#8c8c8c"
            style={styles.inputsty}
            onChangeText={(text) => this.onChangeTextInput(text, 'phone')}
            autoCorrect={false}
            autoCapitalize="none"
            keyboardType={'numeric'}
          />

          <Text note>
            Please enter your phone number in field above, once completed, you will receive a phone SMS containing a confirmation code shortly.
          </Text>

          {/************************** button submit ****************************/}
          <Button full dark style={styles.btnwarp} onPress={() => navigateActions('sms_verifly')}>
            <Text style={styles.btntext}>GENERATE OTP</Text>
          </Button>

        </View>

      </View>

    </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: { flex: 1, alignItems: 'center', },
  inputsty: { height: 50, borderColor: '#ccc', borderWidth: .5, padding: 10, borderRadius: 5, color: '#000', marginBottom: 20 },
  btnwarp: { marginTop: 30, borderRadius: 5, height: 50, },
  btntext: { color: '#fff',  },
  iconWarp: { width: 130, height: 130, borderRadius: 100, backgroundColor: '#f4812026', alignItems: 'center', justifyContent: 'center' },
  iconContainer: { alignItems: 'center', marginTop: 50, width: '80%', },
  contentWarp: { padding: 20, marginTop: 20 },
  modalBackground: { flex: 1, alignItems: 'center', flexDirection: 'column', justifyContent: 'space-around', backgroundColor: '#00000040' },
  activityIndicatorWrapper: {
    backgroundColor: '#FFFFFF', height: 100, width: 100, borderRadius: 10, display: 'flex',
    alignItems: 'center', justifyContent: 'space-around'
  },


});


export default SmsLogin;
