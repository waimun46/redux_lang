import React, { Component } from 'react';
import { View, Text, StyleSheet, ScrollView, } from 'react-native';
import Timeline from 'react-native-timeline-flatlist';

const timeline = [
  { time: '09:00', title: 'Confirm Order', description: 'Event 1 Description' },
  { time: '10:45', title: 'Processing', description: 'Event 2 Description' },
  { time: '12:00', title: 'Item Pick Up', description: 'Event 3 Description' },
  { time: '14:00', title: 'Sending', description: 'Event 4 Description' },
  { time: '16:30', title: 'Success Deliver', description: 'Event 5 Description' }
]


class DeliveryStatus extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <View style={styles.container}>
        <ScrollView>
          <Timeline
            data={timeline}
            circleSize={20}
            circleColor='#28a745'
            lineColor='#28a745'
            timeContainerStyle={{ minWidth: 60, width: 60 }}
            timeStyle={styles.timeStyle}
            descriptionStyle={{ color: 'gray' }}
            options={{
              style: styles.styleOption
            }}
            detailContainerStyle={styles.detailContainerStyle}
            titleStyle={styles.titleStyle}
            // renderFullLine={true}
            innerCircle={'dot'}

          />
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {backgroundColor: '#fff', flex: 1},
  containerLoading: { flex: 1, justifyContent: 'center' },
  timeStyle: { textAlign: 'center', color: '#000', fontWeight: 'bold', padding: 5, fontSize: 12 },
  styleOption: { paddingTop: 20, paddingLeft: 5, paddingRight: 5, },
  detailContainerStyle: { borderBottomColor: '#ccc', borderBottomWidth: .5, paddingTop: 10, paddingBottom: 10, },
  // titleStyle: {fontWeight: 'bold', color: 'gray', fontSize: 14 },
  MainContainer: { justifyContent: 'center', flex: 1, alignItems: 'center', },

})


export default DeliveryStatus;
