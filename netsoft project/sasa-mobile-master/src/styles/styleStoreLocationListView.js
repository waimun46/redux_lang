import {StyleSheet, Dimensions} from 'react-native';
import { futuraPtBook } from './styleText'

const dimWidth= Dimensions.get('window').width
const dimHeight= Dimensions.get('window').height
const buttonColor='#ff4da6'
const textColor='#fff'
const backgroundColor='#fff'

const styles=StyleSheet.create({
    container:{
        height:dimHeight
    },
  containerBackground:{
         backgroundColor:'#EEEEEE',
        // marginRight:10,
        // marginLeft:10
    },
    viewText:{
        marginTop: 5,
        marginBottom:10


    },
    textHeading:{
        color:'#000',
        fontSize:16,
        justifyContent:"center",
        alignItems:'center'
    },
    textNormal:{
        color:'#C0C0C0',
        fontSize:14,
        marginTop: 2,
        marginBottom: 2,
        justifyContent:"center",
        alignItems:'center'
    },
     styleViewOne:{
       flex: 1 , flexDirection:'row',
       marginLeft:20,
       marginRight:20,
       marginTop:10
   },
   styleTextRight:{
       textAlign: 'right', color: '#ff4da6',marginRight:4,
       fontFamily:futuraPtBook,fontWeight:'300'
   },
   bottomLine:{
       height:0.50,
       backgroundColor:'#EEEEEE'
   }


});
export default styles;
