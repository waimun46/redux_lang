import React, { Component } from 'react'
import { View, Text, TouchableOpacity, FlatList } from 'react-native'
import StarRating from 'react-native-star-rating';
import Moment from 'moment';

import Service from '../../../../config/Service';
import CanonicalPath from '../../../../config/CanonicalPath';
import { futuraPtBook } from '../../../../styles/styleText'
import { ScrollView } from 'react-native-gesture-handler';

export default class Reviews extends Component {

  constructor(props) {
    super(props);
    this.state = {
      item: {},
      reviews: [],
    }
  }

  componentDidMount() {
    this.reviews()
  }

  onSuccess = async (filter, json) => {
    switch (filter) {
      case "REVIEWS":
        if (json[0].status == 0) {
          return;
        }

        this.setState({
          reviews: json
        })
        break
    }
  }

  onError = (filter, e) => {
    Alert.alert(e)
  }

  async reviews() {
    var data = await {
      "product_id": this.props.product_id
    }

    var params = await {
      contentType: "multipart/form-data",
      Accept: "application/json",
      keepAlive: true,
      method: 'GET',
      useToken: true,
      data: data,
      canonicalPath: CanonicalPath.REVIEWS
    };

    await Service.request(this.onSuccess, this.onError, "REVIEWS", params);
  }

  renderReviews = (item) => {
    var dateString = item.item.date;
    var momentObj = Moment(dateString, 'yyyy-MM-DD HH:mm:ss');
    var date = momentObj.format('DD MMMM YYYY');
    return (
      <View>
        <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginLeft: 10, marginRight: 10, marginTop: 20 }}>
          <Text style={{ fontSize: 18, fontWeight: '300', color: '#000', fontFamily: 'futura-pt-book' }}>{item.item.name} </Text>
          <Text style={{ fontSize: 16, marginLeft: 80, fontFamily: 'futura-pt-book' }}>{date} </Text>
        </View>
        <View style={{ marginLeft: 10, marginRight: 10, marginTop: 10 }}>
          <Text style={{ fontFamily: 'futura-pt-book', fontSize: 16 }} >{item.item.comment} </Text>
        </View>
        <View style={{ width: 60, marginLeft: 10, marginTop: 10 }}>
          <StarRating
            disabled={false}
            maxStars={5}
            rating={item.item.rating}
            starSize={20}
            fullStarColor={'#DAA520'}
          />
        </View>
        <View style={{ height: 2, marginHorizontal: 10, backgroundColor: '#ddd', marginTop: 10 }} />
      </View>
    )
  }

  render() {
    return (
      <View style={styles.container} >
        {
          (this.state.reviews.length > 0) ?

            <FlatList
              data={this.state.reviews}
              renderItem={item => { this.renderReviews(item) }}>
            </FlatList>
            //       // <List dataArray={this.state.reviews}
            //       //   renderRow={item => this.renderReviews(item)}>
            //       // </List>
            :
            <View style={styles.contentEmpty}>
              <Text>No Record</Text>
            </View>
        }
      </View>
    )
  }
}

const styles = {
  container: {
    backgroundColor: '#fff'
  },
  content: {

  },
  contentEmpty: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
    marginTop: 30
  },
  contentTitle: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginLeft: 10,
    marginRight: 10,
    marginTop: 20
  },
  contentComment: {
    marginLeft: 10,
    marginRight: 10,
    marginTop: 10
  },
  contentRating: {
    width: 60,
    marginLeft: 10,
    marginTop: 10
  },
  textName: {
    fontSize: 18,
    fontWeight: '300',
    color: '#000',
    fontFamily: futuraPtBook
  },
  textDate: {
    fontSize: 16,
    marginLeft: 80,
    fontFamily: futuraPtBook
  },
  textComment: {
    fontFamily: futuraPtBook,
    fontSize: 16
  }
}
