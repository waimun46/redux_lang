import React, { Component } from 'react';
import { FlatList, StyleSheet, Text, View, Image, ScrollView, TextInput, TouchableOpacity, ImageBackground, Dimensions, Alert, ActivityIndicator, AsyncStorage } from 'react-native';
import Dialog, { DialogContent, DialogButton, SlideAnimation } from 'react-native-popup-dialog';
import Icon1 from 'react-native-vector-icons/Foundation';
import { Container, Header, Content, Button, ListItem, List, Left, Right } from 'native-base';
import GridView from 'react-native-super-grid';
import StarRating from 'react-native-star-rating';
import Moment from 'moment';

import Progressbar from 'react-native-loading-spinner-overlay';
import Service from '../../config/Service';
import CanonicalPath from '../../config/CanonicalPath';
import { futuraPtMedium, futuraPtBook } from '../../styles/styleText'

export default class DetailPromotion extends Component {

    static navigationOptions = ({ navigation }) => ({
        title: `${navigation.state.params.data.title}`,
        headerStyle: {
            backgroundColor: 'black',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontSize: 22,
            fontFamily: futuraPtMedium
        },
    })

    constructor(props) {
        super(props);
        this.state = {
            isVisible: false,
            isVisible1: false,
            countList: '',
            picture: '',
            code: '',
            productList: '',
            isError: false,
            item: {},
            productList: {},
            dataIds: {},
            product_id: [],
            cartList: [],
            cart: {},
            subTotal: 0,
            modalCart: false,
        }
    }

    async componentDidMount() {
        var item = this.props.navigation.getParam('data')
        await this.setState({
            item: item,
            product_id: item.product_id,
        })

        this.product()
    }

    setStateAsync(state) {
        return new Promise((resolve) => {
            this.setState(state, resolve)
        });
    }

    onSuccess = async (filter, json) => {
        this.setStateAsync({ isLoading: false });

        switch (filter) {
            case "IDS":
                this.setState({
                    productList: json
                })


                break
        }

    }

    onError = (filter, e) => {
        this.setStateAsync({ isLoading: false });

        alert(e)
    }

    async product() {

        var productid = this.state.product_id;

        var data = {}

        for (var i = 0; i < productid.length; i++) {
            data['product_id[' + i + ']'] = productid[i].productid
        }

        var params = await {
            contentType: "multipart/form-data",
            Accept: "application/json",
            keepAlive: true,
            method: 'GET',
            useToken: true,
            data: data,
            canonicalPath: CanonicalPath.IDS
        };

        await Service.request(this.onSuccess, this.onError, "IDS", params);
    }

    async addCart(item) {

        if (item.quantity == "0") {
            Alert.alert("Out of Stock")
            return;
        }
        this.setState({
            cart: {
                id: item.id,
                product_name: item.product_name,
                sku: item.sku,
                brand: item.brand,
                price: item.price,
                quantity: item.quantity,
                offer_price: item.offer_price,
                rating: item.rating,
                rating_count: item.rating_count,
                image_url: item.image_url[0],
                detail: item.detail,
                how_to_use: item.how_to_use,
                like: 0,
                amount: 1,
                subTotal: ((parseFloat(item.offer_price)) == 0 ? item.price : item.offer_price)
            }
        })

        var cartStorage = await AsyncStorage.getItem('cart')

        if (cartStorage != null) {
            var cart = JSON.parse(cartStorage)
            for (var i = 0; i < cart.length; i++) {
                if (cart[i].id == this.state.cart.id) {
                    this.setState({
                        cartList: cart,
                        modalCart: true,
                    });
                    this.subTotal()
                    return;
                }
            }
        } else {
            var cart = [];
        }

        cart.push(this.state.cart)
        await AsyncStorage.setItem('cart', JSON.stringify(cart))

        this.showDialog(cart)
        this.subTotal()
    }

    showDialog(cart) {
        this.setState({
            cartList: cart,
            modalCart: true,
            listImage: []
        });

    }

    async deleteItem(index) {
        var array = [...this.state.cartList]; // make a separate copy of the array
        if (index !== -1) {
            array.splice(index, 1);
            this.setState({ cartList: array });
        }

        await AsyncStorage.setItem('cart', JSON.stringify(array))

        this.subTotal()
    }

    changeAmount = async (value, index) => {
        const newArray = [...this.state.cartList];
        newArray[index].amount = value;
        var subTotalProduct = value * (parseFloat(newArray[index].offer_price) == 0 ? newArray[index].price : newArray[index].offer_price);
        newArray[index].subTotal = parseFloat(subTotalProduct).toFixed(2);
        this.setState({ cartList: newArray });

        await AsyncStorage.setItem('cart', JSON.stringify(newArray))

        this.subTotal()
    }

    subTotal() {
        var subTotal = 0
        for (var i = 0; i < this.state.cartList.length; i++) {
            if (this.state.cartList[i].offer_price != 0) {
                var total = subTotal + parseFloat(this.state.cartList[i].offer_price) * (this.state.cartList[i].amount ? this.state.cartList[i].amount : 1);
            } else {
                var total = subTotal + parseFloat(this.state.cartList[i].price) * (this.state.cartList[i].amount ? this.state.cartList[i].amount : 1);
            }
            subTotal = (subTotal + total);
        }
        this.setState({
            subTotal: parseFloat(subTotal).toFixed(2),
        })
    }

    onCheckout() {

        if (this.state.cartList.length < 1) {
            return;
        }

        if (this.state.itemTotal < 25) {
            Alert.alert("Minimum Purchase of RM25 is required to checkout.")
            return;
        }

        for (var i = 0; i < this.state.cartList.length; i++) {
            if (this.state.cartList[i].amount === undefined || this.state.cartList[i].amount < 1) {
                Alert.alert("Please fill amount")
                return;
            } else if (this.state.cartList[i].amount > this.state.cartList[i].quantity) {
                Alert.alert(this.state.cartList[i].product_name + " Insufficient Stock")
                return;
            }
        }

        this.props.navigation.navigate('CheckoutScreen')
        this.setState({ modalCart: false })
    }

    renderCart = ({ item, index }) => {

        return (
            <View style={{ flex: 1, flexDirection: 'row', }} >

                <Image style={{ width: 70, height: 80, marginTop: 7, borderWidth: 1, borderColor: 'grey' }} source={{ uri: item.image_url }}></Image>
                <View style={{ flex: 1, flexDirection: 'column', width: 200 }}>

                    <Text numberOfLines={2} style={{ marginLeft: 4, marginTop: 4, fontSize: 16, fontFamily: futuraPtMedium, fontWeight: '500' }}>{item.product_name}</Text>
                    <Text style={{ marginLeft: 4, marginTop: 4, fontSize: 16, textDecorationLine: 'line-through', fontFamily: futuraPtBook, fontWeight: '300' }}>{(parseFloat(item.offer_price)) == 0 ? "" : "RM" + item.price} </Text>
                    <Text style={{ marginLeft: 4, marginTop: 4, fontSize: 16, color: '#ff4da6', fontFamily: futuraPtBook, fontWeight: '300' }}>{(parseFloat(item.offer_price)) == 0 ? "RM" + item.price : "RM" + item.offer_price} </Text>
                </View>

                <View style={{ flex: 1, flexDirection: 'column', alignItems: 'flex-end', }} >
                    <TouchableOpacity style={{ marginRight: 10, marginTop: 10 }} onPress={() => this.deleteItem(index)}>
                        <Icon1 name="trash" size={25} color="lightgrey" style={{}} />

                    </TouchableOpacity>

                    <View style={{ marginTop: 20, height: 40, width: 60, alignItems: 'flex-end', justifyContent: 'center' }} >
                        <View style={{ flexDirection: 'row', }}>
                            <TextInput
                                style={{ height: 40, width: 40, borderColor: 'black', borderWidth: 0.5, padding: 10, }}
                                keyboardType='numeric'
                                defaultValue="1"
                                onChangeText={(text) => this.changeAmount(text, index)}
                                value={this.state.cartList[index].amount} />
                        </View>
                    </View>

                    <Text style={{ fontSize: 14, color: 'black', marginTop: 5, fontFamily: futuraPtMedium, fontWeight: '300' }}>
                        RM {this.state.cartList[index].subTotal}
                    </Text>
                </View>

            </View>)
    }

    promotion(item) {

        if (item.extra_point && item.extra_point != "NONE") {
            return (
                <View style={{ justifyContent: "center", alignItems: 'center', resizeMode: 'cover', backgroundColor: '#F48FB1', marginLeft: 10 }}>
                    <Text style={{ color: '#FFFFFF', alignItems: 'center', fontFamily: futuraPtBook }}>
                        {item.extra_point} X POINTS
              </Text>
                </View>
            )
        } else if (item.buy_free && item.buy_free != "NONE") {
            return (
                <View style={{ justifyContent: "center", alignItems: 'center', resizeMode: 'cover', backgroundColor: '#F48FB1', marginLeft: 10 }}>
                    <Text style={{ color: '#FFFFFF', alignItems: 'center', fontFamily: futuraPtBook }}>
                        {item.buy_free}
                    </Text>
                </View>
            )
        } else if (item.promotion_type && item.promotion_type != "NONE") {
            return (
                <View style={{ justifyContent: "center", alignItems: 'center', resizeMode: 'cover', backgroundColor: '#F48FB1', marginLeft: 10 }}>
                    <Text style={{ color: '#FFFFFF', alignItems: 'center', fontFamily: futuraPtBook }}>
                        {item.promotion_type}
                    </Text>
                </View>
            )
        } else {
            return null;
        }
    }


    render() {

        var dateString = this.state.item.expiry_date;
        var momentObj = Moment(dateString, 'yyyy-MM-DD HH:mm:ss');
        var momentString = momentObj.format('DD MMMM YYYY');

        return (
            <View style={{ backgroundColor: '#fff', flex: 1 }}>
                <ScrollView>

                    <Progressbar cancelable={true} visible={this.state.isLoading} overlayColor="rgba(0, 0, 0, 0.8)" animation="fade" textContent="" style={{ fontSize: 16, textAlign: 'center', justifyContent: 'center', height: 30, alignItems: 'center' }} />

                    <View style={styles.styleCard}>
                        {
                            (this.state.item.image_url === "") ?
                                <Image style={{ width: '100%', height: 155, resizeMode: 'contain' }} source={require('../../assets/image_not_found.png')} />
                                :
                                <Image style={{ width: '100%', height: 155, resizeMode: 'contain' }} source={{ uri: this.state.item.image_url }} />
                        }

                        <View style={{ flex: 1, flexDirection: 'row' }}>

                            <View style={{ flex: 1, flexDirection: 'column' }}>

                                <Text style={styles.textHeading} >{this.state.item.title}</Text>
                                <Text style={styles.textNormal} >Valid until {momentString}</Text>
                            </View>

                            <View>

                            </View>
                        </View>
                    </View>


                    {
                        (this.state.productList.length > 0 && this.state.product_id.length) ?
                            <GridView
                                itemDimension={130}
                                items={this.state.productList}
                                style={styles.gridView}
                                renderItem={item => (

                                    <View style={{ flexDirection: 'row', backgroundColor: '#fff', marginTop: 20, }}>
                                        <View style={{ flexDirection: 'column', flex: 1, }}>
                                            <TouchableOpacity onPress={() => this.props.navigation.navigate('ProductDetailsScreen', {
                                                item: item,
                                            })}>
                                                <Image
                                                    style={{
                                                        marginLeft: 10,

                                                        resizeMode: 'cover', height: 200, borderWidth: 1, borderColor: 'grey'
                                                    }}
                                                    source={{ uri: item.image_url[0] }}
                                                />
                                            </TouchableOpacity>
                                            {this.promotion(item)}

                                            <View style={{ width: 100, height: 20, flex: 1, flexDirection: 'row', marginBottom: 4, marginTop: 8, marginLeft: 10 }}>
                                                <StarRating
                                                    disabled={false}
                                                    maxStars={5}
                                                    rating={item.rating}
                                                    starSize={18}
                                                    fullStarColor={'#DAA520'} />
                                                <Text style={{ marginLeft: 10, fontSize: 14 }}>({item.rating_count})</Text>
                                            </View>
                                            <Text numberOfLines={3} style={{ marginLeft: 10, fontWeight: '300', fontFamily: futuraPtMedium, fontSize: 18, color: '#000000', marginTop: 5 }}>{item.product_name}</Text>
                                            {/* <Text style={{marginLeft:10,fontWeight: '300',fontFamily:futuraPtMedium,fontSize:18,color:'#000000',marginTop:5}}>Brightening Cream</Text> */}
                                            <Text style={{ marginLeft: 10, fontFamily: futuraPtBook, fontSize: 16, fontWeight: '300', marginTop: 5, color: '#111111' }}>by {item.brand}</Text>
                                            <Text style={{ textDecorationLine: 'line-through', fontFamily: futuraPtBook, marginLeft: 10, marginTop: 5 }}>{(parseFloat(item.offer_price)) == 0 ? "" : "RM" + item.price}</Text>

                                            <List >
                                                <ListItem>
                                                    <Left>
                                                        <Text style={{ color: '#ff4da6', fontFamily: futuraPtBook, fontWeight: '300', fontSize: 20 }}>{(parseFloat(item.offer_price)) == 0 ? "RM" + item.price : "RM" + item.offer_price}</Text>
                                                    </Left>
                                                    <TouchableOpacity onPress={() => this.addCart(item)}>
                                                        <Right>
                                                            <View style={{ backgroundColor: '#FFEBEE', height: 40, width: 40, justifyContent: 'center', alignItems: 'center', marginRight: -10, marginLeft: 10 }}>
                                                                <Image source={require('../../assets/cart.png')} />
                                                            </View>
                                                        </Right>
                                                    </TouchableOpacity>
                                                </ListItem>
                                            </List>
                                        </View>
                                    </View>
                                )}
                            />

                            :

                            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 20 }}>
                                <Text>No Record</Text>
                            </View>
                    }

                    <Dialog
                        visible={this.state.modalCart}
                        dialogAnimation={new SlideAnimation({
                            slideFrom: 'bottom',
                        })}
                        rounded={false}
                        width={300}
                        height={400}
                        onTouchOutside={() => {
                            this.setState({ modalCart: false });
                        }}
                    >
                        <DialogContent>
                            <ScrollView style={{ backgroundColor: '#FFF' }}>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 10 }}>

                                    <Text style={{ fontSize: 18, color: 'black', fontFamily: futuraPtMedium }}>ADD TO CART </Text>

                                    <Image style={{ marginTop: 10 }} ource={require('../../assets/cross.png')} />
                                </View>
                                <View style={{ marginTop: 5, backgroundColor: 'grey', height: 0.5, width: '100%' }}></View>

                                {
                                    (this.state.cartList.length > 0) ?
                                        <FlatList
                                            vertical
                                            data={this.state.cartList}
                                            renderItem={item => this.renderCart(item)}>
                                        </FlatList>

                                        :
                                        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 20 }}>
                                            <Text>No Record</Text>
                                        </View>

                                }
                                <Text style={{ fontSize: 14, fontFamily: futuraPtBook }}> </Text>


                                <Text style={{ fontSize: 14, fontFamily: futuraPtBook }}>TOTAL</Text>
                                <Text style={{ fontSize: 16, color: 'black', marginTop: 5, fontFamily: futuraPtMedium, fontWeight: '300' }}>RM {this.state.subTotal}</Text>

                                <View style={{ marginTop: 10, marginLeft: 10, marginRight: 10, height: 50 }}>
                                    <TouchableOpacity style={{ backgroundColor: '#E4007C', justifyContent: "center", alignItems: 'center', height: 50 }}
                                        onPress={() => this.onCheckout()}>
                                        <Text style={{ color: 'white', fontSize: 18, fontFamily: futuraPtMedium }}>Checkout</Text>
                                    </TouchableOpacity>
                                </View>

                                <TouchableOpacity style={{ justifyContent: 'center', alignItems: 'center', marginTop: 10 }} onPress={() => {
                                    this.setState({ modalCart: false });
                                }}>
                                    <Text style={{ fontSize: 18, color: '#E4007C', fontFamily: futuraPtBook }}> Continue Shopping </Text>
                                </TouchableOpacity>

                            </ScrollView>
                        </DialogContent>
                    </Dialog>


                </ScrollView>
            </View>




        );
    }

}
const styles = StyleSheet.create({
    menuItems: {
        flexDirection: 'row',
        justifyContent: 'center',
        height: 50,


    },
    image1: {
        height: 20,
        width: 25,

        marginTop: 15
    },
    text1: {
        fontSize: 20,
        marginLeft: 10,
        marginTop: 15,

        fontWeight: '400',
        fontFamily: futuraPtBook,
        color: '#111111'
    },
    image2: {
        height: 30,
        width: 30,
        marginLeft: 30,
        marginTop: 10

    },
    text2: {
        fontSize: 15,
        marginTop: 15
    },
    cardContainer: {
        flexDirection: 'row',

        marginTop: 10,
        padding: 10
    },
    textSort: {
        fontSize: 16, marginTop: 20, fontWeight: '300', fontFamily: futuraPtBook
    },
    textSortSelected: {
        fontSize: 16, marginTop: 20, fontFamily: futuraPtMedium, color: '#E4007C',
    },
    btnSearch: {
        marginVertical: 10,
        backgroundColor: '#E4007C',
        justifyContent: "center",
        alignItems: 'center',
        height: 45
    },
    textSearch: {
        color: 'white',
        fontSize: 16,
        fontFamily: futuraPtMedium,
        fontWeight: '500'
    },
    styleCard: {
        marginTop: 14,
        marginLeft: 14,
        marginRight: 14,
        marginBottom: 10,
        borderWidth: 0.40,
        borderRadius: 4,
        borderColor: 'grey'
    },
    textHeading: {
        color: '#000',
        fontSize: 20,
        marginTop: 6,
        marginBottom: 2,
        marginLeft: 10,

        fontFamily: futuraPtMedium
    },
    textNormal: {
        color: '#C0C0C0',
        fontSize: 15,
        marginTop: 2,

        marginBottom: 12,
        marginLeft: 10,
        fontFamily: futuraPtBook
    },

})
