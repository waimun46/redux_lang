import React, {Component} from 'react';
import {Text, } from 'react-native';
import BackArrow from './components/BackArrow';
import { createStackNavigator, } from 'react-navigation';
import LoginScreen from './screens/LoginScreen';
import ForgotPassword from './screens/ForgotPassword';
import PhoneVerification1 from './screens/PhoneVerification1';
import PhoneVerification from './screens/PhoneVerification';
import SignUp from './screens/SignUp';
import Activate from './screens/Activate';
import Activate1 from './screens/Activate1';
import NewPassword from './screens/NewPassword';
import { futuraPtMedium } from './styles/styleText'


export default AuthStack = createStackNavigator({
  Login: {
    screen: LoginScreen,
        navigationOptions: ({navigation}) => ({
    }),
  },
  Activate: {
    screen: Activate,
    navigationOptions: ({ navigation }) => ({
      headerTitle: <Text style={{
        flex: 1, color: '#fff', fontSize: 22, fontFamily:
          futuraPtMedium
      }}>Activate</Text>,
      headerLeft:
        <BackArrow onPress={() => navigation.goBack(null)} />
    }),
  },
  Activate1: {
    screen: Activate1,
    navigationOptions: ({ navigation }) => ({
      headerTitle: <Text style={{
        flex: 1, color: '#fff', fontSize: 22, fontFamily:
          futuraPtMedium
      }}>Activate</Text>,
      headerLeft:
        <BackArrow onPress={() => navigation.goBack(null)} />

    }),
  },
  ForgotPassword: {
    screen: ForgotPassword,
    navigationOptions: ({navigation}) => ({
      headerTitle: <Text style={{  flex: 1,color:'#fff',fontSize:22, fontFamily:
          futuraPtMedium}}>Forgot Password</Text>,
      headerLeft:
      <BackArrow onPress={()=>navigation.goBack(null)} />

    }),
  },
  NewPassword: {
    screen: NewPassword,
    navigationOptions: ({ navigation }) => ({
      headerTitle: <Text style={{
        flex: 1, color: '#fff', fontSize: 22, fontFamily:
          futuraPtMedium
      }}>New Password</Text>,
      headerLeft:
        <BackArrow onPress={() => navigation.goBack(null)} />
    }),
  },
  PhoneVerification1: {
    screen: PhoneVerification1,
    navigationOptions: ({navigation}) => ({
      headerTitle: <Text style={{  flex: 1,color:'#fff',fontSize:22, fontFamily:
          futuraPtMedium}}>Phone Verification</Text>,
      headerLeft:
          <BackArrow onPress={()=>navigation.goBack(null)}/>
    }),
  },
  PhoneVerification: {
    screen: PhoneVerification,
    navigationOptions: ({navigation}) => ({
      headerTitle: <Text style={{  flex: 1,color:'#fff',fontSize:22, fontFamily:
          futuraPtMedium}}>Phone Verification</Text>,
      headerLeft:
          <BackArrow onPress={()=>navigation.goBack(null)}/>
    }),
  },
  SignUp: {
    screen: SignUp,
    navigationOptions: ({navigation}) => ({
      headerTitle: <Text style={{  flex: 1,color:'#fff',fontSize:22, fontFamily:
          futuraPtMedium}}>Sign Up</Text>,
      headerLeft:
         <BackArrow onPress={()=>navigation.goBack(null)} />

    }),
  },
}, {
    initialRouteName: 'Login',
    // defaultNavigationOptions: {
    //   headerStyle: {
    //     backgroundColor: color.ColorPrimary,
    //   },
    //   headerTintColor: 'white',
    // },
  });
