import React from 'react'
import {View, Text, TextInput, Platform } from 'react-native'
import moment from 'moment'

import { futuraPtLight, futuraPtBook } from '../styles/styleText'

const OS = Platform.OS;

const styles = {
    container:{
        backgroundColor:'#fff',
        flex:1
    },
    nameText:{
        color:'#000000',
        marginTop: 16,
        marginLeft: 10,
        fontSize: 16,
        fontFamily: futuraPtLight,
        fontWeight:'300'
    },
    nameInputText:{
        marginLeft: 13,
        fontSize: 16,
        fontFamily: futuraPtBook,
        fontWeight: '300',
        color: '#000'
    },
    nameBottomLine:{
        backgroundColor: '#D3D3D3',
        height: 0.50,
        marginRight: 20,
        marginLeft: 10
   },
   nameError:{
       color: 'red',
       fontSize: 16,
       marginLeft: 13,
       fontFamily: futuraPtBook,
       fontWeight: '300'
   },
}

const Input = ({
    label,
    title,
    name,
    value,
    error,
    setFieldValue,
    setFieldTouched,
    secureTextEntry,
    keyboardType,
    autoCapitalize,
    textColor,
    labelColor,
    styleInput
}) => {
    const showError = error && error.length > 0 ? true : false;
    // console.log('Component.Input ', title, secureTextEntry, keyboardType, autoCapitalize, error, "-0", showError)
    return(
        <View>
            <View>
                <Text style={ styles.nameText } >{ title }</Text>
                <TextInput
                    style={ styles.nameInputText }
                    placeholder= ''
                    autoCapitalize= 'none'
                    keyboardType={ keyboardType }
                    secureTextEntry={ secureTextEntry }
                    onChangeText={ ( text ) => {
                        if(text != null){
                            setFieldValue(name, text)
                        }
                        setFieldTouched(name, true)
                    }}
                />
            </View>
            <View style={ styles.nameBottomLine }></View>
            {
                showError
                ?   <View>
                        <Text style={ styles.nameError }>{ error }</Text>
                    </View>
                : null
            }
        </View>
    )
}

Input.defaultProps = {
    label: false,
    title: "",
    name: "",
    value: "",
    error: "",
    secureTextEntry: false,
    setFieldValue: ()=>{},
    setFieldTouched: () => {},
    keyboardType: 'default',
    autoCapitalize: 'sentences',
    labelColor: '#FFF5',
    textColor: '#000',
    styleInput: styles.input
}

export default Input;
