


import React,{Component} from 'react';
import{
 Text,
 Image,
 View,
 TouchableOpacity,
 Alert
}
from 'react-native';
import PropTypes from 'prop-types';
import { futuraPtMedium } from '../styles/styleText'

    class MyCard extends Component {

      static propTypes = {
        title: PropTypes.string.isRequired,
        image:PropTypes.string,
        code:PropTypes.string


      }

      render = () => {
        const { title, image,code } = this.props;
        return (
            <View style={{flex:1}}>
            <Text style={{fontSize:20,marginLeft:10,marginTop:10,fontFamily:futuraPtMedium,fontWeight:'400',color:'#111111'}}> {title}
           </Text>
         <TouchableOpacity style={{justifyContent:'center',alignItems:'center',marginTop:10}}
            onPress={ ()=>this.props.click.navigation.navigate('MakeUp')}>

      <Image style={{height:200,width: 400}}source={require('../assets/2.png')}/>
      </TouchableOpacity>
       </View>
        );
      }
    }


    export default MyCard;
