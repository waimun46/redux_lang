import React, {
  Component
} from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Alert,
  TouchableOpacity
} from 'react-native';
import  FBSDK,{LoginManager} from 'react-native-fbsdk';


export default class DemoFBLogin extends Component {

  _fbAuth=()=> {
      LoginManager.logInWithReadPermissions(['public_profile']).then(
          function(result) {
              if (result.isCancelled) {
                  Alert.alert('Login cancelled');
              } else {
                  Alert.alert('Login success with permissions: ' +
                      result.grantedPermissions.toString());
                      
              }
          },
          function(error) {
              Alert.alert('Login fail with error: ' + error);
          }
      );

  }
  render() {
      return (
  <View style={{alignSelf:'center'}} >
  <TouchableOpacity onPress={this._fbAuth}>
  <Text>Login Via FaceBook</Text>
  </TouchableOpacity>
  </View> 
      );
  }
}
