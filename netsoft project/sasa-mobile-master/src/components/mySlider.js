import { StyleSheet, View, Text, Image, TouchableOpacity } from 'react-native';
import React, { Component } from 'react';
import { PagerTabIndicator, IndicatorViewPager, PagerTitleIndicator, PagerDotIndicator } from 'rn-viewpager';

export default class MySlider extends Component {

    render() {
        return (
            <View style={{ flex: 1, }}>
                <IndicatorViewPager
                    style={{ height: 150,width:330,marginLeft:10,marginRight:10 }}
                    indicator={this._renderDotIndicator()}
                >
                    <View style={{ backgroundColor: 'rgb(223, 225, 223)'}}>
                        <TouchableOpacity onPress={() => this.props.click.navigation.navigate('TrickNdTreat')}>
                            <Image style={{ height: 150, width: 330 }} source={require('../../src/assets/promotion.png')}></Image>
                        </TouchableOpacity>
                    </View>
                    <View style={{ backgroundColor: 'rgb(223, 225, 223)', justifyContent: 'center', alignItems: 'center' }}>
                        <TouchableOpacity onPress={() => this.props.click.navigation.navigate('TrickNdTreat')}>
                            <Image style={{ height: 150, width: 330 }} source={require('../../src/assets/2.png')}></Image>
                        </TouchableOpacity>
                    </View>
                    <View style={{ backgroundColor: 'rgb(223, 225, 223)', justifyContent: 'center', alignItems: 'center' }}>
                        <TouchableOpacity onPress={() => this.props.click.navigation.navigate('TrickNdTreat')}>
                            <Image style={{ height: 150, width: 330 }} source={require('../../src/assets/03.png')}></Image>
                        </TouchableOpacity>
                    </View>
                    
                </IndicatorViewPager>

            </View>
        );
    }

    _renderTitleIndicator() {
        return <PagerTitleIndicator titles={['one', 'two', 'three']} />;
    }

    _renderDotIndicator() {
        return <PagerDotIndicator pageCount={3} color='red' />;
    }



}