import React,{Component} from "react";
import { Header } from "react-navigation";
import { View, Platform ,Image,TouchableOpacity} from "react-native";
import PropTypes from 'prop-types';

 class ErrorMessage extends Component {
   
    static propTypes = {
        
        height : PropTypes.number.isRequired,
        width : PropTypes.number.isRequired,
        title : PropTypes.string.isRequired,
        marginLeft:PropTypes.number.isRequired,
        marginRight:PropTypes.number.isRequired,
        marginTop:PropTypes.number.isRequired
        
      }
      render(){
        const { height,width,title,marginLeft,marginRight,marginTop } = this.props;
    return (
        <View style={{marginLeft:20,marginRight:20,height:80,backgroundColor:'#fff'}}>
        <Text>{title}</Text>
                    
                </View>   
    );
  }
}
  
  export default ErrorMessage;