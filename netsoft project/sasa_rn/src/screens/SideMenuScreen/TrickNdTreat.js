import React, { Component } from 'react';
import { Platform, BackArrow, Text, View, Image, ScrollView, TextInput, TouchableOpacity, ImageBackground, Dimensions } from 'react-native';
import { Container, Header, Content, Button, ListItem, List } from 'native-base';
import moment from 'moment';
import HTML from 'react-native-render-html';

import Util from '../../config/Util'

import styles from '../../styles/trickandReward';
import { futuraPtMedium } from '../../styles/styleText'

export default class TrickNdTreat extends Component {

  static navigationOptions = ({ navigation }) => ({
    title: `${navigation.state.params.data.title}`,
    headerRight: (
      <TouchableOpacity  >
        <Image style={{ height: 20, width: 20, marginRight: 10 }} source={require('../../assets/share-26.png')} />
      </TouchableOpacity>
    ),
    headerStyle: {
      backgroundColor: 'black',
    },
    headerTintColor: '#fff',
    headerTitleStyle: {
      fontSize: 22,
      fontFamily: futuraPtMedium
    },
  })

  constructor(props) {
    super(props);
    this.state = {
      isVisible: false,
      timer: 0,
      data: {},
    }

  }

  clockCall = null

  startTimer = () => {
    this.clockCall = setInterval(() => {
      this.decrementClock()
    }, 1000)
  }

  decrementClock = () => {
    if (this.state.timer === 0) {
      clearInterval(this.clockCall)
      this.setState({
        isTimesUp: true
      })

    }
    if (this.state.timer !== 0) {
      this.setState((prevState) => ({
        timer: prevState.timer - 1
      }))
    }
  }

  componentDidMount = () => {

    let data = this.props.navigation.getParam('data')

    let timer = 0
    if (data.expiry_date) {
      var start = moment(data.expiry_date, "yyyy-MM-DD HH:mm:ss");
      var end = moment(new Date());

      //Difference in number of days
      timer = moment.duration(start.diff(end)).asDays();
    }

    this.setState({
      timer: timer * 24 * 60 * 60,
      data: data,
    })
    this.startTimer()
  }

  componentWillUnmount = () => {
    clearInterval(this.clockCall)
  }

  render() {

    var dateString = this.state.data.expiry_date;
    var momentObj = moment(dateString, 'yyyy-MM-DD HH:mm:ss');
    var momentString = momentObj.format('DD MMMM YYYY');
    return (
      <ScrollView style={styles.container} >
        {/* <View style={styles.view}>
          <Text style={styles.text}> Hurry! only 6 left offer</Text>
        </View> */}

        {
          (this.state.timer != 0) ?
            <View>
              <View style={styles.view}>
                <Text style={styles.text1}> {Util.formatTimeCountDown(this.state.timer)} </Text>
              </View>
              <View style={styles.view1}>
                <Text style={styles.textTimer}>days     </Text>
                <Text style={styles.textTimer}>hours  </Text>
                <Text style={styles.textTimer}>minutes</Text>
                <Text style={styles.textTimer}>seconds </Text>
              </View>
            </View>
            :
            null
        }



        {/* <View style={styles.trContainer}>
          <TouchableOpacity style={styles.takereward}  >
            <Text style={styles.trText}>TAKE REWARD</Text>
          </TouchableOpacity>
        </View> */}
        <Text style={styles.discText}>{this.state.data.title}</Text>
        <Text style={styles.dateText}> Until {momentString} </Text>
        <View style={styles.dateText1}>
          <HTML html={this.state.data.decription} />
        </View>
        {/* <Text style={styles.dateText1}>{this.state.data.decription}</Text> */}

        <View style={styles.imgPromotion}>
          {
            (this.state.data.image_url === "") ?
              <Image style={{ width: '90%', height: 200, resizeMode: 'contain' }} source={require('../../assets/image_not_found.png')} />
              :
              <Image style={{ width: '90%', height: 200 }} source={{ uri: this.state.data.image_url }} />
          }
        </View>


      </ScrollView>
    );
  }
}
