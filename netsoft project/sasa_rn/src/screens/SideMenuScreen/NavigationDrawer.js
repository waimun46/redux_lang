import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Drawer from 'react-native-drawer';
import HomeMenu from '../SideMenuScreen/HomeMenu';
import {Platform,View} from 'react-native';

class NavigationDrawer extends Component {
 render() {
   const { navigationState, onNavigate } = this.props;
  
       return (
     <View style={{flex:1}}>
         <Drawer
           ref={(drawer) => { this.drawer = drawer; }}
           content={<HomeMenu  />}
          
         >
          
         </Drawer>
         </View>
       )
     }
   
 
}

NavigationDrawer.propTypes = {
 navigationState: PropTypes.shape({
   children: PropTypes.array,
   key: PropTypes.string,
   open: PropTypes.bool,
 }),
 onNavigate: PropTypes.func,
};

export default NavigationDrawer;