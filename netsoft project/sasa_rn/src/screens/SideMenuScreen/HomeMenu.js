

import { createStackNavigator, StackActions, NavigationActions } from 'react-navigation'; // Version can be specified in package.json
import { Col, Row, Grid } from "react-native-easy-grid";
import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Image, ScrollView, TextInput, TouchableOpacity, ImageBackground, Dimensions,StatusBar } from 'react-native';
import { Container, Header, Content, Button, ListItem, List } from 'native-base';
import { Drawer } from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome5';
import Icon1 from 'react-native-vector-icons/MaterialCommunityIcons';
import Icon2 from 'react-native-vector-icons/SimpleLineIcons';
import Icon3 from 'react-native-vector-icons/AntDesign';

import {Badge} from 'react-native-elements'
import { futuraPtMedium, futuraPtBook } from '../../styles/styleText'

const width = Dimensions.get('window').width
const height = Dimensions.get('window').height
const aboutsasaArray = ['EXCHANGE & RETURN POLICY', 'TERMS & CONDITIONS', 'PRIVACY POLICY', 'MEMBERSHIP BENEFITS', 'COMPANY PROFILE']
const tellus = ['CONTACT US', 'RATE THE APP',]
export default class HomeMenu extends Component {
  // static navigationOptions = {
  //   title: '',
  //   headerLeft: null,
  //   headerStyle: {
  //     backgroundColor: 'black',
  //     height: 0
  //   },
  //   headerTintColor: '#fff',
  //   headerTitleStyle: {
  //     fontSize: 22,
  //     fontFamily:futuraPtMedium,
  //     fontWeight:'500'
  //   },

  // }
  static navigationOptions=({
    header:null,

})
  constructor(props) {
    super(props);
    this.state = {
      isVisible: false
    }

  }



  render() {
    return (
      <ScrollView showsVerticalScrollIndicator={false} style={styles.container}>
                 <StatusBar
     backgroundColor="black"
     barStyle="light-content"
   />
        <View style={{ backgroundColor: '#fff' ,marginBottom:10}}>
        <View style={{justifyContent:'center',alignItems:'center'}}>
          <ImageBackground style={styles.backgroundImage} source={require('../../assets/base1.png')}>

            {/* <TouchableOpacity onPress={() => this.props.navigation.navigate('HomeScreen')}>
            <Icon name="arrow-left" color='black' size={30}
                    />
            </TouchableOpacity> */}



            <Image style={styles.sasalogo} source={require('../../assets/base2.png')} />
          </ImageBackground>
          </View>
          <TouchableOpacity style={styles.myaccContainer} onPress={() => this.props.click.navigation.navigate('MyAccountScreen')} >
          <Icon  name="user-circle" color='black' size={25}
                    />

            {/* <Image style={{ height: 25, width: 25 }} source={require('../../assets/my_account.png')} /> */}
            <Text style={styles.myaccText}>MY ACCOUNT </Text>

          </TouchableOpacity>
          <TouchableOpacity style={styles.myaccContainer} onPress={() => this.props.click.navigation.navigate('InboxScreen')} >

            <Icon1  name="bell-outline" color='black' size={30}/>
                     {/* <Image style={{ height: 30, width: 25 }} source={require('../../assets/notifications.png')} /> */}
            <Text style={styles.myaccText}>INBOX </Text>
            <Badge style={{marginLeft:30}}
            containerStyle={{ backgroundColor: '#F48FB1',marginLeft:80,height:35,width:35}}
  value={5}
  textStyle={{ color: '#fff' }}
/>
          </TouchableOpacity>
          <TouchableOpacity style={styles.myaccContainer} onPress={() => this.props.click.navigation.navigate('PromotionScreen')} >
          <Icon1  name="map-marker-outline" color='black' size={30}
                    />
            {/* <Image style={{ height: 25, width: 25 }} source={require('../../assets/outline-local.png')} /> */}
            <Text style={styles.myaccText}>LOCATION 2</Text>

          </TouchableOpacity>

          <TouchableOpacity style={styles.myaccContainer} onPress={() => this.props.click.navigation.navigate('RewardScreen')} >
          <Icon1  name="wallet-giftcard" color='black' size={30}
          />

           {/* <Image style={{ height: 25, width: 25 }} source={require('../../assets/outline-redeem.png')} /> */}
            <Text style={styles.myaccText}>REWARDS </Text>

          </TouchableOpacity>
          <TouchableOpacity style={styles.myaccContainer}>
          <Icon1  name="information-outline" color='black' size={30}
                    />
            {/* <Image style={{ height: 25, width: 25 }} source={require('../../assets/outline-info.png')} /> */}
            <Text style={styles.myaccText}>HELP & FAQS </Text>

          </TouchableOpacity>
        </View>
        <View style={{ backgroundColor: '#fff' }}>
          <View style={styles.aboutSasa}>
            <Text style={{ color: 'grey',fontFamily:futuraPtBook,fontSize:16,fontWeight:'300',marginTop:10 }}> ABOUT SASA </Text>
            {aboutsasaArray.map((item, key) => (
              <Text key={key} style={styles.TextStyle} > {item} </Text>)
            )}
          </View>
        </View>
        <View style={{ backgroundColor: '#fff' ,marginTop:10,marginBottom:10,}}>
          <Text style={{ color: 'grey',fontFamily:futuraPtBook,fontSize:16,fontWeight:'300',marginTop:15,marginLeft:15}}> FOLLOW US </Text>
          <View style={styles.myaccContainer}>
          <Icon  name="facebook" color='black' size={25}
                    />
            {/* <Image source={require('../../assets/Facebook-595.png')} /> */}
            <Text style={styles.myaccText}>FACEBOOK </Text>
          </View>
          <View style={styles.myaccContainer}>
            {/* <Image source={require('../../assets/Instagram-59.png')} /> */}
            <Icon1  name="instagram" color='black' size={25}
                    />
            <Text style={styles.myaccText}>INSTAGRAM </Text>
          </View>
        </View>
        <View style={{ backgroundColor: '#fff' }}>
          <View style={styles.aboutSasa}>
            <Text style={{ color: 'grey',fontFamily:futuraPtBook,fontSize:16,fontWeight:'300' }}> TELL US WHAT YOU THINK </Text>
            {tellus.map((item, key) => (
              <Text key={key} style={styles.TextStyle} > {item} </Text>)
            )}
          </View>
        </View>
        <View style={{ backgroundColor: '#fff',marginTop:10,height:50,justifyContent:'center' }}>
          <Text style={{color: 'grey',fontFamily:futuraPtBook,fontSize:16,fontWeight:'300',marginLeft:15}}> APP VERSION 2.1.0 </Text>
        </View>
      </ScrollView>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    height: height,
    backgroundColor:'#F5F5F5'





  },
  backgroundImage: {
    height: 140,
  },
  sasalogo: {
    marginLeft: 80,
    marginRight: 80,
    marginTop:30



  },
  myaccContainer: {
    flexDirection: 'row',
    padding: 15,
    marginTop: 5


  },
  myaccText: {
    fontSize: 15,
    marginLeft: 20,
    marginTop: 5,
  fontWeight:'300',
    fontFamily:futuraPtMedium,
    color:'#000000'
  },
  aboutSasa: {

    padding: 15,
    marginTop: 10


  },
  TextStyle: {
    marginTop: 30,
    fontSize: 16,
    color: '#000',
    fontWeight: '300',
    fontFamily:futuraPtBook
  },
  followus: {
    flexDirection: 'row',
    padding: 15,
    marginTop: 10


  },
  fbText: {

    marginTop: 30,
    fontSize: 15,


    fontWeight: '200'
  },
  backarrowImage: {
    height: 35,
    width: 30
  }



})
