import React, { Component } from 'react';
import styles from '../../styles/styleInbox'
import { Platform, FlatList, Text, View, Image, ScrollView, Dimensions } from 'react-native';
import { Container, Header, Content, Button, ListItem, List } from 'native-base';

import HTML from 'react-native-render-html';
import Progressbar from 'react-native-loading-spinner-overlay';
import Service from '../../config/Service';
import CanonicalPath from '../../config/CanonicalPath';

import Icon3 from 'react-native-vector-icons/MaterialCommunityIcons';
import { Badge } from 'react-native-elements'
import { futuraPtMedium } from '../../styles/styleText'

const width = Dimensions.get('window').width

export default class InboxScreen extends Component {

    static navigationOptions = ({
        headerStyle: {
            backgroundColor: 'black'
        },
        headerTintColor: '#fff',
        drawerLabel:
            <View style={{ flexDirection: 'row', backgroundColor: '#fff', width: width / 1, paddingHorizontal: 10, paddingTop: 25, alignItems: 'center' }}>
                <View style={{ alignItems:'center', width: 40}}>
                    <Icon3 name="bell-outline" color='black' size={30} />
                </View>
                <Text style={{ fontSize: 16, fontFamily: futuraPtMedium, fontWeight: '300', color: '#000000' - 87, marginLeft: 10 }}>INBOX</Text>
                {/* {
                    (this.state.unread != 0) ?
                        <Badge
                            containerStyle={{ backgroundColor: '#F48FB1', marginLeft: 100, height: 32, width: 32, marginTop: 20 }}
                            value={this.state.unread}
                            textStyle={{ color: '#fff' }}
                        />
                        :
                        <View />
                } */}


            </View>

    })

    constructor(props) {
        super(props);
        this.state = {
            check: true,
            selectedIndex: 0,
            isHaveRecord: false,
            notifications: [],
            unread: 0,
        }

    }

    componentDidMount(){
        this.notifications()
    }

    setStateAsync(state) {
        return new Promise((resolve) => {
            this.setState(state, resolve)
        });
    }

    onSuccess = async (filter, json) => {
        this.setStateAsync({ isLoading: false });

        switch (filter) {
            case "NOTIFICATIONS":

                this.setState({
                    notifications: json,
                })

                break;
            case "UNREAD_COUNT":

                var status = json[0].status;
                if (status == 1) {
                    this.setState({
                        unread_count: json[0].unread_count,
                    })
                }

                break
        }

    }

    onError = (filter, e) => {
        this.setStateAsync({ isLoading: false });

        alert(e)
    }

    async notifications() {
        await this.setStateAsync({ isLoading: true });

        var params = await {
            contentType: "multipart/form-data",
            Accept: "application/json",
            keepAlive: true,
            method: 'GET',
            useToken: true,
            data: {},
            canonicalPath: CanonicalPath.NOTIFICATIONS
        };

        await Service.request(this.onSuccess, this.onError, "NOTIFICATIONS", params);
    }

    async unread_count() {
        await this.setStateAsync({ isLoading: true });

        var params = await {
            contentType: "multipart/form-data",
            Accept: "application/json",
            keepAlive: true,
            method: 'GET',
            useToken: true,
            data: {},
            canonicalPath: CanonicalPath.UNREAD_COUNT
        };

        await Service.request(this.onSuccess, this.onError, "UNREAD_COUNT", params);
    }

    renderRow = (item) => {
        return (
            <ListItem>
                <View style={styles.viewText}>
                    <Text style={styles.textHeading} >{item.item.title}</Text>
                    <HTML html={item.item.description} />
                    {/* <Text style={styles.textNormal} >{item.item.description }</Text> */}
                    <Text style={styles.textNormal1} >{item.item.date}</Text>
                </View>
            </ListItem>

        )
    }

    render() {
        return (
            <ScrollView style={styles.containerBackground}>

                <Progressbar cancelable={true} visible={this.state.isLoading} overlayColor="rgba(0, 0, 0, 0.8)" animation="fade" textContent="" style={{ fontSize: 16, textAlign: 'center', justifyContent: 'center', height: 30, alignItems: 'center' }} />


                {
                    (this.state.notifications.length > 0) ?
                        <FlatList
                            vertical
                            data={this.state.notifications}
                            renderItem={item => this.renderRow(item)}>
                        </FlatList>
                        :
                        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 20 }}>
                            <Text>No Record</Text>
                        </View>

                }


            </ScrollView>
        );
    }
}
