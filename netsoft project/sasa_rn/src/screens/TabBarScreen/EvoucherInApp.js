import React, { Component } from 'react';
import { Platform, Alert, Text, View, Image, ScrollView, TextInput, TouchableOpacity, FlatList, Dimensions, StatusBar } from 'react-native';
import styles from '../../styles/styleEvoucher'
import Moment from 'moment';
import Progressbar from 'react-native-loading-spinner-overlay';
import Service from '../../config/Service';
import CanonicalPath from '../../config/CanonicalPath';
import { futuraPtMedium } from '../../styles/styleText'

export default class EvoucherInApp extends Component {

  static navigationOptions = ({
    title: 'EVoucher',
    headerLeft: null,
    headerStyle: {
      backgroundColor: 'black',

    },
    headerTintColor: '#fff',
    headerTitleStyle: {
      fontSize: 22,
      fontFamily: futuraPtMedium,
      fontWeight: '500'
    },
  })

  isPayment = false;

  constructor(props) {
    super(props);
    this.isPayment = this.props.navigation.getParam('isPayment');
    this.state = {
      isVisible: false,
      evouchers: [],
    }

  }

  setStateAsync(state) {
    return new Promise((resolve) => {
      this.setState(state, resolve)
    });
  }

  onSuccess = async (filter, json) => {
    this.setStateAsync({ isLoading: false });

    switch (filter) {
      case "EVOUCHER":
        this.setState({
          evouchers: json
        })

        break
    }

  }

  onError = (filter, e) => {
    this.setStateAsync({ isLoading: false });

    alert(e)
  }

  async evoucher_mobile() {
    await this.setStateAsync({ isLoading: true });

    var data = await {
      "type": "mobile"
    }

    var params = await {
      contentType: "multipart/form-data",
      Accept: "application/json",
      keepAlive: true,
      method: 'GET',
      useToken: true,
      data: data,
      canonicalPath: CanonicalPath.EVOUCHER
    };

    await Service.request(this.onSuccess, this.onError, "EVOUCHER", params);
  }

  componentDidMount() {
    this.evoucher_mobile()
  }

    onSelected( item ){
       let type = this.props.navigation.getParam('type')
       const onSelectVoucher = this.props.navigation.getParam('onSelectVoucher');

       if (type === "payment") {
           this.props.navigation.goBack();
           onSelectVoucher( item.item );
       }else{
           this.props.navigation.navigate('BirthdayVoucher', {
               item: item.item ,
               isPayment: this.isPayment
           })
       }
   }

  showPopup(){
    Alert.alert("You may use this voucher during checkout")
  }

  renderRow = (item) => {

    var dateString = item.item.valid_date;
    var momentObj = Moment(dateString, 'yyyy-MM-DD HH:mm:ss');
    var momentString = momentObj.format('DD MMMM YYYY');

    return (
      <View style={styles.styleCard}>
        {/* <TouchableOpacity style={{ justifyContent: "center", alignItems: 'center', }} onPress={() => this.props.navigation.navigate('WelcomeGiftVoucher', { item: item.item })}  > */}
        <TouchableOpacity style={{ justifyContent: "center", alignItems: 'center', }} onPress={() => this.onSelected( item )}  >
          <Image style={{ width: '100%', height: 145, resizeMode: 'contain' }} source={{ uri: item.item.image_url }} />

        </TouchableOpacity>

        <View style={{ flex: 1, flexDirection: 'row' }}>

          <View style={{ flex: 1, flexDirection: 'column' }}>

            <Text style={styles.textHeading} >{item.item.title}</Text>
            <Text style={styles.textNormal} >Valid until {momentString}</Text>
          </View>

          <View>

            {/* <View style={styles.viewButton}> */}
              {/* <TouchableOpacity style={styles.touch} onPress={() => this.onSelected(item)}   >
                <Text style={styles.textButton}>USE VOUCHER</Text>
              </TouchableOpacity> */}
            {/* </View> */}
          </View>
        </View>
      </View>
    )
  }

  render() {
    return (
      <ScrollView style={styles.containerBackground}>
        <StatusBar
          backgroundColor="black"
          barStyle="light-content"
        />

        <Progressbar cancelable={true} visible={this.state.isLoading} overlayColord="rgba(0, 0, 0, 0.8)" animation="fade" textContent="" style={{ fontSize: 16, textAlign: 'center', justifyContent: 'center', height: 30, alignItems: 'center' }} />

        {
          (this.state.evouchers.length > 0) ?
            <FlatList
              vertical
              data={this.state.evouchers}
              renderItem={item => this.renderRow(item)}>
            </FlatList>
            :
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 20 }}>
              <Text>No Record</Text>
            </View>

        }

      </ScrollView>
    );
  }
}
