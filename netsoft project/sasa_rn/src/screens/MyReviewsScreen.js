import React, { Component } from 'react';
import styles from '../styles/styleMyReviews'
import { Text, View, Image, ScrollView, } from 'react-native';
import { List } from 'native-base';
import StarRating from 'react-native-star-rating';
import Moment from 'moment';

import Progressbar from 'react-native-loading-spinner-overlay';
import Service from '../config/Service';
import CanonicalPath from '../config/CanonicalPath';
import { futuraPtMedium, futuraPtBook } from '../styles/styleText'

export default class MyReviewsScreen extends Component {
  static navigationOptions = ({
    title: 'My Reviews',
    headerLeft: null,
    headerStyle: {
      backgroundColor: 'black'
    },
    headerTintColor: '#fff',
    headerTitleStyle: {
      fontSize: 22,
      fontFamily: futuraPtMedium,
      fontWeight: '500'
    },
  })

  constructor(props) {
    super(props);
    this.state = {
      check: true,
      selectedIndex: 0,
      reviews: [],

    }
  }

  componentDidMount() {
    this.my_reviews()

  }

  setStateAsync(state) {
    return new Promise((resolve) => {
      this.setState(state, resolve)
    });
  }

  onSuccess = async (filter, json) => {
    this.setStateAsync({ isLoading: false });

    switch (filter) {
      case "MY_REVIEWS":

        if (json[0].status == 0) {
          return;
        }
        this.setState({
          reviews: json
        })

        break
    }

  }

  onError = (filter, e) => {
    this.setStateAsync({ isLoading: false });

    alert(e)
  }

  async my_reviews() {
    await this.setStateAsync({ isLoading: true });

    var params = await {
      contentType: "multipart/form-data",
      Accept: "application/json",
      keepAlive: true,
      method: 'GET',
      useToken: true,
      data: {},
      canonicalPath: CanonicalPath.MY_REVIEWS
    };

    await Service.request(this.onSuccess, this.onError, "MY_REVIEWS", params);
  }

  renderRow = (item) => {
    var dateString = item.date;
    var momentObj = Moment(dateString, 'yyyy-MM-DD HH:mm:ss');
    var date = momentObj.format('DD MMMM YYYY');
    return (
      <View>
        <Text style={{ marginTop: 10, fontSize: 16, textAlign: 'right', fontFamily: futuraPtBook }}>{date} </Text>

        <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginLeft: 10, marginRight: 10 }}>
          <Text style={{ fontSize: 18, fontWeight: '300', color: '#000', fontFamily: futuraPtBook }}>{item.comment} </Text>
        </View>

        <View style={{ marginLeft: 10, marginRight: 10, marginTop: 10 }}>
          <Text style={{ fontFamily: futuraPtBook, fontSize: 16 }} >{item.product_name} </Text>
        </View>
        <View style={{ width: 60, marginLeft: 10, marginTop: 10 }}>
          <StarRating
            disabled={false}
            maxStars={5}
            rating={parseInt(item.rating)}
            starSize={20}
            fullStarColor={'#DAA520'}
          />
        </View>
        <View style={{ height: 2, marginHorizontal: 10, backgroundColor: '#ddd', marginTop: 10 }} />
      </View>
    )
  }

  render() {
    return (
      <ScrollView style={styles.containerBackground}>

        <Progressbar cancelable={true} visible={this.state.isLoading} overlayColor="rgba(0, 0, 0, 0.8)" animation="fade" textContent="" style={{ fontSize: 16, textAlign: 'center', justifyContent: 'center', height: 30, alignItems: 'center' }} />

        {
          (this.state.reviews.length > 0) ?
            <List dataArray={this.state.reviews}
              renderRow={item => this.renderRow(item)}>
            </List>
            :
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', flexDirection: 'column', marginTop: 30 }}>
              <Text>No Record</Text>
            </View>

        }

      </ScrollView>
    );
  }
}
