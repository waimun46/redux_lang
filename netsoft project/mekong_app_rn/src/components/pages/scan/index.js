import React, { Component } from 'react';
import { StyleSheet, View, ScrollView, Text, FlatList, Platform, Image, TouchableOpacity, Linking, Button, Alert } from 'react-native';
import { Actions } from 'react-native-router-flux';
import IconANT from "react-native-vector-icons/AntDesign";
import { QRscanner } from 'react-native-qr-scanner';


class ScanPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      flashMode: false,
      zoom: 0.2,

    };
  }

  /************************* Scan Success will open browser to download pdf file  *************************/
  onSuccess = (e) => {
    Linking
      .openURL(e.data)
      .catch(err => console.error('An error occured', err));
  }

  render() {
    return (
      <View style={styles.container}>
        <QRscanner
          onRead={this.onSuccess}
          renderBottomView={this.bottomView}
          flashMode={this.state.flashMode}
          zoom={this.state.zoom}
          finderY={0}
          hintText=" "
        />
      </View>
    );
  }

  /************************* Render Flashlight in bottom button *************************/
  bottomView = () => {
    return (
      <View style={styles.bottomViewWarp}>
        <TouchableOpacity
          style={styles.bottomTouchableOpacity}
          onPress={() => this.setState({ flashMode: !this.state.flashMode })}
        >
          <View style={styles.bottomTextWaro}>
            <Text style={StyleSheet.bottomText}>{global.t('Flashlight')} </Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}


export default ScanPage;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    ...Platform.select({
      ios: { backgroundColor: 'black' },
      android: { backgroundColor: 'black' }
    })
  },
  bottomViewWarp: { flex: 1, flexDirection: 'row', backgroundColor: '#0000004D' },
  bottomTouchableOpacity: { flex: 1, alignItems: 'center', justifyContent: 'center' },
  bottomTextWaro: { backgroundColor: '#ffffff52', padding: 10, borderRadius: 20, borderWidth: 0 },
  bottomText: { color: '#fff', }
})
