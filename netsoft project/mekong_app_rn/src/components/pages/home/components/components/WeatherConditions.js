export const weatherConditions = {
    Rain: {
      title: 'Raining'
    },
    Thunderstorm: {
      title: 'Thunderstorm'
    },
    Clouds: {
      title: 'Clouds'
    },
    Sunny: {
        title: 'Sunny'
      },
    Haze: {
      title: 'Haze'
    },
    Mist: {
        title: 'Mist'
    },
    Snow: {
        title: 'Snow'
    },
    Drizzle: {
        title: 'Drizzle'
    },

   
  };