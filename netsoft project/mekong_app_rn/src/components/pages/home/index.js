import React, { Component } from 'react';
import { StyleSheet, View, ScrollView, AsyncStorage } from 'react-native';
import { Container, Header, Content, Card, CardItem, Thumbnail, Text, Button, Icon, Left, Body, Right } from 'native-base';
import { Actions } from 'react-native-router-flux';

import TimeLocations from './components/TimeLocations';
import LocationsSelect from './components/LocationsSelect';
import Upcoming from './components/Upcoming'
import LoginModal from '../login';


class HomesScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            tokenId: 0,
        }
    }

    componentDidMount() {

        /************************************ Get Store Data From AsyncStorage  ***********************************/
        AsyncStorage.getItem('USER_TOKEN').then(asyncStorageRes => {
            //console.log(asyncStorageRes, 'HomesScreen----------------tokenId')
            this.setState({
                tokenId: asyncStorageRes
            })
            //console.log(this.state.tokenId, '-------------InforScreen')
        });

    }

    render() {
        //console.log(this.state.tokenId,'-------------------HomesScreen----render')
        const { tokenId } = this.state;
        return (

            <View > 

                <TimeLocations />
                <ScrollView >
                    <LocationsSelect />
                    {/* <View style={{ padding: 7, backgroundColor: '#e9ecef' }}></View> */}
                    <Upcoming />

                    {
                        tokenId !== null ? (
                            null
                        ) : (
                                <LoginModal />
                            )
                    }

                </ScrollView>

            </View>
        );
    }
}


export default HomesScreen;

const styles = StyleSheet.create({
    HotpickContainer: {
        backgroundColor: 'white',
    }
})
