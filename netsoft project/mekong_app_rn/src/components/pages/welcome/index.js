import React, { Component } from 'react';
import { StyleSheet, View, ScrollView, Text, FlatList, Platform, Image, TouchableOpacity, Linking, Button, Alert, Dimensions } from 'react-native';
import { Actions } from 'react-native-router-flux';
import IconANT from "react-native-vector-icons/AntDesign";
import Carousel from 'react-native-looped-carousel';
import { Fonts } from '../../../utils/fonts';
const { width, height } = Dimensions.get('window');



class WelcomePage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            size: { width, height },
        };
    }


    /************************* Set page Carousel event width and height *************************/
    _onLayoutDidChange = (e) => {
        const layout = e.nativeEvent.layout;
        this.setState({ size: { width: layout.width, height: layout.height } });
    }



    render() {
        return (
            <View style={{ flex: 1 }} onLayout={this._onLayoutDidChange}>

                <Carousel
                    delay={2000}
                    style={this.state.size}
                    onAnimateNextPage={(p) => console.log(p)}
                    pageInfo
                    bullets
                    autoplay
                    delay={4000}
                    bulletsContainerStyle={{ bottom: 100 }}
                >
                    <View style={[this.state.size]}>
                        <Image
                            source={require('../../../assets/images/background/thailand.jpg')}
                            style={{ width: "100%", height: '100%', resizeMode: 'cover', }}
                        />
                        <View style={{ position: 'absolute', bottom: '48%', left: 30 }}>
                            <Text style={styles.texttitle}>{global.t('Thailand')}</Text>

                        </View>
                    </View>
                    <View style={[this.state.size]}>
                        <Image
                            source={require('../../../assets/images/background/vietnam.jpg')}
                            style={{ width: "100%", height: '100%', resizeMode: 'cover', }}
                        />
                        <View style={{ position: 'absolute', bottom: '48%', left: 30 }}>
                            <Text style={[styles.texttitle, { fontFamily: Fonts.Helvetica }]}>{global.t('Vietnam')}</Text>
                        </View>
                    </View>
                    <View style={[this.state.size]}>
                        <Image
                            source={require('../../../assets/images/background/cambodia.jpg')}
                            style={{ width: "100%", height: '100%', resizeMode: 'cover', }}
                        />
                        <View style={{ position: 'absolute', bottom: '48%', left: 30 }}>
                            <Text style={[styles.texttitle, { fontFamily: Fonts.Helvetica }]}>{global.t('Cambodia')}</Text>
                        </View>
                    </View>
                   
                </Carousel>
                <View style={styles.logowarp}>
                    <Image
                        source={require('../../../assets/images/logo_r.png')}
                        style={{ width: 200, height: 100, resizeMode: 'contain', }}
                    />
                </View>
                <View style={styles.textenterwarp} >
                    <View style={styles.textenterwarpinner}>
                        <TouchableOpacity onPress={() => Actions.Homepage()} style={{}}>
                            <Text style={styles.textenter} >{global.t('Enter')}</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        );
    }

}


export default WelcomePage;

const styles = StyleSheet.create({
    logowarp: { position: 'absolute', top: 80, left: 20, width: '100%', },
    texttitle: {
        color: '#fff', fontSize: 45, fontWeight: 'bold', textShadowOffset: { width: 2, height: 2 },
        textShadowRadius: 2, textShadowColor: '#000',
    },
    textenterwarp: { position: 'absolute', bottom: 30, width: '100%', },
    textenterwarpinner: { width: '90%', marginLeft: 'auto', marginRight: 'auto', },
    textenter: { color: '#fff', fontSize: 20, textAlign: 'center', padding: 10, paddingLeft: 10 }
})
