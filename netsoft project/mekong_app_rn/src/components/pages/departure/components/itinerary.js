import React, { Component } from 'react';
import {
    StyleSheet, View, ScrollView, TouchableHighlight, FlatList, Image, Platform, Picker, Modal, SafeAreaView,
    TouchableOpacity, Dimensions, AsyncStorage, ActivityIndicator
} from 'react-native';
import { Container, Header, Content, List, ListItem, Text, Left, Right, Icon, Body } from 'native-base';
import { Actions } from 'react-native-router-flux';
import IconANT from "react-native-vector-icons/AntDesign";
import { DepartingDetailApi } from '../../../../../PostApi'
import { Select, Option } from "react-native-chooser";
import HTML from 'react-native-render-html';
const { width } = Dimensions.get('window');


const height = width * 0.6;

const images = [
    {
        source: {
            uri: 'https://cdn.pixabay.com/photo/2018/03/11/20/42/mammals-3218028__340.jpg',
        },
    },
    {
        source: {
            uri: 'https://cdn.pixabay.com/photo/2018/03/12/14/03/flower-3219718__340.jpg',
        },
    },
    {
        source: {
            uri: 'https://cdn.pixabay.com/photo/2018/03/04/22/37/orange-3199462__340.jpg',
        },
    },
    {
        source: {
            uri: 'https://cdn.pixabay.com/photo/2018/03/22/17/15/strawberries-3251153__340.jpg',
        },
    },
    {
        source: {
            uri: 'https://cdn.pixabay.com/photo/2018/03/11/20/42/mammals-3218028__340.jpg',
        },
    },
    {
        source: {
            uri: 'https://cdn.pixabay.com/photo/2018/03/12/14/03/flower-3219718__340.jpg',
        },
    },

];


class Carousel extends Component {
    render() {
        const { images } = this.props;
        if (images && images.length) {
            return (
                <View style={styles.scrollContainer}>
                    <ScrollView
                        horizontal={true}
                        pagingEnabled={true}
                        showsHorizontalScrollIndicator={true} >
                        {images.map((image, i) => (
                            <Image style={styles.image} source={image.source} key={i} />
                        ))}
                    </ScrollView>
                </View>
            );
        }
        console.log('Please provide images');
        return null;
    }
}

const SCROLLVIEW_REF = 'scrollview'

class ItineraryPage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            text: '',
            data: [],
            AsyncStorageId: 0,
            value: 'Day 1',
            selectDate: [],
            childVisible: false,
            langPrint: '',
            dataDay1: []
        };
    }

    onSelect(value) {
        this.setState({
            selectDate: this.state.data.filter((element) => {
                return element.day_num == value
            }),
            value: value,
            childVisible: true

        });
    }

    componentDidMount() {

        /************************************ Get Store Data From AsyncStorage  ***********************************/
        AsyncStorage.getItem('USER_DATA').then(asyncStorageRes => {
            //console.log(asyncStorageRes, '----------------AsyncStorage-----ItineraryPage')
            this.setState({
                AsyncStorageId: asyncStorageRes
            })
            //console.log(this.state.AsyncStorageId, '-------------fetchDataApi----ItineraryPage')

            /************************************ Fetch Api Data  ***********************************/
            DepartingDetailApi(this.state.AsyncStorageId).then((fetchData) => {
                //console.log(fetchData, '-----------------fetchData-----ItineraryPage')
                this.setState({
                    data: fetchData[0].daily_itinerary,
                    selectDate: fetchData[0].daily_itinerary,
                    dataDay1: fetchData[0].daily_itinerary[0],
                })
            })




        });

        /************************************ Get Language Store Data From AsyncStorage  ***********************************/
        AsyncStorage.getItem("language").then(langStorageRes => {
            //console.log(langStorageRes, '----------------langStorageRes-----ItineraryPage')
            this.setState({
                langPrint: langStorageRes
            })
        })

        /************************* Image ScrollView animation function *************************/
        // const numOfBackground = 6;
        // let scrollValue = 0, scrolled = 0;
        // setInterval(function () {
        //     scrolled++;
        //     if (scrolled < numOfBackground)
        //         scrollValue = scrollValue + width;
        //     else {
        //         scrollValue = 0;
        //         scrolled = 0
        //     }
        //     toscrollView.scrollTo({ x: scrollValue, animated: false })
        // }, 3000);

    }


    _renderItem = ({ item }) => {
        //console.log(item, '----------ItineraryPage------item')
        const { langPrint } = this.state;
        return (

            <View>

                <View>
                    <Image
                        source={require('../../../../assets/images/img/co01.png')}
                        style={styles.TopImage}
                    />
                    <View style={{ padding: 20 }}>
                        {
                            langPrint === 'zh' ? (
                                <Text style={{ fontSize: 25 }}>{item.title_cn}</Text>
                            ) : (
                                    <Text style={{ fontSize: 25 }}>{item.title}</Text>
                                )
                        }
                        <Text style={styles.subtext}>{global.t('Best_natural_views')}</Text>
                    </View>
                </View>

                {/************** Photo slider *************
                <View style={{ backgroundColor: '#e9ecef', }}>
                    <View>
                        <View style={{ paddingLeft: 20, paddingTop: 20 }}>
                            <Text style={{ fontSize: 20 }}>{global.t('See_Photo')}</Text>
                        </View>
                        <ScrollView
                            // horizontal={true}
                            // scrollEventThrottle={16}
                            ref={(scrollView) => { toscrollView = scrollView; }}
                            horizontal={true} pagingEnabled={true}
                        >
                            <View style={{ flex: 1, paddingTop: 10 }}>
                                <View style={{ height: 200, marginTop: 20 }}>
                                    <View style={styles.container}>
                                        <Carousel images={images} />
                                    </View>
                                </View>
                            </View>
                        </ScrollView>
                    </View>
                </View>
*/}

                {/*************** description ***************/}
                <View style={{ backgroundColor: '#e9ecef', }}>
                    <View style={{ padding: 20 }}>
                        <Text style={{ fontSize: 20, marginBottom: 20 }}>{global.t('Activity')}</Text>
                        {
                            langPrint === 'zh' ? (
                                <HTML
                                    html={item.action_cn}
                                    tagsStyles={{
                                        p: { marginBottom: 15 },
                                    }}
                                    imagesInitialDimensions={{ width: '100%', height: 480 }}
                                />
                            ) : (
                                    <HTML
                                        html={item.action}
                                        tagsStyles={{
                                            p: { marginBottom: 15 },
                                        }}
                                        imagesInitialDimensions={{ width: '100%', height: 480 }}
                                    />
                                )
                        }

                    </View>
                </View>

                {/*************** shoud visit **************
                <View style={{ backgroundColor: '#e9ecef', }}>
                    <View style={{ padding: 20 }}>
                        <Text style={{ fontSize: 20 }}>{global.t('Why_should_visit')}</Text>
                        <List style={{ color: '#777', marginTop: 5 }}>
                            <ListItem style={styles.ListItemstyle}>
                                <View style={styles.ListItemViewstyle} >
                                    <IconANT name="checksquareo" style={styles.ListItemiconstyle} />
                                </View>
                                <View>
                                    <HTML
                                        html={item.action}
                                        tagsStyles={{
                                            p: { marginBottom: 15 },
                                        }}

                                    />
                                </View>
                            </ListItem>

                            <ListItem style={styles.ListItemstyle}>
                                <View style={styles.ListItemViewstyle} >
                                    <IconANT name="checksquareo" style={styles.ListItemiconstyle} />
                                </View>
                                <View><Text style={styles.icontext}>Low cost accomodation</Text></View>
                            </ListItem>

                            <ListItem style={styles.ListItemstyle}>
                                <View style={styles.ListItemViewstyle} >
                                    <IconANT name="checksquareo" style={styles.ListItemiconstyle} />
                                </View>
                                <View><Text style={styles.icontext}>Cheap foods</Text></View>
                            </ListItem>
                        </List>
                    </View>
                </View>*/}


            </View>

        )
    }


    render() {

        const { data, selectDate, childVisible, value, dataDay1, langPrint } = this.state;
        //console.log(dataDay1, '----------------ItineraryPage--------dataDay1')

        return (
            <View >
                <Select
                    onSelect={this.onSelect.bind(this)}
                    defaultText={value}
                    onChange={(e) => this.onSelectCurrency(e.target.value)}
                    style={{ borderWidth: 0, borderBottomWidth: .5, borderBottomColor: "#ccc", width: '100%', height: 50, backgroundColor: '#fff' }}
                    textStyle={{ color: 'red', textAlign: 'center', fontSize: 17, width: '100%', }}
                    backdropStyle={{ backgroundColor: "#00000078" }}
                    optionListStyle={{ backgroundColor: "#F5FCFF", height: 200 }}
                    animationType='none'
                    transparent={true}
                    indicator='down'
                    indicatorColor='red'
                    indicatorSize={Platform.OS === 'ios' ? 15 : 12}
                    indicatorStyle={{ marginTop: Platform.OS === 'ios' ? 10 : 2, right: 30 }}

                >
                    {
                        data.map((item, i) => {
                            return (
                                <Option value={item.day_num} key={i}>{item.day_num}</Option>
                            )
                        })
                    }
                </Select>
                {
                    childVisible ? (
                        <FlatList
                            data={selectDate}
                            renderItem={this._renderItem}
                        />
                    ) : (

                            <View>

                                <View>
                                    <Image
                                        source={require('../../../../assets/images/img/co01.png')}
                                        style={styles.TopImage}
                                    />
                                    <View style={{ padding: 20 }}>
                                        {
                                            langPrint === 'zh' ? (
                                                <Text style={{ fontSize: 25 }}>{dataDay1.title_cn}</Text>
                                            ) : (
                                                    <Text style={{ fontSize: 25 }}>{dataDay1.title}</Text>
                                                )
                                        }
                                        <Text style={styles.subtext}>{global.t('Best_natural_views')}</Text>
                                    </View>
                                </View>
                                {/*************** description ***************/}
                                <View style={{ backgroundColor: '#e9ecef', }}>
                                    <View style={{ padding: 20 }}>
                                        <Text style={{ fontSize: 20, marginBottom: 20 }}>{global.t('Activity')}</Text>
                                        {
                                            langPrint === 'zh' ? (
                                                <HTML
                                                    html={dataDay1.action_cn}
                                                    tagsStyles={{
                                                        p: { marginBottom: 15 },
                                                    }}
                                                    imagesInitialDimensions={{ width: '100%', height: 480 }}
                                                />
                                            ) : (
                                                    <HTML
                                                        html={dataDay1.action}
                                                        tagsStyles={{
                                                            p: { marginBottom: 15 },
                                                        }}
                                                        imagesInitialDimensions={{ width: '100%', height: 480 }}
                                                    />
                                                )
                                        }

                                    </View>
                                </View>

                            </View>
                        )

                }




            </View>
        );
    }
}


export default ItineraryPage;

const styles = StyleSheet.create({
    TopImage: { width: "100%", resizeMode: 'cover', flex: 1, position: 'relative' },
    ListItemstyle: { borderBottomWidth: 0, marginLeft: 0 },
    ListItemViewstyle: { paddingRight: 10 },
    ListItemiconstyle: { fontSize: 15 },
    subtext: { color: '#777', marginTop: 5 },
    icontext: { color: '#777' },
    scrollContainer: {height,},
    image: {width: 150,height,marginRight: 5},
})
