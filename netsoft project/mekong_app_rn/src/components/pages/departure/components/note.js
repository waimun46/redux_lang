import React, { Component } from 'react';
import { StyleSheet, View, ScrollView, FlatList, Image, Text, Platform, AsyncStorage, TouchableOpacity } from 'react-native';
import { Actions } from 'react-native-router-flux';
import IconFAS from "react-native-vector-icons/FontAwesome";
import { DepartingDetailApi } from '../../../../../PostApi';
import HTML from 'react-native-render-html';


class Note extends Component {

    constructor(props) {
        super(props)
        this.state = {
            data: [],
            AsyncStorageId: 0,
            langPrint: ''
        }
    }

    componentDidMount() {

        /************************************ Get Store Data From AsyncStorage  ***********************************/
        AsyncStorage.getItem('USER_DATA').then(asyncStorageRes => {
            //console.log(asyncStorageRes, '----------------AsyncStorage-----Note')
            this.setState({
                AsyncStorageId: asyncStorageRes
            })
            //console.log(this.state.AsyncStorageId, '-------------fetchDataApi----Note')

            /************************************ Fetch Api Data  ***********************************/
            DepartingDetailApi(this.state.AsyncStorageId).then((fetchData) => {
                //console.log(fetchData, '-----------------fetchData-----Note')
                this.setState({
                    data: fetchData[0]
                })
            })

        });


        /************************************ Get Language Store Data From AsyncStorage  ***********************************/
        AsyncStorage.getItem("language").then(langStorageRes => {
            //console.log(langStorageRes, '----------------langStorageRes-----Note')
            this.setState({
                langPrint: langStorageRes
            })
        })


    }

    /************************************ Logout ***********************************/
    _logout() {
        AsyncStorage.removeItem('USER_DATA');
        Actions.Departurelogin();
    }


    render() {
        const { data, langPrint } = this.state;
        return (
            <View>
                <View>

                    <View >
                        <Image source={require('../../../../assets/images/img/probg.jpg')} style={styles.Imagestyletop} />
                        {/* <View style={[styles.overlay, { height: 150}]}></View> */}
                        <View style={styles.imageProfileWarp}>
                            <Image source={{ uri: data.tourmanager_photo }} style={styles.imageProfileImg} />
                        </View>
                        <TouchableOpacity style={styles.IconWarp} onPress={() => this._logout()}>
                            <View >
                                <IconFAS name="sign-out" style={styles.IconStyle} />
                            </View>
                        </TouchableOpacity>
                        <View>
                            <View>
                                <Text style={styles.Text1} >{global.t('Tour_Manager')}</Text>
                                <Text style={styles.Text2} >{data.tourmanager_name}</Text>
                            </View>
                            <View style={styles.profiledetail}>
                                <View style={{ width: '50%', borderRightWidth: .5, borderRightColor: '#ccc' }}>
                                    <Text style={styles.Text9}>{global.t('Phone_no')}</Text>
                                    <Text style={styles.Text11}>{data.tourmanager_phone_num}</Text>
                                </View>
                                <View style={{ width: '50%' }}>
                                    <Text style={styles.Text9}>{global.t('Tour_Code')}</Text>
                                    <Text style={styles.Text11}>{data.tour_code}</Text>
                                </View>
                            </View>
                        </View>

                    </View>

                    <View style={{ width: '100%', height: 'auto', padding: 20 }} >
                        <View style={styles.CenterWarp}>
                            <View style={styles.packagestyle}>
                                <Text style={styles.Text1c} numberOfLines={1}>{global.t('Package_Title')}: </Text>
                                {
                                    langPrint === 'zh' ? (
                                        <Text style={styles.Text8b}>{data.title_cn} </Text>
                                    ) : (
                                            <Text style={styles.Text8b}>{data.title_en} </Text>
                                        )
                                }

                            </View>
                            <Text style={styles.Text6}>{global.t('Meeting_Point')}: </Text>
                            {
                                langPrint === 'zh' ? (
                                    <HTML
                                        html={data.note_cn}
                                        tagsStyles={{
                                            p: { marginBottom: 10 },
                                        }}
                                    />
                                ) : (
                                        <HTML
                                            html={data.note_en}
                                            tagsStyles={{
                                                p: { marginBottom: 10 },
                                            }}
                                        />
                                    )
                            }


                        </View>
                    </View>

                </View>


            </View>
        );
    }
}


export default Note;

const styles = StyleSheet.create({
    TextContainer: { width: '70%', paddingTop: 10, paddingLeft: 25, },
    Text1c: { color: '#fff', fontSize: 18, fontWeight: 'bold', textAlign: 'center', marginTop: 5 },
    Text1: { color: '#de2d30', fontSize: 18, fontWeight: 'bold', textAlign: 'center', marginTop: 5 },
    Text1b: { color: '#de2d30', fontSize: 18, fontWeight: 'bold', marginBottom: 15 },
    Text2: { color: 'black', fontSize: 16, marginTop: 5, fontWeight: 'bold', textAlign: 'center' },
    Text3: { color: '#9e9ea6', marginTop: 5 },
    Text4: { color: '#9e9ea6', marginTop: 5 },
    Text5: { color: '#9e9ea6', marginTop: 20 },
    Text6: { fontWeight: 'bold', marginTop: 5, fontSize: 18, color: '#de2d30', marginBottom: 15, marginTop: 30 },
    Text7: { fontWeight: 'bold', marginTop: 5, marginBottom: 30 },
    Text8: { color: '#9e9ea6', marginTop: 10, },
    Text8b: { color: '#fff', marginTop: 10, marginBottom: 10, fontWeight: 'bold', textAlign: 'center', },
    Text9: { color: '#ccc', marginTop: 10, fontWeight: 'bold', textAlign: 'center', },
    Text11: { color: '#24292e', marginTop: 5, textAlign: 'center', paddingBottom: 10 },
    CenterWarp: { marginTop: 10, },
    profiledetail: { flexDirection: 'row', borderBottomWidth: .5, marginTop: 10, borderBottomColor: '#ccc' },
    packagestyle: {
        backgroundColor: '#de2d30', borderColor: '#ddd', borderBottomWidth: 0, shadowColor: '#000', shadowOffset: { width: 3, height: 1 },
        shadowOpacity: 0.3, shadowRadius: 2, elevation: 5,
    },
    Imagestyletop: { resizeMode: 'cover', width: '100%', height: 150, },
    imageProfileWarp: {
        marginTop: -90, marginLeft: 'auto', marginRight: 'auto', backgroundColor: 'white',
        padding: 3, borderRadius: 100
    },
    imageProfileImg: { resizeMode: 'cover', width: 150, height: 150, borderRadius: 150 / 2 },
    IconWarp: { position: 'absolute', right: 30, marginTop: 120, backgroundColor: '#0095ff', padding: 10, borderRadius: 100 },
    IconStyle: { fontSize: 30, color: 'white', alignItems: 'center', },
    overlay: { flex: 1, position: 'absolute', left: 0, top: 0, opacity: 0.3, backgroundColor: 'black', width: '100%' },

})
