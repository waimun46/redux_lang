import React, { Component } from 'react';
import { StyleSheet, View, ScrollView, ImageBackground, Image, Dimensions, TextInput, Platform, AsyncStorage, SafeAreaView } from 'react-native';
import { Container, Header, Content, Button, Text, Icon, Form, Item, Input, Label, Picker, Toast, } from 'native-base';
import { Actions } from 'react-native-router-flux';
import IconANT from "react-native-vector-icons/AntDesign";
import { SignupApi } from '../../../../PostApi';
import { Select, Option } from "react-native-chooser";


/************************ Validations Toast Style **************************/
const textStyle = { color: "yellow" };
const position = "top";
const buttonText = "Okay";
const duration = 3000;
const style = { top: '15%' };



class RegisterScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            username: '',
            email: '',
            code: '',
            phone: '',
            password: '',
            city: '',
            address: '',
            postcode: '',
            state: global.t('State'),
            data: [],
            Error: '',
            isEditable: false,
            showToast: true,

        }

    }


    /****************************************** Select Option State ******************************************/
    onSelect(value, label) {
        console.log(value, 'value-----------')
        this.setState({ state: value });
    }

    /****************************************** Updata Value onChange TextInput ******************************************/
    updataValue(text, field) {
        console.log(text)
        if (field == 'username') { this.setState({ username: text }) }
        if (field == 'email') { this.setState({ email: text }) }
        //if (field == 'code') { this.setState({ code: text }) }
        if (field == 'phone') { this.setState({ phone: text }) }
        if (field == 'password') { this.setState({ password: text }) }
        if (field == 'city') { this.setState({ city: text }) }
        if (field == 'address') { this.setState({ address: text }) }
        if (field == 'postcode') { this.setState({ postcode: text }) }

    }

    /****************************************** Submit Register Fetch API  ******************************************/
    onSubmit(email = this.state.email, username = this.state.username, phone = this.state.phone,
        password = this.state.password, city = this.state.city, address = this.state.address,
        postcode = this.state.postcode, state = this.state.state) {
        console.log(email, 'email---------')
        console.log(username, 'username---------')

        /****************************** Validations form Toast *****************************/
        if (this.state.username === "") {
            this.setState({
                Error: Toast.show({
                    text: global.t('Fill_Username'),
                    buttonText: buttonText, position: position,
                    duration: duration, textStyle: textStyle, style: style,
                })
            })
        }
        else if (this.state.phone === "") {
            this.setState({
                Error: Toast.show({
                    text: global.t('Fill_Phone'),
                    buttonText: buttonText, position: position,
                    duration: duration, textStyle: textStyle, style: style,
                })
            })
        }
        else if (this.state.email === "") {
            this.setState({
                Error: Toast.show({
                    text: global.t('Fill_Email'),
                    buttonText: buttonText, position: position,
                    duration: duration, textStyle: textStyle, style: style,
                })
            })
        }
        else if (this.state.password === "") {
            this.setState({
                Error: Toast.show({
                    text: global.t('Fill_Password'),
                    buttonText: buttonText, position: position,
                    duration: duration, textStyle: textStyle, style: style,
                })
            })
        }
        else if (this.state.address === "") {
            this.setState({
                Error: Toast.show({
                    text: global.t('Fill_Address'),
                    buttonText: buttonText, position: position,
                    duration: duration, textStyle: textStyle, style: style,
                })
            })
        }
        else if (this.state.city === "") {
            this.setState({
                Error: Toast.show({
                    text: global.t('Fill_City'),
                    buttonText: buttonText, position: position,
                    duration: duration, textStyle: textStyle, style: style,
                })
            })
        }
        else if (this.state.postcode === "") {
            this.setState({
                Error: Toast.show({
                    text: global.t('Fill_Postcode'),
                    buttonText: buttonText, position: position,
                    duration: duration, textStyle: textStyle, style: style,
                })
            })
        }
        else if (this.state.state === global.t('State')) {
            this.setState({
                Error: Toast.show({
                    text: global.t('Fill_State'),
                    buttonText: buttonText, position: position,
                    duration: duration, textStyle: textStyle, style: style,
                })
            })
        }
        else {
            this.setState({ Error: '' })
            /************************************ Fetch API  ***********************************/
            let url = `https://goldendestinations.com/api/new/sign_up.php?gdsecode=GDFX2019213VJVCQWDCZXZUIQW&email=${email}&username=${username}&phone=${phone}&password=${password}&city=${city}&address=${address}&postcode=${postcode}&state=${state}`;

            fetch(url).then((res) => res.json())
                .then(function (myJson) {
                    console.log(myJson, 'myJson-----------');
                    if (myJson[0].status == 1) {
                        return myJson.json(Actions.Home())
                    } else {
                        alert(myJson[0].error);
                    }
                });

        }

    }

    render() {
        //console.log(this.state.data, 'data---------------')
        console.log(this.state.Error);
        const { Error } = this.state;

        return (
            <ImageBackground
                source={require('../../../assets/images/background/rebg.jpg')}
                style={styles.Container}
            >
                <View style={{ flex: 1 }}>
                    <ScrollView style={{ width: '100%', }}>
                        <Image
                            source={require('../../../assets/images/logo_r.png')}
                            style={styles.imagecontainer}
                        />
                        <View style={{ flex: .8, }}>
                            {/* <Text style={styles.errormsg}>{Error}</Text> */}
                            <Form style={styles.formContainer}>
                                <View style={{ width: '60%', }}>
                                    {/*************************** Username  ***************************/}
                                    <Item style={{ borderColor: 'white', width: '100%', marginLeft: 0, }}>
                                        <TextInput
                                            placeholder={global.t('Username')}
                                            placeholderTextColor='white'
                                            style={styles.inputstyle4}
                                            onChangeText={(text) => this.updataValue(text, 'username')}
                                            autoCorrect={false}
                                            autoCapitalize={false}
                                        />
                                    </Item>
                                    {/*************************** Code  ***************************/}
                                    <Item style={{ borderBottomColor: 'white', width: '100%', marginLeft: 0 }}>
                                        <Item style={{ width: '30%', borderBottomWidth: 0 }}>
                                            <TextInput
                                                placeholder="+6"
                                                placeholderTextColor='white'
                                                style={styles.InputStyle}
                                                onChangeText={(text) => this.updataValue(text, 'code')}
                                                autoCorrect={false}
                                                autoCapitalize={false}
                                                editable={this.state.isEditable}
                                            />
                                        </Item>
                                        {/*************************** Phone  ***************************/}
                                        <Item style={{ width: '70%', borderBottomWidth: 0 }}>
                                            <View style={{ width: '100%', textAlign: 'center', }}>
                                                <TextInput
                                                    placeholder={global.t('Phone')}
                                                    placeholderTextColor='white'
                                                    style={styles.InputStyle2}
                                                    onChangeText={(text) => this.updataValue(text, 'phone')}
                                                    autoCorrect={false}
                                                    autoCapitalize={false}
                                                />
                                            </View>
                                        </Item>
                                    </Item>
                                    {/*************************** Email ***************************/}
                                    <Item style={{ width: '100%', borderColor: 'white', height: 50, marginLeft: 0 }}>
                                        <TextInput
                                            placeholder={global.t('Email')}
                                            placeholderTextColor='white'
                                            style={styles.inputstyle4}
                                            onChangeText={(text) => this.updataValue(text, 'email')}
                                            autoCorrect={false}
                                            autoCapitalize={false}
                                        />
                                    </Item>
                                    {/*************************** Password ***************************/}
                                    <Item style={{ width: '100%', borderColor: 'white', height: 50, marginLeft: 0 }}>
                                        <TextInput
                                            secureTextEntry={true}
                                            placeholder={global.t('Password')}
                                            placeholderTextColor='white'
                                            style={styles.inputstyle4}
                                            onChangeText={(text) => this.updataValue(text, 'password')}
                                            autoCorrect={false}
                                            autoCapitalize={false}
                                        />
                                    </Item>
                                    {/*************************** Address ***************************/}
                                    <Item style={{ borderColor: 'white', height: 50, width: '100%', marginLeft: 0 }}>
                                        <TextInput
                                            placeholder={global.t('Address')}
                                            placeholderTextColor='white'
                                            style={styles.inputstyle4}
                                            onChangeText={(text) => this.updataValue(text, 'address')}
                                            autoCorrect={false}
                                            autoCapitalize={false}
                                        />
                                    </Item>
                                    {/*************************** City ***************************/}
                                    <Item style={{ borderBottomColor: 'white', width: '100%', marginLeft: 0 }}>
                                        <Item style={{ width: '30%', borderBottomWidth: 0 }}>
                                            <TextInput
                                                placeholder={global.t('City')}
                                                placeholderTextColor='white'
                                                style={styles.InputStyle}
                                                onChangeText={(text) => this.updataValue(text, 'city')}
                                                autoCorrect={false}
                                                autoCapitalize={false}
                                            //multiline={true}
                                            />
                                        </Item>
                                        {/*************************** Username  ***************************/}
                                        <Item style={{ width: '70%', borderBottomWidth: 0 }}>
                                            <TextInput
                                                placeholder={global.t('Postcode')}
                                                placeholderTextColor='white'
                                                style={styles.InputStyle3}
                                                onChangeText={(text) => this.updataValue(text, 'postcode')}
                                                autoCorrect={false}
                                                autoCapitalize={false}
                                            />
                                        </Item>
                                    </Item>
                                    {/*************************** State ***************************/}
                                    <Item style={{ width: '100%', marginLeft: 0 }}>
                                        <View style={{ borderBottomWidth: 0.5, borderBottomColor: "#fff", width: '95%' }}>
                                            <Select
                                                onSelect={this.onSelect.bind(this)}
                                                defaultText={this.state.state}
                                                style={{ borderWidth: 0, width: '100%', height: 50 }}
                                                textStyle={{ color: '#fff', textAlign: 'center', width: '100%' }}
                                                backdropStyle={{ backgroundColor: "#00000078" }}
                                                optionListStyle={{ backgroundColor: "#F5FCFF", height: 200 }}
                                                animationType='none'
                                                transparent={true}
                                                indicator='down'
                                                indicatorColor='white'
                                                indicatorSize={Platform.OS === 'ios' ? 15 : 12}
                                                indicatorStyle={{ marginTop: Platform.OS === 'ios' ? 10 : 2, }}
                                            >
                                                <Option value={global.t('Selangor')}>{global.t('Selangor')}</Option>
                                                <Option value={global.t('KL')}>{global.t('KL')}</Option>
                                                <Option value={global.t('Pahang')}>{global.t('Pahang')}</Option>
                                                <Option value={global.t('Johor')}>{global.t('Johor')}</Option>
                                                <Option value={global.t('Kelantan')}>{global.t('Kelantan')}</Option>
                                                <Option value={global.t('Perak')}>{global.t('Perak')}</Option>
                                                <Option value={global.t('Melaka')}>{global.t('Melaka')}</Option>
                                                <Option value={global.t('NS')}>{global.t('NS')}</Option>

                                            </Select>
                                        </View>
                                    </Item>
                                </View>
                            </Form>
                        </View>
                    </ScrollView>

                    {/*************************** Submit button ***************************/}
                    <View style={styles.Buttonwarp}>
                        <Button block style={styles.bottomStyle} onPress={() => this.onSubmit()}>
                            <Text style={styles.BottomText} uppercase={false}>{global.t('Create_Account')}</Text>
                        </Button>
                    </View>
                   

                </View>
                {/* <SafeAreaView style={{backgroundColor: '#fc564e' }}/> */}
                 
            </ImageBackground >
        );
    }
}


export default RegisterScreen;

const styles = StyleSheet.create({
    Container: { width: '100%', height: '100%' },
    imagecontainer: {
        width: '100%', height: 80, resizeMode: 'contain', marginLeft: 'auto', marginRight: 'auto',
        ...Platform.select({
            ios: { marginTop: 50, },
            android: { marginTop: 40, }
        })
    },
    formContainer: { width: '100%', marginTop: 30, marginBottom: 100, alignItems: 'center', },
    headerText: { fontSize: 24, textAlign: "center", margin: 10, color: "white", fontWeight: "bold", marginTop: 50 },
    Buttonwarp: {
        position: 'absolute', flex: 0.1, left: 0, right: 0, bottom: 0, flexDirection: 'row', height: 80,
        alignItems: 'center', width: '100%'
    },
    bottomStyle: {
        width: '100%', backgroundColor: '#fc564e', borderRadius: 0,
        ...Platform.select({
            ios: { height: 80 },
            android: { height: 60 }
        })
    },
    BottomText: { fontSize: 16 },
    InputStyle: { borderRightColor: 'white', borderRightWidth: 1, height: 50, width: '100%', textAlign: 'center', color: 'white', },
    InputStyle2: { height: 50, textAlign: 'center', color: 'white', },
    InputStyle3: { height: 50, color: 'white', textAlign: 'center', width: '100%', },
    inputstyle4: { textAlign: 'center', color: 'white', width: '100%', height: 50, },
    pickerStyle: { position: 'absolute', bottom: 0, left: 0, right: 0 },
    errormsg: { textAlign: 'center', width: '100%', position: 'absolute', color: 'yellow', marginTop: 10 },



})
