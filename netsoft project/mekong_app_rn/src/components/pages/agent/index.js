import React, { Component } from 'react';
import { StyleSheet, View, ScrollView, FlatList, Image, TouchableOpacity, Platform, Text, ActivityIndicator } from 'react-native';
import { Actions } from 'react-native-router-flux';
import IconFA5 from "react-native-vector-icons/FontAwesome5";
import { AgentListApi } from '../../../../PostApi';

import { Select, Option } from "react-native-chooser";


const numColumns = 2;

const dataState = [
    { 'state': 'SABAH' }, { 'state': 'JOHOR' }, { 'state': 'KUALA LUMPUR' }, { 'state': 'KELANTAN' }, { 'state': 'LABUAN' }, { 'state': 'MELAKA' },
    { 'state': 'NEGERI SEMBILAN' }, { 'state': 'PULAU PINANG' }, { 'state': 'PAHANG' }, { 'state': 'PERAK' }, { 'state': 'SELANGOR' }, { 'state': 'SARAWAK' },
    { 'state': 'TERENGGANU' },
]

class Agent extends Component {

    constructor(props) {
        super(props);
        this.state = {
            text: '',
            isLoading: true,
            title: 'KUALA LUMPUR',
            displayData: [],
            childVisible: false
        };

    }

    onSelect(value, ) {
        this.setState({
            title: value,

        });
    }

    componentDidMount() {
        AgentListApi('KUALA LUMPUR').then((fetchData) => {
            this.setState({
                displayData: fetchData,
                childVisible: true

            })
        })
    }

    componentDidUpdate() {

        this.AgentList();

    }


    AgentList(title = this.state.title) {
        //console.log(title, '----------------title api')
        let url = `https://goldendestinations.com/api/new/agent.php?gdsecode=GDFX2019213VJVCQWDCZXZUIQW&title=${title}`;
        stateObj = this;
        fetch(url)
            .then((res) => res.json())
            .then(function (myJson) {
                //console.log(myJson, '---------title')
                stateObj.setState({
                    data: myJson,
                    isLoading: false,
                    childVisible: false

                });
            });

    }


    /************************* Render Item in FlatList *************************/
    renderItem = ({ item }) => {
        // console.log(item);
        //console.log(this.state.data.city,'city-----------item')
        return (
            <View style={styles.ImageWarpper}>
                <TouchableOpacity onPress={() => Actions.Agentinfor({ data: item })}>
                    <View style={styles.ImageInner} >
                        <View style={styles.ImageInnerWarp}>
                            <Image source={{ uri: item.agentlogo }}
                                style={styles.ImageStyle} />
                        </View>
                        <View style={styles.textWarp}>
                            <Text style={styles.TextImage}>{item.company_name}</Text>
                            {/* <Text style={styles.TextImage2}></Text>*/}
                        </View>
                    </View>
                </TouchableOpacity>
            </View>
        )
    }


    render() {
        const { isLoading, data, displayData, childVisible } = this.state;
        //console.log(this.state.displayData, 'render----------------displayData')
        // console.log(this.state.title, 'render----------------title')

        return (
            <View >
                <ScrollView>
                    <View>

                        <Select
                            onSelect={this.onSelect.bind(this)}
                            defaultText={this.state.title}
                            onChange={(e) => this.onSelectCurrency(e.target.value)}
                            style={{ borderWidth: 0, width: '100%', height: 50, backgroundColor: '#fff' }}
                            textStyle={{ color: 'red', textAlign: 'center', fontSize: 17, width: '100%', }}
                            backdropStyle={{ backgroundColor: "#00000078" }}
                            optionListStyle={{ backgroundColor: "#F5FCFF", height: 200 }}
                            animationType='none'
                            transparent={true}
                            indicator='down'
                            indicatorColor='red'
                            indicatorSize={Platform.OS === 'ios' ? 15 : 12}
                            indicatorStyle={{ marginTop: Platform.OS === 'ios' ? 10 : 2, right: 30 }}
                        >
                            {
                                dataState.map((item, i) => {
                                    return (
                                        <Option value={item.state} key={i}>{item.state}</Option>
                                    )
                                })
                            }


                        </Select>

                        <View style={{ padding: 15, }}>
                            {
                                isLoading ? (
                                    <View style={{ marginTop: 100 }}>
                                        <Text style={{ textAlign: 'center', marginBottom: 10 }}>{global.t('Fetching_data')}</Text>
                                        <ActivityIndicator size="large" color="#de2d30" />
                                    </View>

                                ) : (
                                        childVisible ? (
                                            <FlatList
                                                data={displayData}
                                                renderItem={this.renderItem}
                                                numColumns={numColumns}
                                                style={styles.FlatContainer}
                                            />
                                        ) : (
                                                <FlatList
                                                    data={data}
                                                    renderItem={this.renderItem}
                                                    numColumns={numColumns}
                                                    style={styles.FlatContainer}
                                                />
                                            )
                                    )
                            }
                        </View>

                    </View>

                </ScrollView>
            </View >
        );
    }
}


export default Agent;

const styles = StyleSheet.create({
    FlatContainer: { flex: 1, marginVertical: 10, },
    IconStyle: {
        textAlign: 'center',
        ...Platform.select({
            ios: { color: 'black' },
            android: { color: 'black' }
        })
    },
    TitleText: {
        textAlign: 'center', fontSize: 18, marginTop: 5, fontWeight: 'bold',
        ...Platform.select({
            ios: { color: 'black' },
            android: { color: 'black' }
        })
    },
    ContentText: { textAlign: 'center', marginTop: 20, paddingLeft: 10, paddingRight: 10, color: '#606c77' },
    textWarp: { borderBottomLeftRadius: 5, borderBottomRightRadius: 5, padding: 8, height: 50, backgroundColor: '#fcf5f5' },
    ImageWarpper: { width: '50%', paddingLeft: 10, paddingRight: 10, marginBottom: 20, },
    ImageInner: { backgroundColor: 'white', borderRadius: 5, },
    ImageInnerWarp: { borderTopRightRadius: 5, borderTopLeftRadius: 5, overflow: 'hidden', borderWidth: 1, borderColor: 'gray' },
    ImageStyle: { height: 150, width: null, flex: 1, },
    TextImage: {
        textAlign: 'center', fontSize: 12,
        ...Platform.select({
            ios: { color: '#3b4045' },
            android: { color: '#3b4045' }
        })
    },
    TextImage2: { textAlign: 'left', fontSize: 12, color: '#0095ff', marginTop: 5 },
    TopIcon: { resizeMode: 'contain', width: 80, height: 80 },

})
