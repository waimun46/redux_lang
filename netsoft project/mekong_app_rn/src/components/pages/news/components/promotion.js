import React, { Component } from 'react';
import { StyleSheet, View, ScrollView, FlatList, Image, TouchableOpacity, ImageBackground, ActivityIndicator } from 'react-native';
import { Container, Header, Content, List, ListItem, Thumbnail, Text, Left, Body, Right, Button } from 'native-base';
import { Actions } from 'react-native-router-flux';
import IconFA5 from "react-native-vector-icons/FontAwesome5";
import { PromotionArticleApi } from '../../../../../PostApi';


keyExtractor = (item) => item.key;


class Promotions extends Component {

    constructor(props) {
        super(props);
        this.state = {
            data: [],
            isLoading: true
        }
    }

    componentDidMount() {

        /************************* Fetch Data in Api *************************/
        PromotionArticleApi().then((fetchData) => {
            this.setState({
                data: fetchData,
                isLoading: false
            })
        })

    }

    renderItem = ({ item }) => {
        return (

            <List>
                <ListItem thumbnail>
                    <Left >
                        <Thumbnail square source={{ uri: item.pic_url }} style={{ height: 80, width: 80 }} />
                    </Left>
                    <Body style={{ height: 100 }}>
                        <Text style={styles.ListStyle}>{item.title}</Text>
                    </Body>
                    <Right>
                        <TouchableOpacity onPress={() => Actions.viewinfor({ data: item })}>
                            <View >
                                <Text note style={styles.ButtonText}>{global.t('View')} ></Text>
                            </View>
                        </TouchableOpacity>
                    </Right>
                </ListItem>
            </List>

        )
    }

    render() {
        //console.log(this.state.data, '------------data')
        const { data, isLoading } = this.state;
        return (
            <View>
                <ImageBackground
                    style={styles.TitleTextContanier}
                    source={require('../../../../assets/images/img/newsbg.jpg')}
                />
                <View style={[styles.overlay, { height: 130 }]}></View>
                <Text style={styles.TitleText}>{global.t('PROMOTION')}</Text>

                <View>
                    {
                        isLoading ? (
                            <ActivityIndicator size="large" color="#de2d30" style={styles.loading} />
                        ) : (
                                <FlatList
                                    data={data}
                                    renderItem={this.renderItem}
                                    keyExtractor={keyExtractor}
                                />
                            )
                    }

                </View>
            </View>
        );
    }
}


export default Promotions;

const styles = StyleSheet.create({
    TitleTextContanier: { height: 130 },
    TitleText: {
        textAlign: 'center', fontWeight: 'bold', fontSize: 25, color: '#fff', left: 0, top: 0, bottom: 0, right: 0,
        padding: 50, textShadowOffset: { width: 2, height: 2 },
        textShadowRadius: 2, textShadowColor: '#000', position: 'absolute',
    },
    overlay: { flex: 1, position: 'absolute', left: 0, top: 0, opacity: 0.3, backgroundColor: 'black', width: '100%' },
    Textstyle: { textAlign: 'center', paddingTop: 10, color: '#7e888b', fontSize: 14 },
    ButtonStyle: { backgroundColor: '#ff2951', borderRadius: 15, },
    ButtonText: { color: '#de2d30', paddingRight: 10, paddingLeft: 8, textAlign: 'right', fontSize: 14,  },
    ListStyle: { fontWeight: 'bold', },
    loading: { marginTop: 50 }
})
