import React, { Component } from 'react';
import { ScrollView, StyleSheet, View, Image, } from 'react-native';
import { Text, List, ListItem, Body, } from 'native-base';
import { ipt } from '../data';


class IPT extends Component {

    constructor(props) {
        super(props)
        this.state = {
            dataIPT: ipt,
        }
    }

    render() {
        const { dataIPT } = this.state;
        //console.log(dataIPT, 'dataIPT')

        const data = dataIPT.map((item, i) => {
            return (
                <List style={{ backgroundColor: '#fff', marginBottom: 10, }} key={i}>
                    <ListItem style={{ borderBottomWidth: 0 }}>
                        <Body>
                            <View style={[styles.contentWarp, { marginBottom: 5 }]}>
                                <Text note style={styles.titlecontent}>{global.t('Company_Name')} :</Text>
                                <Text note style={styles.contenttext}>{item.company_name}</Text>
                            </View>
                            <View style={[styles.contentWarp, { marginBottom: 5 }]}>
                                <Text note style={styles.titlecontent}>{global.t('Destinations')} :</Text>
                                <Text note style={styles.contenttext}>{item.destinations}</Text>
                            </View>

                            <View style={styles.contentWarp}>
                                <Text note style={styles.titlecontent}>{global.t('Group_Size')} :</Text>
                                <Text note style={styles.contenttext}>{item.group_size}</Text>
                            </View>
                        </Body>

                    </ListItem>
                </List>
            )
        })

        return (
            <View style={{ flex: 1, }}>
                <ScrollView>
                    <View style={{ backgroundColor: '#fff' }}>
                        <Image source={require('../../../../../assets/images/iptlogo.png')}
                            style={{ resizeMode: 'contain', width: '100%', height: 150 }}
                        />
                    </View>
                    <View style={{ padding: 10 }}>{data}</View>
                </ScrollView>
            </View>

        );
    }
}



export default IPT;

const styles = StyleSheet.create({
    btnsgStyle: {
        width: '50%', textAlign: 'center', alignItems: 'center',
        borderTopLeftRadius: 0, borderBottomLeftRadius: 0,
        borderTopRightRadius: 0, borderBottomRightRadius: 0, height: 40
    },
    bodyWarp: { width: '100%', padding: 10, paddingBottom: 0 },
    btntitle: { marginLeft: 'auto', marginRight: 'auto', color: '#de2d30' },
    titlecontent: { width: '40%', marginLeft: 0, marginRight: 0, textAlign: 'left', },
    contenttext: { textAlign: 'right', width: '60%', marginLeft: 0, marginRight: 0, fontWeight: 'bold' },
    contentWarp: { flexDirection: 'row', },



})