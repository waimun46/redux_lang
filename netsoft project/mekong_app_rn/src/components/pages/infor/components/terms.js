import React, { Component } from 'react';
import { Container, Header, Content, List, ListItem, Icon, Left, Body, Right, Switch, Button, } from 'native-base';
import { Platform, StyleSheet, Text, View, Image, StatusBar, ScrollView } from 'react-native';
import { Actions } from 'react-native-router-flux';
import IconSIM from "react-native-vector-icons/SimpleLineIcons";
import IconFTH from "react-native-vector-icons/Feather";


class TermsCondition extends Component {
    render() {
        return (
            <View style={{ backgroundColor: '#fff', flex: 1 }}>
                <ScrollView>
                    <View style={styles.topimgwarp}>
                        <Image source={require('../../../../assets/images/img/tc.jpg')} style={styles.topimg} />
                        <View style={[styles.overlay, { height: 200 }]}></View>
                        <View style={styles.toptextwarp}>
                            <Text style={styles.toptext}>{global.t('Terms&Condition')}</Text>
                        </View>
                    </View>

                    <View style={{ padding: 20, }}>
                        <Text style={styles.subtitle}>
                            Customers are deemed to have read, understood and accepted the following terms and conditions.
                            Appointed agents shall be known as “The Company” in the terms and conditions listed below: -
                        </Text>

                        <Text style={styles.contenttitle1}>Reservation:</Text>
                        <Text style={styles.contenttext}>
                            All reservations can be done through ICEB2B online booking, via email or fax.
                        </Text>

                        <Text style={styles.contenttitle}>
                            Deposit, Full payment and Cancellation chargers Terms & Conditions:
                        </Text>
                        <Text style={styles.contenttitle}>Deposit Payment:</Text>
                        <Text style={styles.contenttext}>
                            All tour packages require a deposit upon reservation has been made and confirmed.
                            The company does not recognize any tour booking form and exchange order as confirmation
                            of tours and bookings. Deposit paid to ICE Holidays per pax (except departure less than a month)
                            accordingly:
                        </Text>
                    </View>
                </ScrollView>
            </View>
        );
    }
}



export default TermsCondition;

const styles = StyleSheet.create({
    topimgwarp: { height: 200, },
    topimg: { resizeMode: 'cover', width: '100%', height: 200 },
    toptext: { color: '#fff', fontWeight: 'bold', fontSize: 30 },
    toptextwarp: { position: 'absolute', alignItems: 'center', width: '100%', top: 80 },
    subtitle: { textAlign: 'center', color: '#de2d30', },
    contenttitle: { textAlign: 'left', color: '#000', fontWeight: 'bold', marginTop: 20 },
    contenttext: { textAlign: 'left', color: '#777', marginTop: 10 },
    contenttitle1: { textAlign: 'left', color: '#000', fontWeight: 'bold', marginTop: 30 },
    overlay: { flex: 1, position: 'absolute', left: 0, top: 0, opacity: 0.5, backgroundColor: 'black', width: '100%' },


})