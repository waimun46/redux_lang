const dataArray = [
    {
        title_en: "OUR VISION & MISSION",
        title_cn: "我们的愿景和使命",
        contentTitle_1_en: "Our Vision",
        contentTitle_1_cn: "我们的愿景",
        content_1_en: "To be the market’s leading travel company.",
        content_1_cn: "成为市场领先的旅游公司。",
        contentTitle_2_en: "Our Mission",
        contentTitle_2_cn: "我们的使命",
        content_2_en: "Our goal is to assist with all your needs about traveling, allowing you to focus on your company and clients. Our commitment is to provide you with exceptional, innovative and personalized services in leisure, MICE, and corporate travel. Each program is tailor-made and unique to you, developed based on your company's needs.",
        content_2_cn: "我们的目标是协助您满足旅行的所有需求，让您专注于您的公司和客户。 我们的承诺是为您提供休闲，会展和商务旅行方面的卓越，创新和个性化服务。 每个程序都是根据您公司的需求量身定制的，是您独一无二的。",

    },
    {
        title_en: "PRODUCTS SERVICES",
        title_cn: "产品与服务",
        contentTitle_1_en: "Travel Service",
        contentTitle_1_cn: "旅游服务",
        content_1_en: "INBOUND & OUTBOUND",
        content_1_cn: "出/入境旅游",
        contentTitle_2_en: "Airline Ticketing",
        contentTitle_2_cn: "航空公司票务",
        content_2_en: "WORLDWIDE AIRLINES",
        content_2_cn: "国内/国际机票",

    },


];

const dataArray_2 = [
    {
        title_en: "TRAVEL SERVICES",
        title_cn: "旅行服务",
        subTitle_en: "INBOUND & OUTBOUND",
        subTitle_cn: "出 / 入境旅游",
        contentTitle_1_en: "LEISURE TRAVEL",
        contentTitle_1_cn: "休闲旅游",
        content_1_en: "Provide leisure package for all kinds of groups from one person to large number group",
        content_1_cn: "为各种群体提供从一个人到大量群体的休闲套餐。",
        contentTitle_2_en: "M.I.C.E SERVICES",
        contentTitle_2_cn: "会议/奖励旅游",
        content_2_en: "We offer excellent services for inbound and outbound Conferences, Meeting, Exhibition, Congress and Incentive Group to any destinations requested.",
        content_2_cn: "我们为入境和出境会议，会议，展览，会议和奖励小组提供优质服务，以满足任何目的地要求。",
        content_2b_en: "With intensive experience in ground operations for large groups, we create and implement unique and innovative programs supported by reliable and high quality services.",
        content_2b_cn: "凭借在大型团队的地面运营方面的丰富经验，我们创建并实施独特和创新的计划，并提供可靠和高质量的服务。"
    },

];

const dataArray_3 = [
    {
        title_en: "TICKETING SERVICES",
        title_cn: "票务服务",
        subTitle_en: "Airline GSA & Representation",
        subTitle_cn: "航空公司GSA和代表处",
        contentTitle_1_en: "PASSENGER",
        contentTitle_1_cn: "乘客",
        content_1_en: "We offer the best and most useful service for each individual who has great wish to travel to the world. Focusing on customer service is one the best way to build the bridge between us and our customers.",
        content_1_cn: "我们为每一位希望前往世界的人提供最好，最实用的服务。 专注于客户服务是建立我们与客户之间桥梁的最佳途径之一。",
        contentTitle_2_en: "CARGO",
        contentTitle_2_cn: "物品",
        content_2_en: "We provide shipping service through cargo system to help our customer easy to connect with the world without taking flight. Cargo has benefit itself such as reducing the amount of shipping time, safety & easy to use.",
        content_2_cn: "我们通过货运系统提供运输服务，帮助我们的客户轻松连接世界，无需搭乘航班。 货物本身有益，例如减少运输时间，安全和易于使用。",
        contentTitle_3_en: "CHARTER",
        contentTitle_3_cn: "宪章",
        content_3_en: "We arrange any private flight to and from any place enable our clients to select the schedule they need, from the point of origin to the destination they prefer. The private flight can be arranged for one-way flights or round trip flights for any number of passengers from 1 to maximum capacity of the aircraft (to as many as you need to transport).",
        content_3_cn: "我们安排往返任何地方的私人航班，使我们的客户能够选择他们需要的时间表，从他们喜欢的起点到目的地。 私人航班可以安排单程航班或往返航班，任何数量的乘客从1到最大容量的飞机（到您需要运输的数量）。",
        contentTitle_4_en: "GROUP BOOKING",
        contentTitle_4_cn: "团体预订",
        content_4_en: "We provide the most reasonable price flight for client to travel in group with specific schedule based on our client request. Our service is helpful for multi group who travel to any part of the world. ",
        content_4_cn: "我们根据客户要求，为客户提供最合理的价格航班，以便按照特定时间表在团体旅行。 我们的服务对于前往世界任何地方的多人团队都很有帮助。"
    },

];

const dataArray_incentive = [
    {
        title_en: "Experience & Knowledge",
        title_cn: "经验和知识",
        content_en: "Our team has many years of experience within the travel industry.",
        content_cn: "我们的团队在旅游行业拥有多年的经验。",
    },
    {
        title_en: "Unique Itinerary",
        title_cn: "独特的行程",
        content_en: "In consultation with our clients we created unique and cost effective itineraries for all flights and travel requirements.",
        content_cn: "在与我们的客户协商后，我们为所有航班和旅行要求创建了独特且具有成本效益的行程。",
    },
    {
        title_en: "Wholesale of outbound tour",
        title_cn: "出境旅游批发",
        content_en: "We are the FIRST wholesale of Cambodia outbound tour products which have more than 150 members in Phnom Penh and Siem Reap province.",
        content_cn: "我们是柬埔寨出境旅游产品的第一批批发商，在金边和暹粒省拥有超过150名会员。",
    },

];

const reference = [
    { logo: require('../../../../../assets/images/ref_logo/logo-01.png') },
    { logo: require('../../../../../assets/images/ref_logo/logo-02.png') },
    { logo: require('../../../../../assets/images/ref_logo/logo-03.png') },
    { logo: require('../../../../../assets/images/ref_logo/logo-04.png') },
    { logo: require('../../../../../assets/images/ref_logo/logo-05.png') },
    { logo: require('../../../../../assets/images/ref_logo/logo-06.png') },
    { logo: require('../../../../../assets/images/ref_logo/logo-07.png') },
    { logo: require('../../../../../assets/images/ref_logo/logo-08.png') },
    { logo: require('../../../../../assets/images/ref_logo/logo-09.png') },
    { logo: require('../../../../../assets/images/ref_logo/logo-10.png') },
    { logo: require('../../../../../assets/images/ref_logo/logo-11.png') },
    { logo: require('../../../../../assets/images/ref_logo/logo-12.png') },
    { logo: require('../../../../../assets/images/ref_logo/logo-13.png') },
    { logo: require('../../../../../assets/images/ref_logo/logo-14.png') },
    { logo: require('../../../../../assets/images/ref_logo/logo-15.png') },
    { logo: require('../../../../../assets/images/ref_logo/logo-16.png') },
    { logo: require('../../../../../assets/images/ref_logo/logo-17.png') },
    { logo: require('../../../../../assets/images/ref_logo/logo-18.png') },
    { logo: require('../../../../../assets/images/ref_logo/logo-19.png') },
    { logo: require('../../../../../assets/images/ref_logo/logo-20.png') },
    { logo: require('../../../../../assets/images/ref_logo/logo-21.png') },
    { logo: require('../../../../../assets/images/ref_logo/logo-22.png') },
    { logo: require('../../../../../assets/images/ref_logo/logo-23.png') },
    { logo: require('../../../../../assets/images/ref_logo/logo-24.png') },
    { logo: require('../../../../../assets/images/ref_logo/logo-25.png') },
    { logo: require('../../../../../assets/images/ref_logo/logo-26.png') },
    { logo: require('../../../../../assets/images/ref_logo/logo-27.png') },
    { logo: require('../../../../../assets/images/ref_logo/logo-28.png') },
    { logo: require('../../../../../assets/images/ref_logo/logo-29.png') },
    { logo: require('../../../../../assets/images/ref_logo/logo-30.png') },
    { logo: require('../../../../../assets/images/ref_logo/logo-31.png') },
    { logo: require('../../../../../assets/images/ref_logo/logo-32.png') },
    { logo: require('../../../../../assets/images/ref_logo/logo-33.png') },
    { logo: require('../../../../../assets/images/ref_logo/logo-34.png') },
    { logo: require('../../../../../assets/images/ref_logo/logo-35.png') },
    { logo: require('../../../../../assets/images/ref_logo/logo-36.png') },
    { logo: require('../../../../../assets/images/ref_logo/logo-37.png') },
    { logo: require('../../../../../assets/images/ref_logo/logo-38.png') },
    { logo: require('../../../../../assets/images/ref_logo/logo-39.png') },
    { logo: require('../../../../../assets/images/ref_logo/logo-40.png') },
    { logo: require('../../../../../assets/images/ref_logo/logo-41.png') },
    { logo: require('../../../../../assets/images/ref_logo/logo-42.png') },
    { logo: require('../../../../../assets/images/ref_logo/logo-43.png') },
    { logo: require('../../../../../assets/images/ref_logo/logo-44.png') },
];

const certificate = [
    { cer: require('../../../../../assets/images/certificate/cer-1.jpg') },
    { cer: require('../../../../../assets/images/certificate/cer-2.jpg') },
    { cer: require('../../../../../assets/images/certificate/cer-3.jpg') },
    { cer: require('../../../../../assets/images/certificate/cer-4.jpg') },
    { cer: require('../../../../../assets/images/certificate/cer-5.jpg') },
    { cer: require('../../../../../assets/images/certificate/cer-6.jpg') },
    { cer: require('../../../../../assets/images/certificate/cer-7.jpg') },
    { cer: require('../../../../../assets/images/certificate/cer-8.jpg') },
    { cer: require('../../../../../assets/images/certificate/cer-9.jpg') },
];

const partner = [
    {
        gsa: [
            { logo: require('../../../../../assets/images/partner_logo/hk.png') },
            { logo: require('../../../../../assets/images/partner_logo/aa.png') },
            { logo: require('../../../../../assets/images/partner_logo/jc.jpeg') },
        ],
        psa: [
            { logo: require('../../../../../assets/images/partner_logo/bas.png') },
            { logo: require('../../../../../assets/images/partner_logo/vet.jpg') },
            { logo: require('../../../../../assets/images/partner_logo/bay.png') },
        ],
        wholesale: [
            { logo: require('../../../../../assets/images/partner_logo/cebu.png') },
            { logo: require('../../../../../assets/images/partner_logo/lan.png') },
        ]
    }
];

const mekong = [
    {
        inbound: [
            { company_name: "Nokia", group_size: "20", destinations: "Angkor Wat", },
            { company_name: "VIP Group", group_size: "40", destinations: "Mekong Riverside", },
            { company_name: "CSL Incentive Group", group_size: "50", destinations: "Wat Phnom", },
            { company_name: "Adventure Group", group_size: "60", destinations: "Cambodia City", },
            { company_name: "ING & Public Bank", group_size: "60", destinations: "Angkor Wat", },
            { company_name: "Perodua Stocket Conference", group_size: "100", destinations: "Conference", },
            { company_name: "Incantive Group", group_size: "200", destinations: "Angkor Wat", },
            { company_name: "British American Tobacco", group_size: "500", destinations: "Conference", },
        ],
        outbound: [
            { company_name: "VIP Family Group", group_size: "15", destinations: "Bangkok-Pattaya", },
            { company_name: "DHL", group_size: "15", destinations: "Bangkok-Pattaya", },
            { company_name: "Manulife Cambodia", group_size: "80", destinations: "Hong Kong", },
            { company_name: "Master Color Group", group_size: "30", destinations: "Malaysia", },
            { company_name: "A-Tech Group", group_size: "15", destinations: "Malaysia-Singapore", },
            { company_name: "Manulife Cambodia", group_size: "93", destinations: "South Korea", },
            { company_name: "Manulife Cambodia & May Bank", group_size: "27", destinations: "South Korea", },
            { company_name: "UCMAS", group_size: "117", destinations: "Malaysia", },
            { company_name: "Manulife Cambodia", group_size: "67", destinations: "Thailand", },
            { company_name: "Manulife Cambodia & ABA Bank", group_size: "46", destinations: "Singapore", },
            { company_name: "Angkok Beer", group_size: "16", destinations: "South Korea", },
        ]
    },


];

const ipt = [
    { company_name: "Prudential", group_size: "592", destinations: "Hong Kong & Shenzhen", },
    { company_name: "AIA Malaysia", group_size: "296", destinations: "Langkawi", },
    { company_name: "AVON", group_size: "800", destinations: "Chiangmai", },
    { company_name: "BATM", group_size: "380", destinations: "Bangkok", },
    { company_name: "BPC", group_size: "17", destinations: "Greece", },
    { company_name: "CIMB", group_size: "169", destinations: "Europe", },
    { company_name: "HLA", group_size: "64", destinations: "Greece", },
    { company_name: "ING", group_size: "71", destinations: "London & Paris", },
    { company_name: "MayBank", group_size: "41", destinations: "Beijing", },
    { company_name: "NIAM", group_size: "160", destinations: "Rome", },
    { company_name: "PRU BSN", group_size: "79", destinations: "Bangkok", },
    { company_name: "Rockwills", group_size: "100", destinations: "ZhangJiaJie", },
    { company_name: "UNI Asia", group_size: "58", destinations: "Las Vegas", },
    { company_name: "Walton", group_size: "350", destinations: "Bali", },
    { company_name: "AIA", group_size: "522", destinations: "Monte Carlo / Paris", },
    { company_name: "BP Castrol", group_size: "65", destinations: "Europe", },
    { company_name: "CIMB", group_size: "191", destinations: "Switzerland", },
    { company_name: "Chemopharm", group_size: "37", destinations: "Shanghai", },
    { company_name: "ETIQA", group_size: "125", destinations: "Paris", },
    { company_name: "Maxis", group_size: "27", destinations: "Korea", },
    { company_name: "RHB", group_size: "89", destinations: "Turkey", },
    { company_name: "Panasonic", group_size: "44", destinations: "Croatia", },
    { company_name: "Perodua", group_size: "107", destinations: "Gold Coast", },
    { company_name: "Volkswagen", group_size: "19", destinations: "Puhket", },
    { company_name: "Yamaha", group_size: "74", destinations: "Taiwan", },

];

const office_data = [
    { no: "1", company_name: "Headquarters: MEKONG DISCOVERY CO.,LTD", phone: "(855) 23 992 323" },
    { no: "2", company_name: "AirAsia Travel & Service Centre (PNH)", phone: "(855) 23 983 777" },
    { no: "3", company_name: "AirAsia Sale Centre (AEON MALL BRANCH)", phone: "(855) 23 961 896" },
    { no: "4", company_name: "Flight Booking Centre (AIRPORT PNH)", phone: "(855) 81 777 988" },
    { no: "5", company_name: "Flight Booking Centre (SORYA CENTER POINT)", phone: "(855) 23 992 345" },
    { no: "6", company_name: "Flight Booking Centre (AEON MALL SEN SOK CITY)", phone: "(855) 23 919 888" },
    { no: "7", company_name: "AirAsia Travel & Service Centre (REP)", phone: "(855) 63 968 869" },

];
const contact_data = [
    {
        logo: require('../../../../../assets/images/mkcontact.jpg'), company_name: "MEKONG DISCOVERY CO., Ltd.",
        address: " Rivera Project, No H-01 & H-03, Street H, Sangkat Tonle Bassac,Khan Chamkamorn Phnom Penh",
        email: "corpsale@mekongdiscovery.org", fb: "mekongdiscovery", phone: "+ 855 81 967 888",
    }
];



export {
    dataArray, dataArray_2, dataArray_3, dataArray_incentive,
    reference, certificate, mekong, ipt, partner, office_data, contact_data
};