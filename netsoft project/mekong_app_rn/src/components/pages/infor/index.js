import React, { Component } from 'react';
import { Container, Header, Content, List, ListItem, Icon, Left, Body, Right, Switch, Button, TouchableHighlight } from 'native-base';
import { Platform, StyleSheet, Text, View, Image, StatusBar, ScrollView, Linking, AsyncStorage, } from 'react-native';
import { Actions } from 'react-native-router-flux';
import IconSIM from "react-native-vector-icons/SimpleLineIcons";
import RNRestart from "react-native-restart"

class InforScreen extends Component {

  constructor(props) {
    super(props);
    this.state = {
      tokenId: 0,
    }
  }

  componentDidMount() {

    /************************************ Get Store Data From AsyncStorage  ***********************************/
    AsyncStorage.getItem('USER_TOKEN').then(asyncStorageRes => {
      console.log(asyncStorageRes, '----------------InforScreen')
      this.setState({
        tokenId: asyncStorageRes
      })
      //console.log(this.state.tokenId, '-------------InforScreen')
    });

  }

  /************************************ Logout ***********************************/
  _logout() {
    AsyncStorage.removeItem('USER_TOKEN');
    Actions.Home();
  }


  /****************************************** Change Language ******************************************/
  change = async (language) => {
    console.info("== changing language to:" + language)
    await AsyncStorage.setItem("language", language)
    RNRestart.Restart()
  }



  render() {
    return (

      <ScrollView>

        {/******************************** CompanyProfile ********************************/}
        <ListItem icon style={styles.ListItemStyle1} onPress={() => Actions.company()} >
          <Left><IconSIM active name="briefcase" style={styles.iconstyle} /></Left>
          <Body><Text>{global.t('Company_Profile')}</Text></Body>
          <Right><Icon active name="arrow-forward" /></Right>
        </ListItem>

        {/******************************** REFERENCE ********************************/}
        <ListItem icon style={styles.ListItemStyle2} onPress={() => Actions.reference()} >
          <Left><IconSIM active name="trophy" style={styles.iconstyle} /></Left>
          <Body><Text>{global.t('Reference_Certificates_Partner')}</Text></Body>
          <Right><Icon active name="arrow-forward" /></Right>
        </ListItem>

         {/******************************** customer reference ********************************/}
         <ListItem icon style={styles.ListItemStyle2} onPress={() => Actions.customer()} >
          <Left><IconSIM active name="notebook" style={styles.iconstyle} /></Left>
          <Body><Text>{global.t('Customer_Reference')}</Text></Body>
          <Right><Icon active name="arrow-forward" /></Right>
        </ListItem>

        {/******************************** STRUCTURE & OFFICES ********************************/}
        <ListItem icon style={styles.ListItemStyle2} onPress={() => Actions.structure()} >
          <Left><IconSIM active name="layers" style={styles.iconstyle} /></Left>
          <Body><Text>{global.t('Structure_Office')}</Text></Body>
          <Right><Icon active name="arrow-forward" /></Right>
        </ListItem>


        {/******************************** GD Consortiums ********************************/}
        <ListItem icon style={styles.ListItemStyle2} onPress={() => Actions.consortiums()}>
          <Left><IconSIM active name="social-google" style={styles.iconstyle} /></Left>
          <Body><Text>{global.t('GD_Consortiums')}</Text></Body>
          <Right><Icon active name="arrow-forward" /></Right>
        </ListItem>

        {/******************************** Privacy Policy ********************************/}
        <ListItem icon style={styles.ListItemStyle1} onPress={() => Actions.privacy()}>
          <Left><IconSIM active name="lock" style={styles.iconstyle} /></Left>
          <Body><Text>{global.t('Privacy_Policy')}</Text></Body>
          <Right><Icon active name="arrow-forward" /></Right>
        </ListItem>

        {/******************************** Terms and Condition ********************************/}
        <ListItem icon style={styles.ListItemStyle2} onPress={() => Actions.terms()}>
          <Left><IconSIM active name="note" style={styles.iconstyle} /></Left>
          <Body><Text>{global.t('Terms&Condition')}</Text></Body>
          <Right><Icon active name="arrow-forward" /></Right>
        </ListItem>

        {/******************************** Feedback ********************************/}
        <ListItem icon style={styles.ListItemStyle2} onPress={() => Actions.feedback()} >
          <Left><IconSIM active name="bubbles" style={styles.iconstyle} /></Left>
          <Body><Text>{global.t('Feedback')}</Text></Body>
          <Right><Icon active name="arrow-forward" /></Right>
        </ListItem>

        {/******************************** Facebook ********************************/}
        <ListItem icon style={styles.ListItemStyle1}
          onPress={() => { Linking.openURL('https://www.facebook.com/mekongdiscovery/') }}>
          <Left><IconSIM active name="social-facebook" style={styles.iconstyle} /></Left>
          <Body><Text>Facebook</Text></Body>
          <Right><Icon active name="arrow-forward" /></Right>
        </ListItem>

        {/******************************** Instagram ********************************/}
        <ListItem icon style={styles.ListItemStyle2}
          onPress={() => { Linking.openURL('https://www.instagram.com/golden_destinations_official/') }}>
          <Left><IconSIM active name="social-linkedin" style={styles.iconstyle} /></Left>
          <Body><Text>Instagram</Text></Body>
          <Right><Icon active name="arrow-forward" /></Right>
        </ListItem>

        {/******************************** Youtube ********************************/}
        <ListItem icon style={styles.ListItemStyle2}
          onPress={() => { Linking.openURL('https://www.youtube.com/channel/UC5-TV4XEQORpKLBXNHtsJfA') }}>
          <Left><IconSIM active name="social-youtube" style={styles.iconstyle} /></Left>
          <Body><Text>Youtube</Text></Body>
          <Right><Icon active name="arrow-forward" /></Right>
        </ListItem>


        {/******************************** Change language ********************************/}
        <ListItem icon style={styles.ListItemStyle1} >
          <Left><IconSIM active name="globe" style={styles.iconstyle} /></Left>
          <Body><Text>{global.t('Change_language')}</Text></Body>
          <Right >
            <Button onPress={() => this.change('zh')} style={styles.btnstyle}>
              <Image source={require('../../../assets/images/icon/cn.png')} style={styles.langicon} />
            </Button>
          </Right>

          <Right >
            <Button onPress={() => this.change('en')} style={styles.btnstyle}>
              <Image source={require('../../../assets/images/icon/en.png')} style={styles.langicon} />
            </Button>
          </Right>

        </ListItem>



        {/******************************** Logout ********************************/}
        <ListItem icon style={[styles.ListItemStyle2, { marginBottom: 60 }]} onPress={() => this._logout()}>
          <Left><IconSIM active name="logout" style={styles.iconstyle} /></Left>
          <Body><Text>{global.t('Logout')}</Text></Body>
          <Right><Icon active name="arrow-forward" /></Right>
        </ListItem>

      </ScrollView>

    );
  }
}



export default InforScreen;

const styles = StyleSheet.create({
  ListWarp: { backgroundColor: '#e9ecef', paddingBottom: 50 },
  ListItemStyle1: { marginTop: 30, backgroundColor: 'white', marginLeft: 0 },
  ListItemStyle2: { backgroundColor: 'white', marginLeft: 0 },
  iconstyle: { color: '#de2d30', fontSize: 25, marginLeft: 15 },
  langicon: { width: 40, height: 25, },
  btnstyle: {
    backgroundColor: 'transparent', borderColor: 'transparent', borderWidth: 0,
    borderRadius: 0, height: 'auto', elevation: 0
  },

})