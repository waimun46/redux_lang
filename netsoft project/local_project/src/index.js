import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';
import HeaderScreen from './components/layout/header'
import MainContainer from './components/layout/main'


class Layout extends Component{
  render() {
    return (
        <View style={styles.container} style={{width: '100%'}}>
            
            <MainContainer/>            
        </View>
    );
  }
}



export default Layout;


const styles = StyleSheet.create({
  container: {
    flex: 1,
    
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
    color: '#ffffff',
  },
});