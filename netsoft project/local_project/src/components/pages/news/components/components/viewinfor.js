import React, { Component } from 'react';
import { StyleSheet, View, ScrollView, FlatList, Image } from 'react-native';
import { Container, Header, Content, List, ListItem, Thumbnail, Text, Left, Body, Right, Button } from 'native-base';

import { Actions } from 'react-native-router-flux';
import IconFA5 from "react-native-vector-icons/FontAwesome5";

const data = [
    {
        key: 'Sinchew Business Excellence Awards & Gala Lunch',
        imageUrl: 'https://www.goldendestinations.com/new/tadmin/images/uploads/images/Promotion/sinchew.jpg',
        content: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printertook a galley of type and scrambled it to make a type specimen book.It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using Content here, content here, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for lorem ipsum will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like'
    },
]



class ViewInfor extends Component {

    renderItem = ({ item }) => {
        return (
            <View style={styles.TextContainer}>
                <Text style={styles.TitleText}>{item.key}</Text>
                <View style={styles.ImageContainer}>
                    <Image source={{ uri: item.imageUrl }} style={styles.ImageStyle} />
                </View>
                <Text style={styles.TextStyle}>{item.content}</Text>
            </View>
        )
    }

    render(item) {
        return (
            <View style={{ backgroundColor: 'white' }}>
                <ScrollView>
                    <FlatList
                        data={data}
                        renderItem={this.renderItem}
                        keyExtractor={(item, index) => index}
                    />
                </ScrollView>
            </View>
        );
    }
}


export default ViewInfor;

const styles = StyleSheet.create({
    TextContainer: { padding: 20 },
    TitleText: { textAlign: 'left', fontSize: 22, fontWeight: 'bold', color: '#de2d30', textTransform: 'uppercase' },
    TextStyle: { color: '#999', textAlign: 'left', fontSize: 16,paddingBottom: 20 },
    ImageContainer: { paddingTop: 20, paddingBottom: 20 },
    ImageStyle: { height: 250, width: '100%', resizeMode: "cover", }

})
