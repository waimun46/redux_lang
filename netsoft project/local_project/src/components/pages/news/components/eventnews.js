import React, { Component } from 'react';
import { StyleSheet, View, ScrollView, FlatList, Image, TouchableOpacity } from 'react-native';
import { Container, Header, Content, List, ListItem, Thumbnail, Text, Left, Body, Right, Button } from 'native-base';

import { Actions } from 'react-native-router-flux';
import IconFA5 from "react-native-vector-icons/FontAwesome5";


keyExtractor = (item) => item.key;

const data = [
    { key: 'Golden Destinations Travel & Win 2018', },
    { key: 'Sinchew Business Excellence Awards & Gala Lunch', },
    { key: 'GD Flight Consortium List', },
    { key: 'GDTA 728 Luck Draw Winner List', },
    { key: 'GD Nite 2019', },
    { key: 'International Travel & Tourism Award', },
]


class EventNews extends Component {

    renderItem = ({ item }) => {
        return (

            <List>
                <ListItem thumbnail>
                    <Left >
                        <Thumbnail square
                            source={require('../../../../assets/images/img/co01.png')}
                            style={{ height: 80, width: 80 }}
                        />
                    </Left>
                    <Body style={{ height: 100 }}>
                        <Text style={styles.ListStyle} numberOfLines={2}>{item.key}</Text>
                        <Text note numberOfLines={1} style={{ marginTop: 5 }}>Its time to build a difference . .</Text>

                    </Body>
                    <Right>
                        <TouchableOpacity onPress={() => Actions.viewinfor()}>
                            <View style={styles.ButtonStyle}>
                                <Text style={styles.ButtonText}>VIEW</Text>
                            </View>
                        </TouchableOpacity>
                    </Right>
                </ListItem>
            </List>

        )
    }

    render() {
        return (
            <View>
                <View style={styles.TitleTextContanier}>
                    <Text style={styles.TitleText}>EVENTS & NEWS</Text>
                    <Text style={styles.Textstyle}>
                        Lorem Ipsum is simply dummy text of the printing and
                        typesetting industry. Lorem Ipsum has been the industry's
                        standard dummy text ever since the 1500s, when an unknown printer
                        took a galley of type and scrambled it to make a type specimen book.
                   </Text>
                </View>
                <View>
                    <FlatList
                        data={data}
                        renderItem={this.renderItem}
                    />
                </View>
            </View>
        );
    }
}


export default EventNews;

const styles = StyleSheet.create({
    TitleTextContanier: { padding: 30 },
    TitleText: { textAlign: 'center', fontWeight: 'bold', fontSize: 20 },
    Textstyle: { textAlign: 'center', paddingTop: 10, color: '#7e888b', },
    ButtonStyle: { backgroundColor: '#ff2951', borderRadius: 30, },
    ButtonText: { fontSize: 16, color: 'white', fontWeight: 'bold', paddingLeft: 15, paddingRight: 15, paddingTop: 5, paddingBottom: 5 },
    ListStyle: { fontWeight: 'bold', }
})
