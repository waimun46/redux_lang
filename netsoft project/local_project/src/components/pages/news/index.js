import React, { Component } from 'react';
import { StyleSheet, View, ScrollView, FlatList, Image, Dimensions } from 'react-native';
import { Container, Header, Content, Tab, Text, Tabs } from 'native-base';
import { Actions } from 'react-native-router-flux';
import IconFA5 from "react-native-vector-icons/FontAwesome5";
import EventNews from './components/eventnews';
import Blog from './components/blog';
import Promotions from './components/promotion';



class Newspage extends Component {

    render() {
        return (
            <View style={{ backgroundColor: 'white', }}>
                <ScrollView >
                    <View style={{ paddingBottom: 30 }}>
                        <Tabs
                            tabBarUnderlineStyle={{ backgroundColor: "#de2d30" }}
                            style={{ backgroundColor: 'white', }}
                        >
                            <Tab
                                heading="EVENTS & NEWS"
                                tabStyle={{ backgroundColor: "white" }}
                                activeTabStyle={{ backgroundColor: "white", }}
                                tabContainerStyle={{ backgroundColor: "#de2d30" }}
                                textStyle={{ color: '#ccc', fontSize: 15, fontWeight: 'bold', }}
                                activeTextStyle={{ color: 'red', fontSize: 15, fontWeight: 'bold' }}
                            >
                                <EventNews />
                            </Tab>
                            <Tab
                                heading="BLOG"
                                tabStyle={{ backgroundColor: "white", }}
                                activeTabStyle={{ backgroundColor: "white", }}
                                tabContainerStyle={{ backgroundColor: "#de2d30" }}
                                textStyle={{ color: '#ccc', fontSize: 15, fontWeight: 'bold' }}
                                activeTextStyle={{ color: 'red', fontSize: 15, fontWeight: 'bold' }}
                            >
                                <Blog />
                            </Tab>
                            <Tab
                                heading="PROMOTION"
                                tabStyle={{ backgroundColor: "white" }}
                                activeTabStyle={{ backgroundColor: "white", }}
                                tabContainerStyle={{ backgroundColor: "#de2d30" }}
                                textStyle={{ color: '#ccc', fontSize: 15, fontWeight: 'bold' }}
                                activeTextStyle={{ color: 'red', fontSize: 15, fontWeight: 'bold' }}
                            >
                                <Promotions />
                            </Tab>
                        </Tabs>
                    </View>
                </ScrollView>
            </View>
        );
    }
}


export default Newspage;

const styles = StyleSheet.create({

})
