import React, { Component } from 'react';
import { StyleSheet, View, ScrollView, FlatList, Image, ImageBackground, Dimensions, TouchableOpacity } from 'react-native';
import { Container, Header, Content, Tab, Tabs, Text } from 'native-base';
import { Actions } from 'react-native-router-flux';


const data = [
    { key: 'China' }, { key: 'Japan' }, { key: 'Korea' }, { key: 'Dubai' }, { key: 'USA' }, { key: 'NZ' }, { key: 'Malaysia' }, { key: 'Penang' }, { key: 'Johor' },
    { key: 'Singapore' }, { key: 'Taiwan' }, { key: 'London' },

];

const formatData = (data, numColumns) => {
    const numberOfFullRows = Math.floor(data.length / numColumns);

    let numberOfElementsLastRow = data.length - (numberOfFullRows * numColumns);


    return data;
};

const numColumns = 3;

class CountryData extends Component {

    renderItem = ({ item }) => {
        return (
            <TouchableOpacity onPress={() => Actions.countryinfor()} style={{ width: '33.33%' }}>
                <View>
                    <ImageBackground source={require('../../../../../assets/images/img/gd04.png')} style={styles.item}>
                        <Text style={styles.TextImage}>{item.key}</Text>
                    </ImageBackground>
                </View>
            </TouchableOpacity>

        )
    }


    render() {
        return (

            <FlatList
                data={formatData(data, numColumns)}
                renderItem={this.renderItem}
                numColumns={numColumns}
                style={styles.Flatcontainer}

            />

        );
    }
}


export default CountryData;

const styles = StyleSheet.create({
    Flatcontainer: {
        flex: 1,
        marginVertical: 10,
    },
    item: {
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
        margin: 2,
        height: Dimensions.get('window').width / numColumns, // approximate a square
    },
    TextImage: {
        fontSize: 25, color: 'white', fontWeight: 'bold', textShadowColor: 'black',
        textShadowOffset: { width: 1, height: 4 },
        textShadowRadius: 5
    }
})
