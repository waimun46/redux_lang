import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Button, Image, ScrollView, TouchableOpacity } from 'react-native';
import { Actions } from 'react-native-router-flux';


class GdSeries extends Component {
  render() {
    return (
      <View style={{ backgroundColor: 'white', }}>
        <ScrollView>
          <View style={{ padding: 30, }}>
            <View style={styles.ImageContainer} >
              <TouchableOpacity onPress={() => Actions.gdPremium()}>
                <Image source={require('../../../assets/images/img/gd02.png')} style={styles.ImageStyle} />
              </TouchableOpacity>
            </View>
            <View style={styles.ImageContainer}>
              <Image source={require('../../../assets/images/img/gd01.png')} style={styles.ImageStyle}
              />
            </View>
            <View style={styles.ImageContainer}>
              <Image
                source={require('../../../assets/images/img/gd03.png')} style={styles.ImageStyle} />
            </View>

            <View style={styles.ImageContainer}>
              <Image source={require('../../../assets/images/img/gd04.png')} style={styles.ImageStyle} />
            </View>
          </View>
        </ScrollView>
      </View>

    );
  }
}



export default GdSeries;

const styles = StyleSheet.create({
  ImageContainer: { marginBottom: 20, height: 150 },
  ImageStyle: { width: "100%", resizeMode: 'cover', height: 150, borderRadius: 3, }

})