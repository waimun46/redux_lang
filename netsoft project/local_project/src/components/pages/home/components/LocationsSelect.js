import React, { Component } from 'react';
import { StyleSheet, View, Image } from 'react-native';
import { Container, Header, Content, Card, CardItem, Thumbnail, Text, Button, Icon, Left, Body, Right } from 'native-base';
import { Actions } from 'react-native-router-flux';


class LocationsSelect extends Component {
    render() {
        return (
            <View>
                <View style={styles.Container}>
                    <View style={{ width: '70%', }}>
                        <View style={styles.LeftTopContainer}>
                            <Image
                                source={require('../../../../assets/images/img/nz.png')}
                                style={styles.LeftTopImage}
                            />
                            <Text style={styles.TextLeftTop}>RM 3421</Text>
                        </View>
                        <View style={styles.LeftBottomContainer}>
                            <View style={styles.LeftBottomLeftImage}>
                                <Image
                                    source={require('../../../../assets/images/img/kr.png')}
                                    style={styles.LeftBottomLeftImageImg}
                                />
                                <Text style={styles.TextLeftBottom}>RM 3421</Text>
                            </View>
                            <View style={styles.LeftBottomRightImage}>
                                <Image
                                    source={require('../../../../assets/images/img/id.png')}
                                    style={styles.LeftBottomRightImageImg}
                                />
                                <Text style={styles.TextLeftBottom}>RM 3421</Text>
                            </View>
                        </View>
                    </View>
                    <View style={{ width: '30%', }}>
                        <View style={styles.RightTopContainer}>
                            <View style={styles.RightImagePadding}>
                                <Image
                                    source={require('../../../../assets/images/img/jp.png')}
                                    style={styles.RightImageImg}
                                />
                                <Text style={styles.TextRight}>RM 3421</Text>
                            </View>
                            <View style={styles.RightImagePadding}>
                                <Image
                                    source={require('../../../../assets/images/img/pr.png')}
                                    style={styles.RightImageImg}
                                />
                                <Text style={styles.TextRight}>RM 3421</Text>
                            </View>
                            <View style={styles.RightImagePadding}>
                                <Image
                                    source={require('../../../../assets/images/img/db.png')}
                                    style={styles.RightImageImg}
                                />
                                <Text style={styles.TextRight}>RM 3421</Text>
                            </View>
                        </View>
                    </View>
                </View>
                <View style={{ width: '100%' }}>
                    <Text style={styles.ViewText}>View More</Text>
                </View>
            </View>

        );
    }
}



export default LocationsSelect;


const styles = StyleSheet.create({
    Container: { flex: 1, flexDirection: 'row', padding: 10 },
    //Left image
    LeftTopContainer: { height: 168, paddingRight: 8, },
    LeftTopImage: { width: "100%", resizeMode: 'cover', flex: 1, borderRadius: 3, position: 'relative' },
    LeftBottomContainer: { flex: 1, flexDirection: 'row',height: 40, paddingRight: 8, width: '100%', paddingTop: 8 , },
    LeftBottomLeftImage: {  width: "50%",},
    LeftBottomLeftImageImg: { width: "100%", resizeMode: 'cover', height: 80, borderRadius: 3,position: 'relative'  },
    LeftBottomRightImage: { width: "50%", alignItems: 'flex-end', paddingLeft: 8, },
    LeftBottomRightImageImg: { width: "100%", resizeMode: 'cover', height: 80, borderRadius: 3,position: 'relative'  },

    TextLeftTop: {position: 'absolute', bottom: 0, right: 8, padding: 8, backgroundColor: '#de2d3091', color: 'white',},
    TextLeftBottom: {position: 'absolute',bottom: 8, textAlign: 'right' ,paddingLeft: 10,
        paddingRight: 10,backgroundColor: '#de2d3091', color: 'white',  right: 0
    },
    TextRight: {position: 'absolute', bottom: 8, right: 0,  backgroundColor: '#de2d3091', color: 'white', paddingRight: 10, paddingLeft: 10},


    //Right image
    RightTopContainer: { flex: 1, flexDirection: 'column', },
    RightImagePadding: { paddingBottom: 8 },
    RightImageImg: { width: "100%", resizeMode: 'cover', height: 80, borderRadius: 3, },
    //View more
    ViewText: { textAlign: 'center', fontSize: 20, color: '#de2d30', marginTop: -10, marginBottom: 10 }

})

