import React, { Component } from 'react';
import { StyleSheet, View, Platform } from 'react-native';
import { Container, Header, Content, Card, CardItem, Thumbnail, Text, Button, Icon, Left, Body, Right } from 'native-base';
import { Actions } from 'react-native-router-flux';
import IconMAT from "react-native-vector-icons/MaterialIcons";


class TimeLocations extends Component {
    render() {
        return (
            <View style={styles.ViewContainer}>
                <View style={styles.TextContainer} >
                    <View >
                        <Text style={{ fontSize: 22 }} >32°</Text>
                    </View>
                    <View style={styles.DegreeContainer}>
                        <Text numberOfLines={1} ellipsizeMode='tail' >Sunny</Text>
                        <Text style={{ marginTop: -3, }} numberOfLines={1} ellipsizeMode='tail'>5 Mac 19</Text>
                    </View>
                </View>
                <View style={styles.TextContainer} >
                    <Text style={styles.HotTitle} uppercase={false}>Hot</Text>
                    <Text style={styles.PickTitle} uppercase={false} >Pick</Text>
                </View>
                <View style={styles.LocationContainer}>
                    <View
                        style={styles.LocationStyle}
                        numberOfLines={1}
                        ellipsizeMode='tail'
                    >
                        <View style={styles.LocationTitle}>
                            <IconMAT name="room" style={{ fontSize: 22, color: '#de2d30', }} />
                            <Text numberOfLines={1} ellipsizeMode='tail' style={styles.LocationText}>Puchong</Text>
                        </View>
                    </View>
                </View>
            </View>
        );
    }
}

export default TimeLocations;

const styles = StyleSheet.create({
    TextContainer: { flexDirection: 'row', justifyContent: 'space-between', width: '33.33%' },
    ViewContainer: { padding: 10, flexDirection: 'row', justifyContent: 'space-between', },
    DegreeContainer: { textAlign: 'left', width: '70%', fontSize: 20, position: 'absolute', left: 40 },
    TextWarp: { paddingRight: 13, paddingLeft: 13, },
    HotTitle: { fontSize: 25, color: '#de2d30', fontWeight: 'bold', marginRight: 5, width: '50%', textAlign: 'right' },
    PickTitle: { fontSize: 25, color: 'black', fontWeight: 'bold', width: '50%', textAlign: 'left' },
    LocationContainer: { flexDirection: 'row', justifyContent: 'space-between', width: '33.33%', marginTop: 5 },
    LocationStyle: { fontSize: 25, width: '100%', lineHeight: 25, textAlign: 'right', },
    LocationTitle: { textAlign: 'right', flexDirection: 'row', justifyContent: 'flex-end', width: 80, alignSelf: 'flex-end' },
    LocationText: { fontSize: 16, textAlign: 'right', alignSelf: 'flex-end', },

})
