/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Image, StatusBar } from 'react-native';
import { Icon, StyleProvider, Container } from 'native-base';
import getTheme from './src/themes/components';
import appColor from './src/themes/variables/appColor';
import IconMCI from "react-native-vector-icons/MaterialCommunityIcons";
import IconANT from "react-native-vector-icons/AntDesign";
import IconMAT from "react-native-vector-icons/MaterialIcons";
import IconSIM from "react-native-vector-icons/SimpleLineIcons";
import IconFA from "react-native-vector-icons/FontAwesome";

import { Router, Scene, Actions, Lightbox } from 'react-native-router-flux';

import GdSeries from './src/components/pages/gdseries';
import GDPremium from './src/components/pages/gdseries/components/gdpremium';
import CountryInfor from './src/components/pages/gdseries/components/components/countryinfor';
import CountryInforList from './src/components/pages/gdseries/components/components/countryinforlist';
import HomesScreen from './src/components/pages/home';
import InforScreen from './src/components/pages/infor';
import ProfilePage from './src/components/pages/profile';
import Newspage from './src/components/pages/news';
import Agent from './src/components/pages/agent';
import DepartureInfor from './src/components/pages/departure';
import ViewInfor from './src/components/pages/news/components/components/viewinfor';
import EmailVailed from './src/components/pages/emailvailed';
import RegisterScreen from './src/components/pages/register';

const iconLogo = () => (
  <Image source={require('./src/assets/images/logo.png')} style={{ width: 60, height: 60, marginTop: -30 }} />
)

const TabIcon = ({ selected, title }) => {
  return (
    <Text style={{ color: selected ? '#de2d30' : '#d6cccc' }}>{title}</Text>
  );
}

const Rightbtn = () => {
  return (
    <View style={{ flex: 1, flexDirection: 'row' }}>
      <IconANT name='infocirlceo' style={{ fontSize: 28, paddingRight: 10, color: 'white' }} onPress={() => Actions.Infor()} />
      <IconMCI name='account' style={{ fontSize: 30, paddingRight: 10, color: 'white' }} onPress={() => Actions.profile()} />
    </View>
  );
}


class App extends Component {
  render() {
    return (
      <Container >
        <StatusBar barStyle="light-content" backgroundColor="#de2d30" />
        <Router hideNavBar navigationBarStyle={{ backgroundColor: '#de2d30' }} iosBarStyle={"light-content"}  >
          <Lightbox>
            <Scene key="root" hideNavBar>

              {/* 
              <Scene key="email" component={EmailVailed} hideNavBar={false} />
           
              <Scene
                headerLayoutPreset="center"
                titleStyle={{ color: 'white' }}
                tabBarStyle={{ backgroundColor: '#fcf5f5' }}
                swipeEnabled={true}
                activeTintColor={'#de2d30'}
              >
               <Scene key="register" component={RegisterScreen} title="Create Account" hideTabBar={false} />
              </Scene>
   */}

              <Scene
                headerLayoutPreset="center"
                tabs={true}
                icon={TabIcon}
                titleStyle={{ color: 'white' }}
                tabBarStyle={{ backgroundColor: '#fcf5f5' }}
                swipeEnabled={true}
                activeTintColor={'#de2d30'}

              >


                {/************ home ************/}
                <Scene
                  tabBarLabel="Home"
                  icon={({ focused }) => (
                    <IconANT name='home' size={25} style={{ color: focused ? '#de2d30' : '#d6cccc' }} />
                  )}
                >
                  <Scene key="Home" component={HomesScreen} initial={true} title="Golden Destinations" renderRightButton={Rightbtn} />
                  <Scene key="Infor" component={InforScreen} title="Information" hideTabBar={true} />
                  <Scene key="profile" component={ProfilePage} title="Personal Profile" hideTabBar={true} />

                </Scene>


                {/************ News ************/}
                <Scene
                  tabBarLabel="News"
                  renderRightButton={Rightbtn}
                  icon={({ focused }) => (
                    <IconFA name='newspaper-o' size={25} style={{ color: focused ? '#de2d30' : '#d6cccc' }} />
                  )}
                >
                  <Scene key="News" component={Newspage} initial={true} title="News" />
                  <Scene key="viewinfor" component={ViewInfor} title="News" />

                  {/*--- Rightbtn Screen ---*/}
                  <Scene key="Infor" component={InforScreen} title="Information" hideTabBar={true} />
                  <Scene key="profile" component={ProfilePage} title="Personal Profile" hideTabBar={true} />
                </Scene>


                {/************ GdSeries ************/}
                <Scene
                  tabBarLabel=" "
                  icon={iconLogo}
                  renderRightButton={Rightbtn}

                >
                  <Scene key="gdSeries" component={GdSeries} initial={true} title="GD Series" />
                  <Scene key="gdPremium" component={GDPremium} title="GD Premium" />
                  <Scene key="countryinfor" component={CountryInfor} title="Country Information" />
                  <Scene key="countryinforlist" component={CountryInforList} title="Information List" />

                  {/*--- Rightbtn Screen ---*/}
                  <Scene key="Infor" component={InforScreen} title="Information" hideTabBar={true} />
                  <Scene key="profile" component={ProfilePage} title="Personal Profile" hideTabBar={true} />
                </Scene>


                {/************ Agent ************/}
                <Scene
                  tabBarLabel="Agent"
                  renderRightButton={Rightbtn}
                  icon={({ focused }) => (
                    <IconSIM name='people' size={25} style={{ color: focused ? '#de2d30' : '#d6cccc' }} />
                  )}
                >
                  <Scene key="Agent" component={Agent} initial={true} title="Agent" />

                  {/*--- Rightbtn Screen ---*/}
                  <Scene key="Infor" component={InforScreen} title="Information" hideTabBar={true} />
                  <Scene key="profile" component={ProfilePage} title="Personal Profile" hideTabBar={true} />
                </Scene>


                {/************ Departure ************/}
                <Scene
                  tabBarLabel="Departure Info"
                  renderRightButton={Rightbtn}
                  icon={({ focused }) => (
                    <IconMAT name='flight-takeoff' size={25} style={{ color: focused ? '#de2d30' : '#d6cccc' }} />
                  )}
                >
                  <Scene key="Departure" component={DepartureInfor} initial={true} title="Departure Information" />

                  {/*--- Rightbtn Screen ---*/}
                  <Scene key="Infor" component={InforScreen} title="Information" hideTabBar={true} />
                  <Scene key="profile" component={ProfilePage} title="Personal Profile" hideTabBar={true} />
                </Scene>


              </Scene>
            </Scene>
          </Lightbox>
        </Router>
      </Container>

    );
  }
}
export default App;

