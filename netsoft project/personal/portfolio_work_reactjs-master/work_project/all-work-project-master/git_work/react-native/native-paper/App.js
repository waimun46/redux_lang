/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, Button} from 'react-native';

import {
  createSwitchNavigator,
  createAppContainer,
  createBottomTabNavigator
} from 'react-navigation';

import Layout from './src'


const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});


class App extends Component{
  render() {
    return (
      <AppContainer/>
    );
  }
}
export default App;


class LayoutScreen extends Component{
  render() {
    return (
      <Layout/>
    );
  }
}
/*
class WelcomeScreen extends Component{

  render() {
    return (
      <View>
          <Text>WelcomeScxcxcreen</Text>
          <Button title='go footer' onPress={() => this.props.navigation.navigate('Footer')} />
      </View>
      
    );
  }
}
class FooterScreen extends Component{
  render() {
    return (
      <View>
          <Text>Footer</Text>
      </View>
    );
  }
}
class Game extends Component{
  render() {
    return (
      <View>
          <Text>Game</Text>
      </View>
    );
  }
}
class Settings extends Component{
  render() {
    return (
      <View>
          <Text>Settings</Text>
      </View>
    );
  }
}
class Profile extends Component{
  render() {
    return (
      <View>
          <Text>Profile</Text>
      </View>
    );
  }
}

const FooterTabNavigator = createBottomTabNavigator({
  Game,
  Settings,
  Profile
})
*/
const AppSwitchNavigator = createSwitchNavigator({
  Layout: { screen: LayoutScreen },
  //Welcome: {screen: FooterTabNavigator},
 // Footer: { screen : FooterTabNavigator}
});

const AppContainer = createAppContainer(AppSwitchNavigator);


