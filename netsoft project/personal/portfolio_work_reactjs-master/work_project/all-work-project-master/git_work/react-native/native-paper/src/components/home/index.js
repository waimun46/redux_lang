import React, {Component} from 'react';
import {Platform, StyleSheet, View, Text, Button} from 'react-native';
import Detail from '../details'

import {
    createSwitchNavigator,
    createAppContainer,
    createBottomTabNavigator,
    createStackNavigator
  } from 'react-navigation';

class Home extends Component{

  render() {
    return (
      <AppContainer/>
      
    );
  }
}

export default Home;


class HomeScreen extends Component{

    render() {
      return (
        <View>
          <Text>Hoasasame</Text>
          <Button
            title="Go to Details"
            onPress={() => this.props.navigation.navigate('Next')}
          />
        </View>
        
      );
    }
  }

class DetailsScreen extends Component {
    render() {
      return (
       <Detail/>
      );
    }
  }


const RootStack = createSwitchNavigator(
    {
      Home: HomeScreen,
      Next: DetailsScreen
    }
   
  );

const AppContainer = createAppContainer(RootStack);

