import React, { Component } from 'react';
import BetRecordList from './components/BetRecordList';
import SlideOutPanel from '../bet/components/SlideOutPanel';


class BetRecord extends Component {

  render() {
    return (
      <section id="bet-record">
        <BetRecordList/>
        <SlideOutPanel />
      </section>
    );
  }
};

export default BetRecord;
