import React, { Component } from 'react';
import { NoticeBar, WhiteSpace } from 'antd-mobile';
import Link from 'umi/link';
import User from './infor/user';
import Card from './infor/card';
import Countdown from './infor/countdown';

class TopInfor extends Component {

  render() {
    return (
      <section className="TopInfor">
        <div className="warp">
          <User/>
        </div>
        <div className="warp">
          <Card/>
        </div>
        <div className="warp">
          <Countdown/>
        </div>
        <div className="clearfix"></div>
      </section>
    );
  }
};

export default TopInfor;
