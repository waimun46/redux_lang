import React, { Component } from 'react';
import lang from '@/lang';
import styles from './index.less';

class CardTypeList extends Component {

  render() {
    return (
        <div className={styles.CardTypeList}>
          <div className={styles.card_content}>
            <ul>
              <li>
                <label>{lang.bullbull}</label>
                <label className={styles.game_card}>
                  <div>
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                  <div className="clearfix"></div>
                  </div>
                </label>
                <label>{lang.bullbull_content}</label>
                <div className="clearfix"></div>
              </li>

              <li>
                <label>{lang.bull_9}</label>
                <label className={styles.game_card}>
                  <div>
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                  <div className="clearfix"></div>
                  </div>
                </label>
                <label>{lang.bull_9_content}</label>
                <div className="clearfix"></div>
              </li>

              <li>
                <label>{lang.bull_8}</label>
                <label className={styles.game_card}>
                  <div>
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                  <div className="clearfix"></div>
                  </div>
                </label>
              <label>{lang.bull_8_content}</label>
                <div className="clearfix"></div>
              </li>

              <li>
                <label>{lang.bull_7}</label>
                <label className={styles.game_card}>
                  <div>
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                  <div className="clearfix"></div>
                  </div>
                </label>
              <label>{lang.bull_7_content}</label>
                <div className="clearfix"></div>
              </li>

              <li>
                <label>{lang.bull_6}</label>
                <label className={styles.game_card}>
                  <div>
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                  <div className="clearfix"></div>
                  </div>
                </label>
                <label>{lang.bull_6_content}</label>
                <div className="clearfix"></div>
              </li>

              <li>
                <label>{lang.bull_5}</label>
                <label className={styles.game_card}>
                  <div>
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                  <div className="clearfix"></div>
                  </div>
                </label>
              <label>{lang.bull_5_content}</label>
                <div className="clearfix"></div>
              </li>

              <li>
                <label>{lang.bull_4}</label>
                <label className={styles.game_card}>
                  <div>
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                  <div className="clearfix"></div>
                  </div>
                </label>
              <label>{lang.bull_4_content}</label>
                <div className="clearfix"></div>
              </li>

              <li>
                <label>{lang.bull_3}</label>
                <label className={styles.game_card}>
                  <div>
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                  <div className="clearfix"></div>
                  </div>
                </label>
              <label>{lang.bull_3_content}</label>
                <div className="clearfix"></div>
              </li>

              <li>
                <label>{lang.bull_2}</label>
                <label className={styles.game_card}>
                  <div>
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                  <div className="clearfix"></div>
                  </div>
                </label>
              <label>{lang.bull_2_content}</label>
                <div className="clearfix"></div>
              </li>

              <li>
                <label>{lang.bull_1}</label>
                <label className={styles.game_card}>
                  <div>
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                  <div className="clearfix"></div>
                  </div>
                </label>
                <label>{lang.bull_1_content}</label>
                <div className="clearfix"></div>
              </li>
              <li>
                <label>{lang.bull_null}</label>
                <label className={styles.game_card}>
                  <div>
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                  <div className="clearfix"></div>
                  </div>
                </label>
                <label>{lang.bull_null_content}</label>
                <div className="clearfix"></div>
              </li>
            </ul>
          </div>
        </div>
    );
  }
};

export default CardTypeList;
