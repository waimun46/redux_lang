import React, { Component } from 'react';
import Link from 'umi/link';
import Menuphone from '../bet/components/MenuPhone';
import lang from '@/lang';
import styles from './index.less';

class Homepage extends Component {

  render() {
    return (
      <div id="niuniu"  className={styles.Homepage}>
        <div className={styles.home_warp}>

          <Link to="/bet">
            <div className={styles.warp}>
              <div className={styles.bg}>
                <div className={styles.title}>
                  <h1>{lang.primary}</h1>
                  <p>{lang.primary_subtitle}</p>
                </div>
              </div>
            </div>
          </Link>

          <Link to="/bet">
            <div className={styles.warp}>
              <div className={styles.bg}>
                <div className={styles.title}>
                  <h1>{lang.standard}</h1>
                  <p>{lang.standard_subtitle}</p>
                </div>
              </div>
            </div>
          </Link>

          <Link to="/bet">
            <div className={styles.warp}>
              <div className={styles.bg}>
                <div className={styles.title}>
                  <h1>{lang.advanced}</h1>
                  <p>{lang.advanced_subtitle}</p>
                </div>
              </div>
            </div>
          </Link>

          <Menuphone/>

        <div className="clearfix"></div>
        </div>


      </div>
    );
  }
};

export default Homepage;
