import { Icon } from 'antd';

export default Icon.createFromIconfontCN({
    scriptUrl: '//at.alicdn.com/t/font_1015847_9291vi7k7q.js', // 在 iconfont.cn 上生成
});
