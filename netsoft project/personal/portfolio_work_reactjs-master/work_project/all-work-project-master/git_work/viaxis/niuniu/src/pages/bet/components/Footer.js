import { connect } from 'dva';
import React, { PureComponent } from 'react';
import router from 'umi/router';
import classNames from "classnames";
import { Row, Col, Button, Icon, Spin, message, Modal, Form, Input, InputNumber } from 'antd';
import Ncon from "@/components/NiuIcon";
import styles from "../index.less";
import Wallet from '@/services/wallet';
import lang from '@/lang';

class Footer extends PureComponent {

    state = {
        showBalance: false
    }

    showBalance = (e) => {
        this.setState({ showBalance: true });
    }

    wagerBtnChange = (amt) => {
        this.props.dispatch({ type: "global/changeWageButton", payload: amt });
    }

    render() {
        const wagerButtons = [0.1, 1, 10, 100];
        const { showBalance } = this.state;
        const { selectedWager, eosIdentity } = this.props;
        return (
            <React.Fragment>
                <Row gutter={2} className={styles.footer}>
                    {this.renderBalanceButton()}
                    <Col className={classNames("gutter-row", styles.warger_buttons)} span={12}>
                        {wagerButtons.map(i => <Button key={i} type="primary" className={i == selectedWager ? "active" : ""} size="large" onClick={() => this.wagerBtnChange(i)}>{i}&nbsp;&nbsp;EOS</Button>)}
                    </Col>
                    <Col className="gutter-row" span={6}>
                        <Button className={styles.buttons_bet} type="primary" size="large" style={{ "width": "70%" }} onClick={() => router.push('/bet/history')}>{lang.betHistory}</Button>
                    </Col>
                    {/*
                    <Col className="gutter-row" span={6}>
                        <Button type="primary" size="large" style={{ "width": "70%" }} onClick={this.showBalance}>{lang.betHistory}</Button>
                    </Col>
                    {this.modal()}*/}
                </Row>
            </React.Fragment>
        );
    }

    renderBalanceButton() {
        const { eosIdentity } = this.props;
        if (eosIdentity == null) {
            return (
                <Col className="gutter-row" span={6}>
                    <Button type="primary" size="large" style={{ "width": "100%" }} onClick={() => Wallet.login()} > {lang.login} </Button>
                </Col>
            );
        } else {
            const { account } = this.props;
            return (
                <React.Fragment>
                    <Col className="gutter-row" span={5}>
                        <Button className={styles.btn_balance} type="primary" size="large" style={{ "width": "100%" }} onClick={this.showBalance}>
                            {account && account.balance.toFixed(4)}
                            <Ncon type="icon-Icon_EOS" />
                        </Button>
                    </Col>
                    <Col className="gutter-row" span={1}>
                        <Button type="primary" shape="circle" icon="plus" size="large" />
                    </Col>
                    {this.modal()}
                </React.Fragment>
            );
        }
    }


    handleDeposit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (err) return;

            Wallet.transfer(configs.contract.code, `${parseFloat(values.amount).toFixed(4)} EOS`, "Deposit").then(it => {
                this.setState({ showBalance: false });
                message.success(lang.depositSuccess);
            });
        });
    }
    handleWithdraw = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (err) return;

            Wallet.withdraw(`${parseFloat(values.amount).toFixed(4)} EOS`).then(it => {
                this.setState({ showBalance: false });
                message.success(lang.withdrawSuccess);
            });
        });
    }

    modal() {
        const { balance, dispatch } = this.props;
        if (balance == null)
            dispatch({ type: "global/getEOSBalance" });
        const loading = balance == null ? true : false;
        const { getFieldDecorator } = this.props.form;
        const formItemLayout = {
            labelCol: {
                xs: { span: 24 },
                sm: { span: 6 },
            },
            wrapperCol: {
                xs: { span: 24 },
                sm: { span: 16 },
            },
        };
        return (
            <Modal
                title={lang.depositAndWithdraw}
                visible={this.state.showBalance}
                onCancel={() => this.setState({ showBalance: false })}
                footer={null}
                className={styles.modal_form}
            >
                <Spin indicator={<Icon type="loading" style={{ fontSize: 24 }} span />} spinning={loading}>
                    <Form className={styles.widthdraw_form}>
                        <Form.Item {...formItemLayout} label={lang.balance}>
                            <p className={styles.balance}>{balance}</p>
                        </Form.Item>
                        <Form.Item {...formItemLayout} label={lang.amount}>
                            {getFieldDecorator('amount', {
                                rules: [{
                                    required: true, message: lang.amountRequired,
                                }],
                            })(
                                <Input addonAfter={<Ncon type='icon-Icon_EOS' />} />
                            )}
                        </Form.Item>
                        <Form.Item>
                            <Row type="flex" justify="center">
                                <Col span={8} offset={4}><Button type="primary" className="fn-width-100p" onClick={this.handleWithdraw}>{lang.withdraw}</Button></Col>
                                <Col span={1} />
                                <Col span={8}><Button type="primary" className="fn-width-100p" onClick={this.handleDeposit}>{lang.deposit}</Button></Col>
                            </Row>
                        </Form.Item>
                    </Form>
                </Spin>
            </Modal>
        );
    }
};

export default connect(state => ({
    lang: state.global.lang,
    selectedWager: state.global.selectedWageButton,
    account: state.global.account,
    eosIdentity: state.global.eosIdentity,
    balance: state.global.eosBalance
}))(Form.create()(Footer));
