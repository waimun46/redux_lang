import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import styles from '../Hand/index.less';
import classnames from 'classnames';
import * as Utils from '@/utils/common';

const Suits = {
    "♦": "diamond",
    "♣": "club",
    "♥": "heart",
    "♠": "spade"
};

class Card extends PureComponent {

    state = { rotate: false }

    /**
     * invoked as soon as the props are updated before another render is called.
     * @param  {[type]} newProps [description]
     * @return {[type]}          [description]
     */
    componentWillReceiveProps(newProps) {
        if (Utils.isEmptyOrNull(newProps.value))
            this.setState({ rotate: false });
    }


    open() {
        this.setState({ rotate: true });
    }


    render() {
        const { rotate } = this.state;
        return (
            <span className={styles.poker_card}>
                <span className="back" style={rotate ? { 'transform': "rotateY(180deg)" } : {}}></span>
                <span className={this.parseValue()} style={rotate ? { 'transform': "rotateY(0deg)" } : {}}></span>
            </span>
        );
    }

    parseValue() {
        const { value } = this.props;
        if (Utils.isEmptyOrNull(value)) return 'back';
        let suit = Suits[value[0]];
        let val = value.slice(1).toUpperCase();
        return `${suit}_${val}`;
    }
};
Card.propTypes = {
    value: PropTypes.string
};

export default Card;
