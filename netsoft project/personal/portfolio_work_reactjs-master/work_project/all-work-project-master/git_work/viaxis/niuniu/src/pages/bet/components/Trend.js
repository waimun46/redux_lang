import { connect } from 'dva';
import React, { PureComponent } from 'react';
import { Spin, Icon } from 'antd';
import classnames from 'classnames';
import lang from '@/lang';
import Ncon from '@/components/NiuIcon';
import styles from '../index.less';

const antIcon = <Icon type="loading" style={{ fontSize: 24 }} spin />;

class Trend extends PureComponent {

    state = { slideClass: false }

    slide = (e) => {
        const { slideClass } = this.state;
        this.setState({ slideClass: !slideClass });

        if (!slideClass)
            this.props.dispatch({ type: "game/getHistory" });
    }

    render() {
        const { history, loading } = this.props;
        const { slideClass } = this.state;
        return (
            <div className={classnames(styles.trend, slideClass && "showSlideOut" || "")}>
                <h5 onClick={this.slide}>{slideClass ? <Icon type="left" /> : lang.trend}</h5>
                <div className={styles.slide_popup}>
                    <h3>{lang.trend}</h3>
                    <div className="content">
                        <label>
                            <span><Ncon type='icon-Hashtag_Icon' /></span>
                            <span><Ncon type='icon-Tiles_Icon' /></span>
                            <span><Ncon type='icon-Clovers_Icon' /></span>
                            <span><Ncon type='icon-Hearts_Icon' /></span>
                            <span><Ncon type='icon-Pikes_Icon' /></span>
                        </label>
                        {loading ? <Spin indicator={antIcon} tips="Loading..." />
                            :
                            <ul>
                                {history && history.Data && history.Data.map(it =>
                                    <li key={it.id}>
                                        <span>{it.id}</span>
                                        {JSON.parse(it.heaps).map((x, i) => <span key={i} style={{ color: x.odds > 0 ? '#00B727' : '#EF5261' }}>{x.odds > 0 ? lang.win : lang.loss}</span>)}
                                    </li>
                                )}

                                {(history && history.Data && history.Data.length == 0) && (<li>None Data</li>)}
                            </ul>
                        }
                    </div>
                </div>
            </div >

        );
    }
};

export default connect(state => ({
    lang: state.global.lang,
    history: state.game.history,
    loading: state.loading.effects["game/getHistory"]
}))(Trend);
