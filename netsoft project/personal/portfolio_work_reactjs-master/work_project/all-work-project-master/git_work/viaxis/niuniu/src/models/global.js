import { routerRedux } from 'dva/router';
import WebSocket from '@/websocket';
import Wallet from '../services/wallet';
import lang from '@/lang';

const ws = new WebSocket();

export default {
    namespace: 'global',
    state: {
        lang: "zh",
        account: null,
        eosIdentity: null,
        eosAccount: null,
        eosBalance: null,
        selectedWageButton: 0.1
    },
    reducers: {
        // login(state, { payload }) {
        //     return {
        //         ...state,
        //         eosIdentity: payload,
        //     };
        // },
        saveData(state, { payload }) {
            return { ...state, ...payload };
        },
        changeWageButton(state, { payload }) {
            return {
                ...state,
                selectedWageButton: payload,
            };
        }
    },
    effects: {
        *login({ payload }, { call, put, get }) {
            const { account, referral } = payload;
            yield put({ type: "saveData", payload: { eosIdentity: account } });
            const data = yield ws.call('account.login', { name: account.name, referral });
            yield put({ type: "saveData", payload: { account: data } });
        },
        *logout({ payload }, { call, put, get }) {
            yield put({ type: "saveData", payload: { eosIdentity: null } });
            yield put({ type: "saveData", payload: { account: null } });
        },
        *getInfo(action, { call, put, get }) {
            var ws = new WebSocket();
            let data = yield ws.call("account.info");
            yield put({ type: "saveData", payload: { account: data } });
        },
        *getEOSBalance({ payload }, { call, put, get, select }) {
            const eosIdentity = yield select(state => state.global.eosIdentity);
            let balances = yield Wallet.getBalance(eosIdentity.name);
            if (balances && balances.length > 1)
                balances = balances.filter(it => it.endsWith("EOS"));

            const balance = balances && balances.length > 0 ? balances[0] : "0.0000 EOS";
            yield put({ type: "saveData", payload: { eosBalance: balance } });
        },
        *getEOSAccount({ payload }, { call, put, get, select }) {
            const eosIdentity = yield select(state => state.global.eosIdentity);
            const account = yield Wallet.getAccount(eosIdentity.name);
            console.log(account);
            return yield put({ type: "saveData", payload: { eosAccount: account } });
        },
        *changeLang({ payload }, { call, put, get }) {
            lang.setLanguage(payload);
            localStorage.lang = payload;
            return yield put({ type: "saveData", payload: { lang: payload } });
        }
    },
    subscriptions: {
        setup({ dispatch, history }) {
            return history.listen(({ pathname, query }) => {
                if (localStorage.lang) {
                    lang.setLanguage(localStorage.lang);
                }
                Wallet.login();
            });
        },
    },

};
