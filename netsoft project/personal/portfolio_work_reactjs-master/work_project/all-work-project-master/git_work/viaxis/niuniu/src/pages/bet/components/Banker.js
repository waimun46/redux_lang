import { connect } from 'dva';
import React, { Component } from 'react';
import Link from 'umi/link';
import { Icon, Modal, Button } from 'antd';
import logo from '@/assets/images/svg/Game_Logo.svg';
import user from '@/assets/images/svg/icon/Host_Icon.svg';
import FontIcon from '@/components/NiuIcon';
import classNames from "classnames";
import lang from '@/lang';

import styles from "../index.less";

class Banker extends Component {

    state = {
      visible: false,
      visiblejackpot: false,
     }

    showModal = () => {
        this.setState({
            visible: true,
        });
    }

    handleOk = (e) => {
        console.log(e);
        this.setState({
            visible: false,
        });
    }

    handleCancel = (e) => {
        this.setState({
            visible: false,
        });
    }
    showModaljackpot = () => {
     this.setState({
       visiblejackpot: true,
     });
    }

    handleOkjackpop = (e) => {
     console.log(e);
     this.setState({
       visiblejackpot: false,
     });
    }

    handleCanceljackpop = (e) => {
     console.log(e);
     this.setState({
       visiblejackpot: false,
     });
    }

    render() {
        const { banker } = this.props;
        return (
            <div className={styles.user}>
                <div className={styles.user_card}>
                    <div className={styles.avatar}>
                        <img src={logo} alt="logo" />
                    </div>
                    <div className={styles.details}>
                        <div className={styles.name}>
                            {banker && banker.name}
                        </div>
                        <div className={styles.number}>
                            <p>{banker && banker.balance}</p>
                            <FontIcon type="icon-Icon_EOS" />
                        </div>
                        <div className={styles.user_up_btn} onClick={this.showModal}>
                            <FontIcon type="icon-Challenge_Icon" />
                            <p>{lang.host}</p>
                        </div>
                    </div>
                </div>

                <div className={styles.jackport} onClick={this.showModaljackpot}>
                    <div className={styles.warp_content}>
                        <FontIcon type="icon-EOS_Token" />
                    </div>
                    <div className={styles.warp_content}>
                        <p>JACKPORT</p>
                    </div>
                    <div className={styles.warp_content}>
                        <p>55555555</p>
                    </div>
                    <div className={styles.warp_content} style={{ fontWeight: 'bold' }}>
                        <p>{lang.million}</p>
                    </div>
                    <div className="clearfix"></div>

                </div>

                <Modal
                    title={lang.host_list}
                    visible={this.state.visible}
                    onOk={this.handleOk}
                    onCancel={this.handleCancel}
                    footer={null}
                    className={styles.shangzhuang}
                >
                    <div className={styles.top_content}>
                        <div className={styles.left_side}>
                            <div className={styles.icon_user}>
                                <img src={user} alt="user" />
                            </div>
                            <div className={styles.user_title}>
                                <p>Johnnybarnes</p>
                                <p className={styles.diamond_sty}>
                                    6,666,666,666,666
                                    <FontIcon type="icon-Icon_EOS" />
                                </p>
                                <div className="clearfix"></div>
                            </div>
                        </div>
                        <div className={styles.right_side}>
                            <p>{lang.host_condition} ：</p>
                            <p>10.00000</p>
                            <div className="clearfix"></div>
                        </div>
                        <div className="clearfix"></div>
                    </div>

                    <div className={styles.bottom_content}>
                        <ul>
                            <li>
                                <p>{lang.account_host}</p>
                                <p>{lang.capital}</p>
                                <div className="clearfix"></div>
                            </li>
                            <li>
                                <p>Markez1919</p>
                                <p>10.154875</p>
                                <div className="clearfix"></div>
                            </li>
                            <li>
                                <p>Markez1919</p>
                                <p>10.154875</p>
                                <div className="clearfix"></div>
                            </li>
                        </ul>
                        <div className={styles.btn_shangzhuang}>
                            <Button type="primary">{lang.host}</Button>
                        </div>
                    </div>
                </Modal>

                {/* jackpot popup */}
                <Modal
                  title={lang.jackpot}
                  visible={this.state.visiblejackpot}
                  footer={null}
                  closable={null}
                  onOk={this.handleOkjackpop}
                  onCancel={this.handleCanceljackpop}
                  className={styles.jackpot_popup}
                >
                  <div className={styles.coin_icon}></div>
                  <div className={styles.jackpot_content}>
                    <p>88,0000 EOS</p>
                  </div>
                  <div className={styles.jackpot_btn}>
                    <Button type="primary" onClick={this.handleCanceljackpop}>{lang.oK}</Button>
                  </div>
                </Modal>
            </div>
        );
    }
};

export default connect(state => ({
    lang: state.global.lang,
    banker: state.game.banker,
}))(Banker);
