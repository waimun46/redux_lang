
import { connect } from 'dva';
import React, { PureComponent } from 'react';

import Footer from './components/Footer';
import SlideOutPanel from './components/SlideOutPanel';
import Banker from './components/Banker';
import Countdown from './components/CountDown';
import Hand from './components/Hand';
import BetZone from './components/BetZone';
import Loading from '../loading';
import stylestop from './index.less';
import styles from './components/Hand/index.less';
import drawSound from '@/assets/sounds/effect/draw.mp3';

class Bet extends PureComponent {

    currentGameId = null

    /**
     * executed before rendering
     * @return {[type]} [description]
     */
    componentWillMount() {
        this.updateGameId(this.props);
    }

    // componentDidMount() {
    //     Popup(false);
    // }

    /**
     * invoked as soon as the props are updated before another render is called.
     * @param  {[type]} newProps [description]
     * @return {[type]}          [description]
     */
    componentWillReceiveProps(newProps) {
        this.updateGameId(newProps);
    }

    updateGameId(props) {
        if (!props.actives || !props.actives[0]) return;
        var loadSummary = this.currentGameId == null;

        const game = props.actives[0];
        this.currentGameId = game.id;
        if (loadSummary)
            this.props.dispatch({ type: "game/getSummary", payload: this.currentGameId });
    }

    render() {
        let game = this.props.actives[0];
        let { open, bets, bets_mine, bets_total } = this.props;
        const heaps = open && JSON.parse(open.heaps) || null;
        const open_time = game && game.open_time ? game.open_time * 1000 : new Date().getTime();
        bets = game && bets[game.id] || [];
        bets_mine = game && bets_mine[game.id] || {};
        bets_total = game && bets_total[game.id] || {};

        return (
            <div id="niuniu">
                <section className={stylestop.TopInfor} id={styles.TopInfor}>
                    <div className={stylestop.warp}>
                        <Banker />
                    </div>
                    <div className={stylestop.warp}>
                   <Hand value={open && open.banker || ''} />
                            {/*  <Hand value="♣8,♥2,♠8,♥K,♠3" />*/}
                    </div>
                    <div className={stylestop.warp}>
                        <Countdown game={game} target={open_time} />
                    </div>
                    <div className="clearfix"></div>
                </section>
                <section className={stylestop.MiddleInfor}>
                    <div>
                        {[...Array(4).keys()].map(it => <BetZone key={it} heap={it}
                            game_id={this.currentGameId}
                            hand={heaps && heaps[it]}
                            bets={bets.filter(x => x.heap == it) || []}
                            bets_mine={bets_mine[it] || 0}
                            bets_total={bets_total[it] || 0} />)}
                        <div className="clearfix"></div>
                    </div>
                </section>
                <Footer />
                <SlideOutPanel />
                {heaps && heaps.length &&
                    < audio autoPlay={true}>
                        <source src={drawSound} type="audio/mpeg" >
                        </source>
                    </audio>
                }
            </div>
        );
    }
};

export default connect(state => ({
    actives: state.game.actives,
    open: state.game.open,
    bets: state.game.bets,
    bets_mine: state.game.bets_mine,
    bets_total: state.game.bets_total,
}))(Bet);
