import React, { Component } from 'react';
import $ from 'jquery';

class Countdown extends Component {

  componentDidMount(){

    // Set the date we're counting down to
    let countDownDate = new Date("Jan 5, 2021 15:37:25").getTime();

      // Update the count down every 1 second
      let x = setInterval(function() {

        // Get todays date and time
        let now = new Date().getTime();

        // Find the distance between now and the count down date
        let distance = countDownDate - now;

        // Time calculations for days, hours, minutes and seconds

        let minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        let seconds = Math.floor((distance % (1000 * 60)) / 1000);

        // Output the result in an element with id="demo"

        document.getElementById("time").innerHTML =  minutes + ":" + seconds ;

        // If the count down is over, write some text
        if (distance < 0) {
          clearInterval(x);

          document.getElementById("time").innerHTML = "EXPIRED";
        };

      }, 1000);

  }

  render() {
    return (
      <div className="Countdown">
        <div className="time">
          <span>下局开始</span>
          <p id="time"></p>
        </div>
      </div>
    );
  }
};

export default Countdown;
