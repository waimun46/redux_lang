import React, { Component } from 'react';
import lang from '@/lang';
import styles from '../../index.less';

class OddsLoseTable extends Component {

  render() {
    return (
      <div className={styles.oddstable}>
        <div className={styles.table_content}>
            <label>{lang.banker_lose}</label>
            <div className={styles.list_table}>
              <ul>
                <li>
                  <span className={styles.list_title}>{lang.card_type}</span>
                  <span className={styles.list_title}>{lang.double}</span>
                  <span className={styles.list_title}>{lang.equal}</span>
                  <div className="clearfix"></div>
                </li>
                <li>
                  <span>{lang.five_gong}</span>
                  <span className="red-color">1 : 4.75</span>
                  <span className="red-color">1 : 1</span>
                  <div className="clearfix"></div>
                </li>
                <li>
                  <span>{lang.niuniu}</span>
                  <span className="red-color">1 : 2.85</span>
                  <span className="red-color">1 : 1</span>
                  <div className="clearfix"></div>
                </li>
                <li>
                  <span>{lang.niu9_niu7}</span>
                  <span className="red-color">1 : 1.90</span>
                  <span className="red-color">1 : 1</span>
                  <div className="clearfix"></div>
                </li>
                <li>
                  <span>{lang.normal}</span>
                  <span className="red-color">1 : 0.95</span>
                  <span className="red-color">1 : 1</span>
                  <div className="clearfix"></div>
                </li>
              </ul>
            </div>
          </div>
        </div>
    );
  }
};

export default OddsLoseTable;
