import React, { Component } from 'react';
import rank from '../../../../assets/images/icon/Rank_Gold.png';
import $ from 'jquery'

class PaihangbangPanel extends Component {

  componentDidMount(){
    this.$slideOut = $('#slideOut');

    this.$slideOut.find('.slideOutTab').on('click', function() {
      $("#slideOut").toggleClass('showSlideOut');
    });

  }

  render() {
    return (
      <div id="PaihangbangPanel">

      <div id="slideOut">
        <div className="slideOutTab">
          <div className="right-title">
            <p>排行榜</p>
          </div>
        </div>
        <div className="modal-content">
          <div className="modal-body">
            <h3>排行榜</h3>
            <div className="list-content">
              <label>
                <p>Rank</p>
                <p>Account</p>
                <p>Total EOS</p>
                <div className="clearfix"></div>
              </label>
              <ul>
                <li>
                  <span><img src={rank} alt="rank"/></span>
                  <span>gmg4gggkd</span>
                  <span>80.0000 EOS</span>
                  <div className="clearfix"></div>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>

      </div>
    );
  }
};

export default PaihangbangPanel;
