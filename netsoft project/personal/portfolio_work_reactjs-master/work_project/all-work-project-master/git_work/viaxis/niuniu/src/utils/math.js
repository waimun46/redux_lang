// import uniq from "lodash.uniq";

/**
 * 阶乘Factorial定义为：
 *      ┌ 1        n=0       
 *   N!=│    
 *      └ n(n-1)!  n>0
 * @param  {[type]} number [description]
 * @return {[type]}        [description]
 */
export function factorial(number) {
    if (number != 0) {
        return number * factorial(number - 1);
    } else if (number == 0) {
        return 1;
    }
    return number;
}

/**
 * 组合,從 n 個相異物中不重複的取出 m 個之組合數。
 * http://latex.codecogs.com/gif.latex?_nC_m={n%20\choose%20m}=\frac{_nP_m}{m!}=\frac{n!}{m!(n-m)!}
 * @param  {[type]} n 总数量
 * @param  {[type]} m [description]
 * @return {[type]}   取出的数量
 */
export function combinatorics(n, m) {
    if (n < m) return 0;
    return factorial(n) / (factorial(m) * factorial(n - m));
}


// /**
//  * 将多个数组进行合并排列
//  * @param  {...[type]} arrays [description]
//  * @return {[type]}           [description]
//  */
// export function permutation(duplicate, ...arrays) {
//     if (arrays.length < 1) return arrays;
//     var data = arrays[arrays.length - 1];
//     for (let i = arrays.length - 2; i >= 0; i--) {
//         let arr = arrays[i];
//         data = mergeArray(arrays[i], data);
//     }
//     if (!duplicate)
//         data = data.filter(it => uniq(it).length == it.length);
//     return data;
// }

/**
 * 将两个数组进行排列合并
 * @param  {[type]} n      可以是数组也可以是单个元素
 * @param  {[type]} subArr 要合并的数组
 * @return {[type]}        [description]
 */
export function mergeArray(n, subArr) {
    let data = [];
    if (Array.isArray(n)) {
        for (let i = 0; i < n.length; i++)
            data = [...data, ...mergeArray(n[i], subArr)];
    } else {

        for (let i = 0; i < subArr.length; i++) {
            if (Array.isArray(subArr[i]))
                data.push([n, ...subArr[i]]);
            else
                data.push([n, subArr[i]]);
        }
    }

    return data;
}

/**
 * K-combinations
 * 
 * Get k-sized combinations of elements in a set.
 * 
 * Usage:
 *   k_combinations(set, k)
 * 
 * Parameters:
 *   set: Array of objects of any type. They are treated as unique.
 *   k: size of combinations to search for.
 * 
 * Return:
 *   Array of found combinations, size of a combination is k.
 * 
 * Examples:
 * 
 *   k_combinations([1, 2, 3], 1)
 *   -> [[1], [2], [3]]
 * 
 *   k_combinations([1, 2, 3], 2)
 *   -> [[1,2], [1,3], [2, 3]
 * 
 *   k_combinations([1, 2, 3], 3)
 *   -> [[1, 2, 3]]
 * 
 *   k_combinations([1, 2, 3], 4)
 *   -> []
 * 
 *   k_combinations([1, 2, 3], 0)
 *   -> []
 * 
 *   k_combinations([1, 2, 3], -1)
 *   -> []
 * 
 *   k_combinations([], 0)
 *   -> []
 */
export function k_combinations(set, k) {
    var i, j, combs, head, tailcombs;

    // There is no way to take e.g. sets of 5 elements from
    // a set of 4.
    if (k > set.length || k <= 0) {
        return [];
    }

    // K-sized set has only one K-sized subset.
    if (k == set.length) {
        return [set];
    }

    // There is N 1-sized subsets in a N-sized set.
    if (k == 1) {
        combs = [];
        for (i = 0; i < set.length; i++) {
            combs.push([set[i]]);
        }
        return combs;
    }

    // Assert {1 < k < set.length}

    // Algorithm description:
    // To get k-combinations of a set, we want to join each element
    // with all (k-1)-combinations of the other elements. The set of
    // these k-sized sets would be the desired result. However, as we
    // represent sets with lists, we need to take duplicates into
    // account. To avoid producing duplicates and also unnecessary
    // computing, we use the following approach: each element i
    // divides the list into three: the preceding elements, the
    // current element i, and the subsequent elements. For the first
    // element, the list of preceding elements is empty. For element i,
    // we compute the (k-1)-computations of the subsequent elements,
    // join each with the element i, and store the joined to the set of
    // computed k-combinations. We do not need to take the preceding
    // elements into account, because they have already been the i:th
    // element so they are already computed and stored. When the length
    // of the subsequent list drops below (k-1), we cannot find any
    // (k-1)-combs, hence the upper limit for the iteration:
    combs = [];
    for (i = 0; i < set.length - k + 1; i++) {
        // head is a list that includes only our current element.
        head = set.slice(i, i + 1);
        // We take smaller combinations from the subsequent elements
        tailcombs = k_combinations(set.slice(i + 1), k - 1);
        // For each (k-1)-combination we join it with the current
        // and store it to the set of k-combinations.
        for (j = 0; j < tailcombs.length; j++) {
            combs.push(head.concat(tailcombs[j]));
        }
    }
    return combs;
}


/**
 * Combinations
 * 
 * Get all possible combinations of elements in a set.
 * 
 * Usage:
 *   combinations(set)
 * 
 * Examples:
 * 
 *   combinations([1, 2, 3])
 *   -> [[1],[2],[3],[1,2],[1,3],[2,3],[1,2,3]]
 * 
 *   combinations([1])
 *   -> [[1]]
 */
export function combinations(set) {
    var k, i, combs, k_combs;
    combs = [];

    // Calculate all non-empty k-combinations
    for (k = 1; k <= set.length; k++) {
        k_combs = k_combinations(set, k);
        for (i = 0; i < k_combs.length; i++) {
            combs.push(k_combs[i]);
        }
    }
    return combs;
}