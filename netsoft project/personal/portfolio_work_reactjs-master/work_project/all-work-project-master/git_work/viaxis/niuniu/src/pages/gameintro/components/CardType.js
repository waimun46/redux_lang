import React, { Component } from 'react';
import CardTypeList from './CardTypeList';
import lang from '@/lang';
import styles from '../index.less';

class CardType extends Component {

  render() {
    return (
        <section className={styles.CardType}>
          <div className={styles.content}>
            <header>
                <h1>{lang.types}</h1>
                <p>{lang.types_subtitle}</p>
            </header>
            <CardTypeList/>
          </div>
        </section>
    );
  }
};

export default CardType;
