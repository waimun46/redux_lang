export default {
    define: {
        // restful domain
        "configs.api": "http://10.211.55.3:5000",
        // websocket url
        "configs.ws": "ws://10.211.55.3:5000/ws",
        "configs.eos": {
            "endpoint": "http://127.0.0.1:8888",
            "chainId": "cf057bbfb72640471fd910bcb67639c22df9f92470936cddc1ade0e2f2e7dc4f"
        }
    },
    proxy: {
        "/api": {
            "target": "http://10.211.55.3:5000",
            "changeOrigin": true,
        }
    }
};