import React, { PureComponent } from 'react';
import Ncon from '@/components/NiuIcon';
import Random from '@/utils/random';
import styles from './index.less';
import $ from 'jquery';
import Sound from '@/utils/sound';
import coinSound from '@/assets/sounds/effect/money-drop.mp3';

export default class Coin extends PureComponent {
    state = { offset: null }

    constructor(props) {
        super(props);
        this.myRef = React.createRef();
    }

    /**
     * executed before rendering
     * @return {[type]} [description]
     */
    // componentWillMount() {
    //     this.setState({ offset: { top: Random.randomInt(0, 180), left: Random.randomInt(0, 250) } });
    // }

    componentDidMount() {
        const coins = $(`.${styles.coins}`);
        const width = coins.width() - 40;
        const height = coins.height() - 40;
        this.setState({ offset: { top: Random.randomInt(0, height), left: Random.randomInt(0, width) } });
        Sound.play(coinSound);
    }

    render() {
        const { offset } = this.state;
        return offset &&
            <React.Fragment>
                <span className="coin" style={offset} > <Ncon type="icon-EOS_Token" /></ span>
            </React.Fragment>
            ;
    }
}