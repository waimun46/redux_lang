import React, { Component } from 'react';
import {Icon, Tabs, Popconfirm, message, Button, Table, DatePicker, Input  } from 'antd';
import classNames from "classnames";
import styles from '../index.less';
import stylespop from '../../bet/index.less';
import lang from '@/lang';
const { RangePicker } = DatePicker;

function onChange(value, dateString) {
  console.log('Selected Time: ', value);
  console.log('Formatted Selected Time: ', dateString);
}

function onOk(value) {
  console.log('onOk: ', value);
}

const TabPane = Tabs.TabPane;

const formContent =  <form className={stylespop.search_popout}>
                      <div className={stylespop.form_input}>
                          <label>{lang.date}</label>
                          <RangePicker
                              showTime={{ format: 'HH:mm' }}
                              format="YYYY-MM-DD HH:mm"
                              placeholder={['Start Time', 'End Time']}
                              onChange={onChange}
                              onOk={onOk}
                          />
                          <div className="clearfix"></div>
                      </div>
                      <div className={stylespop.form_input}>
                          <label>{lang.account}</label>
                          <Input className={stylespop.input_popover} />
                          <div className="clearfix"></div>
                      </div>
                      <div className={stylespop.form_input}>
                          <label>{lang.bonus}</label>
                          <Input className={stylespop.input_popover}/>
                          <div className="clearfix"></div>
                      </div>
                      <div className={stylespop.Submit_btn}>
                          <Button type="primary">{lang.oK}</Button>
                      </div>
                    </form>
            ;


const columns = [{
  title: lang.date,
  dataIndex: 'date',
}, {
  title: lang.account,
  dataIndex: 'account',
},{
  title: lang.bonus,
  dataIndex: 'bonus',
}
];
const data = [{
  key: '1',
  date: '03-01-2019',
  account: 'dd555566',
  bonus: '0.1010 EOS'
}, {
  key: '2',
  date: '03-01-2019',
  account: 'dd555566',
  bonus: '0.1010 EOS'
},
];



class RecommenFormList extends Component {

  render() {
    return (
      <div className={classNames("recommen-form-list", styles.recommen_form_list)}>
        <Popconfirm  placement="bottomRight" title={formContent}>
          <div className={styles.search_pop}>
            <label>{lang.recommen}</label>
            <Button>{lang.search} <Icon type="caret-down" /></Button>
          </div>
        </Popconfirm>
        <div className={styles.allbet}>
          <Table columns={columns} dataSource={data}/>
        </div>
      </div>
    );
  }
};

export default RecommenFormList;
