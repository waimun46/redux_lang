import React, { Component } from 'react';
import icon1 from '../../../../assets/images/icon/Icon_Tiles.png';
import icon2 from '../../../../assets/images/icon/Icon_Clovers.png';
import icon3 from '../../../../assets/images/icon/Icon_Hearts.png';
import icon4 from '../../../../assets/images/icon/Icon_Pikes.png';
import $ from 'jquery'

class ZhoushituPanel extends Component {

  componentDidMount(){
    this.$slideOut = $('#slideOut2');

    this.$slideOut.find('.slideOutTab2').on('click', function() {
      $("#slideOut2").toggleClass('showSlideOut2');
    });

  }

  render() {
    return (
      <div id="ZhoushituPanel">

      <div id="slideOut2">
        <div className="slideOutTab2">
          <div className="right-title">
            <p>走势图</p>
          </div>
        </div>
        <div className="modal-content">
          <div className="modal-body2">
            <h3>走势图</h3>
            <div className="list-content">
              <div className="zoushi-icon">
                <div className="href-icon">
                  <img src={icon1} alt="icon1"/>
                </div>
                <div>
                  <span><img src={icon1} alt="icon1"/></span>
                  <span><img src={icon2} alt="icon2"/></span>
                  <span><img src={icon3} alt="icon3"/></span>
                  <span><img src={icon4} alt="icon4"/></span>
                </div>
                <div className="clearfix"></div>
              </div>
              <ul>
                <li>
                  <span>gmg4gggkd</span>
                  <span style={{color: '#00B727'}}>胜</span>
                  <span style={{color: '#00B727'}}>胜</span>
                  <span style={{color: '#00B727'}}>胜</span>
                  <span style={{color: '#EF5261'}}>负</span>
                  <div className="clearfix"></div>
                </li>
              </ul>
            </div>
          </div>
          <div className="modal-footer"> </div>
        </div>
      </div>

      </div>
    );
  }
};

export default ZhoushituPanel;
