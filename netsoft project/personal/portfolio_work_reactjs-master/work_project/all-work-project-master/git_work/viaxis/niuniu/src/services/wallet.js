import ScatterJS from 'scatterjs-core';
import ScatterEOS from 'scatterjs-plugin-eosjs';
import Eos from 'eosjs';

ScatterJS.plugins(new ScatterEOS());

const httpEndpoint = configs.eos.endpoint.split('://');
const host = httpEndpoint[1].split(':');
const network = {
    blockchain: 'eos',
    chainId: configs.eos.chainId,
    host: host[0],
    port: host.length > 1 ? host[1] : (httpEndpoint[0].toLowerCase() == 'https' ? 443 : 80),
    protocol: httpEndpoint[0]
};

class Wallet {
    connected = false;
    identity = null;
    connPromise = null;

    constructor() {
        if (Wallet.instance !== null) {
            return Wallet.instance;
        }
        Wallet.instance = this;
        this.connect();
    }

    connect() {
        if (this.connected)
            return new Promise(function (resolve, reject) { resolve(this.connected); }.bind(this));

        return this.connPromise = ScatterJS.scatter.connect('My-App').then(connected => {
            this.connected = connected;
            this.connPromise = null;
        });
    }

    get EOS() {
        const scatter = ScatterJS.scatter;
        // You can pass in any additional options you want into the eosjs reference.
        const eosOptions = { expireInSeconds: 60 };
        // Get a proxy reference to eosjs which you can use to sign transactions with a user's Scatter.
        const eos = scatter.eos(network, Eos, eosOptions);
        return eos;
    }

    async getBalance(account_name) {
        await this.connect();
        return await this.EOS.getCurrencyBalance("eosio.token", account_name);
    }

    async login() {
        await this.connect();
        const scatter = ScatterJS.scatter;
        return ScatterJS.scatter.getIdentity({ accounts: [network] }).then(async () => {
            // Always use the accounts you got back from Scatter. Never hardcode them even if you are prompting
            // the user for their account name beforehand. They could still give you a different account.
            const account = scatter.identity.accounts.find(x => x.blockchain === 'eos');
            window.g_app._store.dispatch({ type: "global/login", payload: { account, referral: sessionStorage.referral } }).then(() => {
                window.g_app._store.dispatch({ type: "global/getEOSBalance" });
                window.g_app._store.dispatch({ type: "global/getEOSAccount" });
            });
            sessionStorage.account = account.name;
            this.identity = account;
            return account;
        });
    }

    logout() {
        const scatter = ScatterJS.scatter;
        scatter.forgetIdentity()
            .then(() => {
                window.g_app._store.dispatch({ type: "global/logout" });
            });
        sessionStorage.removeItem("account");
    }

    async transfer(receive_name, quantity, memo) {
        await this.connect();
        const account = this.identity;
        const transactionOptions = { authorization: [`${account.name}@${account.authority}`] };
        return await this.EOS.transfer(account.name, receive_name, quantity, memo, transactionOptions);
    }

    async withdraw(quantity) {
        await this.connect();
        const account = this.identity;
        return await this.EOS.transaction({
            actions: [
                {
                    account: configs.contract.code,
                    name: 'withdraw',
                    authorization: [{
                        actor: account.name,
                        permission: account.authority
                    }],
                    data: {
                        to: account.name,
                        quantity: quantity
                    }
                }
            ]
        });
    }

    async bet(game_id, quantity, heap, referral) {
        await this.connect();
        const account = this.identity;
        const resp = await this.EOS.transaction({
            actions: [
                {
                    account: configs.contract.code,
                    name: 'bet',
                    authorization: [{
                        actor: account.name,
                        permission: account.authority
                    }],
                    data: {
                        bettor: account.name,
                        quantity: quantity,
                        game_id: game_id,
                        heap: heap,
                        ref_str: referral
                    }
                }
            ]
        });
        window.g_app._store.dispatch({ type: "global/getEOSAccount" });
        return resp;
    }

    async getAccount(name) {
        var account = await this.EOS.getAccount(name);
        return account;
    }
}
Wallet.instance = null;

const wallet = new Wallet();
export default wallet;