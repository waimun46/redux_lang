import React, { Component } from 'react';
import { Progress } from 'antd';
import classNames from "classnames";
import styles from './index.less';



class LoadingPage extends Component {
  componentDidMount() {
      if (/loading/.test(window.location.href)) {
          let element = document.getElementsByClassName('loadingpage')[0];
          element.classList.add("loading_width");
      }
  }
  render() {
    return (
      <section id="niuniu" className={styles.loading_page}>
        <div className={styles.logo_loading} ></div>
          <div className={styles.loading_bar} >
          <Progress percent={50} status="active" />
        </div>
      </section>
    );
  }
};

export default LoadingPage;
