import React, { Component } from 'react';
import RecommenFormList from './components/RecommenFormList';
import SlideOutPanel from '../bet/components/SlideOutPanel';
import styles from './index.less';


class RecommenForm extends Component {

  render() {
    return (
      <section id="niuniu">
        <RecommenFormList />
        <SlideOutPanel />
      </section>
    );
  }
};

export default RecommenForm;
