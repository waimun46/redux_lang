import React, { Component } from 'react';
import { Icon } from 'antd';
import diamond from '../../../../assets/images/icon/Icon_EOS_Outline.png';
import token from '../../../../assets/images/icon/Icon_Token.png';
import card01 from '../../../../assets/images/icon/Tiles_10.png';
import card02 from '../../../../assets/images/icon/Tiles_A.png';
import card03 from '../../../../assets/images/icon/Tiles_J.png';
import card04 from '../../../../assets/images/icon/Tiles_K.png';
import card05 from '../../../../assets/images/icon/Tiles_Q.png';
import niuniu from '../../../../assets/images/icon/NiuNiu.png';

class GameTable3 extends Component {

  render() {
    return (
      <div className="warp">
        <div className="warp-middle">
        {/* banker  */}
          <div className="bet-banker">
            <p>100</p>
          </div>
        {/* coin  */}
          <div className="coin">

          </div>
        {/* bet money  */}
          <div className="money-bet">
          
          </div>
          {/* player  */}
          <div className="bet-player">
            <p>100</p>
          </div>
        </div>

        {/* card  */}
        <div className="card-table">
          <ul>
            <li><img src={card01} alt="card01"/></li>
            <li><img src={card02} alt="card02"/></li>
            <li><img src={card03} alt="card03"/></li>
            <li><img src={card04} alt="card04"/></li>
            <li><img src={card05} alt="card05"/></li>
            <div className="clearfix"></div>
            <div className="niu-icon-show">
              <img src={niuniu} alt="niuniu"/>
            </div>
          </ul>
        </div>
      </div>

    );
  }
};

export default GameTable3;
