import React, { Component } from 'react';
import OddsWinTable from './OddsWinTable'
import OddsLoseTable from './OddsLoseTable'
import lang from '@/lang';
import styles from '../index.less';

class PayoutOdds extends Component {

  render() {
    return (
        <section className={styles.payout_odds}>
          <div className={styles.content}>
            <header>
                <h1>{lang.payoff}</h1>
                <p>{lang.payoff_subtitle}</p>
            </header>
            <div className={styles.odds_content}>
              <OddsWinTable/>
              <OddsLoseTable/>
              <div className="clearfix"></div>
            </div>
          </div>
        </section>
    );
  }
};

export default PayoutOdds;
