import React, { Component } from 'react';
import lang from '@/lang';
import styles from '../../index.less';


class AttentionListTwo extends Component {

  render() {
    return (
      <div className={styles.attention_list}>
        <div className={styles.list_content}>
          <label>{lang.example_2}</label>
          <ul>
            <li>
              <span><p>{lang.condition}</p></span>
              <span>{lang.bet_player}</span>
              <div className="clearfix"></div>
            </li>
            <li>
              <span><p>{lang.mode}</p></span>
              <span>{lang.double}</span>
              <div className="clearfix"></div>
            </li>
            <li>
              <span><p>{lang.credit}</p></span>
              <span>{lang.deduct}</span>
              <div className="clearfix"></div>
            </li>
            <li>
              <span><p>{lang.result}</p></span>
              <span>{lang.banker_bull_2}</span>
              <div className={styles.card_result}>
                <div className={styles.card_group_1}>
                  <div className={styles.card}>
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                    <div className="clearfix"></div>
                  </div>
                </div>

                <div className={styles.card_title_result}>
                  <span>&nbsp;</span>
                  <span>{lang.player_bull_2}</span>
                </div>

                <div className={styles.card_group_2}>
                  <div className={styles.card}>
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                    <div className="clearfix"></div>
                  </div>
                </div>

                <div className={styles.card_title_result}>
                  <span><p>{lang.payoff_return_title_2}</p></span>
                  <span>{lang.payoff_return_2}</span>
                </div>

                <div className="clearfix"></div>

              </div>
              <div className="clearfix"></div>
            </li>
          </ul>
        </div>
      </div>
    );
  }
};

export default AttentionListTwo;
