import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { message, Row, Col, Table, Tag, Button, Modal, Form, Input, Select, DatePicker } from 'antd';

import BasicComponent from "../common/BasicComponent";
import SearchForm from "../common/SearchForm";

import { queryMailmsg } from '../../actions/notice';
import * as RenderHelper from "../../utils/render";
import { MailMsgStatus } from "../../app_enums";

const createForm = Form.create;
const FormItem = Form.Item;
const Option = Select.Option;
const RangePicker = DatePicker.RangePicker;
const Message = message;

class MailMsg extends BasicComponent {
    constructor(props) {
        super(props);
        this.query = {};
        this.pageIndex = 1;
        this.state = {
            popup: false,
            content: ""
        };
    }

    /* ------------------------ Sys ------------------------ */

    componentWillMount() {
        this.queryData();
    }

    /* ------------------------ Functions ------------------------ */

    queryData() {
        this.props.dispatch(queryMailmsg(this.pageIndex, this.query)).then(act => {
            if (act.error)
                Message.error(act.error);
        });
    }

    /* ------------------------ Events ------------------------ */

    onSearchClick(query) {
        this.query = query;
        this.pageIndex = 1;
        this.queryData();
    }

    onResetClick() {
        this.query = {};
    }

    show(content) {
        this.setState({
            popup: true,
            content: content
        });
    }

    close() {
        this.setState({ popup: false });
    }

    /* ------------------------ Renders ------------------------ */

    render() {
        const self = this;
        const { notice } = this.props;
        const mailmsgs = (notice && notice.mailmsgs) || {
            count: 0,
            data: []
        };
        const pagination = {
            total: mailmsgs.count,
            onChange(current) {
                self.pageIndex = current;
                self.queryData();
            }
        }
        return (
            <div>
                {this.renderSearch()}
                <div className="search-result-list">
                    <Table columns={this.columns} dataSource={mailmsgs.data} loading={notice.isFetching} pagination={pagination} />
                </div>
                <Modal visible={this.state.popup} title="邮件内容" onCancel={this.close.bind(this)} footer={[
                    <Button key="close" type="primary" size="large" onClick={this.close.bind(this)}>关闭</Button>,
                ]}>
                    <div dangerouslySetInnerHTML={{ __html: this.state.content }} />
                </Modal>
            </div>
        );
    }

    renderSearch() {
        const formItemLayout = {
            labelCol: { span: 8 },
            wrapperCol: { span: 16 }
        };

        return (
            <SearchForm onSearch={this.onSearchClick.bind(this)} onReset={this.onResetClick.bind(this)}>
                <email label="邮箱" {...formItemLayout}>
                    <Input />
                </email>
                <create_time label="时间" {...formItemLayout} format={(times) => Utils.dateRangeFormat(times)}>
                    {RenderHelper.rangeDatePick()}
                </create_time>
            </SearchForm>
        );
    }

    get columns() {
        const self = this;
        const columns = [
            { title: '#', render(text, record, index) { return index + 1; } },
            { title: "邮箱", dataIndex: "Email" },
            { title: "标题", dataIndex: "Title" },
            {
                title: "内容", dataIndex: "Content", render(content) {
                    return <Button type="primary" onClick={self.show.bind(self, content)}>详细</Button>
                }
            },
            { title: "备注", dataIndex: "Remark" },
            { title: "状态", dataIndex: "Status", render: RenderHelper.render_status(MailMsgStatus) },
            { title: '发送时间', dataIndex: 'CreateTime', render: RenderHelper.render_time }
        ];
        return columns;
    }
}

export default connect(state => ({
    notice: state.notice,
}))(Form.create()(MailMsg));