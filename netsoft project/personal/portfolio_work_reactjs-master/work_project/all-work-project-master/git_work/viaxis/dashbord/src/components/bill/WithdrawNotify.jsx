import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Link } from "react-router";
import { notification, Badge, Icon } from 'antd';
import Sound from "react-sound";

import BasicComponent from "../common/BasicComponent";
import SearchForm from "../common/SearchForm";

import * as BillAction from "../../actions/bill";
import * as RenderHelper from "../../utils/render";
import * as Enums from "../../app_enums";


class WithdrawNotify extends BasicComponent {

    constructor(props) {
        super(props);
        this.state = {
            playStatus: Sound.status.PLAYING
        };
    }

    /* --------------- Sys --------------- */

    componentWillMount() {
        this.getWithdrawCount();
    }

    componentDidMount() {

        if (this.interval) {
            clearInterval(this.interval);
            this.interval = null;
        }
        this.interval = setInterval(this.getWithdrawCount.bind(this), 10000);

    }

    componentWillUnmount() {
        if (this.interval) {
            clearInterval(this.interval);
            this.interval = null;
        }
    }

    /* --------------- Function --------------- */

    getWithdrawCount() {
        this.props.dispatch(BillAction.countWithdraw());
    }

    /* --------------- Event ------------------- */
    onFinishedPlaying() {
        this.setState({ playStatus: Sound.status.PLAYING });
    }

    /* --------------- Render ------------------ */
    render() {
        const withdrawCount = this.props.withdrawCount || 0;
        return (
            <Link to="/withdraw/list">
                <Badge count={withdrawCount}>
                    <Icon type="pay-circle-o" />
                </Badge>
                {
                    withdrawCount > 0 &&
                    <Sound
                        url="/static/media/ding.mp3"
                        playStatus={this.state.playStatus}
                        playFromPosition={300 /* in milliseconds */}
                        onFinishedPlaying={this.onFinishedPlaying.bind(this)} />
                }
            </Link>
        );
    }
}

export default connect(state => ({
    withdrawCount: state.bill.withdrawCount,
}))(WithdrawNotify);