import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { message, Row, Col, Table, Tag, Button, Modal, Form, Input, Select, DatePicker } from 'antd';

import BasicComponent from "../common/BasicComponent";
import SearchForm from "../common/SearchForm";

import { queryBillflow } from '../../actions/bill';
import * as RenderHelper from "../../utils/render";
import * as Enums from "../../app_enums";
import * as Utils from "../../utils/common";

class BillFlow extends BasicComponent {

    constructor(props) {
        super(props);
        this.query = { LoadOperator: true };
        this.pageIndex = 1;
    }

    /* ----------------------- Sys ----------------------- */

    componentWillMount() {
        this.queryData();
    }

    /* ----------------------- Functions ---------------------- */

    queryData() {
        this.props.dispatch(queryBillflow(this.pageIndex, this.query));
    }

    /* ------------------------ Events ------------------------ */

    onSearchClick(query) {
        this.query = Object.assign(this.query, query);
        this.pageIndex = 1;
        this.queryData();
    }

    onResetClick() {
        this.query = { LoadOperator: true };
    }

    /* ------------------------ Renders ------------------------ */

    render() {
        const { isFetching } = this.props;
        const columns = this.tableColumns();

        const billflows = this.props.billflows || {};
        const data = billflows.data || [];

        const self = this;
        const pagination = {
            current: billflows.pageIndex || 1,
            total: billflows.count || 0,
            onChange(current) {
                self.pageIndex = current;
                self.queryData();
            },
        };
        return (
            <div>
                {this.renderSearch()}
                <div className="search-result-list">
                    <Table columns={columns} scroll={{ x: 1200 }} dataSource={data} loading={isFetching} pagination={pagination} rowKey="Id" />
                </div>
            </div>
        );
    }

    renderSearch() {
        const formItemLayout = {
            labelCol: { span: 8 },
            wrapperCol: { span: 16 }
        };

        return (
            <SearchForm onSearch={this.onSearchClick.bind(this)} onReset={this.onResetClick.bind(this)}>
                <email label="邮箱" {...formItemLayout}>
                    <Input />
                </email>
                <operator label="操作人" {...formItemLayout}>
                    <Input />
                </operator>
                <type label="类型" {...formItemLayout} options={{ initialValue: "" }}>
                    <Select>
                        <Select.Option value="">全部</Select.Option>
                        {Object.keys(Enums.BillFlowType).map(it => {
                            return <Select.Option value={it.toString()} key={it}>{Enums.BillFlowType[it]}</Select.Option>
                        })}
                    </Select>
                </type>
                <txid label="流水号" {...formItemLayout}>
                    <Input />
                </txid>
                <amount label="金额" {...formItemLayout}>
                    <Input />
                </amount>
                <remark label="备注" {...formItemLayout}>
                    <Input />
                </remark>
                <create_time label="时间" {...formItemLayout} format={(times) => Utils.dateRangeFormat(times)}>
                    <DatePicker.RangePicker />
                </create_time>
            </SearchForm>
        );
    }

    tableColumns() {
        const subtypes = {
            0: Enums.DepositType,
            1: Enums.WithdrawType,
            2: Enums.TransferType,
            3: Enums.GameSubType,
            5: Enums.BonuSubType
        };

        const columns = [{ title: '#', render(text, record, index) { return index + 1; } },
        { title: '邮箱', dataIndex: 'Email' },
        {
            title: "类型", render(flow) {
                return Enums.BillFlowType[flow.Type];
            }
        },
        {
            title: "子类型", render: (flow) => {
                let subtype = subtypes[flow.Type];
                if (subtype)
                    return subtype[flow.SubType];
                return '未知';
            }
        },
        { title: "金额", dataIndex: "Amount", className: "column-money", render: RenderHelper.money_color },
        { title: "余额", dataIndex: "Balance", className: 'column-money', render: RenderHelper.money_format },
        {
            title: "流水号", dataIndex: "Txid", render: (txid, flow) => {
                if (flow.Type == 3)
                    return <a target="_blank" href={`/dealinfo?deal_id=${flow.TargetId}`}>{txid}</a>
                return txid;
            }
        },
        { title: "备注", dataIndex: "Remark" },
        { title: "操作人", dataIndex: "Operator" },
        { title: '时间', dataIndex: 'CreateTime', render: RenderHelper.render_time }];

        return columns;
    }
}

export default connect(state => ({
    isFetching: state.bill.isFetching,
    billflows: state.bill.Billflows
}))(BillFlow);