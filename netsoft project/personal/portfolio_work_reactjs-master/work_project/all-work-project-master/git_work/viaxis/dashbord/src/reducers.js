import { combineReducers } from 'redux'
import { RoleReducer,RightsReducer,RoleUserReducer } from "./reducers/rights";
import { ConfigReducer } from "./reducers/config_info";
import { UserReducer } from "./reducers/user";
import { BillReducer } from "./reducers/bill";
import { BankReducer } from "./reducers/bank";
import { GameReducer } from "./reducers/game";
import { ReportReducer } from "./reducers/report";
import { ArticleReducer } from "./reducers/article";
import { NoticeReducer } from "./reducers/notice";
import { NotificationReducer } from "./reducers/notification";

const rootReducer = combineReducers({
	configinfo:ConfigReducer.reduce,
	role: RoleReducer.reduce,
	rights: RightsReducer.reduce,
	role_users: RoleUserReducer.reduce,
	user: UserReducer.reduce,
	bill: BillReducer.reduce,
	bank: BankReducer.reduce,
	game: GameReducer.reduce,
	report: ReportReducer.reduce,
	article: ArticleReducer.reduce,
	notice: NoticeReducer.reduce,
	notification: NotificationReducer.reduce
});

export default rootReducer;