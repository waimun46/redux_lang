import * as Enums from "./app_enums";

export const Menus= [
	{ name: "权限管理", icon: "team", key: "role", rights: Enums.SysRights.ADMIN_INSERT, children: [
		{ name: "角色列表", path: "/role", key: "role" },
		{ name: "权限列表", path: "/rights", key: "rights" },
		{ name: "用户角色", path: "/role/users", key:"userrole" }
	]},
	{ name: "用户管理", icon: "user", key: "user", children: [
		{ name: "用户列表", path: "/user", key: "user" },
		{ name: "代理列表", path: "/agent", key: "agent" },
		{ name: "当前在线", path: "/user/online", key: "online" },
		{ name: "操作日志", path: "/user/action/log", key: "actionlog" },
		{ name: "登陆日志", path: "/user/loginlog", key: "loginlog" },
		{ name: "用户银行卡", path: "/bank/user", key: "user_bank" },
		{ name: "分红比例", path: "/dividend", key: "dividend" },
		{ name: "推广列表", path: "/referral", key: "referral" },
	]},
	{ name: "资金管理", icon: "pay-circle-o", key: "bill", rights: Enums.SysRights.BILL_SELECT, children: [
		{ name: "用户余额", path: "/balance", key: "user_balance" },
		{ name: "提现申请", path: "/withdraw/list", key: "withdraw_req" },
		{ name: "充值申请", path: "/deposit/list", key: "deposit_req" },
		{ name: "流水记录", path: "/billflow", key: "billflow" },
		{ name: "转账记录", path: "/transfer/list", key: "transfer_list" },
		{ name: "手工充值", path: "/deposit", rights: Enums.SysRights.BILL_INSERT, key: "deposit" },
		{ name: "手工提现", path: "/withdraw", rights: Enums.SysRights.BILL_INSERT, key: "withdraw" },
		{ name: "手工转账", path: "/transfer", rights: Enums.SysRights.BILL_INSERT, key: "transfer" },
		{ name: "公司卡号", path: "/bank/company", key: "company_bank" }
	]},
	{ name: "游戏管理", icon: "calculator", key: "game", rights: Enums.SysRights.GAME_SELECT, children: [
		{ name: "投注记录", path: "/dealinfo", key: "dealinfo" },
		{ name: "开奖数据", path: "/game/data", key: "gamedata" },
		{ name: "游戏彩种", path: "/game/group", key: "group" },
		{ name: "开奖算法", path: "/game/algorithm", rights: Enums.SysRights.GAME_INSERT, key: "gamealgorithm"},
	]},
	{ name: "报表管理", icon: "line-chart", key: "report", children: [
		{ name: "用户盈亏", path: "/profit/user", key: "profitUser" },
		{ name: "玩法盈亏", path: "/profit/gametype", key: "profit_gametype" },
		{ name: "红利报表", path: "/profit/dividend", key: "profit_dividend" },
	]},
	{ name: "玩法管理", icon:"book", key: "gametype", rights: Enums.SysRights.GAME_SELECT, children: [
		{ name: "游戏玩法", path: "/game/type", rights: Enums.SysRights.ADMIN_UPDATE, key: "gametype" },
		{ name: "玩法信息", path: "/game/type/info", key: "gametypeinfo" },
		{ name: "玩法目录", path: "/game/type/category", rights: Enums.SysRights.ADMIN_UPDATE, key: "category" },
		{ name: "玩法种类", path: "/game/type/sort", rights: Enums.SysRights.ADMIN_UPDATE, key: "sort" },
		{ name: "玩法类型", path: "/game/type/kind", rights: Enums.SysRights.ADMIN_UPDATE, key: "kind" },
	]},
	{ name: "文章管理", icon: "file-text", key: "cms", children: [
		{ name: "目录列表", path: "/cms/category", key: "category" },
		{ name: "文章管理", path: "/cms/article", key: "article" },
	]},
	{ name: "通知管理", icon: "info-circle", key: "notice", children: [
		{ name: "系统公告", path: "/notice/bulletin", key: "bulletin" },
		{ name: "邮件列表", path: "/notice/mailmsg", key: "mailmsg" },
	]},
	{ name: "系统管理", icon: "setting", key: "setting", rights: Enums.SysRights.ADMIN_SELECT, children: [
		{ name: "系统配置", path: "/config", key: "config" },
		{ name: "银行管理", path: "/bank", key: "bank" }
	]}
];

export function parseBreadcrumb(path){
	path = "/" + path;
	var breadcrumbs = ["首页"];
	for(let i=0; i<Menus.length; i++){
		for(let j=0;j<Menus[i].children.length;j++){
			let link=Menus[i].children[j];
			if(link.path==path){
				breadcrumbs.push(Menus[i].name);
				breadcrumbs.push(link.name);
				return breadcrumbs;
			}
		}
	}
	return breadcrumbs;
}