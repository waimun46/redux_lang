import auth from "../utils/auth";

/**
 * 调用复合型 Action。
 * dispatch({name:str,callAPI:Function,shouldCallAPI:Func<bool>,payload:dict})
 * name : 表示
 * @param  {[type]} options.dispatch [description]
 * @param  {[type]} options.getState [description]
 * @return {[type]}                  [description]
 */
export default function callAPIMiddleware({dispatch,getState}){
	return function(next) {
		return function(action) {
			const { name, callAPI, shouldCallAPI = () => true, payload = {}} = action;

			if (!name) {
				//普通 action
				return next(action);
			}

			if (!typeof name === "string" || name === "" || name === null || name === undefined) {
				throw new Error("Expected name to be a not null string.");
			}

			if (typeof callAPI !== "function") {
				throw new Error("Expected fetch to be a function.");
			}

			if (!shouldCallAPI(getState())) {
				return;
			}

            const requestType = name;
            const successType = name + "_SUCCESS";
            const failureType = name + "_FAILURE";

            dispatch({
                type: requestType,
                payload
            });

			return callAPI().then(response =>response.json())
				.then(json => {
					if (json.code == 401) {
						auth.unauthorized();
					} else if (json.code == 403) {
						// auth.forbidden()
						return dispatch({
							error: "没有权限",
							type: failureType,
							payload
						})
					}else{
						return dispatch({
							response: json,
							type: successType,
							payload
						})
					}
				})
				.catch(error => dispatch({
						error: error,
						type: failureType,
						payload
					})
				);
		}
	}
}