import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Link } from "react-router";
import { message, Icon, Tooltip, Table, Button, Breadcrumb, Form, Input, Select, DatePicker } from 'antd';
import Moment from "moment";

import BasicComponent from "../common/BasicComponent";
import SearchForm from "../common/SearchForm";

import * as UserAction from '../../actions/user';
import * as ReportAction from '../../actions/report';
import * as RenderHelper from "../../utils/render";
import * as Utils from "../../utils/common";
import * as Enums from "../../app_enums";


class ProfitDividend extends BasicComponent {

    constructor(props) {
        super(props);
        this.userId = this.getUserId(props);
        this.pageIndex = 1;
        this.query = {
            from_date: this.getLastProfitDate()
        };
    }

    /* ---------------------------- 系统函数 ------------------------------*/

    componentWillMount() {
        this.queryData();
    }

    /* ---------------------------- Functions ----------------------------------- */

    /**
     * 获取上一次生成红利的时间
     * @return {[type]} [description]
     */
    getLastProfitDate() {
        var now = Moment();

        if (now.date() >= 16)
            now.set("date", 1);
        else
            now.add(-1, "months").set("date", 16);

        return now.format("YYYY-MM-DD");
    }

    queryData() {
        if (this.userId)
            this.props.dispatch(UserAction.getParents(this.userId));
        this.props.dispatch(ReportAction.queryProfitDividend(this.userId, this.query))
    }

    getUserId(props) {
        let userId = props.params.userId;
        if (userId)
            userId = parseInt(userId);
        return userId;
    }

    get parents() {
        let parents = this.props.parents || null;
        if (parents == null || parents.length == 0)
            parents = [{ UserId: 1, UserKind: 0, Email: "Company", Backpct: 0.085 }];
        return parents;
    }

    goto(userId) {
        this.userId = userId;
        const url = `/profit/dividend/${userId}`;
        super.goto(url);
        if (this.query.email)
            delete this.query.email;
        this.queryData();
    }

    disabledDate(current) {
        return !(current && [1, 16].indexOf(current.date()) >= 0);
    }

    /* ---------------------------- Events -------------------------------------- */

    onSearchClick(values) {
        this.query = values;
        this.queryData();
    }

    onResetClick() {
        this.query = {};
    }

    /* ---------------------------- Render -------------------------------------- */

    render() {
        const profitDividend = this.props.profitDividend || [];
        return (
            <div>
                {this.renderSearch()}
                {this.renderBreadcrumb()}
                <div className="search-result-list">
                    <Table columns={this.columns} dataSource={profitDividend}
                        scroll={{ x: 1100 }}
                        loading={this.props.isFetching} rowKey="UserId" />
                </div>
            </div>
        );
    }

    renderSearch() {
        const formItemLayout = {
            labelCol: { span: 8 },
            wrapperCol: { span: 16 }
        };

        return (
            <SearchForm onSearch={this.onSearchClick.bind(this)} onReset={this.onResetClick.bind(this)}>
                <email label="邮箱" {...formItemLayout}>
                    <Input />
                </email>
                <from_date label="时间" {...formItemLayout} format={it => it.format("YYYY-MM-DD")}>
                    <DatePicker disabledDate={this.disabledDate.bind(this)} />
                </from_date>
            </SearchForm>
        );
    }

    renderBreadcrumb() {
        let parents = this.parents;

        if (parents == null)
            parents = []
        return (
            <Breadcrumb separator=">">
                {parents.map((it, i) => <Breadcrumb.Item key={i}><Link onClick={() => this.goto(it.UserId)}>[{Enums.UserKind[it.UserKind]}]{it.Email}</Link></Breadcrumb.Item>)}
            </Breadcrumb>
        );
    }

    get columns() {


        const columns = [{ title: '#', render(text, record, index) { return index + 1; } },
        {
            title: '邮箱', dataIndex: 'Email', render: (email, profit) => {
                if (profit.UserId == this.userId)
                    return `[${Enums.UserKind[profit.UserKind]}] ${email}`;
                return <Link onClick={() => this.goto(profit.UserId)}>[{Enums.UserKind[profit.UserKind]}] {email}</Link>
            }
        },
        {
            title: "日期", render: it => {
                return `${it.FromDate} - ${it.ToDate}`;
            }
        },
        { title: "团队总红利", dataIndex: "TotalDividend", className: "column-money", render: RenderHelper.money_format },
        { title: "自身红利", dataIndex: "SelfDividend", className: "column-money", render: RenderHelper.money_format },
        {
            title: "团队盈亏", dataIndex: "TeamProfit", className: "column-money", render: (it, profit) => {
                if (it < 0 && profit.SelfProfit < 0) {
                    return (
                        <span>
                            {RenderHelper.money_color(it)}
                            &nbsp;&nbsp;
                            <Tooltip placement="topLeft" title="当团队盈亏与自身盈亏都为负数时，团队盈亏会自动减去自身盈亏。" arrowPointAtCenter>
                                <Icon type="info-circle-o" />
                            </Tooltip>
                        </span>
                    );
                }
                return RenderHelper.money_color(it);
            }
        },
        { title: "自身盈亏", dataIndex: "SelfProfit", className: "column-money", render: RenderHelper.money_color },
        { title: "有效投注", dataIndex: "TotalValidBet", className: "column-money", render: RenderHelper.money_format },
        { title: "红利比例", dataIndex: "Scale", className: "column-money", render: RenderHelper.render_percent() },
        { title: "状态", dataIndex: "Status", render: RenderHelper.render_status(Enums.ProfitDividendStatus) },
        { title: "时间", dataIndex: "CreateTime", render: RenderHelper.render_time }
        ];

        return columns;
    }
}

export default connect(state => ({
    isFetching: state.report.isFetching,
    profitDividend: state.report.ProfitDividend,
    parents: state.user.Parents
}))(ProfitDividend);