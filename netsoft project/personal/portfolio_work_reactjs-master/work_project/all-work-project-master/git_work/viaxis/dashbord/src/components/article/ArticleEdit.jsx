import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { message, Row, Col, Table, Tag, Button, Modal, Form, Input, Select, DatePicker } from 'antd';

import BasicComponent from "../common/BasicComponent";
import SearchForm from "../common/SearchForm";

import * as ArticleAction from '../../actions/article';
import * as RenderHelper from "../../utils/render";
import * as Enums from "../../app_enums";


class ArticleEdit extends BasicComponent {

    constructor(props) {
        super(props);
        this.articleId = parseInt(props.params.articleId);
        this.state = {loading:false, article:null};
    }

    /* ----------------------- Sys ----------------------- */

    componentWillMount() {
        this.queryData();

        const articles = (this.props.article || {}).data || [];
        const article = articles.find(it => it.Id == this.articleId);
        if(article==null || article==undefined){
            this.props.dispatch(ArticleAction.getArticle(this.articleId)).then(act => {
                this.assert(act, {
                    fnOk: (resp) => {
                        this.setState({article:resp.data});
                    }
                })
            })
        }else
            this.setState({article});
    }

    /* ----------------------- Functions ---------------------- */

    queryData(){
        this.props.dispatch(ArticleAction.queryCategory());
    }

    /* ------------------------ Events ------------------------ */

    handleSubmit(e){
        const { dispatch } = this.props;
        const { article } = this.state;
        e.preventDefault();
        this.props.form.validateFields((errors, values) => {
            if (errors) return;
            this.setState({loading:true});

            dispatch(ArticleAction.updateArticle(article.Id, values)).then(act => {
                this.setState({
                    loading: false
                });
                this.assert(act, {
                    fnOk: () => message.success("修改成功"),
                    fnError: (resp) => message.error(act.error||resp.message)
                });
            });
        });
    }

    handleReset(e){
        this.props.form.resetFields();
    }


    /* ------------------------ Renders ------------------------ */

    render() {
        const { isFetching } = this.props;
        const categorys = this.props.category || [];
        const {article} = this.state;

        const { getFieldDecorator, isFieldValidating, getFieldError } = this.props.form;
        const formItemLayout = {
            labelCol: { span: 4 },
            wrapperCol: { span: 16 },
        };

        return (
            <Form horizontal>
                <Form.Item {...formItemLayout} label="标题" hasFeedback>
                    {getFieldDecorator('title', {
                        initialValue: article&&article.Title,
                        rules: [
                            { required: true, message: '请输入文章标题' },
                        ],
                    })(
                        <Input />
                    )}
                </Form.Item>
                <Form.Item {...formItemLayout} label="Url地址" hasFeedback>
                    {getFieldDecorator('url_name', {
                        initialValue: article&&article.UrlName,
                        rules: [
                            { required: true, message: '请输入Url地址' },
                        ],
                    })(
                        <Input />
                    )}
                </Form.Item>
                <Form.Item {...formItemLayout} label="目录" hasFeedback>
                    {getFieldDecorator('category_id', {
                        initialValue: article&&article.CategoryId.toString(),
                        rules: [
                            { required: true, message: '请选择文章所属目录' },
                        ],
                    })(
                        <Select>
                            {categorys.map(it=><Select.Option value={it.Id.toString()} key={it}>{it.Name}</Select.Option>)}
                        </Select>
                    )}
                </Form.Item>
                <Form.Item {...formItemLayout} label="状态" hasFeedback>
                    {getFieldDecorator('status', {
                        initialValue: article&&article.Status.toString(),
                    })(
                        <Select>
                            {Object.keys(Enums.CommonStatus).map(it=><Select.Option value={it.toString()} key={it}>{Enums.CommonStatus[it]}</Select.Option>)}
                        </Select>
                    )}
                </Form.Item>
                <Form.Item {...formItemLayout} label="Meta" hasFeedback>
                    {getFieldDecorator('meta', {
                        initialValue: article&&article.Meta,
                        rules: [
                        { required: true, whitespace: false, message: '请输入Meta' },
                        ],
                    })(
                        <Input type="textarea" />
                    )}
                </Form.Item>
                <Form.Item {...formItemLayout} label="内容">
                    {getFieldDecorator('content', {
                        initialValue: article&&article.Content,
                        rules: [
                        { required: true, whitespace: false, message: '请输入内容' },
                        ],
                    })(
                        <Input type="textarea" autosize={{minRows:15}} />
                    )}
                </Form.Item>
                <Form.Item wrapperCol={{ span: 12, offset: 7 }}>
                      <Button type="primary" loading={this.state.loading} onClick={this.handleSubmit.bind(this)}>提交</Button>
                      &nbsp;&nbsp;&nbsp;
                      <Button type="ghost" onClick={this.handleReset.bind(this)}>重置</Button>
                </Form.Item>
            </Form>
        );
    }
}

export default connect(state => ({
    isFetching: state.article.isFetching,
    category: state.article.Category,
    article: state.article.Article
}))(Form.create()(ArticleEdit));