import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { message, Row, Col, Table, Tag, Button, Modal, Form, Input, InputNumber, Select,DatePicker } from 'antd';

import BasicComponent from "../common/BasicComponent";
import SearchForm from "../common/SearchForm";

import * as BillAction from "../../actions/bill";
import * as RenderHelper from "../../utils/render";
import * as Enums from "../../app_enums";


class DividendScaleList extends BasicComponent    {

    constructor(props) {
        super(props);
    }

    /* ---------------------- Sys ----------------------- */

    componentWillMount(){
        this.queryData();
    }

    /* ---------------------- Functions  -------------------------- */

    queryData(){
        this.props.dispatch(BillAction.list_dividend_scale());
    }

    /* ---------------------- Events ----------------------------- */

    onAddClick(){
        this._dialog = RenderHelper.dialog("添加分红比例", <AddFormWraper onSubmit={this.onAddSubmit.bind(this)} />);
    }

    onAddSubmit(form){
        return this.props.dispatch(BillAction.add_dividend_scale(form.scale, form.turnover))
            .then(act => {
                const resp = act.response;
                if (resp.code == 0) {
                    if(this._dialog){
                        this._dialog.destroy();
                        this._dialog = null;
                    }
                    this.queryData();
                }
                return act;
        });
    }

    onUpdateClick(scale) {
        this._dialog = RenderHelper.dialog(`编辑分红比例`, <EditFormWraper scale={scale} onSubmit={(it)=>this.onEditSubmit(scale,it)} />);
    }

    onEditSubmit(scale, form){
        return this.props.dispatch(BillAction.update_dividend_scale(scale.Id, form.scale, form.turnover))
            .then(act => {
                const resp = act.response;
                if (resp.code == 0) {
                    if (this._dialog) {
                        this._dialog.destroy();
                        this._dialog = null;
                    }
                    this.queryData();
                }
                return act;
            });
    }

    /* ---------------------- Renders ----------------------------- */
    render(){
        const { isFetching } = this.props;

        const scales = this.props.scales || [];

        return (
            <div>
                {this.hasRights("ADMIN.UPDATE")&&
                <Button type="primary" onClick={this.onAddClick.bind(this)}>添加</Button>}

                <Table columns={this.columns} dataSource={scales} loading={isFetching} pagination={false} />
            </div>
        );
    }

    get columns(){
        const columns = [{title: '#',render(text, record, index){return index+1;}}, 
            {title: "分红比例", dataIndex:"Scale", render:RenderHelper.render_percent(0)},
            {title: "15天销量", dataIndex:"Turnover", className: "column-money", render: RenderHelper.money_color},
            {title: '创建时间', dataIndex: 'CreateTime',render:RenderHelper.render_time},
            {title: "最后修改", dataIndex: "LastUpdateTime", render:RenderHelper.render_time},
        ];

        if(this.hasRights("ADMIN.UPDATE"))
            columns.push({title: "操作", render:(it)=>{
                return (
                    <Button type="primary" size="small" onClick={()=>this.onUpdateClick(it)}>修改</Button>
                );
            }});

        return columns;
    }

}


class AddForm extends BasicComponent {
    
    constructor(props) {
        super(props);
        this.state = {loading:false};
    }

    handleSubmit(e){
        e.preventDefault();

        const { dispatch } = this.props;

        this.props.form.validateFields((errors, values) => {
            if (errors) return;
            this.setState({loading:true});
            this.props.onSubmit&&this.props.onSubmit(values).then(act => {
                this.setState({loading:false});

                const resp = act.response;
                if (resp.code == 0) {
                    message.success("添加成功");
                } else {
                    message.error(resp.message);
                }

            });

        });
    }

    /* ---------------------------- Renders  ------------------------------*/
    render() {
        const { getFieldDecorator } = this.props.form;

        const formItemLayout = {
            labelCol: { span: 6 },
            wrapperCol: { span: 14 },
        };

        const tailFormItemLayout = {
            wrapperCol: {
                span: 14,
                offset: 6,
            },
        };

        return (
            <Form onSubmit={this.handleSubmit.bind(this)} autoComplete="off">
                <Form.Item label="分红比例" {...formItemLayout}>
                    {
                        getFieldDecorator('scale', {
                            rules: [{
                                required: true, message: "请输入分红比例"
                            }],
                        })(
                            <Input />
                        )
                    }
                </Form.Item>
                <Form.Item label="15日流水" {...formItemLayout}>
                    {
                        getFieldDecorator('turnover', {
                            rules: [{
                                required: true, message: "请输入15日流水"
                            }],
                        })(
                            <Input />
                        )
                    }
                </Form.Item>
                <Form.Item {...tailFormItemLayout}>
                    <Button loading={this.state.loading} className="fn-width100" type="primary" htmlType="submit" size="large">确定</Button>
                </Form.Item>
             </Form>
        );
    }
}
AddForm.propTypes = {
    /**
     * 确定提交事件
     * @param {dict} values 表单内容
     * @type {[type]}
     */
    OnSubmit:React.PropTypes.func
};
const AddFormWraper = Form.create()(AddForm);


class EditForm extends BasicComponent{
    constructor(props) {
        super(props);
        this.state = {loading:false};
    }

    handleSubmit(e){
        e.preventDefault();

        const { dispatch, parent } = this.props;

        this.props.form.validateFields((errors, values) => {
            if (errors) return;
            this.setState({loading:true});
            this.props.onSubmit&&this.props.onSubmit(values).then(act => {
                this.setState({loading:false});

                const resp = act.response;
                if (resp.code == 0) {
                    message.success("修改成功");
                } else {
                    message.error(resp.message);
                }

            });

        });
    }

    /* ---------------------------- Renders  ------------------------------*/

    render() {
        const { getFieldDecorator } = this.props.form;

        const formItemLayout = {
            labelCol: { span: 6 },
            wrapperCol: { span: 14 },
        };

        const tailFormItemLayout = {
            wrapperCol: {
                span: 14,
                offset: 6,
            },
        };

        const {scale} = this.props;

        return (
            <Form onSubmit={this.handleSubmit.bind(this)} autoComplete="off">
                <Form.Item label="分红比例" {...formItemLayout}>
                    {getFieldDecorator('scale', {initialValue:scale.Scale,
                        rules: [{
                                required: true, message: "请输入分红比例"
                            }]})(
                        <Input />
                    )}
                </Form.Item>
                <Form.Item label="15天销量" {...formItemLayout}>
                    {getFieldDecorator('turnover', {initialValue:scale.Turnover,
                        rules: [{
                                required: true, message: "请输入15日流水"
                            }],})(
                        <Input />
                    )}
                </Form.Item>
                <Form.Item {...tailFormItemLayout}>
                    <Button loading={this.state.loading} className="fn-width100" type="primary" htmlType="submit" size="large">确定</Button>
                </Form.Item>
             </Form>
        );
    }
}

EditForm.propTypes = {
    /**
     * 编辑的对象
     * @type {[type]}
     */
    scale: React.PropTypes.object.isRequired,
    /**
     * 确定提交事件
     * @param {dict} values 表单内容
     * @type {[type]}
     */
    OnSubmit:React.PropTypes.func
};
const EditFormWraper = Form.create()(EditForm);

export default connect(state => ({
    isFetching: state.bill.isFetching,
    scales: state.bill.DividendScale
}))(Form.create()(DividendScaleList));