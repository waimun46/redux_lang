import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Link } from "react-router";
import { message, Row, Col, Table, Button, Breadcrumb, Form, Input, Select, DatePicker } from 'antd';

import BasicComponent from "../common/BasicComponent";
import SearchForm from "../common/SearchForm";

import * as UserAction from '../../actions/user';
import * as GameAction from '../../actions/game';
import * as ReportAction from '../../actions/report';
import * as RenderHelper from "../../utils/render";
import * as Utils from "../../utils/common";
import * as Enums from "../../app_enums";


class ProfitGameType extends BasicComponent {

    constructor(props) {
        super(props);
        this.userId = this.getUserId(props);
        this.pageIndex = 1;
        this.queryDetail = false;
        this.query = {};
    }

    /* ---------------------------- 系统函数 ------------------------------*/

    componentWillMount() {
        this.queryData();
        this.props.dispatch(GameAction.queryGameGroup());
    }

    /* ---------------------------- Functions ----------------------------------- */

    queryData() {
        if (!this.queryDetail)
            this.props.dispatch(ReportAction.queryProfitGameType(this.userId, Object.assign({}, this.query, { pageIndex: this.pageIndex })))
        else
            this.props.dispatch(ReportAction.queryProfitGameTypeByUser(this.userId, Object.assign({}, this.query, { pageIndex: this.pageIndex })));
    }

    getUserId(props) {
        let userId = props.params.userId;
        if (userId)
            userId = parseInt(userId);
        return userId;
    }


    /* ---------------------------- Events -------------------------------------- */

    onSearchClick(values) {
        this.pageIndex = 1;
        this.query = values;
        if (!this.queryDetail && Utils.isNonEmptyString(this.query.email))
            this.queryDetail = true;
        this.queryData();
    }

    onResetClick() {
        this.query = {};
    }

    goBack() {
        this.queryDetail = false;
        if (this.query.game_type)
            delete this.query["game_type"];
        this.queryData();
    }

    /* ---------------------------- Render -------------------------------------- */

    render() {
        const profitGameType = this.props.profitGameType || {};
        const self = this;

        const pagination = {
            current: profitGameType.pageIndex || 1,
            total: profitGameType.count || 0,
            onChange(current) {
                self.pageIndex = current;
                self.queryData();
            },
        };
        return (
            <div>
                {this.renderSearch()}
                {this.queryDetail && <Button type="primary" size="small" onClick={this.goBack.bind(this)}>返回</Button>}
                <div className="search-result-list">
                    <Table columns={this.columns} dataSource={profitGameType.data || []}
                        loading={this.props.isFetching}
                        pagination={pagination} rowKey="rn" />
                </div>
            </div>
        );
    }

    renderSearch() {
        const formItemLayout = {
            labelCol: { span: 8 },
            wrapperCol: { span: 16 }
        };

        const groups = this.props.groups || [];

        return (
            <SearchForm onSearch={this.onSearchClick.bind(this)} onReset={this.onResetClick.bind(this)}>
                <email label="邮箱" {...formItemLayout}>
                    <Input />
                </email>
                <group_id label="彩种" {...formItemLayout} options={{ initialValue: "" }}>
                    <Select>
                        <Option value="">全部</Option>
                        {groups.map(group => {
                            return <Select.Option value={group.Id.toString()} key={group.Id}>{group.Name}</Select.Option>
                        })}
                    </Select>
                </group_id>
                <create_time label="时间" {...formItemLayout} format={(times) => Utils.dateRangeFormat(times)}>
                    {RenderHelper.rangeDatePick()}
                </create_time>
            </SearchForm>
        );
    }

    get columns() {

        if (this.queryDetail)
            return this.userColumns;

        return this.gameTypeColumns;

    }

    get gameTypeColumns() {
        const columns = [{ title: '#', render(text, record, index) { return index + 1; } },
        {
            title: "日期", render: it => {
                if (it.ToDate != it.Date)
                    return `${it.Date} - ${it.ToDate}`;
                return it.Date;
            }
        },
        { title: "彩种", dataIndex: "GroupName" },
        {
            title: "玩法", dataIndex: "GameName", render: (it, record) => <a onClick={() => {
                this.query['game_type'] = record.GameType;
                this.queryDetail = true;
                this.queryData();
            }}>{it}</a>
        },
        { title: "用户数", dataIndex: "UserCount" },
        {
            title: "订单数", dataIndex: "OrderCount", className: "column-money", render: (it, record) => {
                return <a target="_blank" href={`/dealinfo?game_type=${record.GameType}&from_time=${record.Date}&to_time=${record.ToDate}235959`}>{it}</a>
            }
        },
        { title: "总投注", dataIndex: "TotalBet", className: "column-money", render: RenderHelper.money_format },
        { title: "有效投注", dataIndex: "ValidBet", className: "column-money", render: RenderHelper.money_format },
        { title: "盈亏", dataIndex: "Profit", className: "column-money", render: RenderHelper.money_color },
        ];

        return columns;
    }

    get userColumns() {
        const columns = [{ title: '#', render(text, record, index) { return index + 1; } },
        {
            title: "日期", render: it => {
                if (it.ToDate != it.Date)
                    return `${it.Date} - ${it.ToDate}`;
                return it.Date;
            }
        },
        { title: "Email", dataIndex: "Email" },
        { title: "彩种", dataIndex: "GroupName" },
        { title: "玩法", dataIndex: "GameName" },
        {
            title: "订单数", dataIndex: "OrderCount", className: "column-money", render: (it, record) => {
                return <a target="_blank" href={`/dealinfo?email=${record.Email}&game_type=${record.GameType}&from_time=${record.Date}&to_time=${record.ToDate}235959`}>{it}</a>
            }
        },
        { title: "总投注", dataIndex: "TotalBet", className: "column-money", render: RenderHelper.money_format },
        { title: "有效投注", dataIndex: "ValidBet", className: "column-money", render: RenderHelper.money_format },
        { title: "盈亏", dataIndex: "Profit", className: "column-money", render: RenderHelper.money_color },
        ];

        return columns;
    }
}

export default connect(state => ({
    isFetching: state.report.isFetching,
    groups: state.game.Groups,
    profitGameType: state.report.ProfitGameType,
    parents: state.user.Parents
}))(ProfitGameType);