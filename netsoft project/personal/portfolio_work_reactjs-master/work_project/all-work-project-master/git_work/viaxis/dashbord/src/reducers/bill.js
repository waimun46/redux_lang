import * as app_const from "../app_const";
import ReduceBase from "./reduce_base";
import * as Utils from "../utils/common";

const defaultState = {
	isFetching: false,
	// 提现记录
	withdraw: {
		pageIndex: 1,
		count: 0,
		data: []
	}
}

class BillReducer extends ReduceBase {
	static reduce(state = {}, action) {
		switch (action.type) {
			case `${app_const.MANUAL_DEPOSIT}_SUCCESS`:
			case `${app_const.MANUAL_WITHDRAW}_SUCCESS`:
			case `${app_const.COMPLETE_DEPOSIT}_SUCCESS`:
			case `${app_const.REFUSE_DEPOSIT}_SUCCESS`:
			case `${app_const.CHECKING_WITHDRAW}_SUCCESS`:
			case `${app_const.CHECKED_WITHDRAW}_SUCCESS`:
			case `${app_const.PROCESS_WITHDRAW}_SUCCESS`:
			case `${app_const.COMPLETE_WITHDRAW}_SUCCESS`:
			case `${app_const.REFUSE_WITHDRAW}_SUCCESS`:
			case `${app_const.TRANSFER}_SUCCESS`:
				return BillReducer._common_success(state, action);
			case `${app_const.QUERY_TRANSFER}_SUCCESS`:
				return BillReducer._query_success(state, action, "Transfer");
			case `${app_const.QUERY_USER_BALANCE}_FAILURE`:
				return Object.assign({}, state, { isFetching: false });
			case `${app_const.QUERY_DEPOSIT}_SUCCESS`:
				return BillReducer._query_success(state, action, "Deposits");
			case `${app_const.QUERY_WITHDRAW}_SUCCESS`:
				return BillReducer._query_success(state, action, "Withdraws");
			case `${app_const.COUNT_WITHDRAW}_SUCCESS`:
				return BillReducer._count_withdraw(state, action);
			case `${app_const.SUM_BILLFLOW}_SUCCESS`:
				return BillReducer._sum_billflow(state, action);
			case `${app_const.QUERY_BILLFLOW}_SUCCESS`:
				return BillReducer._query_success(state, action, "Billflows");
			case `${app_const.QUERY_USER_BALANCE}_SUCCESS`:
				return BillReducer._query_success(state, action, "Balances");
			case `${app_const.LIST_DIVIDEND_SCALE}_SUCCESS`:
				return BillReducer._list_success(state, action, "DividendScale");
			case app_const.QUERY_DEPOSIT:
			case app_const.QUERY_WITHDRAW:
			case app_const.QUERY_BILLFLOW:
			case app_const.SUM_BILLFLOW:
			case app_const.QUERY_USER_BALANCE:
			case app_const.LIST_DIVIDEND_SCALE:
				return Object.assign({}, state, { isFetching: true });
			case `${app_const.QUERY_DEPOSIT}_FAILURE`:
			case `${app_const.QUERY_WITHDRAW}_FAILURE`:
			case `${app_const.QUERY_BILLFLOW}_FAILURE`:
			case `${app_const.TRANSFER}_FAILURE`:
			case `${app_const.QUERY_TRANSFER}_FAILURE`:
			case `${app_const.LIST_DIVIDEND_SCALE}_FAILURE`:
			default:
				return state;
		}
	}

	static _count_withdraw(state, action) {
		const resp = action.response;
		if (resp.code == 0) {
			return Object.assign({}, state, { withdrawCount: resp.data });
		}
		return state;
	}

	static _common_success(state, action) {
		const resp = action.response;
		if (resp.code == 0) return state;
		else {
			action.error = resp.message;
			action.error_code = resp.code;
			return state;
		}
	}

	static _sum_billflow(state, action) {
		const resp = action.response;
		if (resp.code == 0) {
			const Sum = resp.data;
			return Object.assign({}, state, { Sum, isFetching: false });
		}
		return state;
	}

}

export { BillReducer };