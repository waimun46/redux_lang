import React from 'react'
import { connect } from 'react-redux';
import { Spin } from 'antd';
import BasicComponent from "./common/BasicComponent";

import * as ReportAction from '../actions/report';
import * as Utils from "../utils/common";
import * as RenderHelper from "../utils/render";

class home extends BasicComponent {

    constructor(props) {
        super(props);
    }

    componentWillMount() {
        if (!this.props.statistic)
            this.getStatistic();
    }

    getStatistic(fromDate, toDate) {
        this.props.dispatch(ReportAction.queryPlatformStatistic(fromDate, toDate));
    }

    onTimeRangeChange(dates, dateStrings) {
        const format = "YYYY-MM-DD HH:mm:ss";

        dates[0].set('hour', 0).set('minute', 0).set('second', 0);
        dates[1].set('hour', 23).set('minute', 59).set('second', 59);

        const from_time = dates[0].format(format);
        const to_time = dates[1].format(format);

        this.getStatistic(from_time, to_time);
    }


    render() {

        const statistic = this.props.statistic;


        return (
            <div>
                <h1>
                    平台统计
                    &nbsp;&nbsp;
                    {RenderHelper.rangeDatePick({ onChange: this.onTimeRangeChange.bind(this) })}
                </h1>
                <Spin spinning={statistic == null}>
                    <div className="ant-table-body">
                        {statistic &&
                            <table>
                                <tbody className="ant-table-tbody">
                                    <tr className="ant-table-row">
                                        <td>平台总余额</td>
                                        <td>{RenderHelper.money_format(statistic.Balance)}</td>
                                        <td>有效用户</td>
                                        <td>{statistic.TotalUserCount}</td>
                                    </tr>
                                    <tr className="ant-table-row">
                                        <td>总充值</td>
                                        <td>{statistic.TotalDeposit}</td>
                                        <td>次数</td>
                                        <td>{statistic.DepositCount}</td>
                                    </tr>
                                    <tr className="ant-table-row">
                                        <td>总提现</td>
                                        <td>{statistic.TotalWithdraw}</td>
                                        <td>次数</td>
                                        <td>{statistic.WithdrawCount}</td>
                                    </tr>
                                    <tr className="ant-table-row">
                                        <td>登陆人数</td>
                                        <td>{statistic.SignInCount}</td>
                                        <td>注册人数</td>
                                        <td>{statistic.SignUpCount}</td>
                                    </tr>
                                </tbody>
                            </table>
                        }
                    </div>
                </Spin>
            </div>
        );
    }
}

export default connect(state => ({
    isFetching: state.report.isFetching,
    statistic: state.report.PlatformStatistic
}))(home);
