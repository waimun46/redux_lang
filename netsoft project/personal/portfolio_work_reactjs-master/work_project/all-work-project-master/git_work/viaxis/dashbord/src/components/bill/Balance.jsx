import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { message, Row, Col, Table, Tag, Button, Modal, Form, Input, Select, DatePicker } from 'antd';

import BasicComponent from "../common/BasicComponent";
import SearchForm from "../common/SearchForm";

import { queryUserBalance } from '../../actions/bill';
import * as RenderHelper from "../../utils/render";
import { dateFormat } from "../../utils/string";
import * as Enums from "../../app_enums";

class Balance extends BasicComponent {
    constructor(props) {
        super(props);
        this.querys = {};
        this.pageIndex = 1;
    }

    /* ------------------------ Sys -------------------------- */

    componentWillMount() {
        this.queryData();
    }

    /* ------------------------ Functions ------------------------ */

    queryData() {
        this.props.dispatch(queryUserBalance(this.pageIndex, this.querys));
    }

    /* ------------------------ Events --------------------------- */

    onSearchClick(querys) {
        this.querys = querys;
        this.pageIndex = 1;
        this.queryData();
    }

    onResetClick() {
        this.querys = {};
    }

    /* ------------------------ Renders --------------------------- */

    render() {
        const { isFetching } = this.props;
        const balances = this.props.balances || {};
        const data = balances.data || [];

        const self = this;
        const pagination = {
            current: balances.pageIndex || 1,
            total: balances.count || 0,
            onChange(current) {
                self.pageIndex = current;
                self.queryData();
            }
        };

        return (
            <div>
                {this.renderSearch()}
                <br />
                <Table bordered columns={this.columns} dataSource={data} loading={isFetching} pagination={pagination} rowKey="ID" scroll={{ x: 1300 }} />
            </div>
        );
    }

    renderSearch() {
        const formItemLayout = {
            labelCol: { span: 8 },
            wrapperCol: { span: 16 }
        };

        return (
            <SearchForm onSearch={this.onSearchClick.bind(this)} onReset={this.onResetClick.bind(this)}>
                <email label="邮箱" {...formItemLayout}>
                    <Input />
                </email>
            </SearchForm>
        );
    }

    get columns() {
        const columns = [
            { title: "#", render: RenderHelper.render_index },
            { title: "邮箱", dataIndex: "Email" },
            { title: "用户类型", dataIndex: "UserKind", render: (val) => Enums.UserKind[val] },
            {
                title: "资金", children: [
                    { title: "余额", dataIndex: "Balance.Balance", className: "column-money", render: RenderHelper.money_format, key: "Balance.Balance" },
                    { title: "冻结", dataIndex: "Balance.Freeze", className: "column-money", render: RenderHelper.money_format, key: "Balance.Freeze" }
                ]
            },
            {
                title: "充提", children: [
                    { title: "充值", dataIndex: "Balance.Deposit", className: "column-money", render: RenderHelper.money_format, key: "Balance.Deposit" },
                    { title: "提现", dataIndex: "Balance.Withdraw", className: "column-money", render: RenderHelper.money_format, key: "Balance.Withdraw" },
                ]
            },
            {
                title: "转账", children: [
                    { title: "转入", dataIndex: "Balance.TransferIn", className: "column-money", render: RenderHelper.money_format, key: "Balance.TransferIn" },
                    { title: "转出", dataIndex: "Balance.TransferOut", className: "column-money", render: RenderHelper.money_format, key: "Balance.TransferOut" },
                ]
            },
            {
                title: "游戏", children: [
                    { title: "总投注", dataIndex: "Balance.TotalBet", className: "column-money", render: RenderHelper.money_format, key: "Balance.TotalBet" },
                    { title: "转出", dataIndex: "Balance.ValidBet", className: "column-money", render: RenderHelper.money_format, key: "Balance.ValidBet" },
                ]
            },
            {
                title: "盈亏", children: [
                    { title: "输赢", dataIndex: "Balance.Profit", className: "column-money", render: RenderHelper.money_color, key: "Balance.Profit" },
                    { title: "佣金", dataIndex: "Balance.Commission", className: "column-money", render: RenderHelper.money_format, key: "Balance.Commission" },
                    { title: "奖金", dataIndex: "Balance.Bonus", className: "column-money", render: RenderHelper.money_format, key: "Balance.Bonus" },
                    { title: "总输赢", dataIndex: "Balance", className: "column-money", key: "Balance", render: (bal) => RenderHelper.money_color(bal.Profit + bal.Commission + bal.Bonus) },
                ]
            },
            { title: "状态", dataIndex: "Status", render: RenderHelper.render_status(Enums.UserStatus) },
            { title: "注册时间", dataIndex: "Timestamp", render: RenderHelper.render_time }
        ];
        return columns;
    }
}

export default connect(state => ({
    isFetching: state.bill.isFetching,
    balances: state.bill.Balances,
}))(Balance);