var path = require('path');
var webpack = require("webpack");

var node_modules = path.resolve(__dirname, 'node_modules');
var pathToReact = path.resolve(node_modules, 'react/dist/react.min.js');

const webpackDevHost = '0.0.0.0';
const webpackDevPort = 8886;


var definePlugin = new webpack.DefinePlugin({
    __DEV__: JSON.stringify(JSON.parse(process.env.BUILD_DEV || 'true')),
    __PRERELEASE__: JSON.stringify(JSON.parse(process.env.BUILD_PRERELEASE || 'false'))
});

module.exports = {
    entry: {
        // Hot replace
        // main: ['babel-polyfill', './src/main.jsx'],
        // login: ['babel-polyfill', './src/login.jsx']
        main: './src/main.jsx',
    },
    output: {
        filename: '[name].js',
        path: __dirname + '/dist', // `dist` is the destination
        publicPath: "/assets/",
        alias: {
            'react': pathToReact
        }
    },
    resolve: {
        extensions: ['', '.js', '.jsx']
    },
    resolveLoader: {
        modulesDirectories: ["node_modules"],
    },
    devServer: {
        hot: true,
        open: true, // to open the local server in browser
        contentBase: __dirname + '/pages',
        host: webpackDevHost,
        port: webpackDevPort,
        historyApiFallback: true,
    },
    module: {
        loaders: [{
            test: /\.js[x]?$/,
            exclude: /node_modules/,
            loader: 'babel-loader?presets[]=es2015&presets[]=react'
        }, {
            test: /\.css$/,
            loader: 'style!css'
        }, {
            test: /\.(png|jpg)$/,
            loader: 'url-loader'
        }, {
            test: /\.json$/,
            loader: 'json-loader'
        }],
        rules: [{
            test: /\.json$/,
            use: 'json-loader'
        }]
    },
    plugins: [
        definePlugin
    ]
};