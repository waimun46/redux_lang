import $ from "jquery";
import * as app_const from "../app_const";
import * as Utils from "../utils/common";

export function queryCategory(query = {}) {
    const payload = JSON.stringify(query);
    return {
        name: app_const.QUERY_ARTICLE_CATEGORY,
        callAPI: () => Utils.POST(`/api/article/category`, payload)()
    }
}

export function addCategory(name, label, categoryId) {
    const payload = JSON.stringify({ name, label, category_id: categoryId });
    return {
        name: app_const.ADD_ARTICLE_CATEGORY,
        callAPI: () => Utils.POST("/api/article/category/add", payload)()
    }
}

export function updateCategory(id, values) {
    const payload = JSON.stringify(values);
    return {
        name: app_const.UPDATE_ARTICLE_CATEGORY,
        callAPI: () => Utils.POST(`/api/article/category/update/${id}`, payload)()
    }
}

export function getArticle(id) {
    return {
        name: app_const.GET_ARTICLE,
        callAPI: () => Utils.POST(`/api/article/${id}`)()
    }
}

export function queryArticle(page, querys = {}) {
    const payload = JSON.stringify(Object.assign({}, querys, { page }));
    return {
        name: app_const.QUERY_ARTICLE,
        callAPI: () => Utils.POST(`/api/articles`, payload)()
    }
}

export function addArticle(categoryId, urlName, meta, title, content) {
    const payload = JSON.stringify({ meta, title, content, category_id: categoryId, url_name: urlName });
    return {
        name: app_const.ADD_ARTICLE,
        callAPI: () => Utils.POST("/api/article/add", payload)()
    }
}

export function updateArticle(id, values) {
    const payload = JSON.stringify(values);
    return {
        name: app_const.UPDATE_ARTICLE,
        callAPI: () => Utils.POST(`/api/article/update/${id}`, payload)()
    }
}