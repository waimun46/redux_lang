import $ from "jquery";
import * as app_const from "../app_const";
import * as Utils from "../utils/common";

export function addBank(name, code, type) {
	var payload = { name, code, type };
	return {
		name: app_const.ADD_BANK,
		callAPI: () => Utils.POST(`/api/bank/add`, JSON.stringify(payload))()
	};
}

export function updateBank(bank_id, name, code, type, status) {
	var payload = { name, code, type, status };
	return {
		name: app_const.UPDATE_BANK,
		callAPI: () => Utils.POST(`/api/bank/${bank_id}/update`, JSON.stringify(payload))()
	};
}

export function queryBank(querys = {}) {
	var queryStr = JSON.stringify(querys);
	return {
		name: app_const.QUERY_BANK,
		callAPI: () => Utils.POST(`/api/bank`, queryStr)()
	}
}

export function addCompanyBank(bank_id, account_no, account_name, branch, sign_key, gateway_url, total_deposit_limit) {
	var payload = {
		BankId: bank_id,
		AccountNo: account_no,
		AccountName: account_name,
		BranchName: branch,
		SignKey: sign_key,
		GateWayUrl: gateway_url,
		TotalDepositLimit: total_deposit_limit || 0
	};
	return {
		name: app_const.ADD_COMPANYBANK,
		callAPI: () => Utils.POST(`/api/bank/company/add`, JSON.stringify(payload))()
	};
}

export function updateCompanyBank(com_bank_id, bank_id, account_no, account_name, branch, sign_key, gateway_url, total_deposit_limit, status) {
	var payload = { bank_id, account_no, account_name, branch, sign_key, gateway_url, total_deposit_limit, status };
	return {
		name: app_const.UPDATE_COMPANYBANK,
		callAPI: () => Utils.POST(`/api/bank/company/${com_bank_id}/update`, JSON.stringify(payload))()
	};
}

export function queryCompanyBank(pageIndex = 1, querys = {}) {
	var queryParams = Object.assign({}, querys, {
		pageIndex
	});
	var queryStr = JSON.stringify(queryParams);
	return {
		name: app_const.QUERY_COMPANYBANK,
		callAPI: () => Utils.POST(`/api/bank/company`, queryStr)()
	}
}

export function updateUserBank(user_bank_id, bank_id, account_no, account_name, branch, status, sign_key = null, gateway_url = null, total_deposit_limit = null) {
	var payload = { bank_id, account_no, account_name, branch, sign_key, gateway_url, total_deposit_limit, status };

	return {
		name: app_const.UPDATE_USERBANK,
		callAPI: () => Utils.POST(`/api/bank/user/${user_bank_id}/update`, JSON.stringify(payload))()
	};
}

export function queryUserBank(pageIndex = 1, querys = {}) {
	var queryParams = Object.assign({}, querys, {
		pageIndex
	});
	var queryStr = JSON.stringify(queryParams);
	const { userId } = querys;
	const url = userId ? `/api/bank/user/${userId}` : `/api/bank/user`
	return {
		name: app_const.QUERY_USERBANK,
		callAPI: () => Utils.POST(url, queryStr)()
	}
}