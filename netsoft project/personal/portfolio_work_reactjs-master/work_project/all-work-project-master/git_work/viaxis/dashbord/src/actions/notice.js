import $ from "jquery";
import * as app_const from "../app_const";
import * as Utils from "../utils/common";

// Bulletin

export function queryBulletin(pageIndex, query = {}) {
    var queryParams = Object.assign({}, query, { pageIndex });
    var queryStr = JSON.stringify(queryParams);
    return {
        name: app_const.QUERY_BULLETIN,
        callAPI: () => Utils.POST(`/api/notice/bulletin`, queryStr)()
    };
}

export function getBulletin(id) {
    return {
        name: app_const.GET_BULLETIN,
        callAPI: () => Utils.POST(`/api/notice/bulletin/${id}`)()
    };
}

export function addBulletin(content, lang, type, status, expiretime) {
    var payload = { content, lang, type, status, expiretime };
    return {
        name: app_const.ADD_BULLETIN,
        callAPI: () => Utils.POST(`/api/notice/bulletin/add`, JSON.stringify(payload))()
    };
}

export function updateBulletin(id, content, lang, type, status, expiretime) {
    var payload = { content, lang, type, status, expiretime };
    return {
        name: app_const.UPDATE_BULLETIN,
        callAPI: () => Utils.POST(`/api/notice/bulletin/${id}/update`, JSON.stringify(payload))()
    };
}


// mailmsg
export function queryMailmsg(pageIndex, query = {}) {
    var queryParams = Object.assign({}, query, { pageIndex });
    var queryStr = JSON.stringify(queryParams);
    return {
        name: app_const.QUERY_MAILMSG,
        callAPI: () => Utils.POST(`/api/notice/mailmsg`, queryStr)()
    };
}