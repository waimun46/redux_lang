import ReduceBase from "./reduce_base";
import * as app_const from "../app_const"
import * as Utils from "../utils/common";

class UserReducer extends ReduceBase {
    static reduce(state = { isFetching: false }, action) {
        switch (action.type) {
            case app_const.GET_USER:
            case app_const.QUERY_USERS:
            case app_const.ONLINE_USERS:
            case app_const.QUERY_AGENT:
            case app_const.QUERY_LOGINLOG:
            case app_const.QUERY_REBATELEVEL:
            case app_const.ADD_USER:
            case app_const.UPDATE_USER:
            case app_const.UPDATE_USER_PASSWORD:
            case app_const.QUERY_USER_UPDATE_LOG:
            case app_const.QUERY_REFERRAL:
                return Object.assign({}, state, {
                    isFetching: true
                });
            case `${app_const.GET_USER}_SUCCESS`:
                return UserReducer._get_user_success(state, action);
            case `${app_const.QUERY_USERS}_SUCCESS`:
                return UserReducer._query_success(state, action, "Users");
            case `${app_const.ONLINE_USERS}_SUCCESS`:
                return UserReducer._query_success(state, action, "Online");
            case `${app_const.QUERY_LOGINLOG}_SUCCESS`:
                return UserReducer._query_success(state, action, "loginlog");
            case `${app_const.QUERY_ACTIONLOG}_SUCCESS`:
                return UserReducer._query_success(state, action, "ActionLog");
            case `${app_const.QUERY_TEAM}_SUCCESS`:
                return UserReducer._query_success(state, action, "Team");
            case `${app_const.GET_PARENT}_SUCCESS`:
                return UserReducer._list_success(state, action, "Parents");
            case `${app_const.QUERY_REBATELEVEL}_SUCCESS`:
                return UserReducer._query_rebate_level_success(state, action);
            case `${app_const.UPDATE_REBATELEVEL}_SUCCESS`:
                return UserReducer._update_rebate_level_success(state, action);
            case `${app_const.ADD_USER}_SUCCESS`:
            case `${app_const.UPDATE_USER}_SUCCESS`:
            case `${app_const.UPDATE_USER_PASSWORD}_SUCCESS`:
                return UserReducer._common_success(state, action);
            case `${app_const.QUERY_REFERRAL}_SUCCESS`:
                return UserReducer._query_success(state, action, "Referral");
            case `${app_const.GET_USER}_FAILURE`:
            case `${app_const.QUERY_USERS}_FAILURE`:
            case `${app_const.ONLINE_USERS}_FAILURE`:
            case `${app_const.QUERY_AGENT}_FAILURE`:
            case `${app_const.QUERY_LOGINLOG}_FAILURE`:
            case `${app_const.QUERY_ACTIONLOG}_FAILURE`:
            case `${app_const.QUERY_REBATELEVEL}_FAILURE`:
            case `${app_const.UPDATE_REBATELEVEL}_FAILURE`:
            case `${app_const.ADD_USER}_FAILURE`:
            case `${app_const.UPDATE_USER}_FAILURE`:
            case `${app_const.UPDATE_USER_PASSWORD}_FAILURE`:
            case `${app_const.QUERY_REFERRAL}_FAILURE`:
                return Object.assign({}, state, {
                    isFetching: false
                });
            default: return state;
        }
    }

    static _common_success(state, action) {
        const resp = action.response;
        if (resp.code == 0) return state;
        return UserReducer._process_failure(state, action);
    }

    static _get_user_success(state, action) {
        const resp = action.response;
        if (resp.code == 0) {
            const UserInfo = resp.data;
            return Object.assign({}, state, { UserInfo, isFetching: false });
        } else
            return UserReducer._process_failure(state, action);
    }


    static _query_rebate_level_success(state, action) {
        let resp = action.response;
        if (resp.code == 0) {
            return Object.assign({}, state, {
                isFetching: false,
                rebate_levels: resp.data
            });
        }
        else {
            action.error = resp.message;
            action.error_code = resp.code;
            return Object.assign({}, state, {
                isFetching: false
            });
        }
    }

    static _update_rebate_level_success(state, action) {
        const resp = action.response;
        if (resp.code == 0) {
            const level = state.rebate_levels.find(x => x.Level == action.level);
            if (!level) return state;
            var update_index = state.rebate_levels.indexOf(level);

            let update_level = {
                Level: action.level,
                Turnover: action.turnover,
                Backpct: action.backpct
            };

            let rebate_levels = [
                ...state.rebate_levels.slice(0, update_index),
                Object.assign({}, level, update_level),
                ...state.rebate_levels.slice(update_index + 1)
            ];
            return Object.assign({}, state, { rebate_levels });

        } else
            return UserReducer._process_failure(state, action);
    }

    static _process_failure(state, action) {
        const resp = action.response;
        action.error = resp.message;
        action.error_code = resp.code;
        return Object.assign({}, state, {
            isFetching: false
        });
    }

    static _query_referral_success(state, action) {
        let resp = action.response;
        if (resp.code == 0) {
            return Object.assign({}, state, {
                isFetching: false,
                referral: resp.data
            });
        }
        else {
            action.error = resp.message;
            action.error_code = resp.code;
            return Object.assign({}, state, {
                isFetching: false
            });
        }
    }
}

export { UserReducer };