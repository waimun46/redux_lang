import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import JSONTree from 'react-json-tree'
import { message, Row, Col, Table, Tag, Button, Modal, Form, Input, Select, DatePicker } from 'antd';

import BasicComponent from "../common/BasicComponent";
import SearchForm from "../common/SearchForm";

import * as UserAction from '../../actions/user';
import * as RenderHelper from "../../utils/render";
import * as Utils from "../../utils/common";
import * as Enums from "../../app_enums";

const Option = Select.Option;
const RangePicker = DatePicker.RangePicker;
const Message = message;

class ActionLogList extends BasicComponent {

    constructor(props) {
        super(props);
        this.userId = this.getUserId(props);
        this.pageIndex = 1;
        this.query = {};
    }

    /* -------------------------- Sys ------------------------------ */

    componentDidMount() {
        this.queryData();
    }

    componentWillReceiveProps(newProps) {
        if (newProps != this.props) {
            const userId = this.getUserId(newProps);
            if (userId != this.userId && !isNaN(userId)) {
                this.userId = userId;
                this.queryTeam();
            }
        }

    }

    /* -------------------------- Functions ------------------------------ */

    queryData() {
        this.props.dispatch(UserAction.queryActionLog(this.userId, this.pageIndex, this.query));
    }

    getUserId(props) {
        let userId = props.params.userId;
        if (userId)
            userId = parseInt(userId);
        return userId;
    }

    /* -------------------------- Events ------------------------------ */

    onSearchClick(values) {
        this.query = values;
        this.pageIndex = 1;
        this.queryData();
    }

    onResetClick() {
        this.query = {};
    }

    /* -------------------------- Renders ------------------------------ */

    render() {
        const self = this;
        const { isFetching } = this.props;
        const actionLog = this.props.actionLog || {};
        let data = actionLog.data || [];

        const pagination = {
            current: actionLog.pageIndex || 1,
            total: actionLog.count || 0,
            onChange(current) {
                self.pageIndex = current;
                self.queryData();
            },
        };

        return (
            <div>
                {this.renderSearch()}
                <div className="search-result-list">
                    <Table loading={isFetching} columns={this.columns} dataSource={data} rowKey="Id" pagination={pagination} />
                </div>
            </div>
        );
    }

    renderSearch() {
        const formItemLayout = {
            labelCol: { span: 8 },
            wrapperCol: { span: 16 }
        };

        return (
            <SearchForm onSearch={this.onSearchClick.bind(this)} onReset={this.onResetClick.bind(this)}>
                <email label="邮箱" {...formItemLayout}>
                    <Input />
                </email>
                <action label="动作" {...formItemLayout}>
                    <Input />
                </action>
                <target_id label="目标Id" {...formItemLayout}>
                    <Input />
                </target_id>
                <ip label="IP" {...formItemLayout}>
                    <Input />
                </ip>
                <create_time label="时间" {...formItemLayout} format={(times) => Utils.dateRangeFormat(times, 'from_time', 'to_time')}>
                    <DatePicker.RangePicker />
                </create_time>
            </SearchForm>
        );
    }


    get columns() {
        const self = this;
        const columns = [
            { title: '#', render: RenderHelper.render_index },
            { title: "Email", dataIndex: "Email", },
            { title: "动作", dataIndex: "Action" },
            { title: "目标Id", dataIndex: "TargetId" },
            {
                title: "修改数据", dataIndex: "Data", render: it => {
                    return <Button type="primary" onClick={() => Modal.info({ content: <JSONTree data={it} /> })}>查看</Button>
                }
            },
            { title: "备注", dataIndex: "Remark" },
            { title: "IP", dataIndex: "IP" },
            { title: "操作时间", dataIndex: "CreateTime", render: RenderHelper.render_time },
        ];
        return columns;
    }

}


export default connect(state => ({
    isFetching: state.user.isFetching,
    actionLog: state.user.ActionLog,
}))(ActionLogList);