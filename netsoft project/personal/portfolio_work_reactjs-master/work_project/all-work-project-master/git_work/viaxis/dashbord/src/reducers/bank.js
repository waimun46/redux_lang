import * as app_const from "../app_const";

class BankReducer {
    static reduce(state = { isFetching: false }, action) {
        switch (action.type) {
            case app_const.QUERY_BANK:
            case app_const.QUERY_COMPANYBANK:
            case app_const.QUERY_USERBANK:
                return Object.assign({}, state, {
                    isFetching: true
                });
            case `${app_const.QUERY_BANK}_SUCCESS`:
                return BankReducer._query_bank_success(state, action);
            case `${app_const.QUERY_COMPANYBANK}_SUCCESS`:
                return BankReducer._query_success(state, action, "company");
            case `${app_const.QUERY_USERBANK}_SUCCESS`:
                return BankReducer._query_success(state, action, "user");
            case `${app_const.QUERY_BANK}_FAILURE`:
            case `${app_const.QUERY_COMPANYBANK}_FAILURE`:
            case `${app_const.QUERY_USERBANK}_FAILURE`:
                return Object.assign({}, state, {
                    isFetching: false
                });
            case `${app_const.ADD_BANK}_SUCCESS`:
            case `${app_const.UPDATE_BANK}_SUCCESS`:
            case `${app_const.ADD_COMPANYBANK}_SUCCESS`:
            case `${app_const.UPDATE_COMPANYBANK}_SUCCESS`:
            case `${app_const.UPDATE_USERBANK}_SUCCESS`:
                return BankReducer._common_success(state, action);
            default: return state;
        }
    }

    static _common_success(state, action) {
        const resp = action.response;
        if (resp.code == 0) return state;
        return BankReducer._process_failure(state, action);
    }

    static _process_failure(state, action) {
        const resp = action.response;
        action.error = resp.message;
        action.error_code = resp.code;
        return Object.assign({}, state, {
            isFetching: false
        });
    }

    static _query_bank_success(state, action) {
        const resp = action.response;
        if (resp.code == 0) {
            return Object.assign({}, state, {
                isFetching: false,
                banks: resp.data
            });
        } else
            return BankReducer._process_failure(state, action);
    }

    static _query_success(state, action, field_name) {
        const resp = action.response;
        if (resp.code == 0) {
            let data = { isFetching: false };
            data[field_name] = {
                pageIndex: resp.data.PageIndex,
                count: resp.data.TotalCount,
                data: resp.data.Data
            };
            return Object.assign({}, state, data);
        } else
            return BankReducer._process_failure(state, action);
    }

}

export { BankReducer };