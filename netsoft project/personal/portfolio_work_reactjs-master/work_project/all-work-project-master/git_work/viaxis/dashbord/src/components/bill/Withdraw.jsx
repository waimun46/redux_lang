import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Link } from "react-router";
import { message, Row, Col, Table, Button, Modal, Form, Input, Popconfirm, Select, DatePicker } from 'antd';

import SearchForm from "../common/SearchForm";
import EditorModal from "../common/EditorModal";
import BasicComponent from "../common/BasicComponent";

import * as BillAction from "../../actions/bill";
import { queryBank } from "../../actions/bank";

import * as app_const from "../../app_const"
import { WithdrawRequestStatus, SysRights } from "../../app_enums";

import auth, { Rights } from "../../utils/auth";
import * as Utils from "../../utils/common";
import * as RenderHelper from "../../utils/render";


const createForm = Form.create;
const FormItem = Form.Item;
const Option = Select.Option;
const RangePicker = DatePicker.RangePicker;
const Message = message;


class Withdraw extends BasicComponent {

	constructor(props) {
		super(props);
		this.state = { withdrawing: false };
	}

	/* ---------------------------- 系统函数 ------------------------------*/

	/* ---------------------------- Events ------------------------------*/

	handleSubmit(e) {
		const { dispatch } = this.props;
		e.preventDefault();
		this.props.form.validateFields((errors, values) => {
			if (errors) return;
			this.setState({ withdrawing: true });

			dispatch(BillAction.withdraw(values.email, values.amount, values.remark, values.txid)).then(act => {
				this.setState({ withdrawing: false });
				if (act.error) {
					Message.error(act.error);
				} else {
					if (act.response.code == 0) {
						Message.success("提现成功");
						this.props.form.resetFields();
					}
					else
						Message.error(act.response.message);
				}
			});
		});
	}

	handleReset(e) {
		e.preventDefault();
		this.props.form.resetFields();
	}

	/* ---------------------------- Renders ------------------------------*/

	render() {
		const { getFieldDecorator, isFieldValidating, getFieldError } = this.props.form;
		const formItemLayout = {
			labelCol: { span: 6 },
			wrapperCol: { span: 8 },
		};
		return (
			<Form horizontal>
				<FormItem {...formItemLayout} label="用户邮箱" hasFeedback>
					{getFieldDecorator('email', {
						rules: [
							{ required: true, message: '请输入需要提现的用户邮箱' },
							{ type: "email", message: "请输入正确的邮箱" }
						],
					})(
						<Input placeholder="需要提现的用户邮箱" />
					)}
				</FormItem>
				<FormItem {...formItemLayout} label="金额" hasFeedback>
					{getFieldDecorator('amount', {
						rules: [
							{ required: true, whitespace: false, message: '请输入提现金额' },
							{ type: "string", pattern: /^\d+$/, message: "金额必须是数字" }
						],
					})(
						<Input autoComplete="off" />
					)}
				</FormItem>
				<FormItem {...formItemLayout} label="备注">
					{getFieldDecorator('remark', {
						rules: [
							{ required: true, message: '备注必填' },
						],
					})(
						<Input type="textarea" placeholder="手动提现的原因，例如扣款等。" />
					)}
				</FormItem>
				<Form.Item {...formItemLayout} label="流水号">
					{getFieldDecorator('txid', {
						rules: [
							{ required: false, whitespace: false, message: '请输入流水号' },
						],
					})(
						<Input autoComplete="off" placeholder="本次提现流水号,如果不填,则系统自动生成" />
					)}
				</Form.Item>
				<FormItem wrapperCol={{ span: 12, offset: 7 }}>
					<Button type="primary" loading={this.state.withdrawing} onClick={this.handleSubmit.bind(this)}>提交</Button>
					&nbsp;&nbsp;&nbsp;
			          <Button type="ghost" onClick={this.handleReset.bind(this)}>重置</Button>
				</FormItem>
			</Form>
		);
	}
}

class WithdrawList extends BasicComponent {

	constructor(props) {
		super(props);
		this.state = {
			loading: false,
			popup: false,
			popupTitle: "",
			complete: false,
			refuse: false,
			// 当前复制的请求
			copyReq: null,
			// 当前复制的请求项序号
			copyIndex: 0,
		};
		this.query = {};
		this.pageIndex = 1;
	}

	/* ---------------------------- Sys ------------------------------*/

	componentWillMount() {
		this.queryBankData();
		this.queryData();
	}

	componentDidMount() {
		document.addEventListener("keyup", this.handleKeyUp.bind(this), false);
	}

	componentWillUnmount() {
		document.removeEventListener("keyup", this.handleKeyUp, false);
	}

	/* ---------------------------- Functions ------------------------------*/

	queryBankData() {
		this.props.dispatch(queryBank());
	}

	queryData() {
		this.props.dispatch(BillAction.queryWithdraw(this.pageIndex, this.query)).then(act => {
			if (act.error)
				Message.error(act.error);
		});
	}

	Checking(req) {
		this.props.dispatch(BillAction.checkingWithdraw(req.Id)).then(act => {
			if (act.error)
				Message.error(act.error);
			else
				this.queryData();
		});
	}

	Checked(req) {
		this.props.dispatch(BillAction.checkedWithdraw(req.Id)).then(act => {
			if (act.error)
				Message.error(act.error);
			else
				this.queryData();
		});
	}

	Process(req) {
		this.props.dispatch(BillAction.processWithdraw(req.Id)).then(act => {
			if (act.error)
				Message.error(act.error);
			else
				this.queryData();
		});
	}

	Complete(req) {
		this.setState({
			popup: true,
			complete: true,
			refuse: false,
			popupTitle: "完成提现"
		});
		this.request = req;
	}

	Refuse(req) {
		this.setState({
			popup: true,
			complete: false,
			refuse: true,
			popupTitle: "拒绝提现"
		});
		this.request = req;
	}

	copyInfo(content, req, index) {
		Utils.CopyToClipboard(content, () => this.setState({ copyReq: req, copyIndex: index }));
	}

	loopCopyReq() {
		let { copyReq, copyIndex } = this.state;
		if (copyReq == null) return;

		if (copyIndex >= 4)
			copyIndex = 0;
		else
			copyIndex++;

		const copyName = ["Amount", "BankName", "AccountNo", "AccountName", "BranchName"];
		this.copyInfo(copyReq[copyName[copyIndex]], copyReq, copyIndex);
	}

	/* ---------------------------- Events ------------------------------*/

	handleKeyUp(e) {
		e = e || window.event;
		var key = e.which || e.keyCode; // keyCode detection
		var ctrl = e.ctrlKey ? e.ctrlKey : ((key === 17) ? true : false); // ctrl detection

		// Ctrl + c
		if (key == 67 && ctrl) {
			this.loopCopyReq();
		}

	}

	handleSubmit(e) {
		this.props.form.validateFieldsAndScroll((errors, values) => {
			if (errors) {
				return;
			}
			this.setState({ loading: true });
			if (this.state.complete) {
				this.props.dispatch(BillAction.completeWithdraw(this.request.Id, values.txid)).then(act => {
					this.setState({ loading: false });
					if (act.error)
						Message.error(act.error);
					else {
						this.setState({ popup: false });
						this.queryData();
					}
				});
			} else {
				this.props.dispatch(BillAction.refuseWithdraw(this.request.Id, values.remark)).then(act => {
					this.setState({ loading: false });
					if (act.error)
						Message.error(act.error);
					else {
						this.setState({ popup: false });
						this.queryData();
					}
				});
			}

		});
	}

	onSearch(values) {

		if (values.create_time && values.create_time.length == 2) {
			values = Object.assign({}, values, Utils.timeRangeToString(values.create_time, "create_from", "create_to"));
			delete values['create_time'];
		}

		if (values.check_time && values.check_time.length == 2) {
			values = Object.assign({}, values, Utils.timeRangeToString(values.check_time, "check_from", "check_to"));
			delete values['check_time'];
		}

		if (values.operat_time && values.operat_time.length == 2) {
			values = Object.assign({}, values, Utils.timeRangeToString(values.operat_time, "operate_from", "operate_to"));
			delete values['operat_time'];
		}

		this.query = values;
		this.pageIndex = 1;
		this.queryData();
	}

	hideModal() {
		this.setState({ popup: false });
	}

	/* ---------------------------- Renders ------------------------------*/

	tableColumns() {
		const self = this;
		const { copyReq, copyIndex } = this.state;
		const style = { background: "#f00", color: "#fff" };

		const columns = [{ title: '#', render(text, record, index) { return index + 1; } },
		{
			title: '邮箱/用户名', render(it) {
				return (
					<div>
						<Link to={`/user/${it.UserId}`} target="_blank">{it.Email}</Link>
						<div>{it.Username}</div>
					</div>
				);
			}
		},
		{
			title: "金额", dataIndex: "Amount", className: "column-money", render: (it, req) => {
				return (
					<span onClick={() => this.copyInfo(it, req, 0)} style={copyReq == req && copyIndex == 0 ? style : {}}>{RenderHelper.money_format(it)}</span>
				);
			}
		},
		{ title: "余额", dataIndex: "Balance", className: "column-money", render: RenderHelper.money_format },
		{
			title: "银行信息", render(it) {
				return (
					<ul>
						<li onClick={() => self.copyInfo(it.BankName, it, 1)} style={copyReq == it && copyIndex == 1 ? style : {}}>
							{it.BankName}
						</li>
						<li onClick={() => self.copyInfo(it.AccountNo, it, 2)} style={copyReq == it && copyIndex == 2 ? style : {}}>
							{it.AccountNo}
						</li>
						<li onClick={() => self.copyInfo(it.AccountName, it, 3)} style={copyReq == it && copyIndex == 3 ? style : {}}>
							{it.AccountName}
						</li>
						<li onClick={() => self.copyInfo(it.BranchName, it, 4)} style={copyReq == it && copyIndex == 4 ? style : {}}>
							{it.BranchName}
						</li>
					</ul>
				);
			}
		},
		{ title: "流水号", dataIndex: "Txid" },
		{ title: "备注", dataIndex: "Remark" },
		{ title: "状态", dataIndex: "Status", render: RenderHelper.render_status(WithdrawRequestStatus) },
		{ title: '申请时间', dataIndex: 'CreateTime', render: RenderHelper.render_time },
		{
			title: '审核人', render(it) {
				return (
					<div>
						<p>{it.Checker}</p>
						<p>{RenderHelper.render_time(it.CheckTime)}</p>
					</div>
				);
			}
		},
		{
			title: '操作人', render(it) {
				return (
					<div>
						<p>{it.Operator}</p>
						<p>{RenderHelper.render_time(it.OperateTime)}</p>
					</div>
				);
			}
		},
		];

		if (this.hasRights(SysRights.BILL_INSERT)) {
			columns.push({
				title: "操作", fixed: 'right', width: 110, render(req) {
					const isChecker = self.currentUserId == req.CheckerId;
					const isOperator = self.currentUserId == req.OperatorId;
					return <div>
						{req.Status == app_const.WithdrawType.Waiting &&
							<Popconfirm title="你确定要进行审核吗?" onConfirm={() => self.Checking(req)} okText="是" cancelText="否">
								<Button type="primary" size="small">审核</Button>
							</Popconfirm>
						}
						{req.Status == app_const.WithdrawType.Checking && isChecker &&
							<Popconfirm title="你确定通过审核吗?" onConfirm={() => self.Checked(req)} okText="是" cancelText="否">
								<Button type="primary" size="small">通过</Button>
							</Popconfirm>
						}
						{auth.hasRights(Rights.BILL_INSERT) &&
							req.Status == app_const.WithdrawType.Checked &&
							<Popconfirm title="你确定进行出金吗?" onConfirm={() => self.Process(req)} okText="是" cancelText="否">
								<Button type="primary" size="small">出金</Button>
							</Popconfirm>
						}
						{auth.hasRights(Rights.BILL_INSERT) && isOperator &&
							req.Status == app_const.WithdrawType.Withdrawing &&
							<Button type="primary" size="small" onClick={() => self.Complete(req)}>完成</Button>
						}
						&nbsp;
                        {([app_const.WithdrawType.Waiting, app_const.WithdrawType.Checked].indexOf(req.Status) >= 0 ||
							(req.Status < app_const.WithdrawType.Completed && (isChecker || isOperator))) &&
							<Button type="danger" size="small" onClick={() => self.Refuse(req)}>拒绝</Button>
						}
					</div>
				}
			});
		}

		return columns;
	}


	render() {
		const { isFetching } = this.props;
		const withdraws = this.props.withdraws || {};
		const data = withdraws.data || [];
		let columns = this.tableColumns();
		const self = this;
		const pagination = {
			current: withdraws.pageIndex || 1,
			total: withdraws.count || 0,
			onChange(current) {
				self.pageIndex = current;
				self.queryData();
			},
		};

		const { getFieldDecorator } = this.props.form;
		const formItemLayout = {
			labelCol: { span: 7 },
			wrapperCol: { span: 12 },
		};


		return (
			<div>
				{this.renderSearch()}
				<div className="search-result-list">
					<Table columns={columns} dataSource={data} loading={isFetching} pagination={pagination} scroll={{ x: 1400 }} rowKey="Id" />
				</div>
				<Modal title={this.state.popupTitle} visible={this.state.popup}
					footer={[
						<Button key="back" type="ghost" size="large" onClick={this.hideModal.bind(this)}>取消</Button>,
						<Button key="submit" type="primary" size="large" loading={this.state.loading} onClick={this.handleSubmit.bind(this)}>
							提交
            		</Button>,
					]}>
					{this.state.complete && <Form horizontal>
						<FormItem {...formItemLayout} label="流水号">
							{getFieldDecorator('txid', { rules: [{ required: true, message: '请输入本次提现的流水号' }] })(
								<Input type="txid" autoComplete="off" />
							)}
						</FormItem>
					</Form>}

					{this.state.refuse && <Form horizontal>
						<FormItem {...formItemLayout} label="拒绝说明">
							{getFieldDecorator('remark', { rules: [{ required: true, message: '请输入拒绝说明' }] })(
								<Input type="txid" type="textarea" autoComplete="off" />
							)}
						</FormItem>
					</Form>}
				</Modal>
			</div>
		);
	}

	renderSearch() {
		let bank = this.props.bank.banks || [];

		const formItemLayout = {
			labelCol: { span: 8 },
			wrapperCol: { span: 16 }
		};

		return (
			<SearchForm onSearch={this.onSearch.bind(this)}>
				<email label="邮箱" {...formItemLayout}>
					<Input placeholder="Email" />
				</email>
				<amount label="金额" {...formItemLayout}>
					<Input />
				</amount>
				<bank_id label="银行" {...formItemLayout} options={{ initialValue: "" }}>
					<Select notFoundContent="找不到内容">
						<Option value="">全部</Option>
						{bank.map(x => {
							return <Option value={x.Id.toString()} key={x.Id}>{x.Name}</Option>
						})}
					</Select>
				</bank_id>
				<status label="状态" {...formItemLayout} options={{ initialValue: "" }}>
					<Select>
						<Option value="">全部</Option>
						{Object.keys(WithdrawRequestStatus).map(it => {
							return <Option value={it.toString()} key={it}>{WithdrawRequestStatus[it].toString()}</Option>
						})}
					</Select>
				</status>
				<create_time label="申请时间" {...formItemLayout}>
					<RangePicker />
				</create_time>
				<checker label="审核人" {...formItemLayout}>
					<Input placeholder="审核人" />
				</checker>
				<check_time label="审核时间" {...formItemLayout}>
					<RangePicker />
				</check_time>
				<operator label="操作人" {...formItemLayout}>
					<Input placeholder="操作人" />
				</operator>
				<operat_time label="操作时间" {...formItemLayout}>
					<RangePicker />
				</operat_time>

			</SearchForm>
		);
	}
}

export default connect(state => ({
	bill: state.bill,
	data: state.bank,
}))(Form.create()(Withdraw));

const withdrawList = connect(state => ({
	isFetching: state.bill.isFetching,
	withdraws: state.bill.Withdraws,
	bank: state.bank,
}))(Form.create()(WithdrawList));

export { withdrawList as WithdrawList };