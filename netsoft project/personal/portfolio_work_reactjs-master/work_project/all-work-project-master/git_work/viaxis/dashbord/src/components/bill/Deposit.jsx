import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { message, Row, Col, Table, Tag, Button, Modal, Form, Input, InputNumber, Select, DatePicker } from 'antd';

import BasicComponent from "../common/BasicComponent";
import SearchForm from "../common/SearchForm";

import { deposit, completeDeposit, refuseDeposit, queryDeposit } from "../../actions/bill";
import * as RenderHelper from "../../utils/render";
import { DepoisitRequestStatus, DepositType } from "../../app_enums";

const Message = message;

class Deposit extends BasicComponent {

	constructor(props) {
		super(props);
		this.state = { depositing: false };
	}

	handleSubmit(e) {
		const { dispatch } = this.props;
		e.preventDefault();
		this.props.form.validateFields((errors, values) => {
			if (errors) return;
			this.setState({ depositing: true });

			dispatch(deposit(values.email, values.amount, values.remark, values.txid)).then(act => {
				this.setState({ depositing: false });
				var resp = act.response;
				if (resp.code == 0) {
					Message.success("充值成功");
					this.props.form.resetFields();
				} else {
					Message.error(act.error);
				}
			});
		});
	}

	handleReset(e) {
		e.preventDefault();
		this.props.form.resetFields();
	}

	render() {
		const { getFieldDecorator, isFieldValidating, getFieldError } = this.props.form;
		const formItemLayout = {
			labelCol: { span: 6 },
			wrapperCol: { span: 8 },
		};
		return (
			<Form horizontal>
				<Form.Item {...formItemLayout} label="用户邮箱" hasFeedback>
					{getFieldDecorator('email', {
						rules: [
							{ required: true, message: '请输入需要充值的用户邮箱' },
							{ type: "email", message: "请输入正确的邮箱" }
						],
					})(
						<Input placeholder="需要充值的用户邮箱" />
					)}
				</Form.Item>
				<Form.Item {...formItemLayout} label="金额" hasFeedback>
					{getFieldDecorator('amount', {
						rules: [
							{ required: true, whitespace: false, message: '请输入充值金额' },
							{ type: "string", pattern: /^\d+$/, message: "金额必须是数字" }
						],
					})(
						<Input autoComplete="off" />
					)}
				</Form.Item>
				<Form.Item {...formItemLayout} label="备注">
					{getFieldDecorator('remark', {
						rules: [
							{ required: true, message: '备注必填' },
						],
					})(
						<Input type="textarea" placeholder="手动充值的原因，例如返利、奖励等。" />
					)}
				</Form.Item>
				<Form.Item {...formItemLayout} label="流水号">
					{getFieldDecorator('txid', {
						rules: [
							{ required: false, whitespace: false, message: '请输入流水号' },
						],
					})(
						<Input autoComplete="off" placeholder="本次充值流水号,如果不填,则系统自动生成" />
					)}
				</Form.Item>
				<Form.Item wrapperCol={{ span: 12, offset: 7 }}>
					<Button type="primary" loading={this.state.depositing} onClick={this.handleSubmit.bind(this)}>提交</Button>
					&nbsp;&nbsp;&nbsp;
			          <Button type="ghost" onClick={this.handleReset.bind(this)}>重置</Button>
				</Form.Item>
			</Form>
		);
	}
}

class DepositList extends BasicComponent {

	constructor(props) {
		super(props);
		this.state = {
			loading: false,
			popup: false,
			popupTitle: "",
			complete: false,
			refuse: false
		};
		this.query = {};
		this.pageIndex = 1;
	}

	/* -------------------- Sys ------------------*/

	componentWillMount() {
		this.queryData();
	}

	/* -------------------- Functions ------------------------ */

	queryData() {
		this.props.dispatch(queryDeposit(this.pageIndex, this.query)).then(act => {
			if (act.error)
				Message.error(act.error);
		});
	}

	completeReq(req) {
		this.request = req;
		this.setState({ popup: true, popupTitle: "完成本次充值", complete: true, refuse: false });
	}

	refuseReq(req) {
		this.request = req;
		this.setState({ popup: true, popupTitle: "拒绝本次充值", complete: false, refuse: true });
	}

	tableColumns() {
		const self = this;
		const columns = [{ title: '#', render(text, record, index) { return index + 1; } },
		{ title: '邮箱', dataIndex: 'Email' },
		{ title: "金额", dataIndex: "Amount", className: "column-money", render: RenderHelper.money_format },
		{ title: "备注", dataIndex: "Remark" },
		{ title: "状态", dataIndex: "Status", render: RenderHelper.render_status(DepoisitRequestStatus) },
		{ title: '时间', dataIndex: 'CreateTime', render: RenderHelper.render_time },
		{
			title: "操作", render(req) {
				return req.Status == 0 && <div>
					<Button type="primary" onClick={() => self.completeReq(req)}>完成</Button>
					&nbsp;&nbsp;
            			<Button onClick={() => self.refuseReq(req)}>拒绝</Button>
				</div>
			}
		}];

		return columns;
	}

	/* ------------------------ Events -------------------------- */

	onSearchClick(query) {
		this.query = query;
		this.queryData();
	}

	onResetClick() {
		this.query = {};
	}

	handleSubmit(e) {
		this.props.form.validateFieldsAndScroll((errors, values) => {
			if (errors) {
				return;
			}
			this.setState({ loading: true });
			if (this.state.complete) {
				this.props.dispatch(completeDeposit(this.request.Id, values.txid, this.request.Amount, values.deposit_type)).then(act => {
					this.setState({ loading: false });
					if (act.error)
						Message.error(act.error);
					else {
						this.setState({ popup: false });
						this.queryData();
					}
				});
			} else {
				this.props.dispatch(refuseDeposit(this.request.Id, values.remark)).then(act => {
					this.setState({ loading: false });
					if (act.error)
						Message.error(act.error);
					else {
						this.setState({ popup: false });
						this.queryData();
					}
				});
			}

		});
	}

	hideModal() {
		this.setState({ popup: false });
	}

	/* ------------------------- Renders ------------------------- */

	render() {
		const { isFetching } = this.props;
		const columns = this.tableColumns();

		const deposits = this.props.deposits || {};
		const data = deposits.data || [];

		const self = this;
		const pagination = {
			current: deposits.pageIndex || 0,
			total: deposits.count || 0,
			onChange(current) {
				self.pageIndex = current;
				self.queryData();
			},
		};

		const { getFieldDecorator } = this.props.form;
		const formItemLayout = {
			labelCol: { span: 7 },
			wrapperCol: { span: 12 },
		};

		return (
			<div>
				{this.renderSearch()}
				<div className="search-result-list">
					<Table columns={columns} dataSource={data} loading={isFetching} pagination={pagination} rowKey="Id" />
				</div>
				<Modal title={this.state.popupTitle} visible={this.state.popup}
					footer={[
						<Button key="back" type="ghost" size="large" onClick={this.hideModal.bind(this)}>取消</Button>,
						<Button key="submit" type="primary" size="large" loading={this.state.loading} onClick={this.handleSubmit.bind(this)}>
							提交
            		</Button>,
					]}>
					{this.state.complete && <Form horizontal>
						<Form.Item {...formItemLayout} label="流水号">
							{getFieldDecorator('txid', { rules: [{ required: true, message: '请输入本次充值的流水号' }] })(
								<Input type="txid" autoComplete="off" />
							)}
						</Form.Item>
						<Form.Item {...formItemLayout} label="充值类型">
							{getFieldDecorator('deposit_type', { initialValue: "1" })(
								<Select>
									{Object.keys(DepositType).map(it => {
										return <Select.Option key={it} value={it}>{DepositType[it]}</Select.Option>;
									})}
								</Select>
							)}
						</Form.Item>
					</Form>}

					{this.state.refuse && <Form horizontal>
						<Form.Item {...formItemLayout} label="拒绝说明">
							{getFieldDecorator('remark', { rules: [{ required: true, message: '请输入拒绝说明' }] })(
								<Input type="txid" type="textarea" autoComplete="off" />
							)}
						</Form.Item>
					</Form>}
				</Modal>
			</div>
		);
	}

	renderSearch() {
		const formItemLayout = {
			labelCol: { span: 8 },
			wrapperCol: { span: 16 }
		};

		return (
			<SearchForm onSearch={this.onSearchClick.bind(this)} onReset={this.onResetClick.bind(this)}>
				<email label="邮箱" {...formItemLayout}>
					<Input />
				</email>
				<status label="状态" {...formItemLayout} options={{ initialValue: "" }}>
					<Select>
						<Select.Option value="">全部</Select.Option>
						{Object.keys(DepoisitRequestStatus).map(it => {
							return <Select.Option value={it.toString()} key={it}>{DepoisitRequestStatus[it].name}</Select.Option>
						})}
					</Select>
				</status>
				<create_time label="时间" {...formItemLayout}>
					<DatePicker.RangePicker />
				</create_time>
			</SearchForm>
		);
	}
}

export default connect(state => ({
	bill: state.bill,
}))(Form.create()(Deposit));

const depositList = connect(state => ({
	isFetching: state.bill.isFetching,
	deposits: state.bill.Deposits
}))(Form.create()(DepositList));

export { depositList as DepositList };