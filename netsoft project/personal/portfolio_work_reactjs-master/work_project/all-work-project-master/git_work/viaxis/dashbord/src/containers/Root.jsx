import React, { Component } from 'react'
import { Provider } from 'react-redux'
import { browserHistory, Router } from 'react-router';
import routes from "../routes";

import configureStore from '../configure_store'

const store = configureStore()

export default class Root extends Component {
  render() {
    return (
      <Provider store={store}>
    	<Router history={browserHistory} routes={routes} />
      </Provider>
    )
  }
}