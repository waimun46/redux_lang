import * as app_const from "../app_const";
import ReduceBase from "./reduce_base";
import * as Utils from "../utils/common";

class ArticleReducer extends ReduceBase {
    static reduce(state = { isFetching: false }, action) {
        switch (action.type) {
            case app_const.QUERY_ARTICLE_CATEGORY:
            case app_const.QUERY_ARTICLE:
                return Object.assign({},state,{isFetching:true});
            case `${app_const.QUERY_ARTICLE_CATEGORY}_SUCCESS`:
                return ArticleReducer._list_success(state, action, "Category");
            case `${app_const.QUERY_ARTICLE}_SUCCESS`:
                return ArticleReducer._query_success(state, action, "Article");
            case `${app_const.QUERY_ARTICLE_CATEGORY}_FAILURE`:
            case `${app_const.QUERY_ARTICLE}_FAILURE`:
                return Object.assign({}, state, {
                    isFetching: false
                });
            default:
                return state;
        }
    }

}

export { ArticleReducer };