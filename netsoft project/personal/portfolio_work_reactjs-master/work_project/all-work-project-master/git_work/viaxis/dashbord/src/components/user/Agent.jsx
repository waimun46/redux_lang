import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Link } from "react-router";

import { Breadcrumb, message, Row, Col, Table, InputNumber, Button, Form, Input, Select, DatePicker, Switch } from 'antd';

import BasicComponent from "../common/BasicComponent";
import SearchForm from "../common/SearchForm";
import OperateUserForm from "../user/OperateUserForm";

import * as UserAction from '../../actions/user';
import * as Utils from "../../utils/common";
import * as RenderHelper from "../../utils/render";
import * as Enums from "../../app_enums";
import auth, { Rights } from "../../utils/auth";
import * as Validators from "../../utils/validator";

class Agent extends BasicComponent {

    constructor(props) {
        super(props);
        this.userId = this.getUserId(props);
        this.pageIndex = 1;
        this.querys = {};
    }

    /* ----------------------------- Sys --------------------------------- */

    componentWillMount() {
        this.queryData();
    }

    componentWillReceiveProps(newProps) {
        if (newProps != this.props) {
            const userId = this.getUserId(newProps);
            if (userId != this.userId && !isNaN(userId)) {
                this.userId = userId;
                this.pageIndex = 1;
                this.queryTeam();
            }
        }

    }

    /* ----------------------------- Function --------------------------------- */

    queryTeam() {
        if (this.userId)
            this.props.dispatch(UserAction.getParents(this.userId));
        this.props.dispatch(UserAction.getTeam(this.userId, this.pageIndex));
    }

    queryData() {
        if (this.isNullOrEmpty(this.querys))
            this.queryTeam();
        else
            this.props.dispatch(UserAction.queryTeam(this.pageIndex, this.querys));
    }


    getUserId(props) {
        let userId = props.params.userID;
        if (userId)
            userId = parseInt(userId);
        return userId;
    }

    isNullOrEmpty(values) {
        let isNull = true;
        try {
            Object.keys(values).forEach(it => {
                if (!Utils.isEmptyOrNull(values[it])) {
                    isNull = false;
                    throw "break";
                }
            });
        } catch (e) {
            if (e === "break") return;
            else
                throw e;
        }
        return isNull;
    }

    get parents() {
        let parents = this.props.parents || null;
        if (parents == null || parents.length == 0)
            parents = [{ UserId: 1, UserKind: 0, Email: "Company", Backpct: 0.085 }];
        return parents;
    }

    /* ----------------------------- Event --------------------------------- */

    onSearchClick(values) {
        this.querys = values;
        this.queryData();
    }

    onResetClick() {
        this.querys = {};
    }

    onEditUserClick(user) {
        if (!user) return;

        if (!auth.hasRights(Rights.UCENTER_UPDATE)) return;

        const modal = (user, parent) => RenderHelper.modal(<OperateUserForm user={user} parent={parent} isadd={false} modifypwd={true} onSubmit={(user, values) => this.onEditSubmit(user, values)} />);

        const hide = message.loading('Loading...', 0);
        this.props.dispatch(UserAction.getUser(user.Aid)).then(act => {
            hide();
            const parent = act.response.data;
            this._dialog = modal(user, parent);
        });
    }

    onEditSubmit(user, values) {
        return this.props.dispatch(UserAction.updateUser(user.UserId, values))
            .then(act => {
                const resp = act.response;
                if (resp.code == 0) {
                    if (this._dialog) {
                        this._dialog.destroy();
                        this._dialog = null;
                        this.queryData()
                    }
                }
                return act;
            });
    }

    /* ----------------------------- Renders --------------------------------- */

    render() {
        const self = this;
        const { isFetching } = this.props;
        const team = this.props.team || {};
        let data = team.data || [];

        const pagination = {
            current: team.pageIndex || 1,
            total: team.count || 0,
            onChange(current) {
                self.pageIndex = current;
                self.queryData();
            },
        };

        return (
            <div>
                {this.renderSearch()}
                {this.renderBreadcrumb()}
                <div className="search-result-list">
                    <Table loading={isFetching} columns={this.columns} dataSource={data} rowKey="UserId" pagination={pagination} />
                </div>
            </div>
        );
    }

    renderBreadcrumb() {
        let parents = this.parents;

        if (parents == null)
            parents = []
        return (
            <Breadcrumb separator=">">
                {parents.map((it, i) => <Breadcrumb.Item key={i}><Link to={`/agent/${it.UserId}`}>[{Enums.UserKind[it.UserKind]}]{it.Email}</Link></Breadcrumb.Item>)}
            </Breadcrumb>
        );
    }

    renderSearch() {
        const formItemLayout = {
            labelCol: { span: 8 },
            wrapperCol: { span: 16 }
        };

        return (
            <SearchForm onSearch={this.onSearchClick.bind(this)} onReset={this.onResetClick.bind(this)}>
                <email label="邮箱" {...formItemLayout}>
                    <Input />
                </email>
                <user_kind label="用户类型" {...formItemLayout} options={{ initialValue: "" }}>
                    <Select>
                        <Select.Option value="">全部</Select.Option>
                        {Object.keys(Enums.UserKind).map(it => <Select.Option key={it} value={it.toString()}>{Enums.UserKind[it]}</Select.Option>)}
                    </Select>
                </user_kind>
                <backpct label="返点" {...formItemLayout}>
                    <Input />
                </backpct>
                <create_time label="时间" {...formItemLayout} format={(times) => Utils.dateRangeFormat(times, 'from_time', 'to_time')}>
                    <DatePicker.RangePicker />
                </create_time>
            </SearchForm>
        );
    }

    get columns() {
        const self = this;
        const columns = [
            { title: '#', render: RenderHelper.render_index },
            {
                title: "用户",
                dataIndex: "Email",
                render(email, record, index) {
                    if (record.UserKind <= 3) {
                        const url = `/agent/${record.UserId}`;
                        return (<Link to={url}>{email} </Link>);
                    } else
                        return email;
                }
            },
            { title: "备注", dataIndex: "Remark" },
            { title: "用户类型", dataIndex: "UserKind", render: RenderHelper.render_enum(Enums.UserKind) },
            { title: "团队人数", dataIndex: "ChildCount", render(count) { return count + 1; } },
            { title: "团队余额", dataIndex: "Balance", className: 'column-money', render: RenderHelper.money_format_color },
            { title: "返点", dataIndex: "Backpct", className: 'column-money', render: RenderHelper.render_percent() },
            { title: "手机号码", dataIndex: "Mobile" },
            { title: "QQ", dataIndex: "QQ" },
            { title: "状态", dataIndex: "Status", render: (status) => RenderHelper.render_status(Enums.UserStatus, status) },
            { title: "注册时间", dataIndex: "CreateTime", render: RenderHelper.render_time },
            {
                title: "操作", render: (user) => {
                    return (
                        <span>
                            <Button type="primary" shape="circle" icon="edit" onClick={() => this.onEditUserClick(user)}></Button>
                        </span>
                    );
                }
            }
        ];
        return columns;
    }
}

export default connect(state => ({
    isFetching: state.user.isFetching,
    team: state.user.Team,
    parents: state.user.Parents
}))(Agent);
