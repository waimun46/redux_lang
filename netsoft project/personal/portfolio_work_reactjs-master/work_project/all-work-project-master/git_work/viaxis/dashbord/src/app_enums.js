/**
 * 状态类。
 * 实例包含三个字段，分别是:
 * name   : 显示的名称
 * value  : 状态值
 * status : 状态 (用于赋给Badge或自己实现Status显示方法。)
 * 本类主要用于状态枚举，并搭配RenderHelper.render_status使用。
 */
class Status {

	constructor(name, value, status) {
		this.name = name;
		this.value = value;
		this.status = status;
	}

	toString() {
		return this.name;
	}
}

export const SysRights = {
	UC_SELECT: "ucenter.select",
	UC_INSRET: "ucenter.insert",
	UC_UPDATE: "ucenter.update",
	ADMIN_SELECT: "admin.select",
	ADMIN_INSERT: "admin.insert",
	ADMIN_UPDATE: "admin.update",
	BILL_SELECT: "bill.select",
	BILL_INSERT: "bill.insert",
	BILL_UPDATE: "bill.update",
	GAME_SELECT: "game.select",
	GAME_INSERT: "game.update",
	GAME_UPDATE: "game.update",
	CMS_INSERT: "cms.insert",
	CMS_UPDATE: "cms.update",
};


export const CommonStatus = {
	0: "启用",
	1: "禁用"
};

export const CommonStyleStatus = {
	0: new Status("启用", 0, "processing"),
	1: new Status("禁用", 1, "error"),
};

export const UserType = {
	0: "真实",
	1: "试玩"
};

export const UserKind = {
	0: "公司",
	1: "大股东",
	2: "股东",
	3: "代理",
	4: "会员",
	5: "子用户"
}

export const UserStatus = {
	0: new Status("正常", 0, "processing"),
	1: new Status("暂停", 1, "default"),
	2: new Status("禁用", 2, "error")
};

export const ClientType = {
	0: "未知",
	1: "PC",
	2: "手机",
	3: "App"
};

export const BillFlowType = {
	0: "充值",
	1: "提现",
	2: "转账",
	3: "游戏",
	4: "佣金",
	5: "奖金"
};

export const DepositType = {
	0: "手动",
	1: "第三方充值",
	2: "读卡"
};

export const WithdrawType = {
	0: "手动",
	1: "网银"
};

export const TransferType = {
	0: "转入",
	1: "转出"
};

export const BonuSubType = {
	0: "日工资",
	1: "红利"
};

export const GameSubType = {
	0: "投注",
	1: "盈亏",
	2: "取消"
};

export const DepoisitRequestStatus = {
	0: new Status("充值中", 0, "processing"),
	1: new Status("完成", 1, "success"),
	2: new Status("失败", 2, "error"),
};


export const WithdrawRequestStatus = {
	0: new Status("待审核", 0, "default"),
	1: new Status("审核中", 1, "processing"),
	2: new Status("已审核", 2, "default"),
	3: new Status("出金中", 3, "processing"),
	4: new Status("完成", 4, "success"),
	5: new Status("拒绝", 5, "error"),
	6: new Status("取消", 6, "warning"),
};

export const BankType = {
	0: "银行",
	1: "第三方支付",
	2: "扫码支付"
};

export const GameDataStatus = {
	0: new Status("未开奖", 0, "default"),
	1: new Status("已开奖", 1, "success"),
	2: new Status("取消", 2, "error")
};

export const DealStatus = {
	0: new Status("未开奖", 0, "processing"),
	1: new Status("已结算", 1, "success"),
	2: new Status("计盈亏", 2, "warning"),
	3: new Status("已计佣", 3, "success"),
	4: new Status("待取消", 4, "default"),
	5: new Status("已取消", 5, "error")
}

export const WinLoss = {
	0: "和",
	1: "胜",
	2: "负"
};

export const PlayLimitType = {
	0: "单注",
	1: "单期",
	2: "单日"
};

export const UserProfitStatus = {
	0: "未派奖",
	1: "已派奖"
};

export const ProfitDividendStatus = {
	0: new Status("未发放", 0, "default"),
	1: new Status("已发放", 1, "success"),
};

export const GameGroup = {
	1: "真人彩",
	2: "重庆时时彩",
	3: "北京快乐8",
	4: "快乐十分",
	5: "Keno",
	6: "排列3",
	7: "分分彩",
	8: "真人彩",
	9: "PK拾"
};

export const BulletinType = {
	0: "一般",
	1: "紧急"
};

export const BulletinStatusType = {
	0: "启用",
	1: "禁用"
};

export const MailMsgStatus = {
	0: new Status("发送中", 0, "processing"),
	1: new Status("发送成功", 1, "success"),
	2: new Status("发送失败", 2, "error")
};

export const BulletinStatus = {
	0: new Status("启用", 0, "processing"),
	1: new Status("禁用", 1, "error"),
};