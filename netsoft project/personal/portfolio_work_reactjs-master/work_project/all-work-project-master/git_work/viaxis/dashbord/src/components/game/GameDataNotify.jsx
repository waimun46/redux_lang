import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Link } from "react-router";
import { notification, Badge, Icon } from 'antd';
import Sound from "react-sound";

import BasicComponent from "../common/BasicComponent";
import SearchForm from "../common/SearchForm";

import * as GameAction from "../../actions/game";
import * as RenderHelper from "../../utils/render";
import * as Enums from "../../app_enums";


class GameDataNotify extends BasicComponent {

    constructor(props) {
        super(props);
        this.state = {
            playStatus: Sound.status.PLAYING
        };
    }

    /* --------------- Sys --------------- */

    componentWillMount() {
        this.getWithdrawCount();
    }

    componentDidMount() {

        if (this.interval) {
            clearInterval(this.interval);
            this.interval = null;
        }
        this.interval = setInterval(this.getWithdrawCount.bind(this), 30000);

    }

    componentWillUnmount() {
        if (this.interval) {
            clearInterval(this.interval);
            this.interval = null;
        }
    }

    /* --------------- Function --------------- */

    getWithdrawCount() {
        this.props.dispatch(GameAction.countFinishedUnOpenGameData());
    }

    /* --------------- Event ------------------- */
    onFinishedPlaying() {
        this.setState({ playStatus: Sound.status.PLAYING });
    }

    /* --------------- Render ------------------ */
    render() {
        const finishedUnopen = this.props.finishedUnopen || [];
        const unopenCount = finishedUnopen.length ? finishedUnopen.map(it => it.Count).reduce((a, b) => a + b) : 0;
        return (
            <Link to="/game/data?finished=true&status=0">
                <Badge count={unopenCount}>
                    <Icon type="frown-o" />
                </Badge>
                {
                    unopenCount &&
                    <Sound
                        url="/static/media/unopen.mp3"
                        playStatus={this.state.playStatus}
                        playFromPosition={300 /* in milliseconds */}
                        onFinishedPlaying={this.onFinishedPlaying.bind(this)} />
                }
            </Link>
        );
    }
}

export default connect(state => ({
    finishedUnopen: state.game.FinishedUnopen,
}))(GameDataNotify);