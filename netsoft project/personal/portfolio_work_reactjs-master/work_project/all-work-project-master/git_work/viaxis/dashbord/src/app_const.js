export const WithdrawType = {
    Waiting: 0,
    Checking: 1,
    Checked: 2,
    Withdrawing: 3,
    Completed: 4,
    Refuse: 5,
    Cancel: 6
};

// Role
export const LOAD_ROLES="LOAD_ROLES";
export const ADD_ROLE = "ADD_ROLE";

// Right
export const LOAD_RIGHTS="LOAD_RIGHTS";
export const UPDATE_RIGHTS = "UPDATE_RIGHTS";

// Role Rights

export const GET_ROLE_RIGHTS = "GET_ROLE_RIGHTS";
export const ADD_RIGHTS = "ADD_RIGHTS";
export const UPDATE_ROLE_RIGHTS = "UPDATE_ROLE_RIGHTS";

// Role User
export const LOAD_ROLE_USERS = "LOAD_ROLE_USERS";
export const ADD_ROLE_USER = "ADD_ROLE_USER";
export const REMOVE_ROLE_USER = "REMOVE_ROLE_USER";

// User
export const GET_USER = "GET_USER";
export const ONLINE_USERS = "ONLINE_USERS";
export const QUERY_USERS = "QUERY_USERS";
export const QUERY_LOGINLOG = "QUERY_LOGINLOG";
export const QUERY_ACTIONLOG = "QUERY_ACTIONLOG";
export const QUERY_TEAM = "QUERY_TEAM";
export const GET_PARENT = "GET_PARENT";
export const ADD_USER = "ADD_USER";
export const UPDATE_USER = "UPDATE_USER";
export const UPDATE_USER_PASSWORD = "UPDATE_USER_PASSWORD";


// Rebate Level
export const QUERY_REBATELEVEL = "QUERY_REBATELEVEL";
export const UPDATE_REBATELEVEL = "UPDATE_REBATELEVEL";
// Bill Deposit
export const MANUAL_DEPOSIT = "MANUAL_DEPOSIT";
export const COMPLETE_DEPOSIT = "COMPLETE_DEPOSIT";
export const REFUSE_DEPOSIT = "REFUSE_DEPOSIT";
export const QUERY_DEPOSIT = "QUERY_DEPOSIT";
export const COUNT_DEPOSIT = "COUNT_DEPOSIT";
// Bill Withdraw
export const MANUAL_WITHDRAW = "MANUAL_WITHDRAW";
export const CHECKING_WITHDRAW = "CHECKING_WITHDRAW";
export const CHECKED_WITHDRAW = "CHECKED_WITHDRAW";
export const PROCESS_WITHDRAW = "PROCESS_WITHDRAW";
export const COMPLETE_WITHDRAW = "COMPLETE_WITHDRAW";
export const REFUSE_WITHDRAW = "REFUSE_WITHDRAW";
export const QUERY_WITHDRAW = "QUERY_WITHDRAW";
export const COUNT_WITHDRAW = "COUNT_WITHDRAW";

// Bill Transfer
export const TRANSFER = "TRANSFER";
export const QUERY_TRANSFER = "QUERY_TRANSFER";

// Bill Flow
export const QUERY_BILLFLOW = "QUERY_BILLFLOW";
export const SUM_BILLFLOW = "SUM_BILLFLOW";
// User Balance
export const QUERY_USER_BALANCE = "QUERY_USER_BALANCE";

// Dividend Scale
export const LIST_DIVIDEND_SCALE = "LIST_DIVIDEND_SCALE";
export const ADD_DIVIDEND_SCALE = "ADD_DIVIDEND_SCALE";
export const UPDATE_DIVIDEND_SCALE = "UPDATE_DIVIDEND_SCALE";

// Bank
export const ADD_BANK = "ADD_BANK";
export const UPDATE_BANK = "UPDATE_BANK";
export const QUERY_BANK = "QUERY_BANK";

// Company Bank
export const ADD_COMPANYBANK = "ADD_COMPANYBANK";
export const UPDATE_COMPANYBANK = "UPDATE_COMPANYBANK";
export const QUERY_COMPANYBANK = "QUERY_COMPANYBANK";

// User Bank
export const UPDATE_USERBANK = "UPDATE_USERBANK";
export const QUERY_USERBANK = "QUERY_USERBANK";
// Game Group
export const QUERY_GAMEGROUP = "QUERY_GAMEGROUP";
// Game Type
export const QUERY_GAMETYPE = "QUERY_GAMETYPE";
export const QUERY_GAMETYPE_WITHOUTINFO = "QUERY_GAMETYPE_WITHOUTINFO";
export const ADD_GAMETYPE = "ADD_GAMETYPE";
export const UPDATE_GAMETYPE = "UPDATE_GAMETYPE";

// Game Type Info
export const QUERY_GAMETYPEINFO = "QUERY_GAMETYPEINFO";
export const ADD_GAMETYPEINFO = "ADD_GAMETYPEINFO";
export const UPDATE_GAMETYPEINFO = "UPDATE_GAMETYPEINFO";

// Game Type Category
export const QUERY_GAMETYPECATEGORY = "QUERY_GAMETYPECATEGORY"
export const ADD_GAMETYPECATEGORY = "ADD_GAMETYPECATEGORY";
export const UPDATE_GAMETYPECATEGORY = "UPDATE_GAMETYPECATEGORY";

// Game Type Sort
export const QUERY_GAMETYPESORT = "QUERY_GAMETYPESORT";
export const ADD_GAMETYPESORT = "ADD_GAMETYPESORT";
export const UPDATE_GAMETYPESORT = "UPDATE_GAMETYPESORT";

// Game Type Kind
export const QUERY_GAMETYPEKIND = "QUERY_GAMETYPEKIND";
export const ADD_GAMETYPEKIND = "ADD_GAMETYPEKIND";
export const UPDATE_GAMETYPEKIND = "UPDATE_GAMETYPEKIND";

// Game Algorithm
export const QUERY_GAMEALGORITHM = "QUERY_GAMEALGORITHM";
export const UPDATE_GAMEALGORITHM = "UPDATE_GAMEALGORITHM";

// Game Data
export const QUERY_GAMEDATA = "QUERY_GAMEDATA";
export const GET_GAMEDATA = "GET_GAMEDATA";
export const OPEN_GAME_DATA = "OPEN_GAME_DATA";
export const CANCEL_GAME_DATA = "CANCEL_GAME_DATA";
export const COUNT_FINISHED_UNOPEN_GAMEDATA = "COUNT_FINISHED_UNOPEN_GAMEDATA";
// DealInfo
export const QUERY_DEALINFO = "QUERY_DEALINFO";
export const CANCEL_DEALINFO = "CANCEL_DEALINFO";
// PlayLimit
export const ADD_PLAYLIMIT = "ADD_PLAYLIMIT";
export const UPDATE_PLAYLIMIT = "UPDATE_PLAYLIMIT";
export const QUERY_PLAYLIMIT = "QUERY_PLAYLIMIT";
// Rebate Report
export const QUERY_REBATEREPORT = "QUERY_REBATEREPORT";
// Platform Statistic
export const QUERY_PLATFORM_STATISTIC = "QUERY_PLATFORM_STATISTIC";
// User Profit
export const QUERY_PROFIT_USER = "QUERY_PROFIT_USER";
// Game Type Profit
export const QUERY_PROFIT_GAME_TYPE = "QUERY_PROFIT_GAME_TYPE";
export const QUERY_PROFIT_GAME_TYPE_BY_USER = "QUERY_PROFIT_GAME_TYPE_BY_USER";
// Dividend Profit
export const QUERY_PROFIT_DIVIDEND = "QUERY_PROFIT_DIVIDEND";
// Article
export const GET_ARTICLE = "GET_ARTICLE";
export const QUERY_ARTICLE_CATEGORY = "QUERY_ARTICLE_CATEGORY";
export const ADD_ARTICLE_CATEGORY = "ADD_ARTICLE_CATEGORY";
export const UPDATE_ARTICLE_CATEGORY = "UPDATE_ARTICLE_CATEGORY";

export const QUERY_ARTICLE = "QUERY_ARTICLE";
export const ADD_ARTICLE = "ADD_ARTICLE";
export const UPDATE_ARTICLE = "UPDATE_ARTICLE";

// Bulletin
export const QUERY_BULLETIN = "QUERY_BULLETIN";
export const GET_BULLETIN = "GET_BULLETIN";
export const ADD_BULLETIN = "ADD_BULLETIN";
export const UPDATE_BULLETIN = "UPDATE_BULLETIN";
// MailMsg
export const QUERY_MAILMSG = "QUERY_MAILMSG";
// ConfigInfo
export const QUERY_CONFIG = "QUERY_CONFIG";
export const ADD_CONFIG = "ADD_CONFIG";
export const UPDATE_CONFIG = "UPDATE_CONFIG";

//referral
export const QUERY_REFERRAL = "QUERY_REFERRAL"


//投注号码相关
/**
 * 单位位置分割符（服务端），用于需要指定投注号码所占的位置与投注号码之间的分割。
 * 例如 01110?234,834 此号码表示千百十位投234，834两注。
 * @type {String}
 */
export const UNIT_POSITION_SPLIT_CHAR = "?";
/**
 * 每注之间的分割符（服务端）。例如134&345&643代表三注
 * @type {String}
 */
export const MULTIPLE_NUMS_SPLIT_CHAR = "&";
/**
 * 通用数位的默认名称列表
 * @type {Array}
 */
export const DIGIT_UNIT_NAME_LIST = ["万位", "千位", "百位", "十位", "个位"];

export const SUM_NUMS = {
    "大": "BG",
    "小": "SM",
    "单": "OD",
    "双": "EV",
    "前大": "FBG",
    "后大": "BBG",
    "和": "DRW"
};

/**
 * 佣金说明(团队添加、修改下级佣金说明)
 * @type {String}
 */
export const COMMISSION_EXPLAIN = "您可以将您拥有的佣金下发给下级代理或会员，假如您有6.0%佣金，下发给会员5.0%，每当会员投注100，您可以获得1元佣金。向对应的，会员的佣金越高，他的赔率就越高。";
/**
 * 日工资说明(团队添加、修改下级日工资说明)
 * @type {String}
 */
export const DAILY_WAGES_EXPLAIN = "您可以将您的日工资分发给下级代理，日工资每天凌晨结算，当日的日工资为下级有效投注 x 日工资比例。";