import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { message, Row, Col, Table, Tag, Button, Modal, Form, Input, Select, DatePicker } from 'antd';

import BasicComponent from "../common/BasicComponent";
import SearchForm from "../common/SearchForm";

import * as ArticleAction from '../../actions/article';
import * as RenderHelper from "../../utils/render";
import * as Enums from "../../app_enums";

class ArticleList extends BasicComponent {

    constructor(props) {
        super(props);
        this.query = {};
        this.pageIndex = 1;
    }

    /* ----------------------- Sys ----------------------- */

    componentWillMount() {
        this.queryData();
    }

    /* ----------------------- Functions ---------------------- */

    queryData() {
        this.props.dispatch(ArticleAction.queryArticle(this.pageIndex, this.query));
    }

    /* ------------------------ Events ------------------------ */

    onSearchClick(query) {
        this.pageIndex = 1;
        this.query = query;
        this.queryData();
    }

    onResetClick() {
        this.query = {};
    }

    onAddClick() {
        this.goto("/cms/article/add");
    }


    /* ------------------------ Renders ------------------------ */

    render() {
        const { isFetching } = this.props;

        const article = this.props.article || {};
        const data = article.data || [];

        const self = this;
        const pagination = {
            current: article.pageIndex || 1,
            total: article.count || 0,
            onChange(current) {
                self.pageIndex = current;
                self.queryData();
            },
        };

        return (
            <div>
                {this.renderSearch()}
                <div className="search-result-list">
                    <Table columns={this.columns} dataSource={data} loading={isFetching} pagination={pagination} rowKey="Id" />
                </div>
            </div>
        );
    }

    renderSearch() {
        const formItemLayout = {
            labelCol: { span: 8 },
            wrapperCol: { span: 16 }
        };

        return (
            <SearchForm showAddBtn={this.hasRights(Enums.SysRights.CMS_INSERT)} onAdd={this.onAddClick.bind(this)} onSearch={this.onSearchClick.bind(this)} onReset={this.onResetClick.bind(this)}>
                <name label="名称" {...formItemLayout}>
                    <Input />
                </name>
                <label label="标签" {...formItemLayout}>
                    <Input />
                </label>
                <status label="状态" {...formItemLayout} options={{ initialValue: "" }}>
                    <Select>
                        <Select.Option value="">全部</Select.Option>
                        {Object.keys(Enums.CommonStatus).map(it => <Select.Option value={it.toString()} key={it}>{Enums.CommonStatus[it]}</Select.Option>)}
                    </Select>
                </status>
                <create_time label="时间" {...formItemLayout}>
                    <DatePicker.RangePicker />
                </create_time>
            </SearchForm>
        );
    }

    get columns() {

        const columns = [{ title: '#', render(text, record, index) { return index + 1; } },
        { title: "目录", dataIndex: "CategoryName" },
        { title: '名称', dataIndex: 'Title' },
        { title: "Url名称", dataIndex: "UrlName" },
        { title: "状态", dataIndex: "Status", render: RenderHelper.render_status(Enums.CommonStyleStatus) },
        { title: '时间', dataIndex: 'CreateTime', render: RenderHelper.render_time },
        ];

        if (this.hasRights(Enums.SysRights.CMS_UPDATE)) {
            columns.push({
                title: "操作", render: (it) => {
                    return (
                        <Button type="primary" size="small" onClick={() => this.goto(`/cms/article/${it.Id}`)}>编辑</Button>
                    );
                }
            });
        }

        return columns;
    }
}

export default connect(state => ({
    isFetching: state.article.isFetching,
    article: state.article.Article,
}))(ArticleList);