import * as app_const from "../app_const";

class ConfigReducer{
	static reduce(state={isFetching:false,configs:[]},action){
		switch(action.type){
			case app_const.QUERY_CONFIG:
				return Object.assign({},state,{
                    isFetching:true
                });
			case `${app_const.QUERY_CONFIG}_SUCCESS`:
				return ConfigReducer._query_success(state, action);
            case `${app_const.ADD_CONFIG}_SUCCESS`:
                return ConfigReducer._add_success(state,action);
            case `${app_const.UPDATE_CONFIG}_SUCCESS`:
                return ConfigReducer._update_success(state, action);
            case `${app_const.QUERY_CONFIG}_FAILURE`:
                return Object.assign({}, state, {
                    isFetching: false
                });
            default:
            	return state;
		}
	}

	static _common_success(state,action){
        const resp = action.response;
        if(resp.code==0)return state;
        return ConfigReducer._process_failure(state,action);
    }

    static _process_failure(state,action){
        const resp = action.response;
        action.error=resp.message;
        action.error_code=resp.code;
        return Object.assign({}, state, {
            isFetching: false
        });
    }

    static _query_success(state,action){
        const resp = action.response;
        if(resp.code==0){
            let data = {isFetching: false,configs: resp.data};
            return Object.assign({}, state, data);
        }else
            return ConfigReducer._process_failure(state,action);
    }

    static _add_success(state,action){
        const resp = action.response;
        if (resp.code == 0) {
            var data=[
                resp.data,
                ...state.configs
            ];
            return Object.assign({}, state, {configs:data});
        }else
            return ConfigReducer._process_failure(state,action);
    }

    static _update_success(state,action){
        const resp = action.response;
        if (resp.code == 0) {
            const config = state.configs.find(x=>x.Id==action.payload.id);
            if(!config)return state;
            var update_index=state.configs.indexOf(config);

            let update_config = {
                ConfigName: action.payload.name,
                ConfigValue: action.payload.value,
                Desc: action.payload.desc,
            };

            let configs= [
                ...state.configs.slice(0, update_index),
                Object.assign({}, config, update_config),
                ...state.configs.slice(update_index + 1)
            ];
            return Object.assign({},state,{configs});

        }else
            return ConfigReducer._process_failure(state,action);
    }
}

export { ConfigReducer };