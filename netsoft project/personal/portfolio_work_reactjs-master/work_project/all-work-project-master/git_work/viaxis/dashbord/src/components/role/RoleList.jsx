import React from 'react';
import { connect } from 'react-redux';
import { message, Table, Tag, Button, Modal, Form, Input, Select, Checkbox, Switch, Spin } from 'antd';

import BasicComponent from "../common/BasicComponent";
import SearchForm from "../common/SearchForm";

import { loadRoles, updateRole, addRole, loadRights, getRoleRights } from '../../actions/rights';
import { dateFormat } from "../../utils/string";
import * as RenderHelper from "../../utils/render";
import auth, { Rights } from "../../utils/auth";

const createForm = Form.create;
const FormItem = Form.Item;
const CheckboxGroup = Checkbox.Group;
const Option = Select.Option;
const Message = message;

class RoleList extends BasicComponent {

    constructor(props) {
        super(props);
        this.query = {};
        this.pageIndex = 1;
    }

    /* ------------------- Sys --------------------- */

    componentWillMount() {
        this.props.dispatch(loadRoles(this.props.pageIndex)).then(act => {
            if (act.error)
                Message.error(act.error);
        });
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.pageIndex !== this.props.pageIndex) {
            this.props.dispatch(loadRoles(nextProps.pageIndex));
        }
    }

    /* ------------------- Functions ----------------------- */

    queryRoles() {
        this.props.dispatch(loadRoles(this.pageIndex, this.query)).then(act => {
            if (act.error)
                Message.error(act.error);
        });
    }

    /* -------------------- Renders ------------------------- */

    render() {

        const self = this;

        const { role } = this.props;

        const rowKey = function (role) {
            return role.Id
        }

        const pagination = {
            total: role.count,
            onChange(current) {
                console.log('Current: ', current);
                self.pageIndex = current;
                self.queryRoles();
            },
        };

        return (
            <div>
                {auth.hasRights(Rights.ADMIN_INSERT) &&
                    <div style={{ marginBottom: 16 }}>
                        <RoleAdd dispatch={this.props.dispatch} />
                    </div>
                }
                <Table columns={this.columns} dataSource={role.data} loading={role.isFetching} pagination={pagination} rowKey={rowKey} />
            </div>
        );
    }

    get columns() {
        const { rights, role, dispatch } = this.props;
        const columns = [
            { title: '名称', dataIndex: 'Name' },
            { title: '说明', dataIndex: 'Description' },
            { title: '时间', dataIndex: 'CreateTime', render: RenderHelper.render_time },
            {
                title: "状态", dataIndex: "Status", render(status) {
                    return <Switch checkedChildren={'开'} unCheckedChildren={'关'} defaultChecked={status == 0 ? true : false} />;
                }
            },
            {
                title: "操作", render(role) {
                    return <RoleEdit role={role} rights={rights.data} dispatch={dispatch} />;
                }
            }
        ];
        return columns;
    }
}

class RoleAddComponent extends BasicComponent {
    constructor() {
        super();
        this.state = { visible: false, loading: false };
    }

    isLoading(isLoading) {
        this.setState({ loading: isLoading });
    }

    isShow(visible) {
        this.setState({ visible: visible });
    }

    handleCancel() {
        this.isShow(false);
    }
    handleSubmit() {
        this.props.form.validateFields((errors, values) => {
            if (errors) { return; }

            const form = this.props.form.getFieldsValue();
            this.isLoading(true);
            this.props.dispatch(addRole(form.name, form.description)).then((action) => {
                this.isLoading(false);
                if (action.error)
                    Message.error(action.error);
                else {
                    this.isShow(false);
                    Message.success("添加成功");
                }
            });

        });
    }

    render() {
        const { getFieldDecorator } = this.props.form;
        return (
            <div>
                <Button type="primary" onClick={() => this.isShow(true)}>添加角色</Button>
                {this.state.visible && <Modal visible={this.state.visible} title="添加角色" onCancel={this.handleCancel.bind(this)} footer={[
                    <Button key="cancel" type="ghost" size="large" onClick={this.handleCancel.bind(this)}>取消</Button>,
                    <Button key="submit" type="primary" size="large" loading={this.state.loading} onClick={this.handleSubmit.bind(this)}>
                        确定
                    </Button>,
                ]}>
                    <Form horizontal>
                        <FormItem id="name" label="角色名称" labelCol={{ span: 6 }} wrapperCol={{ span: 14 }}>
                            {getFieldDecorator("name", {
                                rules: [
                                    { required: true, whitespace: false, message: "请输入角色名称" }
                                ]
                            })(
                                <Input autoComplete="off" />
                            )}
                        </FormItem>
                        <FormItem id="description" label="说明" labelCol={{ span: 6 }} wrapperCol={{ span: 14 }}>
                            {getFieldDecorator("description", {
                                rules: [
                                    { required: true, whitespace: false, message: "请输入说明" }
                                ]
                            })(
                                <Input autoComplete="off" />
                            )}
                        </FormItem>
                    </Form>
                </Modal>
                }
            </div>
        );
    }
}

class RoleEditComponent extends BasicComponent {

    constructor() {
        super();
        this.state = { visible: false, loading: false };
        this.render_cb = [];
    }

    /* --------------------- Sys ------------------- */

    componentWillMount() {

        const { role, rights } = this.props;

        if (rights == null) {
            this.props.dispatch(loadRights(1));
        }

        if (!role.Rights) {
            this.props.dispatch(getRoleRights(role.Id));
        }

    }

    componentDidUpdate(prevProps, prevState) {
        let cb = this.render_cb.pop()
        while (cb) {
            cb();
            cb = this.render_cb.pop();
        }
    }


    /* --------------------- Functions --------------------- */

    isLoading(isLoading) {
        this.setState({ loading: isLoading });
    }

    isShow(visible) {
        this.setState({ visible: visible });
    }

    /* -------------------- Events ------------------------ */

    handleClick(e) {

        const { rights } = this.props;
        const data = rights.data;
        if (data == null || data.length == 0) {
            this.isLoading(true);
            this.isShow(false);
            this.props.dispatch(loadRights(1)).then(() => { this.isLoading(false); this.isShow(true); });
            return;
        }
        this.isShow(true);
    }

    handleSubmit(e) {
        this.props.form.validateFields((errors, values) => {
            if (errors) { return; }

            const { role } = this.props;
            const form = this.props.form.getFieldsValue();
            this.isLoading(true);
            console.log(form);
            this.props.dispatch(updateRole(role.Id, form.name, form.description, form.status, form.rights)).then((action) => {
                this.isLoading(false);
                if (action.error)
                    Message.error(action.error);
                else
                    Message.success("修改成功");
            });
        });
    }

    handleCancel(e) {
        this.isShow(false);
    }

    /* -------------------------- Renders -------------------------- */

    render() {
        const { role, rights } = this.props;
        const { getFieldDecorator } = this.props.form;

        if (this.state.visible && !role.Rights) {
            this.render_cb.push(() => this.isLoading(true));
            this.render_cb.push(() => this.isShow(false));
            return <Button type="primary" size="small" loading={this.state.loading} onClick={this.handleClick.bind(this)}>编辑</Button>;
        }

        return (
            <div>
                <Button type="primary" size="small" loading={this.state.loading} onClick={this.handleClick.bind(this)}>编辑</Button>
                {this.state.visible && <Modal visible={this.state.visible} title={"编辑" + role.Name} onCancel={this.handleCancel.bind(this)} footer={[
                    <Button key="cancel" type="ghost" size="large" onClick={this.handleCancel.bind(this)}>取消</Button>,
                    <Button key="submit" type="primary" size="large" loading={this.state.loading} onClick={this.handleSubmit.bind(this)}>
                        确定
                    </Button>,
                ]}>
                    <Form horizontal>
                        <FormItem id="name" label="名称" labelCol={{ span: 6 }} wrapperCol={{ span: 14 }}>
                            {getFieldDecorator("name", {
                                initialValue: role.Name,
                                rules: [
                                    { required: true, whitespace: false, message: "请填写名称" }
                                ]
                            })(
                                <Input autoComplete="off" />
                            )}
                        </FormItem>
                        <FormItem id="description" label="说明" labelCol={{ span: 6 }} wrapperCol={{ span: 14 }}>
                            {getFieldDecorator("description", { initialValue: role.Description })(
                                <Input autoComplete="off" />
                            )}
                        </FormItem>
                        <FormItem label="权限" labelCol={{ span: 6 }} wrapperCol={{ span: 14 }}>
                            {getFieldDecorator("rights", {
                                valuePropName: "checked",
                                initialValue: role.Rights.map(x => x.Id)
                            })(
                                <CheckboxGroup options={rights.map(x => { return { label: x.Name, value: x.Id } })} defaultValue={role.Rights.map(x => x.Id)} />
                            )}
                        </FormItem>
                        <FormItem id="{status}" label="状态" labelCol={{ span: 6 }} wrapperCol={{ span: 14 }}>
                            {getFieldDecorator("status", { initialValue: role.Status.toString() })(
                                <Select size="large" style={{ width: 200 }}>
                                    <Option value="0">启用</Option>
                                    <Option value="1">禁用</Option>
                                </Select>
                            )}
                        </FormItem>
                    </Form>
                </Modal>
                }
            </div>
        );
    }
}

const RoleAdd = createForm()(RoleAddComponent);
const RoleEdit = createForm()(RoleEditComponent);

export default connect(state => ({
    role: state.role,
    rights: state.rights
}))(RoleList);
