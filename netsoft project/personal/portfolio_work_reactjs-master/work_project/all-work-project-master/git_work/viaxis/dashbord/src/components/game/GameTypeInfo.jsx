import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { message, Spin, Badge, Tooltip, Row, Col, Table, Tag, Button, Modal, Form, Input, Select, DatePicker } from 'antd';

import BasicComponent from "../common/BasicComponent";
import SearchForm from "../common/SearchForm";
import EditorModal from "../common/EditorModal";

import * as GameAction from '../../actions/game';
import { dateFormat, isEmptyObject } from "../../utils/string";
import { CommonStatus, CommonStyleStatus, SysRights } from "../../app_enums";
import * as RenderHelper from "../../utils/render";

const createForm = Form.create;
const FormItem = Form.Item;
const Option = Select.Option;
const RangePicker = DatePicker.RangePicker;
const Message = message;

class GameTypeInfo extends BasicComponent {

    constructor(props) {
        super(props);
        this.pageIndex = 1;
        this.query = {};
        this.state = {
            submiting: false,
            editType: null
        };
    }

    /* ---------------------------- 系统函数 ------------------------------*/

    componentDidMount() {
        this.queryData();
        this.props.dispatch(GameAction.queryGameGroup());
        this.props.dispatch(GameAction.queryGameTypeCategory());
        this.props.dispatch(GameAction.queryGameTypeSort());
        this.props.dispatch(GameAction.queryGameTypeKind());
    }

    /* ---------------------------- 自定义函数 ------------------------------*/

    queryData() {
        this.props.dispatch(GameAction.queryGameTypeInfo(this.pageIndex, this.query)).then(act => {
            if (act.error)
                Message.error(act.error);
        });
    }

    addTypeInfo() {
        this.setState({
            editType: {}
        });
    }

    updateTypeInfo(type) {
        this.setState({
            editType: type
        });
    }

    /* ---------------------------- Events ------------------------------*/

    handleSubmit(values) {
        if (!this.state.editType) return;
        const { dispatch } = this.props;

        this.setState({ submiting: true });
        if (isEmptyObject(this.state.editType)) {
            dispatch(GameAction.addGameTypeInfo(values.gametype, values.name, values.category,
                values.sort, values.kind, values.explain, values.example, values.weight)).then(act => {
                    this.setState({
                        submiting: false
                    });
                    if (act.error || act.response.code != 0) {
                        Message.error(act.error || act.response.message);
                    } else {
                        Message.success("添加成功");
                        this.setState({
                            editType: null
                        });
                        this.queryData();
                    }
                });
        } else {
            dispatch(GameAction.updateGameTypeInfo(this.state.editType.GameTypeId, values.name, values.category,
                values.sort, values.kind, values.explain, values.example, values.weight, values.status)).then(act => {
                    this.setState({ submiting: false });
                    if (act.error || act.response.code != 0) {
                        Message.error(act.error || act.response.message);
                    } else {
                        Message.success("修改成功");
                        this.setState({ editType: null });
                        this.queryData();
                    }
                });
        }
    }

    onSearch(query) {
        this.pageIndex = 1;
        this.query = query;
        this.queryData();
    }

    onGroupSelectChange(groupId) {
        this.setState({ loadingGroupGameType: true });
        this.props.dispatch(GameAction.queryGameTypeWithoutInfo(groupId)).then(it => {
            this.setState({ loadingGroupGameType: false });
        });
    }

    /* ---------------------------- Renders ------------------------------*/

    tableColumns() {
        const self = this;
        const columns = [{ title: '#', render(text, record, index) { return index + 1; } },
        { title: '彩种', dataIndex: 'Group.Name' },
        { title: '玩法', dataIndex: 'GameType.Name' },
        { title: '名称', dataIndex: 'Name' },
        {
            title: '分类', render(item) {
                return `${item.Category.CategoryName}/${item.Sort.SortName}/${item.Kind.KindName}`;
            }
        },
        { title: '赔率', dataIndex: 'GameType.Scale' },
        { title: '基础赔率', dataIndex: 'GameType.ScaleBase' },
        { title: '权重', dataIndex: 'Weight' },
        {
            title: '说明', render(item) {
                return (
                    <div>
                        <Tooltip title={item.Explain}>
                            <Tag color="#87d068">说明</Tag>
                        </Tooltip>
                        <Tooltip title={item.Example}>
                            <Tag color="#108ee9">示例</Tag>
                        </Tooltip>
                    </div>
                );
            }
        },
        { title: "状态", dataIndex: "Status", render: RenderHelper.render_status(CommonStyleStatus) },
        { title: '时间', dataIndex: 'CreateTime' }
        ];

        if (this.hasRights(SysRights.ADMIN_UPDATE)) {
            columns.push({
                title: "操作", render(type) {
                    return <Button type="primary" onClick={() => self.updateTypeInfo(type)}>编辑</Button>
                }
            });
        }

        return columns;
    }


    render() {
        let gameTypeInfo = this.props.GameTypeInfo || {};
        let columns = this.tableColumns();

        const self = this;
        const pagination = {
            total: gameTypeInfo.count || 0,
            onChange(current) {
                self.pageIndex = current;
                self.queryData();
            },
        };
        return (
            <div>
                {
                    this.hasRights(SysRights.ADMIN_UPDATE) &&
                    <div style={{ marginBottom: 16 }}>
                        <Button type="primary" onClick={this.addTypeInfo.bind(this)}>添加</Button>
                    </div>
                }
                {this.renderSearch()}
                <div className="search-result-list">
                    <Table columns={columns} dataSource={gameTypeInfo.data || []} loading={this.props.isFetching} pagination={pagination} />
                </div>
                {this.render_modal()}
            </div>
        );
    }

    renderSearch() {

        const { Groups, Categorys, Sorts, Kinds } = this.props;

        const formItemLayout = {
            labelCol: { span: 10 },
            wrapperCol: { span: 14 }
        };

        return (
            <SearchForm onSearch={this.onSearch.bind(this)} span={4}>
                <name label="名称" {...formItemLayout}>
                    <Input placeholder="名称" size="default" name="name" />
                </name>
                <game_type_name label="玩法" {...formItemLayout}>
                    <Input placeholder="玩法" size="default" />
                </game_type_name>
                <group_id label="彩种" {...formItemLayout} options={{ initialValue: "" }}>
                    <Select>
                        <Option value="">全部</Option>
                        {(Groups || []).map(group => {
                            return <Option value={group.Id.toString()} key={group.Id}>{group.Name}</Option>
                        })}
                    </Select>
                </group_id>
                <category label="玩法目录" {...formItemLayout} options={{ initialValue: "" }}>
                    <Select>
                        <Option value="">全部</Option>
                        {(Categorys || []).map(category => {
                            return <Option value={category.CategoryId.toString()} key={category.CategoryId}>{category.CategoryName}</Option>
                        })}
                    </Select>
                </category>
                <sort label="玩法种类" {...formItemLayout} options={{ initialValue: "" }}>
                    <Select>
                        <Option value="">全部</Option>
                        {(Sorts || []).map(sort => {
                            return <Option value={sort.SortId.toString()} key={sort.SortId}>{sort.SortName}</Option>
                        })}
                    </Select>
                </sort>
                <kind label="玩法类型" {...formItemLayout} options={{ initialValue: "" }}>
                    <Select>
                        <Option value="">全部</Option>
                        {(Kinds || []).map(kind => {
                            return <Option value={kind.KindId.toString()} key={kind.KindId}>{kind.KindName}</Option>
                        })}
                    </Select>
                </kind>
                <status label="状态" {...formItemLayout} options={{ initialValue: "" }}>
                    <Select>
                        <Option value="">全部</Option>
                        {Object.keys(CommonStatus).map(it => {
                            return <Option value={it.toString()} key={it}>{CommonStatus[it]}</Option>
                        })}
                    </Select>
                </status>
            </SearchForm>
        );
    }

    render_modal() {
        const { editType } = this.state;
        if (!editType) return;

        const { Groups, GameTypes, Categorys, Sorts, Kinds } = this.props;

        if (!Groups) {
            if (!this.state.fetchingGroup) {
                this.state.fetchingGroup = true;
                this.props.dispatch(GameAction.queryGameGroup()).then(it => this.state.fetchingGroup = false);
            }
            return;
        }

        if (!Categorys) {
            this.props.dispatch(GameAction.queryGameTypeCategory());
            return;
        }

        if (!Sorts) {
            this.props.dispatch(GameAction.queryGameTypeSort());
            return;
        }

        if (!Kinds) {
            this.props.dispatch(GameAction.queryGameTypeKind());
            return;
        }

        const title = isEmptyObject(editType) ? "添加游戏玩法" : `编辑${editType.Category.CategoryName}/${editType.Sort.SortName}/${editType.Kind.KindName}-${editType.Name}`;

        return (
            <EditorModal title={title} visible={true}
                loading={this.state.loadingGroupGameType}
                submiting={this.state.submiting}
                onSubmit={this.handleSubmit.bind(this)}
                onClose={it => this.setState({ editType: null })}>
                {isEmptyObject(editType) &&
                    <group label="彩种" options={{
                        initialValue: (editType.Id || "").toString(),
                        rules: [{
                            required: true,
                            message: '请选择彩种'
                        }]
                    }}>
                        <Select onChange={this.onGroupSelectChange.bind(this)}>
                            {Groups.map(it => {
                                return <Option key={it.Name} value={it.Id.toString()}>{it.Name}</Option>;
                            })}
                        </Select>
                    </group>
                }
                {isEmptyObject(editType) &&
                    <gametype label="玩法" options={{
                        rules: [{
                            required: true,
                            message: '请选择玩法'
                        }]
                    }}>
                        <Select>
                            {(GameTypes || []).map(it => {
                                return <Option key={it.Name} value={it.Id.toString()}>{it.Name}</Option>;
                            })}
                        </Select>
                    </gametype>
                }
                <name label="名称" options={{
                    initialValue: editType.Name || "",
                    rules: [{
                        required: true,
                        message: '请输入玩法名称'
                    }]
                }}>
                    <Input type="name" autoComplete="off" />
                </name>
                <category label="玩法目录" options={{
                    initialValue: (editType.CategoryId || '').toString(),
                    rules: [
                        { required: true, message: '请选择玩法目录' }
                    ]
                }}>
                    <Select>
                        {Categorys.map(it => {
                            return <Option key={it.CategoryName} value={it.CategoryId.toString()}>{it.CategoryName}</Option>;
                        })}
                    </Select>
                </category>
                <sort label="玩法种类" options={{
                    initialValue: (editType.SortId || '').toString(),
                    rules: [
                        { required: true, message: '请选择玩法种类' }
                    ]
                }}>
                    <Select>
                        {Sorts.map(it => {
                            return <Option key={it.SortName} value={it.SortId.toString()}>{it.SortName}</Option>;
                        })}
                    </Select>
                </sort>
                <kind label="玩法类型" options={{
                    initialValue: (editType.KindId || '').toString(),
                    rules: [
                        { required: true, message: '请选择玩法类型' }
                    ]
                }}>
                    <Select>
                        {Kinds.map(it => {
                            return <Option key={it.KindName} value={it.KindId.toString()}>{it.KindName}</Option>;
                        })}
                    </Select>
                </kind>
                <explain label="玩法说明" options={{
                    initialValue: (editType.Explain || '').toString(),
                    rules: [
                        { required: true, message: '请输入玩法说明' }
                    ]
                }}>
                    <Input autoComplete="off" type="textarea" />
                </explain>
                <example label="玩法示例" options={{
                    initialValue: (editType.Example || '').toString(),
                    rules: [
                        { required: true, message: '请输入玩法示例' }
                    ]
                }}>
                    <Input autoComplete="off" type="textarea" />
                </example>
                <weight label="玩法权重" options={{
                    initialValue: (editType.Weight || '0').toString(),
                    rules: [
                        { required: true, message: '请输入玩法权重' }
                    ]
                }}>
                    <Input autoComplete="off" />
                </weight>
                {!isEmptyObject(editType) &&
                    <status label="状态" options={{
                        initialValue: (editType.Status || 0).toString(),
                        rules: [{
                            required: true,
                            message: '请选择玩法状态'
                        }]
                    }}>
                        <Select>
                            {Object.keys(CommonStatus).map(it => {
                                return <Option key={it} value={it}>{CommonStatus[it]}</Option>;
                            })}
                        </Select>
                    </status>
                }
            </EditorModal>
        );
    }
}

export default connect(state => ({
    Groups: state.game.Groups,
    GameTypeInfo: state.game.GameTypeInfo,
    GameTypes: state.game.GameType,
    Categorys: state.game.GameTypeCategory,
    Sorts: state.game.GameTypeSort,
    Kinds: state.game.GameTypeKind,
}))(GameTypeInfo);
