import $ from "jquery";
import * as app_const from "../app_const";
import * as Utils from "../utils/common";

// Role

export function loadRoles(pageIndex = 1) {
	var payload = JSON.stringify({ pageIndex });
	return {
		name: app_const.LOAD_ROLES,
		callAPI: () => Utils.POST(`/api/role`, payload)(),
		payload: { pageIndex }
	}
}

export function addRole(name, description) {
	var payload = { name: name, description: description };
	return {
		name: app_const.ADD_ROLE,
		callAPI: () => Utils.POST(`/api/role/add`, JSON.stringify(payload))()
	};
}

export function updateRole(id, name, description, status, rightsIds) {
	console.log(rightsIds);
	var payload = { Id: id, Name: name, Description: description, Status: status };
	if (rightsIds)
		payload["rights"] = Array.isArray(rightsIds) ? rightsIds.join(",") : null;
	return {
		name: app_const.UPDATE_ROLE_RIGHTS,
		callAPI: () => Utils.POST(`/api/role/update`, JSON.stringify(payload))(),
		payload: {
			rights: rightsIds,
			id, name, description, status
		}
	};
}

// Rights

export function loadRights(pageIndex = 1, onSuccess) {
	let payload = JSON.stringify({ pageIndex });
	return {
		name: app_const.LOAD_RIGHTS,
		callAPI: () => Utils.POST(`/api/rights`, payload)(),
		payload: {
			pageIndex
		},
		onSuccess: onSuccess
	};
}

export function addRights(name, description) {
	var payload = { name: name, description: description };
	return {
		name: app_const.ADD_RIGHTS,
		callAPI: () => Utils.POST(`/api/rights/add`, JSON.stringify(payload))()
	};
}

export function updateRights(rightsId, name, description, status, onSuccess = null, onFailure = null) {
	var payload = { Id: rightsId, Name: name, Description: description, Status: status };
	return {
		name: app_const.UPDATE_RIGHTS,
		callAPI: () => Utils.POST(`/api/rights/update`, JSON.stringify(payload))(),
		payload: {
			id: rightsId, name, description, status
		},
		onSuccess: onSuccess,
		onFailure: onFailure
	};
}

// Role Rights

export function getRoleRights(roleId) {
	let timestamp = new Date().getTime();
	return {
		name: app_const.GET_ROLE_RIGHTS,
		callAPI: () => Utils.POST(`/api/role/${roleId}/rights?t=${timestamp}`)(),
		payload: {
			roleId
		}
	};
}

// Role User

export function loadRoleUsers(querys) {
	let params = JSON.stringify(querys);
	return {
		name: app_const.LOAD_ROLE_USERS,
		callAPI: () => Utils.POST(`/api/role/users`, params)()
	}
}

export function addRoleUser(role_id, user_id, email = null) {
	var payload = {
		RoleId: role_id,
		UserId: user_id,
		Email: email
	};
	return {
		name: app_const.ADD_ROLE_USER,
		callAPI: () => Utils.POST(`/api/role/users/add`, JSON.stringify(payload))(),
		payload
	};
}

export function removeRoleUser(role_id, user_id) {
	var payload = { RoleId: role_id, UserId: user_id };
	return {
		name: app_const.REMOVE_ROLE_USER,
		callAPI: () => Utils.POST(`/api/role/users/remove`, JSON.stringify(payload))(),
		payload: {
			role_id, user_id
		}
	};
}