import Auth from "./utils/auth";
import * as Utils from "./utils/common";

Utils.attachTabsShareSessionStorage();

function requireLogin(nextState, replace) {
    if (!Auth.isLogged()) {
        replace({
            pathname: '/login',
            state: { nextPathname: nextState.location.pathname }
        });

    }
}

function redirectToDashboard(nextState, replace) {
    if (Auth.isLogged()) {
        replace({
            pathname: '/',
            state: { nextPathname: nextState.location.pathname }
        });
    }
}

const routes = [
    { path: '/login', component: require("./login").default },
    { path: '/logout', component: require("./containers/Logout").default },
    {
        path: '/', component: require("./containers/App").default,
        onEnter: requireLogin,
        indexRoute: {
            component: require("./components/Home").default, onEnter: requireLogin
        },
        childRoutes: [
            // configinfo
            { path: '/config', component: require("./components/ConfigInfo").default },
            // rights
            { path: '/role', component: require("./components/role/RoleList").default },
            { path: '/rights', component: require("./components/role/RightsList").default },
            { path: '/role/users', component: require("./components/role/RoleUsers").default },
            // user
            { path: '/user', component: require("./components/user/UserList").default },

            { path: '/user/(:userId/)action/log', component: require("./components/user/ActionLog").default },
            { path: '/user/(:userID/)loginlog', component: require("./components/user/LoginLog").default },
            { path: '/user/online', component: require("./components/user/UserList").OnlineList },
            { path: '/user/(:userId)', component: require("./components/user/UserInfo").default },
            { path: "/agent(/:userID)", component: require("./components/user/Agent").default },
            { path: "/rebatelevel", component: require("./components/user/RebateLevel").default },
            { path: "/referral", component: require("./components/user/Referral").default },

            // bill flow
            { path: "/billflow", component: require("./components/bill/BillFlow").default },
            // bill deposit
            { path: "/deposit", component: require("./components/bill/Deposit").default },
            { path: "/deposit/list", component: require("./components/bill/Deposit").DepositList },
            // bill withdraw
            { path: "/withdraw", component: require("./components/bill/Withdraw").default },
            { path: "/withdraw/list", component: require("./components/bill/Withdraw").WithdrawList },
            // bill transfer
            { path: "/transfer", component: require("./components/bill/Transfer").default },
            { path: "/transfer/list", component: require("./components/bill/Transfer").TransferList },
            // balance
            { path: "/balance", component: require("./components/bill/balance").default },
            // dividend scale
            { path: "/dividend", component: require("./components/bill/DividendScale").default },
            // bank
            { path: "/bank", component: require("./components/bank/bank").default },
            { path: "/bank/company", component: require("./components/bank/CompanyBank").default },
            { path: "/bank/user", component: require("./components/bank/UserBank").default },
            // game
            { path: "/game/group", component: require("./components/game/Group").default },
            { path: "/game/type", component: require("./components/game/GameType").default },
            { path: "/game/type/info", component: require("./components/game/GameTypeInfo").default },
            { path: "/game/type/category", component: require("./components/game/GameTypeCategory").default },
            { path: "/game/type/sort", component: require("./components/game/GameTypeSort").default },
            { path: "/game/type/kind", component: require("./components/game/GameTypeKind").default },
            { path: "/game/algorithm", component: require("./components/game/GameAlgorithm").default },
            { path: "/game/data", component: require("./components/game/GameData").default },
            { path: "/dealinfo", component: require("./components/game/DealInfo").default },
            { path: "/game/playlimit", component: require("./components/game/PlayLimit").default },
            // profit
            { path: "/profit/user(/:userId)", component: require("./components/profit/ProfitUser").default },
            { path: "/profit/gameType(/:userId)", component: require("./components/profit/ProfitGameType").default },
            { path: "/profit/dividend(/:userId)", component: require("./components/profit/ProfitDividend").default },
            // article
            { path: "/cms/category", component: require("./components/article/Category").default },
            { path: "/cms/article", component: require("./components/article/ArticleList").default },
            { path: "/cms/article/add", component: require("./components/article/ArticleAdd").default },
            { path: "/cms/article/:articleId", component: require("./components/article/ArticleEdit").default },
            // notice
            { path: "/notice/bulletin", component: require("./components/notice/Bulletin").default },
            { path: "/notice/mailmsg", component: require("./components/notice/MailMsg").default }
        ]
    }];

export default routes;