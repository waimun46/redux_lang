import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { message, Row, Col, Table, Tag, Button, Modal, Form, Input, Select, DatePicker } from 'antd';
import moment from 'moment';

import BasicComponent from "../common/BasicComponent";
import SearchForm from "../common/SearchForm";

import { queryBulletin, addBulletin, updateBulletin } from '../../actions/notice';
import * as RenderHelper from "../../utils/render";
import { SysRights, BulletinStatus, BulletinType, BulletinStatusType, CommonStatus } from "../../app_enums";
import * as Utils from "../../utils/common";

const createForm = Form.create;
const FormItem = Form.Item;
const Option = Select.Option;
const RangePicker = DatePicker.RangePicker;
const Message = message;

class SearchFormComponent extends React.Component {
    constructor() {
        super()
        this.state = {
            from_time: "",
            to_time: "",
            type: "",
            status: ""
        };
    }

    handleStatusChange(val) {
        this.setState({ status: val });
    }

    handleTypeChange(val) {
        this.setState({ type: val });
    }

    handleTimeChange(dates, dateStrings) {
        var from = new Date(dateStrings[0] + " 00:00:00");
        var to = new Date(dateStrings[1] + " 00:00:00");
        to.setDate(to.getDate() + 1);
        this.setState({
            from_time: from,
            to_time: to
        })
    }

    handleClick() {
        this.props.onSearchClick(this.state);
    }

    handleReset() {
        var state = {
            type: "",
            status: ""
        };
        this.setState(state);
    }

    render() {
        return (
            <Form horizontal className="ant-advanced-search-form" ref="query_form">
                <Row gutter={16}>
                    <Col sm={6}>
                        <FormItem label="时间" labelCol={{ span: 10 }} wrapperCol={{ span: 14 }}>
                            <RangePicker name="create_time" style={{ width: 200 }} onChange={this.handleTimeChange.bind(this)} />
                        </FormItem>
                    </Col>
                    <Col sm={6}>
                        <FormItem label="类型" labelCol={{ span: 10 }} wrapperCol={{ span: 14 }}>
                            <Select optionFilterProp="children" value={this.state.type} notFoundContent="找不到内容" onChange={this.handleTypeChange.bind(this)}>
                                <Option value="">全部</Option>
                                {Object.keys(BulletinType).map(it => {
                                    return <Option value={it.toString()} key={it}>{BulletinType[it]}</Option>
                                })}
                            </Select>
                        </FormItem>
                    </Col>
                    <Col sm={6}>
                        <FormItem label="状态" labelCol={{ span: 10 }} wrapperCol={{ span: 14 }}>
                            <Select optionFilterProp="children" value={this.state.status} notFoundContent="找不到内容" onChange={this.handleStatusChange.bind(this)}>
                                <Option value="">全部</Option>
                                {Object.keys(BulletinStatusType).map(it => {
                                    return <Option value={it.toString()} key={it}>{BulletinStatusType[it]}</Option>
                                })}
                            </Select>
                        </FormItem>
                    </Col>
                </Row>
                <Row>
                    <Col span={12} offset={12} style={{ textAlign: 'right' }}>
                        <Button type="primary" icon="search" onClick={this.handleClick.bind(this)}>搜索</Button>
                        <Button type="ghost" icon="reload" onClick={this.handleReset.bind(this)}>清空</Button>
                    </Col>
                </Row>
            </Form>
        );
    }
}

class BulletinAddComponent extends BasicComponent {
    constructor() {
        super();
        this.state = {
            visible: false,
            loading: false,
            expiretime: ""
        };
    }

    isLoading(isLoading) {
        this.setState({ loading: isLoading });
    }
    isShow(visible) {
        this.setState({ visible: visible });
    }

    handleClick() {
        this.isShow(true);
    }

    handleTimeChange(value, dateStrings) {
        var expiretime = Utils.unixtimestampFromDate(new Date(dateStrings));
        this.setState({
            expiretime: expiretime
        })
    }

    handleSubmit() {
        this.props.form.validateFields((errors, values) => {
            if (errors) { return; }
            const form = this.props.form.getFieldsValue();
            this.isLoading(true);
            this.props.dispatch(addBulletin(form.content, 0, form.type, form.status, this.state.expiretime)).then((action) => {
                this.isLoading(false);
                if (action.error)
                    Message.error(action.error);
                else {
                    this.isShow(false);
                    this.props.queryData();
                }
            });
        });
    }
    handleCancel() {
        this.isShow(false);
    }
    render() {
        const { getFieldDecorator } = this.props.form;
        return (
            <div>
                <Button type="primary" loading={this.state.loading} onClick={this.handleClick.bind(this)}>添加公告</Button>
                {this.state.visible &&
                    <Modal visible={this.state.visible} title="添加公告" onCancel={this.handleCancel.bind(this)} footer={[
                        <Button key="cancel" type="ghost" size="large" onClick={this.handleCancel.bind(this)}>取消</Button>,
                        <Button key="submit" type="primary" size="large" loading={this.state.loading} onClick={this.handleSubmit.bind(this)}>
                            确定
                        </Button>
                    ]}>
                        <Form horizontal>
                            <FormItem id="content" label="公告内容" labelCol={{ span: 6 }} wrapperCol={{ span: 14 }}>
                                {getFieldDecorator("content", {
                                    rules: [{ required: true, message: "公告内容为必填项目" }]
                                }
                                )(
                                    <Input type="textarea" rows="6" placeholder="公告内容的明细" />
                                )}
                            </FormItem>
                        </Form>
                        <Form horizontal>
                            <FormItem id="type" label="类型" labelCol={{ span: 6 }} wrapperCol={{ span: 14 }}>
                                {getFieldDecorator("type", {
                                    initialValue: "0"
                                })(
                                    <Select optionFilterProp="children" notFoundContent="找不到内容">
                                        {Object.keys(BulletinType).map(it => {
                                            return <Option value={it.toString()} key={it}>{BulletinType[it]}</Option>
                                        })}
                                    </Select>
                                )}
                            </FormItem>
                        </Form>
                        <Form horizontal>
                            <FormItem id="type" label="过期时间" labelCol={{ span: 6 }} wrapperCol={{ span: 14 }}>
                                {getFieldDecorator("ExpireTime", {
                                    rules: [{ required: true, message: "过期时间为必填选项" }]
                                }
                                )(
                                    <DatePicker showTime format="YYYY-MM-DD HH:mm:ss" placeholder="公告的过期时间" style={{ width: 200 }} onChange={this.handleTimeChange.bind(this)} />
                                )}
                            </FormItem>
                        </Form>
                    </Modal>}
            </div>
        );
    }
}


class Bulletin extends BasicComponent {

    constructor(props) {
        super(props);
        this.state = {
            popup: false,
            popupTitle: "",
            loading: false,
            bulletin: {},
            expiretime: ""
        }
        this.query = {};
        this.pageIndex = 1;
    }

    /* --------------------- Sys ---------------------- */

    componentWillMount() {
        this.queryData();

    }

    /* --------------------- Function ---------------------- */

    queryData() {
        this.props.dispatch(queryBulletin(this.pageIndex, this.query)).then(act => {
            if (act.error)
                Message.error(act.error);
        });
    }

    /* --------------------- Events ---------------------- */

    onUpdateClick(bulletin) {
        this.props.form.setFieldsValue({
            content: bulletin.Content || "",
            type: (bulletin.Type || 0).toString(),
            status: (bulletin.Status || 0).toString(),
            expiretime: (bulletin.ExpireTime || 0).toString()
        })
        this.setState({
            popup: true,
            popupTitle: "编辑公告",
            loading: false,
            bulletin: bulletin,
            expiretime: bulletin.ExpireTime
        });
    }

    handleCancel() {
        this.setState({ popup: false, popupTitle: "", bulletin: {}, loading: false });
    }

    handleTimeChange(value, dateStrings) {
        var expiretime = Utils.unixtimestampFromDate(new Date(dateStrings));
        this.setState({
            expiretime: expiretime
        })
    }

    handleSubmit(e) {
        const { dispatch, form } = this.props;
        e.preventDefault();
        form.validateFields((errors, values) => {
            if (errors) return;

            var formVals = form.getFieldsValue();
            if (this.state.bulletin) {
                this.setState({ loading: true });
                dispatch(updateBulletin(this.state.bulletin.Id, formVals.content, 0, formVals.type, formVals.status, this.state.expiretime)).then((action) => {
                    this.setState({ loading: false });
                    if (action.error)
                        Message.error(action.error);
                    else {
                        this.setState({ popup: false });
                        this.queryData();
                    }
                });
            }
        });
    }

    onSearchClick(query) {
        this.query = query;
        this.pageIndex = 1;
        this.queryData();
    }

    /* --------------------- Renders ---------------------- */

    render() {
        const { getFieldDecorator } = this.props.form;
        const self = this;

        const { notice } = this.props;
        var bulletins = notice.bulletins || {
            count: 0,
            data: []
        };

        const pagination = {
            total: bulletins.count,
            onChange(current) {
                self.pageIndex = current;
                self.queryData();
            }
        };


        return (
            <div>
                <BulletinSearch onSearchClick={this.onSearchClick.bind(this)} /><br />
                <BulletinAdd dispatch={this.props.dispatch} queryData={this.queryData.bind(this)} /><br />
                <Table columns={this.columns} dataSource={bulletins.data} loading={bulletins.isFetching} pagination={pagination} rowKey="Id" />
                <Modal visible={this.state.popup} title={this.state.popupTitle} onCancel={this.handleCancel.bind(this)} footer={[
                    <Button key="cancel" type="ghost" size="large" onClick={this.handleCancel.bind(this)}>取消</Button>,
                    <Button key="submit" type="primary" size="large" loading={this.state.loading} onClick={this.handleSubmit.bind(this)}>
                        确定
                        </Button>
                ]}>
                    <Form horizontal>
                        <FormItem id="content" label="公告内容" labelCol={{ span: 6 }} wrapperCol={{ span: 14 }}>
                            {getFieldDecorator("content", {
                                initialValue: (this.state.bulletin.Content || ""),
                                rules: [{ required: true, message: "公告内容为必填项目" }]
                            },
                            )(
                                <Input type="textarea" rows="6" placeholder="公告内容的明细" />
                            )}
                        </FormItem>
                        <FormItem id="type" label="类型" labelCol={{ span: 6 }} wrapperCol={{ span: 14 }}>
                            {getFieldDecorator("type", {
                                initialValue: (this.state.bulletin.Type || 0).toString()
                            })(
                                <Select optionFilterProp="children" notFoundContent="找不到内容">
                                    {Object.keys(BulletinType).map(it => {
                                        return <Option value={it} key={it}>{BulletinType[it]}</Option>
                                    })}
                                </Select>
                            )}
                        </FormItem>
                        <FormItem id="status" label="状态" labelCol={{ span: 6 }} wrapperCol={{ span: 14 }}>
                            {getFieldDecorator("status", {
                                initialValue: (this.state.bulletin.Status || 0).toString()
                            })(
                                <Select>
                                    {Object.keys(BulletinStatusType).map(it => {
                                        return <Option value={it} key={it}>{BulletinStatusType[it]}</Option>
                                    })}
                                </Select>
                            )}
                        </FormItem>
                        <FormItem id="expiretime" label="过期时间" labelCol={{ span: 6 }} wrapperCol={{ span: 14 }}>
                            {getFieldDecorator("ExpireTime", {
                                initialValue: moment(new Date(Utils.unixtimestampToDate(this.state.bulletin.ExpireTime)))
                            })(
                                <DatePicker showTime format="YYYY-MM-DD HH:mm:ss" placeholder="公告的过期时间" style={{ width: 200 }} onChange={this.handleTimeChange.bind(this)} />
                            )}
                        </FormItem>
                    </Form>
                </Modal>
            </div>
        );
    }

    get columns() {
        const self = this;
        const columns = [
            { title: '#', render: RenderHelper.render_index },
            { title: "内容", dataIndex: "Content", width: 600 },
            {
                title: "类型", dataIndex: "Type", render(type) {
                    return <Tag color={type == 0 ? '#108ee9' : '#f50'}>{type == 0 ? "一般" : "紧急"}</Tag>
                }
            },
            { title: "状态", dataIndex: "Status", render: RenderHelper.render_status(BulletinStatus) },
            { title: '时间', dataIndex: 'CreateTime', render: RenderHelper.render_time },
            { title: '过期时间', dataIndex: 'ExpireTime', render: RenderHelper.render_time }
        ];

        if (this.hasRights(SysRights.CMS_UPDATE)) {
            columns.push({
                titel: "编辑", render(bulletin) {
                    return <Button type="primary" onClick={() => self.onUpdateClick(bulletin)}>编辑</Button>
                }
            });
        }

        return columns;
    }
}

const BulletinSearch = createForm()(SearchFormComponent);
const BulletinAdd = createForm()(BulletinAddComponent);

export default connect(state => ({
    notice: state.notice,
}))(Form.create()(Bulletin));