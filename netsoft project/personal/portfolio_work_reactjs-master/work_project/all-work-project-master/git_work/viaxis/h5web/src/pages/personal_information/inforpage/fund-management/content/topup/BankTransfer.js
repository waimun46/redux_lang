/**
 * Routes:
 *   - ./src/routes/AuthorizationRoute.js
 */

import { connect } from 'dva';
import { message, Row, Col, Alert, Tabs, Button, Input,Icon,Card  } from "antd";
import { Picker, List, InputItem, WhiteSpace } from 'antd-mobile';

import Page from "@/layouts/PageBasic";
import Form from '@/components/Form';
import * as RenderHelper from "@/utils/render_helper";
import banklogo1 from '../../../../../../assets/images/icon/banklogo1.png'
import banklogo2 from '../../../../../../assets/images/icon/banklogo2.png'
import banklogo4 from '../../../../../../assets/images/icon/banklogo4.png'
import banklogo5 from '../../../../../../assets/images/icon/banklogo5.png'

const Item = List.Item;
const Brief = Item.Brief;




const colorStyle = {
  display: 'inline-block',
  verticalAlign: 'middle',
  width: '20px',
  height: '20px',
  marginRight: '10px',
};

const iconimag = [
  {
    label:
      (<div>
        <img src={banklogo1}
             style={{ ...colorStyle, }}
        />
        <span>中国银行</span>
      </div>),
    value: '1',
  },
  {
    label:
      (<div>
        <img src={banklogo2}
             style={{ ...colorStyle, }}
        />
        <span>农业银行</span>
      </div>),
    value: '2',
  },
  {
    label:
      (<div>
        <img src={banklogo4}
             style={{ ...colorStyle, }}
        />
        <span>工商银行</span>
      </div>),
    value: '3',
  },
  {
    label:
      (<div>
        <img src={banklogo5}
             style={{ ...colorStyle, }}
        />
        <span>建设银行</span>
      </div>),
    value: '4',
  },
];










class BankTransfer extends Page {
  title = "银行转账"
  constructor(props) {
    super(props);
    this.state = {
      // 提交按钮
      loading: false,
    }
    // 初始验证码地址,否则每次表单输内容验证码都会变化
    this.captchaUrl = RenderHelper.getCaptchaImgUrl();
  }

  state = {
    iconValue: ['#00FF00'],
  };


  /* ---------------------------- Events ------------------------------*/

  handleUpdateSubmit(e) {
    e.preventDefault();

    const { dispatch } = this.props;

    this.form.validateFields((errors, values) => {

      if (errors) return;

      dispatch({
        type: "user/updateLoginPassword",
        payload: { currentPwd: values.current_pwd, password: values.password, captcha: values.captcha }
      }).then(resp => {
        this.setState({ loading: false });
        if (resp.code == 0) {
          let msg = "您的密码已修改，请重新登录";
          message.success(msg);
          this.props.form.resetFields();
        } else {
          this.refreshCaptcha();
          message.error(resp.message);
        }

      });

      this.setState({ loading: true });
    })
  }


  onChangeIcon = (icon) => {
    this.setState({
      iconValue: icon,
    });
  };

  /* ---------------------------- Renders ------------------------------*/

  render() {
    const { getFieldDecorator, getFieldProps } = this.props.form;
    return (
      <div className="form_sty">
        <div className="top-header"> </div>
        <div className="form_title">
          <h5>选项列表</h5>
        </div>
        <Form form={this.props.form} ref={el => this.form = el} onSubmit={this.handleUpdateSubmit.bind(this)}>

          <List >

            <Picker
              data={iconimag}
              value={this.state.iconValue}
              cols={1}
              onChange={this.onChangeIcon }
            >
              <List.Item arrow="horizontal">收款银行</List.Item>
            </Picker>

          </List>


          <div className="form_title2">
            <h5>填写银行信息</h5>
          </div>
          <InputItem
            {...getFieldProps('current_bnk', {
              rules: [{
                required: true, message: '请输入正确的开户支行',
              }],
            })}
            type=""
            cleartext
            placeholder="请输入开户支行">
            开户支行
          </InputItem>

          <InputItem
            {...getFieldProps('current_pwd', {
              rules: [{
                required: true, message: '请输入正确的收款户名',
              }],
            })}
            type=""
            cleartext
            placeholder="请输入收款户名">
            收款户名
          </InputItem>

          <InputItem
            {...getFieldProps('password', {
              rules: [{
                required: true, message: '请输入正确的收款账号',
              }],
            })}
            type="text"
            clear
            placeholder="请输入收款账号">
            收款账号
          </InputItem>

          <InputItem
            {...getFieldProps('cmf_pwd', {
              rules: [{
                required: true, message: '请输入充值金额',
              }],
            })}
            type=""
            cleartext
            placeholder="请输入充值金额">
            充值金额
          </InputItem>

          <InputItem
            {...getFieldProps('cmf_name', {
              rules: [{
                required: true, message: '请输入正确的银行户名',
              }],
            })}
            type="text"
            clear
            placeholder="请输入银行户名">
            银行户名
          </InputItem>

          <Card>
            <p>1.请转账到以上收款银行账户。</p>
            <p>2.请正确填写转账银行卡的持卡人姓名和充值金额，以变及时刻对。</p>
            <p>3.转账1笔提交1次，请勿重复提交订单。</p>
            <p>4.请务必转账后再提交订单，否则无法及时查到您的款项！</p>
          </Card>





          <div className="game-orders">
            <div className="ButtonBottom">
              <button className="btn_left">
                <Icon type="reload"/> 重置
              </button>

              <button className="btn_right" loading={this.state.loading} htmlType="submit" >
                确定
              </button>

            </div>
            <div className="clearfix"></div>
          </div>


        </Form>

      </div>


    );
  }
};

export default connect(state => ({

  loading: state.loading.models.user,
}))(Form.create()(BankTransfer));
