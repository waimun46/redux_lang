/**
 * Routes:
 *   - ./src/routes/AuthorizationRoute.js
 */

import { connect } from 'dva';
import { Input, Select, Button, Badge, Tooltip, Popover, Table, Icon } from "antd";

import BasicComponent from "@/components/BasicComponent";
import SearchForm from '@/components/SearchForm';
import * as Utils from "@/utils/common";
import * as Constants from "@/app_constants";
import * as Enums from "@/app_enum";
import * as RenderHelper from "@/utils/render_helper";


class BillFlowList extends BasicComponent {

    constructor(props) {
        super(props);
        this.querys = {}
    }

    /* ---------------------------- System  ------------------------------*/

    componentWillMount() {
        this.queryData();
    }

    /* ---------------------------- Functions  ------------------------------*/

    queryData() {
        const { dispatch } = this.props;

        dispatch({
            type: "billflow/queryBillflow",
            payload: this.querys
        });
    }

    get isShowEmail() {
        // const userinfo = this.props.userinfo;
        // return userinfo && userinfo.UserKind < Enums.UserKind.Member;
        return false;
    }

    /* ---------------------------- Events  ------------------------------*/

    onSearchClick(values) {

        this.querys = values;
        this.queryData();
    }

    onResetClick() {
        this.querys = {};
    }

    /* ---------------------------- Renders  ------------------------------*/

    render() {
        const self = this;
        const { billflow, loading } = this.props;
        let data = billflow.Data || [];

        const pagination = {
            total: billflow.TotalCount || 0,
            onChange(current) {
                self.querys.pageIndex = current;
                self.queryData();
            },
        };

        return (
            <div>
                {this.renderSearch()}
                <div className="search-result-list">
                    <Table loading={loading} columns={this.columns} dataSource={data} rowKey="Id" pagination={pagination} />
                </div>
            </div>
        );
    }

    renderSearch() {
        const formItemLayout = {
            labelCol: { span: 8 },
            wrapperCol: { span: 16 }
        };

        return (
            <SearchForm onSearch={this.onSearchClick.bind(this)} onReset={this.onResetClick.bind(this)}>
                {this.isShowEmail && <email label="邮箱" {...formItemLayout}>
                    <Input />
                </email>}
                <type label="流水类型" {...formItemLayout} options={{ initialValue: "" }}>
                    <Select>
                        <Select.Option value="">全部</Select.Option>
                        {Object.keys(Constants.BillFlowType).map(it => <Select.Option key={it} value={it.toString()}>{Constants.BillFlowType[it]}</Select.Option>)}
                    </Select>
                </type>
                <txid label="流水号" {...formItemLayout}>
                    <Input />
                </txid>
                <create_time label="时间" {...formItemLayout} format={(times) => Utils.dateRangeFormat(times)}>
                    {RenderHelper.rangeDatePick()}
                </create_time>
                <amount label="金额" {...formItemLayout}>
                    <Input />
                </amount>
            </SearchForm>
        );
    }

    get columns() {
        const self = this;
        const columns = [
            { title: '#', render: RenderHelper.render_index },
            {
                title: "流水类型", dataIndex: "Type",
                render(type, bill) {
                    if (type == Enums.BillFlowType.Transfer)
                        return bill.Amount > 0 ? "转入" : "转出"
                    else if (type == Enums.BillFlowType.Game)
                        return "注单" + Constants.GameSubType[bill.SubType];
                    else
                        return Constants.BillFlowType[type] || "未知"
                }
            },
            {
                title: "流水号", dataIndex: "Txid", render: (txid, flow) => {
                    if (flow.Type == Enums.BillFlowType.Game)
                        return <a target="_blank" href={`/deals?id=${flow.TargetId}`}>{txid}</a>
                    return txid;
                }
            },
            { title: "金额", dataIndex: "Amount", className: 'column-money', render: RenderHelper.money_format_color },
            { title: "余额", dataIndex: "Balance", className: 'column-money', render: RenderHelper.money_format_color },
            { title: "备注", dataIndex: "Remark" },
            { title: "申请时间", dataIndex: "CreateTime", render: RenderHelper.render_time },
        ];

        if (this.isShowEmail)
            columns.splice(1, 0, { title: 'Email', dataIndex: "Email", render(email) { return <span style={{ color: '#3498DB' }}>{email}</span> } });
        return columns;
    }

}

function mapStateToProps(state) {
    return {
        // userinfo: state.passport.userinfo.data,
        billflow: state.billflow,
        loading: state.loading.models.billflow
    };
}
export default connect(mapStateToProps)(BillFlowList);
