import styles from './index.css';


import { connect } from 'dva';
import { message, Row, Col, Alert, Tabs, Button, Input , Icon} from "antd";
import { List, InputItem, WhiteSpace, Checkbox, Flex, Toast, Modal  } from 'antd-mobile';

import Page from "@/layouts/PageBasic";
import Form from '@/components/Form';
import * as RenderHelper from "@/utils/render_helper";
import { ActionState } from "../../app_constants";



import router from 'umi/router'; 
import { browserHistory, Link } from 'react-router'
import Auth from '@/utils/auth';

const FormItem = Form.Item; 
const title = "注册";
const Message = message;

const alert = Modal.alert;

function tnc1() {
    Modal.info({
        title: '服务条款',
        content: (
        <div>
            <p>1. 服务条款</p>
            <p>2. 服务条款</p>
        </div>
        ),
        onOk() {},
    });
    }
      
    function    tnc2() {
        Modal.info({
            title: '免责条约',
            content: (
            <div>
                <p>3.免责条约</p>
                <p>4.免责条约</p>
            </div>
            ),
            onOk() {},
        });
    }


class NormalRegistrationForm extends React.Component {
        constructor(props) {
        super(props);
        this.state = {
            // 提交按钮
            loading: false,
        }  
        this.captchaUrl = RenderHelper.getCaptchaImgUrl();            
        this.props.dispatch({ type: "global/setTitle", payload: title || this.title });
        this.referral = props.location.query.referral;
    }

    refreshCaptcha() {
        let captcha = this.props.form.getFieldInstance("captcha");
        captcha.clearInput();
        captcha.inputRef.focus();
        this.refs.captcha_img.src = RenderHelper.getCaptchaImgUrl();
    }

    componentWillReceiveProps(newProps) {
        if (newProps.params != this.props.params)
            this.referral = newProps.params.referral;
    }

    /* ---------------------------- Events ------------------------------*/

    handleSubmit(e) {
        e.preventDefault();

        const { dispatch } = this.props;

        this.form.validateFields((errors, values) => {
            if(values.agreement == false)
            { 
                Toast.fail("请阅读并同意服务条款与免责条约",3);

                //message.error("请阅读并同意服务条款与免责条约"); 
                return;
            } 
            if (errors) return;

            dispatch({
                type: "user/createUser",
                payload: { email: values.email, password: values.password, qq: values.qq, mobile: values.mobile, captcha: values.captcha, referral: this.referral }
            }).then((act) => {
                this.setState({ loading: false });
                if (act.code == 0) {
                    Auth.login(act.data);
                    alert('注册成功', '恭喜，你已经注册成功！', [
                        { text: 'Cancel', onPress: () => console.log('cancel') },
                        { text: 'Ok', onPress: () => {router.push("/");} },
                      ])

                    //Modal.success({ title: "注册成功", onOk() { router.push("/"); } });
                }
                else {
                    this.refreshCaptcha();
                    Toast.fail(act.message,2);
                    //message.error(act.message);
                } 


                // const resp = act.response;

                // if (act.state == ActionState.Failure) {
                //     Message.error(act.error);
                // } else if (act.state == ActionState.Success) {
                //     const resp = act.response;
                //     if (resp.code == 0) {
                //         Auth.login(resp.data);
                //         Modal.success({ title: "注册成功", onOk() { browserHistory.push("/bet"); } });
                //     } else {
                //         this.refreshCaptcha();
                //         Message.error(resp.message);
                //     }
                // }

            });

            this.setState({ loading: true });
        })
    }

    checkPassword(rule, value, callback) {
        if (value && value !== rule.getFieldValue('password')) {
            callback(rule.message);
        } else {
            callback();
        }
    }

    checkMobile(rule, value, callback) {
        if (value && value !== rule.getFieldValue('mobile')) {
            callback(rule.message);
        } else {
            let isInt = new RegExp("/^[1-9]*[1-9][0-9]*$/");
            let nReg = new RegExp("^1\\d{10}$");
            if (!nReg.test(rule.getFieldValue('mobile'))&& !isInt.test(rule.getFieldValue('mobile'))) {
                callback("请输入正确的手机号码!");
            }
            else
            {
                callback();
            }
        }
    }

    checkQQ(rule, value, callback) {
        if (value && value !== rule.getFieldValue('qq')) {
            callback(rule.message);
        } else {
            let nReg = new RegExp("\\d{5,}$");
            if (!nReg.test(value)) {
                callback("请输入正确的QQ号码!");
            }
            else
            {
                callback();
            }
        }
    }


    checkEmail(rule, value, callback) {

        if(value == undefined){
            callback("邮箱不能为空!");
            return;
        }
            
        this.props.dispatch({
            type: "user/checkUsername",
            payload: { email: value }
        }).then(act => {
            const existed = act;
            if (existed) {
                callback("该邮箱已被注册!");
            }
            callback();
        });
    }

    render() {
        const { getFieldDecorator, getFieldProps } = this.props.form;
        return (
            <div className="registration-page">
                <div className="form">
                    
                <Form form={this.props.form} ref={el => this.form = el} onSubmit={this.handleSubmit.bind(this)}>         
               
                
                <InputItem
                    {...getFieldProps('email', {
                        rules: [{
                            required: true, message: "请输入邮箱"
                        }
                        , {
                            validator: this.checkEmail.bind(this), message: "该邮箱已被注册!",
                            getFieldValue: this.props.form.getFieldValue
                        }
                    ],
                    })}
                    type="email"
                    clear
                    autoComplete="off"
                    placeholder="请输入信箱">
                    <Icon style={{color: 'rgb(255,255,255)'}} type="mail" />
                </InputItem>
                <WhiteSpace/>
                <WhiteSpace/>

                 <InputItem
                    {...getFieldProps('password', {
                        rules: [{
                            required: true, message: '请输入密码'
                        }, {
                            min: 6, max: 20, message: '密码长度要求为6到20个字符之间。'
                        }],
                    })}
                    type="password"
                    clear
                    placeholder="密码">
                    <Icon style={{color: 'rgb(255,255,255)'}} type="lock" />
                </InputItem>

                 <WhiteSpace/>
                <WhiteSpace/>
                <InputItem
                    {...getFieldProps('cmf_pwd', {
                        rules: [{
                            required: true, message: "请输入确认密码"
                        }, {
                            validator: this.checkPassword, message: "两个密码不一致。",
                            getFieldValue: this.props.form.getFieldValue
                        }],
                    })}
                    type="password"
                    clear
                    placeholder="确认密码">
                    <Icon style={{color: 'rgb(255,255,255)'}} type="lock" />
                </InputItem>

                 <WhiteSpace/>
                <WhiteSpace/>

                <InputItem
                    {...getFieldProps('captcha', {
                        rules: [{ required: true, message: '请输入右侧验证码' }],
                    })}
                    maxLength="4"
                    autoComplete="off"
                    clear
                    placeholder="验证码"
                    extra={<img src={this.captchaUrl} ref="captcha_img" className="img-captcha" onClick={this.refreshCaptcha.bind(this)} />}
                >
                    <Icon style={{color: 'rgb(255,255,255)'}} type="safety-certificate" />
                </InputItem>

                 <WhiteSpace/>
                <WhiteSpace/>

                <InputItem
                    {...getFieldProps('mobile', {
                        rules: [{ required: true, message: '请输入手机' }, {
                            validator: this.checkMobile.bind(this),
                            getFieldValue: this.props.form.getFieldValue
                        }],
                    })} 
                    clear
                    autoComplete="off"
                    placeholder="手机" >
                    <Icon style={{color: 'rgb(255,255,255)'}} type="phone" />
                </InputItem>

                 <WhiteSpace/>
                <WhiteSpace/>

                <InputItem
                    {...getFieldProps('qq', {
                        rules: [{ required: true, message: '请输入QQ' }, {
                            validator: this.checkQQ.bind(this),
                            getFieldValue: this.props.form.getFieldValue
                        }],
                    })} 
                    clear
                    autoComplete="off"
                    placeholder="QQ" >
                    <Icon style={{color: 'rgb(255,255,255)'}} type="qq" />
                </InputItem>
                <WhiteSpace/>
                <WhiteSpace/>

                <Checkbox 
                    {...getFieldProps('agreement', {
                        valuePropName: 'checked',
                        initialValue: true,
                        rules: [{
                            required: true, validator: (rule, value, callback) => {
                                !value ? callback(rule.message) : callback();
                            }, message: "请阅读并同意服务条款与免责条约"
                        },],
                    })}   >
                    <span className="check-span">
                        阅读并同意<a href="#"  onClick={tnc1} >《服务条款》</a>与<a href="#"  onClick={tnc2} >《免责条约》</a>
                    </span>
                    <div className="clearfix"></div>
                </Checkbox>
                <WhiteSpace/>
                <WhiteSpace/>   
           
                <List.Item>
                    <Button loading={this.state.loading} type="primary" htmlType="submit" size="large" block>确定</Button> 
                </List.Item> 
            </Form>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        loading: state.loading.models.global
    };
}

export default connect(mapStateToProps)(Form.create()(NormalRegistrationForm));