/**
 * Routes:
 *   - ./src/routes/AuthorizationRoute.js
 */

import { connect } from 'dva';
import { message, Row, Col, Alert, Tabs, Button, Input,Icon,Card  } from "antd";
import { List, InputItem, WhiteSpace } from 'antd-mobile';

import Page from "@/layouts/PageBasic";
import Form from '@/components/Form';
import * as RenderHelper from "@/utils/render_helper";


const Item = List.Item;
const Brief = Item.Brief;


class Bipipay extends Page {
  title = "电汇指引"
  constructor(props) {
    super(props);
    this.state = {
      // 提交按钮
      loading: false,
      submitting: false,
    }
   
  }




  /* ---------------------------- Events ------------------------------*/

  handleSubmit(e) {
    e.preventDefault();

     const { dispatch } = this.props;

    this.form.validateFields((errors, values) => {

      if (errors) return;

      this.setState({ loading: false });
      this.newWin = window.open();
          setInterval(this.checkChild.bind(this), 500);
      dispatch({type:"payment/submitPayRequest", payload:{amount : values.amount}
      }).then(act => 
        {     
                var resp = act;
                if(resp.code == 0 ){
                   
                    this.setState({submitting:true});
                    let payUrl = resp.data.pay_url;
                    this.newWin.location.href=payUrl;
                
                }else{
                    this.newWin.close();
                    message.warning(resp.message);
                }

                this.setState({ loading: true });
            });    

     
      });
   // })
  }

  handleResetSubmit(e) {
    e.preventDefault();
    this.props.form.resetFields();
 
  }

  checkChild() {       
    if (this.newWin.closed) {
       this.setState({submitting: false})
       clearInterval(this.timer);
    }

}


  /* ---------------------------- Renders ------------------------------*/

  render() {
    const { getFieldDecorator, getFieldProps } = this.props.form;

    const isIPhone = new RegExp('\\biPhone\\b|\\biPod\\b', 'i').test(window.navigator.userAgent);
    let moneyKeyboardWrapProps;
    if (isIPhone) {
      moneyKeyboardWrapProps = {
        onTouchStart: e => e.preventDefault(),
    };
  }

    return (
      <div className="form_sty">
        <div className="top-header"> </div>

        <Card>
          <p>1.单笔充值金额最底10元，最高50000元。</p>
            {/* <p>1.扫一扫弹出的二维码进行充值。</p>
            <p>2.单笔充值金额最底10元，最高50000元。</p>
            <p>3.可以使用其他手机二维码进行充值，也可以将二维码保存到相册
              再使用支付宝识别相册中的二维码进行充值，该二维码仅当次有
              效，每次充值务必重新保存最新的二维码。</p> */}
          </Card>

        <div className="form_title">
          <h5>填写汇款信息</h5>
        </div>
        <Form form={this.props.form} ref={el => this.form = el} onSubmit={this.handleResetSubmit.bind(this)}>

          <InputItem
            {...getFieldProps('amount', {
              rules: [{
                required: true, message: '请输入充值金额',
              }],
            })}
            type="money"
            cleartext
            extra="USDT"
            moneyKeyboardAlign="left"
            moneyKeyboardWrapProps={moneyKeyboardWrapProps}
            placeholder="请输入充值金额">
            充值金额
          </InputItem>

          <div className="game-orders">
            <div className="ButtonBottom">
              <button className="btn_left">
                <Icon type="reload"/> 重置
              </button>

              <button className="btn_right" loading={this.state.loading} htmlType="submit" onClick = {this.handleSubmit.bind(this)} >
                确定
              </button>

            </div>
            <div className="clearfix"></div>
          </div>


        </Form>

      </div>


    );
  }
};

export default connect(state => ({

 
}))(Form.create()(Bipipay));
