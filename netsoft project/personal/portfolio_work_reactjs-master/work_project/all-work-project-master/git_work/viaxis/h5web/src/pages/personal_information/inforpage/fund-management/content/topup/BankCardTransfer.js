/**
 * Routes:
 *   - ./src/routes/AuthorizationRoute.js
 */

import { connect } from 'dva';
import { message, Row, Col, Alert, Tabs, Button, Input,Icon,Card } from "antd";
import {Picker, List, InputItem, WhiteSpace } from 'antd-mobile';

import Page from "@/layouts/PageBasic";
import Form from '@/components/Form';
import CountDownButton from "@/components/CountDownButton";
import * as RenderHelper from "@/utils/render_helper";

import banklogo1 from '../../../../../../assets/images/icon/banklogo1.png'
import banklogo2 from '../../../../../../assets/images/icon/banklogo2.png'
import banklogo4 from '../../../../../../assets/images/icon/banklogo4.png'
import banklogo5 from '../../../../../../assets/images/icon/banklogo5.png'


const Item = List.Item;
const Brief = Item.Brief;



const colorStyle = {
  display: 'inline-block',
  verticalAlign: 'middle',
  width: '20px',
  height: '20px',
  marginRight: '10px',
};



const iconimag = [
  {
    label:
      (<div>
        <img src={banklogo1}
             style={{ ...colorStyle, }}
        />
        <span>中国银行</span>
      </div>),
    value: '1',
  },
  {
    label:
      (<div>
        <img src={banklogo2}
             style={{ ...colorStyle, }}
        />
        <span>农业银行</span>
      </div>),
    value: '2',
  },
  {
    label:
      (<div>
        <img src={banklogo4}
             style={{ ...colorStyle, }}
        />
        <span>工商银行</span>
      </div>),
    value: '3',
  },
  {
    label:
      (<div>
        <img src={banklogo5}
             style={{ ...colorStyle, }}
        />
        <span>建设银行</span>
      </div>),
    value: '4',
  },
];



class BankCardTransfer extends Page {
  title = "银行卡支付";
  constructor(props) {
    super(props);
    this.state = {
      // 提交按钮
      loading: false,
      // 发送验证码按钮
      captchaSending: false,
      // 发送按钮倒计时
      target: 0,
    }
    // 初始验证码地址,否则每次表单输内容验证码都会变化
    this.captchaUrl = RenderHelper.getCaptchaImgUrl();
  }

  state = {
    iconValue: ['#00FF00'],
  };


  /* ---------------------------- Events ------------------------------*/

  handleResetSubmit(e) {
    e.preventDefault();

    const { dispatch } = this.props;

    this.form.validateFields((errors, values) => {
      if (errors) return;

      dispatch({
        type: "user/resetSafePassword",
        payload: { password: values.password, captcha: values.captcha }
      }).then(resp => {
        this.setState({ loading: false });

        if (resp.code === 0) {
          let msg = "重置安全密码成功";
          message.success(msg);
          this.props.form.resetFields();
        } else {
          message.error(resp.message);
        }

      });

      this.setState({ loading: true });
    })
  }

  sendCaptchaClick(e) {
    const { dispatch } = this.props;
    dispatch({ type: "user/sendTicket", payload: true }).then(resp => {
      this.setState({ captchaSending: false });
      let seconds = 0;
      if (resp.code === 0) {
        seconds = 60;
        message.success("验证码已发送到您的邮箱。");
      } else {
        seconds = resp.data;
        message.error(resp.message);
      }
      if (seconds > 0)
        this.setState({ target: new Date().getTime() + seconds * 1000 });

    });
    this.setState({ captchaSending: true });
  }


  onChangeIcon = (icon) => {
    this.setState({
      iconValue: icon,
    });
  };
  /* ---------------------------- Renders ------------------------------*/

  render() {
    const { getFieldProps } = this.props.form;

    return (

      <div className="form_sty form-countdown">
        <div className="top-header"> </div>
        <div className="form_title ">
          <h5>选项列表</h5>
        </div>

        <Form form={this.props.form} ref={el => this.form = el} onSubmit={this.handleResetSubmit.bind(this)}>

          <List >

            <Picker
              data={iconimag}
              value={this.state.iconValue}
              cols={1}
              onChange={this.onChangeIcon }
            >
              <List.Item arrow="horizontal">付款银行</List.Item>
            </Picker>


          </List>


          <div className="form_title">
            <h5>填写银行信息</h5>
          </div>
          <InputItem
            {...getFieldProps('current_name', {
              rules: [{
                required: true, message: '请输入付款用户名',
              }],
            })}
            type="password"
            clear
            placeholder="请输入付款用户名">
            付款用户名
          </InputItem>

          <InputItem
            {...getFieldProps('current_bankcard', {
              rules: [{
                required: true, message: '请输入银行卡号',
              }],
            })}
            type="password"
            clear
            placeholder="请输入银行卡号">
            银行卡号
          </InputItem>


          <InputItem
            {...getFieldProps('current_money', {
              rules: [{
                required: true, message: '请输入充值金额',
              }],
            })}
            type="password"
            clear
            placeholder="请输入充值金额">
            充值金额
          </InputItem>



          <InputItem
            {...getFieldProps('captcha', {
              rules: [{ required: true, message: '请输入您接收到验证码' }],
            })}
            maxLength="6"
            clear
            placeholder="邮箱验证码"
            extra={<CountDownButton loading={this.state.captchaSending}
                                    size="small" type="primary"
                                    target={this.state.target}
                                    onClick={this.sendCaptchaClick.bind(this)}>
              发送验证码
            </CountDownButton>}
          >
            验证码
          </InputItem>


          <Card>
            <p>1.请转账到以上收款银行账户。</p>
            <p>2.请正确填写转账银行卡的持卡人姓名和充值金额，以变及时刻对。</p>
            <p>3.转账1笔提交1次，请勿重复提交订单。</p>
            <p>4.请务必转账后再提交订单，否则无法及时查到您的款项！</p>
          </Card>

          <div className="game-orders">
            <div className="ButtonBottom">
              <button className="btn_left">
                <Icon type="reload"/> 重置
              </button>

              <button className="btn_right" loading={this.state.loading} htmlType="submit" >
                确定
              </button>

            </div>
            <div className="clearfix"></div>
          </div>
        </Form>
      </div>
    );
  }
};

export default connect(state => ({

  loading: state.loading.models.user,
}))(Form.create()(BankCardTransfer));
