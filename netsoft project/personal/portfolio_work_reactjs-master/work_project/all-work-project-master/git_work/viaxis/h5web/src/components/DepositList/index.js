import React, { Component } from 'react';
import {Icon} from "antd";

import * as Enums from "@/app_enum";


class DepositList extends Component
{
    constructor(props){
        super(props);
    }

    render(){
        const {depositData} = this.props;

        return(
            <div class="popup_new">
                            <div className="popup-inner ">
                                <h2>充值记录</h2>

                                <div class="content">
                                    <table>
                                        <thead>
                                            <tr className="tr-style">
                                                <th>Id:</th>
                                                <th>状态:</th>
                                                <th>金额:</th>
                                                <th>申请时间:</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr className="tr2-style">
                                                <td
                                                style={{
                                                    color: 'blue'
                                                }}
                                                >{depositData.Id}</td>
                                                <td >{Enums.DepoisitRequestStatus[depositData.Status]}</td>
                                                <td ><span>￥</span>{depositData.Amount}</td>
                                                <td>{depositData.CreateTime}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            {/* <a class="close" href="#">
                                <div className="btn-popup close">
                                    <h4>关闭</h4>
                                </div>
                            </a> */}
                        </div>


        )
    }

}

export default DepositList;
