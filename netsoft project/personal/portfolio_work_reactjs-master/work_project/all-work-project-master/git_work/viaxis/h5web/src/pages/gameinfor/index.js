import React, {Component} from 'react';
import {Icon, Timeline} from "antd";
import { List } from 'antd-mobile';
import Link from 'umi/link';

import icon from '../../assets/images/icon/question.png'


const Item = List.Item;

class GameInfor extends Component {

  render() {
    return (
      <div className="game-infor">
        <div className="top-header"></div>
        <div className="modal">
          <input id="modal" type="checkbox" name="modal" tabindex="1"/>
          <div className="modal__overlay">
            <div className="top-banner">
              <div className="close-modal-btn">
                <label htmlFor="modal">
                  <Icon type="close" />
                </label>
              </div>
              <div className="infor-title">
                <h2>重庆时时彩的游戏简介？</h2>
              </div>
            </div>
            <div className="modal__box">
            <List renderHeader={() => '信息内容'} className="my-list">
              <Item>
                两面指单、双；大、小。单、双：号码为双数叫双，
                如0、2、4、6、8；号码为单数叫单，
                如1、3、5、7、9。大、小：开出之号码大于或等于5为大
                小于或等于4为小。
              </Item>
            </List>
            </div>
          </div>
        </div>
        <div className="gameinfor-list">
          <List renderHeader={() => '信息列表'}>
            <label htmlFor="modal" >
              <Item
                thumb={<img src={icon} alt="icon"/>}
                arrow="horizontal"
                onClick={() => {}}
              >
              重庆时时彩的游戏简介？
              </Item>
            </label>

            <label htmlFor="modal" >
              <Item
                thumb={<img src={icon} alt="icon"/>}
                arrow="horizontal"
                onClick={() => {}}
              >
              重庆时时彩规则里面的两面指什么？
              </Item>
            </label>
            <label htmlFor="modal" >
              <Item
                thumb={<img src={icon} alt="icon"/>}
                arrow="horizontal"
                onClick={() => {}}
              >
              重庆时时彩规则里面的特殊号码指什么？
              </Item>
            </label>
            <label htmlFor="modal" >
              <Item
                thumb={<img src={icon} alt="icon"/>}
                arrow="horizontal"
                onClick={() => {}}
              >
              重庆时时彩规则里面的龙虎斗指什么？
            </Item>
            </label>
            <label htmlFor="modal" >
              <Item
                thumb={<img src={icon} alt="icon"/>}
                arrow="horizontal"
                onClick={() => {}}
              >
              重庆时时彩规则里面的总和指什么？
            </Item>
            </label>
            <label htmlFor="modal" >
              <Item
                thumb={<img src={icon} alt="icon"/>}
                arrow="horizontal"
                onClick={() => {}}
              >
              重庆时时彩规则里面的全5中1指什么？
              </Item>
            </label>
          </List>
        </div>
      </div>
    );
  }
}

export default GameInfor;
