import React, { Component } from 'react';
import { connect } from 'dva';
import Page from "@/layouts/PageBasic";
import Form from '@/components/Form';
import { List } from 'antd-mobile';

import * as BetHelper from "@/utils/bet_helper";
import User from '@/components/Report/User';
import * as RenderHelper from "@/utils/render_helper";
import DateRange from '@/components/DateRange';

import Link from 'umi/link';

const Item = List.Item;
const Brief = Item.Brief;



class ProfitUser extends Page {
  title = "用户盈亏"

  constructor(props) {
    super(props);
    this.querys = {}
  }
  

  /* ---------------------------- System  ------------------------------*/
  componentDidMount() {
    this.queryData();
  }

  
  /* ---------------------------- Functions  ------------------------------*/

  queryData() {
    this.props.dispatch({ type: "profit/queryProfitUser", payload: {userId: this.userId ,querys: this.querys }  })  
  }


  getUserId() {
    let userId = this.props.userId
    if (userId)
        userId = parseInt(userId);
    return userId;
  }


  render() {
    const profit = this.props.profit || [];
    const showDetail = (profit) => {
      const dialog = RenderHelper.dialog(null,
          <User profitData={profit}  close={ ()=> close() } />);

      const close = () => dialog.destroy();
    }
    
    return (
      <div className="fund-history ">
        <div className="top-header"> </div>

        {/* date */}
        {/* <List className="date-select">
          <Item><DateRange/></Item>
        </List> */}
        {/* {profit[0] ?  
        <div className="form_title">
          <h5>股东</h5>
        </div>
        : <div>暂无记录</div>} */}
        <div className="form_title">
          <h5>用户盈亏</h5>
        </div>
        {profit[0] ? 
        <div>
        <List className="my-list ">
        {profit && profit.map((element,i) => 

            <Item
              key={i}
              extra={
                <div className="bet-money add">
                  {/* {(element.ChildCount > 0) ? <span>￥{element.Balance} ></span> : <span>￥{element.Balance}</span>} */}
                  {element.Profit < 0 ? <span style={{color:'red'}}>￥{element.Profit}</span> : <span>￥{element.Profit}</span> }
                </div>
              }
              align="top"
              multipleLine
              onClick={() => {showDetail(element)}}
            >
              {/* <span className="title-warp"><Link to={`/personal_information/inforpage/agency/content/ChaKanXiaJi/daiLi?token=${ element.UserId }`}>{element.Email}</Link></span> */}
              <span className="title-warp">{element.Email}</span>
              <span className="title-warp">{element.Agents}</span>
              <Brief style={{fontSize: '12px', color: '#cccccc'}}>{element.Date}</Brief>
            </Item>
                             
        )} 
        </List>
        </div>
        : <div>暂无记录</div>}
      </div>
    );
  }
};

function mapStateToProps(state) {
  return {
    profit: state.profit.profitUser.Data,
  }
}

export default connect(mapStateToProps)(Form.create()(ProfitUser));
