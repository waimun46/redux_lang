import * as billService from '@/services/bill';

export default {
    namespace: 'withdraw',
    state: {
        info: {},
        historys: {
            PageSize: 0,
            PageIndex: 0,
            PageCount: 0,
            TotalCount: 0,
            Totals: null,
            Data: []
        }
    },
    reducers: {
        saveWithdrawInfo(state, { payload }) {
            return { ...state, info: { ...payload.data } };
        },
        saveWithdrawList(state, { payload }) {
            return { ...state, historys: { ...payload.data } };
        },
    },
    effects: {
        *addWithdraw({ payload: { bankId, amount, password } }, { call, put }) {
            const data = yield call(billService.addWithdraw, bankId, amount, password);
            return data;
        },
        *getWithdrawInfo({ payload }, { call, put }) {
            const data = yield call(billService.getWithdrawInfo);
            yield put({
                type: 'saveWithdrawInfo', payload: data
            });
        },
        *queryWithdraw({ payload }, { call, put }) {
            const data = yield call(billService.queryWithdraw, payload);
            yield put({
                type: 'saveWithdrawList', payload: data
            });
        },
    
    },
};
