import { connect } from 'dva';
import PropTypes from 'prop-types';
import React from 'react';
import { Icon, Affix, Button } from 'antd';

import BasicComponent from "@/components/BasicComponent";
import * as Utils from "@/utils/common";

/**
 * [ Show countdown ]
 * @param {[number]} timestamp(ms) of end date, Orders of magnitude
 */
class Countdown extends BasicComponent {

    constructor(props) {
        super(props);
        this.state = {
            accept_bet: false,
            seconds: 0
        }
    }

    static propTypes = {
        // 游戏组
        Group: PropTypes.string,
        // 每秒倒数回调
        onTick: PropTypes.func,
        /**
         * 投注时间倒计时结束
         * @param {int} qh 期号
         */
        onBetTimeEnd: PropTypes.func,
        /**
         * 等待开奖时间倒计时结束
         * @param {int} qh 期号
         */
        onWaitTimeEnd: PropTypes.func
    }

    /* ---------------------------- 系统函数 ------------------------------*/

    /**
     * 在Render前执行
     * @return {[type]} [description]
     */
    componentWillMount() {
        this._updateState(this.props);
    }

    /**
     * 在Render后执行
     * @return {[type]} [description]
     */
    componentDidMount() {
        this._startCountdown();
    }

    componentWillUnmount() {
        clearInterval(this.interval);
        this.interval = null;
    }

    componentWillReceiveProps(nextProps) {
        let { current, next } = this.props;
        if (current != nextProps.current || next != nextProps.next) {
            // DEBUG CODE
            // console.log("current != nextProps.current : ", current != nextProps.current);
            // if(current != nextProps.current)
            //     console.log(current,nextProps.current);
            // console.log("next != nextProps.next : ", next != nextProps.next);
            // if (next != nextProps.next)
            //     console.log(next, nextProps.next);
            this._updateState(nextProps);
        }
    }

    /* ---------------------------- 自定义函数 ------------------------------*/

    _updateState(props) {
        const { current, prev, next } = props;
        if (!Utils.isEmptyObject(current)) {

            var server_now_timestamp = Utils.GetServerTimeStamp();
            let openTime = current.OpenTime.getTime() / 1000;
            let endTime = current.EndTime.getTime() / 1000;
            // 投注时间未结束
            if (endTime > server_now_timestamp) {
                this.state.accept_bet = true;
                this.state.seconds = parseInt((endTime - server_now_timestamp));
            } else {
                // 投注时间已结束，等待开奖时间倒计时
                this.state.accept_bet = false;
                this.state.seconds = parseInt((openTime - server_now_timestamp));
            }
        }

        if (!this.interval)
            this._startCountdown();
    }

    _startCountdown() {
        if (this.interval) {
            clearInterval(this.interval);
            this.interval = null;
        }
        this.interval = setInterval(this.tick.bind(this), 1000);
    }

    tick() {
        if (this.state.seconds > 0)
            this.setState({
                seconds: this.state.seconds - 1
            });
        if (this.props.onTick)
            this.props.onTick(this.state.seconds, this.state.accept_bet);

        let { current } = this.props;

        // 倒计时结束，进行下一轮倒计时
        if (this.state.seconds <= 0 && current) {
            let openTime = current.OpenTime.getTime() / 1000;
            // 本期投注倒计时未结束，转为开奖倒计时
            if (this.state.accept_bet) {
                this.state.accept_bet = false;
                var now_timestamp = Utils.GetServerTimeStamp();
                this.state.seconds = parseInt(openTime - now_timestamp);
                // 在投注倒计时结束后，发送请求，获取下一期数据
                this.props.dispatch({ type: "gamedata/getGameDataNext", payload: this.props.Group });
                this.props.onBetTimeEnd && this.props.onBetTimeEnd(current.Qh);
            } else {
                // 当前期倒计时(开奖)结束，如果存在下一期数据，替换下一期数据为当前期
                let { next } = this.props;
                if (!Utils.isEmptyObject(next)) {
                    this.state.accept_bet = true;
                    var now_timestamp = Utils.GetServerTimeStamp();
                    this.state.seconds = parseInt(next.EndTime - now_timestamp);
                } else {
                    this.props.dispatch({ type: "gamedata/getGameDataCurrent", payload: this.props.Group });
                    // 清除计数器，避免不断调用
                    clearInterval(this.interval);
                    this.interval = null;
                }
                this.props.dispatch({ type: "gamedata/closeCurrentGameData", payload: this.props.Group });
                this.props.onWaitTimeEnd && this.props.onWaitTimeEnd(current.Qh);
            }
        }
    }

    /* ---------------------------- Renders ------------------------------*/

    renderTime() {
        var seconds = parseInt(this.state.seconds % 60, 10).toString();
        var minutes = (parseInt(this.state.seconds / 60, 10) % 60).toString();
        var hours = parseInt(this.state.seconds / 3600, 10).toString();

        if (seconds.length < 2)
            seconds = "0" + seconds;
        if (minutes.length < 2)
            minutes = "0" + minutes;
        if (hours.length < 2)
            hours = "0" + hours;

        return (
            <div className="game-summary-countdown-time">
                <span>{hours[0]}</span>
                <span>{hours[1]}</span>
                <b>:</b>
                <span>{minutes[0]}</span>
                <span>{minutes[1]}</span>
                <b>:</b>
                <span>{seconds[0]}</span>
                <span>{seconds[1]}</span>
            </div>
        );
    }

    /**
     * 目前是否可接受投注状态
     */
    get AcceptBet() {
        return this.state.accept_bet || false;
    }

    render() {
        const { current } = this.props;

        return (
            <div className="game-summary-countdown">
                <div className={"game-summary-countdown-qh" + (this.AcceptBet ? "" : " waiting")}>
                    第 <b>{current && current.Qh || <span><Icon type="loading" />loading...</span>}</b> 期
                    <span>{this.AcceptBet ? "投注时间" : "开奖时间"}</span>
                </div>
                {this.renderTime()}
            </div>
        );
    }
}

export default connect(state => ({
    current: state.gamedata.current,
    next: state.gamedata.next,
    prev: state.gamedata.prev
}))(Countdown);
