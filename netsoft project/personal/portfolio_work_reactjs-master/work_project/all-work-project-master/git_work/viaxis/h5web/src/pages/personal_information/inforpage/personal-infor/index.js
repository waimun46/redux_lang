/**
 * Routes:
 *   - ./src/routes/AuthorizationRoute.js
 */

import { connect } from 'dva';
import Link from 'umi/link';
import { message, Row, Col, Alert, Tabs, Button, Input,Icon,Card  } from "antd";
import { List, InputItem, WhiteSpace } from 'antd-mobile';

import * as Util from '../../../../utils/common';

import Page from "@/layouts/PageBasic";
import Form from '@/components/Form';


const Item = List.Item;
const Brief = Item.Brief;


class PersonalInfo extends Page {
  title = "个人信息"

  constructor(props) {
    super(props);
    
    }

  componentDidMount() {
      this.getUserInfo();
      
  }

   getUserInfo() {
    this.props.dispatch({ type: "user/queryUserInfo" });


        // this.props.dispatch({ type: "user/queryUserInfo" }).then(()=>{
        //   let user = this.props.userInfo;
        //   let realName = Util.getMapValue(user,'RealName');
          
        // });
    }


  render() {  

  const userInfo = this.props.userinfo;
  let realName=undefined;
  let isMailVerified = false;
  let isPhoneVerified = false;
  if(!Util.isEmptyObject(userInfo))
  {
    realName = Util.getMapValue(userInfo,'RealName');
    isMailVerified = userInfo.IsMailVerified;
    isPhoneVerified = userInfo.IsPhoneVerified;
  }

    return (
      <div className="form_sty">
        <div className="top-header"> </div>
        <div className="form_title">
          <h5></h5>
        </div>
        <Form form={this.props.form} ref={el => this.form = el} >
          <InputItem
            value={realName==undefined ?'未填写':realName}
            editable={false}
            extra={realName==undefined ?<Link to="/personal_information/inforpage/realname" size="small"  style={{ color:'#E63232'}}>实名认证</Link>: <p></p> }
          >用户名</InputItem>

          <InputItem
            maxLength="6"
            clear
            editable={false}
            placeholder="请输入手机号码"
            extra={ isPhoneVerified? <Link to="/personal_information/inforpage/personal-infor/edit/phone"
            size="small"  style={{ color:'#E63232'}}>
            修改
            </Link> : <Link to="/personal_information/inforpage/security-center/content/BindPhone"
            size="small"  style={{ color:'#E63232'}}>
            认证手机
            </Link>}
            value={userInfo&&userInfo.Mobile}
          >
            手机号码
          </InputItem>


          <InputItem
            maxLength="6"
            clear
            editable={false}
            placeholder="请输入邮箱地址"
            extra={ isMailVerified ? <p></p> :
            <Link to="/personal_information/inforpage/security-center/content/BindEmail"  size="small"  style={{ color:'#E63232'}}>
            认证邮箱
           </Link>}
            value={userInfo&&userInfo.Email}
          >
            邮箱地址
          </InputItem>



          <InputItem
            type=""
            cleartext
            editable={false}
            value={userInfo&&userInfo.QQ}
            placeholder="请输入QQ账号"
            extra={<Link to="/personal_information/inforpage/personal-infor/edit/qqnum" 
                    size="small"  style={{ color:'#E63232'}}>
                    修改
                    </Link>}
            >
            QQ账号
          </InputItem>


          {/* <div className="game-orders">
            <div className="ButtonBottom">
              <button className="btn_left">
                <Icon type="reload"/> 重置
              </button>

              <button className="btn_right"  >
                确定
              </button>

            </div>
            <div className="clearfix"></div>
          </div> */}


        </Form>

      </div>


    );
  }
};

export default connect(state => ({
  userinfo: state.user.user
}))(Form.create()(PersonalInfo));