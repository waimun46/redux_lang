import { connect } from 'dva';
import { Icon } from "antd";
import Link from 'umi/link';
import { List, InputItem, WhiteSpace,  Toast } from 'antd-mobile';
import router from 'umi/router';


import Page from "@/layouts/PageBasic";
import Form from '@/components/Form';


class EditQQNumber extends Page{
    title = "修改QQ账号"
    constructor(props) {
      super(props);
      this.state = {
        loading: false,
      }
    }

    componentDidMount() {
        this.getUserInfo();
    }
  
    getUserInfo() {
      this.props.dispatch({ type: "user/queryUserInfo" });
  
    }

    handleResetSubmit(e) {
        e.preventDefault();
        this.props.form.resetFields(); 
    }

    handleUpdateSubmit(e) {
        e.preventDefault();
    
        const { dispatch } = this.props;
        const userInfo = this.props.userinfo;

        let currentQQ = userInfo.QQ;
    
        this.form.validateFields((errors, values) => {
    
          if (errors) return;
    
          dispatch({
            type: "user/updateQQNumber",
            payload: { currentQQ: currentQQ, newQQ: values.newQQ }
          }).then(resp => {
            this.setState({ loading: false });
            if (resp.code == 0) {
              let msg = "修改QQ账号成功！";
              Toast.success(msg);
           
              this.props.form.resetFields();
              router.goBack();
            } else {
              //this.refreshCaptcha();
              Toast.fail(resp.message);
             
            }
    
          });
    
          this.setState({ loading: true });
        })
    }

    render(){
        const userInfo = this.props.userinfo;
        const { getFieldDecorator, getFieldProps } = this.props.form;
        
        return (
            <div className="form_sty form-list-style">
                <div className="top-header"> </div>
                <div className="form_title">
                <h5>填写修改信息</h5>
                </div>
                <Form form={this.props.form} ref={el => this.form = el} onSubmit={this.handleUpdateSubmit.bind(this)}>
                    <InputItem
                        type=""
                        cleartext
                        value={userInfo && userInfo.QQ}>
                        原QQ账号
                    </InputItem>
                    <InputItem
                        {...getFieldProps('newQQ', {
                        rules: [{
                            required: true, message: '请输入新QQ账号',
                        }],
                        })}
                        type=""
                        cleartext
                        placeholder="请输入新QQ账号">
                        新QQ账号
                    </InputItem>
                    <div className="game-orders">
                        <div className="ButtonBottom">
                        <button className="btn_left" onClick={this.handleResetSubmit.bind(this)}>
                            <Icon type="reload"/> 重置
                        </button>

                        <button className="btn_right" loading={this.state.loading} htmlType="submit" >
                            确定
                        </button>
                        </div>
                        <div className="clearfix"></div>
                    </div>
                </Form>
            </div>
        );
    }
}
export default connect(state => ({
     loading: state.loading.models.user,
     userinfo: state.user.user
   }))(Form.create()(EditQQNumber));
   