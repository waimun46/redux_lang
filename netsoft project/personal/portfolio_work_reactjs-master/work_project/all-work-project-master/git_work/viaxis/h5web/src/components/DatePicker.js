import 'rmc-picker/assets/index.css';
import 'rmc-picker/assets/popup.css';
import PropTypes from 'prop-types';
import React from 'react';
import ReactDOM from 'react-dom';
import Popup from 'rmc-picker/lib/Popup';
import { Icon } from 'antd';
import { DatePickerView } from 'antd-mobile';
import * as Utils from '@/utils/common';

export default class DatePicker extends React.PureComponent {

    static defaultProps = {
        format: "yyyy-MM-dd",
        okText: "确定",
        dismissText: "取消",
        title: "",
    }

    state = {
        value: this.props.value,
    };

    onChange = (value) => {
        this.value = value;
        // this.setState({ value });
    };

    onValueChange = (...args) => {
        console.log(args);
    };

    onOk = (value) => {
        console.log(this.value);
        this.setState({ value: this.value });
        this.props.onChange && this.props.onChange(this.value);
        this.props.onOk && this.props.onOk(this.value);
    }

    render() {

        let { format, ...props } = this.props;


        const popupContent = (
            <DatePickerView mode="date"
                value={this.value}
                {...props}
                onChange={this.onChange}
            // onValueChange={this.onValueChange}
            />
        );

        const { value } = this.state;

        return (
            <Popup
                className="fortest"
                transitionName="rmc-picker-popup-slide-fade"
                maskTransitionName="rmc-picker-popup-fade"
                content={popupContent}
                title={this.props.title}
                dismissText={this.props.dismissText}
                onDismiss={this.props.onDismiss}
                okText={this.props.okText}
                onOk={this.onOk}
            >
                <span>
                    <Icon type="calendar" />
                    {value ? Utils.dateFormat(value, format) : (this.props.title || '日期')}
                </span>
            </Popup>
        );
    }
}

DatePicker.propTypes = {
    onOk: PropTypes.func,
    onDismiss: PropTypes.func,
    okText: PropTypes.string,
    dismissText: PropTypes.string,
}