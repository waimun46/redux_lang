/**
 * Routes:
 *   - ./src/routes/AuthorizationRoute.js
 */

import { connect } from 'dva';
import { message, Row, Col, Alert, Tabs, Button, Input,Icon } from "antd";
import { Flex, List, InputItem, WhiteSpace, Toast } from 'antd-mobile';
import router from 'umi/router';

import Page from "@/layouts/PageBasic";
import Form from '@/components/Form';
import * as RenderHelper from "@/utils/render_helper";


class SafePassword extends Page {
  title = "实名认证"
  constructor(props) {
    super(props);
    this.state = {
      // 提交按钮
      loading: false,
    }

  }






  handleUpdateSubmit(e) {
    e.preventDefault();

    const { dispatch } = this.props;

    this.form.validateFields((errors, values) => {

      if (errors) return;

      dispatch({
        type: "user/verifyRealName",
        payload: { realName: values.realName, idNumber: values.idNumber }
      }).then(resp => {
        this.setState({ loading: false });
        if (resp.code == 0) {
          let msg = "设置实名成功";
          Toast.success(msg);
        
          this.props.form.resetFields();
          router.goBack();

        } else {
          Toast.fail(resp.message);
         
        }

      });

      this.setState({ loading: true });
    })
  }

  /* ---------------------------- Renders ------------------------------*/

  render() {
    const { getFieldDecorator, getFieldProps } = this.props.form;
    return (
      <div className="form_sty ">
        <div className="top-header"> </div>
        <div className="form_title">
          <h5>填写信息</h5>
        </div>

        <Form form={this.props.form} ref={el => this.form = el} onSubmit={this.handleUpdateSubmit.bind(this)}>

          <InputItem
            {...getFieldProps('realName', {
              rules: [{
                required: true, message: '请输入真实姓名',
              }],
            })}
            
            clear
            placeholder="请输入真实姓名">
            真实姓名
          </InputItem>


          <InputItem
            {...getFieldProps('idNumber', {
              rules: [{
                required: true, message: '请输入身身份证号码',
              }],
            })}
            type="number"
            clear
            placeholder="请输入身身份证号码">
           身份证号码
          </InputItem>



          <div className="game-orders">
            <div className="ButtonBottom">
              <button className="btn_left">
                <Icon type="reload"/> 重置
              </button>

              <button className="btn_right" loading={this.state.loading} htmlType="submit" >
                确定
              </button>

            </div>
            <div className="clearfix"></div>
          </div>

        </Form>
      </div>
    );
  }
};

export default connect(state => ({
  loading: state.loading.models.user,
}))(Form.create()(SafePassword));
