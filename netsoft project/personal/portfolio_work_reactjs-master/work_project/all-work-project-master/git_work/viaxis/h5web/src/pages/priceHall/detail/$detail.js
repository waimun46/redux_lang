
import { connect } from 'dva';
import { List,Pagination,Icon } from 'antd-mobile';

import * as betHelper from "../../../utils/bet_helper";
import $ from 'jquery';

import Page from "../../../layouts/PageBasic";
import Form from '../../../components/Form';


const Item = List.Item;
const Brief = Item.Brief;
const title = "开奖数据"

class GameResultDetail extends Page {

    componentWillUpdate() {
        $(document).ready(function() {
            /*北京快乐8分*/
            if(window.location.href.match('/3')){
                $(".jscall").addClass("pricedata_list_square");
            }
            
            /*KENO*/
            if(window.location.href.match('/5')){
                $(".jscall").addClass("pricedata_list_square");
            }

            /*北京PK10*/
            if(window.location.href.match('/9')){
                $(".jscall").addClass("pricedata_list_square");
            }
  
        });  
    }

    constructor(props){
        super(props);
       
        this.groupId = this.getGameGroupId(this.props);
        this.group = betHelper.getGameGroupByGroupId(this.groupId);
        this.querys = this.defaultQuerys = Object.assign({},{ not_trace: true });

       
    }



    componentWillMount(){

       this.querys = Object.assign(this.querys, this.querys,{PageIndex:1,Finished:1});
       this.queryGameHistoryResult();

       let pageTitle = betHelper.getGameNameByGroupId(this.groupId);
       this.props.dispatch({ type: "global/setTitle", payload: pageTitle  });

    }

    getThumbImagesByGroupId(id)
    {
        switch(id){
            case 1: return (<img className="logosizee" src={fc3d} alt="fc3d" />);
            break;
            case 2: return (<img className="logosizee" src={ssc} alt="ssc" />);
                break;
            case 3: return (<img className="logosizee" src={kl8} alt="kl8" />);
                break;
            case 4: return (<img className="logosizee" src={xylc} alt="kl10" />);
                break;
            case 5: return (<img className="logosizee" src={keno} alt="KENO" />);
                break;
            case 6: return (<img className="logosizee" src={pl3} alt="pl3" />);
                break;
            case 7: return "分分彩";
                break;
            case 8: return "真人彩";
                break;
            case 9 : return (<img className="logosizee" src={pk10} alt="pk10" />);
            break;
        }
    }

    splitNum(num) {
        let num1 = num.split(",");
        let num2 = [];
        for(let i = 0; i < num1.length; i++) {
          num2.push(num1);
          return num2;
        }
      }


    queryGameHistoryResult(){
        this.props.dispatch({ type: "gamedata/queryGameData",payload: { group_name: this.group, querys: this.querys  }})

    }

    getGameGroupId(props) {
        let groupId = props.match.params.detail;
        if (groupId)
            groupId = parseInt(groupId);
        return groupId;
    }

    getNumberOfPages(total, pageSize){

        var pg = Math.floor(parseInt(total)/parseInt(pageSize));
        if((parseInt(total)%parseInt(pageSize))!=0)
          pg+=1;
      
          return pg;
      
       }
      
       onPaginationChange(val){
         console.log(val);
         Object.assign(this.querys,this.querys, {PageIndex: val});
         this.queryGameHistoryResult();
       }

    render(){
        let results = this.props.data || [] ;
        let page = this.getNumberOfPages(results.TotalCount, results.PageSize);
        let idx = results.PageIndex;


  
        return(
            <div className="fund-history ">
            <div className="top-header"></div>


            
            <List className="my-list ">
            {
              results.Data &&  results.Data.map(item=>{

                    return(
                        <Item  extra={
                            <div className="bet-money add">
                              {/* <span>第{item.Qh}期 </span>
                              <Brief style={{fontSize: '14px', color: '#cccccc'}}>{item.OpenTime}</Brief> */}
                               
                            </div>
                          }
                          align="top"
                         // thumb={this.getThumbImagesByGroupId(item.GroupId)}
                          multipleLine
                          className="jscall"
                          >
                           <span className="title-warp"> 
                                第<span style={{color: 'red'}}>{item.Qh}</span>期
                           </span>
                           
                           {/* <span className="title-warp" style={{ color:'black' ,fontSize:'20px'}}>{betHelper.getGameNameByGroupId(item.GroupId)}</span> */}
                           {
                               this.splitNum(item.Result).map((element) => 
                               { return((element=='') ?  <span className="title-warp">等待开奖中。。。</span> :
                               <Brief>
                               <ul>
                                   {element.map((e) => <li>{e}</li>)}
                               </ul>
                                </Brief>)
                            
                            
                            }
                              
                            )}
                          <Brief style={{fontSize: '12px', color: '#cccccc'}}>{item.OpenTime}</Brief>
        
                        </Item>
                        
                    )



                })
            }
            
            
            
            </List>
            {
            (page < 2) ? <p></p>:
            <Pagination total={page}
              className="custom-pagination-with-icon"
              current={idx}
              locale={{
                prevText: (<span className="arrow-align"><Icon type="left" />上一页</span>),
                nextText: (<span className="arrow-align">下一页<Icon type="right" /></span>),
              }}
              onChange={this.onPaginationChange.bind(this)}
           />
          }
            </div>
        )
    }



}

export default connect(state => ({

    latestGameResults : state.gamedata.latestResult,
    data: state.gamedata.history.data,
   
  }))(Form.create()(GameResultDetail));

