import { PureComponent } from 'react';
import { Menu, Icon, Switch } from 'antd';
import { NavBar, Icon as MIcon, Drawer, List } from 'antd-mobile';
import router from 'umi/router';
import Link from 'umi/link';
import Auth from '@/utils/auth';
import NavMenu from './NavMenu';

class Header extends PureComponent {
    constructor(props){
        super(props);

        state = {
            open: true,
            home: false
        }
    }

    componentWillMount(){
        if(this.context.router.getCurrentPathName()=="/")
        {
            this.setState({home : true })
        }

    }

   


    onOpenChange = (...args) => {
        this.setState({ open: !this.state.open });
    }
    render() {

        const { open , home } = this.state;
        return (
            <div>
            <div>
                {
                    home 
                    ? <p></p>
                    : <p>home</p>

                    
                }


            </div>
          
            {/* <div>
                <NavBar
                    mode="dark"
                    icon={<MIcon type="left" />}
                    rightContent={[
                        <MIcon key="more" type={open ? "cross" : "ellipsis"} onClick={this.onOpenChange} />
                    ]}
                    onLeftClick={() => router.goBack()}
                >Home</NavBar>
                <NavMenu isOpen={this.state.open}
                    onOpenChange={this.onOpenChange}>
                    {this.props.children}
                </NavMenu>
            </div > */}
            </div>
        );
    }

    get navMenus() {
        return <Menu mode="inline" style={{ width: 256 }}>
            <Menu.Item key="1"><Link to="/"><Icon type="home" />Home</Link></Menu.Item>
            <Menu.SubMenu key="sub2" title={<span><Icon type="appstore" /><Link to="/withdraw">Withdraw</Link></span>}>
                <Menu.Item key="5"><Link to="/withdraw/history">Withdraw History</Link></Menu.Item>
            </Menu.SubMenu >
            <Menu.SubMenu key="sub4" title={<span><Icon type="setting" /><span>Navigation Three</span></span>}>
                <Menu.Item key="9">Option 9</Menu.Item>
                <Menu.Item key="10">Option 10</Menu.Item>
                <Menu.Item key="11">Option 11</Menu.Item>
                <Menu.Item key="12">Option 12</Menu.Item>
            </Menu.SubMenu>
            <Menu.Item key="/game/deals">
                <a href="/game/deals">Deals</a>
            </Menu.Item>
            <Menu.Item key="/404">
                <Link to="/page-you-dont-know"><Icon type="slack" />404</Link>
            </Menu.Item>

            {Auth.isLogged() ?
                <Menu.Item key="/logout">
                    <a href="/logout">Logout</a>
                </Menu.Item>
                :
                <Menu.Item key="/login">
                    <a href="/login">Login</a>
                </Menu.Item>
            }

        </Menu >;
    }
}

export default Header;
