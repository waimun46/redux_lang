import PropTypes from 'prop-types';
import React from 'react';
import ReactDOM from 'react-dom';
import { Icon } from 'antd';
import { Calendar } from 'antd-mobile';
import * as Utils from '@/utils/common';

export default class DateRange extends React.PureComponent {

    static defaultProps = {
        format: "yyyy-MM-dd",
    }

    state = {
        show: false,
        value: this.props.value,
    };

    componentWillMount(){
        const {value} = this.props;
        if(value != undefined){
            const{startTime, endTime} = value;
            if(startTime)
                this.setState({startTime:startTime});
            if(endTime)
                this.setState({endTime:endTime});
        }
       
    }


    onSelectHasDisableDate = (dates) => {
        console.warn('onSelectHasDisableDate', dates);
    }

    onEditClick = () => {
        document.getElementsByTagName('body')[0].style.overflowY = 'hidden';
        this.setState({
            show: true,
        });
    }

    onConfirm = (startTime, endTime) => {
        document.getElementsByTagName('body')[0].style.overflowY = this.originbodyScrollY;
        this.setState({
            show: false,
            startTime,
            endTime,
        });
        this.props.onChange && this.props.onChange([startTime, endTime]);
    }

    onCancel = () => {
        document.getElementsByTagName('body')[0].style.overflowY = this.originbodyScrollY;
        this.setState({
            show: false,
        });
    }

    render() {
        const now = new Date();
        const { format } = this.props;
        const { startTime, endTime } = this.state;

        return (
            <React.Fragment>
                <span class="ant-calendar-picker" tabindex="0">
                    <span class="ant-calendar-picker-input ant-input" onClick={this.onEditClick}>
                        <input readonly="" placeholder="开始日期" class="ant-calendar-range-picker-input" tabindex="-1"
                            value={startTime && Utils.dateFormat(startTime, format)} />
                        <span class="ant-calendar-range-picker-separator"> ~ </span>
                        <input readonly="" placeholder="结束日期" class="ant-calendar-range-picker-input" tabindex="-1"
                            value={endTime && Utils.dateFormat(endTime, format)} />
                        <Icon type="calendar" />
                    </span>
                </span>
                <Calendar
                    visible={this.state.show}
                    onCancel={this.onCancel}
                    onConfirm={this.onConfirm}
                    onSelectHasDisableDate={this.onSelectHasDisableDate}
                    getDateExtra={this.getDateExtra}
                    minDate={new Date(+now - 5184000000)}
                    maxDate={new Date(+now + 31536000000)}
                />
            </React.Fragment>
        );
    }
}