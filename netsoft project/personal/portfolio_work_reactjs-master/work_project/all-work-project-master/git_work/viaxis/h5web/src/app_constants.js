/**
 * 通用(时时彩、分分彩、福彩3D、排列3)每注之间的分割符（客户端）。例如134&345&643代表三注
 * @type {Array}
 */
export const NUMS_SPLIT_CHARS = [",", ";", /\s/];
/**
 * 两位数彩种(快乐8、快乐10、KENO)每注之间的分隔符(客户端)。
 * @type {Array}
 */
export const TWO_DIGITS_NUMS_SPLIT_CHARS = [";", "&", /\n/];
/**
 * 号码间分隔符(客户端)。用于双位数的彩种号码。如快乐8、快乐10、KENO等。
 * 例如:
 * 01,02,03,04,05
 * 01-02-03-04-05
 * 01 02 03 04 05&01 02 03 04 05
 * @type {Array}
 */
export const NUM_SPLIT_CHAR = [",", "-", /\s/];

/**
 * 单位位置分割符（服务端），用于需要指定投注号码所占的位置与投注号码之间的分割。
 * 例如 01110?234,834 此号码表示千百十位投234，834两注。
 * @type {String}
 */
export const UNIT_POSITION_SPLIT_CHAR = "?";
/**
 * 每注之间的分割符（服务端）。例如134&345&643代表三注
 * @type {String}
 */
export const MULTIPLE_NUMS_SPLIT_CHAR = "&";
/**
 * 用于复式每数位之间的分割（服务端）。例如三星前三直选复式，需要选择万千百单位号码，此分割符为分割不同单位之间的号码。
 * 例如: 1,234,28 代表万位1，千位234，百位28
 * 有需要，单位号码配合LAYER_CONTENT_SLPIT_CHAR进行使用。
 * 例如: 2-3,05-11-12,03-04
 * 关于数位，具体可看DigitPosition注释
 * @type {String}
 */
export const DIGIT_POS_SPLIT_CHAR = ",";
/**
 * 数位号码间的分隔符（服务端）。之所以需要采用分割符，是存在10个号码以上的彩种游戏。不加则默认为全为个位号码。
 * 例如 01-12-03,3-18-4,345 三种不同格式
 * 关于数位，具体可看DigitPosition注释
 * @type {String}
 */
export const DIGIT_POS_CONTENT_SLPIT_CHAR = "-";
/**
 * 通用数位的默认名称列表
 * @type {Array}
 */
export const DIGIT_UNIT_NAME_LIST = ["万位", "千位", "百位", "十位", "个位"];
/**
 * PK10数位名称列表
 * @type {Array}
 */
export const PK10_DIGIT_UNIT_NAME_LIST = ["冠军", "亚军", "第三名", "第四名", "第五名", "第六名", "第七名", "第八名", "第九名", "第十名"];

export const SUM_NUMS = {
    "大": "BG",
    "小": "SM",
    "单": "OD",
    "双": "EV",
    "前大": "FBG",
    "后大": "BBG",
    "和": "DRW"
};

/* ----------------------------------------- Docs ---------------------------------------------- */
/**
 * 直选格式说明
 * @type {String}
 */
export const LINEAR_STYLE_FORMAT_EXPLAIN = "每一个号码之间请用一个<b>空格</b>[  ]、<b>逗号</b>[ , ]或者<b>分号</b>[ ; ]隔开。";
export const LINEAR_STYLE_TWO_DIGIT_FORMAT_EXPLAIN = "每注号码之间请用<b>分号</b>[ ; ]、<b>与号</b>[ & ]或者<b>回车</b>隔开，每一个号码之间请用一个<b>空格</b>[ ]、<b>逗号</b>[ , ]隔开。";

export const LINEAR_STYLE_FORMAT_EXAMPLE = "请将您要选择的号码复制或输入到这里\n支持格式:\n";
export const linearStyleFormatExample = (minNum, maxNum, numCount) => {
    const sb = ["请将您要选择的号码复制或输入到这里\n支持格式:\n"];

    const isMultipleDigits = maxNum > 9;
    if (isMultipleDigits) {
        TWO_DIGITS_NUMS_SPLIT_CHARS.forEach(it => {

        });
    }
};

/**
 * 佣金说明(团队添加、修改下级佣金说明)
 * @type {String}
 */
export const COMMISSION_EXPLAIN = "您可以将您拥有的佣金下发给下级代理或会员，假如您有6.0%佣金，下发给会员5.0%，每当会员投注100，您可以获得1元佣金。向对应的，会员的佣金越高，他的赔率就越高。";
/**
 * 日工资说明(团队添加、修改下级日工资说明)
 * @type {String}
 */
export const DAILY_WAGES_EXPLAIN = "您可以将您的日工资分发给下级代理，日工资每天凌晨结算，当日的日工资为下级有效投注 x 日工资比例。";

/* ----------------------------------------- Error Code ---------------------------------------------- */

/**
 * 错误码说明
 * @type {Object}
 */
export const ERROR_CODES = {
    // 投注
    BET: {
        1: "无效的投注格式",
        2: "投注内容为空",
        3300: "投注异常",
        3301: "无效的期号",
        3302: "期号已过期",
        3303: "该期未接受投注",
        3304: "该期停止接受投注",
        3305: "无效的游戏类型",
        3306: "无效的投注格式",

    }
}

/* ----------------------------------------- 系统枚举 ---------------------------------------------- */

/**
 * 状态类。
 * 实例包含三个字段，分别是:
 * name   : 显示的名称
 * value  : 状态值
 * status : 状态 (用于赋给Badge或自己实现Status显示方法。)
 * 本类主要用于状态枚举，并搭配RenderHelper.render_status使用。
 */
class Status {

    constructor(name, value, status) {
        this.name = name;
        this.value = value;
        this.status = status;
    }

    toString() {
        return this.name;
    }
}

/**
 * 游戏分类
 * @type {Array}
 */
export const GameGroup = ["CQSSC", "FC3D", "KL8", "KL10", "KENO", "PL3", "FFC", "ZRC", "PK10"]

/**
 * 游戏数据状态
 * @type {Object}
 */
export const GameDataStatus = {
    0: "未开奖",
    1: "已开奖",
    2: "取消"
}

/**
 * 注单状态
 * @type {Object}
 */
export const DealStatus = {
    0: new Status("未开奖", 0, "processing"),
    1: new Status("已中奖", 1, "success"),
    2: new Status("未中奖", 2, "error"),
    3: new Status("取消", 3, "default")
}

/**
 * 输赢
 * @type {Object}
 */
export const WinLoss = {
    0: "和",
    1: "赢",
    2: "负"
}

/**
 * 流水类型
 * @type {Object}
 */
export const BillFlowType = {
    0: "充值",
    1: "提现",
    2: "转账",
    3: "游戏",
    4: "佣金",
    5: "派彩",
};

/**
 * 游戏流水子类型
 * @type {Object}
 */
export const GameSubType = {
    0: "投注",
    1: "盈利",
    2: "取消"
};


/**
 * 提现申请状态
 * @type {Object}
 */
export const WithdrawStatus = {
    0: new Status("准备中", 0, "default"),
    1: new Status("审核", 1, "processing"),
    2: new Status("审核", 2, "processing"),
    3: new Status("出金", 3, "processing"),
    4: new Status("完成", 4, "success"),
    5: new Status("拒绝", 5, "error"),
    6: new Status("取消", 6, "warning"),
}

/**
 * 用户状态
 * @type {Object}
 */
export const UserStatus = {
    0: new Status("正常", 0, "blue"),
    1: new Status("禁用", 1, "error")
}

/**
 * 用户类型
 * @type {Object}
 */
export const UserKind = {
    1: "大股东",
    2: "股东",
    3: "代理",
    4: "会员",
    // 5: "子账号"
}

/**
 * 中文银行名称
 * @type {Object}
 */
export const BankName = {
    "ICBC": "中国工商银行",
    "ABC": "中国农业银行",
    "CMB": "招商银行",
    "CIB": "兴业银行",
    "CITIC": "中信银行",
    "BOC": "中国银行",
    "BCM": "交通银行",
    "SPDB": "浦东发展银行",
    "CMBC": "民生银行",
    "HB": "华夏银行",
    "CEB": "中国光大银行",
    "BOB": "北京银行",
    "GDB": "广东发展银行",
    "PSBC": "邮政储蓄",
    "SPB": "平安银行",
    "CCB": "中国建设银行",
}

/**
 * 公告  状态
 * @type {Object}
 */
export const BulletinType = {
    0: new Status("一般讯息", 0, "blue"),
    1: new Status("警告信息", 1, "error"),
    2: new Status("紧急讯息", 2, "error")
}


/**
 * Action状态
 * @type {Object}
 */
export const ActionState = {
	// 表示开始执行Action
	Doing: "DOING",
	// 表示Action执行成功
	Success: "SUCCESS",
	// 表示Action执行失败
	Failure: "FAILURE"
};
