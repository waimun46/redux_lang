import * as depositService from '@/services/deposit';

export default {
    namespace: 'deposit',
    state: {
      info: {},
      historys: {
          PageSize: 0,
          PageIndex: 0,
          PageCount: 0,
          TotalCount: 0,
          Totals: null,
          Data: []
      }
    },
    reducers: {
        saveDepositList(state, { payload }) {
            return { ...state, ...payload.data };
        },
    },
    effects: {
        *queryDepositList({ payload, }, { call, put }) {
            const data = yield call(depositService.getDepositList, payload);
            yield put({
                type: 'saveDepositList', payload: data
            });
        },
    },
};
