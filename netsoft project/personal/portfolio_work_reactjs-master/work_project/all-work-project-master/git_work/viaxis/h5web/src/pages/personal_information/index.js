import React, {Component} from 'react';
import { connect } from 'dva';
import Page from "@/layouts/PageBasic";
import Form from '@/components/Form';

import TopInfor from './content/TopInfor';
import BottomInfor from './content/BottomInfor';

class PersonalInformation extends Page {
    title = "我的账户"

    render() {
        return (
          <div className="personal-information">
            <div className="top-header"></div>
            <TopInfor/>
            <BottomInfor/>
          </div>
        );
    }
};

export default connect(state => ({

}))(Form.create()(PersonalInformation));
