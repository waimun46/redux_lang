import { routerRedux } from 'dva/router';

export const dva = {
  config: {
    onError(err, dispatch) {
      err.preventDefault();
      if (!err.response) {
        return console.log(err.message);
      }

      const status = err.response.status;
      if (status === 401) {
        dispatch(routerRedux.push('/logout'));
      } else if (status === 403) {
        dispatch(routerRedux.push('/exception/403'));
      } else if (status <= 504 && status >= 500) {
        dispatch(routerRedux.push('/exception/500'));
      } else if (status >= 404 && status < 422) {
        dispatch(routerRedux.push('/404'));
      }

    },
  },
  plugins: [
    require('dva-logger')(),
  ],
};

