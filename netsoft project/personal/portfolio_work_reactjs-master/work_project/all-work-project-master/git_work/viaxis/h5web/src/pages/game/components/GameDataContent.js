import PropTypes from 'prop-types';
import { connect } from 'dva';
import BasicComponent from "@/components/BasicComponent";

class GameDataContent extends BasicComponent {

    static propTypes = {
        group: PropTypes.number.isRequired,
        qh: PropTypes.number.isRequired
    }

    componentWillMount() {

        const { hash, group, qh } = this.props;
        if (!this.gameData)
            this.queryData();

    }

    queryData() {
        const { group, qh } = this.props;
        this.props.dispatch({
            type: 'gamedata/getGameData',
            payload: { group, qh },
        });
    }

    get gameData() {
        const { hash, group, qh } = this.props;

        return (hash[group] && hash[group][qh]) || null;
    }

    render() {
        const gameData = this.gameData;
        return (
            this.isFetching
                ?
                <span><Icon type="loading" /> loading...</span>
                :
                <span>
                    {gameData ? gameData.Result : "找不到开奖数据"}
                </span>
        );
    }
}

export default connect(state => ({
    hash: state.gamedata.hash
}))(GameDataContent);