import { routerRedux } from 'dva/router';
import router from 'umi/router';
import * as passportService from '@/services/passport';
import * as RenderHelper from "@/utils/render_helper";

export default {
    namespace: 'user',
    state: {
        user: {},
        parents: [],
        lastLogin: {},
        banks: [],
        cards: [],
        team: [],
        hasSafePassword: null
    },
    reducers: {
        saveData(state, { payload }) {
            return { ...state, ...payload };
        },
        saveUser(state, { payload }) {
            return { ...state, user: payload.data };
        },
        saveTeam(state, { payload }) {
            return { ...state, team: payload.data };
        },
        saveLoginLog(state, { payload }) {
            return { ...state, lastLogin: payload.data };
        },
        saveBanks(state, { payload }) {
            return { ...state, banks: [...payload.data] };
        },
        saveBankCards(state, { payload }) {
            return { ...state, cards: [...payload.data] };
        },
        setSafePassword(state, { payload }) {
            return { ...state, hasSafePassword: payload };
        }
    },
    effects: {

        *queryUserInfo(action, { call, put }) {
            const resp = yield call(passportService.queryUserInfo);
            yield put({ type: "saveUser", payload: resp });
        },

        *queryTeam({ payload: {userId, pageIndex} }, { call, put }) {
            const resp = yield call(passportService.queryTeam, userId, pageIndex );
            yield put({ type: "saveTeam",payload: resp });
        },

        *getLastLoginLog(action, { call, put }) {
            const resp = yield call(passportService.getLastLoginLog);
            yield put({ type: "saveLoginLog", payload: resp });
        },

        *getParents({ payload }, { call, put }) {
            const resp = yield call(passportService.getParents, payload);
            yield put({ type: "saveData", payload: { parents: resp.data } });
        },

        /* ------------------------------ Safe Password ------------------------------- */
        *checkSafePassword(action, { select, put }) {
            let hasSafePassword = yield select(state => state.hasSafePassword);
            if (hasSafePassword === null)
                yield put({ type: 'hasSafePassword' });

            hasSafePassword = yield select(state => state.hasSafePassword);
            if (!hasSafePassword)
                RenderHelper.warning("您未设置安全密码", "您需要设置安全密码后才可继续下列的操作",
                    () => router.push("/passport/password/safe?redirect=/bank")
                );
        },
        *hasSafePassword({ payload }, { call, put }) {
            const resp = yield call(passportService.hasSafePassword);
            if (resp.code === 0 && resp.data=== true) {
                yield put({ type: "setSafePassword", payload: true });
            }
            else
            {
                RenderHelper.warning("您未设置安全密码", "您需要设置安全密码后才可继续下列的操作",
                () => router.push("/passport/password/safe?redirect=/bank")
            );

            }
        },
        *updateSafePassword({ payload: { currentPwd, password, captcha } }, { call, put }) {
            const resp = yield call(passportService.updateSafePassword, currentPwd, password, captcha);
            if (resp.code === 0) {
                yield put({ type: "setSafePassword", payload: true });
            }
            return resp;
        },
        *resetSafePassword({ payload: { password, captcha } }, { call, put }) {
            const resp = yield call(passportService.resetSafePassword, password, captcha);
            return resp;
        },

        /* ------------------------------ Login Password ------------------------------- */

        *updateLoginPassword({ payload: { currentPwd, password, captcha } }, { call, put }) {
            const resp = yield call(passportService.updateLoginPassword, currentPwd, password, captcha);
            return resp;
        },

        *updateQQNumber({ payload: { currentQQ, newQQ} }, { call, put }) {
            const resp = yield call(passportService.updateQQNumber, currentQQ, newQQ);
            return resp;
        },

        

        *resetLoginPassword({ payload: {  password, ticket } }, { call, put }){
            const resp = yield call(passportService.resetLoginPassword, ticket,password);
            return resp;

        },

        /* ------------------------------ Username ------------------------------- */

        *checkUsername({ payload }, { call, put }) {
            const resp = yield call(passportService.checkUsername, payload);
            return resp;
        },
        *sendTicket({ payload }, { call, put }) {
            const resp = yield call(passportService.sendTicket, payload);
            return resp;
        },
        *sendPhoneTicket({ payload }, { call, put }) {
            const resp = yield call(passportService.sendPhoneTicket, payload);
            return resp;
        },
        *sendNewPhoneTicket({ payload:{newPhoneNumber, type} }, { call, put }) {
            const resp = yield call(passportService.sendNewPhoneTicket,newPhoneNumber);
            return resp;
        },

       
        *forgetPassword({ payload:{email, captcha} },{ call, put }){
            const resp = yield call(passportService.forgetPassword, email, captcha);
            return resp;

        },

        /* ------------------------------ Registration------------------------------- */

        *createUser({ payload: { email, password, qq, mobile, captcha, referral } }, { call, put }) {
            const resp = yield call(passportService.register, email, password, qq, mobile, captcha, referral);
            return resp;
        },

        *bindEmail({payload}, {call, put}){
            const resp = yield call(passportService.bindEmail, payload);
            return resp;
        },

        *bindPhone({payload}, {call, put}){
            const resp = yield call(passportService.bindPhone, payload);
            return resp;
        },
        *bindNewPhone({payload}, {call, put}){
            const resp = yield call(passportService.bindNewPhone, payload);
            return resp;
        },

        *verifyEmail({payload}, {call, put}){
            const resp = yield call(passportService.verifyEmail, payload);
            return resp;
        },

        *verifyRealName({payload:{realName, idNumber}}, {call, put}){
            const resp = yield call(passportService.verifyRealName, realName, idNumber);
            return resp;
        },

        //verifyRealName

    },
    // subscriptions: {
    //     setup({ dispatch, history }) {
    //         const checkSafePasswordPaths = ["/bank"];
    //         return history.listen(({ pathname, query }) => {
    //             if (checkSafePasswordPaths.includes(pathname)) {
    //                 dispatch({ type: 'hasSafePassword' });
    //             }
    //             else if (pathname === "/passport/password/safe") {
    //                 dispatch({ type: 'hasSafePassword' });
    //             }
    //         });
    //     },
    // },
};
