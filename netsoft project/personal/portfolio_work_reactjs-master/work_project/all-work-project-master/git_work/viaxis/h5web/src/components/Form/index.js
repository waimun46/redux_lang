import React, { PureComponent } from 'react';
import { Form } from "antd";
import { List, InputItem, Toast } from 'antd-mobile';

class MobileForm extends PureComponent {

    state = {
        errors: {}
    }

    render() {
        const { children, ...props } = this.props;
        return (
            <Form {...props}>
                {this.reduceChildren(children)}
            </Form>
        );
    }

    validateFields(cb) {
        const $this = this;
        const { validateFields } = this.props.form;
        validateFields((errors, values) => {
            $this.setState({ errors: errors });
            cb && cb(errors, values);
        });
    }

    reduceChildren(children) {
        if (children === null || children === undefined) children;

        children = React.Children.toArray(children);
        let elements = [];
        children.forEach(ele => {
            if (InputItem === ele.type)
                elements.push(React.cloneElement(ele, this.mixInputProps(ele)));
            else if (List === ele.type)
                elements.push(React.cloneElement(ele, { children: this.reduceChildren(ele.props.children) }));
            else
                elements.push(ele);
        });
        return elements;
    }

    mixInputProps(ele) {
        const { getFieldInstance, getFieldError, validateFields } = this.props.form;
        const { id, onBlur, onFocus, onErrorClick } = ele.props;
        const errors = this.state.errors || {};
        const $this = this;
        return {
            error: errors.hasOwnProperty(id),
            onBlur: v => {
                validateFields([id], (err, values) => {
                    if (err) {
                        $this.setState({ errors: Object.assign(errors, err) });
                        if (err[id].errors.length > 0) {
                            Toast.info(err[id].errors[0].message, 1, null, false);
                            // const input = getFieldInstance(id);
                            // input.inputRef.focus();
                        }
                    }
                });
                onBlur && onBlur(v);
            },
            onFocus: v => {
                if (errors.hasOwnProperty(id)) {
                    delete errors[id];
                    $this.setState({ errors: errors });
                }
                onFocus && onFocus(v);
            },
            onErrorClick: () => {
                if (errors.hasOwnProperty(id)) {
                    Toast.info(errors[id].errors[0].message, 1, null, false);
                }
            }
        }
    }
}

MobileForm.create = Form.create;

export default MobileForm;