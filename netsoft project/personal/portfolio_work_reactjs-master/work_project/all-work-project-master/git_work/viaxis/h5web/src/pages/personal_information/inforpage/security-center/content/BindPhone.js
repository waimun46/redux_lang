/**
 * Routes:
 *   - ./src/routes/AuthorizationRoute.js
 */

import { connect } from 'dva';
import { message, Row, Col, Alert, Tabs, Button, Input,Icon,Card } from "antd";
import { List, InputItem, WhiteSpace, Toast } from 'antd-mobile';

import Page from "@/layouts/PageBasic";
import Form from '@/components/Form';
import CountDownButton from "@/components/CountDownButton";
import * as RenderHelper from "@/utils/render_helper";

import router from 'umi/router';


const Item = List.Item;
const Brief = Item.Brief;

class BindPhone extends Page {
  title = "验证手机";
  constructor(props) {
    super(props);
    this.state = {
      // 提交按钮
      loading: false,
      // 发送验证码按钮
      captchaSending: false,
      // 发送按钮倒计时
      target: 0,
    }
    // 初始验证码地址,否则每次表单输内容验证码都会变化
    this.captchaUrl = RenderHelper.getCaptchaImgUrl();
  }


  /* ---------------------------- Functions ------------------------------*/

  refreshCaptcha() {
    let captcha = this.props.form.getFieldInstance("captcha");
    captcha.clearInput();
    captcha.inputRef.focus();
    this.refs.captcha_img.src = RenderHelper.getCaptchaImgUrl();
  }

  /* ---------------------------- Events ------------------------------*/

  handleResetSubmit(e) {
    e.preventDefault();

    const { dispatch } = this.props;

    this.form.validateFields((errors, values) => {
      if (errors) return;

      dispatch({
        type: "user/bindPhone",
        payload: { verificationCode: values.captcha }
      }).then(resp => {
        this.setState({ loading: false });

        if (resp.code === 0) {
          let msg = "手机认证成功";
          Toast.success(msg,2);
      
          this.props.form.resetFields();
          router.goBack();
        } else {
          Toast.fail(resp.message,2);
          
        }

      });

      this.setState({ loading: true });
    })
  }

  sendCaptchaClick(e) {
    const { dispatch } = this.props;
    dispatch({ type: "user/sendPhoneTicket", payload: true }).then(resp => {
      this.setState({ captchaSending: false });
      let seconds = 0;
      if (resp.code === 0) {
        seconds = 60;
        Toast.success("验证码已发送到您的邮箱。", 2);
        
      } else {
        seconds = resp.data;
        Toast.fail(resp.message,2);
        
      }
      if (seconds > 0)
        this.setState({ target: new Date().getTime() + seconds * 1000 });

    });
    this.setState({ captchaSending: true });
  }

 

  /* ---------------------------- Renders ------------------------------*/

  render() {
    const { getFieldProps } = this.props.form;

    return (

      <div className="form_sty form-countdown">
        <div className="top-header"> </div>
        <div className="form_title ">
          <h5>选项列表</h5>
        </div>

        <Form form={this.props.form} ref={el => this.form = el} onSubmit={this.handleResetSubmit.bind(this)}>

         



          <InputItem
            {...getFieldProps('captcha', {
              rules: [{ required: true, message: '请输入您接收到验证码' }],
            })}
            maxLength="6"
            clear
            placeholder="短信验证码"
            extra={<CountDownButton loading={this.state.captchaSending}
                                    size="small" type="primary"
                                    target={this.state.target}
                                    onClick={this.sendCaptchaClick.bind(this)}>
              发送验证码
            </CountDownButton>}
          >
            验证码
          </InputItem>


          <div className="game-orders">
            <div className="ButtonBottom">
              <button className="btn_left">
                <Icon type="reload"/> 重置
              </button>

              <button className="btn_right" loading={this.state.loading} htmlType="submit" >
                确定
              </button>

            </div>
            <div className="clearfix"></div>
          </div>
        </Form>
      </div>
    );
  }
};

export default connect(state => ({
  hasSafePassword: state.user.hasSafePassword,
  loading: state.loading.models.user,
}))(Form.create()(BindPhone));
