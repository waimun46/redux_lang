/**
 * Routes:
 *   - ./src/routes/AuthorizationRoute.js
 */

import { connect } from 'dva';
import { Input, Select, message, Card, Alert, Button, Badge, Tooltip, Popover, Table, Icon } from "antd";
import {Toast} from 'antd-mobile';

import AddCard from './components/AddCard';

import BasicComponent from "@/components/BasicComponent";
import SearchForm from '@/components/SearchForm';
import * as Utils from "@/utils/common";
import * as Constants from "@/app_constants";
import * as Enums from "@/app_enum";
import * as RenderHelper from "@/utils/render_helper";
import banklogo3 from '@/assets/images/icon/banklogo3.png'
import bankcardbg from '@/assets/images/bankcardbg.png'




class BankCardList extends BasicComponent {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
    };
  }

  /* ---------------------------- System  ------------------------------*/

  componentWillMount() {

    const { cards } = this.props;

    this.props.dispatch({ type: "user/hasSafePassword"});

   
    if (!cards || cards.length == 0)
      this.queryBankCard();
  }

  /* ---------------------------- Functions  ------------------------------*/
  queryBankCard() {
    this.props.dispatch({ type: "bank/getBankCards" });
  }

  /* ---------------------------- Events  ------------------------------*/

  onAddClick() {
    const { banks } = this.props;

    const dialog = (data) => RenderHelper.dialog("添加银行卡", <AddCard banks={data} onSubmit={this.onAddSubmit.bind(this)} />);

    if (!banks || banks.length === 0) {
      const hide = message.loading('Loading...', 0);
      this.props.dispatch({ type: 'bank/getBanks' }).then(rsp => {
        hide();
        this._dialog = dialog(rsp.data);
      });
      return;
    }

    this._dialog = dialog(banks);


  }

  onAddSubmit(form) {

    

    return this.props.dispatch({ type: "bank/addBankCard", 
                          payload:
                          {
                            bank:form.BankId, 
                            account_no:form.AccountNo,
                            account_name:form.AccountName || this.realname,
                            branch: form.BranchName, 
                            password: form.Password
                          }}).then(resp =>{
                            if(resp.code==0){
                              if (this._dialog) {
                                this._dialog.destroy();
                                this._dialog = null;
                              }

                            }
                           
                            return resp;

      });

  }

  /* ---------------------------- Renders  ------------------------------*/
  render() {
    return (
      <div className="form_sty bank-bg">
        <div className="top-header"> </div>
        <div className="fn-clear">
          {this.renderCards()}
        </div>
      </div>
    );
  }

  renderCards() {
    const cards = this.props.cards || [];
    return ( 
      <div className="bank-card-list bank-card-style ">

        <Alert  type="info" showIcon message="银行卡持卡人一经绑定以后无法修改，最多可绑定5张银行卡！" />
        {cards.map(it =>
          <Card key={it.id} title={<span>
                      <i className={`iconfont bankcard-icon icon-${it.BankCode}`}>
                           <img src={banklogo3} alt="bankcard"/>

                      </i>{it.BankName}</span>} extra={`** ${it.AccountNo.slice(-4)}`}>



            <div className="bank-card-bg">

              <div className="color-white " style={{marginTop:10}}>
                <b>持卡人</b>
                {it.AccountName}
              </div>
              <div className="color-white" style={{marginTop:10}}>
                <b>支行信息</b>
                {it.BranchName}

              </div>
              <img src={bankcardbg}/>
            </div>




          </Card>


        )}



        <div className="bank-add bank-add-style">
          <ul>
            <li>

              <Button className="plus-style" onClick={this.onAddClick.bind(this)}>
                <Icon type="plus" />
                <h6>添加银行卡</h6>
              </Button>
            </li>

            {/* <li ><Alert  type="info" showIcon message="银行卡持卡人一经绑定以后无法修改，最多可绑定5张银行卡！" /></li> */}

          </ul>
          <br />

        </div>

      </div>

    );
  }
}

function mapStateToProps(state) {
  return {
    banks: state.bank.banks,
    cards: state.bank.cards,
    hasSafePassword: state.user.hasSafePassword,
    loading: state.loading.models.bank
  };
}
export default connect(mapStateToProps)(BankCardList);
