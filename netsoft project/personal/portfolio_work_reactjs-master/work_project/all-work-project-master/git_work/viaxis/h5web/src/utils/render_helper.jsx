import React from 'react';
import ReactDOM from 'react-dom';
import moment from "moment";
import Sound from "react-sound";
import { Badge, Button, DatePicker, Modal, Icon } from 'antd';

import Dialog from "@/components/Dialog";
import * as Utils from "./common";

export const render_index = (text, record, index) => index + 1;

export const render_time = (time) => {
    // if(!time||time==0)
    // 	return '____-__-__ __:__:__';
    // return Utils.dateFormat(new Date(time*1000));
    if (Utils.isEmptyOrNull(time))
        return '____-__-__ __:__:__';
    return time;
}

/**
 * 显示状态
 * @param  {[type]} statusEnum 状态枚举
 * @param  {[type]} status     状态
 * @return {[type]}            [description]
 */


export const render_status = (statusEnum, status) => {
    return (
        <Badge status={statusEnum[status].status} text={statusEnum[status].toString()} />
    );
}

export const money_format = (val) => {
    return Utils.milliFormat(val);
}

export const money_format_color = (val) => {
    const props = {};
    if (val > 0) props.className = "fn-color-green";
    else if (val < 0) props.className = "fn-color-red";

    return <span {...props}>{Utils.milliFormat(val)}</span>
}

export const render_percent = (digits = 2) => {
    return (val) => (val * 100).toFixed(digits) + "%";
}

export const render_enum = (enumType) => {
    return (val) => enumType[val] || "未知";
}

export const rangeDatePick = () => {
    const beforeYesterday = moment().add(-2, "days");
    const yesterday = moment().add(-1, 'days');
    const today = moment();
    const thisMonth1th = moment().startOf('month');
    const lastMonth1th = moment().startOf('month').add(-1, 'months');
    const ranges = {
        '上月': [lastMonth1th, lastMonth1th.clone().endOf('month')],
        '上周': [moment().startOf('week').add(-1, "weeks"), moment().endOf('week').add(-1, "weeks")],
        '前天': [beforeYesterday, beforeYesterday],
        '昨天': [yesterday, yesterday],
        '今天': [today],
        '本周': [moment().startOf('week'), moment().endOf('week')],
        '本月': [thisMonth1th, moment().endOf('month')],
    };
    return <DatePicker.RangePicker
        ranges={ranges} />
}

export const getCaptchaImgUrl = () => {
    let apiUrl='';
    return Utils.addActionToUrl(apiUrl + "/captcha", "_", new Date().getTime());
}

/* ---------------------------- Dialogs --------------------------- */

export function info(title, content, fnOk, options) {
    return alert({
        title,
        content,
        onOk: fnOk,
        className: "info",
        seconds: options && options.seconds || 3
    });
}

export function warning(title, content, fnOk, options) {
    return alert({
        title,
        content,
        onOk: fnOk,
        className: "warning",
        iconType: "exclamation-circle",
        seconds: options && options.seconds || 3
    });
}

export function success(title, content, fnOk, options) {
    return alert({
        title,
        content,
        onOk: fnOk,
        className: "success",
        iconType: "check-circle",
        seconds: options && options.seconds || 3
    });
}

export function error(title, content, fnOk, options) {
    return alert({
        title,
        content,
        onOk: fnOk,
        className: "error",
        iconType: "cross-circle",
        seconds: options && options.seconds || 3
    });
}

export function confirm(title, content, fnOk, fnCancel, options) {
    return alert({
        title,
        content,
        onOk: fnOk,
        onCancel: fnCancel,
        okCancel: true,
        className: "warning",
        iconType: "question-circle-o"
    });
}

/**
 * [alert description]
 * @param  {[type]} options [description]
 * @return {[type]}         [description]
 */
export function alert(options) {

    const defaultOptions = { okCancel: false, className: "info" };
    const props = Object.assign({}, defaultOptions, options);

    let div = document.createElement('div');
    document.body.appendChild(div);

    function close() {
        const unmountResult = ReactDOM.unmountComponentAtNode(div);
        if (unmountResult && div.parentNode) {
            div.parentNode.removeChild(div);
        }
    }

    ReactDOM.render(
        <Dialog {...props} onClose={close} />
        , div);

    return {
        destroy: close,
    };
}

export function dialog(title, body, options, parentNode) {

    let div = document.createElement('div');
    if (!parentNode)
        // parentNode = document.getElementById("root");
        parentNode = document.body;
    parentNode.appendChild(div);

    function close() {
        const unmountResult = ReactDOM.unmountComponentAtNode(div);
        if (unmountResult && div.parentNode) {
            div.parentNode.removeChild(div);
        }
    }

    var cloneBody = React.cloneElement(body, { destroy: close });

    ReactDOM.render(
        <Modal title={title} {...options} footer='' visible onCancel={close}>
            {cloneBody}
        </Modal>
        , div);

    return {
        destroy: close,
    };

}

export function playSound(url) {
    let div = document.createElement('div');
    document.body.appendChild(div);

    function close() {
        const unmountResult = ReactDOM.unmountComponentAtNode(div);
        if (unmountResult && div.parentNode) {
            div.parentNode.removeChild(div);
        }
    }

    ReactDOM.render(
        <Sound
            url={url}
            playStatus={Sound.status.PLAYING}
            debugMode={false}
            useConsole={false}
            onFinishedPlaying={close} />,
        div);
}
