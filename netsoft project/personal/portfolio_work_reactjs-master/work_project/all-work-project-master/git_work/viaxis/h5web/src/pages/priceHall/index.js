import { connect } from 'dva';
import { List, Icon } from 'antd-mobile';


import Link from 'umi/link';
import Form from '@/components/Form';
import Page from "@/layouts/PageBasic";

import * as betHelper from '../../utils/bet_helper';

import xylc from '../../assets/images/game-icon/xylc.png';
import fc3d from '../../assets/images/game-icon/fc3d.png';
import keno from '../../assets/images/game-icon/keno.png';
import kl8 from '../../assets/images/game-icon/kl8.png';
import pk10 from '../../assets/images/game-icon/pk10.png';
import pl3 from '../../assets/images/game-icon/pl3.png';
import ssc from '../../assets/images/game-icon/ssc.png';
import { from } from 'rxjs';



const Item = List.Item;
const Brief = Item.Brief;
const title = "开奖大厅"

class PriceHall extends Page {
   

    constructor(props){
        super(props);
        this.props.dispatch({ type: "global/setTitle", payload: title || this.title });
    }

    
   

    componentWillMount(){
        this.props.dispatch({type:"gamedata/queryLatestGameResult"});
    }

    getThumbImagesByGroupId(id)
    {
        switch(id){
            case 1: return (<img className="logosizee" src={fc3d} alt="fc3d" />);
            break;
            case 2: return (<img className="logosizee" src={ssc} alt="ssc" />);
                break;
            case 3: return (<img className="logosizee" src={kl8} alt="kl8" />);
                break;
            case 4: return (<img className="logosizee" src={xylc} alt="kl10" />);
                break;
            case 5: return (<img className="logosizee" src={keno} alt="KENO" />);
                break;
            case 6: return (<img className="logosizee" src={pl3} alt="pl3" />);
                break;
            case 7: return "分分彩";
                break;
            case 8: return "真人彩";
                break;
            case 9 : return (<img className="logosizee" src={pk10} alt="pk10" />);
            break;
        }
    }

    splitNum(num) {
        let num1 = num.split(",");
        let num2 = [];
        for(let i = 0; i < num1.length; i++) {
          num2.push(num1);
          return num2;
        }
      }

    render(){

        let gameResults = this.props.latestGameResults;

        gameResults = gameResults.filter(item=>{
            return item.GroupId !=7 && item.GroupId !=8
        });

        return(
            <div className="danielph2">
                <div className="fund-history price_hall_style">
                    <div className="top-header"> </div>
                    
               


            <List className="my-list ">
            {
                gameResults.map(item=>{
                    let linkUrl = '/priceHall/detail/'+ item.GroupId;
                   
                    return(
                        <Link to={linkUrl}>
                            <Item  extra={
                                <div className="bet-money add">
                                    <Icon type="right" />
                                </div>
                            }
                            align="top"
                            thumb={this.getThumbImagesByGroupId(item.GroupId)}
                            multipleLine
                            className="pricehall_list"
                            >
                            
                            <span className="title-warp">
                                    {betHelper.getGameNameByGroupId(item.GroupId)}
                                    <span> 第<span style={{color: 'red'}}>{item.Qh}</span>期</span>
                            </span>
                            {this.splitNum(item.Result).map((element) => 
                                    <Brief>
                                        <ul>
                                            {element.map((e) => <li>{e}</li>)}
                                            <div className="clearfix"></div>
                                        </ul>
                                    </Brief>
                                )}

                            <Brief style={{fontSize: '12px', color: '#cccccc'}}>{item.OpenTime}</Brief>
                            
                            </Item>
                      </Link>

                    )
                
               
            })
        }
            
            </List>
            
                </div>
            </div>
        )
        
    }
}

export default connect(state => ({

    latestGameResults : state.gamedata.latestResult,
    
   
  }))(Form.create()(PriceHall));