import { routerRedux } from 'dva/router';
import * as pstService from '@/services/passport';
import * as billService from '@/services/bill';

/**
 * 专用于处理websocket信息
 */

export default {
    namespace: 'ws',
    state: {
    },
    reducers: {
        saveData(state, { payload }) {
            return { ...state, ...payload };
        },
    },
    effects: {
        /**
         * 开奖
         */
        *GameDataOpen({ payload: { code, data } }, { call, put }) {
            if (code === 0) {
                yield put({
                    type: 'gamedata/openGameData',
                    payload: data
                });
            }
        },
    }
};