/**
 * Routes:
 *   - ./src/routes/AuthorizationRoute.js
 */

import { connect } from 'dva';
import Link from 'umi/link';
import { message, Row, Col, Alert,  Button, Input,Icon,Card  } from "antd";
import {  Tabs, List, InputItem, WhiteSpace } from 'antd-mobile';

import Page from "@/layouts/PageBasic";
import Form from '@/components/Form';
import * as RenderHelper from "@/utils/render_helper";

const tabs = [
  { title: '代理类型' },
  { title: '玩家类型' },

];

const Item = List.Item;
const Brief = Item.Brief;



class LoginPassword extends Page {
  title = "下级开户"

  render() {


    return (
      <div className="form_sty form-list-style ">
        <div className="top-header"> </div>
        <div className="form_title">
          <h5>填写修改信息<span style={{color:'blue'}}> <Link to="">点击查看返点赔率表</Link></span></h5>
        </div>

        <Form form={this.props.form} ref={el => this.form = el} >

          <Tabs  tabs={tabs} initialPage={0} animated={false} useOnPan={false} activeTab={true} >
            <div className="form_tab ">

              <InputItem


                type=""
                cleartext
                placeholder="自身返点8.0，可设置返点0.0-8.0">
                时时彩
              </InputItem>



              <InputItem

                type=""
                cleartext
                placeholder="自身返点8.0，可设置返点0.0-8.0">
                快3
              </InputItem>

            </div>


            <div className="form_tab" >

              <InputItem

                type=""
                cleartext
                placeholder="自身返点8.0，可设置返点0.0-8.0">
                福彩3D
              </InputItem>

            </div>

          </Tabs>









          <div className="game-orders">
            <div className="ButtonBottom">
              <button className="btn_left">
                <Icon type="reload"/> 重置
              </button>

              <button className="btn_right">
                确定
              </button>

            </div>
            <div className="clearfix"></div>
          </div>


        </Form>

      </div>


    );
  }
};

export default connect(state => ({

}))(Form.create()(LoginPassword));
