import { GET, POST } from '@/utils/request';

/**
 * 通过目录标签与文章地址获取文章
 * @param  {[type]} categoryLabel  [description]
 * @param  {[type]} articleUrlName [description]
 * @return {[type]}                [description]
 */
export function getArticle(categoryLabel, articleUrlName) {
    return GET(`/api/article/${categoryLabel}/${articleUrlName}`);
}

export function getArticleByCategory(categoryLabel) {
    // CategoryArticle
    return GET(`/api/article/${categoryLabel}`);
}
