import React, { Component } from 'react';

import * as betHelper from '../../utils/bet_helper';


class TeamRecord extends Component
{
    constructor(props){
        super(props);
    }

    render(){
        const {teamData} = this.props;

        return(
            <div class="popup_new">
                            <div className="popup-inner">
                                <h2>查看下级</h2>

                                <div class="content">
                                    <table>
                                        <thead>
                                            <tr className="tr-style">
                                                <th>用户:</th>
                                                <th>用户类型:</th>
                                                <th>余额:</th>
                                                <th>团队人数:</th>
                                                <th>团队余额:</th>
                                                <th>返点:</th>
                                               
                                                <th>状态:</th>
                                                <th>日工资:</th>
                                                <th>手机号码:</th>
                                                <th>QQ:</th>
                                                <th>备注:</th>
                                                <th>注册时间:</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr className="tr2-style">
                                                <td
                                                    style={{
                                                        color: 'blue'
                                                    }}>{teamData.Agents}</td>
                                                <td>{(() => {
                                                    switch(teamData.UserKind) {
                                                        case 1:
                                                            return '大股东';
                                                        case 2:
                                                            return '股东';
                                                        case 3:
                                                            return '代理';
                                                        case 4:
                                                            return '会员';
                                                        default:
                                                            return teamData.UserKind;
                                                    }
                                                })()}</td>
                                                <td>￥{teamData.Balance}</td>
                                                <td>{teamData.ChildCount}</td>                                          
                                                <td>{teamData.ChildBalance}</td>
                                                <td>{teamData.Backpct}%</td>
                                                <td>{(() => {
                                                    switch(teamData.Status) {
                                                        case 0:
                                                            return '正常';
                                                        case 1:
                                                            return '禁用';
                                                        default:
                                                            return teamData.Status;
                                                    }
                                                })()}</td>
                                                <td>{teamData.DailyWages}%</td> 
                                                <td>{teamData.Mobile}</td>
                                                <td>{teamData.QQ}</td>
                                                <td>{teamData.Remark}</td>
                                                <td>{teamData.CreateTime}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            {/* <a class="close" href="#">
                                <div className="btn-popup close">
                                    <h4>关闭</h4>
                                </div>
                            </a> */}
                        </div>


        )
    }

}

export default TeamRecord;
