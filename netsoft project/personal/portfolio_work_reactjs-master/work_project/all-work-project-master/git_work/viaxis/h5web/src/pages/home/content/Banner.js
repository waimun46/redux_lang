import React, {Component} from 'react';
import {Icon} from "antd";
import { Carousel , WingBlank, NoticeBar, WhiteSpace } from 'antd-mobile';


import banner01 from '../../../assets/images/banner/banner03.jpg';
import banner02 from '../../../assets/images/banner/banner01.jpg';
import banner03 from '../../../assets/images/banner/banner02.jpg';

class Banner extends Component {


  render() {
    return (
      <div className="banner">
        <WingBlank>
          <Carousel
            autoplay
            infinite
            beforeChange={(from, to) => console.log(`slide from ${from} to ${to}`)}
            afterChange={index => console.log('slide to', index)}
          >
            <img
              src={banner01}
              alt="banner01"
              style={{ width: '100%', verticalAlign: 'top' }}
              onLoad={() => {
              // fire window resize event to change height
                window.dispatchEvent(new Event('resize'));
                this.setState({ imgHeight: 'auto' });
              }}
            />
            <img
              src={banner02}
              alt="banner02"
              style={{ width: '100%', verticalAlign: 'top' }}
              onLoad={() => {
              // fire window resize event to change height
                window.dispatchEvent(new Event('resize'));
                this.setState({ imgHeight: 'auto' });
              }}
            />
            <img
              src={banner03}
              alt="banner03"
              style={{ width: '100%', verticalAlign: 'top' }}
              onLoad={() => {
              // fire window resize event to change height
                window.dispatchEvent(new Event('resize'));
                this.setState({ imgHeight: 'auto' });
              }}
            />
          </Carousel>
        </WingBlank>

        <NoticeBar
          marqueeProps={{ loop: true, style: { padding: '0 7.5px' } }}
        >
          Notice: The arrival time of incomes and transfers of Yu &#39;E Ba
           will be delayed during National Day.
        </NoticeBar>
      </div>
    );
  }
}

export default Banner;
