

import { connect } from 'dva';
import router from 'umi/router';
//import { Flex, List, InputItem, Icon, Toast } from 'antd-mobile';
import { ActivityIndicator, WingBlank, WhiteSpace, Button, Toast } from 'antd-mobile';
import * as RenderHelper from "@/utils/render_helper";

import Page from "@/layouts/PageBasic";
import Form from '@/components/Form';

class EmailVerification extends Page{

  title="邮箱验证";

    constructor(props) {
        super(props);
        this.state={
          code:0, 
          vefirying: true,
          verified: false
        }
       

    }

    componentWillMount(){

      let code = this.props.match.params.index;
      this.setState({code:code});
    }

    componentDidMount(){
      this.bindEmail();
    }

    bindEmail(){
      const { dispatch } = this.props;

      dispatch({type: "user/verifyEmail", payload: { verificationCode: this.state.code }}).then(resp=>{

        if (resp.code === 0){
          this.setState({verified:true});
          Toast.success("邮箱验证成功！",5);
          router.push("/");       
        }

        this.setState({verifying:false});
      });


    }


    render(){
        return(
          <div className="form_sty form-countdown">
           <div className="top-header"> </div>
           {
           
              this.state.verifying == true ?
          
             
                  <div className="email-verification-container">
                    <div className="email-verification-row">
                      <ActivityIndicator size="large" />
                    </div>
                    <div className="email-verification-row">
                      <span style={{ marginTop: 8, margenRight:8 }}>验证中，请稍等...</span>                
                    </div>                    
                   
             </div> :  this.state.verified == true ?
  
             <div>邮箱验证成功！</div> : <div>邮箱验证失败！</div>





             


           }

          </div>

          
          
          



        )

    }
}

export default connect(state => ({
   
  }))(Form.create()(EmailVerification));