import PropTypes from 'prop-types';
import { connect } from 'dva';
import { message, Table, Breadcrumb, Button, Select, Input, Icon } from 'antd';
import Link from 'umi/link';

import BasicComponent from "@/components/BasicComponent";
import Form from '@/components/Form';
import * as RenderHelper from "@/utils/render_helper";

class ProfitUserGameType extends BasicComponent {
    constructor(props) {
        super(props);
        this.pageIndex = 1;
    }

    /* ------------------ Sys -------------------- */
    componentWillMount() {
        this.queryData(this.props);
    }

    componentWillReceiveProps(props) {
        if (props != this.props && props.querys != this.props.querys)
            this.queryData(props);
    }

    /* ------------------ Functions -------------------- */

    queryData(props) {
        let { userId, gameType, querys } = props;
        querys = Object.assign({}, querys, { game_type: gameType, pageIndex: this.pageIndex });
        this.props.dispatch({
            type: "profit/queryProfitGameTypeByUser",
            payload: { userId, querys }
        });
    }

    /* ------------------ Events -------------------- */



    /* ------------------ Renders -------------------- */

    render() {
        const self = this;
        const { profitUserGameType, loading } = this.props;
        let data = profitUserGameType.Data || [];

        const pagination = {
            total: profitUserGameType.TotalCount || 0,
            onChange(current) {
                self.queryData(self.props);
            },
        };

        return (
            <div>
                <Table loading={loading}
                    columns={this.columns} dataSource={data} pagination={pagination}
                    title={() => <Button icon="rollback" type="primary" onClick={() => this.props.onGoBackClick()}>返回</Button>} />
            </div>
        );
    }

    get columns() {
        const self = this;
        const columns = [
            { title: '#', render: RenderHelper.render_index },
            {
                title: "日期", render: it => {
                    if (it.ToDate != it.Date)
                        return `${it.Date} - ${it.ToDate}`;
                    return it.Date;
                }
            },
            { title: "Email", dataIndex: "Email" },
            { title: "彩种", dataIndex: "GroupName" },
            { title: "玩法", dataIndex: "GameName" },
            {
                title: "订单数", dataIndex: "OrderCount", className: "column-money", render: (it, record) => {
                    return <a target="_blank" href={`/deals?email=${record.Email}&game_type=${record.GameType}&from_time=${record.Date}&to_time=${record.ToDate}235959`}>{it}</a>
                }
            },
            { title: "总投注", dataIndex: "TotalBet", className: "column-money", render: RenderHelper.money_format },
            { title: "有效投注", dataIndex: "ValidBet", className: "column-money", render: RenderHelper.money_format },
            { title: "盈亏", dataIndex: "Profit", className: "column-money", render: RenderHelper.money_format_color },
        ];

        return columns;
    }
}
ProfitUserGameType.propTypes = {
    onGoBackClick: PropTypes.func.isRequired
}
export default connect(state => ({
    userinfo: state.global.userinfo,
    profitUserGameType: state.profit.profitUserGameType,
    loading: state.loading.models.profit,
}))(ProfitUserGameType);