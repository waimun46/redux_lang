import * as billService from '@/services/bill';

export default {
    namespace: 'billflow',
    state: {},
    reducers: {
        saveBillFlow(state, { payload }) {
            return { ...state, ...payload.data };
        },
    },
    effects: {
        *queryBillflow({ payload, }, { call, put }) {
            const data = yield call(billService.queryBillflow, payload);
            yield put({
                type: 'saveBillFlow', payload: data
            });
        },
    },
};