/**
 * Routes:
 *   - ./src/routes/AuthorizationRoute.js
 */

import { connect } from 'dva';
import { message, Row, Col, Alert, Tabs, Button, Input,Icon } from "antd";
import { List, InputItem, WhiteSpace } from 'antd-mobile';
import Link from 'umi/link';
import Page from "@/layouts/PageBasic";
import Form from '@/components/Form';


const Item = List.Item;
const Brief = Item.Brief;


class FundManagement extends Page {
  title = "资金管理"


  render() {

    return (
      <div className="form_sty">
        <div className="top-header"> </div>
        <div className="form_title">
          <h5>选项列表</h5>
        </div>


        <List >
          <Link className="border-line" to="/personal_information/inforpage/fund-management/content/topup/SelectRechargeMethod">
            <Item arrow="horizontal" onClick={() => {}}>充值</Item>
          </Link>
          <Link className="border-line" to="/withdraw">
            <Item arrow="horizontal" onClick={() => {}}>提现</Item>
          </Link>
          <Link className="border-line" to="/bank">
            <Item arrow="horizontal" onClick={() => {}}>银行卡管理</Item>
          </Link>
          <Link className="border-line" to="/personal_information/inforpage/fund-management/content/DepositHistory">
           <Item arrow="horizontal" onClick={() => {}}>充值纪录</Item>
          </Link>
          <Link className="border-line" to="/personal_information/inforpage/fund-management/content/WithdrawalHistory">
           <Item arrow="horizontal" onClick={() => {}}>提现纪录</Item>
          </Link>
          <Link className="border-line" to="/personal_information/inforpage/huishui">
           <Item arrow="horizontal" onClick={() => {}}>流水纪录</Item>
          </Link>

        </List>




      </div>


    );
  }
};

export default connect(state => ({

}))(Form.create()(FundManagement));
