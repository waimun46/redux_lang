# README

## Mobile专用搜索组件

用法:

```jsx
    <Search textMore="更多" onSubmit={it => console.log(it)}>
        <Search.Header>
          <DPicker getFieldProps={{ 'dpki11': { initialValue: new Date() } }} title="创建日期" />
          <DateRange getFieldProps={{ 'dpki12': rangeConfig }} />
        </Search.Header>
        <Search.Body>

          <InputItem
            getFieldProps={
              {
                password: {
                  rules: [{
                    min: 6, max: 20, message: '安全密码长度要求为6到20个字符之间。'
                  }],
                }
              }
            }
            type="password"
            clear
            placeholder="新的安全密码">
            新的密码
          </InputItem>

          <Form.Item
            {...formItemLayout}
            label="DatePicker"
          >
            <DPicker getFieldDecorator={{ 'dpki': { initialValue: new Date() } }} />
          </Form.Item>

          <Form.Item label="E-mail" {...formItemLayout}>
            <Input getFieldDecorator={{
              email: {
                rules: [{
                  type: 'email', message: 'The input is not valid E-mail!',
                }],
              }
            }} />
          </Form.Item>
          <Form.Item
            {...formItemLayout}
            label="Select"
            hasFeedback
          >
            <Select placeholder="Please select a country" getFieldDecorator={{
              select: {
                rules: [
                  { required: true, message: 'Please select your country!' },
                ],
              }
            }}>
              <Option value="china">China</Option>
              <Option value="usa">U.S.A</Option>
            </Select>
          </Form.Item>
          <Form.Item
            {...formItemLayout}
            label="DatePicker"
          >
            <DatePicker getFieldDecorator={{ 'date-picker': config }} />
          </Form.Item>
        </Search.Body>
    </Search>
```

## Search属性

- onSubmit : function(values) 提交按钮触发事件
- onReset  : function 重置按钮触发事件

## Search.Header

头部内容,支持 Antd Mobile InputItem, Picker, DPicker, DateRange 组件,最终内容会被包在 Flex.Item中.

**属性**:

- BtnMoreName : string 最右侧展开按钮名称, 不设置默认为 "筛选", 点击显示 **Search.Body** 内容

## Search.Body

搜索主体内容,默认隐藏,点击Header右侧筛选按钮显示.支持一切内容,如果元素是 Antd Mobile InputItem, Picker, DPicker, DateRange
等, 支持(**getFieldProps**)[https://www.npmjs.com/package/rc-form#wrappedcomponent-reactcomponent--reactcomponent]属性,
如果是 Antd Input, Select, DatePicker, TimePicker, DPicker等类型,支持(**getFieldDecorator**)[https://ant.design/components/form-cn/#this.props.form.getFieldDecorator(id,-options)]属性.