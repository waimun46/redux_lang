import classnames from 'classnames';
import { Icon } from 'antd-mobile';
import styles from './index.less';

const prefixCls = 'am-loaing';

export default function () {
    return (
        <div className={classnames(styles[prefixCls], styles[`${prefixCls}-mask`])}>
            <span>
                <div className={classnames(styles[`${prefixCls}-notice`])}>
                    <div className={classnames(styles[`${prefixCls}-notice-content`])}>
                        <div className={classnames(styles[`${prefixCls}-text`], styles[`${prefixCls}-text-icon`])}>
                            <Icon type='loading' />
                            <div className={classnames(`${prefixCls}-text-info`)}>Loading...</div>
                        </div>
                    </div>
                </div>
            </span>
        </div >
    );
}