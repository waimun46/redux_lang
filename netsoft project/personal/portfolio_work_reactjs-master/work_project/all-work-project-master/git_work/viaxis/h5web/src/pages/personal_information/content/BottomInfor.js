import React, {Component} from 'react';
import { Button } from 'antd';
import Link from 'umi/link';
import user from '../../../assets/images/icon/user2.png'
import security from '../../../assets/images/icon/security.png'
import money2 from '../../../assets/images/icon/money2.png'
import management from '../../../assets/images/icon/money-management.png'
import agency from '../../../assets/images/icon/agency.png'
import safepassword from '../../../assets/images/icon/safepassword.png'
import usercard from '../../../assets/images/icon/usercard.png'
import BasicComponent from '../../../components/BasicComponent';
import router from 'umi/router';

class BottomInfor extends BasicComponent {

  constructor(props){
    super(props);
  }

  onlogoutClick(){
    this.logout();
    
  }
  render() {
    return (
      <div className="bottom-content">
        <Link to="/personal_information/inforpage/personal-infor">
          <div className="bottom-warp">
            <div className="bottom-infor-btn">
              <img src={user} alt="user2"/>
              <p>个人信息</p>
            </div>
          </div>
        </Link>

        <Link to="/personal_information/inforpage/security-center">
          <div className="bottom-warp">
            <div className="bottom-infor-btn">
              <img src={security} alt="security"/>
              <p>安全中心</p>
            </div>
          </div>
        </Link>

        <Link to="/personal_information/inforpage/huishui">
        <div className="bottom-warp">
          <div className="bottom-infor-btn">
            <img src={money2} alt="money2"/>
            <p>流水记录</p>
          </div>
        </div>
        </Link>

        <Link to="/personal_information/inforpage/fund-management">
          <div className="bottom-warp">
            <div className="bottom-infor-btn">
              <img src={management} alt="management"/>
              <p>资金管理</p>
            </div>
          </div>
        </Link>

        <Link to="/personal_information/inforpage/agency">
          <div className="bottom-warp">
            <div className="bottom-infor-btn">
              <img src={agency} alt="agency"/>
              <p>代理中心</p>
            </div>
          </div>
        </Link>

        <Link to="/personal_information/inforpage/realname">
          <div className="bottom-warp">
            <div className="bottom-infor-btn">
              <img src={usercard} alt="usercard"/>
              <p>实名认证</p>
            </div>
          </div>
        </Link>

        <div className="clearfix"></div>
        <div className="btn-botttom">
          <Button type="primary" block onClick={this.onlogoutClick.bind(this)}>退出登入</Button>
        </div>
      </div>
    );
  }
};

export default BottomInfor
