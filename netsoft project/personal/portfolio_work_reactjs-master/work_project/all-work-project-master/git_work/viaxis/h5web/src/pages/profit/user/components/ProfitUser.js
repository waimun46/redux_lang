import { connect } from 'dva';
import { message, Table, Breadcrumb, Button, Select, Input, Icon } from 'antd';
import Link from 'umi/link';

import Page from "@/layouts/PageBasic";
import Form from '@/components/Form';
import SearchForm from '@/components/SearchForm';
import * as Utils from "@/utils/common";
import * as RenderHelper from "@/utils/render_helper";
import * as Enums from "@/app_enum";
import * as Constants from "@/app_constants";

class ProfitUser extends Page {
    title = "用户盈亏"

    querys = {}

    constructor(props) {
        super(props);
        this.userId = this.getUserId(props);
    }

    /* ------------------ Sys -------------------- */
    componentWillMount() {
        this.setTitle();
        this.queryData();
    }

    componentWillReceiveProps(newProps) {
        if (newProps != this.props) {
            const userId = this.getUserId(newProps);
            if (userId != this.userId && !isNaN(userId)) {
                this.userId = userId;
                this.queryData();
            }
        }

    }
    /* ------------------ Functions -------------------- */

    queryData() {
        let parents = this.props.parents.data;
        if (this.userId || parents != null || parent.length > 0)
            this.props.dispatch({ type: "user/getParents", payload: this.userId });

        this.props.dispatch({
            type: "profit/queryProfitUser",
            payload: { userId: this.userId, querys: this.querys }
        });
    }

    getUserId(props) {
        let userId = props.match.params.userId;
        if (userId)
            userId = parseInt(userId);
        return userId;
    }

    isMemberuser() {
        const userinfo = this.props.userinfo;

        return userinfo && userinfo.UserKind == Enums.UserKind.Member;
    }

    /* ------------------ Events -------------------- */

    onSearchClick(values) {
        this.querys = values;
        this.queryData();
    }

    onResetClick() {
        this.querys = {};
    }

    /* ------------------ Renders -------------------- */

    render() {
        const self = this;
        const { profitUser, loading } = this.props;
        let data = profitUser.Data || [];

        const pagination = {
            total: profitUser.TotalCount || 0,
            onChange(current) {
                self.querys = Object.assign({}, self.querys, { pageIndex: current });
                self.queryData();
            },
        };

        return (
            <div>
                {this.renderSearch()}
                {this.renderBreadcrumb()}
                <div className="search-result-list">
                    <Table loading={loading} columns={this.columns} dataSource={data} rowKey="UserId" pagination={pagination} />
                </div>
            </div>
        );
    }

    renderBreadcrumb() {
        let parents = this.props.parents;
        if (parents == null || parents.length == 0)
            if (this.props.userinfo)
                parents = [this.props.userinfo];

        if (parents == null)
            parents = []
        return (
            <Breadcrumb separator=">">
                {parents.map((it, i) => <Breadcrumb.Item key={i}><Link to={`/profit/user/${it.UserId}`}>{it.Email}</Link></Breadcrumb.Item>)}
            </Breadcrumb>
        );
    }

    renderSearch() {
        const formItemLayout = {
            labelCol: { span: 8 },
            wrapperCol: { span: 16 }
        };

        return (
            <SearchForm onSearch={this.onSearchClick.bind(this)} onReset={this.onResetClick.bind(this)}>
                <email label="邮箱" {...formItemLayout}>
                    <Input />
                </email>
                <date label="时间" {...formItemLayout} format={(times) => Utils.dateRangeFormat(times)}>
                    {RenderHelper.rangeDatePick()}
                </date>
            </SearchForm>
        );
    }

    get columns() {
        const self = this;
        let isMember = this.isMemberuser();
        let columns = [
            { title: '#', render: RenderHelper.render_index },
            {
                title: '邮箱', dataIndex: 'Email', render: (email, profit) => {
                    if (profit.IsSum == 0)
                        return email;
                    else
                        return <Link to={`/profit/user/${profit.UserId}`}>{email}</Link>
                }
            },
            {
                title: "日期", render: it => {
                    if (it.ToDate != it.Date)
                        return `${it.Date} - ${it.ToDate}`;
                    return it.Date;
                }
            },
            { title: "订单数", dataIndex: "OrderCount", className: "column-money" },
            { title: "总投注", dataIndex: "TotalBet", className: "column-money", render: RenderHelper.money_format },
            {
                title: "有效投注", dataIndex: "ValidBet", className: "column-money", render: (it, profit) => {
                    return <a target="_blank" href={`/deals?email=${profit.Email}&load_all=1&from_time=${profit.Date}&to_time=${profit.ToDate}235959`}>{RenderHelper.money_format(it)}</a>
                }
            },
            { title: "奖金", dataIndex: "Bonus", className: isMember ? "column-hidden" : "column-money", render: RenderHelper.money_format },
            { title: "佣金", dataIndex: "Commission", className: isMember ? "column-hidden" : "column-money", render: RenderHelper.money_format },
            { title: "盈亏", dataIndex: "Profit", className: "column-money", render: RenderHelper.money_format_color },
            { title: "充值", dataIndex: "Deposit", className: "column-money", render: RenderHelper.money_format },
            { title: "提现", dataIndex: "Withdraw", className: "column-money", render: RenderHelper.money_format },
            { title: "充值数", dataIndex: "DepositCount", className: "column-money" },
            { title: "提现数", dataIndex: "WithdrawCount", className: "column-money" }
        ];
        return columns;
    }
};

export default connect(state => ({
    userinfo: state.global.userinfo,
    profitUser: state.profit.profitUser,
    parents: state.user.parents,
    loading: state.loading.models.profit,
}))(Form.create()(ProfitUser));