import React from 'react';
import styles from './index.css';
import Header from './Header';
import withRouter from 'umi/withRouter';
import AuthLayout from './AuthLayout';
import SimpleLayout from './SimpleLayout';


function Layout({ children, location }) {

  if (location.pathname === '/login' ) {
    return <SimpleLayout>{children}</SimpleLayout>
  }

  return <AuthLayout>{children}</AuthLayout>
}

export default withRouter(Layout);
