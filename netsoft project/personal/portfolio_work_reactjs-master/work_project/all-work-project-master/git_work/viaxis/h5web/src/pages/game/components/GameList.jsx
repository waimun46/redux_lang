import { connect } from 'dva';
import PropTypes from 'prop-types';
import React from 'react';
import { notification, Spin, Tooltip, Table, Tabs, Icon } from "antd";


import BasicComponent from "@/components/BasicComponent";
import * as Utils from "@/utils/common";
import * as Constants from "@/app_constants";
import * as BetHelper from "@/utils/bet_helper";

const TabPane = Tabs.TabPane;

class GameList extends BasicComponent {
    constructor(props) {
        super(props);
    }

    /* ---------------------------- 系统函数 ------------------------------*/
    componentWillMount() {
        this.queryDealInfo(this.props);
        this.queryGameData(this.props);
    }

    componentWillReceiveProps(newProps) {

        if (this.props.Group != newProps.Group) {
            this.queryDealInfo(newProps);
            this.queryGameData(newProps);
        }

        if (newProps.dealinfo.profit != this.props.dealinfo.profit && newProps.dealinfo.profit) {
            this.showProfit(newProps.dealinfo.profit);
        }
    }

    shouldComponentUpdate(nextProps, nextState) {
        return nextProps != this.props || nextState != this.state;
    }

    /* ---------------------------- 自定义函数 ------------------------------*/
    queryDealInfo(props) {
        var group_name = props.Group;
        this.props.dispatch(GameAction.queryDealInfo({ group_name, started: true, not_trace: true }));
    }

    queryGameData(props) {
        var group_name = props.Group;
        this.props.dispatch(GameAction.queryGameData(group_name, 1, { finished: 1 }));
    }

    showProfit(profit) {
        if (profit) {
            notification.open({
                description: <p> 您有<b> {profit.Count} </b>条注单已结算，盈亏<b>{profit.Profit}</b></p>
            });
            this.queryDealInfo(this.props);
        }
    }


    /* ---------------------------- 事件 ------------------------------*/

    /* ---------------------------- Renders ------------------------------*/

    renderBetHistory() {
        const columns = [
            { title: '玩法', dataIndex: 'GameTypeName' },
            {
                title: '期号', dataIndex: 'Qh', render: (qh, it) => {
                    if (it.RefId > 0) {
                        return (
                            <span className="zhui">
                                {qh}
                                <label className="zhui"></label>
                            </span>
                        );
                    }
                    return qh;
                }
            },
            {
                title: '投注内容', dataIndex: 'BetNumber', render(num) {
                    const beautityNum = BetHelper.betNumBeautify(num);
                    return <Tooltip title={beautityNum}><p className="betnum">{beautityNum}</p></Tooltip>
                }
            },
            {
                title: "盈亏", className: "fn-text-right", render(deal) {
                    if (deal.Status == 0) return '';
                    else if (deal.Status == 5) return Constants.DealStatus[5];
                    var profit = deal.WinBets > 0 ? deal.BetAmount * deal.Scale * deal.WinBets : deal.Turnover * -1;
                    return <span className={profit >= 0 ? "fn-color-green" : "fn-color-red"}>{profit.toFixed(2)}</span>;
                }
            },
        ];

        var { dealinfo } = this.props;
        let data = dealinfo.data || [];

        return (
            <div>
                <Table loading={dealinfo.isFetching}
                       columns={columns}
                       rowKey="Id"
                       dataSource={data}
                       size="middle"
                       scroll={{ y: 290 }}
                       pagination={false} />
                <div className="footer">
                    <a className="fn-left" onClick={() => this.queryDealInfo(this.props)}><Icon type="reload" /> 刷新</a>
                    <a className="fn-right" onClick={() => this.goto("/deals")}>更多 <Icon type="right" /></a>
                </div>
            </div>
        );
    }

    renderOpenResult() {
        const { Group } = this.props;

        var columns = [
            { title: '期号', dataIndex: 'Qh', width: 100 },
            {
                title: "开奖号码", width: 220, dataIndex: "Result", render(text) {
                    return (
                        Utils.isNonEmptyString(text)
                            ?
                            <ul className="nums">
                                {text.split(",").map((x, i) =>
                                    <li key={i}>{x}</li>
                                )}
                            </ul>
                            :
                            <span><Icon type="loading" /> 开奖中...</span>
                    );
                }
            },
            { title: "大小", width: 44, render(item) { return BetHelper.sumBigSmall(Group, item.Result) } },
            { title: "单双", render(item) { return BetHelper.sumOddEven(Group, item.Result) } }
        ];



        var { gamedata } = this.props;
        let data = gamedata.history.data || [];
        if (data.length > 10)
            data.slice(0, 10);

        return (
            <div>
                <Table loading={gamedata.history.isFetching}
                       columns={columns}
                       rowKey="Qh"
                       dataSource={data}
                       size="middle"
                       scroll={{ y: 290 }}
                       pagination={false} />
                <div className="footer">
                    <a className="fn-left" onClick={() => this.queryGameData(this.props)}><Icon type="reload" /> 刷新</a>
                    <a className="fn-right" onClick={() => this.goto(`/trend/${Group}`)}>更多 <Icon type="right" /></a>
                </div>
            </div>
        );
    }

    render() {
        return (
            <div className="game-list">
                <Tabs defaultActiveKey="1">
                    <TabPane tab="投注记录" key="1">
                        {this.renderBetHistory()}
                    </TabPane>
                    <TabPane tab="追号记录" key="2">Content of Tab Pane 2</TabPane>
                    <TabPane tab="开奖记录" key="3">
                        {this.renderOpenResult()}
                    </TabPane>
                </Tabs>
            </div>
        );
    }
}
GameList.propTypes = {
    // 游戏组
    Group: PropTypes.string.isRequired,
}

export default connect(state => ({
    gamedata: state.game.gamedata,
    dealinfo: state.game.dealinfo,
}))(GameList);
