import BasicComponent from "@/components/BasicComponent";

import { connect } from 'dva';
import { message, Row, Col, Alert, Tabs, Button, Input,Icon } from "antd";
import { Flex, List, InputItem, WhiteSpace, Toast } from 'antd-mobile';

import Page from "@/layouts/PageBasic";
import Form from '@/components/Form';
import CountDownButton from "@/components/CountDownButton";
import * as RenderHelper from "@/utils/render_helper";
import { isThrowStatement } from "typescript";


class ForgetPwd extends Page{
  title="找回密码";

    constructor(props){
        super(props);
        this.state = {
            // 提交按钮
            loading: false,
            // 发送验证码按钮
            captchaSending: false,
            // 发送按钮倒计时
            target: 0,
          }
          // 初始验证码地址,否则每次表单输内容验证码都会变化
          this.captchaUrl = RenderHelper.getCaptchaImgUrl();
          
    }

    handleFormSubmit(e) {
        e.preventDefault();
    
        const { dispatch } = this.props;
    
        this.form.validateFields((errors, values) => {
          if (errors) return;
    
          dispatch({
            type: "user/forgetPassword",
            payload: { email: values.email, captcha: values.captcha }
          }).then(resp => {
            this.setState({ loading: false });
    
            if (resp.code === 0) {
              Toast.success("重置密码邮件以发去邮箱", 2);
            }
            else{
              let msg = resp.message;
              Toast.fail(msg, 2);
              
            }
             
    
          });
    
          this.setState({ loading: true });
        })
      }

      onResetSubmit(e){
        e.preventDefault();

        this.props.form.resetFields();


      }
    
      

      refreshCaptcha() {
        let captcha = this.props.form.getFieldInstance("captcha");
        captcha.clearInput();
        captcha.inputRef.focus();
        this.refs.captcha_img.src = RenderHelper.getCaptchaImgUrl();
    }

    render(){
        const { getFieldProps } = this.props.form;

        return (
    
          <div className="form_sty form-countdown">
            <div className="top-header"> </div>
            <div className="form_title">
              <h5>找回密码</h5>
            </div>
    
            <Form form={this.props.form} ref={el => this.form = el} onSubmit={this.handleFormSubmit.bind(this)}>
             <InputItem
                    {...getFieldProps('email', {
                        rules: [{
                            required: true, message: "请输入邮箱"
                        }],
                    })}
                    type="email"
                    clear
                    placeholder="请输入邮箱">
                    <Icon style={{color: 'rgb(255,255,255)'}} type="mail" />
                    邮箱
                </InputItem>

              <InputItem
                    {...getFieldProps('captcha', {
                        rules: [{ required: true, message: '请输入右侧验证码' }],
                    })}
                    maxLength="4"
                    clear
                    placeholder="验证码"
                    extra={<img src={this.captchaUrl} ref="captcha_img" className="img-captcha" onClick={this.refreshCaptcha.bind(this)} />}
                >
                    <Icon style={{color: 'rgb(255,255,255)'}} type="safety-certificate" />
                    验证码
                </InputItem>
    
    
              
              
              <div className="game-orders">
                <div className="ButtonBottom">
                  <button className="btn_left" onClick={this.onResetSubmit.bind(this)}>
                    <Icon type="reload"/> 重置
                  </button>
    
                  <button className="btn_right" loading={this.state.loading} htmlType="submit" >
                    确定
                  </button>
    
                </div>
                <div className="clearfix"></div>
              </div>
            </Form>
          </div>
        );
    }

}

export default connect(state => ({
    hasSafePassword: state.user.hasSafePassword,
    loading: state.loading.models.user,
  }))(Form.create()(ForgetPwd));

