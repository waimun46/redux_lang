import React, { Component } from 'react';

import * as betHelper from '../../utils/bet_helper';


class Game extends Component
{
    constructor(props){
        super(props);
    }

    render(){
        const {profitGameTypeData} = this.props;

        return(
            <div class="popup_new">
                            <div className="popup-inner">
                                <h2>查看下级</h2>

                                <div class="content">
                                    <table>
                                        <thead>
                                            <tr className="tr-style">
                                                
                                                <th>日期:</th>
                                                <th>彩种:</th>
                                                <th>用户数:</th>
                                                <th>订单数:</th>
                                                <th>总投注:</th>
                                                <th>有效投注:</th>
                                                <th>盈亏:</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr className="tr2-style">
                                                <td>{profitGameTypeData.Date}</td>
                                                <td>{profitGameTypeData.GroupName}</td>
                                                <td>{profitGameTypeData.UserCount}</td>                                          
                                                <td>{profitGameTypeData.OrderCount}</td>
                                                <td>{profitGameTypeData.TotalBet}</td>
                                                <td>{profitGameTypeData.ValidBet}</td>
                                                <td>￥{profitGameTypeData.Profit}</td> 
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            {/* <a class="close" href="#">
                                <div className="btn-popup close">
                                    <h4>关闭</h4>
                                </div>
                            </a> */}
                        </div>


        )
    }

}

export default Game;
