import {connect} from 'dva';
import PropTypes from 'prop-types';
import React from 'react';
import {Table, Icon, Popconfirm, Timeline} from "antd";
import Link from 'umi/link';
import BetCount from "./BetCount";
import * as Constants from "@/app_constants";
import * as BetHelper from "@/utils/bet_helper";
import * as Utils from "@/utils/common";
import * as Render from "@/utils/render_helper";
import router from 'umi/router';

import wallet from '../../../assets/images/icon/wallet.png';
import ybag from '../../../assets/images/icon/ybag.png';
import { throws } from 'assert';

class BetOrderList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
           wager: 1,
           betCount: 0,
           
           
        }
        this.getGameOdd.bind(this);
    }

    componentDidMount() {
        this.getUserInfo();
       
        
    }

    componentWillMount(){
        const {totalBetAmount} = this.props;
      }

    getUserInfo() {
     this.props.dispatch({ type: "user/queryUserInfo" });
    }



    handleInputChange = (e) => {
        e.preventDefault()
        this.setState({
            [e.target.value]: e.target.value
        })
    }

    onWagerChange(e) {
            var val = e.target.value;
            if (/^\d+$/.test(val))
            {
                this.setState({ wager: parseInt(val) });
                this.setState({finalBetAmount : parseInt(val) * this.props.totalBetAmount})

                if(this.props.onWagerUpdate && this.props.onWagerUpdate(parseInt(val)));

            }
                
            else if (val == "")
                this.setState({ wager: 0 });
        }

    onSubmitClick(e) {
        e.preventDefault();
        //router.push("/resit");

       if(this.props.updateCartAndCheckout && this.props.updateCartAndCheckout());


    }
    /* ---------------------------- 系统函数 ------------------------------*/
    shouldComponentUpdate(nextProps, nextState) {
        return nextProps != this.props || nextState != this.state;
    }

    /* ---------------------------- Renders ------------------------------*/

    tableColumns() {
        var {user} = this.props;
        const self = this;
        const columns = [{
            title: '#', render(text, record, index) {
                return index + 1;
            }
        },
            {
                title: '玩法', render(item) {
                    var sb = [item.gameType.Sort];
                    if (item.gameType.Sort != item.gameType.Kind)
                        sb.push(item.gameType.Kind);
                    if (item.gameType.Name != item.gameType.Sort && item.gameType.Name != item.gameType.Kind)
                        sb.push(item.gameType.Name);
                    return sb.join("-");
                }
            },
            {
                title: "号码", width: 200, render(item) {
                    var betNum = item.betNum;
                    if (Utils.isString(item.betNum)) {
                        var nums_split_symbols = item.gameType.MaxNum > 9 ? Constants.TWO_DIGITS_NUMS_SPLIT_CHARS : Constants.NUMS_SPLIT_CHARS;
                        var num_split_symbols = item.gameType.MaxNum > 9 ? Constants.NUM_SPLIT_CHAR : null;
                        betNum = BetHelper.removeInvalidBetNum(item.betNum, item.gameType.BetNumCount, nums_split_symbols, num_split_symbols);
                    }
                    return BetHelper.convertBetNumToString(item.gameType, betNum, item.unitPos);
                }
            },
            {title: "金额", dataIndex: "wager", render: Render.render_milli_format},
            {title: "注数", dataIndex: "betCount"},
            {
                title: "总金额", render(item) {
                    return Utils.milliFormat(item.wager * item.betCount);
                }
            },
            {
                title: "赔率", render(item) {
                    var odds = user.data ? (item.gameType.Scale + item.gameType.ScaleBase * user.data.Backpct) : null;
                    if (odds && !Number.isInteger(odds))
                        odds = parseFloat(odds.toFixed(2));
                    return `${odds * 2}/${odds}`;
                }
            },
            {
                title: <Popconfirm title="您确定要清空所有记录吗？" onConfirm={() => self.props.onClear && self.props.onClear()}><a><Icon
                    type="delete"/></a></Popconfirm>, render(text, record, index) {
                    return (
                        <Popconfirm title="您确定要删除吗？"
                                    onConfirm={() => self.props.onRemove && self.props.onRemove(index)}>
                            <a><Icon type="close"/></a>
                        </Popconfirm>
                    );
                }
            },
        ];

        return columns;
    }

    getGameName(item){
        var sb = [item.gameType.Sort];
        if (item.gameType.Sort != item.gameType.Kind)
            sb.push(item.gameType.Kind);
        if (item.gameType.Name != item.gameType.Sort && item.gameType.Name != item.gameType.Kind)
            sb.push(item.gameType.Name);
        return sb.join("-");
    }

    getGameOdd(item){
        var {user} = this.props;

        var odds = user ? (item.gameType.Scale + item.gameType.ScaleBase * user.Backpct) : null;
        if (odds && !Number.isInteger(odds))
            odds = parseFloat(odds.toFixed(2));
        return `${odds * 2}/${odds}`;
    }

    

    render() {
        let columns = this.tableColumns();
        let {data , totalBetAmount} = this.props;
        const userInfo = this.props.user;
        const self = this;

        const getBetNum =  (item) => {
            var betNum = item.betNum;
            if (Utils.isString(item.betNum)) {
                var nums_split_symbols = item.gameType.MaxNum > 9 ? Constants.TWO_DIGITS_NUMS_SPLIT_CHARS : Constants.NUMS_SPLIT_CHARS;
                var num_split_symbols = item.gameType.MaxNum > 9 ? Constants.NUM_SPLIT_CHAR : null;
                betNum = BetHelper.removeInvalidBetNum(item.betNum, item.gameType.BetNumCount, nums_split_symbols, num_split_symbols);
            }
            return BetHelper.convertBetNumToString(item.gameType, betNum, item.unitPos);


        }
        
        let betCount = (data.length > 0 ? data[0].betCount : 0);


        return (
            <div className="game-orders">
            {/*
                <div className="ButtonBottom">
                    <div className="btn_left">
                        <Icon type="reload"/> 重置
                    </div>
                    <a href="#popup-game-orders">
                        <div className="btn_right">
                            立即投注
                        </div>
                    </a>
                    <div className="clearfix"></div>
                </div>*/}
                <div id="popup-game-orders" className="overlay">
                    <div className="popup">
                        <div className="order-top">
                            <h2>注单信息</h2>
                            <div className="left-content">
                                <div className="side-1">
                                    <span>玩法类型</span>
                                    <p>{data.length > 0 ? this.getGameName(data[0]) : ''}</p>
                                </div>
                                <div className="side-2">
                                    <span>购买号码</span>
                                    <p>{data.length > 0 ? getBetNum(data[0]): ''}</p>
                                </div>

                            </div>
                            <div className="right-content">
                                <div className="side-1">
                                    <span>赔率</span>
                                    <p>{data.length > 0 ? this.getGameOdd(data[0]): ''}</p>
                                </div>
                                <div className="side-2">
                                    <span>下注注数</span>
                                    <p>{data.length > 0 ? data[0].betCount : ''}</p>
                                </div>
                            </div>
                            <div className="clearfix"></div>
                        </div>

                        <div className="order-middle">
                            <div className="input-order">
                                <div className="input-order-warp">
                                    金额：
                                    <input
                                        type="text"
                                        placeholder="请输入金额"
                                        name="bet_price"
                                        // onChange={this.handleInputChange}
                                        value={this.state.wager} onChange={this.onWagerChange.bind(this)}
                                    />
                                    元
                                </div>
                            </div>
                        </div>

                        <div className="order-bottom">
                            <Timeline>
                                <Timeline.Item>
                                    <div className="infor-order">
                                        <p>余额</p>
                                        <p><span style={{color: '#faad14'}}>&#165; </span>{userInfo && Render.money_format(userInfo.Balance)}</p>
                                    </div>
                                    <div className="wallet-icon">
                                        <img src={wallet} alt="wallet"/>
                                    </div>
                                    <div className="clearfix"></div>
                                </Timeline.Item>

                                <Timeline.Item>
                                    <div className="infor-order">
                                        <p>下注总额</p>
                                        <p style={{color: 'red'}}>{this.state.wager * betCount}</p>
                                    </div>
                                    <div className="ybag-icon">
                                        <img src={ybag} alt="ybag"/>
                                    </div>
                                </Timeline.Item>
                            </Timeline>
                        </div>

                        <div className="order-button">
                            <a className="close" href="#" onClick={() => self.props.onClear && self.props.onClear()}>
                                <div className="left-btn ">
                                    取消
                                </div>
                            </a>
                            <a onClick={self.onSubmitClick.bind(this)}>
                                <div className="right-btn" >
                                确定

                                </div>
                            </a>
                            {/* <Link to="/resit">
                                <div className="right-btn">
                                    确定
                                </div>
                            </Link> */}
                            <div className="clearfix"></div>
                        </div>
                        {/*
                        <Table columns={columns} dataSource={data} size="middle" pagination={false}/>
`                       */}
                    </div>
                </div>


            </div>

        );
    }
}

BetOrderList.propTypes = {
    // 投注内容
    data: PropTypes.array.isRequired,
    /**
     * 清空所有投注记录
     * @type {[type]}
     */
    onClear: PropTypes.func,
    /**
     * 删除指定下标记录
     * @param {int} index
     */
    onRemove: PropTypes.func,

    /**
     * 添加投注内容事件
     * @param { GameType } gameType 玩法
     * @param { number } betCount 注数
     * @param { number } wager 单注金额
     * @param { dict|str } betNum 投注号码
     * @param { array|null } unitPos 任选数位
     */

    onBetNumAdd: PropTypes.func
}


//export default (BetOrderList);

export default connect(state => ({
    user: state.user.user
}))(BetOrderList);
