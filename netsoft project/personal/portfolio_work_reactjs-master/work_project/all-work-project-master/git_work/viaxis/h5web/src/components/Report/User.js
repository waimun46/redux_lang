import React, { Component } from 'react';

import * as betHelper from '../../utils/bet_helper';


class User extends Component
{
    constructor(props){
        super(props);
    }

    render(){
        const {profitData} = this.props;

        return(
            <div class="popup_new">
                            <div className="popup-inner">
                                <h2>查看下级</h2>

                                <div class="content">
                                    <table>
                                        <thead>
                                            <tr className="tr-style">
                                                <th>邮箱:</th>
                                                <th>日期:</th>
                                                <th>订单数:</th>
                                                <th>总投注:</th>
                                                <th>有效投注:</th>
                                                <th>奖金:</th>
                                               
                                                <th>佣金:</th>
                                                <th>盈亏:</th>
                                                <th>充值:</th>
                                                <th>提现:</th>
                                                <th>充值数:</th>
                                                <th>提现数:</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr className="tr2-style">
                                                <td
                                                    style={{
                                                        color: 'blue'
                                                    }}>{profitData.Email}</td>
                                                <td>{profitData.Date}</td>
                                                <td>{profitData.OrderCount}</td>
                                                <td>{profitData.TotalBet}</td>                                          
                                                <td>{profitData.ValidBet}</td>
                                                <td>￥{profitData.Bonus}</td>

                                                <td>￥{profitData.Commission}</td>
                                                <td>￥{profitData.Profit}</td> 
                                                <td>￥{profitData.Deposit}</td>
                                                <td>￥{profitData.Withdraw}</td>
                                                <td>{profitData.DepositCount}</td>
                                                <td>{profitData.WithdrawCount}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            {/* <a class="close" href="#">
                                <div className="btn-popup close">
                                    <h4>关闭</h4>
                                </div>
                            </a> */}
                        </div>


        )
    }

}

export default User;
