import PropTypes, { object } from 'prop-types';
import { Form, Input, Select, message, Button } from "antd";
import { Picker, List, InputItem, WhiteSpace, Toast } from 'antd-mobile';
import BasicComponent from "@/components/BasicComponent";
import Auth from "@/utils/auth";
import * as Utils from "@/utils/common";
import * as Validators from "@/utils/validator";

import banklogo1 from '../../../assets/images/icon/banklogo1.png'
import banklogo2 from '../../../assets/images/icon/banklogo2.png'
import banklogo4 from '../../../assets/images/icon/banklogo4.png'
import banklogo5 from '../../../assets/images/icon/banklogo5.png'
import banklogo6 from '../../../assets/images/icon/banklogo6.png'
import banklogo7 from '../../../assets/images/icon/banklogo7.png'
import banklogo8 from '../../../assets/images/icon/banklogo8.png'
import banklogo9 from '../../../assets/images/icon/banklogo9.png'
import banklogo10 from '../../../assets/images/icon/banklogo10.jpg'

const Item = List.Item;
const Brief = Item.Brief;


const colorStyle = {
  display: 'inline-block',
  verticalAlign: 'middle',
  width: '20px',
  height: '20px',
  marginRight: '10px',
};

class AddCard extends BasicComponent {

  constructor(props) {
    super(props);
    this.state = { 
      loading: false, 
      bankInfo: {},
      selectedBankId:0
     };
  }


  state = {
    iconValue: ['#00FF00'],
  };

  componentWillMount(){
    const { banks} = this.props;

    let bankInfo = banks.map(item =>{
      return {
        value: item.Id,
        label: item.Name
      }
    });
    this.setState({bankInfo})
  }

  handleSubmit(e) {
    e.preventDefault();

    const { dispatch } = this.props;

    this.props.form.validateFields((errors, values) => {
      if (errors) return;
      this.setState({ loading: true });
      Object.assign(values,{BankId:this.state.selectedBankId});
      this.props.onSubmit && this.props.onSubmit(values).then(act => {
        this.setState({ loading: false });

        const resp = act;
        if (resp.code == 0) {
          Toast.success("添加成功",2);

          
          if (!this.realname && values.account_name) {
            Auth.update("realname", values.account_name);
          }
        } else {
          Toast.fail(resp.message);
          
        }

      });

    });
  }

  onChangeIcon = (val) => {
    this.setState({
      selectedBankId: val[0],
    });
  };

  /* ---------------------------- Renders  ------------------------------*/
  render() {
    const { getFieldDecorator,getFieldProps } = this.props.form;

    const formItemLayout = {
      labelCol: { span: 6 },
      wrapperCol: { span: 14 },
    };

    const tailFormItemLayout = {
      wrapperCol: {
        span: 14,
        offset: 6,
      },
    };

    const { banks } = this.props;

    return (
      <div className="add-bank-style">
        <Form  onSubmit={this.handleSubmit.bind(this)} autoComplete="off">
          <List >

            <Picker
              data={this.state.bankInfo}
              value={{0:this.state.selectedBankId}}
              cols={1}
              onChange={this.onChangeIcon }
            >

              <List.Item arrow="horizontal" >开户银行</List.Item>
            </Picker>

          </List>
          <div className="form_title2">
            <h5>填写银行信息</h5>
          </div>

        

          <div className="add-bank-input">
                <List>
                  <InputItem
                    {...getFieldProps('AccountName', {
                      rules: [{
                        required: true, message: '请输入正确的开户支行',
                      }],
                    })}
                    label="Note"
                    type=""
                    cleartext
                    placeholder="请输入持卡人姓名"
                  >持卡人</InputItem>
                </List>
          </div>

          <div className="add-bank-input">
            <List>
              <InputItem
                {...getFieldProps('AccountNo', {
                  rules: [{
                    required: true, message: '请输入银行卡号',
                  }],
                })}
                type=""
                cleartext
                placeholder="请输入银行卡号"
              >银行卡号</InputItem>
            </List>
          </div>

          <div className="add-bank-input">
            <List>
              <InputItem
                {...getFieldProps('AccountNoConfirm', {
                  rules: [{
                    required: true, message: '两次输入卡号不一致',
                  }],
                })}
                type=""
                cleartext
                placeholder="请输入正确银行卡号"
              >确认卡号</InputItem>
            </List>
          </div>

          <div className="add-bank-input">
            <List>
              <InputItem
                {...getFieldProps('BranchName', {
                  rules: [{
                    required: true, message: '开行具体名，可拨打银行服务热线咨询',
                  }],
                })}
                type=""
                cleartext
                placeholder="请输入支行信息"
              >支行信息</InputItem>
            </List>
            </div>
            
            <div className="add-bank-input">
            <List>
              <InputItem
                {...getFieldProps('Password', {
                  rules: [{
                    required: true, message: '请输入您的安全密码',
                  }],
                })}
                type="password"
                cleartext
                placeholder="请输入您的安全密码"
              >安全密码</InputItem>
            </List>
            
            
            </div>

    
      
          <Form.Item {...tailFormItemLayout}>
            <Button loading={this.state.loading} className="fn-width100" type="primary" htmlType="submit" size="large">确定</Button>
          </Form.Item>
        </Form>
      </div>
    );
  }
}
AddCard.propTypes = {
  /**
   * 银行列表
   * @type {[type]}
   */
  banks: PropTypes.array.isRequired,
  /**
   * 确定提交事件
   * @param {dict} values 表单内容
   * @type {[type]}
   */
  OnSubmit: PropTypes.func
};
export default Form.create()(AddCard);
