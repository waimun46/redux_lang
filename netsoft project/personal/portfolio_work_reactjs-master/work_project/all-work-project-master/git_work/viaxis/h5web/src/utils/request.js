import fetch from 'dva/fetch';
import { routerRedux } from 'dva/router';
import Auth from "./auth";
import { isNullOrUndefined } from 'util';
import { createNoSubstitutionTemplateLiteral } from 'typescript';

//const serv = (uri) => config.gateway + uri;

export function POST(url, body, headers = {}) {
    if (!isNullOrUndefined(apiUrl))
        url = apiUrl + url;

    return request(url, {
        method: "POST",
        body: body,
        mode:'cors',
        credentials:'include',
        headers: Object.assign({}, {
            "Content-Type": "application/json",
            "Authorization": Auth.getAuthToken()
        }, headers)
    });
}

export function GET(url, headers = {}) {
    if (!isNullOrUndefined(apiUrl))
        url = apiUrl + url;
    return request(url, {
        method: "GET",
        mode:'cors',
        credentials:'include',
        headers: Object.assign({}, {
            "Authorization": Auth.getAuthToken()
        }, headers)
    });
}


function checkStatus(response) {
    if (response.status >= 200 && response.status < 300) {
        return response;
    }

    const error = new Error(response.statusText);
    error.response = response;
    throw error;
}

/**
 * Requests a URL, returning a promise.
 *
 * @param  {string} url       The URL we want to request
 * @param  {object} [options] The options we want to pass to "fetch"
 * @return {object}           An object containing either "data" or "err"
 */
export function request(url, options) {
    let headers = {}
    return fetch(url, options)
        .then(checkStatus)
        .then(response => {
            // if (response.headers.get('x-total-count')) {
            //     headers['x-total-count'] = response.headers.get('x-total-count');
            // }
            return response.json();
        });
    // 这里不处理任何错误,直接由 app.js 里统一处理
    // .catch ((error) => {});
}

// 状态码错误信息
const codeMessage = {
    200: '服务器成功返回请求的数据。',
    201: '新建或修改数据成功。',
    202: '一个请求已经进入后台排队（异步任务）。',
    204: '删除数据成功。',
    400: '发出的请求有错误，服务器没有进行新建或修改数据的操作。',
    401: '用户没有权限（令牌、用户名、密码错误）。',
    403: '用户得到授权，但是访问是被禁止的。',
    404: '发出的请求针对的是不存在的记录，服务器没有进行操作。',
    406: '请求的格式不可得。',
    410: '请求的资源被永久删除，且不会再得到的。',
    422: '当创建一个对象时，发生一个验证错误。',
    500: '服务器发生错误，请检查服务器。',
    502: '网关错误。',
    503: '服务不可用，服务器暂时过载或维护。',
    504: '网关超时。',
};