import router from 'umi/router';
import Redirect from 'umi/redirect';
import Auth from '@/utils/auth';

export default (props) => {

    if (!Auth.isLogged()) {
        // return router.push('/');
        return <Redirect to="/login" />
    }

    return props.children;
}
