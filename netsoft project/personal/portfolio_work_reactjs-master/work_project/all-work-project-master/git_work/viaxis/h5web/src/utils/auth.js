import * as Utils from "./common";

var jwt = null;
var Auth = {

	login(data) {
		const { UserId, Email, Realname, Mobile, QQ, Verified } = data.User;
		sessionStorage.xtoken = data.Token;
		sessionStorage.authenticated = true;
		sessionStorage.userinfo = JSON.stringify({
			UserId,
			Email,
			Realname,
			Mobile,
			QQ,
			Verified
		});
	},

	get(name) {
		if (!sessionStorage.userinfo) return null;

		const userinfo = Utils.isString(sessionStorage.userinfo) ? JSON.parse(sessionStorage.userinfo) : sessionStorage.userinfo;
		return userinfo[name];
	},

	update(key, value) {
		const userinfo = Utils.isString(sessionStorage.userinfo) ? JSON.parse(sessionStorage.userinfo) : {};
		userinfo[key] = value;
		sessionStorage.userinfo = JSON.stringify(userinfo);
	},

	getAuthToken() {
		if (!this.isLogged()) return "";

		var user = JSON.parse(sessionStorage.userinfo);
		if (user == null) return false;

		if (jwt === null) {
			if (!window._crypto)
				window._crypto = null;
			jwt = require('jsonwebtoken');
		}

		return "Bearer " + jwt.sign({ UserId: user.UserId }, sessionStorage.xtoken);
	},

	/**
	 * 用户是否已登入
	 * @return {Boolean} [description]
	 */
	isLogged() {
		return !!sessionStorage.authenticated && !!sessionStorage.userinfo;
	},

	/**
	 * 将用户设置为未登录
	 * @return {[type]} [description]
	 */
	logout() {
		sessionStorage.removeItem("authenticated");
		sessionStorage.removeItem("xtoken");
		sessionStorage.removeItem("userinfo");
	},

};

export default Auth;
