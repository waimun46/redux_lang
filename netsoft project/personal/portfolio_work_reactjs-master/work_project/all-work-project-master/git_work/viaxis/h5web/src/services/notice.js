import { GET, POST } from '@/utils/request';

export function getBulletins(status) {
	var body = JSON.stringify({
		status,
	});
	return POST("/api/notice/bulletins", body);
}
