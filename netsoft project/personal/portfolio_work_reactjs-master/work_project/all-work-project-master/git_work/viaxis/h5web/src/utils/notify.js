import React from 'react';
import { notification, Icon } from "antd";

import Auth from "./auth";
import * as RenderHelper from "./render_helper";

export default function notify(command, data) {
    var fn = Notify[command];
    if (fn)
        fn(data);
}

class Notify {
    static DepositNotify(amount) {
        notification.open({
            message: '充值成功',
            description: `您充值的 ${amount} 已到账`,
            icon: <Icon type="check-circle" style={{ color: '#108ee9' }} />,
        });
    }

    static WithdrawNotify(amount) {
        notification.open({
            message: '提现成功',
            description: `您申请提现 ${amount} 已完成`,
            icon: <Icon type="check-circle" style={{ color: '#108ee9' }} />,
        });
    }

    static WithdrawRefuseNotify(data) {
        notification.open({
            message: '提现通知',
            description: `您有一笔 ${data.Amount} 的提现由于 ${data.Remark} 暂停出金，金额已发回您的余额中`,
            icon: <Icon type="check-circle" style={{ color: '#108ee9' }} />,
        });
    }

    static KickOutNotify() {
        RenderHelper.alert({
            title: "您的账号已在别处登陆",
            content: "如若不是您本人操作，请尽快修改您的登陆密码",
            onOk: Auth.logout,
            className: "warning",
            iconType: "exclamation-circle"
        });
    }
}
