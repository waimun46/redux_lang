import {connect} from 'dva';
import PropTypes from 'prop-types';
import React from 'react';
import Link from 'umi/link';
import {Icon} from 'antd';
import {Modal} from 'antd-mobile';


import BasicComponent from "@/components/BasicComponent";
import Countdown from './Countdown';

// import Config from "../../../config";
import * as Utils from "@/utils/common";
import * as RenderHelper from "@/utils/render_helper";

const alert = Modal.alert;

class GameSummary extends BasicComponent {
    constructor(props) {
        super(props);
    }

    /* ----------------- 页面加class ----------------- */
    componentDidMount() {
        if (/kl8/.test(window.location.href)) {
            let element = document.getElementsByClassName('game-body')[0];
            element.classList.add("kl8");
        }
        if (/fc3d/.test(window.location.href)) {
            let element = document.getElementsByClassName('game-body')[0];
            element.classList.add("FC3D");
        }
        if (/cqssc/.test(window.location.href)) {
            let element = document.getElementsByClassName('game-body')[0];
            element.classList.add("CQSSC");
        }
        if (/keno/.test(window.location.href)) {
            let element = document.getElementsByClassName('game-body')[0];
            element.classList.add("KENO");
        }
        if (/pk10/.test(window.location.href)) {
            let element = document.getElementsByClassName('game-body')[0];
            element.classList.add("PK10");
        }
        if (/pl3/.test(window.location.href)) {
            let element = document.getElementsByClassName('game-body')[0];
            element.classList.add("PL3");
        }
        if (/kl10/.test(window.location.href)) {
            let element = document.getElementsByClassName('game-body')[0];
            element.classList.add("kl10");
        }
    }

    /* ----------------- Sys ----------------- */

    componentWillMount() {
        this.getGameData(this.props);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.Group != this.props.Group) {
            this.getGameData(nextProps);
        }
    }

    /* ----------------- Functions ----------------- */

    getGameData(props) {
        this.props.dispatch({ type: "gamedata/getGameDataCurrent", payload: props.Group });
        this.props.dispatch({ type: "gamedata/getGameDataPrev", payload: props.Group });
    }

    /* ----------------- Events ------------------ */

    onBetTimeEnd(qh) {

        RenderHelper.playSound("/stopBet.mp3");

        if (this.modal) return;

        this.modal = alert('注意!!!' ,'第'+ qh +'期投注时间已结束，投注内容将进入到下一期！', [
           // { text: 'Cancel', onPress: () => console.log('cancel') },
            { text: 'Ok', onPress: () => {
                
                if (this.modal)
                this.modal = null;
                
                } 
            },
          ])
  

        // if (this.modal) return;

        // this.modal = Modal.warning({
        //     title: "注意!!!",
        //     content: `第${qh}期投注时间已结束，投注内容将进入到下一期！`,
        //     onOk: function () {
        //         if (this.modal)
        //             this.modal = null;
        //     }.bind(this)
        // });

    }

    onWaitTimeEnd() {
        RenderHelper.playSound("/acceptBet.mp3");
    }

    onTick(seconds, acceptBet) {
        if (acceptBet && seconds == 10) {
            RenderHelper.playSound("/10s.mp3");
        } else if (acceptBet && seconds <= 5 && seconds > 0) {
            RenderHelper.playSound("/coundown.mp3");
        }
    }

    /* ----------------- Render ------------------ */

    render() {
        const { Group } = this.props;
        const { prev, current } = this.props;
        const numCount = config.GameDataNum[Group];
        const isOpened = prev && Utils.isNonEmptyString(prev.Result);
        let ballProps = {};
        if (numCount <= 5) ballProps["className"] = "big";
        else if (numCount <= 8) ballProps["className"] = "middle";

        return (
            <div className="game-summary">

                <div className="game-summary-logo">
                    <i className={`iconfont icon-${Group}`}/>
                </div>

                <Countdown Group={this.props.Group} onTick={this.onTick.bind(this)}
                           onBetTimeEnd={this.onBetTimeEnd.bind(this)}
                           onWaitTimeEnd={this.onWaitTimeEnd.bind(this)}/>

                <div className="game-summary-last-open ">
                    <div className="game-summary-last-open-qh">
                        第<b>{prev && prev.Group == this.props.Group ? prev.Qh :
                        <span><Icon type="loading"/> loading...</span>}</b> 期
                        <span>开奖号码</span>
                    </div>
                    <div className="price_ball">
                        <ul className={(isOpened ? "" : "opening ") + "game-summary-last-open-nums"}>
                            {
                                isOpened
                                    ?
                                    prev.Result.split(",").map((x, i) => <li {...ballProps} key={i}>
                                        <div className="ball">{x}</div>
                                    </li>)
                                    :
                                    [, ...Array(numCount)].map((x, i) => <li {...ballProps} key={i}>
                                        <Icon type="loading" className="ball-loading"/>
                                    </li>)
                            }
                            <div className="clearfix"></div>
                        </ul>
                    </div>
                </div>

                {/*

                <div className="game-summary-links">
                    <a target="_blank" href={`/trend/${this.props.Group}`}><Icon type="area-chart" /> 号码走势</a>
                    <Link to="/"><Icon type="question" /> 游戏说明</Link>
                </div>
              */}

            </div>
        );
    }
}

GameSummary.propTypes = {
    // 游戏组
    Group: PropTypes.string.isRequired,
}

module.exports = connect(state => ({
    current: state.gamedata.current,
    prev: state.gamedata.prev,
    gamedata: state.gamedata.history
}))(GameSummary);
