/**
 * Routes:
 *   - ./src/routes/AuthorizationRoute.js
 */

import { connect } from 'dva';
import { message, Row, Col, Alert, Tabs, Button, Input,Icon,Card  } from "antd";
import { List, InputItem, WhiteSpace } from 'antd-mobile';

import Page from "@/layouts/PageBasic";
import Form from '@/components/Form';
import * as RenderHelper from "@/utils/render_helper";


const Item = List.Item;
const Brief = Item.Brief;


class QQpay extends Page {
  title = "QQ钱包"
  constructor(props) {
    super(props);
    this.state = {
      // 提交按钮
      loading: false,
    }
    // 初始验证码地址,否则每次表单输内容验证码都会变化
    this.captchaUrl = RenderHelper.getCaptchaImgUrl();
  }

  /* ---------------------------- Sys Functions ------------------------------*/




  /* ---------------------------- Events ------------------------------*/

  handleUpdateSubmit(e) {
    e.preventDefault();

    const { dispatch } = this.props;

    this.form.validateFields((errors, values) => {

      if (errors) return;

      dispatch({
        type: "user/updateLoginPassword",
        payload: { currentPwd: values.current_pwd, password: values.password, captcha: values.captcha }
      }).then(resp => {
        this.setState({ loading: false });
        if (resp.code == 0) {
          let msg = "您的密码已修改，请重新登录";
          message.success(msg);
          this.props.form.resetFields();
        } else {
          this.refreshCaptcha();
          message.error(resp.message);
        }

      });

      this.setState({ loading: true });
    })
  }



  /* ---------------------------- Renders ------------------------------*/

  render() {
    const { getFieldDecorator, getFieldProps } = this.props.form;
    return (
      <div className="form_sty">
        <div className="top-header"> </div>
        <div className="form_title">
          <h5>填写修改信息</h5>
        </div>
        <Form form={this.props.form} ref={el => this.form = el} onSubmit={this.handleUpdateSubmit.bind(this)}>

          <InputItem
            {...getFieldProps('current_pwd', {
              rules: [{
                required: true, message: '请输入充值金额',
              }],
            })}
            type=""
            cleartext
            placeholder="请输入充值金额">
            充值金额
          </InputItem>


          <Card>
            <p>1.扫一扫弹出的二维码进行充值。</p>
            <p>2.单笔充值金额最底10元，最高50000元。</p>
            <p>3.可以使用其他手机二维码进行充值，也可以将二维码保存到相册
              再使用支付宝识别相册中的二维码进行充值，该二维码仅当次有
              效，每次充值务必重新保存最新的二维码。</p>

          </Card>

          <div className="game-orders">
            <div className="ButtonBottom">
              <button className="btn_left">
                <Icon type="reload"/> 重置
              </button>

              <button className="btn_right" loading={this.state.loading} htmlType="submit" >
                确定
              </button>

            </div>
            <div className="clearfix"></div>
          </div>


        </Form>

      </div>


    );
  }
};

export default connect(state => ({

  loading: state.loading.models.user,
}))(Form.create()(QQpay));
