import { routerRedux } from 'dva/router';
import * as pstService from '@/services/passport';
import * as billService from '@/services/bill';

export default {
    namespace: 'global',
    state: {
        title: "Home",
        userinfo: null,
        balance: null
    },
    reducers: {
        saveData(state, { payload }) {
            return { ...state, ...payload };
        },
        setBalance(state, { payload: { data } }) {
            return {
                ...state,
                balance: data,
            };
        },
        setTitle(state, { payload }) {
            return {
                ...state,
                title: payload,
            };
        }
    },
    effects: {
        *login({ payload: { username, password, captcha } }, { call, put }) {
            const data = yield call(pstService.login, username, password, captcha);
            if (data.code === 0) {
                yield put({
                    type: 'saveData', payload: { userinfo: data.data }
                });
            }
            return data;
        },
        *getBalance(action, { call, put }) {
            const data = yield call(billService.getBalance);
            if (data.code === 0)
                yield put({
                    type: 'setBalance', payload: data
                });
        },
        *loadUserInfo(action, { select, put }) {
            let userinfo = yield select(state => state.userinfo);
            if (userinfo === null) {

            }
        }
    },
    subscriptions: {
        setup({ dispatch, history }) {
            const skipPaths = ["/", "/login", "/logout","/forgetpwd"];
            return history.listen(({ pathname, query }) => {
                if (!skipPaths.includes(pathname)) {
                    dispatch({ type: 'loadUserInfo' });
                    var websocket = require("@/utils/websocket").default;
                    websocket && websocket();
                }
            });
        },
    },
};