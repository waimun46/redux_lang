
import Countdown from './CountDown';
import { Button } from "antd";

export default class CountDownButton extends Countdown {
    constructor(props) {
        super(props);
    }

    format = (time) => {
        const s = Math.floor(time / 1000);
        return (
            <span style={{ fontSize: 11 }}>({fixedZero(s)}秒)</span>
        );
    }

    render() {
        const { timeDiff, ...rest } = this.props;
        let { lastTime } = this.state;
        const result = this.format(lastTime);

        return (
            <Button {...rest} disabled={lastTime > 0}>
                {this.props.children}
                {lastTime > 0 && result}
            </Button>
        );
    }
}

function fixedZero(val) {
    return val * 1 < 10 ? `0${val}` : val;
}