/**
 * Routes:
 *   - ./src/routes/AuthorizationRoute.js
 */

import { connect } from 'dva';
import { message, Row, Col, Alert, Tabs, Button, Input,Icon } from "antd";
import { List, InputItem, WhiteSpace } from 'antd-mobile';
import Link from 'umi/link';
import Page from "@/layouts/PageBasic";
import Form from '@/components/Form';
import { isConstructorDeclaration } from 'typescript';


const Item = List.Item;
const Brief = Item.Brief;


class Security extends Page {
  title = "安全中心"
  constructor(props) {
    super(props);
  }

  componentWillMount(){
    this.props.dispatch({ type: "user/hasSafePassword"});
  }

  


  componentDidMount() {
    this.getUserInfo();
  }

  getUserInfo() {
    this.props.dispatch({ type: "user/queryUserInfo" });
  }

  render() {

    let hasSafePassword = this.props.userinfo.hasSafePassword;
    
    return (
      <div className="form_sty">
        <div className="top-header"> </div>
        <div className="form_title">
          <h5>选项列表</h5>
        </div>


        <List >
          <Link className="border-line" to="/passport/password">
             <Item arrow="horizontal" onClick={() => {}}>修改登录密码</Item>
          </Link>
          {
            hasSafePassword?
            <Link className="border-line" to="/passport/password/reset">
              <Item arrow="horizontal">修改安全密码</Item>
            </Link> :
            <Link className="border-line" to="/passport/password/safe">
            <Item arrow="horizontal">设置安全密码</Item>
          </Link>

          }

           
         
          {/* <Link className="border-line" to="/personal_information/inforpage/security-center/content/ResetQQ">
              <Item arrow="horizontal" onClick={() => {}}>修改QQ账号</Item>
          </Link> */}
         


        </List>



      </div>


    );
  }
};

export default connect(state => ({
  userinfo: state.user
}))(Form.create()(Security));
