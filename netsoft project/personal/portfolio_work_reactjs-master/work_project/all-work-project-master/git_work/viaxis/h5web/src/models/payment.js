
import * as paymentService from '@/services/payment';

export default {
    namespace: 'payment',
    state:{
        paymentChannel: {
            isFetching: false,
            data: []
        },
        payRequestResp: {
            isFetching: false,
            data: []
        },

    },
    reducers:{

    },
    effects:{
        *queryPayChannelInfo({ payload }, { call, put }) {
            const resp = yield call(paymentService.queryPaymentChannel, payload.channelId);
            return resp;
        },

        *submitPayRequest({ payload }, { call, put }) {
            const resp = yield call(paymentService.submitPaymentRequest, payload.amount);
            return resp;
        }

    }
};