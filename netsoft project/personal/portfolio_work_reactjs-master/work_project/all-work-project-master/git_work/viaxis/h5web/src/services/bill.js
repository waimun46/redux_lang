import { GET, POST } from '@/utils/request';


export function getBalance() {
    return GET("/api/balance");
}

/* ---------------- BillFlow ---------------- */

/**
 * 获取流水记录
 * @param  {[type]} querys [description]
 * @return {[type]}        [description]
 */
export function queryBillflow(querys) {
    var body = JSON.stringify(querys);
    return POST(`/api/billflow`, body);
}

/* --------------- Transfer ---------------- */

/**
 * 转账
 * @param  {[type]} userId   [description]
 * @param  {[type]} way      [description]
 * @param  {[type]} amount   [description]
 * @param  {[type]} remark   [description]
 * @param  {[type]} password [description]
 * @return {[type]}          [description]
 */
export function transfer(userId, amount, remark, password) {
    var body = JSON.stringify({
        ToUserId: userId, amount, remark, password
    });
    return POST(`/api/transfer`, body);
}

/**
 * 获取转账记录
 * @param  {[type]} querys [description]
 * @return {[type]}        [description]
 */
export function queryTransfer(querys) {
    var body = JSON.stringify(querys);
    return POST(`/api/transfer/history`, body);
}

/* --------------- Withdraw ---------------- */

/**
 * 获取提现信息
 * @return {[type]} [description]
 */
export function getWithdrawInfo() {
    return POST("/api/withdraw/info");
}

/**
 * 添加提现申请
 * @param {[type]} bankId   用户银行卡Id
 * @param {[type]} amount   提现金额
 * @param {[type]} password 安全密码
 */
export function addWithdraw(bankId, amount, password) {
    var body = JSON.stringify({
        bank_id: bankId,
        amount,
        password
    });
    return POST("/api/withdraw/add", body);
}

/**
 * 获取提现记录
 * @param  {dict} querys 查询条件
 * @return {[type]}        [description]
 */
export function queryWithdraw(querys) {
    var body = JSON.stringify(querys);
    return POST(`/api/withdraw/history`, body);
}


/* ------------------- Deposit ------------------- */


/**
 * 获取充值信息
 * @return {[type]} [description]
 */
export function getDepositInfo() {
    return POST("/api/deposit/info");
}


/**
 * 获取充值封装数据
 * @return {[type]} [description]
 */
export function getDepositData(amount, com_bank_id) {
    var body = JSON.stringify({
        amount,
        com_bank_id
    });
    return POST("/api/deposit/deposit", body);
}

/**
 * 获取ips二维码
 * @return {[type]} [description]
 */
export function getDepositQrCode(amount, client_type, com_bank_id) {
    var body = JSON.stringify({
        amount,
        client_type,
        com_bank_id
    });
    return POST("/api/deposit/scanpaydeposit", body);
}
