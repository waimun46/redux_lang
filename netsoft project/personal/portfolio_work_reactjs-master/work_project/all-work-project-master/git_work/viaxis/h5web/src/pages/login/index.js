import styles from './index.css';
import {connect} from 'dva';
import Link from 'umi/link';
import router from 'umi/router';
import {
    Form, Icon, Input, Button, Checkbox, message
} from 'antd';

import {Toast} from 'antd-mobile';

import Auth from '@/utils/auth';

import logo from '../../assets/images/logo.png';

const FormItem = Form.Item;

class NormalLoginForm extends React.Component {

    constructor(props) {
        super(props);
        if (Auth.isLogged()) {
            router.push('/');
        }
    }

    handleSubmit = (e) => {
        let {dispatch} = this.props;
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                console.log('Received values of form: ', values);
                dispatch({
                    type: 'global/login',
                    payload: values,
                }).then(rsp => {
                    if (rsp.code == 0) {
                        Auth.login(rsp.data);
                        router.push('/');
                    }
                    else {
                        Toast.fail(rsp.message,1);
                        //message.error(rsp.message);
                    }
                });
            }
        });
    }

    render() {

        const {getFieldDecorator} = this.props.form;
        return (

            <div className="login-page">
                <div className="logo">
                    <Link to='/'>
                        <img src={logo} alt="logo"/>
                    </Link>

                </div>
                <Form onSubmit={this.handleSubmit} className={styles.login_form}>
                    <FormItem>
                        {getFieldDecorator('username', {
                            rules: [{required: true, message: '请输入登入名!'}],
                        })(
                            <Input autoComplete="off" prefix={<Icon type="user" style={{color: 'rgba(0,0,0,.25)'}}/>}
                                   placeholder="登入名" />
                        )}
                    </FormItem>
                    <FormItem>
                        {getFieldDecorator('password', {
                            rules: [{required: true, message: '请输入登入密码!'}],
                        })(
                            <Input autoComplete="off" prefix={<Icon type="lock" style={{color: 'rgba(0,0,0,.25)'}}/>} type="password"
                                   placeholder="登入密码" />
                        )}
                    </FormItem>
                    <FormItem>
                        {getFieldDecorator('remember', {
                            valuePropName: 'checked',
                            initialValue: true,
                        })(
                            <Checkbox>记住我</Checkbox>
                        )}
                        <Link to="/forgetpwd" className="forget_pass">忘记密码 ?</Link>
                        {/* <a className="login-form-forgot" href="">忘记密码 ?</a> */}
                        <div className="clearfix"></div>
                        <Button type="primary" htmlType="submit" className="login-form-button">
                            登入
                        </Button>
                        <div className="register-side">

                            没有账号？<Link to="/register">立即注册</Link>
                        </div>
                    </FormItem>
                </Form>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        loading: state.loading.models.global
    };
}

export default connect(mapStateToProps)(Form.create()(NormalLoginForm));
