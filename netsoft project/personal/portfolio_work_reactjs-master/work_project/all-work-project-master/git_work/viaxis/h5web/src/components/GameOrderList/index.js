import React, { Component } from 'react';
import {Icon} from "antd";

import * as Constants from "@/app_constants";


class GameOrder extends Component
{
    constructor(props){
        super(props);
    }

    render(){
        const {orderListData} = this.props;

        return(
            <div class="popup_new">
                            <div className="popup-inner ">
                                <h2>流水记录</h2>

                                <div class="content">
                                    <table>
                                        <thead>
                                            <tr className="tr-style">
                                                <th>流水类型:</th>
                                                <th>流水号:</th>
                                                <th>金额:</th>
                                                <th>余额:</th>
                                                <th>备注:</th>
                                                <th>申请时间:</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr className="tr2-style">
                                                <td
                                                style={{
                                                    color: 'blue'
                                                }}
                                                >注单{Constants.GameSubType[flowData.SubType]}</td>
                                                <td>{flowData.Txid}</td>
                                                <td className={flowData.Amount >= 0 ? "fn-color-green" : "fn-color-red"}><span>￥</span>{flowData.Amount}</td>
                                                <td><span>￥</span>{flowData.Balance}</td>
                                                <td>{flowData.Remark}</td>
                                                <td>{flowData.CreateTime}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            {/* <a class="close" href="#">
                                <div className="btn-popup close">
                                    <h4>关闭</h4>
                                </div>
                            </a> */}
                        </div>


        )
    }

}

export default GameOrder;
