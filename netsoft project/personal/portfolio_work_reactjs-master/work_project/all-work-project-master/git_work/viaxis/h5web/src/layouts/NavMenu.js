
import { Menu, Icon, Switch } from 'antd';
import { NavBar, Icon as MIcon, Drawer, List } from 'antd-mobile';
import Link from 'umi/link';
import Auth from '@/utils/auth';

import logo from '../assets/images/logo.png';


const menus = (
    <Menu mode="inline" style={{ width: 256 }} >
        <div className="menu-logo"><img src={logo} alt="" /></div>
        <Menu.Item key="0"><Link to="/home"><Icon type="home"/>首页</Link></Menu.Item>
        {/* <Menu.SubMenu key="game" title={<span><Icon type="select" /><span>游戏选项</span></span>}>
            <Menu.Item key="cqssc"><Link to="/game/cqssc/bet">重庆时时彩</Link></Menu.Item>
            <Menu.Item key="pk10"><Link to="/game/pk10/bet">北京PK10</Link></Menu.Item>
            <Menu.Item key="kl10"><Link to="/game/kl10/bet">重庆幸运农场</Link></Menu.Item>
            <Menu.Item key="kl8"><Link to="/game/kl8/bet">快乐8</Link></Menu.Item>
            <Menu.Item key="fc3d"><Link to="/game/fc3d/bet">福彩3D</Link></Menu.Item>
            <Menu.Item key="pl3"><Link to="/game/pl3/bet">排列三</Link></Menu.Item>
            <Menu.Item key="keno"><Link to="/game/keno/bet">KENO</Link></Menu.Item>
s
        </Menu.SubMenu> */}
        <Menu.Item key="1"><Link to="/gameinfor"><Icon type="question-circle" />游戏信息</Link></Menu.Item>
        <Menu.Item key="2"><Link to="/pricehall"><Icon type="trophy" />开奖大厅</Link></Menu.Item>
        <Menu.Item key="3">
          <Link to="/personal_information/inforpage/fund-management/content/topup/SelectRechargeMethod"><Icon type="pay-circle" />充值</Link>
        </Menu.Item>
        <Menu.SubMenu key="sub4" title={<span><Icon type="setting" /><span>账单详情</span></span>}>
            <Menu.Item key="4"><Link to="/bet_history"><Icon type="book" />投注记录</Link></Menu.Item>
            <Menu.Item key="5"><Link to="/personal_information/inforpage/fund-management/content/LiushuiHistory"><Icon type="file-sync" />流水记录</Link></Menu.Item>
            <Menu.Item key="6"><Link to="/personal_information/inforpage/fund-management/content/DepositHistory"><Icon type="money-collect" />充值记录</Link></Menu.Item>
            <Menu.Item key="7"><Link to="/personal_information/inforpage/fund-management/content/WithdrawalHistory"><Icon type="wallet" />提现记录</Link></Menu.Item>
        </Menu.SubMenu>
        <Menu.Item key="8"><Link to="/promotion"><Icon type="gift" />优惠活动</Link></Menu.Item>
        <Menu.Item key="9"><Link to="/personal_information"><Icon type="user" />我的账户</Link></Menu.Item>
        {/* <Menu.Item key="10"><Link to="/"><Icon type="customer-service" />在线客服</Link></Menu.Item> */}
        {Auth.isLogged() ?
            <Menu.Item key="/logout" >
                <a href="/logout"><Icon type="logout" />退出</a>
            </Menu.Item>
            :
            <Menu.Item key="/login">
                <a href="/login">登入</a>
            </Menu.Item>
        }
    </Menu >
);

export default function ({ children, isOpen = false, onOpenChange }) {
    return (
        <Drawer
            className="my-drawer"
            position="right"
            style={{ minHeight: document.documentElement.clientHeight }}
            enableDragHandle
            contentStyle={{ color: '#A6A6A6', textAlign: 'center' }}
            sidebar={menus}
            open={isOpen}
            onOpenChange={onOpenChange}
        >
            {children}
        </Drawer>
    );
}

