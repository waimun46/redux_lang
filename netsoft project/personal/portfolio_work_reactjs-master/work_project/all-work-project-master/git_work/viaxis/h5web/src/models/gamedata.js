import * as gameService from '@/services/game';
import * as Utils from '@/utils/common';

export default {
    namespace: 'gamedata',
    state: {
        // 当前期
        current: null,
        // 下一期
        next: null,
        // 上一期
        prev: [],
        // 未开奖,key:group_name,value:[]
        unopen: {},
        history: {
            count: 0,
            pageIndex: 0,
            data: []
        },
        // 历史开奖数据，这个数据主要用于投注记录中点击期号查看开奖数据。
        // 格式采用哈希格式，key为大写GroupName，value为{qh,data}
        hash: {},
        missing: [],
        game_type_info: null,
        data: [],
        latestResult:[]
    },
    reducers: {
        saveData(state, { payload }) {
            return { ...state, ...payload };
        },
        saveFC3DData(state, { payload }) {
            return { ...state, FC3D:{...payload} };
        },
        saveCQSSCData(state, { payload }) {
            return { ...state, CQSSC:{...payload} };
        },
        saveKL8Data(state, { payload }) {
            return { ...state, KL8:{...payload} };
        },
        saveKL10Data(state, { payload }) {
            return { ...state, KL10:{...payload} };
        },
        saveKENOData(state, { payload }) {
            return { ...state, KENO:{...payload} };
        },
        savePL3Data(state, { payload }) {
            return { ...state, PL3:{...payload} };
        },
        saveFFCData(state, { payload }) {
            return { ...state, FFC:{...payload} };
        },
        saveZRCData(state, { payload }) {
            return { ...state, ZRC:{...payload} };
        },
        savePK10Data(state, { payload }) {
            return { ...state, PK10:{...payload} };
        },
        saveGameData(state, { payload }) {
            let groupData = state.hash[payload.GroupId] || {};
            groupData[payload.Qh] = payload;

            let updateHash = {};
            updateHash[payload.GroupId] = groupData;

            let hash = Object.assign({}, state.hash, updateHash);
            return { ...state, hash };
        },

        /**
         * 关闭当前期数据，将其复制到上一期
         * @param  {[type]} group_name [description]
         * @param  {[type]} data       [description]
         * @return {[type]}            [description]
         */
        closeCurrentGameData(state, { payload }) {
            let { prev, current, next } = state;
            let data = {
                prev: current,
                current: next,
                next: null
            };
            return { ...state, ...data };
        },
        openGameData(state, { payload }) {
            const { Group, Qh, Results } = payload;
            let { prev, history } = state;
            if (prev.Group == Group && Qh >= prev.Qh) {
                var new_prev = { ...payload, Result: Results.join(",") };
                var history_data = [new_prev, ...history.data];
                return { ...state, prev: new_prev, history: { ...history, data: history_data } };
            }
        },
        saveLatestResult(state, { payload }){

            return { ...state, latestResult:payload};

        },
        unOpenGameData(state, { payload }) {
            // const { Group, Qh, Results } = payload;
            // let { prev, history } = state;
            // if (prev.Group == Group && Qh >= prev.Qh) {
            //     var new_prev = { ...payload, Result: Results.join(",") };
            //     var history_data = [new_prev, ...history.data];
            //     return { ...state, prev: new_prev, history: { ...history, data: history_data } };
            // }
        },
    },
    effects: {

        *getGameData({ payload: { group, qh } }, { call, put }) {
            const data = yield call(gameService.getGameData, group, qh);
            yield put({
                type: 'saveGameData', payload: data.data
            });
        },

        *getGameDataCurrent({ payload }, { call, put }) {
            const data = yield call(gameService.getGameDataCurrent, payload);
            if (data.code === 0) {
                if (data.data) {
                    var server_time = Utils.CalibrateServerTime(data.data.ServerTime, data.data.ClientTime);
                    Utils.SaveServerTimeDiff(server_time);
                }

                let current = { ...data.data };

                current['OpenTime'] = new Date(current['OpenTime']);
                current['EndTime'] = new Date(current['EndTime']);
                current['StartTime'] = new Date(current['StartTime']);

                yield put({
                    type: 'saveData', payload: { current }
                });
            }

        },

        *getGameDataPrev({ payload }, { call, put }) {
            const data = yield call(gameService.getGameDataPrev, payload);
            if (data.code === 0)
                yield put({
                    type: 'saveData', payload: { prev: data.data }
                });
        },

        *getGameDataMissing({ payload }, { call, put }) {
            const data = yield call(gameService.getGameDataMissing, payload);
            if (data.code === 0)
                yield put({
                    type: 'saveData', payload: { missing: data.data }
                });
        },

        *getGameTypeInfo({ payload }, { call, put }) {
            const data = yield call(gameService.getGameTypeInfo, payload);
            if (data.code === 0)
                yield put({
                    type: 'saveData', payload: { game_type_info: data.data }
                });
        },
        *getGameDataUnopen({ payload }, { call, put }) {
            const data = yield call(gameService.getGameDataUnOpen, payload);
            if (data.code === 0)
                yield put({
                    type: 'saveData', payload: { unopen: data.data }
                });
            
        },

        *queryGameData({ payload: { group_name, pageIndex, querys }}, { call, put }) {
            const data = yield call(gameService.queryGameData, group_name, pageIndex, querys);
            yield put({
                type: 'saveData', payload: {history: data}
            });
        },

        *queryLatestGameResult({ payload }, { call, put }){
            const data = yield call(gameService.getGameDataAllLatest);
            if (data.code === 0)
            yield put({
                type: 'saveLatestResult', payload:data.data  
            });

        },

        *getFC3DGameDataPrev({ payload }, { call, put }) {
            const data = yield call(gameService.getGameDataPrev, payload);
            if (data.code === 0)
                yield put({
                    type: 'saveFC3DData', payload:data.data  
                });
        },
        
        *getCQSSCGameDataPrev({ payload }, { call, put }) {
            const data = yield call(gameService.getGameDataPrev, payload);
            if (data.code === 0)
                yield put({
                    type: 'saveCQSSCData', payload:data.data 
                });
        },

        *getKL8GameDataPrev({ payload }, { call, put }) {
            const data = yield call(gameService.getGameDataPrev, payload);
            if (data.code === 0)
                yield put({
                    type: 'saveKL8Data', payload:data.data 
                });
        },

        *getKL10GameDataPrev({ payload }, { call, put }) {
            const data = yield call(gameService.getGameDataPrev, payload);
            if (data.code === 0)
                yield put({
                    type: 'saveKL10Data', payload:data.data 
                });
        },

        *getKENOGameDataPrev({ payload }, { call, put }) {
            const data = yield call(gameService.getGameDataPrev, payload);
            if (data.code === 0)
                yield put({
                    type: 'saveKENOData', payload:data.data
                });
        },

        *getPL3GameDataPrev({ payload }, { call, put }) {
            const data = yield call(gameService.getGameDataPrev, payload);
            if (data.code === 0)
                yield put({
                    type: 'savePL3Data', payload:data.data 
                });
        },

        *getFFCGameDataPrev({ payload }, { call, put }) {
            const data = yield call(gameService.getGameDataPrev, payload);
            if (data.code === 0)
                yield put({
                    type: 'saveFFCData', payload:data.data 
                });
        },

        *getZRCGameDataPrev({ payload }, { call, put }) {
            const data = yield call(gameService.getGameDataPrev, payload);
            if (data.code === 0)
                yield put({
                    type: 'saveZRCData', payload:data.data
                });
        },

        *getPK10GameDataPrev({ payload }, { call, put }) {
            const data = yield call(gameService.getGameDataPrev, payload);
            if (data.code === 0)
                yield put({
                    type: 'savePK10Data', payload:data.data
                });
        },
    }
}