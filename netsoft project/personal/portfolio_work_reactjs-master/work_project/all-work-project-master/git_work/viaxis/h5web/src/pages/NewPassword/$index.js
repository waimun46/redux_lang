

import { connect } from 'dva';
import { Flex, List, InputItem, Icon, Toast } from 'antd-mobile';
import * as RenderHelper from "@/utils/render_helper";

import Page from "@/layouts/PageBasic";
import Form from '@/components/Form';

class NewPassword extends Page{

  title="重置登入密码";

    constructor(props) {
        super(props);
        this.state={
          code:0, 
          loading:false
        }
        this.captchaUrl = RenderHelper.getCaptchaImgUrl();

    }

    componentWillMount(){

      let code = this.props.match.params.index;
      this.setState({code:code});
    }

    handleSubmit(e){
      e.preventDefault();

      const { dispatch } = this.props;
  
      this.form.validateFields((errors, values) => {
        if (errors) return;
  
        dispatch({
          type: "user/resetLoginPassword",
          payload: { password: values.password, ticket: this.state.code }
        }).then(resp => {
          this.setState({ loading: false });
  
          if (resp.code === 0) {
            let msg = "重置登入密码成功";
            Toast.success(msg);
           
            this.props.form.resetFields();
          } else {
            Toast.fail(resp.message);
           
          }
  
        });
  
        this.setState({ loading: true });
      })
    }

    onResetClick(e){
      e.preventDefault();
      this.props.form.resetFields();

    }

    refreshCaptcha() {
        let captcha = this.props.form.getFieldInstance("captcha");
        captcha.clearInput();
        captcha.inputRef.focus();
        this.refs.captcha_img.src = RenderHelper.getCaptchaImgUrl();
      }

   
    

    render(){
        const { getFieldDecorator, getFieldProps } = this.props.form;


        return(
            <div className="form_sty form-countdown">
            <div className="top-header"> </div>
            <div className="form_title">
              <h5>填写新密码信息</h5>
            </div>
    
            <Form form={this.props.form} ref={el => this.form = el} onSubmit={this.handleSubmit.bind(this)}>
    
        
    
    
              <InputItem
                {...getFieldProps('password', {
                  rules: [{
                    required: true, message: '请输入新的密码'
                  }, {
                    min: 6, max: 20, message: '密码长度要求为6到20个字符之间。'
                  }],
                })}
                type="password"
                clear
                placeholder="请输入新的密码">
                新的密码
              </InputItem>
              <InputItem
                {...getFieldProps('confirm_pwd', {
                  rules: [{
                    required: true, message: "请输入确认密码"
                  }, {
                    validator: this.checkPassword, message: "两个密码不一致。",
                    getFieldValue: this.props.form.getFieldValue
                  }],
                })}
                type="password"
                clear
                placeholder="请输入确认密码">
                确认密码
              </InputItem>
              <InputItem
                    {...getFieldProps('captcha', {
                        rules: [{ required: true, message: '请输入右侧验证码' }],
                    })}
                    maxLength="4"
                    clear
                    placeholder="验证码"
                    extra={<img src={this.captchaUrl} ref="captcha_img" className="img-captcha" onClick={this.refreshCaptcha.bind(this)} />}
                >
                    <Icon style={{color: 'rgb(255,255,255)'}} type="safety-certificate" />
                </InputItem>
              <div className="game-orders">


                <div className="ButtonBottom">
                  <button className="btn_left" onClick={this.onResetClick.bind(this)}>
                    <Icon type="reload"/> 重置
                  </button>
    
                  <button className="btn_right"  htmlType="submit" >
                    确定
                  </button>
    
                </div>
                <div className="clearfix"></div>
              </div>
            </Form>
          </div>



        )

    }
}

export default connect(state => ({
   
  }))(Form.create()(NewPassword));