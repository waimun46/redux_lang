import React, { Component } from 'react';
import {Icon} from "antd";

import * as RenderHelper from '@/utils/render_helper';


class HistoryList extends Component
{
    constructor(props){
        super(props);
    }

    render(){
        const {historytData} = this.props;

        return(
            <div class="popup_new">
                            <div className="popup-inner ">
                                <h2>提现记录</h2>

                                <div class="content">
                                    <table>
                                        <thead>
                                            <tr className="tr-style">
                                                <th>银行:</th>
                                                <th>户名:</th>
                                                <th>卡号:</th>
                                                <th>金额:</th>
                                                <th>余额:</th>
                                                <th>状态:</th>
                                                <th>备注:</th>
                                                <th>申请时间:</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr className="tr2-style">
                                                <td
                                                style={{
                                                    color: 'blue'
                                                }}
                                                >{historytData.BankName}</td>
                                                <td>{historytData.AccountName}</td>
                                                <td>{historytData.AccountNo}</td>
                                                <td style={{color: 'red'}}><span>￥</span>{historytData.Amount}</td>
                                                <td><span>￥</span>{historytData.Balance}</td>
                                                <td>{(() => {
                                                    switch(historytData.Status) {
                                                        case 0:
                                                            return '准备中';
                                                        case 1:
                                                            return '审核';
                                                        case 2:
                                                            return '审核';
                                                        case 3:
                                                            return '出金';
                                                        case 4:
                                                            return '完成';
                                                        case 5:
                                                            return '拒绝';
                                                        case 6:
                                                            return '取消';    
                                                        default:
                                                            return historytData.Status;
                                                    }
                                                })()}</td>
                                                <td>{historytData.BranchName}</td>
                                                <td>{historytData.CreateTime}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            {/* <a class="close" href="#">
                                <div className="btn-popup close">
                                    <h4>关闭</h4>
                                </div>
                            </a> */}
                        </div>


        )
    }

}

export default HistoryList;
