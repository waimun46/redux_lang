import BasicComponent from "@/components/BasicComponent";
import * as Constants from "@/app_constants";
import * as BetHelper from "@/utils/bet_helper";
import * as Utils from "@/utils/common";
import * as Render from "@/utils/render_helper";
import {Table, Icon, Popconfirm, Timeline} from "antd";
import router from 'umi/router';

import wallet from '../../../assets/images/icon/wallet.png';
import ybag from '../../../assets/images/icon/ybag.png';


class AddBet extends BasicComponent{

    constructor(props){
        super(props);
        this.state = {
            wager: 1,
            betCount: 0,          
         }
    }

    handleInputChange = (e) => {
        e.preventDefault()
        this.setState({
            [e.target.value]: e.target.value
        })
    }

    onWagerChange(e) {
            var val = e.target.value;
            if (/^\d+$/.test(val))
            {
                this.setState({ wager: parseInt(val) });
                this.setState({finalBetAmount : parseInt(val) * this.props.totalBetAmount})

                if(this.props.onWagerUpdate && this.props.onWagerUpdate(parseInt(val)));

            }
                
            else if (val == "")
                this.setState({ wager: 0 });
        
    }
        
    onSubmitClick(e) {
            e.preventDefault();

            if(this.props.onAddBet && this.props.onAddBet());

            if(this.props.close && this.props.close());
    
    
    }

    onCancelClick(e){
        e.preventDefault();

        if(this.props.close && this.props.close());
    }

    render(){

        let {user, scale, kind, betCount, betNum} = this.props;
        const userInfo = user;
        const self = this;

        const getBetNum =  (item) => {
            var betNum = item[0].betNums;
            if (Utils.isString(item[0])) {
                var nums_split_symbols = item[0].gameType.MaxNum > 9 ? Constants.TWO_DIGITS_NUMS_SPLIT_CHARS : Constants.NUMS_SPLIT_CHARS;
                var num_split_symbols = item[0].gameType.MaxNum > 9 ? Constants.NUM_SPLIT_CHAR : null;
                betNum = BetHelper.removeInvalidBetNum(item[0].betNums, item[0].gameType.BetNumCount, nums_split_symbols, num_split_symbols);
            }
            return BetHelper.convertBetNumToString(item[0].GameType, betNum, item[0].unitPos);


        }

        return( 
            <div className="game-orders">
            <div className="">
                    <div className="popup">
                        <div className="order-top">
                            <h2>注单信息</h2>
                            <div className="left-content">
                                <div className="side-1">
                                    <span>玩法类型</span>
                                    <p>{kind }</p>
                                </div>
                                <div className="side-2">
                                    <span>购买号码</span>
                                    <p>{getBetNum(betNum)}</p>
                                </div>

                            </div>
                            <div className="right-content">
                                <div className="side-1">
                                    <span>赔率</span>
                                    <p>{scale}</p>
                                </div>
                                <div className="side-2">
                                    <span>下注注数</span>
                                    <p>{betCount }</p>
                                </div>
                            </div>
                            <div className="clearfix"></div>
                        </div>

                        <div className="order-middle">
                            <div className="input-order">
                                <div className="input-order-warp">
                                    金额：
                                    <input
                                        type="text"
                                        placeholder="请输入金额"
                                        name="bet_price"
                                        value={this.state.wager} onChange={this.onWagerChange.bind(this)}
                                    />
                                    元
                                </div>
                            </div>
                        </div>

                        <div className="order-bottom">
                            <Timeline>
                                <Timeline.Item>
                                    <div className="infor-order">
                                        <p>余额</p>
                                        <p><span style={{color: '#faad14'}}>&#165; </span>{userInfo && Render.money_format(userInfo.Balance)}</p>
                                    </div>
                                    <div className="wallet-icon">
                                        <img src={wallet} alt="wallet"/>
                                    </div>
                                    <div className="clearfix"></div>
                                </Timeline.Item>

                                <Timeline.Item>
                                    <div className="infor-order">
                                        <p>下注总额</p>
                                        { <p style={{color: 'red'}}>{this.state.wager * betCount}</p> }
                                    </div>
                                    <div className="ybag-icon">
                                        <img src={ybag} alt="ybag"/>
                                    </div>
                                </Timeline.Item>
                            </Timeline>
                        </div>

                        <div className="order-button">
                            <a className="close" onClick={self.onCancelClick.bind(this)}>
                                <div className="left-btn ">
                                    取消
                                </div>
                            </a>
                            <a onClick={self.onSubmitClick.bind(this)}>
                                <div className="right-btn" >
                                确定
                                </div>
                            </a>
                           
                            <div className="clearfix"></div>
                        </div>
                        
                    </div>
                </div>


            
            
            </div>



        )



    }
}

export default (AddBet);