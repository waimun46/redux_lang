/**
 * Routes:
 *   - ./src/routes/AuthorizationRoute.js
 */

import ProfitList from './components/ProfitList';

export default function (props) {
    return <ProfitList {...props} />
}