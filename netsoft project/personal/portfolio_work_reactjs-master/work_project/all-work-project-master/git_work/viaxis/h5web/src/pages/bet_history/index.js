import React, {Component} from 'react';
import {connect} from 'dva';
import {Icon} from "antd";
import Page from "@/layouts/PageBasic";
import Form from '@/components/Form';
import {Tabs, Pagination, WhiteSpace, Button, WingBlank} from 'antd-mobile';
import {StickyContainer, Sticky} from 'react-sticky';
import ThisMonthBet from './content/ThisMonthBet';
import SaiXuan from './content/SaiXuan';
import $ from 'jquery';
import * as RenderHelper from '../../utils/render_helper';

function renderTabBar(props) {
    return (
        <Sticky>
            {
                ({style}) => <div
                  style={{
                     ...style,
                     zIndex: 1
                }}><Tabs.DefaultTabBar {...props}/></div>
            }
        </Sticky>
    );
}

const tabs = [
    {title: <span>本月<Icon type="down"/></span>},
     {title: <span>筛选<Icon type="down"/></span>}
];

class BetHistory extends Page {
    title = "历史投注信息";

    constructor(props){
        super(props);
        this.state = {
            query: {}
        }
    }




    render() {
        const showSearch = (e) => {
            e.preventDefault();
            const dialog = RenderHelper.dialog(null,
                <SaiXuan query={this.state.query}   close={ (val)=> {

                    this.setState({query:val});
                    dialog.destroy();



                } } />,{width:360});


        }
        return (
            <div className="bet-history">
                <div className="top-header"></div>

                <div className="top-menu">
                    <div className="this-month">
                        <span>本月</span>
                    </div>
                    <div className="this-month" onClick={(e)=>{showSearch(e);}}>
                        <span>筛选<Icon type="down" style={{marginLeft: '5px', fontSize: '10px'}}/></span>
                    </div>

                    {/* <div class="modal">
                        <input
                            className="hide-input"
                            id="modal"
                            type="checkbox"
                            name="modal"
                            tabindex="1"/>
                        <label className="saixuan" for="modal">
                            <span>筛选<Icon type="down" style={{marginLeft: '5px',  fontSize: '10px'}}/></span>
                        </label>

                        <div class="modal__overlay">
                            <div className="top-header">
                                <h3>筛选</h3>
                            </div>
                            <div class="modal__box">
                                <SaiXuan/>
                                <div className="ButtonBottom">
                                    <div className="btn_left">
                                        <Icon type="reload" style={{marginRight: '3px'}}/>
                                        重置
                                    </div>
                                    <label for="modal">
                                    <div className="btn_right">
                                        确定
                                    </div>
                                    </label>
                                    <div className="clearfix"></div>
                                </div>
                                <div className="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    <div className="clearfix"></div> */}
                    <div className="clearfix"></div>
                </div>

                <ThisMonthBet query={this.state.query} />
                {/* <div  className="pagination-container" >
                    <Pagination total={5}
                        className="custom-pagination-with-icon"
                        current={1}
                        locale={{
                        prevText: (<span className="arrow-align"><Icon type="left" />上一页</span>),
                        nextText: (<span className="arrow-align">下一页<Icon type="right" /></span>),
                        }}
                    />
                </div> */}


            </div>
        );
    }
};

export default connect(state => ({}))(Form.create()(BetHistory));
