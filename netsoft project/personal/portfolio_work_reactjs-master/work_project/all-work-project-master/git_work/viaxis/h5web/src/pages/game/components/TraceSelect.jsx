import { connect } from 'dva';
import PropTypes from 'prop-types';

import React from 'react';

import { Spin, Table, Select, InputNumber, Switch, Tag, Icon } from 'antd';
import BasicComponent from "@/components/BasicComponent";
// import NumberInput from "../../common/NumberInput";

import * as RenderHelper from "@/utils/render_helper";
import * as Utils from "@/utils/common";

class TraceSelect extends BasicComponent {
    constructor(props) {
        super(props);
        this.state = {
            qhCount: 1,
            // 倍数
            multiple: 1,
            // 中奖停止追号
            stopOnWin: true,
            // 选中的追号期号。
            selectedRowKeys: [this.props.current.Qh],
            multiples: {},
            // 这个主要是做缓存使用，避免每次都进行filter计算
            unopen: []
        };
    }

    /* ---------------------------- 系统函数 ------------------------------*/

    componentWillMount() {
        const { current, unopen, group } = this.props;
        if (!unopen || !unopen[group])
            this.props.dispatch(GameAction.getGameDataUnOpen(group));

        let multiples = {};
        multiples[this.props.current.Qh] = 1;
        multiples = Object.assign(this.state.multiples, {}, multiples)
        this.setState({ multiples });
    }

    componentWillReceiveProps(newProps) {
        if (newProps.current != this.props.current && newProps.current) {
            let { selectedRowKeys } = this.state;
            const qh = newProps.current.Qh;
            if (selectedRowKeys.includes(qh)) {
                let index = selectedRowKeys.indexOf(qh);
                selectedRowKeys = selectedRowKeys.slice(index + 1);
            } else {
                selectedRowKeys.push(qh);
                selectedRowKeys = selectedRowKeys.filter(it => it <= qh).sort();
            }

            this.setState({ selectedRowKeys });
        }
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevState != this.state) {
            this.change();
        }
    }


    /* ---------------------------- 自定义函数 ------------------------------*/

    change() {
        const { selectedRowKeys, multiples, multiple, stopOnWin } = this.state;

        const traceData = selectedRowKeys.map(qh => {
            return {
                qh,
                multiple: multiples[qh] || multiple
            };
        });

        this.props.onChange && this.props.onChange(traceData, stopOnWin);

    }

    /* ---------------------------- Events ------------------------------*/

    onQhCountChange(value) {
        if (value != this.state.qhCount) {
            const selectedRowKeys = this.source.slice(0, value).map(it => it.Qh);
            this.setState({ qhCount: value, selectedRowKeys });
        }
    }

    onMultipleChange(multiple) {
        if (multiple != this.state.multiple)
            this.setState({ multiple });
    }

    onSwithChange(checked) {
        this.setState({ stopOnWin: checked });
    }

    get rowSelection() {
        const self = this;
        const { current } = this.props;
        const rowSelection = {
            selectedRowKeys: this.state.selectedRowKeys,
            onChange: (selectedRowKeys, selectedRows) => {
                self.setState({ selectedRowKeys });
            },
            onSelectAll: (selected, selectedRows, changeRows) => {
                self.setState({ selectedRowKeys: selectedRows.map(it => it.Qh) });
            },
            getCheckboxProps: record => ({ disabled: record.Qh == current.Qh })
        };
        return rowSelection;
    }

    /* ---------------------------- Render ------------------------------*/

    render() {

        const { unopen } = this.props;
        const isFetching = (unopen && unopen.isFetching) || false;

        const data = this.source;

        return (
            <div className="game-trace">
                <Spin spinning={isFetching}>
                    <div className="game-trace-types fn-clear">
                        <label>追号</label>
                        <ul>
                            <li className="active">同倍</li>
                            <li>翻倍</li>
                        </ul>
                    </div>
                    <div className="game-trace-config fn-clear">
                        追
                        <NumberInput className="white qhCount" min={1} max={data.length} step={1} value={this.state.qhCount} onChange={this.onQhCountChange.bind(this)} />
                        期
                        (包含当前期最多追<b>{data.length}</b>期)
                        <NumberInput className="white multiple" min={1} max={1000} value={this.state.multiple} onChange={this.onMultipleChange.bind(this)} />
                        倍

                        <label>
                            中奖后停止追号
                            <Switch defaultChecked={this.state.stopOnWin} onChange={this.onSwithChange.bind(this)} checkedChildren={<Icon type="check" />} unCheckedChildren={<Icon type="cross" />} />
                        </label>
                    </div>
                    <div className="game-trace-data">
                        <Table rowKey="Qh" rowSelection={this.rowSelection} columns={this.columns} dataSource={data} pagination={false} scroll={{ y: 335 }} />
                    </div>
                </Spin>
            </div>
        );
    }

    get source() {
        const { current, unopen, group } = this.props;
        return unopen[group] ? unopen[group].filter(it => it.Qh >= current.Qh) : [];
    }

    get columns() {
        const self = this;
        const { amount } = this.props;
        const columns = [
            {
                title: '期号', width: 150, dataIndex: 'Qh', render: (qh, record, index) => {
                    if (index != 0)
                        return qh;
                    return (
                        <span>
                            {qh}<label className="tag">当前期</label>
                        </span>
                    );

                }
            },
            {
                title: "倍数", width: 150, className: "fn-text-center", render(record, index) {
                    const disabled = self.props.current.Qh == record.Qh;
                    const multiple = self.state.multiples[record.Qh] || self.state.multiple;
                    const onChange = (value) => {
                        var multiples = {};
                        multiples[record.Qh] = value;
                        multiples = Object.assign(self.state.multiples, {}, multiples);
                        self.setState({ multiples });
                    };
                    return <NumberInput className="white" disable={disabled} value={multiple} min={1} onChange={onChange} />
                }
            },
            {
                title: "金额", width: 150, render(record) {
                    const multiple = self.state.multiples[record.Qh] || self.state.multiple;
                    return Utils.milliFormat(amount * multiple);
                }
            },
            { title: "开奖时间", dataIndex: "OpenTime", render: RenderHelper.render_time }
        ];
        return columns;
    }

}
TraceSelect.propTypes = {
    // 游戏组
    group: PropTypes.string.isRequired,
    // 投注总金额
    amount: PropTypes.number.isRequired,
    /**
     * 追号内容改变事件
     * @param {dict} traceData 追号数据，key=qh,value=multiple
     * @param {bool} stopOnWin 中奖停止追号
     * @type {[type]}
     */
    onChange: PropTypes.func
}


export default connect(state => ({
    current: state.game.gamedata.current,
    unopen: state.game.gamedata.unopen,
    user: state.passport.userinfo
}))(TraceSelect);
