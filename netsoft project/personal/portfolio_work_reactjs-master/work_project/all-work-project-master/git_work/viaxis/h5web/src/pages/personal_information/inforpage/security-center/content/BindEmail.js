/**
 * Routes:
 *   - ./src/routes/AuthorizationRoute.js
 */

import { connect } from 'dva';
import { message, Row, Col, Alert, Tabs, Button, Input,Icon,Card } from "antd";
import { List, InputItem, WhiteSpace, Toast } from 'antd-mobile';
import router from 'umi/router';

import Page from "@/layouts/PageBasic";
import Form from '@/components/Form';
import CountDownButton from "@/components/CountDownButton";
import * as RenderHelper from "@/utils/render_helper";


const Item = List.Item;
const Brief = Item.Brief;

class BindEmail extends Page {
  title = "验证邮箱";
  constructor(props) {
    super(props);
    this.state = {
      // 提交按钮
      loading: false,
      // 发送验证码按钮
      captchaSending: false,
      // 发送按钮倒计时
      target: 0,
    }
    // 初始验证码地址,否则每次表单输内容验证码都会变化
    this.captchaUrl = RenderHelper.getCaptchaImgUrl();
  }



  /* ---------------------------- Events ------------------------------*/

  handleResetSubmit(e) {
    e.preventDefault();

    this.props.form.resetFields();

 
  }

  sendCaptchaClick(e) {
    const { dispatch } = this.props;
    dispatch({ type: "user/sendTicket", payload: true }).then(resp => {
      this.setState({ captchaSending: false });
      let seconds = 0;
      if (resp.code === 0) {
        seconds = 60;
        Toast.success("验证码已发送到您的邮箱");
       // message.success("验证码已发送到您的邮箱。");
      } else {
        seconds = resp.data;
        Toast.fail(resp.message);
        //message.error(resp.message);
      }
      if (seconds > 0)
        this.setState({ target: new Date().getTime() + seconds * 1000 });

    });
    this.setState({ captchaSending: true });
  }

  confirmBinding(e){
    e.preventDefault();
    const { dispatch } = this.props;
    this.form.validateFields((errors, values) => {
      if (errors) return;

      this.setState({ loading: true });

      dispatch({
        type: "user/bindEmail",
        payload: { verificationCode: values.captcha }
      }).then(resp => {
        this.setState({ loading: false });

        if (resp.code === 0) {
          let msg = "邮箱认证成功";
          Toast.success(msg);
          //message.success(msg);
          this.props.form.resetFields();
          router.goBack();
        } else {
          let msg=resp.message;
          Toast.fail(msg);
          //message.error(msg);
        }

      });

     
    })
  }



  /* ---------------------------- Renders ------------------------------*/

  render() {
    const { getFieldProps } = this.props.form;

    return (

      <div className="form_sty form-countdown">
        <div className="top-header"> </div>
        <div className="form_title ">
          <h5>选项列表</h5>
        </div>

        <Form form={this.props.form} ref={el => this.form = el} onSubmit={this.handleResetSubmit.bind(this)}>

          {/* <InputItem
            {...getFieldProps('current_pwd', {
              rules: [{
                required: true, message: '请输入邮箱地址',
              }],
            })}
            type="password"
            clear
            placeholder="请输入邮箱地址">
            邮箱地址
          </InputItem> */}



          <InputItem
            {...getFieldProps('captcha', {
              rules: [{ required: true, message: '请输入您接收到验证码' }],
            })}
            maxLength="6"
            clear
            placeholder="邮箱验证码"
            extra={<CountDownButton loading={this.state.captchaSending}
                                    size="small" type="primary"
                                    target={this.state.target}
                                    onClick={this.sendCaptchaClick.bind(this)}>
              发送验证码
            </CountDownButton>}
          >
            验证码
          </InputItem>


          <div className="game-orders">
            <div className="ButtonBottom">
              <button className="btn_left">
                <Icon type="reload"/> 重置
              </button>

              <button className="btn_right" loading={this.state.loading} htmlType="submit" onClick = {this.confirmBinding.bind(this)}>
                确定
              </button>

            </div>
            <div className="clearfix"></div>
          </div>
        </Form>
      </div>
    );
  }
};

export default connect(state => ({
  loading: state.loading.models.user,
}))(Form.create()(BindEmail));
