import {Collapse } from 'antd';

import BasicComponent from "@/components/BasicComponent";



const Panel = Collapse.Panel;

function callback(key) {
    console.log(key);
}

class BetTypeSelection extends BasicComponent{

    constructor(props){
        super(props);
        this.state={
            selectedCategory: null,
            // 当前选中的玩法种类
            selectedSort: null,
            // 当前选中的玩法
            selectedType: null,
        }
      //  this.onCategoryClick.bind(this);
        
    }

    componentWillMount(){

        const{selectedCategory,selectedSort, selectedType} = this.props;

        this.setState({selectedCategory:selectedCategory});
        this.setState({selectedSort:selectedSort});
        this.setState({selectedType:selectedType});

    }

    updateSelectType(selectedCategory) {
        if (!selectedCategory) return;
        this.setState({selectedCategory});
        let selectedSort = this.sort(selectedCategory.Sorts)[0];
        this.setState({selectedSort});
        
        let selectedType = this.sort(this.sort(selectedSort.Kinds)[0].Types)[0];
        // if(selectedType.Name==='单式')
        //         selectedType = this.sort(this.sort(selectedSort.Kinds)[0].Types)[1];
        this.setState({selectedType});
       // this.props.onChange && this.props.onChange(selectedType);

    }

    onCategoryClick(category){
        if (category == this.state.selectedCategory) return;
        this.updateSelectType(category);

    }

    onSortClick(sort){

        if (sort == this.state.selectedSort) return;
        this.setState({
            selectedSort: sort
        });

        


        let selectedType = this.sort(this.sort(sort.Kinds)[0].Types)[0];
        this.setState({selectedType});
        //let value={selectedType:selectedType, selectedSort: sort, selectedCategory:this.state.selectedCategory };
        let filteredSort = sort.Kinds[0].Types.filter(item=>{
            return item.Name !='单式';
        })

        if(filteredSort.length>1)
            this.props.onSortClick && this.props.onSortClick(selectedType);
        else
        {
            let value={selectedType:selectedType, selectedSort: sort, selectedCategory:this.state.selectedCategory };
            this.props.close && this.props.close(value);

        }
            


    }

    onTypeClick(type){

        if (type == this.state.selectedType) return;
        this.setState({
            selectedType: type
        });

        let value={selectedType:type, selectedSort: this.state.sort, selectedCategory:this.state.selectedCategory };
        this.props.close && this.props.close(value);
        //this.props.onChange && this.props.onChange(type);

    }

    sort(list) {
        list.sort((a, b) => b.Weight - a.Weight);
        return list;
    }

    render(){

        let otherCategorys = this.props.otherCategorys;
        let selectedCategory = this.props.selectedCategory;
        let selectedSort = this.props.selectedSort;
        let selectedType = this.props.selectedType;

        return(
            // <div className="modal__overlay">
            <div className="modal__box">
           

            <div>
             
                <div className="sub-title">
                    <label>游戏类型</label> 
                </div>

                <div className="game-bettype-group fn-clea">
                    {otherCategorys.map(category =>
                        <a key={category.Name}
                               onClick={() => this.onCategoryClick(category)}>
                            {category.Name}</a>)
                    }
                    <ul>

                        {this.state.selectedCategory.Sorts.map(sort =>
                            <div className="bettype-warp">
                                <li key={sort.Name}
                                    className={sort == this.state.selectedSort ? "active" : ""}
                                    onClick={() => this.onSortClick(sort)}>{sort.Name}</li>
                            </div>
                        )
                        }

                        <div className="clearfix"></div>
                    </ul>

                </div>

                <div className="game-bettype-types">
                    <div className="sub-title">
                        <label>玩法类型</label>
                    </div>

                    {this.state.selectedSort.Kinds.map(kind =>

                        <div className="fn-clear tab" key={kind.Name}>
                            <Collapse defaultActiveKey={['1']} onChange={callback}>
                                <Panel header={kind.Name} key="i">
                                    {this.sort(kind.Types)
                                         .filter(type=>
                                            {
                                                return type.Name!="单式"; 
                                            })
                                            .filter(type=>
                                                {
                                                    return (type.Name!="直选");
                                                })
                                          .map(type =>
                                        <label htmlFor="modal" className="modal_close">
                                            <p

                                                key={type.Name}
                                                className={type == this.state.selectedType ? "active" : ""}
                                                onClick={() => this.onTypeClick(type)}>
                                                <label style={ type.Name == this.state.selectedType.Name ? {color:'red'} : {}}>
                                                    {type.Name}
                                                    
                                                </label>
                                            </p>
                                        </label>
                                    )
                                    }

                                </Panel>

                            </Collapse>
                         
                        </div>
                    )}
                </div>
            
            
                {/* <a class="close" href="#" onClick={e=>{callback}}>
        <div className="btn-popup close">
            <h4>关闭</h4>
        </div>
    </a> */}
            </div>
        </div>


        )
    }
}

export default BetTypeSelection;