import React, { Component } from 'react';
import { connect } from 'dva';
import Page from "@/layouts/PageBasic";
import Form from '@/components/Form';
import { List,Pagination, Icon } from 'antd-mobile';

import * as BetHelper from "@/utils/bet_helper";
import TeamRecord from '@/components/teamRecord';
import * as RenderHelper from "@/utils/render_helper";
import DateRange from '@/components/DateRange';
import kl10logo from "../../../assets/images/game-icon/xylc.png";

import Link from 'umi/link';

const Item = List.Item;
const Brief = Item.Brief;



class Kl10 extends Page {
  title = "快乐十分"

  constructor(props) {
    super(props);

    // const query = new URLSearchParams(this.props.location.search);
    // this.qr = query.get('qr')

    this.state = {
      queryDate:[],
      loading: false,
      query: {},
      pageIndex: 15 
    }

  }



  /* ---------------------------- System  ------------------------------*/
  componentDidMount() {
    this.queryGameData();
  }

  componentWillUpdate(nextProps, nextState) {
    if(nextState.queryDate != this.state.queryDate)
    {
      let values = nextState.queryDate;
      this.setState({queryDate:values});
      this.props.dispatch({ type: "gamedata/queryGameData",payload: { group_name: 'KL10', querys: { OpenTime: nextState.queryDate} } })
      console.log(nextState.queryDate)
    }
    if(nextState.pageIndex != this.state.pageIndex)
    {this.props.dispatch({ type: "gamedata/queryGameData",payload: { group_name: 'KL10', querys: {PageIndex: nextState.pageIndex} } })}
    

  }

  
  /* ---------------------------- Functions  ------------------------------*/

  queryGameData() {
    this.props.dispatch({ type: "gamedata/queryGameData",payload: { group_name: 'KL10', querys: {PageIndex: this.state.pageIndex} }})
  }
  
  splitNum(num) {
    let num1 = num.split(",");
    let num2 = [];
    for(let i = 0; i < num1.length; i++) {
      num2.push(num1);
      return num2;
    }
  }

  onDateRangeChange = (value) => {
    console.log(value);
    let date = [];
    date.push(value[0].toLocaleString());
    date.push(value[1].toLocaleString());
    this.setState({queryDate:date});
}

onClickLeft = () => {
  console.log("left")
  let newPage = this.state.pageIndex - 1;
  (newPage >= 15)
  ? 
  this.setState({pageIndex:newPage})
  :
  this.setState({pageIndex:15})
}

onClickRight = () => {
  console.log("right")
  let newPage = this.state.pageIndex + 1;
  this.setState({pageIndex:newPage});

}

 /* ---------------------------- Events ------------------------------*/

  render() {
    const data = this.props.data || [];
    
    return (
      <div className="danielph2">
        <div className="fund-history ">
        <div className="top-header"> </div>

        {/* date */}
        <List className="date-select">
          <Item><DateRange onChange = {this.onDateRangeChange.bind(this)} /></Item>
        </List>

        <div className="form_title">
          <h5>开奖数据</h5>
        </div>
        <List className="my-list ">
        {data && data.map((element,i) =>
        (element.Result) ? 
            <Item
              key={i}
              extra={
                <div className="bet-money add">
                  <span>第{element.Qh}期</span>
                  <span>{element.OpenTime}</span>
                </div>
              }
              align="top"
              thumb={<img className="logosizee" src={kl10logo}/>}
              multipleLine
            >
              <span className="title-warp" style={{ color:'black' ,fontSize:'20px'}}>快乐十分</span>
              {this.splitNum(element.Result).map((ele) => 
              <Brief style={{fontSize: '12px', color: '#cccccc'}}><div className="danielph" style={{display:"flex"}}>{ele.map((e) => <div className="dCircleNum" >{e}</div>)}</div></Brief>
              )}
            </Item>
            : false                
        )}  
        </List>
        <div  className="pagination-container" >
          <Pagination total={5}
            className="custom-pagination-with-icon"
            current={1}
            locale={{
              prevText: (<span onClick = {this.onClickLeft.bind(this)} className="arrow-align"><Icon type="left" />上一页</span>),
              nextText: (<span onClick = {this.onClickRight.bind(this)} className="arrow-align">下一页<Icon type="right"  /></span>),
            }}
          />
        </div>
      </div>
      </div>
    );
  }
};

function mapStateToProps(state) {
    return {
      data: state.gamedata.data.Data,
    }
  }
  
  export default connect(mapStateToProps)(Kl10);
