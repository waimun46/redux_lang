import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router'
import { message, Row, Col, Checkbox, Button, Form, Input, Icon } from 'antd';

import BasicComponent from "../common/BasicComponent";
import Layout from './Layout';

import * as PassportAction from "../../actions/passport";
import * as Utils from "../../utils/common";
import * as RenderHelper from "../../utils/render_helper";


class ForgotPassword extends BasicComponent {

    constructor(props) {
        super(props);
        this.captchaUrl = RenderHelper.getCaptchaImgUrl();
        this.state = {
            loading: false
        };
    }

    /* -------------------------- Sys ----------------------------- */

    /* -------------------------- Function ----------------------------- */

    /* -------------------------- Events ----------------------------- */

    handleSubmit(e){
        const { dispatch } = this.props;
        e.preventDefault();
        this.props.form.validateFields((errors, values) => {
            if (errors) return;

            this.setState({loading:true});
            this.props.dispatch(PassportAction.forgetPassword(values.email, values.captcha)).then(act=>{
                this.setState({loading:false});
                this.assert(act, {
                    fnOk: () => message.success("发送成功")
                });
                this.refreshCaptcha();
            });
        });
    }

    handleReset(e) {
        e.preventDefault();
        this.props.form.resetFields();
    }

    refreshCaptcha(){
        let captcha = this.props.form.getFieldInstance("captcha");
        captcha.refs.input.value = "";
        captcha.refs.input.focus();
        this.refs.captcha_img.src = RenderHelper.getCaptchaImgUrl();
    }

    /* -------------------------- Renders ----------------------------- */

    render(){
        const { getFieldDecorator } = this.props.form;
        return (
            <Layout>
                <div className="passport-box" style={{width:400}}>
                    <div className="passport-box-header fn-clear">
                        <h2 className="fn-left">找回密码</h2>
                        <span className="fn-right">
                            还没有账户? <Link to="/register">立即注册</Link>
                        </span>
                    </div>
                    <div className="passport-box-body">
                        <Form className="passport-box-body-form" horizontal onKeyPress={(e)=>{if(e.key=="Enter")this.handleSubmit(e);}}>
                            <div className="passport-box-body-content">
                                <br/>
                                <Form.Item>
                                    {getFieldDecorator('email', {
                                        rules: [{ 
                                            required: true, message: '请输入邮箱'
                                        }],
                                    })(
                                        <Input type="text" className="input-user" autoComplete="off" placeholder="邮箱" />
                                    )}
                                </Form.Item>
                                <Form.Item>
                                    <Row gutter={8} className="underline">
                                        <Col span={12}>
                                            {getFieldDecorator('captcha', {
                                                rules: [{ required: true, message: "请输入验证码" }],
                                            })(
                                                <Input type="text" className="input-captcha" autoComplete="off" maxLength="4" placeholder="验证码" />
                                            )}
                                        </Col>
                                        <Col span={12}>
                                            <img src={this.captchaUrl} ref="captcha_img" className="img-captcha" onClick={this.refreshCaptcha.bind(this)} />
                                        </Col>
                                    </Row>
                                </Form.Item>
                                <br/>
                                <Form.Item>
                                    <Button className="passport-submit" loading={this.state.loading} onClick={this.handleSubmit.bind(this)}>找回密码</Button>
                                    <br/><br/>
                                    <Link className="fn-right" to="/login">立即登录?</Link>
                                </Form.Item>
                            </div>
                        </Form>
                    </div>
                </div>
            </Layout>
        );
    }
}

module.exports = connect(state => ({
    data: state.passport
}))(Form.create()(ForgotPassword));
