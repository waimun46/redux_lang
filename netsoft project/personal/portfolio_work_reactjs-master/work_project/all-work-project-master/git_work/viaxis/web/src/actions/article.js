import $ from "jquery";
import { ActionState } from "../app_constants";
import { POST, GET } from "../utils/common";

/**
 * 通过目录标签与文章地址获取文章
 * @param  {[type]} categoryLabel  [description]
 * @param  {[type]} articleUrlName [description]
 * @return {[type]}                [description]
 */
export function getArticle(categoryLabel, articleUrlName) {
    return {
        name: "Article",
        callAPI: () => GET(`/api/article/${categoryLabel}/${articleUrlName}`)()
    };
}

export function getArticleByCategory(categoryLabel){
    return {
        name: "CategoryArticle",
        callAPI: () => GET(`/api/article/${categoryLabel}`)()
    };
}
