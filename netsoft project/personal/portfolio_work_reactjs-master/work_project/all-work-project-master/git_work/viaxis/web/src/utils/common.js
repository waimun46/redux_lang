import fetch from 'isomorphic-fetch'
import Config from "../config";
import Auth from "../auth";

/*------------ Action -----------*/
export function POST(url, body, headers = {}) {
    return () => fetch(Config.API + url, {
        credentials: 'include',
        method: "POST",
        body: body,
        headers: Object.assign({}, {
            "Content-Type": "application/json",
            "Authorization": Auth.getAuthToken()
        }, headers)
    });
}

export function GET(url, headers = {}) {
    return () => fetch(Config.API + url, {
        credentials: 'include',
        method: "GET",
        headers: Object.assign({}, {
            "Authorization": Auth.getAuthToken()
        }, headers)
    });
}



/**
 * 检查一个字典是否为空
 * @param  {[type]}  obj [description]
 * @return {Boolean}     [description]
 */
export function isEmptyObject(obj) {
    if (!obj) return true;
    for (var n in obj) {
        return false
    }
    return true;
}

/**
 * 检查一个实例是否字典
 * @param  {[type]}  v [description]
 * @return {Boolean}   [description]
 */
export function isDict(v) {
    return typeof v === 'object' && v !== null && !(v instanceof Array) && !(v instanceof Date);
}

/* ****************************************************************************************************************************** *
 * ******************************************************* Format Helper ******************************************************** *
 * ****************************************************************************************************************************** */

/**
 * 千位符号
 * @param  {Function} () [description]
 * @return {[type]}      [description]
 */
export const milliFormat = (() => {
    const DIGIT_PATTERN = /^-?\d+(?=\.?\d*($|\s))/g
    const MILLI_PATTERN = /(?=(?!\b)(\d{3})+$)/g

    return (input, digits = 2) => input && input.toFixed(digits)
        .replace(DIGIT_PATTERN, (m) => m.replace(MILLI_PATTERN, ','))
})()


/**
 * 将日期区间转换为字符串模式
 * @param  {[Moment]}  dateRange 时间区间
 * @param  {Boolean} accurate  是否精准转换(不做任何其他干涉)
 * @param  {String}  format    时间格式
 * @return {[type]}            [description]
 */
export function dateRangeFormat(dateRange, accurate = false, format = "YYYY-MM-DD HH:mm:ss") {
    if (!Array.isArray(dateRange) || dateRange.length != 2)
        return null;

    // 如果不是精准模式，则将日期设置为开始和结束时间。
    if (!accurate) {
        dateRange[0].set('hour', 0).set('minute', 0).set('second', 0);
        dateRange[1].set('hour', 23).set('minute', 59).set('second', 59);
    }

    var data = [];
    data[0] = dateRange[0].format(format);
    data[1] = dateRange[1].format(format);
    return data;
}



/* ****************************************************************************************************************************** */
/* ****************************************************** Time Helper *********************************************************** */
/* ****************************************************************************************************************************** */

/**
 * 校验服务器时间。
 * 此方法用于获取服务器时间，并在此时间上补上客户端与服务端之间的延迟。
 * @param {float} server_unit_timestamp      [description]
 * @param {float} client_send_unix_timestamp [description]
 */
export function CalibrateServerTime(server_unit_timestamp, client_send_unix_timestamp) {
    var now = new Date();
    var send_time = new Date(client_send_unix_timestamp * 1000);
    var server_time = new Date(server_unit_timestamp * 1000);
    server_time.setMilliseconds(server_time.getMilliseconds() + (now - send_time) / 2);
    return server_time;
}

/**
 * [SaveServerTimeDiff description]
 * @param {[type]} server_time [description]
 */
export function SaveServerTimeDiff(server_time) {
    var store = sessionStorage || window;

    var diffs = [];
    if (store.ServerClientTimeDiff)
        diffs = store.ServerClientTimeDiff.split(",");

    var diff = server_time.getTime() - new Date().getTime();

    diffs = [diff, ...diffs];
    // 保存十个已经足够
    store.ServerClientTimeDiff = diffs.slice(0, 10);
}

/**
 * 获取服务器时间戳
 */
export function GetServerTimeStamp() {
    var store = sessionStorage || window;
    var diffs = [];
    if (store.ServerClientTimeDiff)
        diffs = store.ServerClientTimeDiff.split(",").map(it => parseInt(it));

    if (!diffs || diffs.length == 0) return null;

    var ave = parseInt(diffs.reduce((a, b) => a + b) / diffs.length);

    return parseInt((new Date().getTime() + ave) / 1000);
}

/**
 * 将日期转换成unix timestamp 时间戳
 * @param  {[type]} time [description]
 * @return {[type]}      [description]
 */
export function unixtimestampFromDate(time) {
    return (time || new Date()).getTime() / 1000;
}

/**
 * 将unix timestamp 时间戳转换为 Date对象
 * @param  {[type]} timestamp [description]
 * @return {[type]}           [description]
 */
export function unixtimestampToDate(timestamp) {
    return new Date(timestamp * 1000);
}

export function dateFormat(date, fmt = "yyyy-MM-dd HH:mm:ss") {
    var o = {
        "M+": date.getMonth() + 1, //月份         
        "d+": date.getDate(), //日         
        "h+": date.getHours() % 12 == 0 ? 12 : date.getHours() % 12, //小时         
        "H+": date.getHours(), //小时         
        "m+": date.getMinutes(), //分         
        "s+": date.getSeconds(), //秒         
        "q+": Math.floor((date.getMonth() + 3) / 3), //季度         
        "S": date.getMilliseconds() //毫秒         
    };
    var week = {
        "0": "/u65e5",
        "1": "/u4e00",
        "2": "/u4e8c",
        "3": "/u4e09",
        "4": "/u56db",
        "5": "/u4e94",
        "6": "/u516d"
    };
    if (/(y+)/.test(fmt)) {
        fmt = fmt.replace(RegExp.$1, (date.getFullYear() + "").substr(4 - RegExp.$1.length));
    }
    if (/(E+)/.test(fmt)) {
        fmt = fmt.replace(RegExp.$1, ((RegExp.$1.length > 1) ? (RegExp.$1.length > 2 ? "/u661f/u671f" : "/u5468") : "") + week[date.getDay() + ""]);
    }
    for (var k in o) {
        if (new RegExp("(" + k + ")").test(fmt)) {
            fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
        }
    }
    return fmt;
}


/* ****************************************************************************************************************************** *
 * ***************************************************** Numerical Helper ********************************************************** *
 * ****************************************************************************************************************************** */

/**
 * 数值百分比
 * @param  {[type]} val    [description]
 * @param  {Number} digits [description]
 * @return {[type]}        [description]
 */
export function percent(val, digits = 2) {
    return parseFloat((val * 100).toFixed(digits));
}

/**
 * 获取数值的最小精度
 * @param  {[type]} val    [description]
 * @return {[type]}        [description]
 */
export function getPrecision(val) {
    var index = val.indexOf('.');
    var length = val.length - (index + 1);
    if (index > -1)
        return 1 / Math.pow(10, length);
    else
        return 1;
}


/* ****************************************************************************************************************************** *
 * ***************************************************** String Helper ********************************************************** *
 * ****************************************************************************************************************************** */


export function isString(o) {
    return typeof o === "string";
}

export function isEmptyOrNull(s) {
    return !isString(s) || s == null || s == undefined || s == "";
}

export function isNonEmptyString(s) {
    return isString(s) && s !== "";
}

/**
 * String.format
 * @param  {[type]}    str  [description]
 * @param  {...[type]} args [description]
 * @return {[type]}         [description]
 */
export function format(str, ...args) {
    return str.replace(/{(\d+)}/g, function (match, number) {
        return typeof args[number] != 'undefined'
            ? args[number]
            : match
            ;
    });
}

export function maskStart(str, showLength) {
    if (str.length <= showLength) return str;

    return pad("", str.length - showLength, "*") + str.slice(str.length - showLength);
}

export function maskEnd(str, showLength) {
    if (str.length <= showLength) return str;

    return str.slice(0, showLength) + pad("", str.length - showLength, "*");
}


/**
 * 字符补全
 * @param  {[type]} source    原字符串或数字
 * @param  {[type]} width     最终要求字符串长度
 * @param  {String} padSymbol 填充内容
 * @return {[type]}           [description]
 */
export function pad(source, width, padSymbol = '0') {
    source = source + '';
    return source.length >= width ? source : new Array(width - source.length + 1).join(padSymbol) + source;
}

/**
 * 字符串分割
 * @param  {[type]} o       [description]
 * @param  {[Array|String]} symbols 分割的符号
 * @return {[type]}         [description]
 */
export function split(o, symbols) {
    if (Array.isArray(symbols)) {
        var tmp_symbol = "πø˙";
        symbols.forEach(function (symbol) {
            if (symbol == tmp_symbol) return;
            var re = RegExp(symbol, "g");
            o = o.replace(re, tmp_symbol);
        });
        return o.split(tmp_symbol);
    } else
        return o.split(symbols);
}

/**
 * 获取数组中第一个
 * @param  {Array|string} array         [description]
 * @param  {[type]} default_value [description]
 * @return {[type]}               [description]
 */
export function getFirst(array, default_value) {
    if (array && isString(array))
        return array;
    else if (Array.isArray(array) && array.length > 0)
        return array[0];
    else
        return default_value;
}

/**
 * 计算字符串所占的内存字节数，默认使用UTF-8的编码方式计算，也可制定为UTF-16
 * UTF-8 是一种可变长度的 Unicode 编码格式，使用一至四个字节为每个字符编码
 * 
 * 000000 - 00007F(128个代码)      0zzzzzzz(00-7F)                             一个字节
 * 000080 - 0007FF(1920个代码)     110yyyyy(C0-DF) 10zzzzzz(80-BF)             两个字节
 * 000800 - 00D7FF 
   00E000 - 00FFFF(61440个代码)    1110xxxx(E0-EF) 10yyyyyy 10zzzzzz           三个字节
 * 010000 - 10FFFF(1048576个代码)  11110www(F0-F7) 10xxxxxx 10yyyyyy 10zzzzzz  四个字节
 * 
 * 注: Unicode在范围 D800-DFFF 中不存在任何字符
 * {@link http://zh.wikipedia.org/wiki/UTF-8}
 * 
 * UTF-16 大部分使用两个字节编码，编码超出 65535 的使用四个字节
 * 000000 - 00FFFF  两个字节
 * 010000 - 10FFFF  四个字节
 * {@link http://zh.wikipedia.org/wiki/UTF-16}
 * @param  {[type]} charset charset utf-8, utf-16
 * @return {Number}
 */
export function sizeof(str, charset) {
    var total = 0,
        charCode,
        i,
        len;
    charset = charset ? charset.toLowerCase() : '';
    if (charset === 'utf-16' || charset === 'utf16') {
        for (i = 0, len = str.length; i < len; i++) {
            charCode = str.charCodeAt(i);
            if (charCode <= 0xffff) {
                total += 2;
            } else {
                total += 4;
            }
        }
    } else {
        for (i = 0, len = str.length; i < len; i++) {
            charCode = str.charCodeAt(i);
            if (charCode <= 0x007f) {
                total += 1;
            } else if (charCode <= 0x07ff) {
                total += 2;
            } else if (charCode <= 0xffff) {
                total += 3;
            } else {
                total += 4;
            }
        }
    }
    return total;
}

/* ****************************************************************************************************************************** *
 * ******************************************************** Url Helper ********************************************************** *
 * ****************************************************************************************************************************** */


/**
 * 从当前地址中获取指定名称的 queryString
 * @param  {[type]} name [description]
 * @return {[type]}      [description]
 */
export function getUrlParameter(name) {
    name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
    var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
    var results = regex.exec(location.search);
    return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
}

/**
 * 获取传入地址或当前地址的QueryString
 * @param  {[type]} url [description]
 * @return {dict}     [description]
 */
export function getQueryString(url) {
    if (url === undefined || url == null)
        url = location.search;
    else {
        let urlPaths = url.split("?");
        if (urlPaths.length < 2)
            return {};
        url = urlPaths[1];
    }
    var querys = location.search.replace("?", "");
    var data = querys.split("&");

    var results = {};
    for (let i = 0; i < data.length; i++) {
        var param = data[i].split("=");
        results[param[0]] = param[1];
    }
    return results;
}

//增加回发参数
export function addActionToUrl(formUrl, action /*参数名*/, value /*参数值*/, clear /*是否清空参数*/) {
    //如果Url中含有参数
    if (formUrl.indexOf("?") >= 0) {
        //清空参数
        if (clear) formUrl = formUrl.substr(0, formUrl.indexOf("?") + 1);
        //Url中含有要添加的参数对
        if (formUrl.indexOf(action) > 0) {
            //获取所有参数对值
            var queryString = formUrl.substr(formUrl.indexOf("?") + 1, formUrl.length);
            //分组
            var queryStringArr = queryString.split("&");
            //不包含参数的Url
            formUrl = formUrl.substr(0, formUrl.indexOf("?") + 1);
            var isFirst = true;
            //遍历参数对
            for (var i = 0; i < queryStringArr.length; i++) {
                //如果参数对不等于要添加的Action.则添加到Url中
                if (queryStringArr[i].indexOf(action) < 0) {
                    if (isFirst) isFirst = false;
                    else formUrl += "&";
                    formUrl += queryStringArr[i];
                }
            }
        }
        //避免只有一个参数,并且参数相同时出现的?&情况
        if (formUrl.indexOf("?") + 1 != formUrl.length) formUrl += "&";
    } else {
        formUrl += "?";
    }

    formUrl += action + "=" + value;
    return formUrl;
}


/* ****************************************************************************************************************************** *
 * ****************************************************** Array Helper ********************************************************** *
 * ****************************************************************************************************************************** */

export function range(start, end) {
    return Array.from({ length: (end - start) }, (v, k) => k + start);
}

/**
 * 将一个时间对象数组转换成字符串格式数组
 * @param  {[type]}    timeRange 时间数组
 * @param  {...[type]} names     对应的时间格式名称
 * @return {[type]}              [description]
 */
export function timeRangeToString(timeRange, ...names) {
    if (!Array.isArray(timeRange) || timeRange.length != names.length)
        return null;

    var data = {};
    for (let i = 0; i < timeRange.length; i++) {
        data[names[i]] = timeRange[i].format("YYYY-MM-DD HH:mm:ss");
    }
    return data;
}


/* ****************************************************************************************************************************** *
 * ******************************************************* 特殊工具类 ************************************************************ *
 * ****************************************************************************************************************************** */


/*--------------------------------银行卡----------------------------------------*
 * 银行卡号Luhm校验
 * Luhm校验规则：16位银行卡号（19位通用）:
 * 1.将未带校验位的 15（或18）位卡号从右依次编号 1 到 15（18），位于奇数位号上的数字乘以 2。
 * 2.将奇位乘积的个十位全部相加，再加上所有偶数位上的数字。
 * 3.将加法和加上校验位能被 10 整除。
 * 方法步骤很清晰，易理解，需要在页面引用Jquery.js    
 * @param {[type]} bankno [description]
 * @return {bool} [description]
 */
export function isBankCard(bankno) {
    if (bankno.length < 16) return false;

    var lastNum = bankno.substr(bankno.length - 1, 1); //取出最后一位（与luhm进行比较）

    var first15Num = bankno.substr(0, bankno.length - 1); //前15或18位
    var newArr = new Array();
    for (var i = first15Num.length - 1; i > -1; i--) { //前15或18位倒序存进数组
        newArr.push(first15Num.substr(i, 1));
    }
    var arrJiShu = new Array(); //奇数位*2的积 <9
    var arrJiShu2 = new Array(); //奇数位*2的积 >9

    var arrOuShu = new Array(); //偶数位数组
    for (var j = 0; j < newArr.length; j++) {
        if ((j + 1) % 2 == 1) { //奇数位
            if (parseInt(newArr[j]) * 2 < 9)
                arrJiShu.push(parseInt(newArr[j]) * 2);
            else
                arrJiShu2.push(parseInt(newArr[j]) * 2);
        } else //偶数位
            arrOuShu.push(newArr[j]);
    }

    var jishu_child1 = new Array(); //奇数位*2 >9 的分割之后的数组个位数
    var jishu_child2 = new Array(); //奇数位*2 >9 的分割之后的数组十位数
    for (var h = 0; h < arrJiShu2.length; h++) {
        jishu_child1.push(parseInt(arrJiShu2[h]) % 10);
        jishu_child2.push(parseInt(arrJiShu2[h]) / 10);
    }

    var sumJiShu = 0; //奇数位*2 < 9 的数组之和
    var sumOuShu = 0; //偶数位数组之和
    var sumJiShuChild1 = 0; //奇数位*2 >9 的分割之后的数组个位数之和
    var sumJiShuChild2 = 0; //奇数位*2 >9 的分割之后的数组十位数之和
    var sumTotal = 0;
    for (var m = 0; m < arrJiShu.length; m++) {
        sumJiShu = sumJiShu + parseInt(arrJiShu[m]);
    }

    for (var n = 0; n < arrOuShu.length; n++) {
        sumOuShu = sumOuShu + parseInt(arrOuShu[n]);
    }

    for (var p = 0; p < jishu_child1.length; p++) {
        sumJiShuChild1 = sumJiShuChild1 + parseInt(jishu_child1[p]);
        sumJiShuChild2 = sumJiShuChild2 + parseInt(jishu_child2[p]);
    }
    //计算总和
    sumTotal = parseInt(sumJiShu) + parseInt(sumOuShu) + parseInt(sumJiShuChild1) + parseInt(sumJiShuChild2);

    //计算Luhm值
    var k = parseInt(sumTotal) % 10 == 0 ? 10 : parseInt(sumTotal) % 10;
    var luhm = 10 - k;

    if (lastNum == luhm) {
        return true;
    } else {
        return false;
    }
}


/*--------------------------------身份证----------------------------------------*/
/*
根据〖中华人民共和国国家标准 GB 11643-1999〗中有关公民身份号码的规定，公民身份号码是特征组合码，由十七位数字本体码和一位数字校验码组成。排列顺序从左至右依次为：六位数字地址码，八位数字出生日期码，三位数字顺序码和一位数字校验码。
    地址码表示编码对象常住户口所在县(市、旗、区)的行政区划代码。
    出生日期码表示编码对象出生的年、月、日，其中年份用四位数字表示，年、月、日之间不用分隔符。
    顺序码表示同一地址码所标识的区域范围内，对同年、月、日出生的人员编定的顺序号。顺序码的奇数分给男性，偶数分给女性。
    校验码是根据前面十七位数字码，按照ISO 7064:1983.MOD 11-2校验码计算出来的检验码。

出生日期计算方法。
    15位的身份证编码首先把出生年扩展为4位，简单的就是增加一个19或18,这样就包含了所有1800-1999年出生的人;
    2000年后出生的肯定都是18位的了没有这个烦恼，至于1800年前出生的,那啥那时应该还没身份证号这个东东，⊙﹏⊙b汗...
下面是正则表达式:
 出生日期1800-2099  (18|19|20)?\d{2}(0[1-9]|1[12])(0[1-9]|[12]\d|3[01])
 身份证正则表达式 /^\d{6}(18|19|20)?\d{2}(0[1-9]|1[012])(0[1-9]|[12]\d|3[01])\d{3}(\d|X)$/i            
 15位校验规则 6位地址编码+6位出生日期+3位顺序号
 18位校验规则 6位地址编码+8位出生日期+3位顺序号+1位校验位

 校验位规则     公式:∑(ai×Wi)(mod 11)……………………………………(1)
                公式(1)中： 
                i----表示号码字符从由至左包括校验码在内的位置序号； 
                ai----表示第i位置上的号码字符值； 
                Wi----示第i位置上的加权因子，其数值依据公式Wi=2^(n-1）(mod 11)计算得出。
                i 18 17 16 15 14 13 12 11 10 9 8 7 6 5 4 3 2 1
                Wi 7 9 10 5 8 4 2 1 6 3 7 9 10 5 8 4 2 1

*/
//身份证号合法性验证 
//支持15位和18位身份证号
//支持地址编码、出生日期、校验位验证

/*
 * 验证身份证
 * 返回:{
 *     pass:boolean:验证是否通过
 *     error:str:验证不通过的原因
 *     gender:int：性别(1:男 0:女)
 * }
 */
export function isIDCodeValid(code) {
    var city = {
        11: "北京",
        12: "天津",
        13: "河北",
        14: "山西",
        15: "内蒙古",
        21: "辽宁",
        22: "吉林",
        23: "黑龙江 ",
        31: "上海",
        32: "江苏",
        33: "浙江",
        34: "安徽",
        35: "福建",
        36: "江西",
        37: "山东",
        41: "河南",
        42: "湖北 ",
        43: "湖南",
        44: "广东",
        45: "广西",
        46: "海南",
        50: "重庆",
        51: "四川",
        52: "贵州",
        53: "云南",
        54: "西藏 ",
        61: "陕西",
        62: "甘肃",
        63: "青海",
        64: "宁夏",
        65: "新疆",
        71: "台湾",
        81: "香港",
        82: "澳门",
        91: "国外 "
    };
    var tip = "";
    var pass = true;
    var gender = code.substr(16, 1) % 2 ? 1 : 0;

    if (!code || !/^\d{6}(18|19|20)?\d{2}(0[1-9]|1[012])(0[1-9]|[12]\d|3[01])\d{3}(\d|X)$/i.test(code)) {
        tip = "身份证号格式错误";
        pass = false;
    } else if (!city[code.substr(0, 2)]) {
        tip = "地址编码错误";
        pass = false;
    } else {
        //18位身份证需要验证最后一位校验位
        if (code.length == 18) {
            code = code.split('');
            //∑(ai×Wi)(mod 11)
            //加权因子
            var factor = [7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2];
            //校验位
            var parity = [1, 0, 'X', 9, 8, 7, 6, 5, 4, 3, 2];
            var sum = 0;
            var ai = 0;
            var wi = 0;
            for (var i = 0; i < 17; i++) {
                ai = code[i];
                wi = factor[i];
                sum += ai * wi;
            }
            var last = parity[sum % 11];
            if (parity[sum % 11] != code[17]) {
                tip = "校验位错误";
                pass = false;
            }
        }
    }
    return {
        pass: pass,
        error: tip,
        gender: gender
    };
}
