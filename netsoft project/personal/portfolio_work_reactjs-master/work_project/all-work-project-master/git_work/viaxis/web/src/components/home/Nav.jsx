import React, { PropTypes } from 'react';
import TweenOne from 'rc-tween-one';
import { Menu, Icon } from 'antd';
import { Link } from 'react-router';

import BasicComponent from "../common/BasicComponent";


const Item = Menu.Item;

class Header extends BasicComponent {

    state = {
        navPadTop: false
    }

    /* ----------------------- Sys ------------------------- */

    componentDidMount() {
        window.addEventListener('scroll', this.handleScroll.bind(this));
    }

    componentWillUnmount() {
        window.removeEventListener('scroll', this.handleScroll.bind(this));
    }

    /* ----------------------- Events -------------------------- */
    handleScroll(e) {
        // 滚动条离页面最上方的距离
        const scrollTop = document.body.scrollTop;
        if (scrollTop >= 20 && !this.state.navPadTop)
            this.setState({ navPadTop: true });
        else if (scrollTop < 20 && this.state.navPadTop)
            this.setState({ navPadTop: false });
    }

    /* ----------------------- Render ------------------------- */
    render() {

        const headerClass = this.state.navPadTop ? "header-nav top" : "header-nav";

        const navData = [{ text: '首页', url: "/" },
        { text: '活动', url: "/active" },
        { text: '帮助', url: "/help" },
        { text: '关于我们', url: "/about" }
        ];

        if (this.isLogin)
            navData.push({ text: "立即游戏", url: "/bet" });

        const navChildren = Object.keys(navData).map((key) => {
            return (<Item key={key}>
                <a href={navData[key].url} target="_blank">{navData[key].text}</a>
            </Item>);
        });

        return (
            <TweenOne
                className={headerClass}
                component="header"
                animation={{ opacity: 0, type: 'from' }}
            >
                <TweenOne
                    className={`${this.props.className}-logo`}
                    animation={{ x: -30, delay: 100, type: 'from', ease: 'easeOutQuad' }}
                >
                    <img width="100%" src="images/logo.svg" />
                </TweenOne>
                <TweenOne
                    className={`${this.props.className}-nav`}
                    animation={{ x: 30, delay: 100, type: 'from', ease: 'easeOutQuad' }}
                >
                    <Menu mode="horizontal" defaultSelectedKeys={['a']}>
                        {navChildren}
                        {this.isLogin ?
                            this.renderLoginMenu() :
                            [
                                <Item key="login">
                                    <Link to="/login">登陆</Link>
                                </Item>,
                                <Item key="register">
                                    <Link to="/register">注册</Link>
                                </Item>
                            ]
                        }
                    </Menu>
                </TweenOne>
            </TweenOne>);
    }

    renderLoginMenu() {
        return [
            <Item key="username">
                <Link to="/passport/profile">个人中心</Link>
            </Item>,
            <Item key="logout">
                <span onClick={this.logout.bind(this)}>退出</span>
            </Item>
        ];
    }
}

Header.propTypes = {
    className: PropTypes.string,
};

Header.defaultProps = {
    className: 'header-nav',
};

export default Header;
