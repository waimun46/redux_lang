import React, { Component } from 'react';
import { Icon, Button, Form, Input, Select, DatePicker, Row, Col } from 'antd';

const FormItem = Form.Item;

import BasicComponent from "./BasicComponent";

import * as Utils from "../../utils/common";

/**
 * 通用搜索组件
 * Example:
 * <SearchForm span={4} gutter={16}>
 *     <com_id label="label1" options={} format={function}>
 *         <Input placeholder="placeholder" />
 *     </com_id>
 *     <name label="label2">
 *         <Input placeholder="placeholder" />
 *     </name>
 * </SearchForm>
 * format 是自定义方法，用于自定义解析返回的数据。
 */
class SearchForm extends BasicComponent {

    constructor(props) {
        super(props);

        const { span, gutter, expand } = this.props;
        this.state = {
            expand: expand || false,
            span: span || 6,
            gutter: gutter || 16
        };
    }

    /* ---------------------------- 系统函数 ------------------------------*/

    componentWillMount() {
        this.initChildren(this.props.children);
    }

    componentWillReceiveProps(newProps) {
        if (newProps.children != this.props.children) {
            this.initChildren(newProps.children);
        }
    }

    /* ---------------------------- Functions ------------------------------*/

    initChildren(children) {
        if (!Array.isArray(children))
            children = [children];

        const childrens = {};
        for (let i = 0; i < children.length; i++) {
            if (!children[i]) continue;
            var id = children[i].type;
            childrens[id] = children[i];
        }

        this.children = childrens;
    }

    /* ---------------------------- Events ------------------------------*/

    handleSearch(e) {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {

            var data = {};
            for (var name in values) {
                if (values[name] != null && values[name] != undefined) {
                    var fnFormat = this.children && this.children[name] && this.children[name].props && this.children[name].props.format;
                    var key = this.parseTypeName(name);
                    if (fnFormat && typeof fnFormat === "function") {
                        var res = this.children[name].props.format(values[name]);
                        if (Utils.isDict(res))
                            data = Object.assign({}, data, res);
                        else
                            data[key] = res;
                    } else {
                        data[key] = values[name];
                    }
                }
            }
            this.props.onSearch && this.props.onSearch(data);
        });
    }

    handleReset(e) {
        this.props.form.resetFields();
        this.props.onReset && this.props.onReset();
    }

    toggle() {
        const { expand } = this.state;
        this.setState({ expand: !expand });
    }

    /**
     * 表单项数量
     * @return {[type]} [description]
     */
    get childsCount() {
        let childrens = this.props.children;
        if (!Array.isArray(childrens))
            return 1;
        return childrens.filter(it => it).length;
    }

    /**
     * 每行显示的个数
     * @return {[type]} [description]
     */
    get rowChildCount() {
        return 24 / this.state.span;
    }

    /**
     * 是否显示更多按钮
     * @return {[type]} [description]
     */
    get showExtraButton() {
        return this.childsCount / this.rowChildCount > 1;
    }

    parseTypeName(name) {
        let names = name.replace("uuid", "UUID").replace('id', "ID").split('_');
        for (let i in names) {
            names[i] = names[i][0].toUpperCase() + names[i].slice(1);
        }
        return names.join('');
    }

    /* ---------------------------- Renders ------------------------------*/

    renderChildren() {
        const { span, gutter, expand } = this.state;
        const { getFieldDecorator } = this.props.form;
        const formItemLayout = {
            labelCol: { span: 5 },
            wrapperCol: { span: 19 },
        };

        let childrens = this.props.children;
        if (!Array.isArray(childrens))
            childrens = [childrens];

        const children = [];
        for (let i = 0; i < childrens.length; i++) {
            var child = childrens[i];
            if (!child) continue;
            const { label } = child.props;
            children.push(
                <Col span={child.props.span || span} key={i}>
                    <FormItem {...Object.assign({}, formItemLayout, child.props)} label={child.props.label}>
                        {getFieldDecorator(child.type, child.props.options)(
                            child.props.children
                        )}
                    </FormItem>
                </Col>
            );
        }

        const shownCount = expand ? children.length : (24 / span);

        return children.slice(0, shownCount);
    }

    render() {
        const showExtra = this.showExtraButton;

        // 是否在同一行内合并所有内容(显示按钮)
        const mixedInOneRow = !showExtra && this.childsCount < this.rowChildCount;

        return (
            <Form horizontal className="ant-advanced-search-form" onSubmit={this.handleSearch.bind(this)}>
                <Row>
                    {this.renderChildren()}
                    {mixedInOneRow && this.renderButtons((this.rowChildCount - this.childsCount) * this.state.span)}
                </Row>
                {!mixedInOneRow &&
                    <Row>
                        {this.renderButtons()}
                    </Row>
                }
            </Form>
        );
    }

    renderButtons(span = 24) {
        const expand = this.state.expand;
        const showExtra = this.showExtraButton;
        return (
            <Col span={span} style={{ textAlign: 'right' }}>
                {this.props.showAddBtn &&
                    <Button type="primary" className="fn-left" onClick={() => this.props.onAdd && this.props.onAdd()}>添加</Button>
                }
                <Button type="primary" htmlType="submit" icon="search">搜索</Button>
                <Button style={{ marginLeft: 8 }} onClick={this.handleReset.bind(this)} icon="reload">
                    清空
                </Button>
                {showExtra &&
                    <a style={{ marginLeft: 8, fontSize: 12 }} onClick={this.toggle.bind(this)}>
                        {expand ? "收起" : "展开"} <Icon type={expand ? 'up' : 'down'} />
                    </a>
                }
            </Col>
        );
    }
}

SearchForm.propTypes = {
    onSearch: React.PropTypes.func,
    onReset: React.PropTypes.func,
    onAdd: React.PropTypes.func,
}

export default Form.create()(SearchForm);
