import $ from "jquery";
import { ActionState } from "../app_constants";
import * as Common from "../utils/common";
import getSocket from "../websocket";

const socket = getSocket();

/* ----------------------- Bet ---------------------------- */
/**
 * 投注
 * @param  {[type]} data 投注数据，格式具体详见 /utils/bet_helper.buildBetNumData
 * @return {[type]}      [description]
 */
export function bet(data) {
	// var body = JSON.stringify({
	// 	data: JSON.stringify(data)
	// });

	var body = JSON.stringify(
		data
	);
	return {
		name: "Bet",
		callAPI: () => Common.POST("/api/game/bet", body)()
	};
}

/**
 * 投注已接受
 * @param  {[type]} data 投注成功的数据
 * @return {[type]}      [description]
 */
export function betAccepted(data) {
	return dispatch => {
		dispatch({
			type: "BetAccepted",
			state: ActionState.Success,
			data
		});
		// 告诉调用代码不需要再等待。
		return Promise.resolve();
	};
}

/* ----------------------- DealInfo ---------------------------- */

/**
 * 获取投注记录
 * @param  {[type]} querys    [查询条件]
 * @param  {Number} pageIndex [页码]
 * @return {[type]}           [description]
 */
export function queryDealInfo(querys) {
	var queryStr = JSON.stringify(querys);
	return {
		name: "DealInfo",
		callAPI: () => Common.POST(`/api/dealinfo`, queryStr)()
	}
}

/* ----------------------- Game Type Info ---------------------------- */
/**
 * 获取游戏玩法类型信息
 * @param  {[type]} group_name [description]
 * @return {[type]}            [description]
 */
export function getGameTypeInfo(group_name) {
	var body = JSON.stringify({
		client_time: Common.unixtimestampFromDate()
	});
	return {
		name: "GameTypeInfo",
		callAPI: () => Common.POST(`/api/game/${group_name}/type/info`, body)()
	};
}

/* ----------------------- Game Group ---------------------------- */
/**
 * 获取游戏组信息
 * @return {[type]} [description]
 */
export function getGameGroup() {
	return {
		name: "GameGroup",
		callAPI: () => Common.POST("/api/game/group")()
	}
}


/* ----------------------- Game Data ---------------------------- */

/**
 * 查询历史开奖数据
 * @param  {[type]} group_name [description]
 * @param  {[type]} pageIndex  [description]
 * @param  {[type]} querys     [description]
 * @return {[type]}            [description]
 */
export function queryGameData(group_name, pageIndex, querys) {
	var queryParams = Object.assign({}, querys, {
		pageIndex,
		group_name
	});
	var body = JSON.stringify(queryParams);
	return {
		name: "GameDataHistory",
		callAPI: () => Common.POST(`/api/game/${group_name}/data`, body)()
	};
}

/**
 * 查询指定彩种期号开奖数据
 * @param  {[type]} groupId [description]
 * @param  {[type]} qh         [description]
 * @return {[type]}            [description]
 */
export function getGameData(groupId, qh) {
	return {
		name: "GameData",
		callAPI: () => Common.POST(`/api/game/${groupId}/${qh}`)()
	};
}

/**
 * 获取当前期游戏数据
 * @param  {[type]} group_name [description]
 * @return {[type]}            [description]
 */
export function getGameDataCurrent(group_name) {
	var body = JSON.stringify({
		client_time: Common.unixtimestampFromDate()
	});
	return {
		name: "GameDataCurrent",
		callAPI: () => Common.POST(`/api/game/${group_name}/data/current`, body)()
	};
}

/**
 * 获取下一期游戏数据
 * @param  {[type]} group_name [description]
 * @return {[type]}            [description]
 */
export function getGameDataNext(group_name) {
	var body = JSON.stringify({
		client_time: Common.unixtimestampFromDate()
	});
	return {
		name: "GameDataNext",
		payload: { group: group_name },
		callAPI: () => Common.POST(`/api/game/${group_name}/data/next`, body)()
	};
}

/**
 * 获取上一期游戏数据
 * @param  {[type]} group_name [description]
 * @return {[type]}            [description]
 */
export function getGameDataPrev(group_name) {
	return {
		name: "GameDataPrev",
		payload: { group: group_name },
		callAPI: () => Common.POST(`/api/game/${group_name}/data/prev`)()
	};
}

/**
 * 获取未开奖游戏数据
 * @param  {[type]} group_name [description]
 * @return {[type]}            [description]
 */
export function getGameDataUnOpen(group_name) {
	return {
		name: "GameDataUnOpen",
		callAPI: () => Common.POST(`/api/game/${group_name}/data/unopen`)(),
		payload: { group: group_name }
	};
}

export function gameDataOpen(group_name) {
	return {
		type: "GameDataOpen"
	}
}

/**
 * 关闭当前期数据，将其复制到上一期
 * @param  {[type]} group_name [description]
 * @param  {[type]} data       [description]
 * @return {[type]}            [description]
 */
export function closeCurrentGameData(group_name) {
	return {
		type: "GameDataCurrentClosed"
	}
}

/**
 * 获取当前号码走势
 */
export function getGameTrendData(group_id, topCount, startTime, endTime, columns) {
	var queryParams = Object.assign({}, {
		group_name: group_id,
		topCount,
		startTime,
		endTime,
		columns
	});
	var body = JSON.stringify(queryParams);
	return {
		name: "GameDataTrend",
		callAPI: () => Common.POST(`/api/game/trend`, body)()
	};
}

export function getGameDataMissing(groupName) {
	var body = JSON.stringify({ groupName });
	return {
		name: "GameDataMissing",
		callAPI: () => Common.POST(`/api/game/missing`, body)()
	};
}

export function getGameGroupById(group_id) {
	var params = Object.assign({}, {
		group_name: group_id
	});
	var body = JSON.stringify(params);
	return {
		name: "GameGroupById",
		callAPI: () => Common.POST(`/api/game/gamegroup`, body)()
	};
}
