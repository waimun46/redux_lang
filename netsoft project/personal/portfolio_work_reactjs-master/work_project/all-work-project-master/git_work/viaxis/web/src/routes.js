import Auth from "./auth";

const passport = './components/passport';
const user = './components/user';
const bill = './components/bill';
const game = './components/game';
const bank = './components/bank';
const report = './components/report';


function requireLogin(nextState, replace) {
    if (!Auth.isLogged()) {
        replace({
            pathname: '/login',
            state: { nextPathname: nextState.location.pathname }
        });
    }
}

function redirectToDashboard(nextState, replace) {
    if (Auth.isLogged()) {
        replace('/');
    }
}

/* ----------- Split Code Module -------------*/

const login = (nextState, cb) => {
    require.ensure([], require => {
        cb(null, require(`${passport}/Login`))
    })
};

const bet = (nextState, cb) => {
    require.ensure([], require => {
        cb(null, require(`${game}/Bet`))
    })
};

const deals = (nextState, cb) => {
    require.ensure([], require => {
        cb(null, require(`${game}/DealHistory`))
    })
};

const trend = (nextState, cb) => {
    require.ensure([], require => {
        cb(null, require(`${game}/Trend`))
    })
};

const bankCard = (nextState, cb) => {
    require.ensure([], require => {
        cb(null, require(`${bank}/CardList`))
    })
};

const billflow = (nextState, cb) => {
    require.ensure([], require => {
        cb(null, require(`${bill}/BillFlowList`))
    })
};

const withdraw = (nextState, cb) => {
    require.ensure([], require => {
        cb(null, require(`${bill}/Withdraw`))
    })
};

const withdrawList = (nextState, cb) => {
    require.ensure([], require => {
        cb(null, require(`${bill}/WithdrawList`))
    })
};

const deposit = (nextState, cb) => {
    require.ensure([], require => {
        cb(null, require(`${bill}/Deposit`))
    })
};

const profitUser = (nextState, cb) => {
    require.ensure([], require => {
        cb(null, require(`${report}/ProfitUser`))
    })
};

const profitGame = (nextState, cb) => {
    require.ensure([], require => {
        cb(null, require(`${report}/ProfitGame`))
    })
};

const profitDividend = (nextState, cb) => {
    require.ensure([], require => {
        cb(null, require(`${report}/ProfitDividend`))
    })
};

/* --------------- Routes ------------- */

const routes = [
    { path: '/', component: require("./components/home/index") },
    { path: 'login', component: require(`${passport}/Login`), onEnter: redirectToDashboard },
    { path: 'register(/:referral)', component: require(`${passport}/Register`) },
    { path: 'forget', component: require(`${passport}/ForgotPassword`) },
    { path: "password/reset/:ticket", components: require(`${passport}/ResetPassword`) },
    {
        path: '/', component: require("./containers/DashLayout"),
        childRoutes: [
            /* --------------- Passport -------- */
            { path: '/passport/profile', component: require(`${passport}/Profile`), onEnter: requireLogin },
            { path: '/passport/password(/:type)', component: require(`${passport}/ChangePassword`), onEnter: requireLogin },
            /* --------------- Referral --------- */
            { path: '/user/referral', component: require(`${user}/Referral`), onEnter: requireLogin },
            /* --------------- User ------------ */
            { path: '/user(/:userId)', component: require(`${user}/UserList`), onEnter: requireLogin },
            /* --------------- Bank -------- */
            { path: '/bank', getComponent: bankCard, onEnter: requireLogin },
            /* --------------- Bill --------------- */
            { path: '/billflow', getComponent: billflow, onEnter: requireLogin },
            { path: "/transfer/history", component: require(`${bill}/Transfer`).default, onEnter: requireLogin },
            { path: '/withdraw', getComponent: withdraw, onEnter: requireLogin },
            { path: '/withdraw/history', getComponent: withdrawList, onEnter: requireLogin },
            { path: '/deposit', getComponent: deposit, onEnter: requireLogin },
            /* --------------- Game --------------- */
            { path: '/bet(/:gameGroup)', getComponent: bet, onEnter: requireLogin },
            { path: '/deals', getComponent: deals, onEnter: requireLogin },
            { path: '/trend(/:gameGroup)', getComponent: trend, onEnter: requireLogin },
            /* --------------- Profit --------------- */
            { path: "/profit/user(/:userId)", getComponent: profitUser, onEnter: requireLogin },
            { path: "/profit/game(/:userId)", getComponent: profitGame, onEnter: requireLogin },
            { path: "/profit/dividend(/:userId)", getComponent: profitDividend, onEnter: requireLogin },
        ]
    },
    /* --------------- Article -------------- */
    { path: "/:category/:articleName.html", component: require("./components/article") },
    // 404
    { path: "*", component: require("./components/PageNotFound").default }
];

export default routes;
