import * as MathHelper from '../src/utils/math.js';
import Lodash from "lodash";
import assert from "assert";


describe('全排列组合测试', function() {
    var resp1 = MathHelper.mergeArray(1,[1,2,3,4]);

    it('必须是数组', ()=>assert(Array.isArray(resp1)));
    it("长度为4", () => assert.equal(resp1.length, 4));

    var resp2 = MathHelper.mergeArray([1, 2], [1, 2, 3, 4]);
    it('必须是数组', () => assert(Array.isArray(resp2)));
    it("长度为8", () => assert.equal(resp2.length, 8));


    var resp3 = MathHelper.mergeArray([1, 2, 3, 4], [
        [1, 5],
        [1, 6],
        [1, 7],
        [1, 8],
        [2, 5],
        [2, 6],
        [2, 7],
        [2, 8],
        [3, 5],
        [3, 6],
        [3, 7],
        [3, 8],
        [4, 5],
        [4, 6],
        [4, 7],
        [4, 8]
    ]);
    it('必须是数组', () => assert(Array.isArray(resp3)));
    it("长度为64", () => assert.equal(resp3.length, 64));

});

describe('全排列测试', function() {
    var resp1 = MathHelper.permutation(true, [1, 2, 3, 4, 5], [6, 7, 8, 9, 10]);
    it("长度为25", () => assert.equal(resp1.length, 25));

    var resp2 = MathHelper.permutation(true, [1, 2, 3, 4, 5], [6, 7, 8, 9, 10], [1, 2, 3, 4, 5]);
    it("长度为125", () => assert.equal(resp2.length, 125));

    var resp3 = MathHelper.permutation(false, [1, 2, 3, 4, 5], [1, 2, 3, 4, 5]);
    it("长度为20", () => assert.equal(resp3.length, 20));

    it("长度为16", () => {
        var resp = MathHelper.permutation(true, [1, 2, 3, 4], [1, 2, 3, 4]);
        assert.equal(resp.length, 16);
    });

    it("1", () => {
        var resp = MathHelper.permutation(true, [10], [9]);
        assert.equal(resp.length, 1);
    });

    it("长度为24", () => {
        var resp = MathHelper.permutation(true, [1, 2, 3, 4, 5], [1, 2, 3, 4, 5], [1, 2, 3, 4, 5], [1]);
        assert.equal(resp.length, 24);
    });
})