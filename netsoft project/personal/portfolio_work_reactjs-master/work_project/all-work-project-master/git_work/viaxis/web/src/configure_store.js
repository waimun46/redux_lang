import { createStore, applyMiddleware } from 'redux'
import thunkMiddleware from 'redux-thunk'
import createLogger from 'redux-logger'
import rootReducer from './reducers'
import callAPIMiddleware from "./actions/callAPIMiddleware";

const loggerMiddleware = createLogger()

const middleWares = [
    thunkMiddleware,
    callAPIMiddleware,
    loggerMiddleware
];

// if (__DEV__)
//     middleWares.push(loggerMiddleware);

const createStoreWithMiddleware = applyMiddleware(
    ...middleWares
)(createStore);

/**
 * PS.请注意！！！！
 * redux的调用方式为实例化一个store,调用connect方法注册组件，添加到自身的监听列表中，
 * 并在state发生变化时调用组件的 componentWillReceiveProps 方法设置新state。
 * 所以必须保证store的唯一。本项目至少有两处会直接调用到store,一处在组件注册，一处在websocket调用。
 * 故采用单例模式保证实例唯一。
 *
 * 编写组件时，要求实现 shouldComponentUpdate 方法，提高性能。
 */

let _instance;
export default function configureStore(initialState) {
	if (_instance != null) return _instance;
	
  	_instance = createStoreWithMiddleware(rootReducer, initialState);
  	return _instance;
}
