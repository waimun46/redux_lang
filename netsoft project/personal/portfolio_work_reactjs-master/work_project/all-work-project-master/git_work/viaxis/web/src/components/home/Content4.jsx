import React from 'react';
import TweenOne from 'rc-tween-one';
import QueueAnim from 'rc-queue-anim';
import OverPack from 'rc-scroll-anim/lib/ScrollOverPack';

class Content extends React.Component {

  static propTypes = {
    id: React.PropTypes.string,
    className: React.PropTypes.string,
  };

  static defaultProps = {
    className: 'content5',
  };

  getBlockChildren = (item, i) =>
    (<li
      key={i}
      style={item.style}
    >
      <span style={item.children.img.style}>
        <img src={item.children.img.children} width="100%" />
      </span>
      <h2 style={item.children.title.style}>{item.children.title.children}</h2>
      <p style={item.children.content.style}>{item.children.content.children}</p>
    </li>);


  render() {
    const dataSource = [
      { children: { img: { children: 'https://zos.alipayobjects.com/rmsportal/NKBELAOuuKbofDD.png' }, title: { children: '技术' }, content: { children: '10年行业经验 创新种类 稳定于畅顺的体验。' } } },
      { children: { img: { children: 'https://zos.alipayobjects.com/rmsportal/xMSBjgxBhKfyMWX.png' }, title: { children: '融合' }, content: { children: '符合客户需求 娱乐界面简单直接 操作容易 客户量及利润瞬间凝聚。\n解放业务及技术生产力，推动彩票娱乐服务底层创新。' } } },
      { children: { img: { children: 'https://zos.alipayobjects.com/rmsportal/MNdlBNhmDBLuzqp.png' }, title: { children: '整合方案' }, content: { children: '提供省时省力 成本低 效益大的整合方案 让你赢在起跑线 业务蒸蒸日上。' } } },
    ];
    const ulChildren = dataSource.map(this.getBlockChildren);
    return (
      <div
        {...this.props}
        className="content-template-wrapper"
      >
        <OverPack
          className={`content-template ${this.props.className}`}
          hideProps={{ img: { reverse: true } }}
          location={this.props.id}
        >
          <QueueAnim
            className={`${this.props.className}-text`}
            key="text"
            type="left"
            leaveReverse
            ease={['easeOutCubic', 'easeInCubic']}
          >
            <h1
              key="h1"
            >
              帝国娱乐提供专业的介接服务
            </h1>
            <p
              key="p"
            >
              基于阿里云计算强大的基础资源
            </p>
            <QueueAnim component="ul" key="ul" type="left">
              {ulChildren}
            </QueueAnim>
          </QueueAnim>
          <TweenOne
            className={`${this.props.className}-img`}
            key="img"
            animation={{ x: 30, opacity: 0, type: 'from' }}
          >
            <img width="100%" src="https://zos.alipayobjects.com/rmsportal/VHGOVdYyBwuyqCx.png" />
          </TweenOne>
        </OverPack>
      </div>
    );
  }
}

export default Content;
