import React from 'react';
import { connect } from 'react-redux';
import $ from "jquery";

import BasicComponent from "../components/common/BasicComponent";
import DashLayout from "./DashLayout";

class DashContainer extends BasicComponent {

    constructor(props){
        super(props);
    }

    render(){
        const winHeight = $(window).height() - 64;
        return (
            <div className="ant-layout-body" style={{minHeight:winHeight}}>
                <div className="title">
                    <h2>{this.props.title}</h2>
                    {this.props.extra}
                </div>
                {this.props.children}
            </div>
        );
    }


}

module.exports = DashContainer;
