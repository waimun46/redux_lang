import React from 'react';
import { Spin, Icon, Button, Dropdown, Menu, notification } from 'antd';
import { connect } from 'react-redux';
import { Link } from 'react-router'

import BasicComponent from "./BasicComponent";
import LotteryMenu from "./LotteryMenu";
import Marquee from "../marquee";

import { getUserInfo } from "../../actions/passport";
import { getBalance } from "../../actions/bill";
import { getBulletins,getWarnBulletins } from "../../actions/notice";
import * as Utils from "../../utils/common";
import * as Constants from "../../app_constants";

const SubMenu = Menu.SubMenu;

class Header extends BasicComponent {
    
    constructor(props){
        super(props);
    }

    /* ---------------- Sys ----------------- */
    
    componentWillMount() {
        this.getUserInfo();
        this.getBalance();
        this.getBulletins();
    }

    componentDidUpdate(prevProps, prevState){
        if(this.props.bulltins != prevProps.bulltins)
            this.showWarn();
    }


    /* ---------------- Events ----------------- */
    

    /* ---------------------------- Functions ------------------------------*/

    getBalance(){
        this.props.dispatch(getBalance());
    }

    getUserInfo(){
        const { user } = this.props;
        if (!user.data)
            this.props.dispatch(getUserInfo());
    }

    getBulletins(){
        this.props.dispatch(getBulletins(0));
    }

    showWarn(){
        const warnings = this.props.bulltins.warning;
        warnings.map(it=>{
            notification.warning({message: it.Content, duration:null});
        }); 
    }

    /* ---------------- Renders ----------------- */

    render() {
        const { user, balance, bulltins } = this.props;
        const userinfo = user.data;
        const bulltinsInfo = bulltins.info;

        return (
            <div className="ant-layout-header">
                <ul>
                    <li>
                        <LotteryMenu />
                    </li>
                    <li className="email">
                        {userinfo&&userinfo.Email}
                    </li>
                    <li className="balance">
                        余额
                        <span className="balance-amt">
                            {balance.isFetching?<span><Spin />加载中...</span>:`￥ ${Utils.milliFormat(balance.data)}`}
                        </span>

                        <span className="balance-refresh" onClick={()=>this.getBalance()}>
                            <Icon type="reload" />
                            刷新
                        </span>
                    </li>
                    <li className="buttons">
                        <Link to="/deposit">
                            <i className="iconfont icon-deposit"></i> 充值
                        </Link>
                        <Link to="/withdraw">
                            <i className="iconfont icon-withdraw"></i> 提现
                        </Link>
                    </li>
                    <li className="bulletin">
                        <Icon type="notification" />
                        <div className="bulletin-content">
                            <Marquee>                            
                                {bulltinsInfo.map(it=><li key={it.Id}>{it.Content}</li>)}                         
                            </Marquee>
                        </div>
                    </li>
                </ul>
                
            </div>
        );
    }
}

export default connect(state => ({
    user: state.passport.userinfo,
    balance: state.bill.balance,
    bulltins: state.notice.bulltins,
}))(Header);
