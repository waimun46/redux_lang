import { ActionState } from "../app_constants";
import ReduceBase from "./reduce_base";

const defaultState = {
    userinfo: {
        isFetching: false,
        data: null
    },
    // 最后登陆信息
    lastlogin: null,
    // 团队成员
    team: {
        isFetching: false,
        data: null,
        pageIndex: 0,
        count: 0
    },
    // 用户上级列表
    parents: {
        isFetching: false,
        data: null
    },
    // 会员推广链接
    referral: {
        isFetching: false,
        data: null,
    }
};


class PassportReducer extends ReduceBase {
    static reduce(state = defaultState, action) {
        switch (action.type) {
            case "UserInfo":
                return PassportReducer._reduce_userinfo(state, action);
            case "LastLogin":
                return PassportReducer._reduce_last_login(state, action);
            case "Team":
                return PassportReducer._process_page(state, action, "team");
            case "GetParents":
                return PassportReducer._process_list(state, action, "parents");
            case "AddAgent":
                return PassportReducer._add_agent(state, action);
            case "ReferralQuery":
                return PassportReducer._process_list(state, action, "referral");
            case "ReferralAdd":
                return PassportReducer._add_referral(state, action);
            case "Config":
                return PassportReducer._reduce_config(state, action);
            default:
                return state;
        }
    }

    static _reduce_userinfo(state, action) {
        let isFetching = action.state == ActionState.Doing;
        const resp = action.response;
        if (resp && resp.code == 0) {
           console.log(resp);
            let userinfo = {
                isFetching,
                data: resp.data
            };
            return Object.assign({}, state, { userinfo });
        }
        return Object.assign({}, state, {
            userinfo: {
                isFetching
            }
        });
    }

    static _reduce_last_login(state, action) {
        const resp = action.response;
        if (resp && resp.code == 0) {
            const lastlogin = resp.data;
            return Object.assign({}, state, { lastlogin });
        }
        return state;
    }

    static _add_agent(state, action) {
        let isFetching = action.state == ActionState.Doing;
        const resp = action.response;
        if (resp && resp.code == 0) {
            let data = state.team.data;
            if (data == null) data = [resp.data];
            else
                data = [resp.data, ...data];

            let team = {
                isFetching,
                data: data,
                count: state.team.count + 1,
                pageIndex: state.team.pageIndex
            };
            return Object.assign({}, state, { team });
        }
        return state;
    }

    static _add_referral(state, action) {
        let isFetching = action.state == ActionState.Doing;
        const resp = action.response;
        if (resp && resp.code == 0) {
            let data = state.referral.data;
            if (data == null) data = [resp.data];
            else
                data = [resp.data, ...data];

            let referral = {
                isFetching,
                data
            };
            return Object.assign({}, state, { referral });
        }
        return state;
    }

    static _reduce_config(state, action) {
        const resp = action.response;
        if (resp && resp.code == 0) {
            let config = resp.data;
            return Object.assign({}, state, { config });
        }
        return state;
    }

}

module.exports = PassportReducer;
