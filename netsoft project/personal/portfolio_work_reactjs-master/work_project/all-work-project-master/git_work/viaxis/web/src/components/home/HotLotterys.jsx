import React from 'react';
import TweenOne, { TweenOneGroup } from 'rc-tween-one';
import OverPack from 'rc-scroll-anim/lib/ScrollOverPack';

import { Link } from 'react-router';

import BasicComponent from "../common/BasicComponent";

class HotLotterys extends BasicComponent {

    static propTypes = {
        id: React.PropTypes.string,
        className: React.PropTypes.string,
    };

    static defaultProps = {
        className: 'content4',
    };

    getChildrenToRender = data =>
        data.map((item, i) => {
            const children = item.children;
            return (
                <li key={i} style={item.style}>
                    <a href={children.url}>
                        <div className="content-wrapper" style={children.wrapper && children.wrapper.style}>
                            <span>
                                <i className={`iconfont icon-${children.icon}`}></i>
                            </span>
                            <p style={children.content.style}>
                                {children.content.children}
                            </p>
                        </div>
                    </a>
                </li>
            );
    });

    getEnterAnim = (e) => {
        const index = e.index;
        const delay = index % 4 * 100 + Math.floor(index / 4) * 100 + 300;
        return { y: '+=30', opacity: 0, type: 'from', delay };
    };

    /* ------------------------ Render ---------------------------- */

    render() {
        const dataArray = [
            { children: { icon:"CQSSC", url:"/bet", content: { children: '重庆时时彩' } } },
            { children: { icon:"PK10", url:"/bet/pk10", content: { children: '北京PK10' } } },
            { children: { icon:"KL10", url:"/bet/kl10", content: { children: '重庆幸运农场' } } },
            { children: { icon:"KL8", url:"/bet/kl8", content: { children: '北京快乐八分' } } },
            { children: { icon:"FC3D", url:"/bet/fc3d", content: { children: '福彩3D' } } },
            { children: { icon:"PL3", url:"/bet/pl3", content: { children: '排列三' } } },
            { children: { icon:"KENO", url:"/bet/keno", content: { children: 'KENO' } } },
            { children: { icon:"FFC", url:"/bet/ffc", content: { children: '分分彩' } } },
        ];

        const childrenToRender = this.getChildrenToRender(dataArray);
        return (
            <div {...this.props} className="content-template-wrapper content4-wrapper">
                <OverPack
                className={`content-template ${this.props.className}`}
                hideProps={{ h1: { reverse: true }, p: { reverse: true } }}
                location={this.props.id}>
                    <TweenOne
                    animation={{ y: '+=30', opacity: 0, type: 'from' }}
                    component="h1"
                    key="h1"
                    reverseDelay={300}>
                        热门彩种
                    </TweenOne>
                    <TweenOne
                    animation={{ y: '+=30', opacity: 0, type: 'from', delay: 200 }}
                    component="p"
                    key="p"
                    reverseDelay={200}>
                        在这里用一段话介绍彩种的情况
                    </TweenOne>
                    <TweenOneGroup
                    className={`${this.props.className}-img-wrapper`}
                    component="ul"
                    key="ul"
                    enter={this.getEnterAnim}
                    leave={{ y: '+=30', opacity: 0 }}>
                        {childrenToRender}
                    </TweenOneGroup>
                </OverPack>
            </div>
        );
    }
}


export default HotLotterys;
