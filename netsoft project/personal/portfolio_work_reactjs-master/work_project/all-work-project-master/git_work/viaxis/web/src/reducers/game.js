import { ActionState } from "../app_constants";
import ReduceBase from "./reduce_base";
import * as Utils from "../utils/common";

const defaultState = {
    dealinfo: {
        isFetching: false,
        profit: null,
        count: 0,
        pageIndex: 0,
        data: []
    },
    // 开奖数据
    gamedata: {
        // 当前期
        current: {},
        // 下一期
        next: {},
        // 上一期
        prev: {},
        // 未开奖,key:group_name,value:[]
        unopen: {
            isFetching: false
        },
        history: {
            isFetching: false,
            count: 0,
            pageIndex: 0,
            data: []
        },
        // 历史开奖数据，这个数据主要用于投注记录中点击期号查看开奖数据。
        // 格式采用哈希格式，key为大写GroupName，value为{qh,data}
        hash: {
            isFetching: false,
        }
    },
    // 游戏组信息
    groups: [],
    // 游戏玩法类型信息
    game_type_info: {
        isFetching: false,
        data: []
    },
    // 遗漏
    missing: {
        isFetching: false,
        data: []
    },
    //号码走势
    trenddata: {
        isFetching: false,
        data: []
    },
    //号码走势统计
    statisticsdata: {
        isFetching: false,
        data: []
    }
};

class GameReducer extends ReduceBase {
    static reduce(state = defaultState, action) {
        switch (action.type) {
            case "BetAccepted":
                return GameReducer._reduce_bet_accepted(state, action);
            case "DealInfo":
                return GameReducer._reduce_dealinfo(state, action);
            case "DealProfit":
                return GameReducer._reduce_deal_profit(state, action);
            /* ----------------------- Game Type ---------------------- */
            case "GameTypeInfo":
                return GameReducer._process_list(state, action, "game_type_info");;
            /* ----------------------- Game Group ---------------------- */
            case "GameGroup":
                return GameReducer._reduce_game_group(state, action);
            /* ----------------------- Game Data ---------------------- */
            case "GameData":
                return GameReducer._reduce_game_data(state, action);
            case "GameDataHistory":
                return GameReducer._reduce_game_data_history(state, action);
            case "GameDataCurrent":
                return GameReducer._reduce_current_game_data(state, action);
            case "GameDataPrev":
                return GameReducer._reduce_prev_game_data(state, action);
            case "GameDataNext":
                return GameReducer._reduce_next_game_data(state, action);
            case "GameDataUnOpen":
                return GameReducer._reduce_unopen_game_data(state, action);
            case "GameDataCurrentClosed":
                return GameReducer._reduce_current_game_closed(state, action);
            case "GameDataOpen":
                return GameReducer._reduce_game_data_open(state, action);
            case "GameDataMissing":
                return GameReducer._process_list(state, action, "missing");
            case "GameDataTrend":
                return GameReducer.reduce_game_data_trend(state, action);
            case "GameGroupById":
                return GameReducer.reduce_game_group(state, action);
            default:
                return action.payload && Object.assign({}, state, action.payload) || state;
        }
    }

    /* --------------------------------- DealInfo ---------------------------------- */

    static _reduce_bet_accepted(state, action) {
        var { data } = action;
        if (data) {
            var deals = [...data, ...state.dealinfo.data];
            return Object.assign({}, state, {
                dealinfo: {
                    isFetching: false,
                    data: deals.slice(0, 10)
                }
            });
        }
        return state;
    }

    static _reduce_dealinfo(state, action) {
        let isFetching = false;

        if (action.state == ActionState.Doing) {
            // dealinfo存在profit,说明是服务器推动过profit信息，刷新不显示loading状态。
            isFetching = state.dealinfo.profit ? false : true;
            return Object.assign({}, state, {
                dealinfo: { isFetching, count: 0, pageIndex: 0, data: [] }
            });
        }

        const resp = action.response;
        if (resp && resp.code == 0) {
            return Object.assign({}, state, {
                dealinfo: {
                    isFetching,
                    count: resp.data.TotalCount,
                    pageIndex: resp.data.PageIndex,
                    data: resp.data.Data
                }
            });
        }
        return Object.assign({}, state, {
            dealinfo: { isFetching }
        });
    }

    static _reduce_deal_profit(state, action) {
        const resp = action.response;
        if (resp && resp.code == 0) {
            return Object.assign({}, state, {
                dealinfo: {
                    isFetching: false,
                    profit: resp.data
                }
            });
        }
        return state;
    }

    /* --------------------------------- Game Type ---------------------------------- */

    static _reduce_game_type_info(state, action) {
        const resp = action.response;
        if (resp && resp.code == 0) {
            let game_type_info = resp.data;
            return Object.assign({}, state, { game_type_info });
        }
        return state;
    }

    /* --------------------------------- Game Group ---------------------------------- */

    static _reduce_game_group(state, action) {
        const resp = action.response;
        if (resp && resp.code == 0) {
            let groups = resp.data;
            return Object.assign({}, state, { groups });
        }
        return state;
    }

    /* --------------------------------- Game Data ---------------------------------- */

    static _reduce_game_data(state, action) {
        let isFetching = false;

        if (action.state == ActionState.Doing) {
            isFetching = true;
            var hash = Object.assign({}, state.gamedata.hash, { isFetching });
            let gamedata = Object.assign({}, state['gamedata'], { hash });
            return Object.assign({}, state, { gamedata });
        }

        const resp = action.response;
        if (resp && resp.code == 0) {

            let hash = state.gamedata.hash;
            const data = resp.data;
            if (data) {
                let groupData = hash[data.GroupId] || {};
                groupData[data.Qh] = data;

                const updateHash = {
                    isFetching
                };
                updateHash[data.GroupId] = groupData;

                hash = Object.assign({}, hash, updateHash);

                let gamedata = Object.assign({}, state['gamedata'], {
                    hash
                });
                return Object.assign({}, state, {
                    gamedata
                });
            }
        }

        let gamedata = Object.assign({}, state['gamedata'], { hash: { isFetching } });
        return Object.assign({}, state, { gamedata });
    }

    static _reduce_game_data_history(state, action) {
        let isFetching = false;

        if (action.state == ActionState.Doing) {
            isFetching = true;
            var history = {
                isFetching,
                count: 0,
                pageIndex: 0,
                data: []
            };
            let gamedata = Object.assign({}, state['gamedata'], { history });
            return Object.assign({}, state, { gamedata });
        }

        const resp = action.response;
        if (resp && resp.code == 0) {
            var history = {
                isFetching,
                count: resp.data.TotalCount,
                pageIndex: resp.data.PageIndex,
                data: resp.data.Data
            };
            let gamedata = Object.assign({}, state['gamedata'], { history });
            return Object.assign({}, state, { gamedata });
        }

        let gamedata = Object.assign({}, state['gamedata'], { history: { isFetching } });
        return Object.assign({}, state, { gamedata });
    }

    static _reduce_current_game_data(state, action) {
        const resp = action.response;
        if (resp && resp.code == 0) {

            if (resp.data) {
                var server_time = Utils.CalibrateServerTime(resp.data.ServerTime, resp.data.ClientTime);
                Utils.SaveServerTimeDiff(server_time);
            }

            resp.data['OpenTime'] = new Date(resp.data['OpenTime']);
            resp.data['EndTime'] = new Date(resp.data['EndTime']);
            resp.data['StartTime'] = new Date(resp.data['StartTime']);

            let current = {
                current: resp.data
            };
            let gamedata = Object.assign({}, state['gamedata'], current);
            return Object.assign({}, state, { gamedata });
        }
        return state;
    }

    static _reduce_prev_game_data(state, action) {
        if (action.state == ActionState.Doing) {
            if (state['gamedata'].prev.Group && state['gamedata'].prev.Group != action.group) {
                let prev = { prev: {} };
                let gamedata = Object.assign({}, state['gamedata'], prev);
                return Object.assign({}, state, {
                    gamedata
                });
            }
        } else {
            const resp = action.response;
            if (resp && resp.code == 0) {
                let prev = {
                    prev: resp.data
                };
                let gamedata = Object.assign({}, state['gamedata'], prev);
                return Object.assign({}, state, {
                    gamedata
                });
            }
        }
        return state;
    }

    static _reduce_next_game_data(state, action) {
        const resp = action.response;
        if (resp && resp.code == 0) {
            var server_time = Utils.CalibrateServerTime(resp.data.ServerTime, resp.data.ClientTime);
            Utils.SaveServerTimeDiff(server_time);

            resp.data['OpenTime'] = new Date(resp.data['OpenTime']);
            resp.data['EndTime'] = new Date(resp.data['EndTime']);
            resp.data['StartTime'] = new Date(resp.data['StartTime']);


            let next = {
                next: resp.data
            };
            let gamedata = Object.assign({}, state['gamedata'], next);
            return Object.assign({}, state, { gamedata });
        }
        return state;
    }

    static _reduce_unopen_game_data(state, action) {
        let isFetching = false;

        if (action.state == ActionState.Doing) {
            isFetching = true;
            var unopen = {
                isFetching
            };
            let gamedata = Object.assign({}, state['gamedata'], { unopen });
            return Object.assign({}, state, { gamedata });
        }

        const resp = action.response;
        if (resp && resp.code == 0) {

            let data = { isFetching: false };
            data[action.group] = resp.data;

            let unopen = Object.assign({}, state.gamedata.unopen, data);

            let gamedata = Object.assign({}, state['gamedata'], { unopen });
            return Object.assign({}, state, { gamedata });
        }
        return state;
    }

    /**
     * 游戏当前期结束
     * @param  {[type]} state  [description]
     * @param  {[type]} action [description]
     * @return {[type]}        [description]
     */
    static _reduce_current_game_closed(state, action) {
        let { prev, current, next } = state.gamedata;
        let data = {
            prev: current,
            current: next,
            next: null
        }
        let gamedata = Object.assign({}, state['gamedata'], data);
        return Object.assign({}, state, { gamedata });
    }

    static _reduce_game_data_open(state, action) {
        const resp = action.response;
        if (resp && resp.code == 0) {
            let { prev, history } = state.gamedata;
            if (prev.Group == resp.data.Group && resp.data.Qh >= prev.Qh) {
                var new_prev = Object.assign({}, resp.data, { Result: resp.data.Results.join(",") });
                var history_data = [new_prev, ...history.data];

                let data = {
                    prev: new_prev,
                    history: Object.assign({}, history, { data: history_data })
                };

                let gamedata = Object.assign({}, state['gamedata'], data);
                return Object.assign({}, state, { gamedata });
            }
        }
        return state;
    }

    static reduce_game_data_trend(state, action) {
        let isFetching = action.state == ActionState.Doing;
        const resp = action.response;
        if (resp && resp.code == 0) {
            let trenddata = {
                isFetching: false,
                data: resp.data.GameResults
            }
            let statisticsdata = {
                isFetching: false,
                data: resp.data.GameDataStatistics
            }
            //let statisticsdata = resp.data.GameDataStatistics
            return Object.assign({}, state, { trenddata, statisticsdata });
        }
        return Object.assign({}, state, {
            trenddata: {
                isFetching
            },
            statisticsdata: {
                isFetching
            }
        });
        return state;
    }

    static reduce_game_group(state, action) {
        const resp = action.response;
        if (resp && resp.code == 0) {
            let gamegroup = {
                Name: resp.data.Name,
                MinNum: resp.data.MinNum,
                MaxNum: resp.data.MaxNum,
                NumCount: resp.data.NumCount
            }
            return Object.assign({}, state, { gamegroup });
        }
        return state;
    }
}

module.exports = GameReducer;
