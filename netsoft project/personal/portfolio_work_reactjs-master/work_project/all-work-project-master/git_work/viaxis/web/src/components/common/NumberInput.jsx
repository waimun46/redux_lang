import React from 'react';
import { Button, Icon, Input } from "antd";

class NumberInput extends React.Component{
    
    constructor(props) {
        super(props);
        const {value,step,min,max} = props;
        this.state = {
            value: value || 1,
            step: step || 1,
            min: min || 0,
            max: max || null,
        }
    }

    /* ---------------------------- 系统函数 ------------------------------*/

    componentWillReceiveProps(nextProps) {
        const { value, max } = nextProps;
        if (value != this.state.value) {
            this.setState({value});
        }
        if(max!=this.state.max)
            this.setState({max});
    }


    shouldComponentUpdate(nextProps, nextState) {
        return nextState != this.state;
    }

    componentDidUpdate(prevProps, prevState) {

        // if (prevState.value != this.state.value) {
        //     this.props.onChange && this.props.onChange(this.state.value);
        // }
    }

    /* ---------------------------- 自定义函数 ------------------------------*/

    change(value){
        this.props.onChange && this.props.onChange(value);
    }

    /* ---------------------------- Events ------------------------------*/

    onValueUp() {
        let { value, step, max } = this.state;
        value += step;
        if (value <= max || !max){
            this.setState({value});
            this.change(value);
        }
    }

    onValueDown() {
        let { value, step, min } = this.state;
        value -= step;
        if (value >= min){
            this.setState({value});
            this.change(value);
        }
    }

    onValueChange(e) {
        var val = e.target.value;
        if (/^\d+$/.test(val)){
            let { max } = this.state;
            const value = parseInt(val);
            if (value <= max || !max){
                this.setState({value});
                this.change(value);
            }
        }
        else if (val == ""){
            this.setState({value:this.state.min});
            this.change(this.state.min);
        }
    }

    /* ---------------------------- Renders ------------------------------*/
    render() {
        const { width,height,className,disable } = this.props;
        const style = {};
        if(width)
            style['width'] = width;
        if(height)
            style['height'] = height;
        return (
            <span className={"number-input-wrap "+(className||"")} style={style} disabled={disable}>
                <span onClick={this.onValueDown.bind(this)}><Icon type="minus" /></span>
                <input value={this.state.value} onChange={this.onValueChange.bind(this)} disabled={disable} />
                <span onClick={this.onValueUp.bind(this)}><Icon type="plus" /></span>
            </span>
        );
    }

}

NumberInput.propTypes = {
    /**
     * 默认值
     * @type {[type]}
     */
    defaultValue: React.PropTypes.number,
    step: React.PropTypes.number,
    min: React.PropTypes.number,
    max: React.PropTypes.number,
    onChange: React.PropTypes.func
}

module.exports = NumberInput;
