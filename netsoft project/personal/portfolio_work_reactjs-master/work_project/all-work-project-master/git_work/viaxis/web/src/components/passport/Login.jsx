import React from 'react';
import { connect } from 'react-redux';
import { browserHistory, Link } from 'react-router'
import { message, Row, Col, Button, Form, Input, Checkbox } from 'antd';
import { ActionState } from "../../app_constants";
import { login } from "../../actions/passport";
import Auth from "../../auth";
import * as Utils from "../../utils/common";
import * as RenderHelper from "../../utils/render_helper";
import Layout from './Layout';

import getSocket from "../../websocket";

const FormItem = Form.Item;
const Message = message;

class Login extends React.Component {

    constructor(props) {
        super(props);
        this.captchaUrl = RenderHelper.getCaptchaImgUrl();
        this.state = {
            loading: false
        }
    }

    /* ---------------------------- Events ------------------------------*/

    handleSubmit(e) {
        const { dispatch } = this.props;
        e.preventDefault();
        this.props.form.validateFields((errors, values) => {
            if (errors) return;
            this.setState({ loading: true });
            dispatch(login(values.username, values.password, values.captcha)).then(act => {
                this.setState({ loading: false });

                if (act.state == ActionState.Failure) {
                    Message.error(act.error);
                } else if (act.state == ActionState.Success) {
                    const resp = act.response;
                    if (resp.code == 0) {
                        Auth.login(resp.data);
                        const ws = getSocket()
                        if (ws && ws.conn && ws.conn.readyState == 1)
                            ws.conn.close();
                        browserHistory.push("/bet");
                    } else {
                        this.refreshCaptcha();
                        Message.error(resp.message);
                    }
                }
            });
        });
    }

    handleReset(e) {
        e.preventDefault();
        this.props.form.resetFields();
    }

    refreshCaptcha() {
        let captcha = this.props.form.getFieldInstance("captcha");
        captcha.refs.input.value = "";
        captcha.refs.input.focus();
        this.refs.captcha_img.src = RenderHelper.getCaptchaImgUrl();
    }

    /* ---------------------------- Renders ------------------------------*/

    render() {
        const { getFieldDecorator } = this.props.form;
        return (
            <Layout>
                <div className="passport-box">
                    <div className="passport-box-header fn-clear">
                        <h2 className="fn-left">登陆</h2>
                        <span className="fn-right">
                            还没有账户? <Link to="/register">立即注册</Link>
                        </span>
                    </div>
                    <div className="passport-box-body">
                        <Form className="passport-box-body-form" horizontal onKeyPress={(e) => { if (e.key == "Enter") this.handleSubmit(e); }}>
                            <div className="passport-box-body-content">
                                <FormItem>
                                    {getFieldDecorator('username', {
                                        rules: [{
                                            required: true, message: '请输入用户名'
                                        }],
                                    })(
                                        <Input type="text" className="input-user" autoComplete="off" placeholder="用户名/邮箱" />
                                    )}
                                </FormItem>
                                <FormItem>
                                    {getFieldDecorator('password', {
                                        rules: [{ required: true, message: "请输入登陆密码" }],
                                    })(
                                        <Input type="password" className="input-password" placeholder="密码" />
                                    )}
                                </FormItem>
                                <FormItem>
                                    <Row gutter={8} className="underline">
                                        <Col span={12}>
                                            {getFieldDecorator('captcha', {
                                                rules: [{ required: true, message: "请输入验证码" }],
                                            })(
                                                <Input type="text" className="input-captcha" autoComplete="off" maxLength="4" placeholder="验证码" />
                                            )}
                                        </Col>
                                        <Col span={12}>
                                            <img src={this.captchaUrl} ref="captcha_img" className="img-captcha" onClick={this.refreshCaptcha.bind(this)} />
                                        </Col>
                                    </Row>
                                </FormItem>
                            </div>
                            <div className="passport-box-body-content" style={{ paddingTop: 16 }}>
                                <FormItem>
                                    {getFieldDecorator('agreement', {
                                        valuePropName: 'checked',
                                        initialValue: true,
                                        rules: [{
                                            required: true, validator: (rule, value, callback) => {
                                                !value ? callback(rule.message) : callback();
                                            }, message: "请阅读并同意服务条款与免责条约"
                                        },],
                                    })(
                                        <Checkbox>阅读并同意<a>《服务条款》</a>与<a>《免责条约》</a></Checkbox>
                                    )}
                                    <br />
                                </FormItem>
                                <Button className="passport-submit" loading={this.state.loading} onClick={this.handleSubmit.bind(this)}>登&nbsp;&nbsp;&nbsp;&nbsp;陆</Button>
                                <br /><br />
                                <Link to="/forget" className="fn-right">忘记密码?</Link>
                            </div>
                        </Form>
                    </div>
                </div>
            </Layout>
        );
    }
}

module.exports = connect(state => ({
    data: state.passport
}))(Form.create()(Login));
