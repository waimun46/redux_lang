import React from 'react';
import ReactDOM from 'react-dom';
import { browserHistory, Router } from 'react-router';
import { Provider } from 'react-redux'
import configureStore from './configure_store'
import routes from "./routes";

import getSocket from "./websocket";
getSocket();

const store = configureStore()

// import Perf from 'react-addons-perf'

class Root extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <Router history={browserHistory} routes={routes} />
      </Provider>
    )
  }
}

// Perf.start()
ReactDOM.render(<Root />,
  document.getElementById('root'));

// Perf.stop()

// let measurements = Perf.getLastMeasurements()
// Perf.printInclusive(measurements)

if (module.hot)
  module.hot.accept();
