import { combineReducers } from 'redux';

const rootReducer = combineReducers({
    passport: require("./reducers/passport").reduce,
    game: require("./reducers/game").reduce,
    bill: require("./reducers/bill").reduce,
    bank: require("./reducers/bank").reduce,
    report: require("./reducers/report").reduce,
    article: require("./reducers/article").reduce,
    notice: require("./reducers/notice").reduce,
    payment: require("./reducers/payment").reduce,
});

export default rootReducer;
