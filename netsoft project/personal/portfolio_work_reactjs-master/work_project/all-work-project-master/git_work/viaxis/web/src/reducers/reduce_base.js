/**
 * Action 格式说明：
 * {
 * 		type:str, 					// 类型
 * 		state:enums.ActionState,	// 状态
 * 		data:object,				// 数据
 * 		code:int,					// 状态码
 * 		message:str 				// 说明
 * }
 *
 * State 格式说明 :
 * {
 * 	// 这是最外层的State
 * 	passport:{
 * 		// 这是第一层State
 * 		userinfo:{
 * 			// 这是具体State
 * 			isFetching: bool 	// 是否更新中，
 * 			data:object 		// 最终数据
 * 		}
 * 		...
 * 	}
 * }
 */
import { ActionState } from "../app_constants";

export default class ReduceBase {

    static _common_success(state, action) {

        const resp = action.response;
        if (resp.code == 0) return state;

        return ReduceBase._process_failure(state, action);
    }

    static _process_failure(state, action) {
        const resp = action.response;
        action.error = resp.message;
        action.error_code = resp.code;
        return Object.assign({}, state, {
            isFetching: false
        });
    }

    /**
     * 处理分页数据集通用方法
     * @param  {[type]} state  [description]
     * @param  {[type]} action [description]
     * @param  {[type]} name   [description]
     * @return {[type]}        [description]
     */
    static _process_page(state, action, name) {
        let isFetching = action.state == ActionState.Doing;
        const resp = action.response;
        if (resp && resp.code == 0) {
            let data = {
                isFetching,
                count: resp.data && resp.data.TotalCount,
                pageIndex: resp.data && resp.data.PageIndex,
                data: resp.data && resp.data.Data
            };

            let pack = {};
            pack[name] = Object.assign({}, state[name], data);
            return Object.assign({}, state, pack);

        } else {
            let pack = {};
            pack[name] = Object.assign({}, state[name], { isFetching });
            return Object.assign({}, state, pack);
        }
    }

    static _process_list(state, action, name) {
        let isFetching = action.state == ActionState.Doing;
        const resp = action.response;
        if (resp && resp.code == 0) {
            let data = {
                isFetching,
                data: resp.data
            };

            let pack = {};
            pack[name] = data;
            return Object.assign({}, state, pack);
        }

        let pack = {};
        pack[name] = Object.assign({}, state[name], { isFetching });
        return Object.assign({}, state, pack);
    }
};
