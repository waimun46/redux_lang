import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import { Modal, Icon } from 'antd';

import BasicComponent from "../../common/BasicComponent";
import Countdown from './Countdown';

import * as GameAction from "../../../actions/game";
import Config from "../../../config";
import * as Utils from "../../../utils/common";
import * as RenderHelper from "../../../utils/render_helper";

class GameSummary extends BasicComponent {
    constructor(props) {
        super(props);
    }

    /* ----------------- Sys ----------------- */ 

    componentWillMount() {
        this.getGameData(this.props);
    }

    componentWillReceiveProps(nextProps) {
        
        if (nextProps.Group != this.props.Group || this.props.gamedata.current.Qh != nextProps.gamedata.current.Qh){
            this.getGameData(nextProps);
        }
        
    }

    /* ----------------- Functions ----------------- */

    getGameData(props) {
        this.props.dispatch(GameAction.getGameDataCurrent(props.Group));
        this.props.dispatch(GameAction.getGameDataPrev(props.Group));
    }

    /* ----------------- Events ------------------ */

    onBetTimeEnd(qh){

        RenderHelper.playSound("/stopBet.mp3");

        if (this.modal) return;

        this.modal = Modal.warning({
            title: "注意!!!",
            content: `第${qh}期投注时间已结束，投注内容将进入到下一期！`,
            onOk: function() {
                if (this.modal)
                    this.modal = null;
            }.bind(this)
        });

    }

    onWaitTimeEnd(){
        RenderHelper.playSound("/acceptBet.mp3");
       
    }

    onTick(seconds, acceptBet) {
        if (acceptBet && seconds == 10) {
            RenderHelper.playSound("/10s.mp3");
        } else if (acceptBet && seconds <= 5 && seconds > 0) {
            RenderHelper.playSound("/coundown.mp3");
        }
    }

    /* ----------------- Render ------------------ */

    render() {
        const { Group } = this.props;
        const { prev, current, history } = this.props.gamedata;
        const numCount = Config.GameDataNum[Group];
        const isOpened = prev && Utils.isNonEmptyString(prev.Result);

        
        let ballProps = {};
        if (numCount <= 5) ballProps["className"] = "big";
        else if (numCount <= 8) ballProps["className"] = "middle";
        
        return (
            <div className="game-summary">

                <div className="game-summary-logo">
                    <i className={`iconfont icon-${Group}`} />
                </div>

                <Countdown Group={this.props.Group} onTick={this.onTick.bind(this)} 
                    onBetTimeEnd={this.onBetTimeEnd.bind(this)} 
                    onWaitTimeEnd={this.onWaitTimeEnd.bind(this)}/>

                <div className="game-summary-last-open">
                    <div className="game-summary-last-open-qh">
                        第 <b>{prev&&prev.Group==this.props.Group?prev.Qh:<span><Icon type="loading" /> loading...</span>}</b> 期
                        <p>开奖号码</p>
                    </div>
                    <ul className={(isOpened ? "" : "opening ") + "game-summary-last-open-nums"}>
                        {
                            isOpened
                            ?
                             prev.Result.split(",").map((x, i) =><li {...ballProps} key={i}>{x}</li>)
                            :
                            [,...Array(numCount)].map((x,i)=><li {...ballProps} key={i}>?</li>)
                        }
                    </ul>
                </div>

                <div className="game-summary-links">
                    {/* <a target="_blank" href={`/trend/${this.props.Group}`}><Icon type="area-chart" /> 号码走势</a> */}
                    <a onClick={()=>this.goto(`/trend/${this.props.Group}`)}><Icon type="area-chart" /> 号码走势</a>
                    <Link to="/"><Icon type="question" /> 游戏说明</Link>
                </div>
            </div>
        );
    }
}

GameSummary.propTypes = {
    // 游戏组
    Group: React.PropTypes.string.isRequired,
}

module.exports = connect(state => ({
    gamedata: state.game.gamedata
}))(GameSummary);
