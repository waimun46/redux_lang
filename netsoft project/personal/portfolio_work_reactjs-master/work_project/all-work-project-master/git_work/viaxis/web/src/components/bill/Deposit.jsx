import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router'
import { notification, Row, Col, Alert, Spin, Button, Tabs, Form, Select, Input, InputNumber, Icon, message } from 'antd';
import $ from "jquery";
import QRCode from 'qrcode.react'

import BasicComponent from "../common/BasicComponent";
import DashContainer from '../../containers/DashContainer';

import Config from "../../config";

import * as Passport from "../../actions/passport";
import * as Bill from "../../actions/bill";
import * as Bank from "../../actions/bank";
import * as Payment from "../../actions/payment";
import * as Constants from "../../app_constants";
import * as Utils from "../../utils/common";
import * as RenderHelper from "../../utils/render_helper";
import * as Enums from "../../app_enum";

class TTPDeposit extends BasicComponent{

    constructor(props) {
        super(props);
        this.com_bank_id = 0,
        this.state = {
            loading:false,
        };
    }

    /* ---------------------------- System Functions ------------------------------*/
    componentWillMount() {
        this.queryBank();
        this.queryDepositInfo();
    }

    /* ---------------------------- Functions ------------------------------*/

    queryBank(){
        this.props.dispatch(Bank.getDepositBank(Enums.BankType.TTP));
    }

    queryDepositInfo(){
        this.props.dispatch(Bill.getDepositInfo());
    }

    /* ---------------------------- Events ------------------------------*/
    handleDeposit(e){
        e.preventDefault();

        const { dispatch } = this.props;

        if(this.state.loading) return;

        this.props.form.validateFields((errors, values) => {
            if (errors) return;

            dispatch(Bill.getDepositData(values.amount,this.com_bank_id)).then(act=>{
                this.setState({loading:false});
                var resp = act.response;
                if(resp.code == 0 ){
                    $('.deposit-ips-body-pre').remove();
                    $('.deposit-ips-body-confirm').append(resp.data);
                    $('.deposit-ips-body-confirm').toggle();

                    this.refs.depositamount.innerText = values.amount;
                    this.refs.depositbank.innerHTML = '<span><i class="iconfont icon-'+ values.bank.title +'"></i>'+ Constants.BankName[values.bank.title] +'</span>';
                }else{
                    message.warning(resp.message);
                }
            });    
        });
    }

    handleComBank(){
        if(this.com_bank_id != this.props.depositbank.data[0].com_bank_id)
            this.com_bank_id = this.props.depositbank.data[0].com_bank_id;
    }

    /* ---------------------------- Renders ------------------------------*/
    render(){
        const { getFieldDecorator } = this.props.form;

        const formItemLayout = {
            labelCol: { span: 6 },
            wrapperCol: { span: 12 },
        };

        const tailFormItemLayout = {
            wrapperCol: {
                span: 8,
                offset: 6,
            },
        };

        const {depositbank,deposit} = this.props;
        const minAmt = deposit.info ? deposit.info.Min : 0;
        const maxAmt = deposit.info ? deposit.info.Max : 0;

        const depositWarn = "当前无可充值银行，请联系客服!";

        return (
            <div className="wrapper800">
                { depositbank.data ?
                    <div>
                        <div className="deposit-ips-body-pre">
                            <Form autoComplete="off">
                                <Form.Item label="充值银行" {...formItemLayout}>
                                    {                          
                                        <Row>
                                            <Col span={18}>
                                                {getFieldDecorator('bank', {
                                                    rules: [{
                                                        required: true, message: '请选择您要充值的银行开户行'
                                                    }],
                                                })(
                                                    <Select placeholder="请选择充值银行" notFoundContent="暂无充值银行可选择" labelInValue onChange={this.handleComBank.bind(this)}>
                                                        {(depositbank.data || []).map(it=><Select.Option key={it.value} value={it.value} title={it.name}><i className={`iconfont icon-${it.name}`}></i> {Constants.BankName[it.name]}</Select.Option>)}
                                                    </Select>
                                                )}
                                            </Col>
                                        </Row>
                                    }
                                </Form.Item>
                                <Form.Item label="充值金额" {...formItemLayout}>
                                    <Row>
                                        <Col span={18}>
                                        {getFieldDecorator('amount', {
                                            rules: [{
                                                required: true, message: '请输入充值金额'
                                            },{
                                                type: "number",
                                                min: minAmt, max: maxAmt,
                                                message: `提现金额必须大于等于${minAmt}小于等于${maxAmt}`,
                                                transform(value) {
                                                    return parseInt(value);
                                                }
                                            }],
                                            validateTrigger: "onBlur"
                                        })(
                                            <Input />
                                        )}
                                        </Col>
                                    </Row>
                                </Form.Item>
                                <Form.Item {...tailFormItemLayout}>
                                    <Button loading={this.state.loading} type="primary" htmlType="submit" size="large" className="fn-width100" onClick={this.handleDeposit.bind(this)}>确定</Button>
                                </Form.Item>
                            </Form>
                        </div>
                        <dl className="deposit-ips-body-confirm">
                            <Form autoComplete="off" >
                                <Form.Item {...formItemLayout} label="充值银行">
                                    <span className="ant-form-text" ref="depositbank">
                                    </span>
                                </Form.Item>
                                <Form.Item {...formItemLayout} label="充值金额">
                                    <span className="ant-form-text" ref="depositamount">
                                    </span>
                                </Form.Item>
                            </Form>
                        </dl>
                    </div>
                    : 
                    <Alert type="warning" showIcon message="充值通知" description={<p dangerouslySetInnerHTML={{__html:depositWarn}}></p>} />
                }
            </div>
        )
    }
}

class EbankDeposit extends BasicComponent{

    constructor(props) {
        super(props);
        
        this.state = {
            loading:false,
        };
    }

    componentWillMount() {
        this.queryPaymentChannelInfo();
    }


    queryPaymentChannelInfo(){
        this.props.dispatch(Payment.getPayChannelInfo(1));
    }

    handleDeposit(e){
        e.preventDefault();

        const { dispatch } = this.props;
        if(this.state.loading) return;

        this.props.form.validateFields((errors, values) => {
            if (errors) return;

            dispatch(Payment.submitPayRequest(values.amount)).then(act=>{
                //this.setState({loading:false});
                var resp = act.response;
                if(resp.code == 0 ){
                   
                   
                }else{
                    message.warning(resp.message);
                }
            });    
        });



       
    }

    /* ---------------------------- Renders ------------------------------*/
    render(){
        const formItemLayout = {
            labelCol: { span: 6 },
            wrapperCol: { span: 12 },
        };

        const tailFormItemLayout = {
            wrapperCol: {
                span: 8,
                offset: 6,
            },
        };

        const { getFieldDecorator } = this.props.form;

        const paymentChannel = this.props.paymentChannel.data;
        
        const minAmt = paymentChannel ? paymentChannel.MinAmount : 0;
        const maxAmt = paymentChannel ? paymentChannel.MaxAmount : 0;

        const rate = paymentChannel ? paymentChannel.Rate : 0;
      //  const realPayAmount = rate * 

        const depositspWarn = "当前无法使用电汇指引，请联系客服!";

        return(
            paymentChannel?
            <div className="wrapper800">
                    <div>
                        <div className="deposit-ips-body-pre">
                            <Form autoComplete="off">
             
                                <Form.Item label="充值金额" {...formItemLayout}>
                                    <Row>
                                        <Col span={12}>
                                        {getFieldDecorator('amount', {
                                            rules: [{
                                                required: true, message: '请输入充值金额'
                                            },{
                                                type: "number",
                                                min: minAmt, max: maxAmt,
                                                message: `充值金额必须大于等于${minAmt}小于等于${maxAmt}`,
                                                transform(value) {
                                                    return parseInt(value);
                                                }
                                            }],
                                            validateTrigger: "onBlur"
                                        })(
                                            <Input />
                                        )}
                                        </Col>
                                        <Col span={6}>
                                        <p> &nbsp;&nbsp;USDT</p>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <span>汇率：{rate}</span>
                                    </Row>
                                    {/* <Row>
                                        <span>实付金额：{realPayAmount}&nbsp;元</span>
                                    </Row> */}
                                </Form.Item>
                                <Form.Item {...tailFormItemLayout}>
                                    <Button type="primary" htmlType="submit" size="large" className="fn-width100" onClick={this.handleDeposit.bind(this)}>确定</Button>
                                </Form.Item>
                            </Form>
                        </div>
                       
                    </div>
        
            </div>
            :
            <Alert type="warning" showIcon message="充值通知" description={<p dangerouslySetInnerHTML={{__html:depositspWarn}}></p>} />
       ) 
        
    }
}

class SPDeposit extends BasicComponent{
    constructor(props) {
        super(props);
        this.state = {
            wechatloading:false,
            alipayloading:false,
            qrcode:""
        };
    }

    /* ---------------------------- System Functions ------------------------------*/
    componentWillMount() {
        this.queryBank();
    }

    /* ---------------------------- Functions ------------------------------*/

    queryBank(){
        this.props.dispatch(Bank.getDepositSP(Enums.BankType.SP));
    }

    /* ---------------------------- Events ------------------------------*/
    handleWechat(e){
        e.preventDefault();

        const { dispatch } = this.props;

        if(this.state.alipayloading) return;

        this.props.form.validateFields((errors, values) => {
            if (errors) return;
            this.setState({wechatloading:true});
            dispatch(Bill.getDepositQrCode(values.amount, 1, this.props.depositsp.combankid)).then(act=>{
                this.setState({wechatloading:false});
                var resp = act.response;
                if(resp.code == 0 ){
                    if(resp.data != ""){
                        this.setState({qrcode:resp.data})
                        $('.deposit-thirdpartypayment-qrcode').css('display','block')
                    }else{
                        $('.deposit-thirdpartypayment-qrcode').css('display','none')
                    }
                }else{
                    $('.deposit-thirdpartypayment-qrcode').css('display','none')
                    message.warning(resp.message);
                }
            }); 
        });
    }

    handleAliPay(e){
        e.preventDefault();

        const { dispatch } = this.props;

        if(this.state.wechatloading) return;

        this.props.form.validateFields((errors, values) => {
            if (errors) return;

            this.setState({alipayloading:true});
            dispatch(Bill.getDepositQrCode(values.amount, 2, this.props.depositsp.combankid)).then(act=>{
                this.setState({alipayloading:false});
                var resp = act.response;
                if(resp.code == 0 ){
                    if(resp.data != ""){
                        this.setState({qrcode:resp.data})
                        $('.deposit-thirdpartypayment-qrcode').css('display','block')
                    }else{
                        $('.deposit-thirdpartypayment-qrcode').css('display','none')
                    }
                }else{
                    $('.deposit-thirdpartypayment-qrcode').css('display','none')
                     message.warning(resp.message);
                }
            }); 
        });
    }


    /* ---------------------------- Renders ------------------------------*/
    render(){
        const { getFieldDecorator } = this.props.form;

        const formItemLayout = {
            labelCol: { span: 6 },
            wrapperCol: { span: 12 },
        };

        const tailFormItemLayout = {
            wrapperCol: {
                span: 8,
                offset: 6,
            },
        };

        const {deposit, depositsp} = this.props;
        
        const minAmt = deposit.info ? deposit.info.Min : 0;
        const maxAmt = deposit.info ? deposit.info.Max : 0;

        const depositspWarn = "当前无可用扫码支付，请联系客服!";

        return (
            <div className="wrapper800">
                { depositsp.combankid > 0 ?
                    <div>
                        <Form autoComplete="off">
                            <Form.Item label="充值金额" {...formItemLayout}>
                                <Row>
                                    <Col span={18}>
                                    {getFieldDecorator('amount', {
                                        rules: [{
                                            required: true, message: '请输入充值金额'
                                        },{
                                            type: "number",
                                        min: minAmt,max: maxAmt,
                                            message: `充值金额必须大于等于${minAmt}小于等于${maxAmt}`,
                                            transform(value) {
                                                return parseInt(value);
                                            }
                                        }],
                                        validateTrigger: "onBlur"
                                    })(
                                        <Input />
                                    )}
                                    </Col>
                                </Row>
                            </Form.Item>
                            <Form.Item {...tailFormItemLayout}>
                                <Button type="primary" className="deposit-thirdpartypayment-button-wechat" loading={this.state.wechatloading} onClick={this.handleWechat.bind(this)}>微信</Button>
                                <Button type="primary" className="deposit-thirdpartypayment-button-alipay" loading={this.state.alipayloading} onClick={this.handleAliPay.bind(this)}>支付宝</Button>
                            </Form.Item>
                            <Form.Item {...tailFormItemLayout}>
                                <div className="deposit-thirdpartypayment-qrcode">
                                    <QRCode value={this.state.qrcode} size={200}/>
                                </div>
                            </Form.Item>
                        </Form> 
                    </div>
                    : 
                    <Alert type="warning" showIcon message="充值通知" description={<p dangerouslySetInnerHTML={{__html:depositspWarn}}></p>} />
                }
            </div>
        )
    }
}

TTPDeposit.PropTypes = {
    depositbank: React.PropTypes.array.isRequired,
    deposit:React.PropTypes.array.isRequired,
};
const TTPWraper = connect(state => ({
    depositbank: state.bank.depositbank,
    deposit: state.bill.deposit,
}))(Form.create()(TTPDeposit));

const EbankWraper = connect(state =>({
    paymentChannel:state.payment.paymentChannel
}))(Form.create()(EbankDeposit));

SPDeposit.PropTypes = {
    deposit:React.PropTypes.array.isRequired,
    depositsp:React.PropTypes.array.isRequired,
};
const SPWraper = connect(state => ({
    deposit: state.bill.deposit,
    depositsp: state.bank.depositsp,
}))(Form.create()(SPDeposit));

class Deposit extends BasicComponent{
    constructor(props) {
        super(props);
        let type = (props.params.type || "").toLowerCase();
        if (["", "ebank", "sp"].indexOf(type) < 0)
            this.goto("/404");

        this.state = {
            type
        }
    }

    /* ---------------------------- Renders ------------------------------*/
    render(){

        const type = this.state.type;      

        return (
            <DashContainer title="充值">
                <br/><br/>
                <div className="wrapper800">
                    <br/>
                    <Tabs defaultActiveKey={type}>
                        {/* <Tabs.TabPane tab="网银充值" key="">
                            <TTPWraper />
                        </Tabs.TabPane> */}
                        {/* <Tabs.TabPane tab="电汇指引" key="ebank"> */}
                        <Tabs.TabPane tab="电汇指引" key="">
                            <EbankWraper />
                        </Tabs.TabPane>
                        {/* <Tabs.TabPane tab="扫码支付" key="sp">
                            <SPWraper />
                        </Tabs.TabPane> */}
                    </Tabs>
                     
                </div>
            </DashContainer>
        );
    }
}

module.exports = Deposit;
