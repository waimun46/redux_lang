import { ActionState } from "../app_constants";
import ReduceBase from "./reduce_base";

const defaultState = {
    profitUser: {
        isFetching: false,
        data: null
    },
    profitGameType: {
        isFetching: false,
        data: null
    },
    profitUserGameType: {
        isFetching: false,
        data: null
    },
    profitGroupGameType: {
        isFetching: false,
        data: null
    },
    profitDividend: {
        isFetching: false,
        data: null
    }
}

class ReportReducer extends ReduceBase {
    static reduce(state=defaultState, action) {
        switch (action.type) {
            case "ProfitUser":
                return ReduceBase._process_page(state, action, "profitUser");
            case "ProfitGameType":
                return ReduceBase._process_page(state, action, "profitGameType");
            case "ProfitUserGameType":
                return ReduceBase._process_page(state, action, "profitUserGameType");
            case "ProfitGroupGameType":
                return ReduceBase._process_list(state, action, "profitGroupGameType");
            case "ProfitDividend":
                return ReduceBase._process_list(state, action, "profitDividend");
            default:
                return state;
        }
    }
}

module.exports = ReportReducer;
