import React from 'react';
import { connect } from 'react-redux';
import { browserHistory, Link } from 'react-router'
import { message, Modal, Row, Col, Button, notification, Form, Input, Checkbox, Icon } from 'antd';

import Layout from './Layout';
import BasicComponent from "../common/BasicComponent";

import * as Passport from "../../actions/passport";
import Auth from "../../auth";
import { ActionState } from "../../app_constants";
import * as Utils from "../../utils/common";
import * as RenderHelper from "../../utils/render_helper";


const Message = message;
const FormItem = Form.Item;

class Register extends BasicComponent {
    constructor(props) {
        super(props);
        // this.referral = props.params.referral;
        this.referral = props.location.query.referral;
        this.captchaUrl = RenderHelper.getCaptchaImgUrl();
        this.state = {
            loading: false
        }
    }

    componentWillReceiveProps(newProps) {
        if (newProps.params != this.props.params)
            this.referral = newProps.params.referral;
    }

    /* ---------------------------- 自定义函数 ------------------------------*/

    /* ---------------------------- Events ------------------------------*/

    handleSubmit(e) {
        const { dispatch } = this.props;
        e.preventDefault();
        this.props.form.validateFields((errors, values) => {
            if (errors) return;

            this.setState({ loading: true });

            dispatch(Passport.register(values.email, values.password, values.qq, values.mobile, values.captcha, this.referral)).then((act) => {
                this.setState({ loading: false });

                const resp = act.response;

                if (act.state == ActionState.Failure) {
                    Message.error(act.error);
                } else if (act.state == ActionState.Success) {
                    const resp = act.response;
                    if (resp.code == 0) {
                        Auth.login(resp.data);
                        Modal.success({ title: "注册成功", onOk() { browserHistory.push("/bet"); } });
                    } else {
                        this.refreshCaptcha();
                        Message.error(resp.message);
                    }
                }

            });
        });
    }

    refreshCaptcha() {
        let captcha = this.props.form.getFieldInstance("captcha");
        captcha.refs.input.value = "";
        captcha.refs.input.focus();
        this.refs.captcha_img.src = RenderHelper.getCaptchaImgUrl();
    }

    /* ---------------------------- Validators ------------------------------*/

    checkPassword(rule, value, callback) {
        // the validator aplly https://github.com/yiminghe/async-validator, but no merge form to this library
        if (value && value !== rule.getFieldValue('password')) {
            callback(rule.message);
        } else {
            callback();
        }
    }

    checkEmail(rule, value, callback) {
        value && this.props.dispatch(Passport.checkUsername(value)).then(act => {
            const existed = act.response;
            if (existed) {
                callback("该邮箱已被注册!");
            }
            callback();
        });
    }

    /* ---------------------------- Renders ------------------------------*/


    render() {
        const { getFieldDecorator } = this.props.form;
        return (
            <Layout>
                <div className="passport-box">
                    <div className="passport-box-header fn-clear">
                        <h2 className="fn-left">注册</h2>
                        <span className="fn-right">
                            已有帐号? <Link to="/login">立即登陆</Link>
                        </span>
                    </div>
                    <div className="passport-box-body">
                        <Form horizontal onKeyPress={(e) => { if (e.key == "Enter") this.handleSubmit(e); }}>
                            <div className="passport-box-body-form">
                                <div className="passport-box-body-content fn-left">
                                    <FormItem>
                                        {getFieldDecorator('email', {
                                            rules: [{
                                                required: true,
                                                message: '请输入邮箱'
                                            }, {
                                                type: 'email',
                                                message: '请输入合法信箱',
                                            }, {
                                                validator: this.checkEmail.bind(this)
                                            }],
                                        })(
                                            <Input type="email" autoComplete="off" placeholder="邮箱" tabIndex="1" />
                                        )}
                                        <Icon type="mail" />
                                    </FormItem>


                                    <FormItem>
                                        {getFieldDecorator('password', {
                                            rules: [{ required: true, message: "请输入登陆密码", min: 6 }],
                                        })(
                                            <Input type="password" className="input-password" placeholder="登陆密码" tabIndex="2" />
                                        )}
                                    </FormItem>

                                    <FormItem>
                                        {getFieldDecorator('confirmPassword', {
                                            rules: [{ required: true, message: "请输入登陆密码" },
                                            { required: true, validator: this.checkPassword, message: "请确保登陆密码一致", getFieldValue: this.props.form.getFieldValue }],
                                        })(
                                            <Input type="password" className="input-password" placeholder="请再次输入登陆密码" tabIndex="3" />
                                        )}
                                    </FormItem>

                                    <FormItem>
                                        {getFieldDecorator('agreement', {
                                            valuePropName: 'checked',
                                            initialValue: true,
                                            rules: [{
                                                required: true, validator: (rule, value, callback) => {
                                                    !value ? callback(rule.message) : callback();
                                                }, message: "请阅读并同意服务条款与免责条约"
                                            },],
                                        })(
                                            <Checkbox tabIndex="7">阅读并同意<a>《服务条款》</a>与<a>《免责条约》</a></Checkbox>
                                        )}
                                        <br />
                                    </FormItem>

                                    <Button tabIndex="8" className="passport-submit" loading={this.state.loading} onClick={this.handleSubmit.bind(this)}>注&nbsp;&nbsp;&nbsp;&nbsp;册</Button>
                                </div>
                                <div className="passport-box-body-right passport-box-body-content">

                                    <FormItem>
                                        {getFieldDecorator('mobile', {
                                            rules: [{
                                                required: true, message: '请输入手机'
                                            }],
                                        })(
                                            <Input type="text" autoComplete="off" placeholder="手机" tabIndex="4" />
                                        )}
                                        <Icon type="mobile" />
                                    </FormItem>
                                    <FormItem>
                                        {getFieldDecorator('qq', {
                                            rules: [{
                                                required: true, message: '请输入QQ'
                                            }],
                                        })(
                                            <Input type="text" autoComplete="off" placeholder="QQ" tabIndex="5" />
                                        )}
                                        <Icon type="github" />
                                    </FormItem>
                                    <FormItem>
                                        <Row gutter={8} className="underline">
                                            <Col span={12}>
                                                {getFieldDecorator('captcha', {
                                                    rules: [{ required: true, message: "请输入验证码" }],
                                                })(
                                                    <Input type="text" className="input-captcha" autoComplete="off" maxLength="4" placeholder="验证码" tabIndex="6" />
                                                )}
                                            </Col>
                                            <Col span={12}>
                                                <img src={this.captchaUrl} ref="captcha_img" className="img-captcha" onClick={this.refreshCaptcha.bind(this)} />
                                            </Col>
                                        </Row>
                                    </FormItem>
                                </div>
                            </div>
                        </Form>
                    </div>
                </div>
            </Layout>
        );
    };
}

module.exports = connect(state => ({
    data: state.passport
}))(Form.create()(Register));
