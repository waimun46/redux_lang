import React from 'react';
import { connect } from 'react-redux';
import { browserHistory, Link } from 'react-router'

import { Modal, Menu, Icon, Button } from 'antd';

import BasicComponent from "./BasicComponent";
import Auth from "../../auth";
import * as Enums from "../../app_enum";

class SliderMenu extends BasicComponent {
    constructor(props) {
        super(props);
    }

    handleClick(e) {
        browserHistory.push(e.key);
    }

    render() {
        const { user } = this.props;
        const userinfo = user.data;

        let isMember = userinfo && userinfo.UserKind == Enums.UserKind.Member;

        let userProfitkey = userinfo? '/profit/user/'+ userinfo.UserId :  '/profit/user/';
        return (
            <aside className="ant-layout-sider">
                <div className="ant-layout-logo">
                    <Link to="/">
                        <img src="/images/logo.svg" />
                    </Link>
                </div>
                <Menu mode="inline" onClick={this.handleClick} theme="dark">
                    <Menu.Item key="/passport/profile" >
                        <Icon type='user' />
                        <span className="nav-text">用戶中心</span>
                    </Menu.Item>
                    {
                        isMember
                            ?
                            ""
                            :
                            <Menu.Item key="/user" >
                                <Icon type='team' />
                                <span className="nav-text">团队管理</span>
                            </Menu.Item>
                    }
                    <Menu.Item key="/deals" >
                        <Icon type='pie-chart' />
                        <span className="nav-text">下注列表</span>
                    </Menu.Item>
                    <Menu.Item key="/billflow" >
                        <Icon type='line-chart' />
                        <span className="nav-text">流水历史</span>
                    </Menu.Item>

                    <Menu.Item key="/withdraw/history" >
                        <Icon type="pay-circle" />
                        <span className="nav-text">提现记录</span>
                    </Menu.Item>
                    <Menu.Item key="/transfer/history" >
                        <Icon type="swap" />
                        <span className="nav-text">转账记录</span>
                    </Menu.Item>

                    <Menu.SubMenu key="profit" title={<span><Icon type="bar-chart" /><span className="nav-text">盈亏报表</span></span>}>
                        {/* <Menu.Item key="/profit/user" > */}
                        <Menu.Item key={userProfitkey} >
                            <span className="nav-text">用户盈亏</span>
                        </Menu.Item>
                        <Menu.Item key="/profit/game" >
                            <span className="nav-text">游戏盈亏</span>
                        </Menu.Item>
                        {
                            isMember
                                ?
                                ""
                                :
                                <Menu.Item key="/profit/dividend" >
                                    <span className="nav-text">游戏分红</span>
                                </Menu.Item>
                        }

                    </Menu.SubMenu>
                    <Menu.Item key="/bank" >
                        <Icon type='credit-card' />
                        <span className="nav-text">银行卡管理</span>
                    </Menu.Item>
                    <Menu.Item key="/passport/password" >
                        <Icon type='lock' />
                        <span className="nav-text">密码修改</span>
                    </Menu.Item>
                </Menu>
                <div className="ant-aside-action">
                    <Button type="primary" onClick={this.logout.bind(this)}>退出</Button>
                </div>
            </aside>
        );
    }
}
//export default connect()(SliderMenu);

export default connect(state => ({
    user: state.passport.userinfo,
}))(SliderMenu);
