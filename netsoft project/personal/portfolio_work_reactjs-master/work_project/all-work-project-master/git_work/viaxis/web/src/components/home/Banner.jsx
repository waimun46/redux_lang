import React, { PropTypes } from 'react';
import { Button, Icon } from 'antd';
import QueueAnim from 'rc-queue-anim';
import TweenOne from 'rc-tween-one';
import BannerAnim, { Element } from 'rc-banner-anim';
import 'rc-banner-anim/assets/index.css';
import OverPack from 'rc-scroll-anim/lib/ScrollOverPack';

const BgElement = Element.BgElement;

class Banner extends React.Component {
    render() {
        const childrenData = [
            { children: { title: {}, content: {}, button: {} } },
            { children: { title: { children: 'https://zos.alipayobjects.com/rmsportal/HqnZZjBjWRbjyMr.png' }, content: { children: '一个高效的页面动画解决方案' }, button: { children: 'Learn More' } } },
        ];
        const defaultImg = [
            '/banner/banner-one.jpg',
            'https://zos.alipayobjects.com/rmsportal/xHxWkcvaIcuAdQl.jpg',
        ]

        return (
            <TweenOne animation={{ opacity: 0, type: 'from' }}>
                <OverPack {...this.props} hideProps={{ icon: { reverse: true } }}>
                    {this.renderBanner()}
                    <TweenOne
                        animation={{ y: '-=20', yoyo: true, repeat: -1, duration: 1000 }}
                        className={`${this.props.className}-icon`}
                        style={{ bottom: 40 }}
                        key="icon"
                    >
                        <Icon type="down" />
                    </TweenOne>
                </OverPack>
            </TweenOne>
        );
    }

    renderBanner() {
        return (
            <BannerAnim autoPlay>
                <Element key="firstPage" prefixCls="banner-user-elem">
                    <BgElement
                        className="bg"
                        key="bg"
                        style={{
                            backgroundImage: `url(images/banner/banner-one.jpg)`,
                            backgroundSize: 'cover',
                            backgroundPosition: 'center',
                        }}
                    />
                    <QueueAnim
                        type={['bottom', 'top']} delay={200}
                        className={`${this.props.className}-title`} key="text">

                    </QueueAnim>
                </Element>

                <Element key="secondPage" prefixCls="banner-user-elem">
                    <BgElement
                        className="bg"
                        key="bg"
                        style={{
                            backgroundImage: `url(https://zos.alipayobjects.com/rmsportal/xHxWkcvaIcuAdQl.jpg)`,
                            backgroundSize: 'cover',
                            backgroundPosition: 'center',
                        }}
                    />
                    <QueueAnim
                        type={['bottom', 'top']} delay={200}
                        className={`${this.props.className}-title`} key="text">

                        <span className="logo" key="logo">
                            <img width="100%" src="https://zos.alipayobjects.com/rmsportal/HqnZZjBjWRbjyMr.png" />
                        </span>

                        <p key="content">
                            一个高效的页面动画解决方案
                        </p>
                        <Button type="ghost" key="button">
                            Learn More
                        </Button>
                    </QueueAnim>
                </Element>

            </BannerAnim>
        );
    }
}

Banner.propTypes = {
    className: PropTypes.string,
};

Banner.defaultProps = {
    className: 'home-banner',
};

export default Banner;
