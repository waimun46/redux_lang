import { ActionState } from "../app_constants";
import ReduceBase from "./reduce_base";

const defaultState = {
    paymentChannel: {
		isFetching: false,
		data: []
    },
    payRequestResp: {
		isFetching: false,
		data: []
	},

}
class PaymentReducer extends ReduceBase {
    static reduce(state = defaultState, action){
        switch (action.type) {
            case "PayChannelInfo":
                return PaymentReducer._reduce_paymentChannelInfo(state, action);
            case "submitPayRequest":
                return PaymentReducer._reduce_submitPayRequest(state, action);
                
            default:
                return state;
        
        
        
        }
    }

    static _reduce_paymentChannelInfo(state, action) {
		let isFetching = action.state == ActionState.Doing;
		const resp = action.response;
		if (resp && resp.code == 0) {
			let paymentChannel = {
				isFetching,
				data: resp.data
			};
			return Object.assign({}, state, { paymentChannel });
		}
		return Object.assign({}, state, {
			paymentChannel: {
				isFetching
			}
		});
    }
    
    static _reduce_submitPayRequest(state, action) {
		let isFetching = action.state == ActionState.Doing;
		const resp = action.response;
		if (resp && resp.code == 0) {
			let payRequestResp = {
				isFetching,
				data: resp.data
			};
			return Object.assign({}, state, { payRequestResp });
		}
		return Object.assign({}, state, {
			payRequestResp: {
				isFetching
			}
		});
	}
};

module.exports = PaymentReducer;