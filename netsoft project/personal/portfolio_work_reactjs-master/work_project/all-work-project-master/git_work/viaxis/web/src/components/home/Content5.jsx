import React from 'react';
import TweenOne from 'rc-tween-one';
import OverPack from 'rc-scroll-anim/lib/ScrollOverPack';
import { Tabs } from 'antd';

const TabPane = Tabs.TabPane;

class Content extends React.Component {

  static propTypes = {
    className: React.PropTypes.string,
    id: React.PropTypes.string,
    dataSource: React.PropTypes.object,
  };

  static defaultProps = {
    className: 'content6',
  };

  getBlockChildren = (item, i) =>
    (<TabPane
      key={i}
      tab={(
        <span
          className={`${this.props.className}-tag`}
        >
          <i><img src={item.children.icon.children} width="100%" /></i>
          {item.children.tag.children}
        </span>)}
    >
      <TweenOne
        animation={{ x: -30, delay: 300, opacity: 0, type: 'from' }}
        className={`${this.props.className}-text`}
        style={item.children.content.style}
        dangerouslySetInnerHTML={{ __html: item.children.content.children }}
      />
      <TweenOne
        animation={{ x: 30, delay: 400, opacity: 0, type: 'from' }}
        className={`${this.props.className}-img`}
        style={item.children.img.style}
      >
        <img src={item.children.img.children} width="100%" />
      </TweenOne>
    </TabPane>);

  render() {
    const dataSource = [
      { children: { tag: { children: 'PHONE' }, icon: { children: 'https://zos.alipayobjects.com/rmsportal/XnzcslQvRoBHMHd.svg' }, content: { children: '<h3>技术</h3>\n丰富的技术组件，简单组装即可快速搭建金融级应用，丰富的技术组件，简单组装即可快速搭建金融级应用。\n<h3>融合</h3>\n解放业务及技术生产力，推动金融服务底层创新，推动金融服务底层创新。解放业务及技术生产力，推动金融服务底层创新。\n<h3>开放</h3>\n符合金融及要求的安全可靠、高可用、高性能的服务能力，符合金融及要求的安全可靠、高可用、高性能的服务能力。' }, img: { children: 'https://zos.alipayobjects.com/rmsportal/xBrUaDROgtFBRRL.png' } } },
      { children: { tag: { children: 'TABLET' }, icon: { children: 'https://zos.alipayobjects.com/rmsportal/XnzcslQvRoBHMHd.svg' }, content: { children: '<h3>技术</h3>\n丰富的技术组件，简单组装即可快速搭建金融级应用，丰富的技术组件，简单组装即可快速搭建金融级应用。\n<h3>融合</h3>\n解放业务及技术生产力，推动金融服务底层创新，推动金融服务底层创新。解放业务及技术生产力，推动金融服务底层创新。\n<h3>开放</h3>\n符合金融及要求的安全可靠、高可用、高性能的服务能力，符合金融及要求的安全可靠、高可用、高性能的服务能力。' }, img: { children: 'https://zos.alipayobjects.com/rmsportal/xBrUaDROgtFBRRL.png' } } },
      { children: { tag: { children: 'DESKTOP' }, icon: { children: 'https://zos.alipayobjects.com/rmsportal/XnzcslQvRoBHMHd.svg' }, content: { children: '<h3>技术</h3>\n丰富的技术组件，简单组装即可快速搭建金融级应用，丰富的技术组件，简单组装即可快速搭建金融级应用。\n<h3>融合</h3>\n解放业务及技术生产力，推动金融服务底层创新，推动金融服务底层创新。解放业务及技术生产力，推动金融服务底层创新。\n<h3>开放</h3>\n符合金融及要求的安全可靠、高可用、高性能的服务能力，符合金融及要求的安全可靠、高可用、高性能的服务能力。' }, img: { children: 'https://zos.alipayobjects.com/rmsportal/xBrUaDROgtFBRRL.png' } } },
    ]
    const tabsChildren = dataSource.map(this.getBlockChildren);
    return (
      <div
        {...this.props}
        className={`content-template-wrapper ${this.props.className}-wrapper`}
      >
        <OverPack
          className={`content-template ${this.props.className}`}
          hideProps={{ h1: { reverse: true }, p: { reverse: true } }}
          location={this.props.id}
        >
          <TweenOne
            animation={{ y: '+=30', opacity: 0, type: 'from' }}
            component="h1"
            key="h1"
            reverseDelay={200}
          >
            蚂蚁金融云提供专业的服务
          </TweenOne>
          <TweenOne
            animation={{ y: '+=30', opacity: 0, type: 'from', delay: 100 }}
            component="p"
            key="p"
            reverseDelay={100}
          >
            科技想象力，金融创造力
          </TweenOne>
          <TweenOne.TweenOneGroup
            key="tabs"
            enter={{ y: 30, opacity: 0, delay: 200, type: 'from' }}
            leave={{ y: 30, opacity: 0 }}
            className={`${this.props.className}-tabs`}
          >
            <Tabs key="tabs">
              {tabsChildren}
            </Tabs>
          </TweenOne.TweenOneGroup>
        </OverPack>
      </div>
    );
  }
}


export default Content;
