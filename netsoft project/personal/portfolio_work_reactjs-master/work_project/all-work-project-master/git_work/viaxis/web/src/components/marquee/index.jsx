import React from 'react';
import ReactDOM from 'react-dom';

import $ from "./marquee";
import BasicComponent from "../common/BasicComponent";




class Marquee extends BasicComponent {

    constructor(props) {
        super(props);
    }

    /* -------------------- Sys --------------------- */

    componentDidMount(){
        this.bindMarquee();
    }

    componentDidUpdate(){
        this.bindMarquee();
    }

    /* -------------------- Function ------------------------ */

    bindMarquee(){
        $(ReactDOM.findDOMNode(this)).marquee();
    }

    /* -------------------- Renders ------------------------ */    

    render(){
        return (
            <ul className="marquee">
                {this.props.children}
            </ul>
        );
    }

}

module.exports = Marquee;
