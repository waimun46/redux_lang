import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router'
import { notification, Row, Col, Alert, Spin, Button, Form, Select, Input, InputNumber, Icon } from 'antd';

import BasicComponent from "../common/BasicComponent";
import DashContainer from '../../containers/DashContainer';

import * as Passport from "../../actions/passport";
import * as Bill from "../../actions/bill";
import * as Bank from "../../actions/bank";
import * as Constants from "../../app_constants";
import * as Utils from "../../utils/common";
import * as RenderHelper from "../../utils/render_helper";


class Withdraw extends BasicComponent {

    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            // 是否已设置安全密码
            hasSafePassword: true,
        };
    }

    /* ---------------------------- System  ------------------------------*/

    componentWillMount() {
        const { cards } = this.props;
        this.getWitdhrawInfo();

        if (!cards.data || cards.data.length == 0)
            this.queryBankCards();

        // this.queryHasSafePassword();

    }

    componentWillUpdate(nextProps, nextState) {
        // 未设置安全密码
        // 由于需要绑定银行卡，而银行卡绑定必须要安全密码，所以这里可以不做安全密码检查。
        // if (nextState.hasSafePassword != this.state.hasSafePassword && !nextState.hasSafePassword) {
        //     return RenderHelper.warning("您未设置安全密码", "您需要设置安全密码后才可继续下列的操作",()=>this.goto("/passport/password/safe?redirect=/bank"));
        // }
    }

    /* ---------------------------- Functions ------------------------------*/

    queryBankCards() {
        const self = this;
        this.props.dispatch(Bank.getBankCards()).then(act => {
            this.assert(act, {
                fnOk: it => {
                    if ((!it.data || it.data.length == 0))
                        RenderHelper.warning("您未添加银行卡", "您需要添加银行卡后才可继续下列的操作", () => this.goto("/bank?redirect=/withdraw"));
                }
            });
        });
    }

    getWitdhrawInfo() {
        this.props.dispatch(Bill.getWithdrawInfo());
    }

    queryHasSafePassword() {
        this.props.dispatch(Passport.hasSafePassword()).then(it => {

            const resp = it.response;
            if (resp.code == 0) {
                this.setState({ hasSafePassword: resp.data });
            }

        });
    }

    /* ---------------------------- Events ------------------------------*/

    handleSubmit(e) {
        e.preventDefault();

        const { dispatch } = this.props;

        this.props.form.validateFields((errors, values) => {
            if (errors) return;

            this.setState({ loading: true });

            dispatch(Bill.addWithdraw(values.bank_card, values.amount, values.password)).then(act => {
                this.setState({ loading: false });
                this.assert(act, {
                    fnOk: (resp) => {
                        notification.open({
                            message: "申请成功",
                            description: <div><p>您的提交申请已提交。</p>你可以在 <a onClick={() => this.goto("/withdraw/history")}>提现历史</a> 查看当前进度。</div>,
                            icon: <Icon type="smile-circle" style={{ color: '#108ee9' }} />
                        });
                        //提现成功则刷新当前帐号余额
                        dispatch(Bill.getBalance())
                        this.props.form.resetFields();
                    }
                });
            });
        });
    }

    /* ---------------------------- Renders ------------------------------*/
    render() {
        const { withdraw } = this.props;

        const infoTipsTemplate = "提现所产生的银行手续费由平台为您免除。提现限额：最低 <b>{0}</b> 元，最高 <b>{1}</b> 元, 已提现 <b>{2}</b> 次，还可以提现 <b>{3}</b> 次。";
        const infoTips = withdraw.info ?
            Utils.format(infoTipsTemplate,
                Utils.milliFormat(withdraw.info.Min),
                Utils.milliFormat(withdraw.info.Max),
                withdraw.info.Count,
                withdraw.info.MaxCount - withdraw.info.Count) :
            Utils.format(infoTipsTemplate, "loading", "loading", "loading", "loading");

        return (
            <DashContainer title="提现" extra={<Link to="/withdraw/history">历史记录</Link>}>
                <br />
                <div className="wrapper800">
                    <Alert type="info" showIcon message="提现须知" description={<p dangerouslySetInnerHTML={{ __html: infoTips }}></p>} />
                    {this.renderForm()}
                </div>
            </DashContainer>
        );
    }

    renderForm() {
        const { getFieldDecorator } = this.props.form;

        const formItemLayout = {
            labelCol: { span: 6 },
            wrapperCol: { span: 12 },
        };

        const tailFormItemLayout = {
            wrapperCol: {
                span: 8,
                offset: 6,
            },
        };

        const { balance, withdraw, cards } = this.props;
        const minAmt = withdraw.info ? withdraw.info.Min : 0;
        const maxAmt = Math.min(balance.data || 0, withdraw.info ? withdraw.info.Max : 0);

        return (
            <Form onSubmit={this.handleSubmit.bind(this)} autoComplete="off">

                <Form.Item {...formItemLayout} label="余额">
                    <span className="ant-form-text">
                        {balance.isFetching ? <Spin>loading...</Spin> : Utils.milliFormat(balance.data)}
                    </span>
                </Form.Item>

                <Form.Item label="提现金额" {...formItemLayout}>
                    <Row>
                        <Col span={18}>
                            {getFieldDecorator('amount', {
                                rules: [{
                                    required: true, message: '请输入提现金额'
                                }, {
                                    type: "number",
                                    min: minAmt, max: maxAmt,
                                    message: `提现金额必须大于等于${minAmt}小于等于${maxAmt}`,
                                    transform(value) {
                                        return parseInt(value);
                                    }
                                }],
                                validateTrigger: "onBlur"
                            })(
                                <Input />
                            )}
                        </Col>
                    </Row>
                </Form.Item>

                <Form.Item label="提现银行卡" {...formItemLayout}>
                    {
                        cards.isFetching
                            ?
                            <Spin>loading...</Spin>
                            :
                            <Row gutter={8}>
                                <Col span={18}>
                                    {getFieldDecorator('bank_card', {
                                        rules: [{
                                            required: true, message: '请选择您要提现的银行卡'
                                        }],
                                    })(
                                        <Select>
                                            {(cards.data || []).map(it =>
                                                <Select.Option key={it.Id} value={it.Id.toString()}>
                                                    <i className={`iconfont icon-${it.BankCode}`}></i> {it.BankName}&nbsp;&nbsp; 尾号 <b> ** {it.AccountNo.slice(-4)}</b> &nbsp;&nbsp;<i>[{it.AccountName}]</i>
                                                </Select.Option>
                                            )}
                                        </Select>
                                    )}
                                </Col>
                                <Col span={6}>
                                    <Link to="/bank">绑定银行卡</Link>
                                </Col>
                            </Row>
                    }
                </Form.Item>

                <Form.Item label="安全密码" {...formItemLayout}>
                    <Row gutter={8}>
                        <Col span={18}>
                            {getFieldDecorator('password', {
                                rules: [{
                                    required: true, message: '请输入安全密码'
                                }],
                            })(
                                <Input type="password" />
                            )}
                        </Col>
                        <Col span={6}>
                            <Link to="/passport/password/reset?redirect=/withdraw">忘记密码</Link>
                        </Col>
                    </Row>
                </Form.Item>

                <Form.Item {...tailFormItemLayout}>
                    <Button loading={this.state.loading} type="primary" htmlType="submit" size="large" className="fn-width100">确定</Button>
                </Form.Item>

            </Form>
        );
    }
}

module.exports = connect(state => ({
    balance: state.bill.balance,
    withdraw: state.bill.withdraw,
    cards: state.bank.cards,
}))(Form.create()(Withdraw));
