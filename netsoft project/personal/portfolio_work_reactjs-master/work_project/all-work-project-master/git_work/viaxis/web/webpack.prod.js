const path = require('path');
const webpack = require('webpack');

const node_modules = path.resolve(__dirname, 'node_modules');

const ExtractTextPlugin = require("extract-text-webpack-plugin");

module.exports = function () {
    return {
        devtool: false,
        entry: {
            // react: ['react', "react-dom", "react-router", "react-redux", 'babel-polyfill'],
            antd: ['antd'],
            bundle: './src/index'
        },
        output: {
            path: __dirname + '/dist/assets', // `dist` is the destination
            // publicPath: "/assets/",
            filename: '[name].js',
            chunkFilename: '[name].[chunkhash:5].chunk.js'
        },
        // https://github.com/zhengweikeng/blog/issues/10
        externals: {
            "react": "React",
            "react-dom": "ReactDOM",
            'redux': 'Redux',
            'redux-thunk': 'ReduxThunk',
            'react-redux': 'ReactRedux',
            'react-router': 'ReactRouter',
            "jquery": "jQuery",
        },
        plugins: [
            new webpack.optimize.CommonsChunkPlugin({
                names: ['antd'],
                filename: 'antd.js'
            }),
            new webpack.optimize.UglifyJsPlugin({
                beautify: false,
                mangle: {
                    screw_ie8: true,
                    keep_fnames: true
                },
                compress: {
                    warnings: false,
                    screw_ie8: true
                },
                comments: false
            }),
            new webpack.DefinePlugin({
                __DEV__: JSON.stringify(false),
                'process.env.NODE_ENV': JSON.stringify("production")
            })
        ],
        resolve: {
            extensions: ['', '.js', '.jsx']
        },
        module: {
            loaders: [{
                test: /\.jsx?$/,
                loader: 'babel?presets[]=react,presets[]=es2015,presets[]=stage-0'
            }, {
                test: /\.json$/,
                loader: "json"
            },
            // LESS
            {
                test: /\.less$/,
                loader: 'style!css!less'
            },
            {
                test: /\.css$/,
                loader: 'style!css'
            },
            ]
        }
    };
}();