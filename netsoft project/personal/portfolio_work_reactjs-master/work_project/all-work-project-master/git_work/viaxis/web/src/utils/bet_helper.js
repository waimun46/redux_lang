/**
 * 投注功能类
 *
 * 服务端投注号码格式说明:
 * 投注号码主要由 单位位置占位符(UnitPos) 与 号码(Num)两部分组成，中间用 分隔符(Constants.UNIT_POSITION_SPLIT_CHAR) 进行分割，例如:
 * 00111?342
 * 号码的组成由两部分：
 * 1.纯数字     例如123
 * 2.数位(个十百千万)+数字  例如234,134,63
 * 第二种代表需要选择不同数位的号码，数位用 分隔符(Constants.DIGIT_POS_SPLIT_CHAR) 进行分隔, 
 * 例如上面例子在前三直选中，234代表万位的号码，134代表千位号码，63代表百位号码。
 * 在定位胆中，存在指定数位没有下载数字，例如五星定位胆，正常例子如下:
 * 13,2,45,8,9  这是一注常规的五星定位胆。
 * ,,,,1        这是一组非常规的五星定位胆，也是合法的。
 * ------------------------------
 *          例子大全
 * ------------------------------
 * 时时彩 前三复式：
 * 234,1,345
 * 034,1234567890,3451
 * 234,134,5&0,345,308
 * 
 * 快乐8 选三中三复式:
 * 123
 * 1-2-3  这里需要注意的是，号码之间采用了数位号码分隔符(Constants.DIGIT_POS_CONTENT_SLPIT_CHAR),原因是该彩种的号码存在>=10，也就是双位数，所以要采用分隔符进行号码分隔。
 * 1-2-3&1-2-3
 *
 * 时时彩 任三组三
 * 10101?123
 * 11100?123
 * 00111?12&34
 *
 * 时时彩 大小单双
 * 
 */

import findKey from "lodash.findkey";
import * as Constants from "../app_constants";
import * as Utils from "./common";

/**
 * 将 投注项转 换成服务器接受的 投注数据
 * @param  {[GameType]} gameType [description]
 * @param  {[decimal]} wager    单注金额
 * @param  {[dict|str]} betNum   投注号码
 * @param  {[array|null]} unitPos  单位位置占位符
 * @param  {[dict]} traceData 追号数据 {qh,multiple}
 * @param  {bool} stopOnWin 追号停止
 * @return {object}          [description]
 */
export function buildBetNumData(gameType, qh, wager, betNum, unitPos, traceData, stopOnWin) {
    var data = {
        GameType: gameType.Id,
        Qh: qh,
        BetAmount: wager,
        StopOnWin: stopOnWin
    };

    if (traceData)
        data['Trace'] = buildTraceData(traceData);

    // 号码位数
    const numDigits = gameType.MaxNum.toString().length;
    // 获取服务端号码间的分割符
    const server_num_split_symbol = numDigits > 1 ? Constants.DIGIT_POS_CONTENT_SLPIT_CHAR : "";

    // 构建号码字符串
    var betNumStr = "";
    // 投注号码是字符串，说明是直选
    if (Utils.isString(betNum)) {
        var nums_split_symbols = numDigits > 1 ? Constants.TWO_DIGITS_NUMS_SPLIT_CHARS : Constants.NUMS_SPLIT_CHARS;
        var num_split_symbols = numDigits > 1 ? Constants.NUM_SPLIT_CHAR : null;
        var nums_split_symbol = Utils.getFirst(nums_split_symbols, ",");
        // 删除掉无效号码
        betNumStr = removeInvalidBetNum(betNum, gameType.BetNumCount, nums_split_symbols, num_split_symbols);
        let betNums = parseBetNumFromString(betNumStr, gameType.BetNumCount, nums_split_symbols, num_split_symbols)
            .map(x => x && numDigits > 1 ?
                x.map(it => Utils.pad(it, numDigits)).join(server_num_split_symbol) :
                x.join(server_num_split_symbol));
        betNumStr = betNums.join(Constants.MULTIPLE_NUMS_SPLIT_CHAR);;
    }
    // 非字符串的号码，说明是选码器选出来的号码，格式为: {index:[num<int>,...]}
    else {
        var digitNums = [];
        for (let i = 0; i < gameType.DigitPosCount; i++) {
            if (gameType.BetCountCalurator == "SUMBetCount") {
                digitNums.push(convertSumTextToNum(betNum[i]));
            } else {
                // 补全号码前的0
                if (numDigits > 1)
                    betNum[i] = betNum[i] && betNum[i].map(it => Utils.pad(it, numDigits));
                var num = betNum[i] ? betNum[i].join(server_num_split_symbol) : "";
                digitNums.push(num);
            }
        }
        betNumStr = digitNums.join(Constants.DIGIT_POS_SPLIT_CHAR);
    }

    // 单位位置占位符,不为空的情况下为任选，格式为 [x,x,x,x...] 其中x取值为(0,1)
    if (unitPos) {
        betNumStr = unitPos.join("") + "?" + betNumStr;
    }
    data['BetNum'] = betNumStr;

    return data;
}

/**
 * 创建追号数据
 * @param  {dict} traceData  {qh,multiple}
 * @return {[type]}           [description]
 */
function buildTraceData(traceData) {
    const data = [];

    for (var i in traceData) {
        data.push({
            Qh: traceData[i].qh,
            Multiple: traceData[i].multiple
        });
    }

    return data;
}

/* ------------------------------- BET STRING HELP -------------------------------- */

function convertSumTextToNum(nums) {
    return nums.map(it => Utils.isString(it) && Constants.SUM_NUMS[it] || it).join(Constants.MULTIPLE_NUMS_SPLIT_CHAR);
}

/**
 * 将投注号码转换成字符串，该方法主要用于待投注项号码显示。
 * @param  {dict|str} betNum 投注号码存在两种类型，分别为：
 * 1. 字典 对应使用选码器玩法， 格式: { index:[nums<int>] } 如: {0:[0,3],1:[],2:[4,5]}
 * 2. 字符串 对应直选(Textarea)玩法， 格式: num(symbol)num 如: 123,345;934
 * @param {dict} unitPos 任选位置
 * @return {[type]}        [description]
 */
export function convertBetNumToString(gameType, betNum, unitPos) {
    if (Utils.isString(betNum))
        return betNum;

    if (!Object.keys(betNum)) return "";

    var nums = [];
    for (let i = 0; i < gameType.DigitPosCount; i++) {
        var symbol = gameType.MaxNum < 10 ? "" : Constants.DIGIT_POS_CONTENT_SLPIT_CHAR;
        var num = betNum[i] ? betNum[i].join(symbol) : "";
        nums.push(num);
    }

    var betNumStr = nums.join(Constants.DIGIT_POS_SPLIT_CHAR);
    if (unitPos) {
        let indexArr = [];
        unitPos.forEach((x, i) => x == 1 && indexArr.push(i));
        var unitPosStr = indexArr.map(x => Constants.DIGIT_UNIT_NAME_LIST[x]).join(",");
        betNumStr = `(${unitPosStr})${betNumStr}`;
    }
    return betNumStr;
}

/**
 * 将投注号码美化为可读格式，主要用于投注列表显示
 * @param  {[type]} betNumStr [description]
 * @return {[type]}           [description]
 */
export function betNumBeautify(betNumStr) {
    if (!Utils.isString(betNumStr) || Utils.isEmptyOrNull(betNumStr)) return betNumStr;
    let betNums = betNumStr.split(Constants.UNIT_POSITION_SPLIT_CHAR);

    let unitPos = betNums.length == 2 ? betNums[0] : null;
    let numStr = betNums.length == 2 ? betNums[1] : betNums[0];

    var nums = numStr.split(Constants.MULTIPLE_NUMS_SPLIT_CHAR);
    nums = nums.map(it => /[\d\-]+/.test(it) ? it : findKey(Constants.SUM_NUMS, x => x == it));
    var numBody = nums.join(Constants.MULTIPLE_NUMS_SPLIT_CHAR);
    if (unitPos) {
        let indexArr = [];
        unitPos.split("").forEach((x, i) => x == '1' && indexArr.push(i));
        var unitPosStr = indexArr.map(x => Constants.DIGIT_UNIT_NAME_LIST[x]).join(",");
        numBody = `(${unitPosStr})${numBody}`;
    }
    return numBody;
}

/**
 * 将字符串解析成投注号码
 * @param  {[type]} betNumStr        [description]
 * @param  {[type]} betNumCount      [description]
 * @param  {[type]} nums_group_split_symbols [description]
 * @param  {[type]} num_split_symbols  [description]
 * @return {[type]}                  [description]
 */
export function parseBetNumFromString(betNumStr, betNumCount, nums_group_split_symbols = Constants.NUMS_SPLIT_CHARS, num_split_symbols = Constants.NUM_SPLIT_CHAR) {
    var betNumsArr = Utils.split(betNumStr, nums_group_split_symbols);
    var betNumSet = [];
    betNumsArr.forEach(function (num) {
        num = num.trim();
        // 号码分割符不为空，对号码进行分割
        if (num_split_symbols) {
            var numArr = Utils.split(num, num_split_symbols).map(it => parseInt(it));
            if (numArr && numArr.length == betNumCount && numArr.every(it => !isNaN(it)))
                betNumSet.push(numArr);
        }
        else if (!Utils.isEmptyOrNull(num) && num.length == betNumCount)
            betNumSet.push(num.split(''));
    });
    return betNumSet;
}

/**
 * 删除重复投注号码
 * @param  {str} betNumStr [description]
 * @param  {[type]} nums_group_split_symbols [description]
 * @param  {[type]} num_split_symbols  [description]
 * @return {[type]}           [description]
 */
export function deduplicateBetNum(betNumStr, nums_group_split_symbols = Constants.NUMS_SPLIT_CHARS, num_split_symbols = Constants.NUM_SPLIT_CHAR) {
    var betNumsArr = Utils.split(betNumStr, nums_group_split_symbols);
    var betNumSet = [];
    betNumsArr.forEach(function (num) {
        if (!Utils.isEmptyOrNull(num) && !betNumSet.includes(num))
            betNumSet.push(num);
    });

    var nums_split_symbol = Utils.getFirst(nums_group_split_symbols, ",");
    return betNumSet.join(nums_split_symbol);
}

/**
 * 去除无效投注号码
 * @param  {[type]} betNumStr   [description]
 * @param  {int} betNumCount 投注的号码数量(*星玩法),对应GameType.BetNumCount
 * @param  {[type]} nums_group_split_symbols [description]
 * @param  {[type]} num_split_symbols  [description]
 * @return {[type]}                  [description]
 */
export function removeInvalidBetNum(betNumStr, betNumCount, nums_group_split_symbols = Constants.NUMS_SPLIT_CHARS, num_split_symbols = Constants.NUM_SPLIT_CHAR) {
    var betNums = parseBetNumFromString(betNumStr, betNumCount, nums_group_split_symbols, num_split_symbols);
    var num_split_symbol = Utils.getFirst(num_split_symbols, "");
    for (var i in betNums) {
        betNums[i] = betNums[i].join(num_split_symbol);
    }
    var nums_split_symbol = Utils.getFirst(nums_group_split_symbols, ",");
    return betNums.join(nums_split_symbol);
}

/**
 * 传入当前的游戏数据，判断当前是否接受投注状态
 * @param  {[type]}  current_game_data [description]
 * @return {Boolean}                   [description]
 */
export function isAcceptingBet(current_game_data) {
    if (!current_game_data) return false;

    var server_time_stamp = Utils.GetServerTimeStamp();

    if (!server_time_stamp) return false;

    return (current_game_data.EndTime.getTime() / 1000) > server_time_stamp;
}


/* ------------------------------- UNIT HELP -------------------------------- */

export function getGameTypeDefaultUnits(groupName) {
    const unit_list_name = `${groupName}_DIGIT_UNIT_NAME_LIST`;
    return Constants[unit_list_name] || Constants.DIGIT_UNIT_NAME_LIST;
}

/**
 * 获取玩法的数位名称
 * @param  {[type]} 游戏组名 [description]
 * @param  {GameType} 玩法类型  [description]
 * @return {Array<string>}           [description]
 */
export function getGameTypeUnits(groupName, gameType) {

    if (groupName == "PK10")
        return getPK10GameTypeUnits(gameType);


    // 直选&定位/不定位胆
    if (["StraightPickTreeStyleBetCount", "PositionBetCount"].includes(gameType.BetCountCalurator)) {
        if (gameType.Sort.includes("不定位胆"))
            return [gameType.Name];

        const defaultUnits = getGameTypeDefaultUnits(groupName);
        // 直选复式(X星直选)
        return getUnitsByPos(gameType, defaultUnits);
    } else if (["组选60", '组选30', "组选12"].includes(gameType.Name)) {
        return ["二重号", "单号"];
    } else if (gameType.Name == "组选20") {
        return ["三重号", "单号"];
    } else if (gameType.Name == "组选10") {
        return ["三重号", "二重号"];
    } else if (gameType.Name == "组选5") {
        return ["四重号", "单号"];
    } else if (gameType.Name == "组选6") {
        return ["二重号"];
    } else if (gameType.Name == "组选4") {
        return ["三重号", "单号"];
    } else {
        return [gameType.Name];
    }
}

function getPK10GameTypeUnits(gameType) {
    const defaultUnits = getGameTypeDefaultUnits("PK10");
    if (gameType.GameTypeName.endsWith("BSOE") || gameType.GameTypeName.endsWith("AP")) {
        return [gameType.Name];
    }
    return getUnitsByPos(gameType, defaultUnits);
}

function getUnitsByPos(gameType, defaultUnits) {
    const direction = gameType.GameTypeName.split("_")[1];
    switch (direction[0]) {
        case "B":
            return defaultUnits.slice(defaultUnits.length - gameType.BetNumCount);
        case "M":
            return defaultUnits.slice((defaultUnits.length - gameType.BetNumCount) / 2, gameType.BetNumCount + 1);
        case "F":
        default:
            return defaultUnits.slice(0, gameType.BetNumCount);
    }
}

/**
 * 生成对应玩法的遗漏数据区间
 * @param  {[type]} groupName [description]
 * @param  {[type]} gameType  [description]
 * @param  {[type]} numCount  [description]
 * @param  {[type]} missStr   [description]
 * @return {[type]}           [description]
 */
export function getMissingData(groupName, gameType, numCount, missStr) {
    if (Utils.isEmptyOrNull(missStr)) return null;
    const missList = missStr.split(",");
    const direction = gameType.GameTypeName.split("_")[1];
    const sliceCount = numCount * parseInt(direction[1]);
    switch (direction[0]) {
        case "B":
            return missList.slice(missList.length - sliceCount);
        case "M":
            var units = getGameTypeDefaultUnits(groupName);
            var start = (units.length - gameType.BetNumCount) / 2 * numCount;
            return missList.slice(start, start + sliceCount);
        case "F":
            return missList.slice(0, sliceCount);
        default:
            return missList;
    }
}

/**
 * 生成单式投注范例
 * @param  {[type]} minNum   [description]
 * @param  {[type]} maxNum   [description]
 * @param  {[type]} numCount [description]
 * @return {[type]}          [description]
 */
export function getLinearStyleFormatExample(minNum, maxNum, numCount) {
    const sb = ["请将您要选择的号码复制或输入到这里\n支持格式:"];

    const firstNums = Utils.range(minNum, minNum + numCount);
    const firstLastNum = firstNums[firstNums.length - 1];
    const secondNums = maxNum - firstLastNum >= numCount ?
        Utils.range(firstLastNum + 1, firstLastNum + 1 + numCount) :
        Utils.range(minNum + 1, minNum + 1 + numCount);
    const specialSplits = [" ", "\n"];
    const isMultipleDigits = maxNum > 9;

    const parseRegSp = sp => {
        if (!Utils.isString(sp)) {
            const reg = new RegExp(sp);
            const regSplit = specialSplits.find(t => reg.test(t));
            if (regSplit)
                sp = regSplit;
        }
        return sp;
    };
    if (isMultipleDigits) {
        Constants.NUM_SPLIT_CHAR.forEach(sp => {
            sp = parseRegSp(sp);
            sb.push(firstNums.join(sp) + ";" + secondNums.join(sp));
        });

    } else {
        Constants.NUMS_SPLIT_CHARS.forEach(sp => {
            sp = parseRegSp(sp);
            sb.push(firstNums.join("") + sp + secondNums.join(""));
        });
    }

    return sb.join("\n");
}

/* ------------------------------- SUM BSOE -------------------------------- */
/**
 * 计算开奖结果 单双
 * @param  {[type]} group   [description]
 * @param  {[type]} results [description]
 * @return {[type]}         [description]
 */
export function sumOddEven(group, results) {
    switch (group.toUpperCase()) {
        case "KL8":
            return sumKL8OddEven(results);
        case "KL10":
            return sumKL10OddEven(results);
        case "KENO":
            return sumKENOOddEven(results);
        default:
            return sumDefaultOddEven(results);
    }
}

/**
 * 计算开奖结果 大小
 * @param  {[type]} group   [description]
 * @param  {[type]} results [description]
 * @return {[type]}         [description]
 */
export function sumBigSmall(group, results) {
    switch (group.toUpperCase()) {
        case "KL8":
            return sumKL8BigSmall(results);
        case "KL10":
            return sumKL10BigSmall(results);
        case "KENO":
            return sumKENOBigSmall(results);
        default:
            return sumDefaultBigSmall(results);
    }
}

export function sumDefaultOddEven(results) {
    if (Utils.isString(results))
        results = results.split(",");
    var num = results[results.length - 1];
    if (!Number.isInteger(num))
        num = parseInt(num, 10);
    return num % 2 == 0 ? "双" : "单";
}

export function sumDefaultBigSmall(results) {
    if (Utils.isString(results))
        results = results.split(",");
    var num = results[results.length - 1];
    if (!Number.isInteger(num))
        num = parseInt(num, 10);
    return num < 5 ? "小" : "大";
}

function sumResults(results) {
    if (Utils.isString(results))
        results = results.split(",");
    var total = 0;
    for (var i in results) {
        var num = Number.isInteger(results[i]) ? num : parseInt(results[i], 10);
        total += num;
    }
    return total;
}

export function sumKL8OddEven(results) {

    var total = sumResults(results);

    var res = "";
    if (total == 810) res = "和";
    else if (total >= 811 && total <= 1410) res = "大";
    else if (total >= 210 && total <= 1409) res = "小";
    return res;
}

export function sumKL8BigSmall(results) {
    var total = sumResults(results);
    var res = "";
    if (total == 810) res = "和";
    if (total >= 210 && total <= 1410) {
        res = (total % 2 == 0 ? "双" : "单");
    }
    return res;
}

export function sumKL10OddEven(results) {
    var total = sumResults(results);
    var res = "";
    if (total == 84) res = "和";
    if (total >= 36 && total <= 132) {
        res = (total % 2 == 0 ? "双" : "单");
    }
    return res;
}

export function sumKL10BigSmall(results) {
    var total = sumResults(results);
    var res = "";
    if (total == 84) res = "和";
    if (total >= 85 && total <= 132) res = "大";
    else if (total >= 36 && total <= 83) res = "小";
    return res;
}

export function sumKENOOddEven(results) {
    var total = sumResults(results);
    var res = "";
    if (total == 810) res = "和";
    if (total >= 210 && total <= 1410) {
        res = (total % 2 == 0 ? "双" : "单");
    }
    return res;
}

export function sumKENOBigSmall(results) {
    var total = sumResults(results);
    var res = "";
    if (total >= 811 && total <= 1410) res = "大";
    else if (total >= 210 && total <= 1409) res = "小";
    return res;
}
