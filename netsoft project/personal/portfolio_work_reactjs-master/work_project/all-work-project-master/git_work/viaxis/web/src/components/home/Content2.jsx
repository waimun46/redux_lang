import React from 'react';
import QueueAnim from 'rc-queue-anim';
import TweenOne from 'rc-tween-one';
import OverPack from 'rc-scroll-anim/lib/ScrollOverPack';

class Content extends React.Component {

    static propTypes = {
        id: React.PropTypes.string,
    };

    static defaultProps = {
        className: 'content2',
    };

    getDelay = e => e.index % 3 * 100 + Math.floor(e.index / 3) * 100 + 200;

    render() {
        const blockArray = [
            { children: { icon: { children: 'https://zos.alipayobjects.com/rmsportal/ScHBSdwpTkAHZkJ.png' }, title: { children: '10年经验' }, content: { children: '最靠谱的线上娱乐网站，10年经验 优质完善的服务' } } },
            { children: { icon: { children: 'https://zos.alipayobjects.com/rmsportal/NKBELAOuuKbofDD.png' }, title: { children: '迅速到账' }, content: { children: '充值无需手续费 迅速到账。' } } },
            { children: { icon: { children: 'https://zos.alipayobjects.com/rmsportal/xMSBjgxBhKfyMWX.png' }, title: { children: '云监控' }, content: { children: '涵盖国内主流彩种 娱乐种类齐全。' } } },
            { children: { icon: { children: 'https://zos.alipayobjects.com/rmsportal/MNdlBNhmDBLuzqp.png' }, title: { children: '强大技术支援' }, content: { children: '强大技术支援 多重账户安全 提供安全购彩家园。' } } },
            { children: { icon: { children: 'https://zos.alipayobjects.com/rmsportal/UsUmoBRyLvkIQeO.png' }, title: { children: '手机端娱乐' }, content: { children: '提供手机客户端 随时随地实现梦想 随心娱乐。' } } },
            { children: { icon: { children: 'https://zos.alipayobjects.com/rmsportal/ipwaQLBLflRfUrg.png' }, title: { children: '实力保障' }, content: { children: '雄厚资金实力 为你遮风挡雨 保驾护航。' } } },
        ];
        const children = blockArray.map((item, i) => {
            const children = item.children;
            const styleObj = item.style || {};
            return (
                <li
                key={i}
                style={{ left: `${i % 3 * 33.33}%`, top: `${Math.floor(i / 3) * 200}px`, ...styleObj }}
                >
                    <TweenOne
                        animation={{ x: '-=10', opacity: 0, type: 'from' }}
                        className="img"
                        key="img"
                        style={children.icon.style}
                    >
                        <img src={children.icon.children} width="100%" />
                    </TweenOne>
                    <QueueAnim delay={100} leaveReverse key="text" className="text">
                        <h1 key="h1" style={children.title.style}>{children.title.children}</h1>
                        <p key="p" style={children.content.style}>{children.content.children}</p>
                    </QueueAnim>
                </li>
            );
        });

        const titleAnim = { y: '+=30', opacity: 0, type: 'from' };
        return (
            <div {...this.props} className="content-template-wrapper">
                <OverPack
                    className={`content-template ${this.props.className}`}
                    hideProps={{ h1: { reverse: true }, p: { reverse: true } }}
                    location={this.props.id}>
                    <TweenOne
                        key="h1"
                        animation={titleAnim}
                        component="h1"
                    >
                        帝国娱乐提供专业完美的服务
                    </TweenOne>
                    <QueueAnim
                        key="ul"
                        component="ul"
                        leaveReverse
                        type="bottom"
                        interval={0}
                        delay={this.getDelay}
                    >
                        {children}
                    </QueueAnim>
                </OverPack>
            </div>
        );
    }
}


export default Content;
