import { ActionState } from "../app_constants";
import ReduceBase from "./reduce_base";

const defaultState = {
	balance: {
		isFetching: false
	},
	billflow: {
		isFetching: false,
		data: [],
		count: 0,
		pageIndex: 0
	},
	transfer: {
		isFetching: false,
		data: [],
		count: 0,
		pageIndex: 0
	},
	withdraw: {
		isFetching: false,
		// 提现信息(提现次数，最大、最小，提现金额最大最小等。)
		info: null,
		data: [],
		// 总条数
		count: 0,
		// 当前页码
		pageIndex: 0
	},
	deposit: {
		isFetching: false,
		data: []
	},
	depositbank: {
		isFetching: false,
		data: []
	},
	depositscan: {
		isFetching: false,
		QrCodeAddress: ''
	}
}

class BillReducer extends ReduceBase {
	static reduce(state = defaultState, action) {
		switch (action.type) {
			case "Balance":
				return BillReducer._reduce_balance(state, action);
			case "BillFlow":
				return BillReducer._process_page(state, action, "billflow");
			case "TransferHistory":
				return BillReducer._process_page(state, action, "transfer");
			case "WithdrawInfo":
				return BillReducer._reduce_withdraw_info(state, action);
			case "WithdrawHistory":
				return BillReducer._process_page(state, action, "withdraw");
			case "DepositInfo":
				return BillReducer._reduce_deposit_info(state, action);
			case "IPSDeposit":
				return BillReducer._reduce_deposit_ips(state, action);
			case "DepositIpsBank":
				return BillReducer._reduce_deposit_ipsbank(state, action);
			case "IPSScanDeposit":
				return BillReducer._reduce_deposit_ips_scan(state, action);
			default:
				return state;
		}
	}

	static _reduce_balance(state, action) {
		let isFetching = action.state == ActionState.Doing;
		const resp = action.response;
		if (resp && resp.code == 0) {
			let balance = {
				isFetching,
				data: resp.data
			};
			return Object.assign({}, state, { balance });
		}
		return Object.assign({}, state, {
			balance: {
				isFetching
			}
		});
	}

	/* ----------------------------- Withdraw ------------------------------ */
	static _reduce_withdraw_info(state, action) {
		let isFetching = action.state == ActionState.Doing;
		const resp = action.response;
		if (resp && resp.code == 0) {
			let withdraw = {
				isFetching,
				info: resp.data
			};
			return Object.assign({}, state, { withdraw });
		}
		return Object.assign({}, state, {
			withdraw: {
				isFetching
			}
		});
	}

	/* ----------------------------- Deposit ------------------------------ */
	static _reduce_deposit_info(state, action) {
		let isFetching = action.state == ActionState.Doing;
		const resp = action.response;
		if (resp && resp.code == 0) {
			let deposit = {
				isFetching,
				info: resp.data
			};
			return Object.assign({}, state, { deposit });
		}
		return Object.assign({}, state, {
			deposit: {
				isFetching
			}
		});
	}

	static _reduce_deposit_ips(state, action) {
		let isFetching = action.state == ActionState.Doing;
		const resp = action.response;
		if (resp && resp.code == 0) {
			let deposit = {
				isFetching,
				info: resp.data
			};
			return Object.assign({}, state, { deposit });
		}
		return Object.assign({}, state, {
			deposit: {
				isFetching
			}
		});
	}

	static _reduce_deposit_ipsbank(state, action) {
		let isFetching = action.state == ActionState.Doing;
		const resp = action.response;
		if (resp && resp.code == 0) {
			let depositbank = {
				isFetching,
				data: resp.data
			};
			return Object.assign({}, state, { depositbank });
		}
		return Object.assign({}, state, {
			depositbank: {
				isFetching
			}
		});
	}

	static _reduce_deposit_ips_scan(state, action) {
		let isFetching = action.state == ActionState.Doing;
		const resp = action.response;
		if (resp && resp.code == 0) {
			let depositscan = {
				isFetching,
				QrCodeAddress: resp.data
			};
			return Object.assign({}, state, { depositscan });
		}
		return Object.assign({}, state, {
			depositscan: {
				isFetching
			}
		});
	}
}

module.exports = BillReducer;
