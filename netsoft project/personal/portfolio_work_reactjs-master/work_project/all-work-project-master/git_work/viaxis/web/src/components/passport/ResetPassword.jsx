import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router'
import { message, Alert, Spin, Row, Col, Checkbox, Button, Form, Input, Icon } from 'antd';

import BasicComponent from "../common/BasicComponent";
import Layout from './Layout';

import * as PassportAction from "../../actions/passport";
import * as Validators from "../../utils/validator";
import * as Utils from "../../utils/common";
import * as RenderHelper from "../../utils/render_helper";


class ResetPassword extends BasicComponent {

    constructor(props) {
        super(props);
        this.ticket = props.params.ticket;
        this.captchaUrl = RenderHelper.getCaptchaImgUrl();
        this.state = {
            // 检查验证
            checking: false,
            loading: false,
            isValid: false
        };
    }

    /* -------------------------- Sys ----------------------------- */

    componentWillMount(){
        this.checkTicketValid();
    }

    /* -------------------------- Function ----------------------------- */

    checkTicketValid(){
        this.setState({checking:true});
        this.props.dispatch(PassportAction.checkResetTicket(this.ticket)).then(act => {
            this.setState({checking:false});
            this.assert(act, {
                fnOk: it => this.setState({
                    isValid: it.data
                })
            });
        });
    }

    /* -------------------------- Events ----------------------------- */

    handleSubmit(e){
        const { dispatch } = this.props;
        e.preventDefault();
        this.props.form.validateFields((errors, values) => {
            if (errors) return;

            this.setState({loading:true});
            this.props.dispatch(PassportAction.resetLoginPassword(this.ticket, values.password)).then(act=>{
                this.setState({loading:false});
                this.assert(act, {
                    fnOk: () => RenderHelper.success("重置成功", "恭喜您修改成功", () => this.goto("/login"))
                });
            });
        });
    }

    handleReset(e) {
        e.preventDefault();
        this.props.form.resetFields();
    }

    refreshCaptcha(){
        let captcha = this.props.form.getFieldInstance("captcha");
        captcha.refs.input.value = "";
        captcha.refs.input.focus();
        this.refs.captcha_img.src = RenderHelper.getCaptchaImgUrl();
    }

    /* -------------------------- Renders ----------------------------- */

    render(){
        return (
            <Layout>
                <div className="passport-box" style={{width:400}}>
                    <div className="passport-box-header fn-clear">
                        <h2 className="fn-left">重置密码</h2>
                        <span className="fn-right">
                            <Link to="/login">立即登陆</Link>
                        </span>
                    </div>
                    <div className="passport-box-body">
                        {this.state.checking?
                            this.renderLoading()
                            :
                            (
                                this.state.isValid?
                                this.renderForm():
                                this.renderAlert()
                            )
                        }
                    </div>
                </div>
            </Layout>
        );
    }

    renderForm(){

        const { getFieldDecorator } = this.props.form;

        return (
            <Form className="passport-box-body-form" horizontal onKeyPress={(e)=>{if(e.key=="Enter")this.handleSubmit(e);}}>
                <div className="passport-box-body-content">
                    <br/>
                    <Form.Item>
                        {getFieldDecorator('password', {
                            rules: [{
                                required: true, message: '请输入新的登陆密码'
                            },{
                                min:6, max:20, message:'登陆密码长度要求为6到20个字符之间。'
                            }],
                        })(
                            <Input type="password" className="input-password" placeholder="新的登陆密码" />
                        )}
                    </Form.Item>
                    <Form.Item>
                        {getFieldDecorator('confirm_pwd', {
                            rules: [{
                                required: true, message: "请输入确认密码"
                            },{
                                validator: Validators.checkEqual, message: "两个密码不一致。",
                                getFieldValue: this.props.form.getFieldValue,
                                equalEleName: "password"
                            }],
                        })(
                            <Input type="password" className="input-password" placeholder="请再次输入登陆密码" />
                        )}
                    </Form.Item>
                    <br/>
                    <Form.Item>
                        <Button className="passport-submit" loading={this.state.loading} onClick={this.handleSubmit.bind(this)}>修改密码</Button>
                        <br/><br/>
                        <span className="fn-right">
                            没有账户?&nbsp;&nbsp;
                            <Link to="/register">立即注册</Link>
                        </span>
                    </Form.Item>
                </div>
            </Form>
        );
    }

    renderAlert(){
        return (
            <div>
                <br/>
                <Alert
                    message="无效的链接"
                    description={<p>无效的链接或者该链接已经失效，若您还需继续操作，请<Link to="/forget">重新发送</Link>重置链接.</p>}
                    type="warning"
                    showIcon
                />
            </div>
        );
    }

    renderLoading(){
        return (
            <div className="fn-text-center mt30 pt30">
                <Spin tip="加载中..." />
            </div>
        );
    }
}

module.exports = connect(state => ({
    data: state.passport
}))(Form.create()(ResetPassword));
