import * as Units from '../src/utils/common.js';
import Lodash from "lodash";
import assert from "assert";
import moment from "moment";


describe('测试String Pad', function () {
    
    it("结果为0012", () => {
        var resp = Units.pad(12, 4, '0');
        console.log(resp);
        assert.equal(resp, "0012");
    });
})

describe('测试String Mask Start', function() {
    it("结果为***1245", () => {
        var resp = Units.maskStart("1231245", 4);
        console.log(resp);
        assert.equal(resp, "***1245");
    });
})

describe('测试String Mask End', function() {
    it("测试结果为123***", () => {
        var resp = Units.maskEnd("123456", 3);
        console.log(resp);
        assert.equal(resp, "123***");
    });
})

describe('测试moment', function () {
    it("",()=>{
        var time = moment().startOf('week');
        console.log(time);
        const lastMonth1th = moment().startOf('month').add(-1, 'months');
        console.log(lastMonth1th);
        console.log(lastMonth1th.endOf('month'));
        console.log(lastMonth1th);
    });
})