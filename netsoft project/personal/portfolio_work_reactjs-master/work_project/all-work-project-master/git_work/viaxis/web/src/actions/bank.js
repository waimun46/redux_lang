import $ from "jquery";
import { ActionState } from "../app_constants";
import { POST, GET } from "../utils/common";
import getSocket from "../websocket";

const socket = getSocket();

/**
 * 获取银行列表
 * @return {[type]} [description]
 */
export function getBank() {
    return {
        name: "Bank",
        callAPI: () => POST("/api/bank")()
    };
}

/**
 * 获取用户的银行卡列表
 * @return {[type]} [description]
 */
export function getBankCards() {
    return {
        name: "BankCard",
        callAPI: () => POST("/api/bank/cards")()
    };
}

/**
 * 用户添加银行卡
 * @param {[type]} bank         所在银行ID
 * @param {[type]} account_no   卡号
 * @param {[type]} account_name 户名
 * @param {[type]} branch       支行信息
 * @param {[type]} password     安全密码
 */
export function addBankCard(bank, account_no, account_name, branch, password) {
    var body = JSON.stringify({
        BankId: bank, AccountNo: account_no, AccountName: account_name, BranchName: branch, Password: password
    });
    return {
        name: "BankCardAdd",
        callAPI: () => POST("/api/bank/cards/add", body)()
    };
}

/**
 * 获取充值银行
 * @return {[type]} [description]
 */
export function getDepositBank(bank_type) {
    var body = JSON.stringify({
        bank_type
    });
    return {
        name: "DepositBank",
        callAPI: () => POST("/api/deposit/depositbank", body)()
    };
}

/**
 * 获取扫码银行信息
 * @return {[type]} [description]
 */
export function getDepositSP(bank_type) {
    var body = JSON.stringify({
        bank_type
    });
    return {
        name: "DepositSP",
        callAPI: () => POST("/api/deposit/depositspbank", body)()
    };
}
