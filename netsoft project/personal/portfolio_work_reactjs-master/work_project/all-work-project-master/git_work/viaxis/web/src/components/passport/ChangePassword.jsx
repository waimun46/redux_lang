import React from 'react';
import { connect } from 'react-redux';
import { message, Row, Col, Alert, Tabs, Button, Form, Input, Icon } from 'antd';

import BasicComponent from "../common/BasicComponent";
import DashContainer from '../../containers/DashContainer';

import * as Passport from "../../actions/passport";
import * as Constants from "../../app_constants";
import Auth from "../../auth";
import * as Utils from "../../utils/common";
import * as RenderHelper from "../../utils/render_helper";


class FormBase extends BasicComponent{

    refreshCaptcha(){
        let captcha = this.props.form.getFieldInstance("captcha");
        captcha.refs.input.value = "";
        captcha.refs.input.focus();
        this.refs.captcha_img.src = RenderHelper.getCaptchaImgUrl();
    }

    redirect(){
        const redirectUrl = Utils.getUrlParameter("redirect");
        if (Utils.isNonEmptyString(redirectUrl))
            // 2秒后才执行是因为成功后有一个Message显示，而Message默认显示2秒。
            setTimeout(() => this.goto(redirectUrl), 2000);
    }

    checkPassword(rule, value, callback){
        if (value && value !== rule.getFieldValue('password')) {
            callback(rule.message);
        } else {
            callback();
        }
    }
}

/**
 * 修改登陆密码表单
 */
class LoginPasswordForm extends FormBase{

    constructor(props) {
        super(props);
        this.state = {
            loading: false
        }
        this.captchaUrl = RenderHelper.getCaptchaImgUrl();
    }

    /* ---------------------------- Events ------------------------------*/

    handleSubmit(e){
        e.preventDefault();

        const { dispatch } = this.props;

        this.props.form.validateFields((errors, values) => {
            if (errors) return;

            dispatch(Passport.updateLoginPassword(values.current_pwd, values.password, values.captcha)).then(act => {
                this.setState({loading:false});

                const resp = act.response;

                if(act.state == Constants.ActionState.Success){
                    if (resp.code == 0) {
                        RenderHelper.success("您的密码已修改，请重新登录!", "", Auth.logout);
                        this.props.form.resetFields();
                    } else {
                        this.refreshCaptcha();
                        message.error(resp.message);
                    }
                }else{
                    message.error(act.error);
                }
            });
            this.setState({loading:true});
        })
    }

    /* ---------------------------- Renders ------------------------------*/

    render(){
        const { getFieldDecorator } = this.props.form;

        const formItemLayout = {
            labelCol: { span: 6 },
            wrapperCol: { span: 12 },
        };

        const tailFormItemLayout = {
            wrapperCol: {
                span: 14,
                offset: 6,
            },
        };

        return (
            <Form onSubmit={this.handleSubmit.bind(this)}>
                <Form.Item label="当前密码" {...formItemLayout} hasFeedback>
                    {getFieldDecorator('current_pwd', {
                        rules: [{
                            required: true, message: '请输入当前的登陆密码',
                        }],
                    })(
                        <Input type="password" />
                    )}
                </Form.Item>
                <Form.Item label="新的密码" {...formItemLayout} hasFeedback>
                    {getFieldDecorator('password', {
                        rules: [{
                            required: true, message: '请输入新的登陆密码'
                        },{
                            min:6, max:20, message:'登陆密码长度要求为6到20个字符之间。'
                        }],
                    })(
                        <Input type="password" />
                    )}
                </Form.Item>
                <Form.Item label="确认密码" {...formItemLayout} hasFeedback>
                    {getFieldDecorator('confirm_pwd', {
                        rules: [{
                            required: true, message: "请输入确认密码"
                        },{
                            validator: this.checkPassword, message: "两个密码不一致。",
                            getFieldValue: this.props.form.getFieldValue
                        }],
                    })(
                        <Input type="password" />
                    )}
                </Form.Item>
                <Form.Item label="验证码" {...formItemLayout}>
                    <Row gutter={8}>
                        <Col span={12}>
                        {getFieldDecorator('captcha', {
                            rules: [{ required: true, message: '请输入右侧验证码' }],
                        })(
                            <Input size="large" autoComplete="off" maxLength="4" />
                        )}
                        </Col>
                        <Col span={12}>
                            <img src={this.captchaUrl} ref="captcha_img" className="img-captcha" onClick={this.refreshCaptcha.bind(this)} />
                        </Col>
                    </Row>
                </Form.Item>
                <Form.Item {...tailFormItemLayout}>
                    <Button loading={this.state.loading} type="primary" htmlType="submit" size="large">确定</Button>
                </Form.Item>
             </Form>
         );
    }
}

class SafePasswordForm extends FormBase{

    constructor(props) {
        super(props);
        this.captchaUrl = RenderHelper.getCaptchaImgUrl();
        this.state = {
            // 提交按钮
            loading: false,
            // 查询用户是否已设置安全密码
            querying: false,
            // 用户是否已设置安全密码
            hasSafePassword: true,
            // 发送验证码按钮
            captchaSending: false,
            // 发送按钮倒计时
            interval: 0,
        }
    }

    /* ---------------------------- Sys Functions ------------------------------*/
    componentWillMount() {
        this.queryHasSafePassword();
    }

    componentWillUnmount() {
        if (this.interval) {
            clearInterval(this.interval);
            this.interval = null;
        }
    }

    /* ---------------------------- Functions ------------------------------*/

    queryHasSafePassword() {
        this.props.dispatch(Passport.hasSafePassword()).then(it => {
            this.setState({querying:false});

            const resp = it.response;
            if (resp.code == 0) {
                this.setState({hasSafePassword:resp.data});
            }

        });

        this.setState({querying:true});
    }

    /* ---------------------------- Events ------------------------------*/

    /**
     * 修改/设置(第一次)安全密码
     * @param  {[type]} e [description]
     * @return {[type]}   [description]
     */
    handleUpdateSubmit(e){
        e.preventDefault();

        const { dispatch } = this.props;

        this.props.form.validateFields((errors, values) => {
            if (errors) return;

            dispatch(Passport.updateSafePassword(values.current_pwd, values.password, values.captcha)).then(act=>{
                this.setState({loading:false});

                const resp = act.response;
                if(act.state == Constants.ActionState.Success){
                    if (resp.code == 0) {
                        let msg = "修改安全密码成功";
                        if(!this.state.hasSafePassword){
                            msg="设置安全密码成功";
                            this.setState({hasSafePassword:true});
                            this.props.form.resetFields();
                        }
                        message.success(msg);
                        this.redirect();
                    } else {
                        this.refreshCaptcha();
                        message.error(resp.message);
                    }
                }else{
                    message.error(act.error);
                }

            });

            this.setState({loading:true});
        })
    }

    /**
     * 重置安全密码
     * @param  {[type]} e [description]
     * @return {[type]}   [description]
     */
    handleResetSubmit(e) {
        e.preventDefault();

        const { dispatch } = this.props;

        this.props.form.validateFields((errors, values) => {
            if (errors) return;

            dispatch(Passport.resetSafePassword(values.password, values.captcha)).then(act=>{
                this.setState({loading:false});

                const resp = act.response;
                if(act.state == Constants.ActionState.Success){
                    if (resp.code == 0) {
                        let msg = "重置安全密码成功";
                        message.success(msg);
                        this.props.form.resetFields();
                    } else {
                        message.error(resp.message);
                    }
                }else{
                    message.error(act.error);
                }

            });

            this.setState({loading:true});
        })
    }

    sendCaptchaClick(e) {
        const {dispatch} = this.props;

        dispatch(Passport.sendTicket()).then(act=>{
            this.setState({captchaSending:false});

            const resp = act.response;
            if (resp.code == 0) {
                this.setState({interval:60});
                this.interval = setInterval(()=>{
                    const {interval} = this.state;

                    if (interval <= 1) {
                        clearInterval(this.interval);
                        this.interval = null;
                        this.props.onClose && this.props.onClose();
                    }

                    this.setState({
                        interval: interval - 1,
                    });

                },1000);
                message.error("验证码已发送到您的邮箱。");
            }else{
                message.error(resp.message);
            }

        });

        this.setState({captchaSending:true});
    }

    /* ---------------------------- Renders ------------------------------*/

    render(){
        if(this.props.reset)
            return this.renderResetForm();

        return this.renderChangeForm();
    }

    renderChangeForm(){
        const { getFieldDecorator } = this.props.form;

        const formItemLayout = {
            labelCol: { span: 6 },
            wrapperCol: { span: 12 },
        };

        const tailFormItemLayout = {
            wrapperCol: {
                span: 14,
                offset: 6,
            },
        };

        return (
            <Form onSubmit={this.handleUpdateSubmit.bind(this)}>
                {this.state.hasSafePassword&&<Form.Item label="当前密码" {...formItemLayout} hasFeedback>
                    {getFieldDecorator('current_pwd', {
                        rules: [{
                            required: true, message: '请输入当前的安全密码',
                        }],
                    })(
                        <Input type="password" />
                    )}
                </Form.Item>}
                <Form.Item label="新的密码" {...formItemLayout} hasFeedback>
                    {getFieldDecorator('password', {
                        rules: [{
                            required: true, message: '请输入新的安全密码'
                        },{
                            min:6, max:20, message:'安全密码长度要求为6到20个字符之间。'
                        }],
                    })(
                        <Input type="password" />
                    )}
                </Form.Item>
                <Form.Item label="确认密码" {...formItemLayout} hasFeedback>
                    {getFieldDecorator('confirm_pwd', {
                        rules: [{
                            required: true, message: "请输入确认密码"
                        },{
                            validator: this.checkPassword, message: "两个密码不一致。",
                            getFieldValue: this.props.form.getFieldValue
                        }],
                    })(
                        <Input type="password" />
                    )}
                </Form.Item>
                <Form.Item label="验证码" {...formItemLayout}>
                    <Row gutter={8}>
                        <Col span={12}>
                        {getFieldDecorator('captcha', {
                            rules: [{ required: true, message: '请输入右侧验证码' }],
                        })(
                            <Input size="large" autoComplete="off" maxLength="4" />
                        )}
                        </Col>
                        <Col span={12}>
                            <img src={this.captchaUrl} ref="captcha_img" className="img-captcha" onClick={this.refreshCaptcha.bind(this)} />
                        </Col>
                    </Row>
                </Form.Item>
                <Form.Item {...tailFormItemLayout}>
                    <Button loading={this.state.loading} type="primary" htmlType="submit" size="large">确定</Button>
                </Form.Item>
             </Form>
         );
    }

    renderResetForm(){
        const { getFieldDecorator } = this.props.form;

        const formItemLayout = {
            labelCol: { span: 6 },
            wrapperCol: { span: 12 },
        };

        const tailFormItemLayout = {
            wrapperCol: {
                span: 14,
                offset: 6,
            },
        };

        return (
            <Form onSubmit={this.handleResetSubmit.bind(this)}>
                <Form.Item label="新的密码" {...formItemLayout} hasFeedback>
                    {getFieldDecorator('password', {
                        rules: [{
                            required: true, message: '请输入新的登陆密码'
                        },{
                            min:6, max:20, message:'登陆密码长度要求为6到20个字符之间。'
                        }],
                    })(
                        <Input type="password" />
                    )}
                </Form.Item>
                <Form.Item label="确认密码" {...formItemLayout} hasFeedback>
                    {getFieldDecorator('confirm_pwd', {
                        rules: [{
                            required: true, message: "请输入确认密码"
                        },{
                            validator: this.checkPassword, message: "两个密码不一致。",
                            getFieldValue: this.props.form.getFieldValue
                        }],
                    })(
                        <Input type="password" />
                    )}
                </Form.Item>
                <Form.Item label="邮箱验证码" {...formItemLayout}>
                    <Row gutter={8}>
                        <Col span={12}>
                        {getFieldDecorator('captcha', {
                            rules: [{ required: true, message: '请输入您接收到验证码' }],
                        })(
                            <Input size="large" autoComplete="off" maxLength="6" />
                        )}
                        </Col>
                        <Col span={12}>
                            <Button loading={this.state.captchaSending}
                                size="large" type="ghost"
                                disabled={this.state.interval>0}
                                onClick={this.sendCaptchaClick.bind(this)}>
                                发送验证码{this.state.interval>0?`(${this.state.interval})`:''}
                            </Button>
                        </Col>
                    </Row>
                </Form.Item>
                <Form.Item {...tailFormItemLayout}>
                    <Button type="primary" htmlType="submit" size="large">确定</Button>
                </Form.Item>
             </Form>
         );
    }
}


const LoginPasswordFormWraper = connect()(Form.create()(LoginPasswordForm));
const SafePasswordFormWraper = connect()(Form.create()(SafePasswordForm));


class ChangePassword extends BasicComponent {

    constructor(props) {
        super(props);

        let type = (props.params.type || "").toLowerCase();
        if (["", "safe", "reset"].indexOf(type) < 0)
            this.goto("/404");

        this.state = {
            type
        }

        
    }

    /* ---------------------------- 系统函数 ------------------------------*/


    /* ---------------------------- Events ------------------------------*/


    /* ---------------------------- Renders ------------------------------*/

    render(){

        const type = this.state.type;

        return (
            <DashContainer title="账号安全">
                <br/><br/>
                <div className="wrapper600">
                    <Alert type="info" showIcon message="我们建议您至少每个月修改一次登录密码与安全密码，有助于提高您的账户安全。" />
                    <br/>
                    <Tabs defaultActiveKey={type}>
                        <Tabs.TabPane tab="登陆密码" key="">
                            <LoginPasswordFormWraper />
                        </Tabs.TabPane>
                        <Tabs.TabPane tab="安全密码" key="safe">
                            <SafePasswordFormWraper />
                        </Tabs.TabPane>
                        <Tabs.TabPane tab="忘记安全密码" key="reset">
                            <SafePasswordFormWraper reset />
                        </Tabs.TabPane>
                    </Tabs>
                     
                </div>
            </DashContainer>
        );
    }

}



module.exports = connect(state => ({
    data: state.passport
}))(ChangePassword);
