import React from 'react';
import { connect } from 'react-redux';
import $ from "jquery";
import { Carousel } from "antd";
import { browserHistory, Link } from 'react-router'


class Home extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            navPadTop: true
        }
    }

    /* ---------------------------- 系统函数 ------------------------------*/

    componentDidMount() {
        window.addEventListener('scroll', this.handleScroll.bind(this));
    }

    componentWillUnmount() {
        window.removeEventListener('scroll', this.handleScroll.bind(this));
    }

    /* ---------------------------- Events ------------------------------*/

    handleScroll(e) {
        // 滚动条离页面最上方的距离
        const scrollTop = document.body.scrollTop;
        if (scrollTop >= 20 && this.state.navPadTop)
            this.setState({ navPadTop: false });
        else if (scrollTop < 20 && !this.state.navPadTop)
            this.setState({ navPadTop: true });
    }

    /* ---------------------------- Renders ------------------------------*/

    render() {
        const winHeight = $(window).height();
        let headerClass = "index-page-header";
        if (this.state.navPadTop)
            headerClass += " paddingTop";

        return (
            <div className="index-page">
                <div className={headerClass}>
                    <div className="nav-wrapper">
                        <div className="nav-content fn-clear">
                            <a href="./" className="nav-logo">
                                <img src="https://os.alipayobjects.com/rmsportal/nAStiIiXOkhKVdi.svg" />
                                <span className="logo-text">G2</span>
                                一套图形语法
                            </a>

                            <ul className="nav">
                                <li><a>首页</a></li>
                                <li><a>活动</a></li>
                                <li><a>帮助</a></li>
                                <li><a>关于我们</a></li>
                                <li><Link to="/login">登陆</Link></li>
                                <li><Link to="/register">注册</Link></li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div className="index-page-banner" style={{ height: winHeight }}>
                    <Carousel>
                        <div className="item" style={{ backgroundImage: "url(banner/banner-one.jpg)" }}></div>
                    </Carousel>
                </div>
                <div className="index-page-body">
                    <div className="section-one">
                        <div className="title">
                            <h1>最热彩种</h1>
                            <p>popular lottery games</p>
                        </div>
                    </div>
                    <div className="section-two">
                        <div className="title">
                            <h1>平台优势</h1>
                            <p>our advantage</p>
                        </div>
                        <p className="explain">这是一段文字说明。这是一段文字说明。这是一段文字说明。这是一段文字说明。这是一段文字说明。这是一段文字说明。</p>
                    </div>
                </div>
            </div>
        );
    }

}

module.exports = connect(state => ({ passport: state.passport }))(Home);
