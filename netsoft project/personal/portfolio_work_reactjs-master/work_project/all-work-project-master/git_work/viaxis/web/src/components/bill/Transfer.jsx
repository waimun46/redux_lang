import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router'
import { message, notification, Form, Row, Col, Table, Badge, Button, Select, Input, DatePicker, Icon } from 'antd';
import moment from 'moment';

import BasicComponent from "../common/BasicComponent";
import DashContainer from '../../containers/DashContainer';
import SearchForm from "../common/SearchForm";

import * as PassportAction from '../../actions/passport';
import * as BillAction from "../../actions/bill";
import * as Constants from "../../app_constants";
import * as Utils from "../../utils/common";
import * as RenderHelper from "../../utils/render_helper";


class TransferList extends BasicComponent {
    constructor(props) {
        super(props);
        this.querys = {}
    }

    /* ---------------------------- System  ------------------------------*/

    componentWillMount() {
        this.queryData();
    }

    /* ---------------------------- Functions  ------------------------------*/

    queryData() {
        this.props.dispatch(BillAction.queryTransfer(this.querys));
    }

    /* ---------------------------- Events  ------------------------------*/

    onSearchClick(values) {

        this.querys = values;
        this.queryData();
    }

    onResetClick() {
        this.querys = {};
    }


    /* ---------------------------- Renders  ------------------------------*/

    render() {

        const self = this;
        const { transfer } = this.props;
        let data = transfer.data || [];

        const pagination = {
            total: transfer.count || 0,
            current: transfer.pageIndex,
            onChange(current) {
                self.querys.pageIndex = current;
                self.queryData();
            },
        };

        return (
            <DashContainer title="转账记录">
                {this.renderSearch()}
                <div className="search-result-list">
                    <Table loading={transfer.isFetching} columns={this.columns} dataSource={data} rowKey="Id" pagination={pagination} />
                </div>
            </DashContainer>
        );
    }

    renderSearch() {
        const formItemLayout = {
            labelCol: { span: 8 },
            wrapperCol: { span: 16 }
        };

        return (
            <SearchForm onSearch={this.onSearchClick.bind(this)} onReset={this.onResetClick.bind(this)}>
                <to_email label="转入邮箱" {...formItemLayout}>
                    <Input />
                </to_email>
                <amount label="金额" {...formItemLayout}>
                    <Input />
                </amount>
                <create_time label="转账时间" {...formItemLayout} format={(times) => Utils.dateRangeFormat(times)}>
                    <DatePicker.RangePicker
                        ranges={{ '今天': [moment(), moment()], '本月': [moment(), moment().endOf('month')] }} />
                </create_time>
            </SearchForm>
        );
    }

    get columns() {
        const user = this.props.user.data;
        const columns = [
            { title: '#', render: RenderHelper.render_index },
            { title: "转出用户", dataIndex: "FromEmail" },
            {
                title: "金额", dataIndex: "Amount", className: 'column-money', render: (amt, it) => {
                    if (user && user.UserId == it.FromUserId)
                        return RenderHelper.money_format_color(amt * -1);
                    return RenderHelper.money_format(amt);
                }
            },
            { title: "转入用户", dataIndex: "ToEmail" },
            { title: "备注", dataIndex: "Remark" },
            { title: "转账时间", dataIndex: "CreateTime", render: RenderHelper.render_time },
        ];
        return columns;
    }
}

export default connect(state => ({
    user: state.passport.userinfo,
    transfer: state.bill.transfer
}))(TransferList);


class Transfer extends BasicComponent {

    state = {
        transferWay: 1,
        loading: false,
        // 是否已设置安全密码
        hasSafePassword: true,
    }

    /* ----------------- Sys -----------------*/

    componentWillMount() {
        this.queryHasSafePassword();
    }

    componentWillUpdate(nextProps, nextState) {
        // 未设置安全密码
        if (nextState.hasSafePassword != this.state.hasSafePassword && !nextState.hasSafePassword) {
            RenderHelper.warning("您未设置安全密码", "您需要设置安全密码后才可继续下列的操作", () => {
                const { destroy } = this.props;
                destroy && destroy();
                this.goto("/passport/password/safe?redirect=/user");
            });
        }
    }

    /* ----------------- Functions ----------------- */
    queryHasSafePassword() {
        this.props.dispatch(PassportAction.hasSafePassword()).then(it => {

            const resp = it.response;
            if (resp.code == 0) {
                this.setState({ hasSafePassword: resp.data });
            }
        });
    }

    /* ----------------- Events ---------------- */

    handleSubmit(e) {
        e.preventDefault();

        const { dispatch, parent } = this.props;

        this.props.form.validateFields((errors, values) => {
            if (errors) return;

            const { balance, targetUser } = this.props;
            const { transferWay } = this.state;

            const amount = parseFloat(values.amount);

            // if (transferWay == 0 && amount > targetUser.Balance) {
            //     message.error(`转账金额不能超过${targetUser.Balance}`);
            //     return;
            // }
            if (amount > balance.data) {
                message.error(`转账金额不能超过${balance.data}`);
                return;
            }

            // const msg = transferWay == 0 ? `您将从${targetUser.Email}转入${amount}` : `您将转出${amount}到${targetUser.Email}`;
            const msg = `您将转出${amount}到${targetUser.Email}`;

            RenderHelper.confirm(msg, "您确定要继续此操作吗？", () => {
                this.setState({ loading: true });
                this.props.dispatch(BillAction.transfer(targetUser.UserId, amount, values.remark, values.password))
                    .then(act => {
                        this.setState({ loading: false });
                        this.assert(act, {
                            fnOk: (resp) => {
                                notification.open({
                                    message: "转账成功",
                                    description: <div><p>您的操作已成功提交。</p>你可以在 <a onClick={() => this.goto("/transfer/history")}>转账历史</a> 查看。</div>,
                                    icon: <Icon type="smile-circle" style={{ color: '#108ee9' }} />
                                });
                                this.props.form.resetFields();
                                //提现成功则刷新当前帐号余额
                                dispatch(BillAction.getBalance())
                                this.props.onCompleted && this.props.onCompleted();
                            }
                        });
                    });
            });
        });
    }


    /* ---------------- Renders ---------------- */

    render() {
        const { getFieldDecorator } = this.props.form;

        const formItemLayout = {
            labelCol: { span: 6 },
            wrapperCol: { span: 14 },
        };

        const tailFormItemLayout = {
            wrapperCol: {
                span: 14,
                offset: 6,
            },
        };

        const leftStyle = { lineHeight: "32px", textAlign: "right" };
        const rightStyle = { lineHeight: "32px", textAlign: "left" };

        const { balance, targetUser } = this.props;

        return (
            <Form onSubmit={this.handleSubmit.bind(this)} autoComplete="off">
                <Row gutter={8}>
                    <Col span={8} style={leftStyle}>
                        我的余额
                    </Col>
                    <Col span={8} offset={8} style={rightStyle}>
                        {targetUser.Email}余额
                    </Col>
                </Row>
                <Row gutter={8}>
                    <Col span={8} style={leftStyle}>
                        {RenderHelper.money_format(balance.data)}
                    </Col>
                    <Col span={8}>
                        <Form.Item>
                            {getFieldDecorator('amount', {
                                rules: [{ required: true, message: '请输入要转账的金额。' }],
                            })(
                                <Input />
                            )}
                        </Form.Item>
                    </Col>
                    <Col span={8} style={rightStyle}>
                        {RenderHelper.money_format(targetUser.Balance)}
                    </Col>
                </Row>
                <Form.Item label="安全密码" {...formItemLayout}>
                    {getFieldDecorator('password', {
                        rules: [{
                            required: true,
                            message: `请输入安全密码`
                        }],
                    })(
                        <Input type="password" />
                    )}
                </Form.Item>
                <Form.Item label="说明" {...formItemLayout}>
                    {getFieldDecorator('remark', {
                        rules: [{
                            required: true,
                            max: 20,
                            message: `请输入转账说明`,
                        }],
                    })(
                        <Input />
                    )}
                </Form.Item>
                <Form.Item {...tailFormItemLayout}>
                    <Row gutter={8}>
                        {/* <Col span={8} offset={2}>
                            <Button loading={this.state.loading} htmlType="submit" className="fn-width100" type="primary" size="large" onClick={() => this.setState({ transferWay: 0 })}>
                                <Icon type="swap-left" />转入
                            </Button>
                        </Col> */}
                        <Col span={8} offset={2}>
                            <Button loading={this.state.loading} htmlType="submit" className="fn-width100" type="danger" size="large">
                                转出<Icon type="swap-right" />
                            </Button>
                        </Col>
                    </Row>
                </Form.Item>
            </Form>
        );

    }
}

const transferForm = Form.create()(Transfer);

export {
    transferForm as Transfer
};
