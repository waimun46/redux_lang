import uniq from "lodash.uniq";

/**
 * 阶乘Factorial定义为：
 *      ┌ 1        n=0       
 *   N!=│    
 *      └ n(n-1)!  n>0
 * @param  {[type]} number [description]
 * @return {[type]}        [description]
 */
export function factorial(number) {
	if (number != 0) {
		return number * factorial(number - 1);
	} else if (number == 0) {
		return 1;
	}
	return number;
}

/**
 * 组合,從 n 個相異物中不重複的取出 m 個之組合數。
 * http://latex.codecogs.com/gif.latex?_nC_m={n%20\choose%20m}=\frac{_nP_m}{m!}=\frac{n!}{m!(n-m)!}
 * @param  {[type]} n 总数量
 * @param  {[type]} m [description]
 * @return {[type]}   取出的数量
 */
export function combinatorics(n, m) {
	if (n < m) return 0;
	return factorial(n) / (factorial(m) * factorial(n - m));
}


/**
 * 将多个数组进行合并排列
 * @param  {...[type]} arrays [description]
 * @return {[type]}           [description]
 */
export function permutation(duplicate, ...arrays) {
    if (arrays.length < 1) return arrays;
    var data = arrays[arrays.length - 1];
    for (let i = arrays.length - 2; i >= 0; i--) {
        let arr = arrays[i];
        data = mergeArray(arrays[i], data);
    }
    if (!duplicate)
        data = data.filter(it => uniq(it).length == it.length);
    return data;
}

/**
 * 将两个数组进行排列合并
 * @param  {[type]} n      可以是数组也可以是单个元素
 * @param  {[type]} subArr 要合并的数组
 * @return {[type]}        [description]
 */
export function mergeArray(n, subArr) {
    let data = [];
    if (Array.isArray(n)) {
        for (let i = 0; i < n.length; i++)
            data = [...data, ...mergeArray(n[i], subArr)];
    } else {

        for (let i = 0; i < subArr.length; i++) {
            if (Array.isArray(subArr[i]))
                data.push([n, ...subArr[i]]);
            else
                data.push([n, subArr[i]]);
        }
    }

    return data;
}
