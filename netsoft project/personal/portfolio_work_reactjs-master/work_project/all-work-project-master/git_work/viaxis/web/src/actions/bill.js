import $ from "jquery";
import { ActionState } from "../app_constants";
import { POST, GET } from "../utils/common";
import getSocket from "../websocket";

const socket = getSocket();

export function getBalance() {
    return {
        name: "Balance",
        callAPI: () => GET("/api/balance")()
    };
}

/* ---------------- BillFlow ---------------- */

/**
 * 获取流水记录
 * @param  {[type]} querys [description]
 * @return {[type]}        [description]
 */
export function queryBillflow(querys) {
    var body = JSON.stringify(querys);
    return {
        name: "BillFlow",
        callAPI: () => POST(`/api/billflow`, body)()
    };
}

/* --------------- Transfer ---------------- */

/**
 * 转账
 * @param  {[type]} userId   [description]
 * @param  {[type]} way      [description]
 * @param  {[type]} amount   [description]
 * @param  {[type]} remark   [description]
 * @param  {[type]} password [description]
 * @return {[type]}          [description]
 */
export function transfer(userId, amount, remark, password) {
    var body = JSON.stringify({
        ToUserId: userId, amount, remark, password
    });
    return {
        name: "Transfer",
        callAPI: () => POST(`/api/transfer`, body)()
    };
}

/**
 * 获取转账记录
 * @param  {[type]} querys [description]
 * @return {[type]}        [description]
 */
export function queryTransfer(querys) {
    var body = JSON.stringify(querys);
    return {
        name: "TransferHistory",
        callAPI: () => POST(`/api/transfer/history`, body)()
    };
}

/* --------------- Withdraw ---------------- */

/**
 * 获取提现信息
 * @return {[type]} [description]
 */
export function getWithdrawInfo() {
    return {
        name: "WithdrawInfo",
        callAPI: () => POST("/api/withdraw/info")()
    };
}

/**
 * 添加提现申请
 * @param {[type]} bankId   用户银行卡Id
 * @param {[type]} amount   提现金额
 * @param {[type]} password 安全密码
 */
export function addWithdraw(bankId, amount, password) {
    var body = JSON.stringify({
        bank_id: bankId,
        amount,
        password
    });
    return {
        name: "WithdrawAdd",
        callAPI: () => POST("/api/withdraw/add", body)()
    }
}

/**
 * 获取提现记录
 * @param  {dict} querys 查询条件
 * @return {[type]}        [description]
 */
export function queryWithdraw(querys) {
    var body = JSON.stringify(querys);
    return {
        name: "WithdrawHistory",
        callAPI: () => POST(`/api/withdraw/history`, body)()
    };
}


/* ------------------- Deposit ------------------- */


/**
 * 获取充值信息
 * @return {[type]} [description]
 */
export function getDepositInfo() {
    return {
        name: "DepositInfo",
        callAPI: () => POST("/api/deposit/info")()
    };
}


/**
 * 获取充值封装数据
 * @return {[type]} [description]
 */
export function getDepositData(amount, com_bank_id) {
    var body = JSON.stringify({
        amount,
        com_bank_id
    });
    return {
        name: "Deposit",
        callAPI: () => POST("/api/deposit/deposit", body)()
    };
}

/**
 * 获取ips二维码
 * @return {[type]} [description]
 */
export function getDepositQrCode(amount, client_type, com_bank_id) {
    var body = JSON.stringify({
        amount,
        client_type,
        com_bank_id
    });
    return {
        name: "ScanPayDeposit",
        callAPI: () => POST("/api/deposit/scanpaydeposit", body)()
    };
}
