import React, { Component } from 'react';
import BaseComponent from "./BaseComponent";
import {Link} from 'react-router-dom';

import Maybanklogo from '../../include/images/Maybanklogo.jpg';


class ThirdPartyBank extends BaseComponent {
  render() {
    return (
	
	<section id="Maybanklogo">
	<div className="form">
	<h1>Third Party Bank</h1>
 
 <div className="figure_style">
 <figure><img src={Maybanklogo}/></figure>
 </div>
 
 <form role="form" action="/login" method="POST" name="login" accept-charset="UTF-8" enctype="application/x-www-form-urlencoded" autocomplete="off">
    <div className="field-wrap">
      <div className="form-group input-group">
	  <span id="basic-addon2" className="input-group-addon">
	  <span className="glyphicon glyphicon-lock"></span></span>
        <input type="password" name="password" placeholder="Username" aria-describedby="basic-addon2" autocomplete="off" className="form-control" />
      </div>
    </div>
	
	 <div className="field-wrap">
      <div className="form-group input-group">
	  <span id="basic-addon2" className="input-group-addon">
	  <span className="glyphicon glyphicon-lock"></span></span>
        <input type="password" name="password" placeholder="Password" aria-describedby="basic-addon2" autocomplete="off" className="form-control" />
      </div>
    </div>
	
	
	 
	<Link to="./Home">
	<button type="button" className="btn btn-danger button button-block" onclick="logIn()">SUBMIT</button>
	</Link>
 </form>
	
	</div>
     </section>
    );
  }
}

export default ThirdPartyBank;

