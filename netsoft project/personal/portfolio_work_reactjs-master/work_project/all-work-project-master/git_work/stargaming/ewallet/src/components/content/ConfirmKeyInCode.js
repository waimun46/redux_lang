import React, { Component } from 'react';
import BaseComponent from "./BaseComponent";

import {Link} from 'react-router-dom';
import store from '../../store';

class ConfirmKeyInCode extends BaseComponent {
	
	constructor(props) {
		super(props);
		this.state = { 
			props: props,
			data: store.getContent()
		};
	}
	
  render() {
    return (
	
	<section id="ConfirmKeyInCode">
		<header className="header-same">
			<Link to="KeyInCode">
				<div className="back-btn">
					<i className="glyphicon glyphicon-menu-left"></i>
				</div>
			</Link>
			<div className="title-same">
				{this.state.data.content.confirmkeycode}
			</div>
		</header>

		
		<div className="chart">
			<h3>{this.state.data.content.amount}</h3>
			<div className="diagram">
				<h4>20.00</h4>
			</div>

			<div className="chart_list">
				<ul>
					<li className="red  chart-p"> 
						{this.state.data.content.usercode} :
						<p className="percentage">123456789</p>
					</li>
					<li className="purple">
						{this.state.data.content.username} :
						<p className="percentage">John</p>
					</li>
					
					<div className="clear"></div>
				</ul>
			</div>
			<div className="chart_list margin-style-keyin">
				<ul>
					<li className="yellow  chart-p chart-p-keyin">
						{this.state.data.content.name} :
						<p className="percentage">John Wong</p>
					</li>
					<li className="blue">
						{this.state.data.content.remark} :
						<p className="percentage">Reveive Money From Amber</p>
					</li>
					<div className="clear"></div>
				</ul>
			</div>
			
		</div>
		
		<div className="padding-btn button-align-setting">
			<Link to="History">
			<button type="button" className="btn btn-danger button button-block button_margintop">
				{this.state.data.content.submit}
			</button>
			</Link>
		</div>
		
		
	</section>
    );
  }
}

export default ConfirmKeyInCode;

