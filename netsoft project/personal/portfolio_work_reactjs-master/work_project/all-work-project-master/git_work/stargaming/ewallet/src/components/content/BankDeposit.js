import React, { Component } from 'react';
import BaseComponent from "./BaseComponent";

import {Link} from 'react-router-dom';
import store from '../../store';


import { Select } from 'antd';
const Option = Select.Option;

class BankDeposit extends BaseComponent {
	
	constructor(props) {
		super(props);
		this.state = { 
			props: props,
			data: store.getContent()
		};
	}
	
	
  render() {
    return (
	
	<section id="BankDeposit">
	
		<header className="header-same">
				<Link to="Deposit">
					<div className="back-btn">
						<i className="glyphicon glyphicon-menu-left"></i>
					</div>
				</Link>
				<div className="title-same">
					{this.state.data.content.bankdeposit}
				</div>
		</header>
	
		<div className="form">	   
			<div className="select_margin">
				<Select className="select_style"  placeholder={this.state.data.content.selectaccount} >
					<Option value="CIMB - 123456789">CIMB - 123456789</Option>
					<Option value="PublicBank - 987654321">PublicBank - 987654321</Option>
					<Option value="Maybank - 123456789012">Maybank - 123456789012</Option>
				</Select>

			</div>

			<form role="form" action="/login" method="POST" name="login" accept-charset="UTF-8" enctype="application/x-www-form-urlencoded" autocomplete="off">
				<div className="field-wrap input_width">
					<ul>
						<li>{this.state.data.content.amount} :</li>
					</ul>
					<input type="text" name="text" placeholder="30" aria-describedby="basic-addon2" autocomplete="off" className="form-control" />
				</div>
				
				<div className="field-wrap input_style textarea_style">
					<ul>
						<li>{this.state.data.content.remark} :</li>
					</ul>
					<textarea rows="5" cols="10" placeholder={this.state.data.content.maybanktransfertoCTS}  aria-describedby="basic-addon2" autocomplete="off" className="form-control"></textarea>
				</div>
			</form>
			<div className="button-align-setting">
				<Link to="ConfirmDeposit" >	 
					<button type="button" className="btn btn-danger button button-block button_margintop">
						{this.state.data.content.submit} 
					</button>
				</Link>		
			</div>			
		</div>
	</section>  
    );
  }
}

export default BankDeposit;

