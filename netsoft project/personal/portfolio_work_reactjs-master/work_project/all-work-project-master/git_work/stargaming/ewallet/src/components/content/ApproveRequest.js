import React, { Component } from 'react';
import BaseComponent from "./BaseComponent";

import {Link} from 'react-router-dom';




class ApproveRequest extends BaseComponent {
	
	
  render() {
    return (
	
	<section id="ConfirmSendMoney">
	<div className="form">
	<h1>Approve Request</h1>
   
 <div>
		 <ul>
		 <li className="lineHeight_style">Username :</li>
		 <li className="lineHeight_style2">Amber</li>
		 <li className="lineHeight_style">Phone Number :</li>
		 <li className="lineHeight_style2">01234567890</li>
		 <li className="lineHeight_style">Amount :</li>
		 <li className="lineHeight_style2">20.00</li>
		 <li className="lineHeight_style">Remark :</li>
		 <li className="lineHeight_style2">Need Money</li>
		 </ul>
	</div>


	 <form role="form" action="/login" method="POST" name="login" accept-charset="UTF-8" enctype="application/x-www-form-urlencoded" autocomplete="off">
		 
		<div className="field-wrap security_style">
			 <input type="password" name="password" placeholder="Security Code" aria-describedby="basic-addon2" autocomplete="off" className="form-control" />
			</div>
			
			
		
			<Link to="./Home">	 
				<button type="button" className="btn btn-danger button button-block button_margintop" onclick="logIn()">YES</button>
			 </Link>
		</form>
		
		</div>
	   </section>  
    );
  }
}

export default ApproveRequest;

