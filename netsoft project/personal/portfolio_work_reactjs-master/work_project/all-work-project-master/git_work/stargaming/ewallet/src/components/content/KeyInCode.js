import React, { Component } from 'react';
import BaseComponent from "./BaseComponent";
import {Link} from 'react-router-dom';
import store from '../../store';


class KeyInCode extends BaseComponent {
	
	constructor(props) {
		super(props);
		this.state = { 
			props: props,
			data: store.getContent()
		};
	}
	
  render() {
    return (
		<section id="KeyInCode" className="same-form-style">
			
			<header className="header-same">
				<Link to="Home">
					<div className="back-btn">
						<i className="glyphicon glyphicon-menu-left"></i>
					</div>
				</Link>
				<div className="title-same">{this.state.data.content.keyincode}</div>						
			</header>
		
			<div className="form">
				<form role="form" action="/login" method="POST" name="login" accept-charset="UTF-8" enctype="application/x-www-form-urlencoded" autocomplete="off">
					<div className="field-wrap input_width">
						<ul>
							<li>{this.state.data.content.usercode} :</li>
						</ul>
						<input type="text" name="text"  aria-describedby="basic-addon2" autocomplete="off" className="form-control" />
					</div>
					
					<div className="field-wrap input_width">
						<ul>
							<li>{this.state.data.content.amount} :</li>
						</ul>
						<input type="text" name="text"  aria-describedby="basic-addon2" autocomplete="off" className="form-control" />
					</div>
					
					<div className="field-wrap input_style textarea_style">
						<ul>
							<li>{this.state.data.content.remark} :</li>
						</ul>
						<textarea rows="5" cols="10" aria-describedby="basic-addon2" autocomplete="off" className="form-control"></textarea>
					</div>
				</form>
				<Link to="ConfirmKeyInCode">
					<div className="button-align-setting">
						<button type="button" className="btn btn-danger button button-block">{this.state.data.content.submit}</button>
					</div>
				</Link>
			</div>
		</section>
    );
  }
}

export default KeyInCode;

