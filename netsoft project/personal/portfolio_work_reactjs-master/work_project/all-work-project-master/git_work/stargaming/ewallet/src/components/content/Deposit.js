import React, { Component } from 'react';
import BaseComponent from "./BaseComponent";
import raect from 'react-dom'

import {Link} from 'react-router-dom';
import store from '../../store';


class Deposit extends BaseComponent {
	
	constructor(props) {
		super(props);
		this.state = { 
			props: props,
			data: store.getContent()
		};
	}
	
  render() {
    return (
	<section id="deposit">
	
		<article className="profile-w3layouts deposit">
			
				<header className="header-same">
					<Link to="Balance">
						<div className="back-btn">
							<i className="glyphicon glyphicon-menu-left"></i>
						</div>
					</Link>
					<div className="title-same">{this.state.data.content.deposit}</div>						
				</header>			
				
				
				<div className="profile-ser_2">
					<div className="profile-ser-grids border-bottom">
						<Link to="BankDeposit">
							<span className="fa fa-university deposit" aria-hidden="true"></span>
							<h4>{this.state.data.content.bankdeposit}</h4>
							<i className="fa fa-angle-right" aria-hidden="true"></i>
						</Link>
					</div>
                    <div className="profile-ser-grids agileinfo">
                        <Link to="paymentgateway">
                            <span className="fa fa-university deposit" aria-hidden="true"></span>
                            <h4>{this.state.data.content.topupgateway}</h4>
                            <i className="fa fa-angle-right" aria-hidden="true"></i>
                        </Link>
                    </div>
					<div className="profile-ser-grids agileinfo">
						<Link to="TopupCardDeposit">
							<span className="fa fa-credit-card withdraw" aria-hidden="true"></span>
							<h4>{this.state.data.content.topupcarddeposit}</h4>
							<i className="fa fa-angle-right" aria-hidden="true"></i>
						</Link>
					</div>

					<div className="clearfix"> </div>
				</div>
			</article>
	
	</section>
	
    );
  }
}

export default Deposit;
