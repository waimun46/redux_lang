import React, { Component } from 'react';
import raect from 'react-dom'

import { Row, Col } from 'antd';
import {Link} from 'react-router-dom';
import store from '../store';
import PageTransition from 'react-router-page-transition';


import scanqr from '../include/images/scanqr.png';
import generateqr from '../include/images/generateqr.png';
import keyincode from '../include/images/keyincode.png';
import history from '../include/images/history.png';
import bill from '../include/images/bill.png';
import lend from '../include/images/lend.png';
import applyloan from '../include/images/applyloan.png';
import loansummary from '../include/images/loansummary.png';
import loanactivity from '../include/images/loanactivity.png';

class Homecontent extends Component {
	constructor(props) {
		super(props);
		this.state = { 
			props: props,
			data: store.getContent()
		};
	}

    componentWillMount = function() {
        store.addChangeListener(this._onChange);
    }

    componentWillUnmount = function() {
        store.removeChangeListener(this._onChange);
    }

    _onChange = () => {
        this.setState({ data: store.getContent() });
    }
	
  render() {
    return (

        <article className="home-content">

			<header className="select-top">
				<i className="glyphicon glyphicon-folder-open"></i>
				<span>{this.state.data.content.menu}</span>
			</header>
			
			<div className="receive-money">
				<div className="home-select">
					<header>
					<i className="glyphicon glyphicon-gift"></i>
						<span>{this.state.data.content.receivemoney}</span>
					</header>
				</div>
			
				<Row className="home-icon">
					<Col span={6}>
						<img src={scanqr}/>
						<li>{this.state.data.content.scanQR}</li>
					</Col>
					
					<Link to="ConfirmGenerateQRCode">
						<Col span={6}>
							<img src={generateqr}/>
							<li>{this.state.data.content.generateQR}</li>
						</Col>
					</Link>
					
					<Link to="KeyInCode">
						<Col span={6}>
							<img src={keyincode}/>
							<li>{this.state.data.content.keyincode}</li>
						</Col>
					</Link>
					
					<Link to="History">
						<Col span={6}>
							<img src={history}/>
							<li>{this.state.data.content.history}</li>
						</Col>
					</Link>
				</Row>
			
			</div>
			
			
			<div className="payment">
				<div className="home-payment">
					<header>
					<i className="glyphicon glyphicon-list-alt"></i>
						<span>{this.state.data.content.payment}</span>
					</header>
				</div>
				
				<Row className="home-icon">
					<Link to="PayBill">
						<Col span={6}>
							<img src={bill}/>
							<li>{this.state.data.content.bill}</li>
						</Col> 
					</Link>
				</Row>
			
			</div>
			
			<div className="social-loan">
				<div className="home-payment">
					<header>
					<i className="glyphicon glyphicon-globe"></i>
						<span>{this.state.data.content.socialloan}</span>
					</header>
				</div>
				
				<Row className="home-icon">
					<Link to="Lend">
						<Col span={6}>
							<img src={lend}/>
							<li>{this.state.data.content.lend}</li>
						</Col>
					</Link>
					
					<Link to="ApplyLoan">
						<Col span={6}>
							<img src={applyloan}/>
							<li>{this.state.data.content.applyloan}</li>
						</Col>
					</Link>
					<Link to="LoanSummary">
						<Col span={6}>
							<img src={loansummary}/>
							<li>{this.state.data.content.loansummary}</li>
						</Col>
					</Link>
					<Link to="LoanActivity">
						<Col span={6}>
							<img src={loanactivity}/>
							<li>{this.state.data.content.loanactivity}</li>
						</Col>
					</Link>
				</Row>
			
			</div>
			
		
			

        </article>
		
    );
  }
}

export default Homecontent;
