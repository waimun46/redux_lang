import React, { Component } from 'react';
import BaseComponent from "./content/BaseComponent";
import raect from 'react-dom'
import { Row, Col } from 'antd';
import {Link} from 'react-router-dom';
import actions from '../actions';
import store from '../store';
import chinese from '../include/images/chinese.png';
import english from '../include/images/english.png';
import malay from '../include/images/malay.png';

class Language extends BaseComponent {
	
	constructor(props) {
		super(props);
		this.state = { 
			props: props,
			data: store.getContent()
		};
	   }
	   
		
  render() {
    return (
	
		<section id="language">
			
				<header className="header-same">
					<Link to="./">
						<div className="back-btn">
							<i className="glyphicon glyphicon-menu-left"></i>
						</div>
					</Link>
					<div className="title-same">{this.state.data.content.language}</div>
						
				</header>
		
			<div className="lang-icon">
				<div className="icon chinese">
					<a href="#" data-target="#" onClick={switchLanguage.bind(this,'ch')}>
					<img src={chinese}/>
						<li>{this.state.data.content.chinese}</li>
					</a>
				</div>
				
				<div className="icon english">
					<a href="#" data-target="#" onClick={switchLanguage.bind(this,'en')}>
					<img src={english}/>
						<li>{this.state.data.content.english}</li>
					</a>
				</div>
				
				<div className="icon malay">
					<a href="#" data-target="#" onClick={switchLanguage.bind(this,'my')}>
					<img src={malay}/>
						<li>{this.state.data.content.malay}</li>
					</a>
				</div>
			</div>
	  	</section>
		
	 
    );
  }
}

function switchLanguage(key, event) {

	actions.switchLanguage(key, this.state.data.content.session);
}

export default Language;
