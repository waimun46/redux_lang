import React, { Component } from 'react';
import BaseComponent from "./BaseComponent";

import {Link} from 'react-router-dom';


class ConfirmPaymentQRCode extends BaseComponent {
	
	
  render() {
    return (
	
	<section id="ConfirmSendMoney">
	<div className="form">
	<h1>Confirm Payment QR Codee</h1>
   
		<div>
			 <ul>
			 <li className="lineHeight_style">To :</li>
			 <li className="lineHeight_style2">John</li>
			 <li className="lineHeight_style">Amount :</li>
			 <li className="lineHeight_style2">20.00</li>
			 <li className="lineHeight_style">Remark :</li>
			 <li className="lineHeight_style3">Money Transfer</li>
			</ul>
		</div>


		 <Link to="./Home">	 
				<button type="button" className="btn btn-danger button button-block button_margintop" onclick="logIn()">MAKE PAYMENT</button>
			 </Link>
	
		
		</div>
	   </section>  
    );
  }
}

export default ConfirmPaymentQRCode;

