import React, { Component } from 'react';
import BaseComponent from "./BaseComponent";

import {Link} from 'react-router-dom';
import store from '../../store';

class ConfirmDeposit extends BaseComponent {
	
	constructor(props) {
		super(props);
		this.state = { 
			props: props,
			data: store.getContent()
		};
	}
	
	
  render() {
    return (
	<section id="ConfirmDeposit">
		<header className="header-same">
			<Link to="BankDeposit">
				<div className="back-btn">
					<i className="glyphicon glyphicon-menu-left"></i>
				</div>
			</Link>
			<div className="title-same">
				{this.state.data.content.confirmdeposit}
			</div>
		</header>

		
		<div className="chart">
			<h3>{this.state.data.content.amount}</h3>
			<div className="diagram">
				<h4>30.00</h4>
			</div>

			<div className="chart_list">
				<ul>
					<li className="red  chart-p"> 
						{this.state.data.content.bank} :
						<p className="percentage">Maybank - 123456789012</p>
					</li>
					<li className="purple">
						{this.state.data.content.remark} :
						<p className="percentage">Maybank Transfer to CTS</p>
					</li>
					
					<div className="clear"></div>
				</ul>
			</div>
			
			
		</div>
		
		<div className="padding-btn button-align-setting">
					<Link to="Security">	
						<button type="button" className="btn btn-danger button button-block button_margintop">
							{this.state.data.content.submit}
						</button>
					</Link>
			</div>
		
		
	</section>
    );
  }
}

export default ConfirmDeposit;

