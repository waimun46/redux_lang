import React, { Component } from 'react';
import BaseComponent, {CollectionCreateForm} from "./BaseComponent";
import {Link} from 'react-router-dom';
import store from '../../store';
import { Select } from 'antd';
import {PostData} from "../services/PostData";
import $ from "jquery";
import ConfirmAddBank from './ConfirmAddBank';

const Option = Select.Option;

const reactStringReplace = require('react-string-replace');



class AddBank extends BaseComponent {
	
	constructor(props) {
		super(props);
		this.state = { 
			props: props,
			data: store.getContent(),
            cookit: document.cookie,
			list:[],
            bankid : '',

		};
	}

    handleChange = (event) => {
        console.log(event);
        this.setState({ bankid : event});
    }

    componentDidMount = () => {

        let session = this.state.cookit.replace(/(?:(?:^|.*;\s*)session\s*\=\s*([^;]*).*$)|^.*$/, "$1");
        let params = {
            'action': 'addBankAccount',
            'session': session
        };
        PostData('BankAccount', params, this.props).then((result) => {
            if (result.status == 'ok' && result.data != '') {
                console.log(result);
                this.setState({
                    list: result.data.list,
                })
            } else {
                this.setState({
                    validationerror: this.state.data.content[result.error]
                });
            }
        });
    }

    submit = () => {
	    let session = document.cookie.replace(/(?:(?:^|.*;\s*)session\s*\=\s*([^;]*).*$)|^.*$/, "$1");
        let bankId  = this.state.bankid;
        let name 	= $('input[name="name"]').val();
        let account = $('input[name="account"]').val();
        let params 	= {
                'action': 'updateInit',
                'account' : account,
                'bank' : bankId,
                'name' : name,
                'session': session,
            };

        console.log(params);
        PostData('BankAccount',params ,this.props).then((result) => {
            console.log(result);
            if(result.status == 'ok'){
                console.log(result);
                this.setState({
                    prefix : result.data.prefix,
                    name : result.data.name,
                    account : result.data.account,
                    bankid :result.data.bank,
                    bankname : result.data.bankname,
                    validationerror : '',
                });
            }else{
                this.setState({
                    validationerror: this.state.data.content[result.code]
                });
            }
        });
    }

    verifysend = () => {

        let session  = document.cookie.replace(/(?:(?:^|.*;\s*)session\s*\=\s*([^;]*).*$)|^.*$/, "$1");
        let prefix   = this.state.prefix;
        let name     = this.state.name;
        let account  = this.state.account;
        let bankid   = this.state.bankid;
        let password = $('input[name="password"]').val();
        let params = {
            'action': 'updateSubmit',
            'name' : name,
            'account' : account,
            'bank' : bankid,
            'otp': prefix + '-' + password,
            'session': session
        };


        console.log(params);
        PostData('BankAccount',params ,this.props).then((result) => {
            console.log(result);
            if(result.status == 'ok'){
                console.log(result);
                this.setState({
                    successVisible : true,
                })
            }else{
                this.setState({
                    validationerror: this.state.data.content[result.error]
                });
            }
        });
    }

    resendverifycode = () =>{

        let session = document.cookie.replace(/(?:(?:^|.*;\s*)session\s*\=\s*([^;]*).*$)|^.*$/, "$1");
        let params = {
            'action': 'resendVerify',
            'verifyAction': 'updateBankProfile',
            'session': session
        };
        console.log(params);
        PostData('BankAccount', params, this.props).then((result) => {
            console.log(result);
            if(result.status == 'ok') {
                this.setState({
                    prefix : result.data.prefix,
                });
            }else{
                this.setState({
                    validationerror: this.state.data.content[result.error]
                });
            }
        });

    }

    successChangeBank = () => {
        this.setState({
            validationerror : '',
            successVisible : false,
            visible : false,
        })

        this.props.history.push('/Home');
    }

	
	
	
	
    render() {
		
		const content = 'Hey my number is 555-555-5555.';

		
    return (
        this.state.prefix ?
            <div>
                <ConfirmAddBank prefix={this.state.prefix}
                                            name={this.state.name}
                                            account={this.state.account}
                                            bankname={this.state.bankname}
                                            bankid={this.state.bankid}
                                            validationerror={this.state.validationerror}
                                            onClick={this.verifysend}
                                            onReSendVerify={this.resendverifycode}/>
                <CollectionCreateForm
                visible={this.state.successVisible}
                onCancel={this.successChangeBank}
                onCreate={this.successChangeBank}
                btnLabel="OK"
                content="Add Bank Successfully."
                />
            </div>:
	
        <section id="AddBank">
            <header className="header-same">
                    <Link to="Bank">
                        <div className="back-btn">
                            <i className="glyphicon glyphicon-menu-left"></i>
                        </div>
                    </Link>
                    <div className="title-same">
                        {this.state.data.content.addbank}

					
						
						
                    </div>
            </header>
            <div className="form">
			

                <div className="">
                    <label className="text-danger">{this.state.validationerror}</label>
                </div>

                <form>
				 
						
                    <div className="select_margin">
                        <Select className="select_style"  placeholder={this.state.data.content.selectbank} onChange={this.handleChange}>
                            {
                            this.state.list.map((dynamicData,key) =>
                                 <Option value={dynamicData.bank}>{dynamicData.bankname}</Option>
                                )
                            }
                        </Select>
                    </div>

                    <div className="field-wrap input_width">
                        <ul>
                            <li>{this.state.data.content.holdername} :</li>
                        </ul>
                        <input type="text" name="name" aria-describedby="basic-addon2" autocomplete="off"  onChange={this.handleInputChange} value={this.state.name} className="form-control" />
                    </div>

                    <div className="field-wrap input_width">
                        <ul>
                            <li>{this.state.data.content.accountno} :</li>
                        </ul>
                        <input type="text" name="account" aria-describedby="basic-addon2" autocomplete="off"  onChange={this.handleInputChange} value={this.state.account} className="form-control" />
                    </div>

                </form>

                <div className="button-align-setting">
                    <button type="button" className="btn btn-danger button button-block button_margintop" onClick={this.submit}>{this.state.data.content.submit} </button>
                </div>
            </div>
        </section>
        );
    }
}

export default AddBank;

