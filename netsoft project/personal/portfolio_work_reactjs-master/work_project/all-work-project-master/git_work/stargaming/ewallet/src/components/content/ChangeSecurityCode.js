import React, { Component } from 'react';
import BaseComponent, {CollectionCreateForm} from "./BaseComponent";

import {PostData} from '../services/PostData';
import {Link} from 'react-router-dom';
import store from '../../store';
import $ from "jquery";

class ChangeSecurityCode extends BaseComponent {
	
	constructor(props) {
		super(props);
		this.state = { 
			props: props,
			data: store.getContent()
		};
	}
	
	changeTransPass = () => {

		 let session = document.cookie.replace(/(?:(?:^|.*;\s*)session\s*\=\s*([^;]*).*$)|^.*$/, "$1");
		 
		 let params = {
		   'action' : 'updateTransPass',
		   'oldtranspass': $('input[name="oldtranspass"]').val(),
		   'transpass': $('input[name="transpass"]').val(),
		   'conftranspass': $('input[name="conftranspass"]').val(),
		   'session': session,
		 };
		  
		  PostData('Profile', params, this.props).then((result) => {
			  if(result.status == 'ok') {
				  this.setState({
				      successVisible: true,
	              })
			  }else{
				  this.setState({
					  //validationerror: result.error
					  validationerror: this.state.data.content[result.error]
				  });
			  }
		  });
		  
	 }
	
	 successChangeTransPass = () => {
		 
		 this.setState({
				validationerror : '',
				successVisible : false,
        })
        
        this.props.history.push('/Home');
	}
	
  render() {
    return (

	
	<section id="ChangeSecurityCode">
	
	    <CollectionCreateForm
            visible={this.state.successVisible}
            onCancel={this.successChangeTransPass}
            onCreate={this.successChangeTransPass}
            btnLabel="OK"
            content="Security Code had Changed Successfully."
        />
	 
		<header className="header-same">
			<Link to="Home">
				<div className="back-btn">
					<i className="glyphicon glyphicon-menu-left"></i>
				</div>
			</Link>
			<div className="title-same">{this.state.data.content.changesecuritycode}</div>
						
		</header>
	
		<div className="form">		 
			<form>
				
				<div className="field-wrap">
					<div className="form-group input-group">
						<laber className="text-danger" >{this.state.validationerror}</laber>
	        		</div>
	        	</div>
			
				<div className="field-wrap">
					<div className="form-group input-group">
						<span id="basic-addon2" className="input-group-addon">
						<span className="glyphicon glyphicon-lock"></span></span>
						<input type="oldtranspass" name="oldtranspass" placeholder={this.state.data.content.currentsecuritycode} aria-describedby="basic-addon2" autocomplete="off" className="form-control" />
					</div>
				</div>
				
				<div className="field-wrap">
					<div className="form-group input-group">
						<span id="basic-addon2" className="input-group-addon">
						<span className="glyphicon glyphicon-lock"></span></span>
						<input type="transpass" name="transpass" placeholder={this.state.data.content.newsecuritycode} aria-describedby="basic-addon2" autocomplete="off" className="form-control" />
					</div>
				</div>
				
				
				<div className="field-wrap">
					<div className="form-group input-group">
						<span id="basic-addon2" className="input-group-addon">
						<span className="glyphicon glyphicon-lock"></span></span>
						<input type="conftranspass" name="conftranspass" placeholder={this.state.data.content.confirmsecuritycode} aria-describedby="basic-addon2" autocomplete="off" className="form-control" />
					</div>
				</div>
				<div className="button-align-setting">
					<button type="button" className="btn btn-danger button button-block" onClick={this.changeTransPass}>{this.state.data.content.submit}</button>
				</div>
			
				<p className="forgot">
					<Link to="/RequestVerifyCode">{this.state.data.content.forgetsecuritycode} ?</Link>
				</p>
			</form>
		</div>
    </section>
    );
  }
}

export default ChangeSecurityCode;

