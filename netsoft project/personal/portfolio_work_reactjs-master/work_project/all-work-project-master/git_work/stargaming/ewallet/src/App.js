import React, { Component } from 'react';
import react from 'react-dom'
//import CSSTransitionGroup from 'react-transition-group/CSSTransitionGroup';



import {
	BrowserRouter as Router, Route, Link
} from 'react-router-dom';

import Login from './components/login';
import Menu from './components/menu';
import Home from './components/home';
import Language from './components/language';
import Languagelogin from './components/language_login';
import Register from './components/content/Register';
import ForgetPassword from './components/content/ForgetPassword';
import Balance from './components/content/Balance';
import Deposit from './components/content/Deposit';
import TopupCardDeposit from './components/content/TopupCardDeposit';
import paymentgateway from './components/content/paymentgateway';
import MyWallet from './components/content/MyWallet';
import BillPayment from './components/content/BillPayment';
import PersonalProfile from './components/content/PersonalProfile';
import Verify from './components/content/Verify';
import ChangePassword from './components/content/ChangePassword';
import ConfirmSecurityCode from './components/content/ConfirmSecurityCode';
import ChangeSecurityCode from './components/content/ChangeSecurityCode';
import RequestVerifyCode from './components/content/RequestVerifyCode';
import Setting from './components/content/Setting';
import VerifySetting from './components/content/VerifySetting';
import ThirdPartyBankTransferConfirm from './components/content/ThirdPartyBankTransferConfirm';
import BankDeposit from './components/content/BankDeposit';
import ConfirmDeposit from './components/content/ConfirmDeposit';
import Withdraw from './components/content/Withdraw';
import ConfirmWithdraw from './components/content/ConfirmWithdraw';
import Security from './components/content/Security';
import ConfirmGenerateQRCode from './components/content/ConfirmGenerateQRCode';
import KeyInCode from './components/content/KeyInCode';
import ConfirmKeyInCode from './components/content/ConfirmKeyInCode';
import PayBill from './components/content/PayBill';
import ConfirmPaymentBill from './components/content/ConfirmPaymentBill';
import History from './components/content/History';
import Bank from './components/content/Bank';
import AddBank from './components/content/AddBank';
import ConfirmAddBank from './components/content/ConfirmAddBank';
import VerifyConfirmSecurityCode from './components/content/VerifyConfirmSecurityCode';
import VerifyForgetPassword from './components/content/VerifyForgetPassword';
import ConfirmForgotPassword from './components/content/ConfirmForgotPassword';
import Lend from './components/content/Lend';
import ConfirmPayment from './components/content/ConfirmPayment';
import LearnMore from './components/content/LearnMore';
import LoanSummary from './components/content/LoanSummary';
import ApplyLoan from './components/content/ApplyLoan';
import LoanActivity from './components/content/LoanActivity';





import Login2 from './components/login2';
import Table from './components/table';


import './include/css/bootstrap.min.css';
import './include/css/font-awesome.min.css';
import './include/css/normalize.css';
import './include/css/style.css';
import './include/css/slide.css';
import './include/css/component.css';
import './include/css/form_style.css';










class App extends Component {

	constructor(props) {
		super(props);
		this.state = { 
			props: props
		};
	  }

  render() {
	
		
    return (

<Router>
			
			<div className="App">
				<Route exact path='/' component={Login}/>
				<Route exact path='/Home' component={Home}/>
				<Route exact path='/Register' component={Register}/>
				<Route exact path='/ForgetPassword' component={ForgetPassword}/>
				<Route exact path='/Language' component={Language}/>
				<Route exact path='/Balance' component={Balance}/>
				<Route exact path='/Deposit' component={Deposit}/>
				<Route exact path='/TopupCardDeposit' component={TopupCardDeposit}/>
                <Route exact path='/paymentgateway' component={paymentgateway}/>
				<Route exact path='/PersonalProfile' component={PersonalProfile}/>
				<Route exact path='/Languagelogin' component={Languagelogin}/>
				<Route exact path='/Verify' component={Verify}/>
				<Route exact path='/ChangePassword' component={ChangePassword}/>
				<Route exact path='/ConfirmSecurityCode' component={ConfirmSecurityCode}/>
				<Route exact path='/ChangeSecurityCode' component={ChangeSecurityCode}/>
				<Route exact path='/RequestVerifyCode' component={RequestVerifyCode}/>
				<Route exact path='/Setting' component={Setting}/>
				<Route exact path='/VerifySetting' component={VerifySetting}/>
				<Route exact path='/ThirdPartyBankTransferConfirm' component={ThirdPartyBankTransferConfirm}/>
				<Route exact path='/ConfirmDeposit' component={ConfirmDeposit}/>
				<Route exact path='/BankDeposit' component={BankDeposit}/>
				<Route exact path='/Withdraw' component={Withdraw}/>
				<Route exact path='/ConfirmWithdraw' component={ConfirmWithdraw}/>
				<Route exact path='/Security' component={Security}/>
				<Route exact path='/ConfirmGenerateQRCode' component={ConfirmGenerateQRCode}/>
				<Route exact path='/KeyInCode' component={KeyInCode}/>
				<Route exact path='/MyWallet' component={MyWallet}/>
				<Route exact path='/ConfirmKeyInCode' component={ConfirmKeyInCode}/>
				<Route exact path='/PayBill' component={PayBill}/>
				<Route exact path='/BillPayment' component={BillPayment}/>
				<Route exact path='/ConfirmPaymentBill' component={ConfirmPaymentBill}/>
				<Route exact path='/History' component={History}/>
				<Route exact path='/Bank' component={Bank}/>
				<Route exact path='/AddBank' component={AddBank}/>
				<Route exact path='/ConfirmAddBank' component={ConfirmAddBank}/>
				<Route exact path='/VerifyConfirmSecurityCode' component={VerifyConfirmSecurityCode}/>
				<Route exact path='/VerifyForgetPassword' component={VerifyForgetPassword}/>
				<Route exact path='/ConfirmForgotPassword' component={ConfirmForgotPassword}/>
				<Route exact path='/Lend' component={Lend}/>
				<Route exact path='/ConfirmPayment' component={ConfirmPayment}/>
				<Route exact path='/LearnMore' component={LearnMore}/>
				<Route exact path='/LoanSummary' component={LoanSummary}/>
				<Route exact path='/ApplyLoan' component={ApplyLoan}/>
				<Route exact path='/LoanActivity' component={LoanActivity}/>
			
				
				
				
	
				
			
			</div>
	
		</Router>
		
	
    );
  }
}

export default App;
