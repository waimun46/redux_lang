import React, { Component } from 'react';
import BaseComponent from "./BaseComponent";
import {Link} from 'react-router-dom';

class VerifyConfirmSecurityCode extends BaseComponent {
  render() {
   
   return (
   

	<section id="VerifyConfirmSecurityCode">
	
		<header className="header-same">
	
			<div className="title-same">Verify</div>						
		</header>
	
		<div className="form">
			<form role="form" action="/login" method="POST" name="login">
				<div className="field-wrap security_style">
					<input  style={{ height: '40px' ,textAlign: "center" ,fontSize:"16px"}} type="password" name="password" placeholder=" *  *  *  *  *  *" aria-describedby="basic-addon2" autocomplete="off" className="form-control" />
				</div>
				<Link to="ConfirmSecurityCode">
				<div className="button-align-setting">
					<button type="button" className="btn btn-danger button button-block" onclick="logIn()">VERIFY</button>
				</div>
				</Link>
			</form>	
		</div>
     </section>
    );
  }
}

export default VerifyConfirmSecurityCode;

