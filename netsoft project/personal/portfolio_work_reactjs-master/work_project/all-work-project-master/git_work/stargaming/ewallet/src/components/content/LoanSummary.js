import React, { Component } from 'react';
import BaseComponent from "./BaseComponent";

import {Link} from 'react-router-dom';
import { Avatar } from 'antd';
import { Rate } from 'antd';
import { Pagination } from 'antd';
import { Select } from 'antd';
import store from '../../store';


const Option = Select.Option;

class LoanSummary extends BaseComponent {
	 state = {
     value: 1,
  }
  onChange = (e) => {
    console.log('radio checked', e.target.value);
    this.setState({
      value: e.target.value,
    });
  }
  
  constructor(props) {
		super(props);
		this.state = { 
			props: props,
			data: store.getContent()
		};
	}
	
 
 render() {
    return (
	
		<article id="tab-LoanSummary">

    <header className="header-same">
        <Link to="Home">
        <div className="back-btn">
            <i className="glyphicon glyphicon-menu-left"></i>
        </div>
        </Link>
        <div className="title-same">
            {this.state.data.content.loansummary}
        </div>
    </header>

    <div className="tab-style">
        <input id="tab1" type="radio" name="tabs" checked/>
        <label for="tab1">{this.state.data.content.lend}</label>

        <input id="tab4" type="radio" name="tabs" />
        <label for="tab4">{this.state.data.content.borrow}</label>

        <section id="content1">
            <article id="tab-style" className="tab-panel">
                <div className="table_style">

                    <div className="table_uploadco2">
                        <div className="table">

                            <div className="table-cell table-cellstyle">
                                <span>{this.state.data.content.lender} :</span>
                            </div>
                            <div className="table-cell table-cellstyle">
                                <span>Johnny</span>
                            </div>
                            <div className="table-cell">
                                <span>{this.state.data.content.totallenderamount} :</span>
                            </div>
                            <div className="table-cell">
                                <span>500.00</span>
                            </div>
                            <div className="table-cell">
                                <span>{this.state.data.content.totolreturn} :</span>
                            </div>
                            <div className="table-cell">
                                <span>10.00</span>
                            </div>

                            <div className="table-cell table-cellstyle">
                                <span>{this.state.data.content.lender} :</span>
                            </div>

                            <div className="table-cell table-cellstyle">
                                <span>Bryan</span>
                            </div>

                            <div className="table-cell">
                                <span>{this.state.data.content.totallenderamount} :</span>
                            </div>

                            <div className="table-cell">
                                <span>550.00</span>
                            </div>
                            <div className="table-cell">
                                <span> {this.state.data.content.totolreturn} :</span>
                            </div>
                            <div className="table-cell">
                                <span>10.00</span>
                            </div>

                        </div>
                    </div>

                </div>
            </article>
        </section>

        <section id="content4">
            <aside id="BORROW" className="tab-panel">
                <div className="table_style">

                    <div className="table_uploadco2">
                        <div className="table">

                            <div className="table-cell table-cellstyle">
                                <span>{this.state.data.content.borrower}  :</span>
                            </div>
                            <div className="table-cell table-cellstyle">
                                <span>John</span>
                            </div>
                            <div className="table-cell">
                                <span> {this.state.data.content.totalborrowamount} :</span>
                            </div>
                            <div className="table-cell">
                                <span>50.00</span>
                            </div>
                            <div className="table-cell">
                                <span>{this.state.data.content.totolreturn}  :</span>
                            </div>
                            <div className="table-cell">
                                <span>10.00</span>
                            </div>

                            <div className="table-cell table-cellstyle">
                                <span>{this.state.data.content.borrower} :</span>
                            </div>

                            <div className="table-cell table-cellstyle">
                                <span>Amy</span>
                            </div>

                            <div className="table-cell">
                                <span>{this.state.data.content.totalborrowamount} :</span>
                            </div>

                            <div className="table-cell">
                                <span>50.00</span>
                            </div>
                            <div className="table-cell">
                                <span>{this.state.data.content.totolreturn}  :</span>
                            </div>
                            <div className="table-cell">
                                <span>10.00</span>
                            </div>

                        </div>
                    </div>

                </div>
            </aside>
        </section>

        <div className="table-cell pagin-style">
            <Pagination defaultCurrent={1} total={30} className="pagination_style" />
        </div>

    </div>
</article>
    );
  }
}

export default LoanSummary;

