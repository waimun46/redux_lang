import React, { Component } from 'react';
import BaseComponent from "./BaseComponent";


class GenerateQR extends BaseComponent {
  render() {
    return (
	
<section id="ConfirmSendMoney">
	<div className="form">
	<h1>Generate QR</h1>
 
 <form role="form" action="/login" method="POST" name="login" accept-charset="UTF-8" enctype="application/x-www-form-urlencoded" autocomplete="off">
    <div className="field-wrap input_width">
	<ul>
	<li>Amount :</li></ul>
     <input type="text" name="text" placeholder="  " aria-describedby="basic-addon2" autocomplete="off" className="form-control" />
    </div>
	
	<div className="field-wrap input_style textarea_style">
	<ul>
	<li>Remark :</li></ul>
    <textarea rows="5" cols="10" placeholder=" "  aria-describedby="basic-addon2" autocomplete="off" className="form-control"></textarea>
    </div>
	

	<button type="button" className="btn btn-danger button button-block" onclick="logIn()">GENERATE QR CODE</button>
  </form>
	
	</div>
   </section>  
    );
  }
}

export default GenerateQR;

