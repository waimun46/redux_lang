<?php

$params = '{"password":"456","username":"android","email":"abc@hotmail.com","phone":"0172585597","otp":"111111","action":"register"}';
//$params = '{"username":"123","action":"verify","otp":"123456"}';
$params = json_decode($params, true);
ksort($params, SORT_REGULAR);

$iv = '123456';
$key = '123';

$params['signature'] = sha1(json_encode($params).$key.$iv);
$params['iv'] = $iv;

$opts = array(
    CURLOPT_URL => "http://localhost/ewalletmember/index.php/Member/Register",
    CURLOPT_CONNECTTIMEOUT => 30,
    CURLOPT_TIMEOUT => 30,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_USERAGENT => "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36",
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_MAXREDIRS => 5,
    CURLOPT_SSL_VERIFYPEER => false,
    CURLOPT_SSL_VERIFYHOST => false,
    CURLOPT_HEADER => true,
    CURLOPT_POST => true,
    CURLOPT_POSTFIELDS => json_encode($params),
);

$ch = curl_init();
curl_setopt_array($ch, $opts);
$response = curl_exec($ch);

$info = curl_getinfo($ch);
curl_close($ch);

if(substr($info['http_code'], 0, 1) === '2'){
    $ret = array(
        'header' => array(),
        'body' => substr($response,$info['header_size'])
    );

    $headers = explode("\r\n", substr($response, 0, $info['header_size']));
    array_shift($headers);

    foreach($headers as $v){
        if($v){
            $t = explode(":", $v, 2);
            if(isset($t[0]) && isset($t[1]))
                $ret['header'][strtoupper(trim($t[0]))] = trim($t[1]);
        }
    }
}else{
    $ret = false;
}

echo $ret['body'];