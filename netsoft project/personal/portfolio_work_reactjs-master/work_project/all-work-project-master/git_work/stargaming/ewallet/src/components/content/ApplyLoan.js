import React, { Component } from 'react';
import BaseComponent from "./BaseComponent";

import {Link} from 'react-router-dom';
import { Avatar } from 'antd';
import { Rate } from 'antd';
import { Input } from 'antd';
import { Upload, message, Button, Icon } from 'antd';
import { Checkbox } from 'antd';
import store from '../../store';


import { Select } from 'antd';
const Option = Select.Option;


const props = {
  name: 'file',
  action: '//jsonplaceholder.typicode.com/posts/',
  headers: {
    authorization: 'authorization-text',
  },
  onChange(info) {
    if (info.file.status !== 'uploading') {
      console.log(info.file, info.fileList);
    }
    if (info.file.status === 'done') {
      message.success(`${info.file.name} file uploaded successfully`);
    } else if (info.file.status === 'error') {
      message.error(`${info.file.name} file upload failed.`);
    }
  },
};



class ApplyLoan extends BaseComponent {
	 state = {
     value: 1,
  }
  onChange = (e) => {
    console.log('radio checked', e.target.value);
    this.setState({
      value: e.target.value,
    });
  }
	
	constructor(props) {
		super(props);
		this.state = { 
			props: props,
			data: store.getContent()
		};
	}
	
	
	
 
 render() {
    return (
	
	<section id="ApplyLoan">
	
		<header className="header-same">
			<Link to="Home">
				<div className="back-btn">
					<i className="glyphicon glyphicon-menu-left"></i>
				</div>
			</Link>
			<div className="title-same">
				{this.state.data.content.applyloan}
			</div>
		</header>
		
		<div className="form">
			<div className="table_loanstyle">
				<div className="icon_style">
					<Avatar size="large" icon="user" />
					<h4 className="icon_textstyle">John</h4>
				</div>

				<div className="table table_outline">
					<div className="table-cell lern-title">
						<span>{this.state.data.content.identitycardnumber} :</span>
					</div>

					<div className="table-cell upload-style">
						<Input className="inputupload_width" size="large" placeholder="" />
						<Upload {...props}>
							<Button className="upload_style">
								<Icon type="upload" />
							</Button>
						</Upload>
					</div>

					<div className="table-cell lern-title">
						<span>{this.state.data.content.amount} :</span>
					</div>
					<div className="table-cell">
						<Input className="inputupload2_width" size="large" placeholder="" />

					</div>

					<div className="table-cell lern-title">
						<span>{this.state.data.content.proposal} :</span>
					</div>

					<div className="table-cell table_uploadcol">
						<Upload {...props}>
							<Button>
								<Icon type="upload" /> Click to Upload
							</Button>
						</Upload>

					</div>

				</div>
			</div>

			<div className="text_color">
				<span>* {this.state.data.content.red1} '{this.state.data.content.red2}', 
		   '{this.state.data.content.red3}' {this.state.data.content.red4} '{this.state.data.content.red5}'.
		   <p>* {this.state.data.content.red6}...</p></span>

			</div>

			<Checkbox className="checkbox_style">
				{this.state.data.content.agree}.
			</Checkbox>
			
			<div className="button-align-setting">
				<Link to="LoanActivity">
					<button type="button" className="btn btn-danger button button-block btn_bottom">{this.state.data.content.submit}</button>
				</Link>
			</div>
		</div>
	</section>
    );
  }
}

export default ApplyLoan;

