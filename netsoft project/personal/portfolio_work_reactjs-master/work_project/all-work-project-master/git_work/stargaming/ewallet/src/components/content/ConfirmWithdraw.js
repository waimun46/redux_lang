import React, { Component } from 'react';
import BaseComponent from "./BaseComponent";

import {Link} from 'react-router-dom';
import store from '../../store';

class ConfirmWithdraw extends BaseComponent {
	
	constructor(props) {
		super(props);
		this.state = { 
			props: props,
			data: store.getContent()
		};
	}
	
	
  render() {
    return (
		<section id="ConfirmWithdraw">
			<header className="header-same">
				<Link to="Withdraw">
					<div className="back-btn">
						<i className="glyphicon glyphicon-menu-left"></i>
					</div>
				</Link>
				<div className="title-same">
					{this.state.data.content.confirmwithdraw}
				</div>
			</header>

			
			<div className="chart">
				<h3>{this.state.data.content.amount}</h3>
				<div className="diagram">
					<h4>15.00</h4>
				</div>

				<div className="chart_list">
					<ul>
						<li className="red  chart-p"> 
							{this.state.data.content.account} :
							<p className="percentage">PublicBank - 987654321</p>
						</li>
						<li className="purple">
							{this.state.data.content.remark} :
							<p className="percentage">{this.state.data.content.withdrawmoney}</p>
						</li>
						
						<li className="yellow chart-p-left">
							{this.state.data.content.servicecharge} :
							<p className="percentage">0.10</p>
						</li>
						
						<div className="clear"></div>
					</ul>
				</div>
				
				
			</div>
			
			<div className="padding-btn button-align-setting">
					<Link to="Security">	
						<button type="button" className="btn btn-danger button button-block button_margintop">
							{this.state.data.content.withdraw}
						</button>
					</Link>
			</div>
			
			
		</section>
    );
  }
}

export default ConfirmWithdraw;

