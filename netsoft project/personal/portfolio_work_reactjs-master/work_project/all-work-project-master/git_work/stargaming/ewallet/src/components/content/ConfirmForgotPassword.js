import React, { Component } from 'react';
import BaseComponent, {CollectionCreateForm} from "./BaseComponent";

import {PostData} from '../services/PostData';
import {Link} from 'react-router-dom';
import store from '../../store';
import $ from "jquery";
import sha1 from "sha1";


class ConfirmForgotPassword extends BaseComponent {
	
	constructor(props) {
		super(props);
		this.state = { 
			props: props,
            cookit: document.cookie,
			data: store.getContent(),
            phone : '',
            user : ''
		};
	}

    componentDidMount = () => {
        let username = this.state.cookit.replace(/(?:(?:^|.*;\s*)username\s*\=\s*([^;]*).*$)|^.*$/, "$1");
        let params = {
            'action': 'getUserDetail',
            'username': username
        };

        console.log(params);
        PostData('ForgotPass', params, this.props).then((result) => {
            if(result.status == 'ok') {
                this.setState({
                    phone: result.data['phone'],
                    user:username
                })
            }else{
                this.setState({
                    //validationerror: result.error
                    validationerror: this.state.data.content[result.error]
                });
            }
        });
    }

    submit = () => {
		let username = this.state.cookit.replace(/(?:(?:^|.*;\s*)username\s*\=\s*([^;]*).*$)|^.*$/, "$1");
		let password = $('input[name="password"]').val();
		let confpass = $('input[name="confpass"]').val();

		let params = {
			'action': 'confirmForgotPassword',
			'confpass': confpass,
			'password': password,
			'username': username
		};

        console.log(params);
		PostData('ForgotPass', params, this.props).then((result) => {
			if(result.status == 'ok') {
				this.setState({
					successVisible: true,
                })
			}else{
				this.setState({
					//validationerror: result.error
					validationerror: this.state.data.content[result.code]
				});
			}
		});
    }
    
    successChangePassword = () => {
		 this.setState({
				validationerror : '',
				successVisible : false,
				visible : false,
        })
        
        this.props.history.push('/');
	}

    render() {
    return (
	
	<section id="ConfirmForgotPassword" className="same-form-style">
	
	    <CollectionCreateForm
            visible={this.state.successVisible}
            onCancel={this.successChangePassword}
            onCreate={this.successChangePassword}
            btnLabel="OK"
            content="Password had Reset Successfully."
        />
	
		<header className="header-same">
			<Link to="PayBill">
			<div className="back-btn">
				<i className="glyphicon glyphicon-menu-left"></i>
			</div>
			</Link>
			<div className="title-same">
				{this.state.data.content.confirmforgotpassword}
			</div>
		</header>

		<div className="form">
            <div className="login-error">
                <label className="text-danger" >{this.state.validationerror}</label>
            </div>
			<form role="form" action="/login" method="POST" name="login" accept-charset="UTF-8" enctype="application/x-www-form-urlencoded" autocomplete="off">
				<div className="field-wrap input_width">
					<ul>
						<li>{this.state.data.content.username} :</li>
					</ul>
					<div className="span-style-text">
						<span>{this.state.user}</span>
					</div>
				</div>

				<div className="field-wrap input_width">
					<ul>
						<li>{this.state.data.content.phone} :</li>
					</ul>
					<div className="span-style-text">
						<span>{this.state.phone}</span>
					</div>
				</div>

				<div className="field-wrap ">
					<div className="form-group input-group">
						<span id="basic-addon2" className="input-group-addon">
		  <span className="glyphicon glyphicon-lock"></span></span>
						<input type="password" name="password" placeholder={this.state.data.content.password} aria-describedby="basic-addon2" autocomplete="off" className="form-control" />
					</div>
				</div>

				<div className="field-wrap">
					<div className="form-group input-group">
						<span id="basic-addon2" className="input-group-addon">
		  <span className="glyphicon glyphicon-lock"></span></span>
						<input type="password" name="confpass" placeholder={this.state.data.content.confirmpassword} aria-describedby="basic-addon2" autocomplete="off" className="form-control" />
					</div>
				</div>
			</form>
			<div className="button-align-setting">
					<button type="button" className="btn btn-danger button button-block" onClick={this.submit}>{this.state.data.content.submit}</button>
			</div>
		</div>
	</section>
	
    );
  }
}

export default ConfirmForgotPassword;

