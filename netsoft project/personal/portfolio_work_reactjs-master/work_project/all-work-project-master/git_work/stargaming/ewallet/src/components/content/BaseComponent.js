import React, { Component } from 'react';
import { Modal, Form } from 'antd';
import store from '../../store';

const FormItem = Form.Item;

export const CollectionCreateForm = Form.create()(
		(props) => {
			const { visible, onCancel, onCreate, content, btnLabel } = props;

		    return (
		    		<Modal
		    			visible={visible}
		      
		    			okText={btnLabel}
		    			onCancel={onCancel}
		    			onOk={onCreate}
		    		>
		    			<Form layout="vertical">
		    				<FormItem label={content}>
		    				</FormItem>
		    			</Form>
		    		</Modal>
		    );
		}
);

export default class BaseComponent extends Component{
	
	constructor(props) {
		super(props);
		this.state = {
			visible: false,
			successVisible : false,
		}
		this.handleInputChange = this.handleInputChange.bind(this);
        
	}
	
	 handleInputChange(event) {
		 const target = event.target;
		 const value = target.type === 'checkbox' ? target.checked : target.value;
		 const name = target.name;
			
		 this.setState({
			 [name]: value
		 });
	       
	}
	 
	componentWillMount = function() {
		store.addChangeListener(this._onChange);
	}
		  
	componentWillUnmount = function() {
		store.removeChangeListener(this._onChange);
	}

	_onChange = () => {
		this.setState({ data: store.getContent() });
	}
		  
	showModal = () => {
		this.setState({ visible: true });
	}
	
	handleCancel = () => {
		this.setState({ visible: false });
	}
	
	
	
}