import React, { Component } from 'react';
import BaseComponent from "./BaseComponent";

import {Link} from 'react-router-dom';
import { Avatar } from 'antd';
import { Rate } from 'antd';
import { Pagination } from 'antd';
import { Select } from 'antd';
import store from '../../store';

const Option = Select.Option;

class LoanActivity extends BaseComponent {
	 state = {
     value: 1,
  }
  onChange = (e) => {
    console.log('radio checked', e.target.value);
    this.setState({
      value: e.target.value,
    });
  }
	constructor(props) {
		super(props);
		this.state = { 
			props: props,
			data: store.getContent()
		};
	}
	
 
 render() {
    return (
	
<article id="tab">

    <header className="header-same">
        <Link to="Home">
        <div className="back-btn">
            <i className="glyphicon glyphicon-menu-left"></i>
        </div>
        </Link>
        <div className="title-same">
            {this.state.data.content.loanactivity}
        </div>
    </header>

    <div className="tab-style">
        <input id="tab1" type="radio" name="tabs" checked/>
        <label for="tab1">{this.state.data.content.all}</label>

        <input id="tab2" type="radio" name="tabs" />
        <label for="tab2">{this.state.data.content.lend}</label>

        <input id="tab3" type="radio" name="tabs" />
        <label for="tab3">{this.state.data.content.return}</label>

        <input id="tab4" type="radio" name="tabs" />
        <label for="tab4">{this.state.data.content.borrow}</label>

        <section id="content1">
            <article id="tab-style" className="tab-panel">
                <div className="table_style">
 
                    <div className="table_uploadco4">
                        <div className="table">

                            <div className="table-cell table-cellstyle table_cellwidth">{this.state.data.content.lend}</div>

                            <div className="table-cell ">
                                <span>28 Sept 2017</span>
                            </div>
                            <div className="table-cell">
                                <span>Lend To Rola</span>
                            </div>
                            <div className="table-cell">
                                <span>RM 20.00</span>
                            </div>

                            <div className="table-cell table-cellstyle table_cellwidth">{this.state.data.content.return}</div>

                            <div className="table-cell">
                                <span>25 Sept 2017</span>
                            </div>
                            <div className="table-cell">
                                <span>Return From Lily</span>
                            </div>
                            <div className="table-cell">
                                <span>RM 10.00</span>
                            </div>

                            <div className="table-cell table-cellstyle table_cellwidth">{this.state.data.content.borrow}</div>

                            <div className="table-cell">
                                <span>23 Sept 2017</span>
                            </div>
                            <div className="table-cell">
                                <span>Borrow From Ali</span>
                            </div>
                            <div className="table-cell">
                                <span>RM 50.00</span>
                            </div>

                        </div>
                    </div>

                </div>
            </article>
        </section>

        <section id="content2">
            <aside id="LEND" className="tab-panel">
                <div className="table_style">

                    <div className="table_uploadco4">
                        <div className="table">

                            <div className="table-cell">
                                <span>28 Sept 2017</span>
                            </div>
                            <div className="table-cell">
                                <span>Lend To Rola</span>
                            </div>
                            <div className="table-cell">
                                <span>RM 20.00</span>
                            </div>

                        </div>
                    </div>

                </div>
            </aside>
        </section>

        <section id="content3">
            <aside id="RETURN" className="tab-panel">
                <div className="table_style">

                    <div className="table_uploadco4">
                        <div className="table">

                            <div className="table-cell">
                                <span>25 Sept 2017</span>
                            </div>
                            <div className="table-cell">
                                <span>Return From Lily</span>
                            </div>
                            <div className="table-cell">
                                <span>RM 10.00</span>
                            </div>

                        </div>
                    </div>

                </div>
            </aside>
        </section>

        <section id="content4">
            <aside id="BORROW" className="tab-panel">
                <div className="table_style">

                    <div className="table_uploadco4">
                        <div className="table">

                            <div className="table-cell">
                                <span>23 Sept 2017</span>
                            </div>
                            <div className="table-cell">
                                <span>Borrow From Ali</span>
                            </div>
                            <div className="table-cell">
                                <span>RM 50.00</span>
                            </div>

                        </div>
                    </div>

                </div>
            </aside>
        </section>

        <div className="table-cell pagin-style">
            <Pagination defaultCurrent={1} total={30} className="pagination_style" />
        </div>

    </div>
</article>
    );
  }
}

export default LoanActivity;

