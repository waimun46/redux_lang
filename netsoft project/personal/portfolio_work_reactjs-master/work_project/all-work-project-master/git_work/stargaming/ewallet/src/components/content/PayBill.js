import React, { Component } from 'react';
import BaseComponent from "./BaseComponent";

import ReactDOM  from 'react-dom';
import {Link} from 'react-router-dom';
import store from '../../store';

import { Select } from 'antd';
const Option = Select.Option;

class PayBill extends BaseComponent {
	
	constructor(props) {
		super(props);
		this.state = { 
			props: props,
			data: store.getContent()
		};
	}
	
  render() {
    return (
	
	<section id="PayBill">
	
		<header className="header-same">
			<Link to="Home">
				<div className="back-btn">
					<i className="glyphicon glyphicon-menu-left"></i>
				</div>
			</Link>
			<div className="title-same">{this.state.data.content.paybill}</div>						
		</header>
		
		<div className="form">
			<div className="select_margin">
				<Select className="select_style" placeholder={this.state.data.content.type}>
					<Option value={this.state.data.content.all}>{this.state.data.content.all}</Option>
					<Option value="Utility">Utility</Option>
					<Option value="Government">Government</Option>
				</Select>

			</div>

			<div className="select_margin">
				<Select className="select_style" placeholder={this.state.data.content.selectcompany}>
					<Option value="Indah Water">Indah Water</Option>
					<Option value="Tenaga Nasional">Tenaga Nasional</Option>
					<Option value="Syabas">Syabas</Option>
				</Select>

			</div>

			<div className="button-align-setting">
				<Link to="BillPayment">
				<button type="button" className="btn btn-danger button button-block button_margintop">{this.state.data.content.select}</button>
				</Link>
			</div>
		</div>
	</section>
    );
  }
}

export default PayBill;

