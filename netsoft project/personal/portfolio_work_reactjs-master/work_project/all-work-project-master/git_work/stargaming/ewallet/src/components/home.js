import React, { Component } from 'react';
import BaseComponent from "./content/BaseComponent";
import react from 'react-dom';
import Login from './login';

import {Link} from 'react-router-dom';
import {PostData} from './services/PostData';
import { Row, Col } from 'antd';
import Homecontent from './home-content';
import Menu from './menu';
import store from '../store';
import actions from '../actions';
import mywallet from '../include/images/wallet.png';
import balance from '../include/images/balance.png';
import makepayment from '../include/images/makepayment.png';
import $ from "jquery";
import sha1 from "sha1";



class Home extends BaseComponent {

    constructor(props) {
        super(props);
        this.state = {
            props: props,
            data: store.getContent(),
        };
    }

    componentDidMount () {

        console.log(process._root);

        let mysession = document.cookie.replace(/(?:(?:^|.*;\s*)session\s*\=\s*([^;]*).*$)|^.*$/, "$1");
        //console.log(mysession);
        let params = {
            'action' : 'getBalance',
            'session': mysession
        };

        PostData('Wallet', params, this.props).then((result) => {
            console.log(result);
            if(result.status == 'ok' && result.data.session != '') {
                actions.switchLanguage('ch');
                this.setState({
                    balance: Number(result.data['balance']).toFixed(2),

                });
            }else{
                this.setState({
                    //validationerror: result.error
                    validationerror: this.state.data.content[result.error]
                });
                console.log(result);
            }
        });

    }

    render() {
        return (
            <section id="Home" >
                <Menu/>
                <main className="home-body">
                    <div className="gutter-example hometop-btn">
                        <Row>

                            <Col className="mywallet" span={8}>
                                <Link to='MyWallet'>
                                    <div className="mywallet-icon">
                                        <img src={mywallet}/>
                                        <li className="li-padding">{this.state.data.content.mywallet}</li>
                                    </div>
                                </Link>
                            </Col>
                            <Col className="balance" span={8}>
                                <Link to='Balance'>
                                    <div className="balance-icon">
                                        <img src={balance}/>
                                        <li>{this.state.data.content.balance}</li>
                                        <h6>{ this.state.balance }</h6>
                                    </div>
                                </Link>
                            </Col>
                            <Col className="makepayment" span={8}>

                                <div className="makepayment-icon">
                                    <img src={makepayment}/>
                                    <li className="li-padding">{this.state.data.content.makepayment}</li>
                                </div>

                            </Col>
                        </Row>
                    </div>

                    <Homecontent/>

                </main>



            </section>

        );
    }
}


export default Home;