import React, { Component } from 'react';
import BaseComponent from "./BaseComponent";
import {Link} from 'react-router-dom';

class ReceiveMoney extends BaseComponent {
  render() {
    return (
	
<section id="ConfirmSendMoney">
	<div className="form">
	<h1>Receive Money</h1>
 
 <form role="form" action="/login" method="POST" name="login" accept-charset="UTF-8" enctype="application/x-www-form-urlencoded" autocomplete="off">
    <div className="field-wrap security_style2">
     <input  type="text" name="text" placeholder="Amount: " aria-describedby="basic-addon2" autocomplete="off" className="form-control" />
    </div>
	
	<div className="field-wrap ">
	<textarea style={{ height: '300px' ,fontSize:"18px" ,marginTop:"-25px"}} rows="5" cols="10" placeholder="Remark :"  aria-describedby="basic-addon2" autocomplete="off" className="form-control"></textarea>
    </div>
	
	<Link to="./Home">	 
	<button type="button" className="btn btn-danger button button-block" onclick="logIn()">GENERATE</button>
 </Link>
 </form>
	
	</div>
 </section>    
    );
  }
}

export default ReceiveMoney;

