import React, { Component } from 'react';
import BaseComponent from "./BaseComponent";

import {Link} from 'react-router-dom';


class ConfirmScanQRCode extends BaseComponent {
	
	
  render() {
    return (
	
	<section id="ConfirmSendMoney">
	<div className="form">
	<h1>Confirm Scan QR Code</h1>
   
		<div>
			 <ul>
			 <li className="lineHeight_style">From :</li>
			 <li className="lineHeight_style2">John</li>
			 <li className="lineHeight_style">Amount :</li>
			 <li className="lineHeight_style2">20.00</li>
			 <li className="lineHeight_style">Remark :</li>
			 <li className="lineHeight_style3">Money Transfer</li>
			</ul>
		</div>


		 <Link to="./Home">	 
				<button type="button" className="btn btn-danger button button-block button_margintop" onclick="logIn()">CONFIRM</button>
			 </Link>
	
		
		</div>
	   </section>  
    );
  }
}

export default ConfirmScanQRCode;

