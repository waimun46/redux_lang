import React, { Component } from 'react';
import BaseComponent, {CollectionCreateForm} from "./BaseComponent";

import {PostData} from '../services/PostData';
import {Link} from 'react-router-dom';
import store from '../../store';
import $ from "jquery";

class ChangePassword extends BaseComponent {
	
	constructor(props) {
		super(props);
		this.state = { 
			props: props,
			data: store.getContent()
		};
	}
	
	changePassword = () => {

		 let session = document.cookie.replace(/(?:(?:^|.*;\s*)session\s*\=\s*([^;]*).*$)|^.*$/, "$1");
		 
		 let params = {
		   'action' : 'updatePassword',
		   'oldpassword': $('input[name="currentpassword"]').val(),
		   'password': $('input[name="newpassword"]').val(),
		   'confpass': $('input[name="confirmpassword"]').val(),
		   'session': session,
		 };
		  
		  PostData('Profile', params, this.props).then((result) => {
			  if(result.status == 'ok') {
			      this.setState({
				      successVisible: true,
	              })
			  }else{
				  this.setState({
					  //validationerror: result.error
					  validationerror: this.state.data.content[result.error]
				  });
			  }
		  });
		  
	 }
	
	 successChangePassword = () => {
		 // Cleasr Cookie
		 document.cookie = "session=" + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
		 this.setState({
				validationerror : '',
				successVisible : false,
        })
        
        this.props.history.push('/');
	}
	
  render() {
    return (
	<section id="ChangePassword">
	
	    <CollectionCreateForm
            visible={this.state.successVisible}
            onCancel={this.successChangePassword}
            onCreate={this.successChangePassword}
            btnLabel="OK"
            content="Password had Changed Successfully."
        />
		
		<header className="header-same">
			<Link to="Home">
				<div className="back-btn">
					<i className="glyphicon glyphicon-menu-left"></i>
				</div>
			</Link>
			<div className="title-same">{this.state.data.content.changepassword}</div>
						
		</header>
	
	
		<div className="form">
			<form role="form" accept-charset="UTF-8" enctype="application/x-www-form-urlencoded" autocomplete="off">
				
				<div className="field-wrap">
					<div className="form-group input-group">
	            		<laber className="text-danger" >{this.state.validationerror}</laber>
	            	</div>
	            </div>
	            
        		<div className="field-wrap">
					<div className="form-group input-group">
						<span id="basic-addon2" className="input-group-addon">
						<span className="glyphicon glyphicon-lock"></span></span>
						<input type="password" name="currentpassword" placeholder={this.state.data.content.currentpassword} aria-describedby="basic-addon2" autocomplete="off" className="form-control" />
					</div>
				</div>
				
				<div className="field-wrap">
					<div className="form-group input-group">
						<span id="basic-addon2" className="input-group-addon">
						<span className="glyphicon glyphicon-lock"></span></span>
						<input type="password" name="newpassword" placeholder={this.state.data.content.newpassword} aria-describedby="basic-addon2" autocomplete="off" className="form-control" />
					</div>
				</div>
				
				
				<div className="field-wrap">
					<div className="form-group input-group">
						<span id="basic-addon2" className="input-group-addon">
						<span className="glyphicon glyphicon-lock"></span></span>
						<input type="password" name="confirmpassword" placeholder={this.state.data.content.confirmpassword} aria-describedby="basic-addon2" autocomplete="off" className="form-control" />
					</div>
				</div>
				
					<div className="button-align-setting">
						<button type="button" className="btn btn-danger button button-block" onClick={this.changePassword}>{this.state.data.content.submit}</button>
					</div>
				
			</form>		
		</div>
    </section>
    );
  }
}

export default ChangePassword;

