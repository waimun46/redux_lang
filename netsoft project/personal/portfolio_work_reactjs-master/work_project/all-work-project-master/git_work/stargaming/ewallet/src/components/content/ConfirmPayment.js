import React, { Component } from 'react';
import BaseComponent from "./BaseComponent";

import {Link} from 'react-router-dom';
import store from '../../store';

class ConfirmPayment extends BaseComponent {
	
	constructor(props) {
		super(props);
		this.state = { 
			props: props,
			data: store.getContent()
		};
	}
	
  render() {
    return (
	<section id="ConfirmPayment" className="same-form-style">
		<header className="header-same">
			<Link to="Lend">
				<div className="back-btn">
					<i className="glyphicon glyphicon-menu-left"></i>
				</div>
			</Link>
			<div className="title-same">
				{this.state.data.content.confirmpayment}
			</div>
		</header>

		
		<div className="chart">
			<h3>{this.state.data.content.lendamount}</h3>
			<div className="diagram">
				<h4>50.00</h4>
			</div>

			<div className="chart_list">
				<ul>
					<li className="red  chart-p"> 
						{this.state.data.content.borrowerusername} :
						<p className="percentage">May123</p>
					</li>
					<li className="purple">
						{this.state.data.content.borrowername} :
						<p className="percentage">May Lim</p>
					</li>
					
					<div className="clear"></div>
				</ul>
			</div>
			<div className="chart_list">
				<ul>
					<li className="yellow  chart-p">
						{this.state.data.content.borrowerphone} :
						<p className="percentage">0171234567</p>
					</li>
					<li className="blue">
						{this.state.data.content.requestamount}:
						<p className="percentage">20000.00</p>
					</li>
					<div className="clear"></div>
				</ul>
			</div>
			
		</div>
		
		<div className="button-align-setting">
			<Link to="Security">
				<button type="button" className="btn btn-danger button button-block">
					{this.state.data.content.lend}
				</button>
			</Link>
		</div>
		
		
	</section>
    );
  }
}

export default ConfirmPayment;

