import React, { Component } from 'react';
import BaseComponent, {CollectionCreateForm} from "./BaseComponent";
import {Link} from 'react-router-dom';
import store from '../../store';
import {PostData} from "../services/PostData";
import $ from "jquery";

class ConfirmSecurityCode extends BaseComponent {
		
	constructor(props) {
		super(props);
		this.state = { 
			props: props,
            cookit: document.cookie,
			data: store.getContent()
		};
	}

    submit = () => {
        let session = this.state.cookit.replace(/(?:(?:^|.*;\s*)session\s*\=\s*([^;]*).*$)|^.*$/, "$1");

        let transpass 	  = $('input[name="transpass"]').val();
        let conftranspass = $('input[name="conftranspass"]').val();
        let params = {
            'action': 'addTransPass',
			'transpass' : transpass,
			'conftranspass' : conftranspass,
            'session': session
        };
        console.log(params);
        PostData('Profile',params ,this.props).then((result) => {
            if(result.status == 'ok' && result.data != ''){
            	this.setState({
 					successVisible: true,
                })
            }else{
                this.setState({
                    validationerror: this.state.data.content[result.error]
                });
            }
        });
    }
    
    successChangeTransPass = () => {
		this.setState({
				validationerror : '',
				successVisible : false,
        })
        
        this.props.history.push('/Home');
	}

  	render() {
		return (
			<section id="ConfirmSendMoney">
			    <CollectionCreateForm
	                visible={this.state.successVisible}
	                onCancel={this.successChangeTransPass}
	                onCreate={this.successChangeTransPass}
	                btnLabel="OK"
	                content="Security Code had Changed Successfully."
	            />
			
			
				<header className="header-same">
					<Link to="Home">
						<div className="back-btn">
							<i className="glyphicon glyphicon-menu-left"></i>
						</div>
					</Link>
					<div className="title-same">{this.state.data.content.confirmsecuritycode}</div>

				</header>


				<div className="form">
                    <div className="">
                        <laber className="text-danger">{this.state.validationerror}</laber>
                    </div>
					<form>
						<div className="field-wrap">
						  <div className="form-group input-group">
						  <span id="basic-addon2" className="input-group-addon">
						  <span className="glyphicon glyphicon-lock"></span></span>
							<input type="password" name="transpass" placeholder={this.state.data.content.newsecuritycode} aria-describedby="basic-addon2" autocomplete="off" className="form-control" />
						  </div>
						</div>

						 <div className="field-wrap">
						  <div className="form-group input-group">
						  <span id="basic-addon2" className="input-group-addon">
						  <span className="glyphicon glyphicon-lock"></span></span>
							<input type="password" name="conftranspass" placeholder={this.state.data.content.confirmsecuritycode} aria-describedby="basic-addon2" autocomplete="off" className="form-control" />
						  </div>
						</div>

						<div className="button-align-setting">
							<button type="button" className="btn btn-danger button button-block" onClick={this.submit}>{this.state.data.content.submit}</button>
						</div>

					</form>
				</div>
			</section>
    	);
  	}
}

export default ConfirmSecurityCode;

