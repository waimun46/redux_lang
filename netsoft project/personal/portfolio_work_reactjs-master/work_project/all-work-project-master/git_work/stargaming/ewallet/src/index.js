import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router } from 'react-router-dom';
import './index.css';
import App from './App';

import actions from './actions';

window.Router = Router

actions.initApp();
ReactDOM.render(<Router><App /></Router>, document.getElementById('root'));

