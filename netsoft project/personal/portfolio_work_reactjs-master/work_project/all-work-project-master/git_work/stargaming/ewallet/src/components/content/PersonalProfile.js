import React, { Component } from 'react';
import raect from 'react-dom'
import BaseComponent, {CollectionCreateForm} from "./BaseComponent";

import {Link} from 'react-router-dom';
import { Avatar, Row, Col, Input } from 'antd';
import store from '../../store';
import $ from 'jquery';
import Verify from './Verify';
import {PostData} from "../services/PostData";

class PersonalProfile extends BaseComponent {
	
	constructor(props) {
		super(props);
		this.state = { 
			props: props,
			data: store.getContent(),
            prefix : '',
			name : '',
			email : '',
			cookit: document.cookie,
			
		};
		 
	}

	componentDidMount = () => {
		
		let session = this.state.cookit.replace(/(?:(?:^|.*;\s*)session\s*\=\s*([^;]*).*$)|^.*$/, "$1");
		let params = {
			'action' : 'profile',
			'session' : session
		};

        PostData('Profile',params ,this.props).then((result) => {
        	console.log(result);
			if(result.status == 'ok' && result.data != ''){
			 	this.setState({
					 name: result.data.name,
					 email:result.data.email,
					 phone:result.data.phone,
					 username: result.data.username,

				 })
			} else{
				this.setState({ 
					validationerror: this.state.data.content[result.error]
				});
			}

		});
	}

	verify = () => {
        let session = this.state.cookit.replace(/(?:(?:^|.*;\s*)session\s*\=\s*([^;]*).*$)|^.*$/, "$1");
        let password = $('input[name="password"]').val();
        let prefix = this.state.prefix;
        let params = {
            'action' : 'updateProfileSubmit',
            'email': this.state.email,
            'name': this.state.name,
            'otp': prefix + '-' + password,
            'session' : session
        }
        console.log(params);
        PostData('Profile',params).then((result) => {
            console.log(result);
            if(result.status == 'ok' && result.data != ''){
                this.setState({
					successVisible: true,
                })
                
            } else {
                this.setState({
                    validationerror: this.state.data.content[result.error]
                });
            }
        });
	}

	updateProfile = () => {

		let session = this.state.cookit.replace(/(?:(?:^|.*;\s*)session\s*\=\s*([^;]*).*$)|^.*$/, "$1");
		let params = {
			'action' : 'updateProfile',
            'email': $('input[name="email"]').val(),
            'name': $('input[name="name"]').val(),
			'session' : session
		}
        console.log(params);
        PostData('Profile',params).then((result) => {
            console.log(result);
			if(result.status == 'ok' && result.data != ''){
				this.setState({
					 name: result.data.name,
					 email:result.data.email,
                     prefix : result.data.prefix,
				 })
			} else {
				this.setState({
					validationerror: this.state.data.content[result.error]
				});
			}
		});
	}
	
	successVerify = () => {
		 this.setState({
				prefix: '',
				validationerror : '',
				successVisible : false,
				visible : false,
         })
         
         this.props.history.push('/PersonalProfile');
	}
	

    resendverifycode = () => {
        let session = document.cookie.replace(/(?:(?:^|.*;\s*)session\s*\=\s*([^;]*).*$)|^.*$/, "$1");
        let params = {
            'action': 'resendVerify',
            'verifyAction': 'updateProfile',
            'session': session
        };
        console.log(params);
        PostData('Profile', params, this.props).then((result) => {
            console.log(result);
            if(result.status == 'ok') {
                this.setState({
                    prefix : result.data.prefix,
                });
                this.showModal();
            }else{
                this.setState({
                    //validationerror: result.error
                    validationerror: this.state.data.content[result.error]
                });
            }
        });

    }


  	render() {
		return (
			<ChangeForm chg={this}/>
		)
	}
}

function ChangeForm(props)
{
	const otp = props.chg.state.prefix;
    if(otp)
        return (
        	<div>
			    <Verify 
			        validationerror={props.chg.state.validationerror}  
			        prefix={props.chg.state.prefix} 
			        onClick={props.chg.verify} 
			        onReSendVerify={props.chg.resendverifycode}
			        visible={props.chg.state.visible}
                    handleCancel={props.chg.handleCancel}
			    />
			
			    <CollectionCreateForm
			        visible={props.chg.state.successVisible}
			        onCancel={props.chg.successVerify}
			        onCreate={props.chg.successVerify}
			        btnLabel="OK"
			        content="Info had Changed Successfully."
			    />
			</div>
		);
	else
		return(
            <section id="PersonalProfile">
                <article className="profile-w3layouts">
                    <header className="header-same">
                        <Link to="Home">
                            <div className="back-btn">
                                <i className="glyphicon glyphicon-menu-left"></i>
                            </div>
                        </Link>
                        <div className="title-same">{props.chg.state.data.content.personalprofile}</div>
                    </header>

                    <div className="profile-top_personal">
                        <div className=" profile-top-right_personal">
                            <div className="profile-logo">
                                <Avatar  size="large" icon="user" />
                                <li>{props.chg.state.username}</li>
                            </div>
                        </div>
                    </div>
                    
                    <div className="login-error field-wrap">
                    	<label className="text-danger" >{props.chg.state.validationerror}</label>
                    </div>

					<div className="username-style">

						<Row className="profile-from">
							<Col span={12}>
								<span className="username-style-color">{props.chg.state.data.content.username} :</span>
							</Col>
							<Col span={12}>
								<li>{props.chg.state.username}</li>
							</Col>
						</Row>

						<Row className="profile-from">
							<Col span={12}>
								<span className="username-style-color">{props.chg.state.data.content.name} :</span>
							</Col>

							<Col span={12}>
								<Input name="name" placeholder="name" onChange={props.chg.handleInputChange} value={props.chg.state.name} placeholder={props.chg.state.name} / >
							</Col>
						</Row>

						<Row className="profile-from">
							<Col span={12}>
								<span className="username-style-color">{props.chg.state.data.content.email} :</span>
							</Col>
							<Col span={12}>
								<Input name="email" placeholder="email" onChange={props.chg.handleInputChange} value={props.chg.state.email} placeholder={props.chg.state.email}/>
							</Col>
						</Row>

						<div className="btn-same-style button-align-setting">
							<button type="button" className="btn btn-danger button button-block" onClick = {props.chg.updateProfile}>{props.chg.state.data.content.update}</button>
						</div>
					</div>
                </article>
            </section>
		);
}

export default PersonalProfile;
