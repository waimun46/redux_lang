import React, { Component } from 'react';
import BaseComponent from "./content/BaseComponent";
import ReactDOM from 'react-dom';

import logo from '../include/images/logo-01.png';


class Login2 extends BaseComponent {
  render() {
    return (
        <div className="login-form">
			<div className="top-login">
				<span><img src={logo}/></span>
			</div>
			<div className="login-top">
			<form>
				<div className="login-ic">
					<i ></i>
				<input type="text" placeholder="Username"/>
					<div className="clear"> </div>
				</div>
				<div className="login-ic">
					<i className="icon"></i>
					<input type="password" placeholder="Password" />
					<div className="clear"> </div>
				</div>

				<div className="log-bwn">
					<input type="submit"  value="Login" />
				</div>
			</form>
			</div>
            <div className="regis_style">
            <a className="Forgetpassword">Forget password?</a>
            <a className="sign-up">Sign Up? Click Here</a>


            </div>
        </div>

    );
  }
}

export default Login2;
