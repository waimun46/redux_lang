import React, { Component } from 'react';
import BaseComponent from "./BaseComponent";
import {PostData} from '../services/PostData';
import { Link } from 'react-router-dom';
import store from '../../store';
import $ from "jquery";
import Verify from './Verify';

class ForgetPassword extends BaseComponent {

	constructor(props) {
		super(props);
		this.state = {
			props: props,
			data: store.getContent(),
            prefix : '',
            username : ''
		};
	}

	submit = () => {

        let username = $('input[name="username"]').val();
		let params = {
			'action': 'forgotPassword',
			'username': username
		};

		console.log(params);
        PostData('ForgotPass', params, this.props).then((result) => {
            if(result.status == 'ok' && result.data.prefix != '') {
                this.setState({ username : result.data.username});
                this.setState({ prefix : result.data.prefix});
            }else{
                this.setState({
                    //validationerror: result.error
                    validationerror: this.state.data.content[result.error]
                });
            }
        });

	}

    verifysend = () => {
        let prefix = this.state.prefix;
        let username = this.state.username;
        let password = $('input[name="password"]').val();

        let params = {
            'action': 'forgotPasswordVerify',
            'otp': prefix + '-' + password,
            'username': username,

        };
        console.log(params);
        PostData('ForgotPass', params, this.props).then((result) => {
            if(result.status == 'ok') {
                document.cookie = "username="+username;
                this.props.history.push('/ConfirmForgotPassword');
            }else{
                this.setState({
                    //validationerror: result.error
                    validationerror: this.state.data.content[result.error]
                });
            }
        });

    }

    resendverifycode = () => {
        let session = document.cookie.replace(/(?:(?:^|.*;\s*)session\s*\=\s*([^;]*).*$)|^.*$/, "$1");
        let username = this.state.username;
        let params = {
            'action': 'resendVerify',
			'username': username,
            'verifyAction': 'forgotPasswordVerify',
            'session': session
        };
        console.log(params);
        PostData('ForgotPass', params, this.props).then((result) => {
            console.log(result);
            if(result.status == 'ok') {
                this.setState({
                    prefix : result.data.prefix,
                });
                this.showModal();
            }else{
                this.setState({
                    //validationerror: result.error
                    validationerror: this.state.data.content[result.error]
                });
            }
        });

    }

	render() {
		return (
			this.state.prefix ? 
				
				<Verify 
				    prefix={this.state.prefix}
					username={this.state.username}
					onClick={this.verifysend}
                    onReSendVerify={this.resendverifycode}
			        visible={this.state.visible}
			        handleCancel={this.handleCancel}
			        validationerror={this.state.validationerror}
			    />
             
			:
				
			<section id="ForgetPassword">
				<header className="header-same">
					<Link to="./">
						<div className="back-btn">
							<i className="glyphicon glyphicon-menu-left"></i>
						</div>
					</Link>
					<div className="title-same">{this.state.data.content.forgetpassword}</div>

				</header>
				<div className="form">
					<div className="">
						<label className="text-danger">{this.state.validationerror}</label>
					</div>
					<form role="form" action="/login" method="POST" name="login">
						<div className="field-wrap">
							<div className="form-group input-group">
							<span id="basic-addon1" className="input-group-addon">
								<span className="glyphicon glyphicon-user"></span>
							</span>
								<input type="text" name="username" placeholder={this.state.data.content.username}
									   aria-describedby="basic-addon1" className="form-control"/>
							</div>
						</div>
						<div className="button-align-setting">

							<button type="button" className="btn btn-danger button button-block"
									onClick={this.submit}>{this.state.data.content.submit}</button>

						</div>
					</form>
				</div>
			</section>
		);
	}

}

export default ForgetPassword;
