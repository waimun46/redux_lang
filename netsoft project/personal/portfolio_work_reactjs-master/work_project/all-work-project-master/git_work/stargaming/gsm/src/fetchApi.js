import fetchJsonp from 'fetch-jsonp';

export default (obj)=>{
    // let url = obj.url || 'http://jp.stargt.com.my/scr4/'+obj.api;
    let url = obj.url ||process.apiUrl +obj.api;
    let headers = {
        'Content-Type':'application/json',
    };

    if(obj.jsonp){
        return  fetchJsonp(url).then(response=>response.json())

    }else{
        if (obj.headers === false) {
            return fetch(url,{
                method:obj.method || 'POST',
                mode:'cors',
                credentials: 'include',
                body:obj.body
            });
        } else {
            return fetch(url,{
                method:obj.method || 'POST',
                mode:'cors',
                credentials: 'include',
                headers:Object.assign({},headers,obj.headers||{}),
                body:JSON.stringify(obj.data)
            });
        }
    }
}



