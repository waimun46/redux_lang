import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import {Icon} from 'react-fa';
import FaAngleDown from 'react-icons/lib/fa/angle-down'


import Logo from '../../include/images/logo.png';
import {connect} from "react-redux";
import _fetch from "../../fetchApi";
import sessionExpired from "../../components/SessionExpired";

import NavLang from "../../components/NavLang/NavLang";

import './menu.css';


class Menu extends Component {

    state = {
        balance: 0
    }

    componentDidMount = () => {
        setInterval(() => {
            _fetch({
                api: 'Client/Home',
                data: {
                    action: 'get_credit'
                }
            }).then(res => res.json()).then((data) => {
                if (data.status.toLocaleLowerCase() === 'error') {
                    if (data.error.toLowerCase() === 'unknown_error') {

                    }
                    sessionExpired(data)
                } else {
                    this.setState({
                        balance: data.data.client
                    })
                }
            })
        }, 10000)
    }

    logout = () => {
        if (!this.props.isLogin) {
            return
        }

        _fetch({
            api: 'Client/Profile',
            data: {
                action: 'logout'
            }
        }).then(res => res.json()).then((data) => {
            if (data.status.toLocaleLowerCase() === 'error') {
                if (data.error.toLowerCase() === 'unknown_error') {

                }
                sessionExpired(data)
            } else {
                localStorage.setItem('isLogin', '');
                localStorage.setItem('client', '');
                localStorage.setItem('menu', '');
                window.location.href = '/';
            }
        })
    };

    home = () => {
        window.location.href = '/main';
    }

    render() {
        return (
            <div className="menu">
                <header>
                    <nav className="fh5co-nav">
                        <div className="top">
                            <div className="row width_style">
                                <div className="col-xs-6 text-left top_left_style">
                                    <p className="num pointer" onClick={this.home}><Icon name="home"/>SMS</p>
                                </div>
                                <div className="col-xs-6 text-right">
                                    <p className="num"><Icon name="user" />&nbsp;{this.props.client}</p>
                                    <p className="num">{window.intl.get('home.balance')}&nbsp;:&nbsp;{this.state.balance}</p>
                                    {
                                        /*
                                        <p className="num"><Icon name="cog" />  Setting</p>
                                        <p className="num"><Icon name="question-circle" />  Help</p>
                                        <p className="num"><Icon name="comments" />  FAQ</p>
                                         */
                                    }
                                    <div className="num"><NavLang/></div>
                                    <p className="num pointer" onClick={this.logout}><Icon name="home"/>{window.intl.get('action.logout')}</p>
                                </div>
                            </div>
                        </div>
                        <div className="top-menu">
                            <div className="row">
                                <div className="col-xs-2">
                                    <div id="fh5co-logo"><img src={Logo} alt=""/></div>


                                </div>
                                <div className="col-xs-10 text-right">
                                    <ul>

                                        <div className="dropdown_menu ">
                                            {
                                                this.props.menu.split && (this.props.menu.split(',').indexOf('SMS')!== -1) &&
                                                <div className="dropdown">
                                                    <button className="dropbtn">
                                                        <span className="con_1">{window.intl.get('sms.sms')}</span>
                                                        <span className="con_2"><FaAngleDown /></span>
                                                    </button>
                                                    <div className="dropdown-content">
                                                        <Link to ="Userpage">
                                                            <div className={this.props.pathname === '/Userpage' || this.props.pathname === '/Userprofile' || this.props.pathname === '/Createuser' ? 'active' : null}>
                                                                <span>{window.intl.get('user.user')}</span>
                                                            </div>
                                                        </Link>
                                                        <Link to ="Topuppage">
                                                            <div className={this.props.pathname === '/Topuppage' ? 'active' : null}>
                                                                <span>{window.intl.get('topup.title')}</span>
                                                            </div>
                                                        </Link>
                                                        <Link to ="Transferpage">
                                                            <div className={this.props.pathname === '/Transferpage' ? 'active' : null}>
                                                                <span>{window.intl.get('transfer.title')}</span>
                                                            </div>
                                                        </Link>
                                                        <Link to ="Groupreceiver">
                                                            <div className={this.props.pathname === '/Groupreceiver' || this.props.pathname === '/Addgroup' || this.props.pathname === '/Viewgroup' ? 'active' : null}>
                                                                <span>{window.intl.get('groupReceiver.title')}</span>
                                                            </div>
                                                        </Link>
                                                        <Link to ="Campaigns">
                                                            <div className={this.props.pathname === '/Campaigns' || this.props.pathname === '/Createcamp' || this.props.pathname === '/Campaigndetail' ? 'active' : null}>
                                                                <span>{window.intl.get('sms.campaign')}</span>
                                                            </div>
                                                        </Link>
                                                        <Link to ="Sendmsg">
                                                            <div className={this.props.pathname === '/Sendmsg' ? 'active' : null}>
                                                                <span>{window.intl.get('sms.singleSMS')}</span>
                                                            </div>
                                                        </Link>
                                                        <Link to="Apidoc">
                                                            <div className={this.props.pathname === '/Apidoc' ? 'active' : null}>
                                                                <span>{window.intl.get('apiDocument.title')}</span>
                                                            </div>
                                                        </Link>
                                                    </div>
                                                </div>
                                            }
                                            {
                                                this.props.menu.split && (this.props.menu.split(',').indexOf('Voucher')!== -1) &&
                                                <div className="dropdown">
                                                    <button className="dropbtn">
                                                        <span
                                                            className="con_1">{window.intl.get('voucher.voucher')}</span>
                                                        <span className="con_2"><FaAngleDown/></span>
                                                    </button>
                                                    <div className="dropdown-content">
                                                        <Link to="Voucherpackage">
                                                            <div
                                                                className={this.props.pathname === '/Voucherpackage' ? 'active' : null}>
                                                                <span>{window.intl.get('voucher.voucherPackage')}</span>
                                                            </div>
                                                        </Link>
                                                        <Link to="Voucherlist">
                                                            <div
                                                                className={this.props.pathname === '/Voucherlist' ? 'active' : null}>
                                                                <span>{window.intl.get('voucher.voucherList')}</span>
                                                            </div>
                                                        </Link>
                                                    </div>
                                                </div>
                                            }
                                        </div>

                                    </ul>
                                </div>
                            </div>
                        </div>
                    </nav>
                </header>
            </div>
        );
    }

}

function mapProps(state){
    return {
        client:state.client,
        isLogin:state.isLogin,
        menu: state.menu,
    }
}

export default connect(mapProps)(Menu)
