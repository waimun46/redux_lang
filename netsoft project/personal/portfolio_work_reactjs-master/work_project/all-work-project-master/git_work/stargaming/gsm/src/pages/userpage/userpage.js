import React , { Component } from 'react';
import { Modal } from 'antd';
import _fetch from '../../fetchApi';
import {connect} from 'react-redux';

import sessionExpired from '../../components/SessionExpired';
import Pagination from '../../components/Pagination/Pagination';
import '../../css/pages.css';

class Userpage extends  Component{

    state = {
        visible: false,
        deleteId: 0,
        action: '',
        errorMsg: '',

        content: [],
        totalRecord: 0,
        totalPage: 0,
        curPage: 1
    };

    componentDidMount = () => {
        this.getUserList()
    };

    getUserList = (page) => {
        _fetch({
            api: 'Client/User',
            data: {
                action: 'list',
                page: page || 1
            }
        }).then(res => res.json()).then((data) => {
            if (data.status.toLocaleLowerCase() === 'error') {
                if (data.error.toLowerCase() === 'unknown_error') {

                }

                sessionExpired(data)
            } else {

                this.setState({
                    content: data.data.reports,
                    totalRecord: data.data.total_record,
                    totalPage: data.data.total_page,
                    curPage: data.data.cur_page,
                    errorMsg: ''
                });
            }
        })
    };

    createUser = () => {
        this.props.history.push('/Createuser')
    };

    viewUser = (id) => {
        localStorage.setItem('user', id);
        this.props.history.push('/Userprofile')
    };

    actionConfirm = (id, action) => {
        this.setState({
            visible: true,
            deleteId: id,
            action: action,
            errorMsg: ''
        });
    };

    handleOk = () => {
        _fetch({
            api: 'Client/User',
            data: {
                action: this.state.action,
                user: this.state.deleteId
            }
        }).then(res => res.json()).then((data) => {
            if (data.status.toLocaleLowerCase() === 'error') {
                if (data.error.toLowerCase() === 'unknown_error') {

                }
                if (data.msg) {
                    this.setState({
                        errorMsg: data.msg[this.props.local],
                    });
                }
            } else {
                this.getUserList()
                this.setState({
                    visible: false,
                    errorMsg: ''
                });
            }
        })
    };

    handleCancel = () => {
        this.setState({
            visible: false,
            deleteId: 0,
            errorMsg: ''
        });
    };

    render(){
        return (
            <article className="main_content">
                <header>
                    <h2>{window.intl.get('user.user')}</h2>
                </header>
                <section className="page_content">

                    <div id="userpage">
                        <div className="title-btn">
                            <span className="createuser_btn createBtn" onClick={this.createUser}>{window.intl.get('user.create')}</span>
                        </div>

                        {/*content over here*/}
                        <div id="table_body">
                            <div className='totalRecord'>
                                <label>{window.intl.get('pagination.totalRecord')} : </label>
                                <span>{this.state.totalRecord}</span>
                                &nbsp;&nbsp;&nbsp;
                                <label>{window.intl.get('pagination.curPage')} : </label>
                                <span>{this.state.curPage}</span>
                            </div>
                            <table className="report table_style">
                                <thead>
                                <tr>
                                    <th>{window.intl.get('user.user')}</th>
                                    <th>{window.intl.get('credit.credit')}</th>
                                    <th>{window.intl.get('other.status')}</th>
                                    <th>{window.intl.get('action.title')}</th>
                                </tr>
                                </thead>

                                <tbody>
                                {
                                    this.state.content && this.state.content[this.props.local] &&
                                    this.state.content[this.props.local].map((item, key) => {
                                        return (
                                            <tr key={key}>
                                                <td>{item.name}</td>
                                                <td>{item.credit}</td>
                                                <td><span style={ item.status === 1 ? {color:'#228b22'} : {color:'#ff0000'}}>{item.status_display}</span></td>
                                                <td>
                                                    <span className="view_button" onClick={() => this.viewUser(item.id)}>{window.intl.get('action.view')}</span>
                                                    {
                                                        item.status === 1 ?
                                                            <span className="del_button" onClick={() => this.actionConfirm(item.id, 'inactive')}>{window.intl.get('status.inactive')}</span> :
                                                            <span className="activate_button" onClick={() => this.actionConfirm(item.id, 'active')}>{window.intl.get('status.active')}</span>
                                                    }
                                                </td>
                                            </tr>
                                        )
                                    })
                                }
                                </tbody>
                            </table>
                        </div>
                        <Pagination totalRecord={this.state.totalRecord} totalPage={this.state.totalPage} curPage={this.state.curPage} switchPage={this.getUserList}/>

                        <Modal className="userpage"
                               visible={this.state.visible}
                               onCancel={this.handleCancel}
                        >
                            <div className='pop-msg'>
                                <h1 className="title_color">{this.state.action === 'active' ? window.intl.get('status.active') : window.intl.get('status.inactive')}</h1>
                                <span className="errorMsg">{this.state.errorMsg}</span>
                                <p className="content_text">{this.state.action === 'active' ? window.intl.get('user.activeMsg') : window.intl.get('user.inactiveMsg')}?</p>

                                <button className="yes_button" onClick={this.handleOk}>{window.intl.get('button.yes')}</button>
                                <button className="no_button" onClick={this.handleCancel}>{window.intl.get('button.no')}</button>
                            </div>
                        </Modal>
                    </div>
                    {/*content over here*/}
                </section>
            </article>
        )
    }
}

function mapProps(state){
    return {
        menu: state.menu,
        local:state.local
    }
}

export default connect(mapProps)(Userpage)