import React, {Component} from 'react';
import Highcharts from 'highcharts';
import {
    HighchartsChart, Chart, withHighcharts, XAxis, YAxis, Title, Tooltip, Legend, LineSeries
} from 'react-jsx-highcharts';

const chart = {
    height: 400
    //height: (9 / 6 * 100) + '%'
}


class MyChart extends Component {

    state = {
        row: [],
        totalSent: [],
        totalDelivered: [],
        totalPending: [],
        totalFailed: []
    }

    componentDidMount = () => {
        this.getStatistics(this.props.statistics)
    }

    componentWillReceiveProps = (nextprops) => {
        if (this.props.statistics !== nextprops.statistics) {
            this.getStatistics(nextprops.statistics)
        }
    };

    getStatistics = (statistics) => {
        let row = []
        let totalSent = []
        let totalDelivered = []
        let totalPending = []
        let totalFailed = []

        statistics.forEach((item, index)=>{
            row.push(item.date)

            totalSent.push(item.detail.totalsent)
            totalDelivered.push(item.detail.totalsuccess)
            totalPending.push(item.detail.totalpending)
            totalFailed.push(item.detail.totalfailed)
        })

        this.setState({
            row: row,
            totalSent: totalSent,
            totalDelivered: totalDelivered,
            totalPending: totalPending,
            totalFailed: totalFailed
        });
    }

    render() {
        return (

            <HighchartsChart className="chart_style" week={this.state.row} chart={chart}>
                <Chart/>
                <Title>{window.intl.get('statistics.title')}</Title>
                <Legend/>
                <Tooltip valueSuffix=" " shared/>
                <XAxis categories={this.state.row}>
                    <XAxis.Title>{window.intl.get('date.date')}</XAxis.Title>
                </XAxis>
                <YAxis id="number">
                    <YAxis.Title>{window.intl.get('statistics.total')}</YAxis.Title>
                    <LineSeries id="totalsend" name={window.intl.get('statistics.totalSent')} data={this.state.totalSent}/>
                    <LineSeries id="delivered" name={window.intl.get('statistics.delivered')} data={this.state.totalDelivered}/>
                    <LineSeries id="pending" name={window.intl.get('statistics.pending')} data={this.state.totalPending}/>
                    <LineSeries id="failed" name={window.intl.get('statistics.failed')} data={this.state.totalFailed}/>
                </YAxis>
            </HighchartsChart>

        )
    }
}

export default withHighcharts(MyChart, Highcharts);