import React, {Component} from 'react';
import './Pagination.css';

class Pagination extends Component {

    state = {
        page: 1
    };

    handleChangeField = (field, value) => {
        this.setState({
            [field]: value
        })
    };

    switchPage = (page) => {
        if (page > this.props.totalPage)
            return;
        
        if (typeof this.props.switchPage === 'function') {
            document.getElementById('paginationInput').value = '';
            this.props.switchPage(page)
        }
    };

    render() {
        return (
            <div className="table_pagination">
                <p>{this.props.curPage}/{this.props.totalPage}</p>
                <div className="switch_page">
                    <span onClick={() => this.switchPage(this.props.curPage - 1)}
                          className={this.props.curPage === 1 ? 'disable' : null}
                          title={window.intl.get('pagination.prevPage')}>&lt;</span>
                    &nbsp;
                    <span onClick={() => this.switchPage(this.props.curPage + 1)}
                          className={this.props.curPage >= this.props.totalPage ? 'disable' : null}
                          title={window.intl.get('pagination.nextPage')}>&gt;</span>
                    &nbsp;
                    <input id="paginationInput" onInput={(event) => {this.handleChangeField('page', event.target.value.trim())}}
                           onKeyDown={(e) => {if (e.keyCode === 13) {}}} type="text"/>
                    <span onClick={() => this.switchPage(this.state.page)}
                          className={this.props.totalPage === 1 ? 'disable' : null}
                          title={window.intl.get('pagination.anyPage')}>{window.intl.get('pagination.go')}</span>
                </div>
            </div>
        )
    }
}

export default Pagination;