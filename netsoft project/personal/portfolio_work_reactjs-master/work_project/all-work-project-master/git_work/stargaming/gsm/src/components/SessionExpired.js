function setCookie(cname, cvalue, exdays) {
    let d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    let expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

export default function (data) {

    if(data.error &&
        ( data.error.toLocaleLowerCase()==='session_expired' || data.error.toLocaleLowerCase()==='session expired')
    ){

        localStorage.setItem('isLogin', '');
        localStorage.setItem('client', '');
        window.location.href = '/';
        return
    }

    if(data.error && data.error.toLowerCase()==='system_maintenance'){
        setCookie('PHPSESSID','',-1);
        localStorage.setItem('isLogin', '');
        localStorage.setItem('client', '');
        window.location.href = '/SystemMaintenance';
    }

}