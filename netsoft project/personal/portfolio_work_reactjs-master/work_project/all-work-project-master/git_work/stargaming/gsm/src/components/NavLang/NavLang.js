import React, { Component } from 'react';
import {connect} from 'react-redux';
import {Select} from 'antd';

import {handleLocal} from "../../redux/action";
import fetchApi from "../../fetchApi";

import "./NavLang.css";
const Option = Select.Option;

class NavLang extends Component {

    componentDidMount = () => {

    };

    changeLang = (language) => {
        this.props.handleLocal(language);

        fetchApi({
            api: 'Client/Profile',
            data: {
                action: 'change_language',
                language: language
            }
        }).then(res => res.json()).then((data) => {
            if (data.status.toLocaleLowerCase() === 'error') {
                if(data.error.toLowerCase() === 'unknown_error'){
                    return
                }
            }
        })
    };

    render() {
        return (
            <Select className="navLang" value={this.props.local} onChange={this.changeLang}>
                <Option value='english' key='english'>{window.intl.get('language.english')}</Option>
                <Option value='chinese' key='chinese'>{window.intl.get('language.chinese')}</Option>
            </Select>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        local: state.local
    }
};

function mapDispatchToProps(dispatch) {
    return {
        handleLocal: (local) => {
            dispatch(handleLocal(local))
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(NavLang);