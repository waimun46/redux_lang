import React , { Component } from 'react';
import { Input } from 'antd';
import {connect} from "react-redux";
import _fetch from "../../fetchApi";
import sessionExpired from "../../components/SessionExpired";


class Createuser extends  Component{

    state = {
        name: '',
        submitLoad: false,
        errorMsg: ''
    }

    handleChangeField = (field, value) => {
        this.setState({
            [field]: value
        })
    };

    back = () => {
        this.props.history.push('/Userpage')
    }

    create = () => {
        if (this.state.submitLoad) {
            return
        }
        if (this.state.name.trim().length === 0) {
            this.setState({
                errorMsg: window.intl.get('error.usernameEmpty'),
            });
            return
        }
        this.setState({
            submitLoad: true,
        });
        _fetch({
            api: 'Client/User',
            data: {
                action: 'create',
                name: this.state.name
            }
        }).then(res => res.json()).then((data) => {
            if (data.status.toLocaleLowerCase() === 'error') {
                if (data.error.toLowerCase() === 'unknown_error') {

                }
                sessionExpired(data)
                if (data.msg) {
                    this.setState({
                        errorMsg: data.msg[this.props.local],
                    });
                }
            } else {
                this.props.history.push('/Userpage')
            }
            this.setState({
                submitLoad: false,
            });
        })
    }

    render(){
        return (
            <article className="main_content">
                <header>
                    <h2>{window.intl.get('user.create')}</h2>
                </header>
                <section className="page_content">

                    <div className="title-btn">
                        <span className="backBtn" onClick={this.back}>{window.intl.get('other.back')}</span>
                    </div>

                    {/*content here*/}
                    <div className="half-page">
                        <span className="errorMsg">{this.state.errorMsg}</span>
                        <div className=" select_margin">
                            <label className="label_space">{window.intl.get('user.username')} :</label>
                            <Input className="select_style" type="text"
                                   onInput={(event) => {
                                       this.handleChangeField('name', event.target.value.trim())
                                   }}/>
                        </div>
                        <div className="button-align-setting">
                            <span className="sendmsg_button submitBtn" onClick={this.create}>{window.intl.get('button.submit')}</span>
                        </div>
                    </div>

                </section>
            </article>
        )
    }
}

function mapProps(state){
    return {
        local:state.local
    }
}

export default connect(mapProps)(Createuser);