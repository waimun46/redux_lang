import React, {Component} from 'react';
import {Input} from 'antd';

import {Select} from "antd/lib/index";
import _fetch from "../../fetchApi";
import sessionExpired from "../../components/SessionExpired";
import {connect} from "react-redux";

import Pagination from '../../components/Pagination/Pagination';
import './transferpage.css';
import '../../css/pages.css';

const Option = Select.Option;

class Transferpage extends Component {

    state = {
        to: '',
        credit: '',
        submitLoad: false,
        errorMsg: '',
        mapUser: [],
        client: '',
        user: '0',
        transferSection: true,
        transactionSection: false,

        initDone: false,

        content: [],
        totalRecord: 0,
        totalPage: 0,
        curPage: 1
    };

    componentDidMount = () => {
        this.init()
    };

    changeSection = (event) => {
        if (event.currentTarget.value === 'transfer') {
            this.setState({
                transferSection: true,
                transactionSection: false,
                initDone: false,
                errorMsg: ''
            });
            this.init()
        } else {
            this.setState({
                transferSection: false,
                transactionSection: true,
                initDone: false,
                errorMsg: ''
            });
            this.getTransaction()
        }
    };

    init = () => {
        _fetch({
            api: 'Client/Transfer',
            data: {
                action: 'init'
            }
        }).then(res => res.json()).then((data) => {
            if (data.status.toLocaleLowerCase() === 'error') {
                if (data.error.toLowerCase() === 'unknown_error') {

                }
                sessionExpired(data)
            } else {
                this.setState({
                    mapUser: data.data.user,
                    client: data.data.client,
                    to: data.data.user[0] ? data.data.user[0]['id'] : '',
                    credit: '',
                    initDone: true,
                });
            }
        })
    };

    handleTo = (value) => {
        this.setState({
            to: value
        })
    };

    handleChangeField = (field, value) => {
        this.setState({
            [field]: value
        })
    };

    handleList = (value) => {
        this.setState({
            user: value
        })

        this.getTransaction(value)
    };

    handlePage = (value) => {
        this.getTransaction('',value)
    };

    transfer = () => {
        if (this.state.submitLoad) {
            return
        }

        let credit = this.state.credit.trim().toString();
        if (credit.charAt(0) === '0' || (credit.charAt(0) === '-' && credit.charAt(1) === '0') || !(/^-?\d+$/.test(credit))) {
            this.setState({
                errorMsg: window.intl.get('error.transferError'),
            });
            return
        }

        this.setState({
            submitLoad: true,
        });
        _fetch({
            api: 'Client/Transfer',
            data: {
                action: 'submit',
                credit: credit,
                to: this.state.to
            }
        }).then(res => res.json()).then((data) => {
            if (data.status.toLocaleLowerCase() === 'error') {
                if (data.error.toLowerCase() === 'unknown_error') {

                }
                sessionExpired(data);
                if (data.msg) {
                    this.setState({
                        errorMsg: data.msg[this.props.local],
                    });
                }
            } else {
                this.setState({
                    transferSection: false,
                    transactionSection: true
                });
                this.getTransaction()
            }
            this.setState({
                submitLoad: false,
            });
        })
    };

    getTransaction = (user,page) => {
        _fetch({
            api: 'Client/Transfer',
            data: {
                action: 'list',
                page: page || 1,
                user: user ? user : this.state.user
            }
        }).then(res => res.json()).then((data) => {
            if (data.status.toLocaleLowerCase() === 'error') {
                if (data.error.toLowerCase() === 'unknown_error') {

                }
                sessionExpired(data)
            } else {
                this.setState({
                    content: data.data.reports,
                    totalRecord: data.data.total_record,
                    totalPage: data.data.total_page,
                    curPage: data.data.cur_page,
                    initDone: true,
                    user: user ? user : this.state.user
                });
            }
        })
    };

    render() {
        return (
            <article className="main_content">
                <header>
                    <h2>{window.intl.get('transfer.title')}</h2>
                </header>
                <section className="page_content">
                    {/*content over here*/}
                    <div id="transferpage">
                        <section id="tabs" className="tabs">
                            <input id="tab-1" type="radio" name="radio-set" className="myinput tab-selector-1"
                                   value="transfer" onChange={this.changeSection} checked={this.state.transferSection}/>
                            <label htmlFor="tab-1"
                                   className="mylabel tab-label-1">{window.intl.get('transfer.title')}</label>
                            <input id="tab-2" type="radio" name="radio-set" className="myinput tab-selector-2"
                                   value="transaction" onChange={this.changeSection}
                                   checked={this.state.transactionSection}/>
                            <label htmlFor="tab-2"
                                   className="mylabel tab-label-2">{window.intl.get('transfer.list')}</label>

                            {
                                this.state.initDone &&
                                <div className="content">
                                    <div className="content-1 items">
                                        <div className="half-page">
                                            <span className="errorMsg">{this.state.errorMsg}</span>
                                            <p>{window.intl.get('transfer.notice')}</p>
                                            <div className="row select_margin">
                                                <label className="label_space">{window.intl.get('user.user')}
                                                    :</label>
                                                <Select className="formSelect" value={this.state.to} onChange={this.handleTo}>
                                                    {
                                                        this.state.mapUser && this.state.mapUser.map((item, index) => {
                                                            return (
                                                                <Option value={item.id} key={index}>{item.name}</Option>
                                                            )
                                                        })
                                                    }
                                                </Select>
                                            </div>

                                            <div className="row select_margin">
                                                <label className="label_space">{window.intl.get('credit.credit')}
                                                    :</label>
                                                <Input className="formInput" onInput={(event) => {
                                                    this.handleChangeField('credit', event.target.value.trim())
                                                }}/>
                                            </div>

                                            <div className="row button-align-setting">
                                                <span className="sendmsg_button submitBtn"
                                                      onClick={this.transfer}>{window.intl.get('button.submit')}</span>
                                            </div>

                                        </div>
                                    </div>

                                    <div className="content-2 items">
                                        <div id="table_body">

                                            <div className="row select_margin">
                                                <label>{window.intl.get('user.user')} :  &nbsp;</label>
                                                <Select className="formSelect" value={this.state.user}
                                                        onChange={this.handleList}>
                                                    <Option value='0'>{window.intl.get('select.all')}</Option>
                                                    {
                                                        this.state.mapUser && this.state.mapUser.map((item, index) => {
                                                            return (
                                                                <Option value={item.id} key={index}>{item.name}</Option>
                                                            )
                                                        })
                                                    }
                                                </Select>

                                            </div>
                                            <div className='totalRecord'>
                                                <label>{window.intl.get('pagination.totalRecord')} : </label>
                                                <span>{this.state.totalRecord}</span>
                                                &nbsp;&nbsp;&nbsp;
                                                <label>{window.intl.get('pagination.curPage')} : </label>
                                                <span>{this.state.curPage}</span>
                                            </div>
                                            <table className="table_style report">
                                                <thead>
                                                <tr>
                                                    <th>{window.intl.get('user.user')}</th>
                                                    <th>{window.intl.get('date.datetime')}</th>
                                                    <th>{window.intl.get('credit.credit')}</th>
                                                    <th>{window.intl.get('credit.clientOpenBal')}</th>
                                                    <th>{window.intl.get('credit.clientCloseBal')}</th>
                                                    <th>{window.intl.get('credit.userOpenBal')}</th>
                                                    <th>{window.intl.get('credit.userCloseBal')}</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                {
                                                    this.state.content &&
                                                    this.state.content.map((item, key) => {
                                                        return (
                                                            <tr key={key}>
                                                                <td>{item.user}</td>
                                                                <td>{item.date_time}</td>
                                                                <td>{item.credit}</td>
                                                                <td>{item.client_open_bal}</td>
                                                                <td>{item.client_close_bal}</td>
                                                                <td>{item.user_open_bal}</td>
                                                                <td>{item.user_close_bal}</td>
                                                            </tr>
                                                        )
                                                    })
                                                }
                                                </tbody>
                                            </table>
                                        </div>
                                        <Pagination totalRecord={this.state.totalRecord} totalPage={this.state.totalPage} curPage={this.state.curPage} switchPage={this.handlePage}/>
                                    </div>

                                </div>
                            }

                        </section>
                    </div>
                    {/*content over here*/}
                </section>
            </article>
        )
    }
}

function mapProps(state) {
    return {
        local: state.local
    }
}

export default connect(mapProps)(Transferpage)