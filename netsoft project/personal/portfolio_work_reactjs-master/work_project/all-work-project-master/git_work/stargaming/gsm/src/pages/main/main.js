import React, { Component } from 'react';
import { Select } from 'antd';
import MyChart from'./chart';
import _fetch from "../../fetchApi";
import sessionExpired from "../../components/SessionExpired";

const Option = Select.Option;


class Main extends Component {

    state = {
        hasUser: false,
        user: 0,
        credit: 0,
        report: [],
        statistics: []
    }

    componentDidMount = () => {
        this.getStatistics('')

    }

    getStatistics = (user) => {
        _fetch({
            api: 'Client/Home'
        }).then(res => res.json()).then((data) => {

            if (data.status.toLocaleLowerCase() === 'error') {
                if (data.error.toLowerCase() === 'unknown_error') {

                }
                sessionExpired(data)
            } else {
                if (data.data.reports.length > 0) {
                    if (user) {
                        data.data.reports.forEach((item, index) => {
                            if (item['user'] === user) {
                                this.setState({
                                    user: item['user'],
                                    statistics: item
                                });

                                this.getCredit(user);
                            }
                        })
                    } else {
                        let user = data.data.reports[0] ? data.data.reports[0]['user'] : 0;
                        this.setState({
                            user: user,
                            statistics: data.data.reports[0] ? data.data.reports[0] : []
                        });

                        this.getCredit(user);
                    }

                    this.setState({
                        hasUser: true,
                        report: data.data.reports
                    });
                }
            }
        })
    }

    getCredit = (user) => {
        _fetch({
            api: 'Client/Home',
            data: {
                action: 'get_credit'
            }
        }).then(res => res.json()).then((data) => {
            if (data.status.toLocaleLowerCase() === 'error') {
                if (data.error.toLowerCase() === 'unknown_error') {

                }
                sessionExpired(data)
            } else {
                this.setState({
                    credit: data.data.user[user]
                });
            }
        })
    }

    changeUser = (user) => {
        this.getStatistics(user)
    }

    render() {
        return (
            <article className="main_content">
                <header>
                    <h2>{window.intl.get('home.title')}</h2>
                </header>
                <section className="page_content">
                    {/*page content here*/}

                    {
                        this.state.hasUser &&
                        <div className="header-bottom home_page">
                            <div className="header-head">
                                <span className="text-left">
                                    <p className="acc_style">{window.intl.get('user.user')} :</p>
                                    <Select value={this.state.user} onChange={this.changeUser}>

                                        {
                                            this.state.report && this.state.report.map((item, index) => {
                                                return (
                                                    <Option value={item.user} key={index}>{item.user}</Option>
                                                )
                                            })
                                        }

                                    </Select>
                                </span>
                                <span className="text-right media_style">
                                    <p className="balance_style01">{window.intl.get('home.balance')} :
                                        <label className="balance_style">{this.state.credit}</label>
                                        <label className="cre_style">{window.intl.get('credit.credit')}</label>
                                    </p>
                                </span>
                                <div className="clearfix"></div>
                            </div>


                            <div className="navigation-strip text-center">
                                <div className="main_center">
                                    <ul className="nav navbar-nav">
                                        <li>
                                            <div className="title_bg">
                                                <span>{window.intl.get('statistics.totalSent')}</span>
                                                <label>{this.state.statistics.total.totalsent}</label>
                                            </div>
                                        </li>
                                        <li >
                                            <div className="title_bg">
                                                <span>{window.intl.get('statistics.delivered')}</span>
                                                <label>{this.state.statistics.total.totalsuccess}</label>
                                            </div>
                                        </li>
                                        <li >
                                            <div className="title_bg">
                                                <span>{window.intl.get('statistics.pending')}</span>
                                                <label>{this.state.statistics.total.totalpending}</label>
                                            </div>
                                        </li>
                                        <li >
                                            <div className="title_bg">
                                                <span>{window.intl.get('statistics.failed')}</span>
                                                <label>{this.state.statistics.total.totalfailed}</label>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div className="clearfix"></div>
                            </div>


                            <MyChart statistics={this.state.statistics.statistics}/>
                        </div>
                    }

                </section>
            </article>
        );
    }
}

export default Main
