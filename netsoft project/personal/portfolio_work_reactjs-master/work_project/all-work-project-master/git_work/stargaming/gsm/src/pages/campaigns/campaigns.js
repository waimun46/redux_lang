import React , { Component } from 'react';
import {Link} from 'react-router-dom';
import { Modal } from 'antd';
import _fetch from "../../fetchApi";
import sessionExpired from "../../components/SessionExpired";
import {connect} from "react-redux";
import {handleForwardMsg} from "../../redux/action";
import Pagination from '../../components/Pagination/Pagination';
import '../../css/pages.css';

class Campaigns extends  Component{

    state = {
        visible: false,
        content: [],
        totalPage: 0,
        totalRecord: 0,
        curPage: 1,
        message: ''
    };

    componentDidMount = () => {
        this.getCampaignList()
    };

    getCampaignList = (page) => {
        _fetch({
            api: 'Client/Campaign',
            data: {
                action: 'list',
                page: page || 1
            }
        }).then(res => res.json()).then((data) => {
            if (data.status.toLocaleLowerCase() === 'error') {
                if (data.error.toLowerCase() === 'unknown_error') {

                }

                sessionExpired(data)
            } else {

                this.setState({
                    content: data.data.reports,
                    totalPage: data.data.total_page,
                    totalRecord: data.data.total_record,
                    curPage: data.data.cur_page
                });
            }
        })
    };

    viewCampaign = (campaignId) => {
        let message = '';
        this.state.content[this.props.local].forEach((item, index) => {
            if (item.id === campaignId)
                message = item.message
        });

        this.setState({
            visible: true,
            message: message
        });
    };


    handleOk = (e) => {
        this.setState({
            visible: false,
        });
    };

    handleCancel = (e) => {
        this.setState({
            visible: false,
        });
    };

    detail = (id) => {
        localStorage.setItem('campaign', id);
        this.props.history.push('/Campaigndetail')
    };

    forward = (id) => {
        this.props.handleForwardMsg(id);
        this.props.history.push('/Createcamp')
    }

    render(){
        return (

            <article className="main_content">
                <header>
                    <h2>{window.intl.get('campaign.title')}</h2>
                </header>
                <section className="page_content">
                    {/*content here*/}
                    <div id="campaigns">
                        <div className="title-btn">
                            <Link to="Createcamp"><button className="createuser_btn">{window.intl.get('campaign.create')}</button></Link>
                        </div>
                        <div id="table_body">
                            <div className='totalRecord'>
                                <label>{window.intl.get('pagination.totalRecord')} : </label>
                                <span>{this.state.totalRecord}</span>
                                &nbsp;&nbsp;&nbsp;
                                <label>{window.intl.get('pagination.curPage')} : </label>
                                <span>{this.state.curPage}</span>
                            </div>
                            <table className="table_style report">
                                <thead>
                                <tr>
                                    <th>{window.intl.get('table.no')}</th>
                                    <th>{window.intl.get('date.datetime')}</th>
                                    <th>{window.intl.get('campaign.campaignName')}</th>
                                    <th>{window.intl.get('user.user')}</th>
                                    <th>{window.intl.get('statistics.rate')}</th>
                                    <th>{window.intl.get('action.title')}</th>
                                </tr>
                                </thead>
                                <tbody>
                                {
                                    this.state.content && this.state.content[this.props.local] &&
                                    this.state.content[this.props.local].map((item, key) => {
                                        return (
                                            <tr key={key}>
                                                <td>{key+1}</td>
                                                <td>{item.created_date}</td>
                                                <td>{item.name}</td>
                                                <td>{item.user}</td>
                                                <td>{item.rate}</td>
                                                <td>
                                                    <span className="view_button" onClick={() => this.forward(item.id)}>{window.intl.get('action.copy')}</span>&nbsp;&nbsp;
                                                    <span className="view_button" onClick={() => this.viewCampaign(item.id)}>{window.intl.get('action.viewContent')}</span>&nbsp;&nbsp;
                                                    <span className="view_button" onClick={() => this.detail(item.id)}>{window.intl.get('action.detail')}</span>
                                                </td>
                                            </tr>
                                        )
                                    })
                                }
                                </tbody>
                            </table>
                        </div>
                        <Pagination totalRecord={this.state.totalRecord} totalPage={this.state.totalPage} curPage={this.state.curPage} switchPage={this.getCampaignList}/>

                        <Modal className="userpage"
                               visible={this.state.visible}
                               onCancel={this.handleCancel}
                        >
                            <div className='pop-msg'>
                                <h1 className="title_color">{window.intl.get('sms.content')}</h1>
                                <p className="hplist">{this.state.message}</p>
                            </div>
                        </Modal>
                    </div>
                    {/*content here*/}
                </section>
            </article>
        )
    }
}

function mapProps(state){
    return {
        local:state.local
    }
}

function mapAction(dispatch){
    return {
        handleForwardMsg:(campaign)=>{
            dispatch(handleForwardMsg(campaign))
        }
    }
}

export default connect(mapProps, mapAction)(Campaigns)