import React , { Component } from 'react';
import { Row, Col } from 'antd';
import _fetch from "../../fetchApi";
import sessionExpired from "../../components/SessionExpired";
import {connect} from "react-redux";

import Pagination from '../../components/Pagination/Pagination';
import '../../css/pages.css';

class TopupPage extends  Component{

    state = {
        package: 0,
        submitLoad: false,
        errorMsg: '',
        currency: '',
        mapPackage: [],

        topupSection: true,
        transactionSection: false,

        initDone: false,

        content: [],
        totalRecord: 0,
        totalPage: 0,
        curPage: 1
    }

    componentDidMount = () => {
        this.init()
    }

    changeSection = (event) => {
        if (event.currentTarget.value === 'topup') {
            this.setState({
                topupSection: true,
                transactionSection: false,
                initDone: false
            });
            this.init()
        } else {
            this.setState({
                topupSection: false,
                transactionSection: true,
                initDone: false
            });
            this.getTransaction()
        }
    }

    init = () => {
        _fetch({
            api: 'Client/Topup',
            data: {
                action: 'get_package'
            }
        }).then(res => res.json()).then((data) => {
            if (data.status.toLocaleLowerCase() === 'error') {
                if (data.error.toLowerCase() === 'unknown_error') {

                }
                sessionExpired(data)
            } else {
                this.setState({
                    mapPackage: data.data.package,
                    currency: data.data.currency,
                    package: 0,
                    initDone: true,
                });
            }
        })
    }

    handleChangeField = (field, value) => {
        this.setState({
            [field]: value
        })
    };

    topup = () => {
        if (this.state.submitLoad) {
            return
        }
        if (this.state.package === 0) {
            this.setState({
                errorMsg: window.intl.get('error.packageEmpty'),
            });
            return
        }
        this.setState({
            submitLoad: true,
        });
        _fetch({
            api: 'Client/Topup',
            data: {
                action: 'buy_package',
                package: this.state.package

            }
        }).then(res => res.json()).then((data) => {
            if (data.status.toLocaleLowerCase() === 'error') {
                if (data.error.toLowerCase() === 'unknown_error') {

                }
                sessionExpired(data)
                if (data.msg) {
                    this.setState({
                        errorMsg: data.msg[this.props.local],
                    });
                }
            } else {
                this.setState({
                    topupSection: false,
                    transactionSection: true
                });
                this.getTransaction()
            }
            this.setState({
                submitLoad: false
            });
        })
    }

    getTransaction = (page) => {
        _fetch({
            api: 'Client/Topup',
            data: {
                action: 'transaction',
                page: page || 1
            }
        }).then(res => res.json()).then((data) => {
            if (data.status.toLocaleLowerCase() === 'error') {
                if (data.error.toLowerCase() === 'unknown_error') {

                }
                sessionExpired(data)
            } else {
                this.setState({
                    content: data.data.reports,
                    totalRecord: data.data.total_record,
                    totalPage: data.data.total_page,
                    curPage: data.data.cur_page,
                    initDone: true
                });
            }
        })
    }

    render(){
        return (
            <article className="main_content">
                <header>
                    <h2>{window.intl.get('topup.title')}</h2>
                </header>
                <section className="page_content">
                {/*content here*/}
                    <section id="tabs" className="tabs">
                        <input id="tab-1" type="radio" name="radio-set" className="myinput tab-selector-1" value="topup" onChange={this.changeSection} checked={this.state.topupSection}/>
                        <label htmlFor="tab-1" className="mylabel tab-label-1">{window.intl.get('topup.title')}</label>
                        <input id="tab-2" type="radio" name="radio-set" className="myinput tab-selector-2" value="transaction" onChange={this.changeSection} checked={this.state.transactionSection}/>
                        <label htmlFor="tab-2" className="mylabel tab-label-2">{window.intl.get('topup.transaction')}</label>

                        {
                            this.state.initDone &&
                            <div  className="content">
                                <div className="content-1 items">
                                    <div className="full-page">
                                        <span className="errorMsg">{this.state.errorMsg}</span>
                                        <div className="row select_margin">
                                            <div className="topup-cat">
                                                <Row>
                                                {
                                                    this.state.mapPackage && this.state.mapPackage.map((item, index) => {
                                                        return (
                                                            <Col key={index}>
                                                                <label htmlFor={item.package}>
                                                                    <input type="radio" name="package" value={item.package} id={item.package} onChange={(event) => {
                                                                        this.handleChangeField('package', event.target.value)
                                                                    }} checked={item.package.toString() === this.state.package}/>
                                                                    &nbsp;&nbsp;
                                                                    <span className="topup-credit">
                                                                        {item.credit + ' ' + window.intl.get('credit.credit') + ' (' + this.state.currency + ' ' + item.rate + ' /s)'}
                                                                    </span>
                                                                </label>
                                                            </Col>
                                                        )
                                                    })
                                                }
                                                </Row>
                                            </div>
                                        </div>

                                        <div className="button-align-setting">
                                            <span className="sendmsg_button submitBtn" onClick={this.topup}>{window.intl.get('button.submit')}</span>
                                        </div>
                                    </div>
                                </div>

                                <div className="content-2 items">
                                    <div id="table_body">
                                        <div className='totalRecord'>
                                            <label>{window.intl.get('pagination.totalRecord')} : </label>
                                            <span>{this.state.totalRecord}</span>
                                            &nbsp;&nbsp;&nbsp;
                                            <label>{window.intl.get('pagination.curPage')} : </label>
                                            <span>{this.state.curPage}</span>
                                        </div>
                                        <table className="table_style report">
                                            <thead>
                                            <tr>
                                                <th>{window.intl.get('date.datetime')}</th>
                                                <th>{window.intl.get('topup.invoiceNo')}</th>
                                                <th>{window.intl.get('credit.credit')}</th>
                                                <th>{window.intl.get('other.status')}</th>
                                                <th>{window.intl.get('action.download')}</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            {
                                                this.state.content && this.state.content[this.props.local] &&
                                                this.state.content[this.props.local].map((item, key) => {
                                                    return (
                                                        <tr key={key}>
                                                            <td>{item.created_date}</td>
                                                            <td>{item.invoice_no}</td>
                                                            <td>{item.credit}</td>
                                                            <td>{item.status_display}</td>
                                                            <td>
                                                                <span className="inv_button" onClick={() => window.open(item.invoice_url)}>
                                                                    {window.intl.get('topup.invoice')}&nbsp;
                                                                    <span className="gly-margin"><i className="glyphicon glyphicon-arrow-down"></i></span>
                                                                </span>
                                                                {
                                                                    item.status === 3 ?
                                                                        <span className="pay_button" onClick={() => window.open(item.payment_url)}>
                                                                            {window.intl.get('topup.payment')}&nbsp;
                                                                            <span className="gly-margin"><i className="glyphicon glyphicon-arrow-down"></i></span>
                                                                        </span> :
                                                                        ''
                                                                }
                                                            </td>
                                                        </tr>
                                                    )
                                                })
                                            }
                                            </tbody>
                                        </table>
                                    </div>
                                    <Pagination totalRecord={this.state.totalRecord} totalPage={this.state.totalPage} curPage={this.state.curPage} switchPage={this.getTransaction}/>
                                </div>

                            </div>
                        }

                    </section>

                {/*content here*/}
                </section>
            </article>
        )
    }
}

function mapProps(state){
    return {
        local:state.local
    }
}

export default connect(mapProps)(TopupPage)