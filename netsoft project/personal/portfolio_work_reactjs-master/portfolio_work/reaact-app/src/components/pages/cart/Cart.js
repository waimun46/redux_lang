import React, {Component} from 'react';
import { browserHistory } from 'react-router'
import $ from "jquery";



class Cart extends Component {

    componentDidMount() {
        let shoppingCart = (function () {
            // =============================
            // Private methods and propeties
            // =============================
            let cart = [];

            // Constructor
            function Item(name, price, count) {
                this.name = name;
                this.price = price;
                this.count = count;
            }

            // Save cart
            function saveCart() {
                sessionStorage.setItem('shoppingCart', JSON.stringify(cart));
            }

            // Load cart
            function loadCart() {
                cart = JSON.parse(sessionStorage.getItem('shoppingCart'));
            }

            if (sessionStorage.getItem("shoppingCart") != null) {
                loadCart();
            }


            // =============================
            // Public methods and propeties
            // =============================
            var obj = {};

            // Add to cart
            obj.addItemToCart = function (name, price, count) {
                for (var item in cart) {
                    if (cart[item].name === name) {
                        cart[item].count++;
                        saveCart();
                        return;
                    }
                }
                var item = new Item(name, price, count);
                cart.push(item);
                saveCart();
            }
            // Set count from item
            obj.setCountForItem = function (name, count) {
                for (var i in cart) {
                    if (cart[i].name === name) {
                        cart[i].count = count;
                        break;
                    }
                }
            };
            // Remove item from cart
            obj.removeItemFromCart = function (name) {
                for (var item in cart) {
                    if (cart[item].name === name) {
                        cart[item].count--;
                        if (cart[item].count === 0) {
                            cart.splice(item, 1);
                        }
                        break;
                    }
                }
                saveCart();
            }

            // Remove all items from cart
            obj.removeItemFromCartAll = function (name) {
                for (var item in cart) {
                    if (cart[item].name === name) {
                        cart.splice(item, 1);
                        break;
                    }
                }
                saveCart();
            }

            // Clear cart
            obj.clearCart = function () {
                cart = [];
                saveCart();
            }

            // Count cart
            obj.totalCount = function () {
                var totalCount = 0;
                for (var item in cart) {
                    totalCount += cart[item].count;
                }
                return totalCount;
            }

            // Total cart
            obj.totalCart = function () {
                var totalCart = 0;
                for (var item in cart) {
                    totalCart += cart[item].price * cart[item].count;
                }
                return Number(totalCart.toFixed(2));
            }

            // List cart
            obj.listCart = function () {
                var cartCopy = [];
                var i = i;
                var p = p;
                for (i in cart) {
                    let item = cart[i];
                    let itemCopy = {};
                    for (p in item) {
                        itemCopy[p] = item[p];

                    }
                    itemCopy.total = Number(item.price * item.count).toFixed(2);
                    cartCopy.push(itemCopy)
                }
                return cartCopy;
            }

            // cart : Array
            // Item : Object/Class
            // addItemToCart : Function
            // removeItemFromCart : Function
            // removeItemFromCartAll : Function
            // clearCart : Function
            // countCart : Function
            // totalCart : Function
            // listCart : Function
            // saveCart : Function
            // loadCart : Function
            return obj;
        })();


// *****************************************
// Triggers / Events
// *****************************************
// Add item
        $('.add-to-cart').click(function (event) {
            event.preventDefault();
            var name = $(this).data('name');
            var price = Number($(this).data('price'));
            shoppingCart.addItemToCart(name, price, 1);
            displayCart();
        });

// Clear items
        $('.clear-cart').click(function () {
            shoppingCart.clearCart();
            displayCart();
        });


        function displayCart() {
            var cartArray = shoppingCart.listCart();
            var output = "";
            for (var i in cartArray) {
                output += "<div class='panel-body'>"
                    + "<div class='col-xs-4'>" + cartArray[i].name + "  <span>(" + cartArray[i].price + ")</span></div>"
                    /* + "<div class='cart_price'>(" + cartArray[i].price + ")</div>"*/
                    + "<div class='col-xs-4'><div class='input-group'><button class='plus-item btn btn-primary input-group-addon' data-name=" + cartArray[i].name + ">+</button>"
                    + "<button class='minus-item input-group-addon btn btn-primary' data-name=" + cartArray[i].name + ">-</button></div></div>"
                    /* + "<input type='number' class='item-count form-control' data-name='" + cartArray[i].name + "' value='" + cartArray[i].count + "'>"
                     + " = "*/
                    + "<div class='col-xs-4'><div class='col-xs-8'>" + cartArray[i].total + "</div>"
                    + "<div class='col-xs-4 margin_cart'><button class='delete-item btn btn-danger' data-name=" + cartArray[i].name + ">X</button></div></div>"
                    + ""
                    + "</div>";
            }
            $('.show-cart').html(output);
            $('.total-cart').html(shoppingCart.totalCart());
            $('.total-count').html(shoppingCart.totalCount());
        }


// Delete item button

        $('.show-cart').on("click", ".delete-item", function (event) {
            var name = $(this).data('name')
            shoppingCart.removeItemFromCartAll(name);
            displayCart();
        })


// -1
        $('.show-cart').on("click", ".minus-item", function (event) {
            var name = $(this).data('name')
            shoppingCart.removeItemFromCart(name);
            displayCart();
        })
// +1
        $('.show-cart').on("click", ".plus-item", function (event) {
            var name = $(this).data('name')
            shoppingCart.addItemToCart(name);
            displayCart();
        })

// Item count input
        $('.show-cart').on("change", ".item-count", function (event) {
            var name = $(this).data('name');
            var count = Number($(this).val());
            shoppingCart.setCountForItem(name, count);
            displayCart();

        });

        displayCart();
    }

    checkout() {
        browserHistory.push('/checkout');
    }




    render() {
        return (
            <section id="Cart">
                <div className="modal fade" id="cart" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                     aria-hidden="true">
                    <div className="modal-dialog modal-lg" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h4 className="modal-title" id="exampleModalLabel">Cart</h4>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="modal-body">
                                <div className="show-cart ">

                                </div>
                                <div className="price_sty">Total price: <span className="total_sty"><small>RM</small><span
                                    className="total-cart"></span></span></div>
                            </div>
                            <div className="modal-footer">
                                <button className="clear-cart btn btn-danger">Clear Cart</button>
                                <button type="button" className="btn btn-primary" data-dismiss="modal" onClick={this.checkout}>Proceed To Checkout</button>
                                {
                                    /*
                                     <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>

                                     */
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}

export default Cart;
