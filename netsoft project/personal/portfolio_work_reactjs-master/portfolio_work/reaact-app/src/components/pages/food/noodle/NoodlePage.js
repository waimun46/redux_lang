import React, {Component} from 'react';
import MenuPage from '../../../menu/MenuPage';
import NoodleForm from './NoodleForm';
import BottomMenu from '../../../menu/BottomMenu';
import Cart from '../../cart/Cart'

class NoodlePage extends Component {

    render() {
        return (
            <section id="NoodlePage">
                <MenuPage title="Noodle"/>
                <main>
                    <NoodleForm/>
                    <Cart/>
                </main>
                <BottomMenu/>
            </section>
        );
    }
}

export default NoodlePage;
