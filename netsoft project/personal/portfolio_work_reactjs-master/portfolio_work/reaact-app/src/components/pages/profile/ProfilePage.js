import React, {Component} from 'react';
import MenuPage from '../../menu/MenuPage';
import BottomMenu from '../../menu/BottomMenu';
import ProfileForm from './ProfileForm';
import Cart from '../cart/Cart'


class ProfilePage extends Component {
    render() {
        return (
            <section id="ProfilePage">
                <MenuPage title="Profile"/>
                <main>
                    <ProfileForm/>
                    <Cart/>
                </main>
                <BottomMenu/>
            </section>
        );
    }
}

export default ProfilePage;
