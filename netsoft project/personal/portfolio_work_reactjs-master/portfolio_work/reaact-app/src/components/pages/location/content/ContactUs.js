import React, {Component} from 'react';


class ContactUs extends Component {
    render() {
        return (
            <div id="FoodPage" className="ContactUs FoodForm">
                <div className="food_banner">
                    <div className="banner">
                        <div className="banner_title">
                            <h2>Get In Touch</h2>

                            <div className=" row contact-info">
                                <div className="col-md-12 col-sm-12">
                                    <p className="addre"><i class="fa fa-map-marker"></i>Bandar Sunway, 47500 Subang Jaya, Selangor</p>
                                </div>
                                <div className="col-md-12 col-sm-12">
                                    <p className="phone-call"><i className="fa fa-phone"></i> <a href="tel:+10484579845">03-7494 3100</a></p>
                                </div>
                                <div className="col-md-12 col-sm-12">
                                    <p className="mail-id"><i className="fa fa-envelope"></i>contact@sunway.com</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="food_menu">
                    <ul>
                        <li>
                            <div className="inner_li">
                                <div className="contact-form">
                                    <form className="row"  id="contact" name="contact" method="post" >
                                        <div className="form-group col-md-6">
                                            <input type="text" className="form-control" name="InputName" id="InputName" placeholder="Your Name" required />
                                        </div>
                                        <div className="form-group col-md-6">
                                            <input type="email" className="form-control" name="InputEmail" id="InputEmail" placeholder="Your Email" required />
                                        </div>
                                        <div className="form-group col-md-12">
                                            <input type="text" className="form-control" name="InputWeb" id="InputWeb" placeholder="Web Site (Optional)" />
                                        </div>
                                        <div className="form-group col-md-12">
                                            <textarea className="form-control" name="InputMessage" id="InputMessage" rows="4" placeholder="Message" required  ></textarea>
                                        </div>
                                        <div className="col-md-12">
                                            <button name="submit" type="submit" className="btn normal-btn dart-btn-xs">SEND MESSAGE</button>
                                        </div>
                                    </form>
                                </div>

                            </div>


                        </li>
                    </ul>
                </div>
            </div>
        );
    }
}

export default ContactUs;
