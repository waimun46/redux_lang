import React, {Component} from 'react';
import $ from "jquery";
import MenuPage from '../../menu/MenuPage';
import BottomMenu from '../../menu/BottomMenu';
import FoodForm from './FoodForm';
import Cart from '../cart/Cart'

class FoodPage extends Component {

    componentDidMount() {
        $(document).ready(function () {
            $(".hover").mouseleave(
                function () {
                    $(this).removeClass("hover");
                }
            );
        });
    }

    render() {
        return (
            <section id="FoodPage">
                <MenuPage title="Food"/>
                <main>
                    <FoodForm/>
                    <Cart/>
                </main>
                <BottomMenu/>
            </section>
        );
    }
}

export default FoodPage;
