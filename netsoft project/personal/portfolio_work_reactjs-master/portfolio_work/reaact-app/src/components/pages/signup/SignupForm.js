import React, {Component} from 'react';
import PropTypes from 'prop-types';
import $ from "jquery";
import classnames from 'classnames';
import {browserHistory} from 'react-router';
import TextFieldGroup from '../../common/TextFieldGroup'

import {Icon} from 'antd';
import {Link} from 'react-router';


class SignupForm extends Component {

    constructor(props) {
        super(props);
        this.state = {
            username: '',
            email: '',
            password: '',
            passwordConfirmation: '',
            errors: {}
        };
        this.onChange = this.onChange.bind(this);
    }

    onChange(e) {
        this.setState({ [e.target.name] : e.target.value });
    }


    submit() {

        let username=$("#name").val();
        let email=$("#email").val();
        let password=$("#pass").val();
        let passwordConfirmation=$("#passwordConfirmation").val();

        if(username == 'test' && email == 'test@gmail.com' && password == '123' && passwordConfirmation == '123'){
            return browserHistory.push('home');
        }else{
            $(".text-danger").html("All field is required.").show().fadeOut(8000);
        }

    }



    render() {
        const {errors} = this.state;
        return (
            <div id="SignupForm">
                <form onSubmit={this.onSubmit}>
                    <span className="text-danger"></span>
                    <TextFieldGroup
                        id="name"
                        error={errors.username}
                        label="User Name "
                        onChange={this.onChange}
                        value={this.state.username}
                        field="username"
                        placeholder="Enter Username"
                    />
                    <TextFieldGroup
                        id="email"
                        error={errors.email}
                        label="Email"
                        onChange={this.onChange}
                        value={this.state.email}
                        field="email"
                        placeholder="Enter Email"
                    />
                    <TextFieldGroup
                        id="pass"
                        error={errors.password}
                        label="Password"
                        type="password"
                        onChange={this.onChange}
                        value={this.state.password}
                        field="password"
                        placeholder="Enter Password"
                    />

                    <TextFieldGroup
                        id="passwordConfirmation"
                        error={errors.passwordConfirmation}
                        label="Password Confirmation"
                        type="password"
                        onChange={this.onChange}
                        value={this.state.passwordConfirmation}
                        field="passwordConfirmation"
                        placeholder="Enter Password Confirmation"
                    />

                    <div className="login_btn">
                        <input disabled={this.state.isLoading} type="button" value="Submit" onClick={this.submit}/>
                    </div>
                </form>
            </div>
        );
    }
}





SignupForm.contextTypes = {
    router: PropTypes.object.isRequired
}

export default SignupForm;
