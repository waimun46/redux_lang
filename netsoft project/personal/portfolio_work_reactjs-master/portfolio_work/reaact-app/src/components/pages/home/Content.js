import React, {Component} from 'react';
import {Tabs, Icon} from 'antd';
import {Link} from 'react-router';
import {Rate} from 'antd';
import img01 from '../../../images/04.jpg';

const TabPane = Tabs.TabPane;

class Content extends Component {
    render() {
        return (
            <div id="Content">
                <Tabs defaultActiveKey="1">
                    <TabPane tab={<span><i className="fab fa-elementor"></i>Menu</span>} key="1">
                        <div className="page page-start active" data-page="start">
                            <Link to="food">
                                <div className="panel panel-link textcenter time1">
                                    <i className="fas fa-utensils"></i>
                                    <span className="panel-title">Food</span>
                                </div>
                            </Link>

                            <Link to="beverage">
                                <div className="panel panel-link textcenter time2">
                                    <i className="fas fa-glass-martini"></i>
                                    <span className="panel-title">Beverage</span>
                                </div>
                            </Link>

                            <Link to="dessert">
                                <div className="panel panel-link textcenter time2">
                                    <i className="fas fa-birthday-cake"></i>
                                    <span className="panel-title">Dessert</span>
                                </div>
                            </Link>

                            <Link to="location">
                                <div className="panel panel-link textcenter time3">
                                    <i className="fas fa-map-marker-alt"></i>
                                    <span className="panel-title">Location</span>
                                </div>
                            </Link>

                        </div>
                    </TabPane>
                    <TabPane tab={<span><i className="fas fa-gift"></i>Today Special</span>} key="2">
                        <div className="today_spc">
                            <div className="book-grid ">
                                <div className="book">
                                    <img src={img01}/>
                                </div>
                                <div className="book-info">
                                    <p>Lorem Ipsum is simply</p>
                                    <hr/>
                                    <div className="stars-rate">
                                        <div className="rating">
                                            <Rate disabled defaultValue={2}/>
                                        </div>
                                        <div className="heart">
                                            <i className="fas fa-heart"></i>
                                        </div>
                                        <div className="clearfix"></div>
                                    </div>
                                </div>
                            </div>

                            <div className="book-grid ">
                                <div className="book">
                                    <img src={img01}/>
                                </div>
                                <div className="book-info">
                                    <p>Lorem Ipsum is simply</p>
                                    <hr/>
                                    <div className="stars-rate">
                                        <div className="rating">
                                            <Rate disabled defaultValue={2}/>
                                        </div>
                                        <div className="heart">
                                            <i className="fas fa-heart"></i>
                                        </div>
                                        <div className="clearfix"></div>
                                    </div>
                                </div>
                            </div>

                            <div className="book-grid ">
                                <div className="book">
                                    <img src={img01}/>
                                </div>
                                <div className="book-info">
                                    <p>Lorem Ipsum is simply</p>
                                    <hr/>
                                    <div className="stars-rate">
                                        <div className="rating">
                                            <Rate disabled defaultValue={2}/>
                                        </div>
                                        <div className="heart">
                                            <i className="fas fa-heart"></i>
                                        </div>
                                        <div className="clearfix"></div>
                                    </div>
                                </div>
                            </div>

                            <div className="book-grid ">
                                <div className="book">
                                    <img src={img01}/>
                                </div>
                                <div className="book-info">
                                    <p>Lorem Ipsum is simply</p>
                                    <hr/>
                                    <div className="stars-rate">
                                        <div className="rating">
                                            <Rate disabled defaultValue={2}/>
                                        </div>
                                        <div className="heart">
                                            <i className="fas fa-heart"></i>
                                        </div>
                                        <div className="clearfix"></div>
                                    </div>
                                </div>
                            </div>
                            <div className="clearfix"></div>
                        </div>
                    </TabPane>
                </Tabs>

            </div >
        )
            ;
    }
}

export default Content;
