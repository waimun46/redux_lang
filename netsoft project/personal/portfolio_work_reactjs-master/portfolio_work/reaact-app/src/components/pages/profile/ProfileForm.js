import React, {Component} from 'react';
import {Avatar} from 'antd';
import {Link} from 'react-router';

class ProfileForm extends Component {
    render() {
        return (
            <div className="ProfileForm">
                <div className="user">
                    <div className="user1">
                        <Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png"/>
                    </div>
                    <div className="user2">
                        <h4>Mary</h4>
                        <ul>
                            <li>
                                <span className="fas fa-phone" aria-hidden="true"></span>
                                <span className="user2_span">0123456789</span>
                            </li>
                            <li>
                                <span className="fas fa-envelope" aria-hidden="true"></span>
                                <span className="user2_span">mary@gmail.com</span>
                            </li>
                            <li>
                                <span className="fas fa-map-marker-alt" aria-hidden="true"></span>
                                <span className="user2_span">Petaling Jaya, Selangor</span>
                            </li>
                        </ul>
                    </div>
                    <div className="clearfix"></div>
                </div>
                <div className="flowers">
                    <div className="flower-grid">
                        <h5>200.00</h5>
                        <p>Your belance</p>
                    </div>

                    <div className="clearfix"></div>
                </div>
                <div className="form_sty">
                    <form id="contact" name="contact" method="post">
                        <div className="form-group col-md-6">
                            <input type="text" className="form-control" name="InputName" id="InputName"
                                   placeholder="Username" required/>
                        </div>
                        <div className="form-group col-md-6">
                            <input type="email" className="form-control" name="InputEmail" id="InputEmail"
                                   placeholder="Email" required/>
                        </div>
                        <div className="form-group col-md-12">
                            <input type="text" className="form-control" name="InputWeb" id="InputWeb"
                                   placeholder="Phone"/>
                        </div>
                        <div className="form-group col-md-12">
                            <input type="text" className="form-control" name="InputWeb" id="InputWeb"
                                   placeholder="Address"/>
                        </div>

                    </form>
                    <Link to="home">
                        <div className="btn_sty col-md-12">
                            <button name="submit" type="submit" className="btn normal-btn dart-btn-xs">Update
                            </button>
                        </div>
                    </Link>
                </div>
            </div>
        );
    }
}

export default ProfileForm;
