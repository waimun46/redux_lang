import React, {Component} from 'react';
import {Tabs, Icon} from 'antd';
import Map from './content/Map';
import ContactUs from './content/ContactUs';

const TabPane = Tabs.TabPane;

class LocationForm extends Component {
    render() {
        return (
            <div className="LocationForm">
                <Tabs defaultActiveKey="1">
                    <TabPane tab={<span><i className="fas fa-map-pin"></i>Map</span>} key="1">
                       <Map/>
                    </TabPane>
                    <TabPane className="contact_sty" tab={<span><i className="fas fa-phone-square"></i>Contact Us</span>} key="2">
                        <ContactUs/>
                    </TabPane>
                </Tabs>
            </div>
        );
    }
}

export default LocationForm;
