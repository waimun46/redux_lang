import React, {Component} from 'react';
import MenuPage from '../../../menu/MenuPage';
import VegeForm from './VegeForm';
import BottomMenu from '../../../menu/BottomMenu';
import Cart from '../../cart/Cart'

class VegePage extends Component {
    render() {
        return (
            <section id="VegePage">
                <MenuPage title="Vegetarian"/>
                <main>
                    <VegeForm/>
                    <Cart/>
                </main>
                <BottomMenu/>
            </section>
        );
    }
}

export default VegePage;
