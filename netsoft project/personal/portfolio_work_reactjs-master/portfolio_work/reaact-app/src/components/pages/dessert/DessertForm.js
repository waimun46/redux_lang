import React, {Component} from 'react';
import {Link} from 'react-router';

import icon01 from '../../../images/cake.png';
import icon02 from '../../../images/ice2.png';

class DessertForm extends Component {
    render() {
        return (
            <div id="FoodPage" className="DessertForm FoodForm">
                <div className="food_banner">
                    <div className="banner">
                        <div className="banner_title">
                            <h2>Menu Category</h2>
                        </div>
                    </div>
                </div>
                <div className="food_menu">
                    <ul>
                        <li>
                            <Link to="cake">
                                <div className="inner_li">
                                    <div className="img_sty">
                                        <img src={icon01}/>
                                    </div>
                                    <span>Cake</span>
                                </div>
                            </Link>
                        </li>

                        <li>
                            <Link to="ice">
                                <div className="inner_li">
                                    <div className="img_sty">
                                        <img src={icon02}/>
                                    </div>
                                    <span>Ice Cream</span>
                                </div>
                            </Link>
                        </li>

                        <div className="clearfix"></div>
                    </ul>

                </div>
            </div>
        );
    }
}

export default DessertForm;
