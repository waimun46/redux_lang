import React, {Component} from 'react';
import {Link} from 'react-router';

import icon01 from '../../../images/cof.png';
import icon02 from '../../../images/sof.png';



class BeverageForm extends Component {
    render() {
        return (
            <div id="FoodPage" className="BeverageForm FoodForm">
                <div className="food_banner">
                    <div className="banner">
                        <div className="banner_title">
                            <h2>Menu Category</h2>
                        </div>
                    </div>
                </div>
                <div className="food_menu">
                    <ul>
                        <li>
                            <Link to="coffee">
                                <div className="inner_li">
                                    <div className="img_sty">
                                        <img src={icon01}/>
                                    </div>
                                    <span>Coffee </span>
                                </div>
                            </Link>
                        </li>

                        <li>
                            <Link to="drink">
                                <div className="inner_li">
                                    <div className="img_sty">
                                        <img src={icon02}/>
                                    </div>
                                    <span>Soft Drink</span>
                                </div>
                            </Link>
                        </li>

                        <div className="clearfix"></div>
                    </ul>

                </div>

            </div>
        );
    }
}

export default BeverageForm;
