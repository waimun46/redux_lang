import React, {Component} from 'react';
import foodlogo from '../../../images/food.png';

class LoadingPage extends Component {
    render() {
        return (
            <section id="LoadingPage">
                <div className="loading_img">
                    <div className="margin_inner">
                        <img src={foodlogo} alt="foodlogo"/>
                    </div>
                </div>
            </section>
        );
    }
}

export default LoadingPage;
