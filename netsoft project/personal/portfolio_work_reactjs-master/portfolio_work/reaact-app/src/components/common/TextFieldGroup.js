import React, {Component} from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

const TextFieldGroup = ({ field, value, label, type, onChange, error, placeholder }) => {
    return (
        <div className={classnames("form-group", {'has-error': error})}>
            <label>{label}</label>
            <input
                value={value}
                type={type}
                name={field}
                onChange={onChange}
                placeholder={placeholder}
            />

        </div>
    );
};

TextFieldGroup.propTypes = {
    field: PropTypes.string.isRequired,
    value: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
    error: PropTypes.string,
    type: PropTypes.string.isRequired,
    placeholder: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired
}

TextFieldGroup.defaultProps = {
    type: 'text',


}

export default TextFieldGroup;