import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {browserHistory} from 'react-router';
import $ from "jquery";
import {Link} from 'react-router';
import {Icon, Avatar} from 'antd';


class MenuPage extends Component {
    componentDidMount(){
        $(document).ready(function(){

            $('.menu-toggle').on('click', function(e){
                e.preventDefault();
                $('.menu').toggleClass('off');
            });

            $('.navicon').on('click', function() {
                $(this).toggleClass('open');
            });

        });
    }

    render() {
        const {title} = this.props;
        return (
            <header id="MenuPage">
                <div className="menu-top">

                    <div className="home_btn"  onClick={this.context.router.goBack}>
                        <i className=" faa-horizontal animated fal fa-long-arrow-left"
                           style={{fontSize: 28, color: '#fff'}}></i>
                    </div>
                    <div className="head_title">{title}</div>


                    <div className="icon_pos">
                        <a href="#" className="menu-toggle navicon menu1">
                            <span></span>
                            <span className="sp_sty"></span>
                            <span></span>
                        </a>
                    </div>


                    <nav className="menu off">
                        <ul className="nav">
                            <li className="li_height">
                                <div className="pro_sty">
                                    <div className="pro_pic">
                                        <Avatar style={{backgroundColor: '#858585'}} icon="user"/>
                                    </div>
                                    <div className="pro_name">
                                        <span>Mary</span>
                                        <div className="pro_balance">
                                            <span>RM 200.00</span>
                                        </div>
                                    </div>
                                </div>
                            </li>

                            <hr/>
                            <Link to="food">
                            <li className="menu_title">
                                <div className="logo">
                                    <div className="logo_bg" style={{background: '#e76b63'}}>
                                        <i className="fal fa-utensils" style={{fontSize: 18, color: '#fff'}}></i>
                                    </div>
                                </div>
                                <div className="title">
                                    <span>Food</span>
                                </div>
                                <div className="clearfix"></div>
                            </li>
                            </Link>

                            <Link to="beverage">
                            <li className="menu_title">
                                <div className="logo">
                                    <div className="logo_bg" style={{background: '#e76b63'}}>
                                        <i className="fal fa-glass-martini" style={{fontSize: 18, color: '#fff'}}></i>
                                    </div>
                                </div>
                                <div className="title">
                                    <span>Beverage</span>
                                </div>
                                <div className="clearfix"></div>
                            </li>
                            </Link>

                            <Link to="dessert">
                            <li className="menu_title">
                                <div className="logo">
                                    <div className="logo_bg" style={{background: '#e76b63'}}>
                                        <i className="fal fa-birthday-cake" style={{fontSize: 18, color: '#fff'}}></i>
                                    </div>
                                </div>
                                <div className="title">
                                    <span>Dessert</span>
                                </div>
                                <div className="clearfix"></div>
                            </li>
                            </Link>
                            {/*
                                <li className="menu_title">
                                    <div className="logo">
                                        <div className="logo_bg" style={{background: '#f03978'}}>
                                            <i className="fal fa-cart-arrow-down"
                                               style={{fontSize: 18, color: '#fff'}}></i>
                                        </div>
                                    </div>
                                    <div className="title">
                                        <span>Your order</span>
                                    </div>
                                    <div className="clearfix"></div>
                                </li>
                            */}
                            <hr/>


                            <div className="menu_bottom">
                                {/*
                                    <li className="menu_title">
                                        <div className="logo">
                                            <i className="fal fa-globe" style={{fontSize: 20, color: '#fff'}}></i>
                                        </div>
                                        <div className="title">
                                            <span>language</span>
                                        </div>
                                        <div className="clearfix"></div>
                                    </li>
                                    < li className="menu_title">
                                    <div className="logo">
                                    <i className="fal fa-sliders-h" style={{fontSize: 20, color: '#fff'}}></i>
                                    </div>
                                    <div className="title">
                                    <span>Setting</span>
                                    </div>
                                    <div className="clearfix"></div>
                                    </li>
                                */}
                                <Link to="/">
                                    <li className="menu_title">
                                        <div className="logo ">
                                            <i className="faa-horizontal animated  fal fa-sign-out-alt"
                                               style={{fontSize: 20, color: '#fff'}}></i>
                                        </div>
                                        <div className="title">
                                            <span>Logout</span>
                                        </div>
                                        <div className="clearfix"></div>
                                    </li>
                                </Link>
                            </div>

                        </ul>

                    </nav>

                </div>

            </header>
        );
    }
}


MenuPage.contextTypes = {
    router: PropTypes.object.isRequired
}



export default MenuPage;
