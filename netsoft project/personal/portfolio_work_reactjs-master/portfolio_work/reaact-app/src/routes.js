import React, {Component} from 'react';
import { Route, Router, browserHistory } from 'react-router';
import LoadingPage from './components/pages/loading/LoadingPage';
import LoginPage from './components/pages/login/LoginPage';
import SignupPage from './components/pages/signup/SignupPage';
import HomePage from './components/pages/home/HomePage';
import FoodPage from './components/pages/food/FoodPage';
import NoodlePage from './components/pages/food/noodle/NoodlePage';
import RicePage from './components/pages/food/rice/RicePage';
import SoupPage from './components/pages/food/soup/SoupPage';
import VegePage from './components/pages/food/vege/VegePage';
import BeveragePage from './components/pages/beverage/BeveragePage';
import CoffeePage from './components/pages/beverage/coffee/CoffeePage';
import DrinkPage from './components/pages/beverage/drink/DrinkPage';
import DessertPage from './components/pages/dessert/DessertPage';
import CakePage from './components/pages/dessert/cake/CakePage';
import IcePage from './components/pages/dessert/ice/IcePage';
import LocationPage from './components/pages/location/LocationPage';
import ProfilePage from './components/pages/profile/ProfilePage';
import Cart from './components/pages/cart/Cart';
import MenuPage from './components/menu/MenuPage';

import CheckoutPage from './components/pages/checkout/CheckoutPage';

class Routes extends Component {

    render() {
        return (
            <Router history={browserHistory}>

                <Route exact path="/loading" component={LoadingPage}/>
                <Route exact path="/" component={LoginPage}/>
                <Route exact path="/signup" component={SignupPage}/>

                <Route path="/home" component={HomePage}/>
                <Route path="/food" component={FoodPage}/>
                <Route path="/noodle" component={NoodlePage}/>
                <Route path="/rice" component={RicePage}/>
                <Route path="/soup" component={SoupPage}/>
                <Route path="/vege" component={VegePage}/>
                <Route path="/beverage" component={BeveragePage}/>
                <Route path="/coffee" component={CoffeePage}/>
                <Route path="/drink" component={DrinkPage}/>
                <Route path="/dessert" component={DessertPage}/>
                <Route path="/cake" component={CakePage}/>
                <Route path="/ice" component={IcePage}/>
                <Route path="/location" component={LocationPage}/>
                <Route path="/profile" component={ProfilePage}/>
                <Route path="/cart" component={Cart}/>
                <Route path="/checkout" component={CheckoutPage}/>

            </Router>
        )
    }
}


export default Routes