import express from 'express';
import path from 'path';
import bodyParser from 'body-parser';

import users from './server/route/users';


let app = express();

app.use(bodyParser.json());

app.use('/api/users', users);


/*app.get('/', (req, res) => {
 res.send('hello..ggg.');
 });*/


app.listen('3001');
