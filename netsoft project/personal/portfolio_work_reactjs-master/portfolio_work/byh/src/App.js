import React, {Component} from 'react';
import Menu from './components/menu/Menu';
import Home from './components/pages/home/Home';

import './App.css';
import './css/css/fontawesome-all-pro.css';

class App extends Component {
    render() {
        return (
            <div className="App">
                <Menu/>
                <main>
                    <Home/>
                </main>
            </div>
        );
    }
}

export default App;
