import React, {Component} from 'react';


class Marquee extends Component {
    render() {
        return (
            <section id="Marquee">
                <div className="top_warp" style={{margin: 'auto'}}>
                    <div className="icon_mar">
                        <i className="fas fa-volume-up"></i>
                        <div className="text_title">
                            <p>游戏公告 :</p>
                        </div>
                        <div style={{clear: 'both'}}></div>
                    </div>
                    <div className="marWarp">
                        <div className="marquee">
                            <span>亲爱的会员，十分抱歉，由于目前短信通道异常，不影响正常游戏和充值，但您将无法绑定手机号码。若您需要绑定电话后取款，待明日公告恢复后再重新绑定。</span>
                        </div>
                    </div>
                    <div style={{clear: 'both'}}></div>
                </div>
            </section>
        );
    }
}

export default Marquee;
