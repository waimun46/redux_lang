import React, {Component} from 'react';
import Logo from '../../../images/icon_logo.png';


class Register extends Component {

    render() {
        return (
            <div>
                <div id="Register">
                    <div className="popup-wrap2">
                        <div className="popup-box2">
                            <div className="box_title">
                                <div className="text_bg">
                                    <div className="box_img">
                                        <img src={Logo} alt="logo"/>
                                    </div>
                                    <h2>注册</h2>
                                </div>
                            </div>

                            <form>
                                <div className="input-container">
                                    <input className="input-field" type="text" placeholder="用户名" name="username"/>
                                </div>
                                <div className="input-container">
                                    <input className="input-field" type="number" placeholder="电话号码" name="number"/>
                                </div>
                                <div className="input-container" style={{marginBottom: '30px'}}>
                                    <input className="input-field" type="password" placeholder="密码" name="password"/>
                                </div>

                                <button type="submit" className="btn">注册</button>
                            </form>
                            <div style={{clear: 'both'}}></div>
                            <a className="close-btn popup-close" href="#">x</a>
                        </div>
                    </div>

                </div>
            </div>
        );
    }
}

export default Register;
