import React, {Component} from 'react';
import Logo from '../../../images/icon_logo.png';

class Login extends Component {
    render() {
        return (
            <div id="Login">
                <div className="popup-wrap">
                    <div className="popup-box">
                        <div className="box_title">
                            <div className="text_bg">
                            <div className="box_img">
                                <img src={Logo} alt="logo"/>
                            </div>
                            <h2>登录</h2>
                            </div>
                        </div>

                        <form>
                            <div className="input-container">
                                <i className="fa fa-user icon"></i>
                                <input className="input-field" type="text" placeholder="用户名" name="usrnm"/>
                            </div>
                            <div className="input-container">
                                <i className="fas fa-unlock-alt icon"></i>
                                <input className="input-field" type="password" placeholder="密码" name="password"/>
                            </div>
                            <label className="check_con">
                                记住我
                                <input type="checkbox"/>
                                    <span className="checkmark"></span>
                            </label>
                            <div className="forget_pass">
                                <span>忘记密码？</span>
                            </div>

                            <div style={{clear: 'both'}}></div>

                            <button type="submit" className="btn">登录</button>
                        </form>
                        <div style={{clear: 'both'}}></div>
                        <a className="close-btn popup-close" href="#">x</a>
                    </div>
                </div>
            </div>
        );
    }
}

export default Login;
