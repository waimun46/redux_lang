import React, {Component} from 'react';
import $ from 'jquery';
import Menu from './components/menu/Menu';
import Footer from './components/footer/Footer';

import './App.css';
import './css/css/fontawesome-all.css';
import './css/ionicons.min.css';

class App extends Component {

    componentDidMount(){
        $('body').append('<div id="backToTop" class="btn btn-lg"><span class="far fa-angle-up"></span></div>');
        $(window).scroll(function () {
            if ($(this).scrollTop() <= 50) {
                $('#backToTop').fadeOut();
            } else {
                $('#backToTop').fadeIn();
            }
        });
        $('#backToTop').click(function(){
            $("html, body").animate({ scrollTop: 0 }, 600);
            return false;
        });
    }

    render() {
        return (
            <div className="App">
                <Menu/>
                <main>
                    {this.props.children}
                </main>
                <div className="clearfix"></div>
                <Footer/>
            </div>
        );
    }
}

export default App;
