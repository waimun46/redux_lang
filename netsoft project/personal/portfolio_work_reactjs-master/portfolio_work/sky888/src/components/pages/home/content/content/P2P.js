import React, {Component} from 'react';
import img05 from '../../../../../images/slot/5.png';
import img06 from '../../../../../images/slot/6.png';
import img07 from '../../../../../images/slot/7.png';
import img08 from '../../../../../images/slot/8.png';

class P2P extends Component {
    render() {
        return (
            <div className="P2P">
                <div className="slot_warp">
                    <img src={img05} alt="img01"/>
                </div>
                <div className="slot_warp">
                    <img src={img06} alt="img01"/>
                </div>
                <div className="slot_warp">
                    <img src={img07} alt="img01"/>
                </div>
                <div className="slot_warp">
                    <img src={img08} alt="img01"/>
                </div>
                <div className="clearfix"></div>
            </div>
        );
    }
}

export default P2P;
