import React, {Component} from 'react';
import img01 from '../../../../images/slot/s1.png';
import img02 from '../../../../images/slot/s2.png';
import img03 from '../../../../images/slot/s3.png';
import img04 from '../../../../images/slot/s4.png';

class SlotIntro extends Component {
    render() {
        return (
            <section id="SlotIntro">
                <article>
                    <header>
                        <h1>SLOTS INTRODUCTION</h1>
                        <hr/>
                    </header>

                    <div className="slotintro_warp">
                        <ul>
                            <li>
                                <div className="mb_pd2">
                                    <img src={img01} alt="img01"/>
                                    <a>Slot Games</a>
                                    <p>SKY888 Slot Games Download ARCADA </p>
                                </div>

                            </li>
                            <li className="mb_pd">
                                <div className="mb_pd2">
                                    <img src={img02} alt="img01"/>
                                    <a>Slot Games</a>
                                    <p>SKY888 Slot Games Download CASINO </p>
                                </div>
                            </li>
                            <li className="mb_pd">
                                <div className="mb_pd2">
                                    <img src={img03} alt="img01"/>
                                    <a>Slot Games</a>
                                    <p>SKY888 Slot Games Download RUNLIGHT </p>
                                </div>
                            </li>
                            <li className="mb_pd">
                                <div className="mb_pd2">
                                    <img src={img04} alt="img01"/>
                                    <a>Slot Games</a>
                                    <p>SKY888 Slot Games Download SLOT </p>
                                </div>
                            </li>
                            <div className="clearfix"></div>
                        </ul>
                    </div>

                </article>
            </section>
        );
    }
}

export default SlotIntro;
