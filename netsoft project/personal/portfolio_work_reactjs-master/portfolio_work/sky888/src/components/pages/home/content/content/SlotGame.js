import React, {Component} from 'react';
import img01 from '../../../../../images/slot/1.png';
import img02 from '../../../../../images/slot/2.png';
import img03 from '../../../../../images/slot/3.png';
import img04 from '../../../../../images/slot/4.png';
import img05 from '../../../../../images/slot/5.png';
import img06 from '../../../../../images/slot/6.png';
import img07 from '../../../../../images/slot/7.png';
import img08 from '../../../../../images/slot/8.png';

class SlotGame extends Component {
    render() {
        return (
            <div className="SlotGame">
                 <div className="slot_warp">
                     <img src={img01} alt="img01"/>
                 </div>
                <div className="slot_warp">
                    <img src={img02} alt="img01"/>
                </div>
                <div className="slot_warp">
                    <img src={img03} alt="img01"/>
                </div>
                <div className="slot_warp">
                    <img src={img04} alt="img01"/>
                </div>
                <div className="slot_warp">
                    <img src={img05} alt="img01"/>
                </div>
                <div className="slot_warp">
                    <img src={img06} alt="img01"/>
                </div>
                <div className="slot_warp">
                    <img src={img07} alt="img01"/>
                </div>
                <div className="slot_warp">
                    <img src={img08} alt="img01"/>
                </div>
                <div className="clearfix"></div>
            </div>
        );
    }
}

export default SlotGame;
