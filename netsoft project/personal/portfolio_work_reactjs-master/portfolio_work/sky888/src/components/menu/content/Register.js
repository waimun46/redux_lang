import React, {Component} from 'react';


class Register extends Component {
    render() {
        return (
            <div className="Register">
                <div className="modal-wrapper">
                    <div className="modal">
                        <div className="head">
                            <h3>REGISTER</h3>
                            <a className="btn-close trigger" href="#">
                                <i className="fa fa-times" aria-hidden="true"></i>
                            </a>
                            <div className="clearfix"></div>
                        </div>
                        <div className="content">
                            <div className="good-job">
                                <i className="fa fa-thumbs-o-up" aria-hidden="true"></i>
                                <form>
                                    <div className="input-container">
                                        <i className="fa fa-user icon"></i>
                                        <input className="input-field" type="text" placeholder="Username" name="usrnm"/>
                                    </div>

                                    <div className="input-container">
                                        <i className="fa fa-envelope icon"></i>
                                        <input className="input-field" type="text" placeholder="Email" name="email"/>
                                    </div>
                                    <div className="input-container">
                                        <i className="fas fa-phone icon"></i>
                                        <input className="input-field" type="text" placeholder="Phone" name="email"/>
                                    </div>

                                    <div className="input-container">
                                        <i className="fa fa-key icon"></i>
                                        <input className="input-field" type="password" placeholder="Password"
                                               name="psw"/>
                                    </div>

                                    <button type="submit" className="btn">Register</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Register;
