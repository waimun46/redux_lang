import React, {Component} from 'react';
import $ from 'jquery';
import Register from './content/Register';
import Login from './content/Login';

import logo from '../../images/logo2.png';
import logo01 from '../../images/ag.png';
import logo02 from '../../images/t1.png';
import logo03 from '../../images/t2.png';
import logo04 from '../../images/t3.png';
import logo05 from '../../images/t4.png';
import logo06 from '../../images/t5.png';
import logo07 from '../../images/t6.png';
import logo08 from '../../images/t7.png';
import logo09 from '../../images/t8.png';
import logo10 from '../../images/t9.png';

class Menu extends Component {

    componentDidMount() {
        $(document).ready(function () {
            $('.trigger').on('click', function () {
                $('.modal-wrapper').toggleClass('open');
                $('.page-wrapper').toggleClass('blur-it');
                return false;
            });

            $('.trigger2').on('click', function () {
                $('.modal-wrapper2').toggleClass('open');
                $('.page-wrapper').toggleClass('blur-it');
                return false;
            });
        });

        window.onscroll = function () {
            myFunction()
        };

        var header = document.getElementById("cbp-hsmenu-wrapper");
        var sticky = header.offsetTop;

        function myFunction() {
            if (window.pageYOffset > sticky) {
                header.classList.add("sticky");
            } else {
                header.classList.remove("sticky");
            }
        }
    }


    render() {
        return (
            <section id="Menu">
                <div className="container">
                    <header className="clearfix">
                        <div className="logo_sty">
                            <img src={logo} id="imgid" alt="logo"/>
                        </div>
                        <nav>

                            <a className="btn trigger2" href="#">
                                Login
                            </a>
                            <a className="btn trigger" href="#">
                                Free Register
                            </a>
                            {/*
                            <a href="#" data-info="Download">
                                <i className="fas fa-mobile-android-alt"></i>
                                <span>Download</span>
                            </a>
                            <a href="#" data-info="Promotion">
                                <i className="fas fa-percent"></i>
                                <span>Promotion</span>
                            </a>
                                  */}
                            {/*
                            <form className="topform">
                                <div className="form_sty">
                                    <div className="input-container">
                                        <i className="fa fa-user icon"></i>
                                        <input className="input-field" type="text" placeholder="Username" name="usrnm"/>
                                    </div>

                                    <div className="input-container">
                                        <i className="fa fa-envelope icon"></i>
                                        <input className="input-field" type="text" placeholder="Email" name="email"/>
                                    </div>
                                    <div className="clearfix"></div>
                                </div>
                                <div className=" btn_con">
                                    <button type="submit" className="btn">Register</button>
                                    <button type="submit" className="btn">Register</button>
                                    <div className="clearfix"></div>
                                </div>
                                <div className="clearfix"></div>

                            </form>
                             */}
                        </nav>
                    </header>

                    <nav className="cbp-hsmenu-wrapper" id="cbp-hsmenu-wrapper">
                        <div className="menu-container">
                            <div className="menu">
                                <ul className="line_ul">
                                    <li>
                                        <a href="#" className="menu_i">
                                            <i className="fas fa-gamepad fa_left"></i>
                                            Slots
                                            <i className="far fa-angle-down"></i>
                                        </a>

                                        <ul className="inner_logo">
                                            <li className="li_out">
                                                <ul>
                                                    <li className="btn trigger2">
                                                        <a href="#">
                                                            <img src={logo01} id="imgid" alt="logo"/>
                                                            <span>AG</span>
                                                        </a>
                                                    </li>
                                                    <li className="btn trigger2">
                                                        <a href="#">
                                                            <img src={logo02} id="imgid" alt="logo"/>
                                                            <span>ICASION</span>
                                                        </a>
                                                    </li>
                                                    <li className="btn trigger2">
                                                        <a href="#">
                                                            <img src={logo03} id="imgid" alt="logo"/>
                                                            <span>MG</span>
                                                        </a>
                                                    </li>
                                                    <li className="btn trigger2">
                                                        <a href="#">
                                                            <img src={logo04} id="imgid" alt="logo"/>
                                                            <span>PP</span>
                                                        </a>
                                                    </li>
                                                    <li className="btn trigger2">
                                                        <a href="#">
                                                            <img src={logo05} id="imgid" alt="logo"/>
                                                            <span>SG</span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </li>

                                    <li>
                                        <a href="#" className="menu_i">
                                            <i className="fas fa-dice fa_left"></i>

                                            Casino
                                            <i className="far fa-angle-down"></i>
                                        </a>

                                        <ul className="inner_logo">
                                            <li className="li_out">
                                                <ul>
                                                    <li className="btn trigger2">
                                                        <a href="#">
                                                            <img src={logo03} id="imgid" alt="logo"/>
                                                            <span>MG</span>
                                                        </a>
                                                    </li>
                                                    <li className="btn trigger2">
                                                        <a href="#">
                                                            <img src={logo06} id="imgid" alt="logo"/>
                                                            <span>ICASINO+</span>
                                                        </a>
                                                    </li>
                                                    <li className="btn trigger2">
                                                        <a href="#">
                                                            <img src={logo07} id="imgid" alt="logo"/>
                                                            <span>BBIN</span>
                                                        </a>
                                                    </li>
                                                    <li className="btn trigger2">
                                                        <a href="#">
                                                            <img src={logo08} id="imgid" alt="logo"/>
                                                            <span>IPT</span>
                                                        </a>
                                                    </li>

                                                </ul>
                                            </li>
                                        </ul>
                                    </li>


                                    <li>
                                        <a href="#" className="menu_i">
                                            <i className="fas fa-futbol fa_left"></i>
                                            Sport
                                            <i className="far fa-angle-down"></i>
                                        </a>

                                        <ul className="inner_logo">
                                            <li className="li_out">
                                                <ul>
                                                    <li className="btn trigger2">
                                                        <a href="#">
                                                            <img src={logo08} id="imgid" alt="logo"/>
                                                            <span>IPT</span>
                                                        </a>
                                                    </li>
                                                    <li className="btn trigger2">
                                                        <a href="#">
                                                            <img src={logo01} id="imgid" alt="logo"/>
                                                            <span>AG</span>
                                                        </a>
                                                    </li>
                                                    <li className="btn trigger2">
                                                        <a href="#">
                                                            <img src={logo09} id="imgid" alt="logo"/>
                                                            <span>IBIT</span>
                                                        </a>
                                                    </li>
                                                    <li className="btn trigger2">
                                                        <a href="#">
                                                            <img src={logo04} id="imgid" alt="logo"/>
                                                            <span>PP</span>
                                                        </a>
                                                    </li>
                                                    <li className="btn trigger2">
                                                        <a href="#">
                                                            <img src={logo06} id="imgid" alt="logo"/>
                                                            <span>ICASINO+</span>
                                                        </a>
                                                    </li>
                                                    <li className="btn trigger2">
                                                        <a href="#">
                                                            <img src={logo03} id="imgid" alt="logo"/>
                                                            <span>MG</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="#">
                                                            <img src={logo02} id="imgid" alt="logo"/>
                                                            <span>ICASINO</span>
                                                        </a>
                                                    </li>
                                                    <li className="btn trigger2">
                                                        <a href="#">
                                                            <img src={logo07} id="imgid" alt="logo"/>
                                                            <span>BBIN</span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </nav>
                </div>
                <Login/>
                <Register/>
            </section>
        );
    }
}

export default Menu;
