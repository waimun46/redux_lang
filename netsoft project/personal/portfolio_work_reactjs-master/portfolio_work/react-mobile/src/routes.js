import React, {Component} from 'react';
import {Route, Router, browserHistory} from 'react-router';
import LoginPage from './components/pages/login/LoginPage';
import SingupPage from './components/pages/signup/SingupPage';
import HomePage from './components/pages/home/HomePage';
import VideoPage from './components/pages/video/VideoPage';
import VideoPage01 from './components/pages/video01/VideoPage01';
import Science from './components/pages/movie/contentView/Science';
import Horror from './components/pages/movie/contentView/Horror';
import Action from './components/pages/movie/contentView/Action';
import Comedy from './components/pages/movie/contentView/Comedy';
import HK from './components/pages/movie/contentView/HK';
import TVB from './components/pages/tv/contentView/TVB';
import Chinese from './components/pages/tv/contentView/Chinese';
import Korean from './components/pages/tv/contentView/Korean';
import Western from './components/pages/tv/contentView/Western';
import RealityView from './components/pages/show/contentView/RealityView';
import MusicalView from './components/pages/show/contentView/MusicalView';
import AwardView from './components/pages/show/contentView/AwardView';
import ScienceVideo from './components/pages/movie/movieVideo/science/ScienceVideo';
import HorrorVideo from './components/pages/movie/movieVideo/horror/HorrorVideo';
import ActionVideo from './components/pages/movie/movieVideo/action/ActionVideo';
import ComedyVideo from './components/pages/movie/movieVideo/comedy/ComedyVideo';
import HKVideo from './components/pages/movie/movieVideo/hk/HKVideo';
import ChineseVideo from './components/pages/tv/tvVideo/chinese/ChineseVideo';
import KoreanVideo from './components/pages/tv/tvVideo/korean/KoreanVideo';
import WesternVideo from './components/pages/tv/tvVideo/western/WesternVideo';
import RealityVideo from './components/pages/show/showVideo/reality/RealityVideo';
import MusicVideo from './components/pages/show/showVideo/musical/MusicVideo';
import AwardVideo from './components/pages/show/showVideo/award/AwardVideo';
import Profile from './components/pages/profile/Profile';

class Routes extends Component {
    render() {
        return (
            <Router onUpdate={() => window.scrollTo(0, 0)} history={browserHistory}>

                    <Route exact path="/" component={LoginPage}/>
                    <Route path="/signup" component={SingupPage}/>
                    <Route path="/home" component={HomePage}/>
                    <Route path="/video" component={VideoPage}/>
                    <Route path="/video01" component={VideoPage01}/>
                    <Route path="/science" component={Science}/>
                    <Route path="/horror" component={Horror}/>
                    <Route path="/action" component={Action}/>
                    <Route path="/comedy" component={Comedy}/>
                    <Route path="/hk" component={HK}/>
                    <Route path="/tvb" component={TVB}/>
                    <Route path="/chinese" component={Chinese}/>
                    <Route path="/korean" component={Korean}/>
                    <Route path="/western" component={Western}/>
                    <Route path="/realityview" component={RealityView}/>
                    <Route path="/musicalview" component={MusicalView}/>
                    <Route path="/awardview" component={AwardView}/>
                    <Route path="/sciencevideo" component={ScienceVideo}/>
                    <Route path="/horrorvideo" component={HorrorVideo}/>
                    <Route path="/actionvideo" component={ActionVideo}/>
                    <Route path="/comedyvideo" component={ComedyVideo}/>
                    <Route path="/hkvideo" component={HKVideo}/>
                    <Route path="/chinesevideo" component={ChineseVideo}/>
                    <Route path="/koreanvideo" component={KoreanVideo}/>
                    <Route path="/westernvideo" component={WesternVideo}/>
                    <Route path="/realityvideo" component={RealityVideo}/>
                    <Route path="/musicvideo" component={MusicVideo}/>
                    <Route path="/awardvideo" component={AwardVideo}/>
                    <Route path="/profile" component={Profile}/>

            </Router>
        );
    }
}

export default Routes;
