import React, {Component} from 'react';
import $ from "jquery";
import Splash from './components/pages/Splash';
import Routes from './routes'
import './App.css';
import './css/font-fa/css/fontawesome-all-pro.css';
import  './css/font-awesome-animation.min.css';
import './css/bootstrap.min.css';

class Screen extends Component {
    constructor(props) {
        super(props);
        this.state = { currentScreen: 'Splash' };
        setTimeout(()=>{
            this.setState({ currentScreen: 'LoginPage' })
        }, 3000)
    }
    render() {
        const { currentScreen } = this.state;
        let mainScreen = currentScreen === 'Splash' ? <Splash /> : <Routes />;
        return (
            mainScreen
        );
    }
}

class App extends Component {

    componentDidMount(){
        $('body').append('<div id="backToTop" class="btn btn-lg"><span class="glyphicon glyphicon-chevron-up"></span></div>');
        $(window).scroll(function () {
            if ($(this).scrollTop() <= 50) {
                $('#backToTop').fadeOut();
            } else {
                $('#backToTop').fadeIn();
            }
        });
        $('#backToTop').click(function(){
            $("html, body").animate({ scrollTop: 0 }, 600);
            return false;
        });





    }

    render() {

        return (
            <div className="App">
                <Screen/>
            </div>
        );
    }
}

export default App;
