import React, {Component} from 'react';
import {Link} from 'react-router';
import r01 from '../../../../images/r01.jpg';
import r02 from '../../../../images/r02.jpg';
import r03 from '../../../../images/r03.jpg';
import r04 from '../../../../images/r04.jpg';

class TopRate extends Component {
    render() {
        return (
            <article className="TopRate">
                <div className="top_01">
                    <header>
                        <h4>Top Rated</h4>
                    </header>
                    <Link to="video">
                        <div className="card text-center">
                            <img className="card-img-top" src={r01} alt="r01"/>
                            <div className="card-body" id="card">
                                <h5 className="card-title">Title</h5>
                            </div>
                        </div>
                    </Link>
                    <Link to="video">
                        <div className="card text-center">
                            <img className="card-img-top" src={r02} alt="r02"/>
                            <div className="card-body" id="card">
                                <h5 className="card-title">Title</h5>
                            </div>
                        </div>
                    </Link>
                    <Link to="video">
                        <div className="card text-center">
                            <img className="card-img-top" src={r03} alt="r03"/>
                            <div className="card-body" id="card">
                                <h5 className="card-title">Title</h5>
                            </div>
                        </div>
                    </Link>
                    <Link to="video">
                        <div className="card text-center">
                            <img className="card-img-top" src={r04} alt="r04"/>
                            <div className="card-body" id="card">
                                <h5 className="card-title">Title</h5>
                            </div>
                        </div>
                    </Link>
                    <div className="clearfix"></div>
                </div>
            </article>
        );
    }
}

export default TopRate;
