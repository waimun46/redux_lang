import React, {Component} from 'react';
import { Button } from 'antd-mobile';
import Menu from '../menu/Menu';

function openNav() {
    document.getElementById("myfullscreen").style.height = "100%";
}

function closeNav() {
    document.getElementById("myfullscreen").style.height = "0%";
}
function openNav2() {
    document.getElementById("myfullscreen2").style.height = "100%";
}

function closeNav2() {
    document.getElementById("myfullscreen2").style.height = "0%";
}
class Profile extends Component {

    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',

        };
        this.onChange = this.onChange.bind(this);

    }

    onChange(e) {
        this.setState({[e.target.name]: e.target.value});
    }


    render() {
        return (
            <div>
                <main>
                    <section id="Profile">
                        <article>
                            <header>
                                <div className="top">
                                    <h4>Account</h4>
                                    <div className="icon_user">
                                        <i className="fas fa-user"></i>
                                    </div>
                                    <h5>mary</h5>
                                </div>
                                <div className="bottom">
                                    <div  className="grid_sty" style={{borderRight: 'none', borderLeft: 'none'}} onClick={openNav}>
                                        <i className="far fa-clock"></i>
                                        <div className="sp_sty">
                                            <span>History</span>
                                        </div>
                                    </div>

                                    <div id="myfullscreen" className="overlay">
                                        <div className="closebtn" onClick={closeNav}>
                                            <i className="far fa-long-arrow-left"></i>
                                        </div>
                                        <div className="overlay-content h4_sty">
                                            <header>
                                                <h4>History</h4>
                                            </header>
                                            <div className="his_i">
                                                <i className="fas fa-copy"></i>
                                                <h5>No histories</h5>
                                            </div>
                                        </div>
                                    </div>


                                    <div className="grid_sty" style={{borderRight: 'none'}} onClick={openNav2}>
                                        <i className="fas fa-share-alt"></i>
                                        <div className="sp_sty">
                                            <span>Share</span>
                                        </div>
                                    </div>
                                    <div id="myfullscreen2" className="overlay">
                                        <div className="closebtn" onClick={closeNav2}>
                                            <i className="far fa-long-arrow-left"></i>
                                        </div>
                                        <div className="overlay-content h4_sty2">
                                            <header>
                                                <h4>Share</h4>
                                            </header>
                                            <div className="his_i2">
                                                <i className="fas fa-share-alt-square"></i>
                                                <h5>No share histories</h5>
                                            </div>
                                        </div>
                                    </div>




                                    <div className="clearfix"></div>
                                </div>
                            </header>


                            <Button  data-toggle="modal" data-target="#myModal">
                                <div className="btn_w">
                                    <i className="far fa-cog" style={{paddingRight: '10px'}}></i>
                                    Settings
                                </div>
                                <i className="fal fa-angle-right float_i"></i>
                                <div className="clearfix"></div>
                            </Button>


                            <div className="modal fade" id="myModal" role="dialog">
                                <div className="modal-dialog modal-lg">
                                    <div className="modal-content">
                                        <div className="modal-header">
                                            <button type="button" className="close" data-dismiss="modal">&times;</button>
                                            <h4 className="modal-title">Edit Account</h4>
                                        </div>
                                        <div className="modal-body">
                                            <div className="form-group">
                                                <label>New Username</label>
                                                <input
                                                    field="username"
                                                    onChange={this.onChange}
                                                    placeholder="Enter new username"
                                                    type="text"
                                                />
                                            </div>
                                            <div className="form-group">
                                                <label>New Password</label>
                                                <input
                                                    field="username"
                                                    onChange={this.onChange}
                                                    placeholder="Enter new password"
                                                    type="password"
                                                />
                                            </div>
                                        </div>
                                        <div className="modal-footer">
                                            <button type="button" className="btn btn-default" data-dismiss="modal">Add</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <Button  data-toggle="modal" data-target="#myModal2">
                                <div className="btn_w">
                                    <i className="far fa-comment-alt" style={{paddingRight: '10px'}}></i>
                                    Feedback
                                </div>
                                <i className="fal fa-angle-right float_i"></i>
                                <div className="clearfix"></div>
                            </Button>
                            <div className="modal fade " id="myModal2" role="dialog">
                                <div className="modal-dialog modal-lg">
                                    <div className="modal-content">
                                        <div className="modal-header">
                                            <button type="button" className="close" data-dismiss="modal">&times;</button>
                                            <h4 className="modal-title">Suggest Something</h4>
                                        </div>
                                        <div className="modal-body modal_sty">
                                            <div className="form-group ">
                                                <label>Email</label>
                                                <input
                                                    field="username"
                                                    onChange={this.onChange}
                                                    placeholder="Your email address"
                                                    type="text"
                                                />
                                            </div>
                                        </div>
                                        <div className="modal-body">
                                            <div className="form-group">
                                                <textarea className="text_sty" placeholder="Briefly explain what could improve"
                                                        ></textarea>
                                            </div>
                                        </div>
                                        <div className="modal-footer">
                                            <button type="button" className="btn btn-default" data-dismiss="modal">Send</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </section>
                </main>
                <Menu/>
            </div>
        );
    }
}

export default Profile;
