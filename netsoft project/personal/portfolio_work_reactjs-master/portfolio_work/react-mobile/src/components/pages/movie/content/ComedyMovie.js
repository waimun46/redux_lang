import React, {Component} from 'react';
import {Link} from 'react-router';
import c01 from '../../../../images/c01.jpg';
import c02 from '../../../../images/c02.jpg';
import c03 from '../../../../images/c03.jpg';
import c04 from '../../../../images/c04.jpg';

class ComedyMovie extends Component {
    render() {
        return (
            <div className="ComedyMovie">
                <div className="top_01">
                    <header>
                        <h4>Comedy Movie</h4>
                        <Link to="comedy">
                            <span className="span_home">view more<i className="fal fa-angle-double-right"></i></span>
                        </Link>
                        <div className="clearfix"></div>
                    </header>

                    <Link to="comedyvideo">
                        <div className="card text-center">
                            <img className="card-img-top" src={c01} alt="c01"/>
                            <div className="card-body" id="card">
                                <h5 className="card-title">Title</h5>
                            </div>
                        </div>
                    </Link>
                    <Link to="comedyvideo">
                        <div className="card text-center">
                            <img className="card-img-top" src={c02} alt="c02"/>
                            <div className="card-body" id="card">
                                <h5 className="card-title">Title</h5>
                            </div>
                        </div>
                    </Link>
                    <Link to="comedyvideo">
                        <div className="card text-center">
                            <img className="card-img-top" src={c03} alt="c03"/>
                            <div className="card-body" id="card">
                                <h5 className="card-title">Title</h5>
                            </div>
                        </div>
                    </Link>
                    <Link to="comedyvideo">
                        <div className="card text-center">
                            <img className="card-img-top" src={c04} alt="c04"/>
                            <div className="card-body" id="card">
                                <h5 className="card-title">Title</h5>
                            </div>
                        </div>
                    </Link>
                    <div className="clearfix"></div>
                </div>
            </div>
        );
    }
}

export default ComedyMovie;
