import React, {Component} from 'react';
import {Link} from 'react-router';
import {Badge} from 'antd-mobile';
import v01 from '../../../../images/v01.jpg';
import v02 from '../../../../images/v02.jpg';
import v03 from '../../../../images/v03.jpg';
import v04 from '../../../../images/v04.jpg';
import v05 from '../../../../images/v05.jpg';
import v06 from '../../../../images/v06.jpg';

class HomeShow extends Component {
    render() {
        return (
            <article className="HomeShow">
                <div className="top_01">
                    <header>
                        <h4>Variety show</h4>
                    </header>
                    <Link to="video">
                        <div className="card text-center">
                            <Badge text="EP 265" style={{
                                marginLeft: 12,
                                padding: '0 3px',
                                backgroundColor: '#e91e63',
                                borderRadius: 2
                            }}/>
                            <img className="card-img-top" src={v01} alt="v01"/>
                            <div className="card-body" id="card">
                                <h5 className="card-title">Title</h5>
                            </div>
                        </div>
                    </Link>
                    <Link to="video">
                        <div className="card text-center">
                            <Badge text="EP 133" style={{
                                marginLeft: 12,
                                padding: '0 3px',
                                backgroundColor: '#e91e63',
                                borderRadius: 2
                            }}/>
                            <img className="card-img-top" src={v02} alt="v02"/>
                            <div className="card-body" id="card">
                                <h5 className="card-title">Title</h5>
                            </div>
                        </div>
                    </Link>
                    <Link to="video">
                        <div className="card text-center">
                            <Badge text="EP 65" style={{
                                marginLeft: 12,
                                padding: '0 3px',
                                backgroundColor: '#e91e63',
                                borderRadius: 2
                            }}/>
                            <img className="card-img-top" src={v03} alt="v03"/>
                            <div className="card-body" id="card">
                                <h5 className="card-title">Title</h5>
                            </div>
                        </div>
                    </Link>
                    <Link to="video">
                        <div className="card text-center">
                            <Badge text="EP 12" style={{
                                marginLeft: 12,
                                padding: '0 3px',
                                backgroundColor: '#e91e63',
                                borderRadius: 2
                            }}/>
                            <img className="card-img-top" src={v04} alt="v04"/>
                            <div className="card-body" id="card">
                                <h5 className="card-title">Title</h5>
                            </div>
                        </div>
                    </Link>
                    <Link to="video">
                        <div className="card text-center">
                            <Badge text="EP 45" style={{
                                marginLeft: 12,
                                padding: '0 3px',
                                backgroundColor: '#e91e63',
                                borderRadius: 2
                            }}/>
                            <img className="card-img-top" src={v05} alt="v05"/>
                            <div className="card-body" id="card">
                                <h5 className="card-title">Title</h5>
                            </div>
                        </div>
                    </Link>
                    <Link to="video">
                        <div className="card text-center">
                            <Badge text="EP 104" style={{
                                marginLeft: 12,
                                padding: '0 3px',
                                backgroundColor: '#e91e63',
                                borderRadius: 2
                            }}/>
                            <img className="card-img-top" src={v06} alt="v06"/>
                            <div className="card-body" id="card">
                                <h5 className="card-title">Title</h5>
                            </div>
                        </div>
                    </Link>
                    <div className="clearfix"></div>
                </div>
            </article>
        );
    }
}

export default HomeShow;
