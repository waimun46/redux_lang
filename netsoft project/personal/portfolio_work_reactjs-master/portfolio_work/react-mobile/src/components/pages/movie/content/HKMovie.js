import React, {Component} from 'react';
import {Link} from 'react-router';
import hk01 from '../../../../images/hk01.jpg';
import hk02 from '../../../../images/hk02.jpg';
import hk03 from '../../../../images/hk03.jpg';
import hk04 from '../../../../images/hk04.jpg';

class HKMovie extends Component {
    render() {
        return (
            <div className="HKMovie">
                <div className="top_01">
                    <header>
                        <h4>HK Movie</h4>
                        <Link to="hk">
                            <span className="span_home">view more<i className="fal fa-angle-double-right"></i></span>
                        </Link>
                        <div className="clearfix"></div>
                    </header>

                    <Link to="hkvideo">
                        <div className="card text-center">
                            <img className="card-img-top" src={hk01} alt="hk01"/>
                            <div className="card-body" id="card">
                                <h5 className="card-title">Title</h5>
                            </div>
                        </div>
                    </Link>
                    <Link to="hkvideo">
                        <div className="card text-center">
                            <img className="card-img-top" src={hk02} alt="hk02"/>
                            <div className="card-body" id="card">
                                <h5 className="card-title">Title</h5>
                            </div>
                        </div>
                    </Link>
                    <Link to="hkvideo">
                        <div className="card text-center">
                            <img className="card-img-top" src={hk03} alt="hk03"/>
                            <div className="card-body" id="card">
                                <h5 className="card-title">Title</h5>
                            </div>
                        </div>
                    </Link>
                    <Link to="hkvideo">
                        <div className="card text-center">
                            <img className="card-img-top" src={hk04} alt="hk04"/>
                            <div className="card-body" id="card">
                                <h5 className="card-title">Title</h5>
                            </div>
                        </div>
                    </Link>
                    <div className="clearfix"></div>
                </div>
            </div>
        );
    }
}

export default HKMovie;
