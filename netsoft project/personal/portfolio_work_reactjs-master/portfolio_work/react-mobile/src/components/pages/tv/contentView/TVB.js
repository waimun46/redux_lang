import React, {Component} from 'react';
import TitleMenu from '../../menu/TitleMenu'
import {Link} from 'react-router';
import Menu from '../../menu/Menu';
import m02 from '../../../../images/tv01.jpg';

class TVB extends Component {
    render() {
        return (
            <section id="TVB">
                <article>
                    <TitleMenu title="TVB Channel"/>
                    <main>
                        <div className="top_01">
                            <Link to="video">
                                <div className="card text-center">
                                    <img className="card-img-top" src={m02} alt="m02"/>
                                    <div className="card-body" id="card">
                                        <h5 className="card-title">Title</h5>
                                    </div>
                                </div>
                            </Link>
                            <Link to="video">
                                <div className="card text-center">
                                    <img className="card-img-top" src={m02} alt="m02"/>
                                    <div className="card-body" id="card">
                                        <h5 className="card-title">Title</h5>
                                    </div>
                                </div>
                            </Link>
                            <Link to="video">
                                <div className="card text-center">
                                    <img className="card-img-top" src={m02} alt="m02v"/>
                                    <div className="card-body" id="card">
                                        <h5 className="card-title">Title</h5>
                                    </div>
                                </div>
                            </Link>
                            <Link to="video">
                                <div className="card text-center">
                                    <img className="card-img-top" src={m02} alt="m02"/>
                                    <div className="card-body" id="card">
                                        <h5 className="card-title">Title</h5>
                                    </div>
                                </div>
                            </Link>
                            <Link to="video">
                                <div className="card text-center">
                                    <img className="card-img-top" src={m02} alt="m02"/>
                                    <div className="card-body" id="card">
                                        <h5 className="card-title">Title</h5>
                                    </div>
                                </div>
                            </Link>
                            <Link to="video">
                                <div className="card text-center">
                                    <img className="card-img-top" src={m02} alt="m02"/>
                                    <div className="card-body" id="card">
                                        <h5 className="card-title">Title</h5>
                                    </div>
                                </div>
                            </Link>
                            <Link to="video">
                                <div className="card text-center">
                                    <img className="card-img-top" src={m02} alt="m02"/>
                                    <div className="card-body" id="card">
                                        <h5 className="card-title">Title</h5>
                                    </div>
                                </div>
                            </Link>
                            <Link to="video">
                                <div className="card text-center">
                                    <img className="card-img-top" src={m02} alt="m02"/>
                                    <div className="card-body" id="card">
                                        <h5 className="card-title">Title</h5>
                                    </div>
                                </div>
                            </Link>
                            <Link to="video">
                                <div className="card text-center">
                                    <img className="card-img-top" src={m02} alt="m02"/>
                                    <div className="card-body" id="card">
                                        <h5 className="card-title">Title</h5>
                                    </div>
                                </div>
                            </Link>
                            <Link to="video">
                                <div className="card text-center">
                                    <img className="card-img-top" src={m02} alt="m02"/>
                                    <div className="card-body" id="card">
                                        <h5 className="card-title">Title</h5>
                                    </div>
                                </div>
                            </Link>
                            <Link to="video">
                                <div className="card text-center">
                                    <img className="card-img-top" src={m02} alt="m02"/>
                                    <div className="card-body" id="card">
                                        <h5 className="card-title">Title</h5>
                                    </div>
                                </div>
                            </Link>
                            <Link to="video">
                                <div className="card text-center">
                                    <img className="card-img-top" src={m02} alt="m02"/>
                                    <div className="card-body" id="card">
                                        <h5 className="card-title">Title</h5>
                                    </div>
                                </div>
                            </Link>
                            <Link to="video">
                                <div className="card text-center">
                                    <img className="card-img-top" src={m02} alt="m02"/>
                                    <div className="card-body" id="card">
                                        <h5 className="card-title">Title</h5>
                                    </div>
                                </div>
                            </Link>
                            <Link to="video">
                                <div className="card text-center">
                                    <img className="card-img-top" src={m02} alt="m02"/>
                                    <div className="card-body" id="card">
                                        <h5 className="card-title">Title</h5>
                                    </div>
                                </div>
                            </Link>
                            <Link to="video">
                                <div className="card text-center">
                                    <img className="card-img-top" src={m02} alt="m02"/>
                                    <div className="card-body" id="card">
                                        <h5 className="card-title">Title</h5>
                                    </div>
                                </div>
                            </Link>
                            <div className="clearfix"></div>
                        </div>


                    </main>

                </article>
                <Menu/>
            </section>


        );
    }
}

export default TVB;
