import React, {Component} from 'react';
import {Link} from 'react-router';
import au01 from '../../../../images/au01.jpg';
import au02 from '../../../../images/au02.jpg';
import au03 from '../../../../images/au03.jpg';
import au04 from '../../../../images/au04.jpg';

class Award extends Component {
    render() {
        return (
            <div className="Award">
                <div className="top_01">
                    <header>
                        <h4>Award Ceremony</h4>
                        <Link to="awardview">
                            <span className="span_home">view more<i className="fal fa-angle-double-right"></i></span>
                        </Link>
                        <div className="clearfix"></div>
                    </header>

                    <Link to="awardvideo">
                        <div className="card text-center">
                            <img className="card-img-top" src={au01} alt="au01"/>
                            <div className="card-body" id="card">
                                <h5 className="card-title">Title</h5>
                            </div>
                        </div>
                    </Link>
                    <Link to="awardvideo">
                        <div className="card text-center">
                            <img className="card-img-top" src={au02} alt="au02"/>
                            <div className="card-body" id="card">
                                <h5 className="card-title">Title</h5>
                            </div>
                        </div>
                    </Link>
                    <Link to="awardvideo">
                        <div className="card text-center">
                            <img className="card-img-top" src={au03} alt="au03"/>
                            <div className="card-body" id="card">
                                <h5 className="card-title">Title</h5>
                            </div>
                        </div>
                    </Link>
                    <Link to="awardvideo">
                        <div className="card text-center">
                            <img className="card-img-top" src={au04} alt="au04"/>
                            <div className="card-body" id="card">
                                <h5 className="card-title">Title</h5>
                            </div>
                        </div>
                    </Link>
                    <div className="clearfix"></div>
                </div>
            </div>
        );
    }
}

export default Award;
