import React, {Component} from 'react';
import {Badge} from 'antd-mobile';
import {Link} from 'react-router';
import kd01 from '../../../../images/kd01.jpg';
import kd02 from '../../../../images/kd02.jpg';
import kd03 from '../../../../images/kd03.jpg';
import kd04 from '../../../../images/kd04.jpg';


class KoreanTV extends Component {
    render() {
        return (
            <div className="KoreanTV">
                <div className="top_01">
                    <header>
                        <h4>Korean</h4>
                        <Link to="korean">
                            <span className="span_home">view more<i className="fal fa-angle-double-right"></i></span>
                        </Link>
                        <div className="clearfix"></div>
                    </header>

                    <Link to="koreanvideo">
                        <div className="card text-center">
                            <Badge text="EP 26"
                                   style={{
                                       marginLeft: 12,
                                       padding: '0 3px',
                                       backgroundColor: '#e91e63',
                                       borderRadius: 2
                                   }}/>
                            <img className="card-img-top" src={kd01} alt="kd01"/>
                            <div className="card-body" id="card">
                                <h5 className="card-title">Title</h5>
                            </div>
                        </div>
                    </Link>
                    <Link to="koreanvideo">
                        <div className="card text-center">
                            <Badge text="EP 20"
                                   style={{
                                       marginLeft: 12,
                                       padding: '0 3px',
                                       backgroundColor: '#e91e63',
                                       borderRadius: 2
                                   }}/>
                            <img className="card-img-top" src={kd02} alt="kd02"/>
                            <div className="card-body" id="card">
                                <h5 className="card-title">Title</h5>
                            </div>
                        </div>
                    </Link>
                    <Link to="koreanvideo">
                        <div className="card text-center">
                            <Badge text="EP 50"
                                   style={{
                                       marginLeft: 12,
                                       padding: '0 3px',
                                       backgroundColor: '#e91e63',
                                       borderRadius: 2
                                   }}/>
                            <img className="card-img-top" src={kd03} alt="kd03"/>
                            <div className="card-body" id="card">
                                <h5 className="card-title">Title</h5>
                            </div>
                        </div>
                    </Link>
                    <Link to="koreanvideo">
                        <div className="card text-center">
                            <Badge text="EP 22"
                                   style={{
                                       marginLeft: 12,
                                       padding: '0 3px',
                                       backgroundColor: '#e91e63',
                                       borderRadius: 2
                                   }}/>
                            <img className="card-img-top" src={kd04} alt="kd04"/>
                            <div className="card-body" id="card">
                                <h5 className="card-title">Title</h5>
                            </div>
                        </div>
                    </Link>
                    <div className="clearfix"></div>
                </div>
            </div>
        );
    }
}

export default KoreanTV;
