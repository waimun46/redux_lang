import React, {Component} from 'react';
import {Link} from 'react-router';
import m01 from '../../../../images/m01.jpg';
import m02 from '../../../../images/m02.jpg';
import m03 from '../../../../images/m03.jpg';
import m04 from '../../../../images/m04.jpg';
import m05 from '../../../../images/m05.jpg';
import m06 from '../../../../images/m06.jpg';

class HomeMovie extends Component {
    render() {
        return (
            <article className="HomeMovie">
                <div className="top_01">
                    <header>
                        <h4>Movie</h4>
                    </header>
                    <Link to="video">
                        <div className="card text-center">
                            <img className="card-img-top" src={m01} alt="m01"/>
                            <div className="card-body" id="card">
                                <h5 className="card-title">Title</h5>
                            </div>
                        </div>
                    </Link>
                    <Link to="video">
                        <div className="card text-center">
                            <img className="card-img-top" src={m02} alt="m02"/>
                            <div className="card-body" id="card">
                                <h5 className="card-title">Title</h5>
                            </div>
                        </div>
                    </Link>
                    <Link to="video">
                        <div className="card text-center">
                            <img className="card-img-top" src={m03} alt="m03"/>
                            <div className="card-body" id="card">
                                <h5 className="card-title">Title</h5>
                            </div>
                        </div>
                    </Link>
                    <Link to="video">
                        <div className="card text-center">
                            <img className="card-img-top" src={m04} alt="m04"/>
                            <div className="card-body" id="card">
                                <h5 className="card-title">Title</h5>
                            </div>
                        </div>
                    </Link>
                    <Link to="video">
                        <div className="card text-center">
                            <img className="card-img-top" src={m05} alt="m05"/>
                            <div className="card-body" id="card">
                                <h5 className="card-title">Title</h5>
                            </div>
                        </div>
                    </Link>
                    <Link to="video">
                        <div className="card text-center">
                            <img className="card-img-top" src={m06} alt="m06"/>
                            <div className="card-body" id="card">
                                <h5 className="card-title">Title</h5>
                            </div>
                        </div>
                    </Link>
                    <div className="clearfix"></div>
                </div>
            </article>
        );
    }
}

export default HomeMovie;
