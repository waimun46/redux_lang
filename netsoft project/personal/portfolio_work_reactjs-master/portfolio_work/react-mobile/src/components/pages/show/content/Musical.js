import React, {Component} from 'react';
import {Badge} from 'antd-mobile';
import {Link} from 'react-router';
import mu02 from '../../../../images/mu02.jpg';
import mu01 from '../../../../images/mu01.jpg';
import mu03 from '../../../../images/mu03.jpg';
import mu04 from '../../../../images/mu04.jpg';

class Musical extends Component {
    render() {
        return (
            <div className="Musical">
                <div className="top_01">
                    <header>
                        <h4>Musical</h4>
                        <Link to="musicalview">
                            <span className="span_home">view more<i className="fal fa-angle-double-right"></i></span>
                        </Link>
                        <div className="clearfix"></div>
                    </header>

                    <Link to="musicvideo">
                        <div className="card text-center">
                            <Badge text="EP 256"
                                   style={{
                                       marginLeft: 12,
                                       padding: '0 3px',
                                       backgroundColor: '#e91e63',
                                       borderRadius: 2
                                   }}/>
                            <img className="card-img-top" src={mu02} alt="mu02"/>
                            <div className="card-body" id="card">
                                <h5 className="card-title">Title</h5>
                            </div>
                        </div>
                    </Link>
                    <Link to="musicvideo">
                        <div className="card text-center">
                            <Badge text="EP 56"
                                   style={{
                                       marginLeft: 12,
                                       padding: '0 3px',
                                       backgroundColor: '#e91e63',
                                       borderRadius: 2
                                   }}/>
                            <img className="card-img-top" src={mu01} alt="mu01"/>
                            <div className="card-body" id="card">
                                <h5 className="card-title">Title</h5>
                            </div>
                        </div>
                    </Link>
                    <Link to="musicvideo">
                        <div className="card text-center">
                            <Badge text="EP 355"
                                   style={{
                                       marginLeft: 12,
                                       padding: '0 3px',
                                       backgroundColor: '#e91e63',
                                       borderRadius: 2
                                   }}/>
                            <img className="card-img-top" src={mu03} alt="mu03"/>
                            <div className="card-body" id="card">
                                <h5 className="card-title">Title</h5>
                            </div>
                        </div>
                    </Link>
                    <Link to="musicvideo">
                        <div className="card text-center">
                            <Badge text="EP 233"
                                   style={{
                                       marginLeft: 12,
                                       padding: '0 3px',
                                       backgroundColor: '#e91e63',
                                       borderRadius: 2
                                   }}/>
                            <img className="card-img-top" src={mu04} alt="mu04"/>
                            <div className="card-body" id="card">
                                <h5 className="card-title">Title</h5>
                            </div>
                        </div>
                    </Link>
                    <div className="clearfix"></div>
                </div>
            </div>
        );
    }
}

export default Musical;
