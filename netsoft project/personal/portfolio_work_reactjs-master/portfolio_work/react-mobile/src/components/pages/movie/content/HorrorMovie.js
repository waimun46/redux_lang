import React, {Component} from 'react';
import {Link} from 'react-router';
import h01 from '../../../../images/h01.jpg';
import h02 from '../../../../images/h03.jpg';
import h03 from '../../../../images/h04.jpg';
import h04 from '../../../../images/h05.jpg';

class HorrorMovie extends Component {
    render() {
        return (
            <div className="HorrorMovie">
                <div className="top_01">
                    <header>
                        <h4>Horror Movie</h4>
                        <Link to="horror">
                            <span className="span_home">view more<i className="fal fa-angle-double-right"></i></span>
                        </Link>
                        <div className="clearfix"></div>
                    </header>

                    <Link to="horrorvideo">
                        <div className="card text-center">
                            <img className="card-img-top" src={h01} alt="h01"/>
                            <div className="card-body" id="card">
                                <h5 className="card-title">Title</h5>
                            </div>
                        </div>
                    </Link>
                    <Link to="horrorvideo">
                        <div className="card text-center">
                            <img className="card-img-top" src={h02} alt="h02"/>
                            <div className="card-body" id="card">
                                <h5 className="card-title">Title</h5>
                            </div>
                        </div>
                    </Link>
                    <Link to="horrorvideo">
                        <div className="card text-center">
                            <img className="card-img-top" src={h03} alt="h03"/>
                            <div className="card-body" id="card">
                                <h5 className="card-title">Title</h5>
                            </div>
                        </div>
                    </Link>
                    <Link to="horrorvideo">
                        <div className="card text-center">
                            <img className="card-img-top" src={h04} alt="h04"/>
                            <div className="card-body" id="card">
                                <h5 className="card-title">Title</h5>
                            </div>
                        </div>
                    </Link>
                    <div className="clearfix"></div>
                </div>
            </div>
        );
    }
}

export default HorrorMovie;
