import React, {Component} from 'react';
import {Tabs, WhiteSpace} from 'antd-mobile';
import {StickyContainer, Sticky} from 'react-sticky';
import Menu from '../menu/Menu';
import HomeContent from './HomeContent';
import MoviePage from '../movie/MoviePage'
import TVPage from '../tv/TVPage'
import ShowPage from '../show/ShowPage'


function renderTabBar(props) {
    return (<Sticky>
        {({style}) => <div style={{...style, zIndex: 1}}><Tabs.DefaultTabBar {...props} /></div>}
    </Sticky>);
}
const tabs = [
    {title: 'Home'},
    {title: 'Movie'},
    {title: 'TV-Series'},
    {title: 'Show'}
];


class HomePage extends Component {
    render() {
        return (
            <div>
                <main>
                    <section id="HomePage">
                        <WhiteSpace />
                        <StickyContainer>
                            <Tabs tabs={tabs}
                                  initalPage={'t2'}
                                  renderTabBar={renderTabBar}
                            >
                                <div style={{
                                    display: 'flex',
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                    backgroundColor: '#eee'
                                }}>
                                    <HomeContent/>
                                </div>
                                <div style={{
                                    display: 'flex',
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                    backgroundColor: '#eee'
                                }}>

                                    <MoviePage/>
                                </div>
                                <div style={{
                                    display: 'flex',
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                    backgroundColor: '#eee'
                                }}>
                                    <TVPage/>
                                </div>
                                <div style={{
                                    display: 'flex',
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                    backgroundColor: '#eee'
                                }}>
                                    <ShowPage/>
                                </div>
                            </Tabs>
                        </StickyContainer>
                        <WhiteSpace />
                    </section>
                </main>
                <Menu/>
            </div>
        );
    }
}

export default HomePage;
