import React, {Component} from 'react';

import ScienceMovie from './content/ScienceMovie'
import HorrorMovie from './content/HorrorMovie'
import ActionsMovie from './content/ActionsMovie'
import ComedyMovie from './content/ComedyMovie'
import HKMovie from './content/HKMovie'

class MoviePage extends Component {
    render() {
        return (
            <article id="MoviePage">
                <ScienceMovie/>
                <HorrorMovie/>
                <ActionsMovie/>
                <ComedyMovie/>
                <HKMovie/>
            </article>
        );
    }
}

export default MoviePage;
