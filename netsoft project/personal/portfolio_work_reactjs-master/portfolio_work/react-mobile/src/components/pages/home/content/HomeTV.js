import React, {Component} from 'react';
import {Badge} from 'antd-mobile';
import {Link} from 'react-router';
import t01 from '../../../../images/t01.jpg';
import t02 from '../../../../images/t02.jpg';
import t03 from '../../../../images/t03.jpg';
import t04 from '../../../../images/t04.jpg';
import t05 from '../../../../images/t05.jpg';
import t06 from '../../../../images/t06.jpg';

class HomeTV extends Component {
    render() {
        return (
            <article className="HomeTV">
                <div className="top_01">
                    <header>
                        <h4>TV-Series</h4>
                    </header>
                    <Link to="video">
                        <div className="card text-center">
                            <Badge text="EP 05" style={{
                                marginLeft: 12,
                                padding: '0 3px',
                                backgroundColor: '#e91e63',
                                borderRadius: 2
                            }}/>
                            <img className="card-img-top" src={t01} alt="t01"/>
                            <div className="card-body" id="card">
                                <h5 className="card-title">Title</h5>
                            </div>
                        </div>
                    </Link>
                    <Link to="video">
                        <div className="card text-center">
                            <Badge text="EP 16" style={{
                                marginLeft: 12,
                                padding: '0 3px',
                                backgroundColor: '#e91e63',
                                borderRadius: 2
                            }}/>
                            <img className="card-img-top" src={t02} alt="t02"/>
                            <div className="card-body" id="card">
                                <h5 className="card-title">Title</h5>
                            </div>
                        </div>
                    </Link>
                    <Link to="video">
                        <div className="card text-center">
                            <Badge text="EP 18" style={{
                                marginLeft: 12,
                                padding: '0 3px',
                                backgroundColor: '#e91e63',
                                borderRadius: 2
                            }}/>
                            <img className="card-img-top" src={t03} alt="t03"/>
                            <div className="card-body" id="card">
                                <h5 className="card-title">Title</h5>
                            </div>
                        </div>
                    </Link>
                    <Link to="video">
                        <div className="card text-center">
                            <Badge text="EP 12" style={{
                                marginLeft: 12,
                                padding: '0 3px',
                                backgroundColor: '#e91e63',
                                borderRadius: 2
                            }}/>
                            <img className="card-img-top" src={t04} alt="t04"/>
                            <div className="card-body" id="card">
                                <h5 className="card-title">Title</h5>
                            </div>
                        </div>
                    </Link>
                    <Link to="video">
                        <div className="card text-center">
                            <Badge text="EP 06" style={{
                                marginLeft: 12,
                                padding: '0 3px',
                                backgroundColor: '#e91e63',
                                borderRadius: 2
                            }}/>
                            <img className="card-img-top" src={t05} alt="t05"/>
                            <div className="card-body" id="card">
                                <h5 className="card-title">Title</h5>
                            </div>
                        </div>
                    </Link>
                    <Link to="video">
                        <div className="card text-center">
                            <Badge text="EP 45" style={{
                                marginLeft: 12,
                                padding: '0 3px',
                                backgroundColor: '#e91e63',
                                borderRadius: 2
                            }}/>
                            <img className="card-img-top" src={t06} alt="t06"/>
                            <div className="card-body" id="card">
                                <h5 className="card-title">Title</h5>
                            </div>
                        </div>
                    </Link>
                    <div className="clearfix"></div>
                </div>
            </article>
        );
    }
}

export default HomeTV;
