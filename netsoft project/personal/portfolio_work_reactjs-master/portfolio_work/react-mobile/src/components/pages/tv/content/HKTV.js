import React, {Component} from 'react';
import {Badge} from 'antd-mobile';
import {Link} from 'react-router';
import ts01 from '../../../../images/ts01.jpg';
import ts02 from '../../../../images/ts02.jpg';
import ts03 from '../../../../images/ts03.jpg';
import ts04 from '../../../../images/ts04.jpg';

class HKTV extends Component {
    render() {
        return (
            <div className="HKTV">
                <div className="top_01">
                    <header>
                        <h4>TVB Channel</h4>
                        <Link to="tvb">
                            <span className="span_home">view more<i className="fal fa-angle-double-right"></i></span>
                        </Link>
                        <div className="clearfix"></div>
                    </header>

                    <Link to="video">
                        <div className="card text-center">
                            <Badge text="EP 10"
                                   style={{
                                       marginLeft: 12,
                                       padding: '0 3px',
                                       backgroundColor: '#e91e63',
                                       borderRadius: 2
                                   }}/>
                            <img className="card-img-top" src={ts01} alt="ts01"/>
                            <div className="card-body" id="card">
                                <h5 className="card-title">Title</h5>
                            </div>
                        </div>
                    </Link>
                    <Link to="video">
                        <div className="card text-center">
                            <Badge text="EP 05"
                                   style={{
                                       marginLeft: 12,
                                       padding: '0 3px',
                                       backgroundColor: '#e91e63',
                                       borderRadius: 2
                                   }}/>
                            <img className="card-img-top" src={ts02} alt="ts02"/>
                            <div className="card-body" id="card">
                                <h5 className="card-title">Title</h5>
                            </div>
                        </div>
                    </Link>
                    <Link to="video01">
                        <div className="card text-center">
                            <Badge text="EP 23"
                                   style={{
                                       marginLeft: 12,
                                       padding: '0 3px',
                                       backgroundColor: '#e91e63',
                                       borderRadius: 2
                                   }}/>
                            <img className="card-img-top" src={ts03} alt="ts03"/>
                            <div className="card-body" id="card">
                                <h5 className="card-title">Title</h5>
                            </div>
                        </div>
                    </Link>
                    <Link to="video">
                        <div className="card text-center">
                            <Badge text="EP 20"
                                   style={{
                                       marginLeft: 12,
                                       padding: '0 3px',
                                       backgroundColor: '#e91e63',
                                       borderRadius: 2
                                   }}/>
                            <img className="card-img-top" src={ts04} alt="ts04"/>
                            <div className="card-body" id="card">
                                <h5 className="card-title">Title</h5>
                            </div>
                        </div>
                    </Link>
                    <div className="clearfix"></div>
                </div>
            </div>
        );
    }
}

export default HKTV;
