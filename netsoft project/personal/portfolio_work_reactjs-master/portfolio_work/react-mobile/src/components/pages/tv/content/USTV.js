import React, {Component} from 'react';
import {Badge} from 'antd-mobile';
import {Link} from 'react-router';
import img03 from '../../../../images/img03.jpg';
import am01 from '../../../../images/am01.jpg';
import am02 from '../../../../images/am02.jpg';
import am03 from '../../../../images/am03.jpg';

class USTV extends Component {
    render() {
        return (
            <div className="USTV">
                <div className="top_01">
                    <header>
                        <h4>Western</h4>
                        <Link to="western">
                            <span className="span_home">view more<i className="fal fa-angle-double-right"></i></span>
                        </Link>
                        <div className="clearfix"></div>
                    </header>

                    <Link to="video01">
                        <div className="card text-center">
                            <Badge text="EP 08"
                                   style={{
                                       marginLeft: 12,
                                       padding: '0 3px',
                                       backgroundColor: '#e91e63',
                                       borderRadius: 2
                                   }}/>
                            <img className="card-img-top" src={img03} alt="img03"/>
                            <div className="card-body" id="card">
                                <h5 className="card-title">Title</h5>
                            </div>
                        </div>
                    </Link>
                    <Link to="westernvideo">
                        <div className="card text-center">
                            <Badge text="EP 13"
                                   style={{
                                       marginLeft: 12,
                                       padding: '0 3px',
                                       backgroundColor: '#e91e63',
                                       borderRadius: 2
                                   }}/>
                            <img className="card-img-top" src={am01} alt="am01"/>
                            <div className="card-body" id="card">
                                <h5 className="card-title">Title</h5>
                            </div>
                        </div>
                    </Link>
                    <Link to="westernvideo">
                        <div className="card text-center">
                            <Badge text="EP 22"
                                   style={{
                                       marginLeft: 12,
                                       padding: '0 3px',
                                       backgroundColor: '#e91e63',
                                       borderRadius: 2
                                   }}/>
                            <img className="card-img-top" src={am02} alt="am02"/>
                            <div className="card-body" id="card">
                                <h5 className="card-title">Title</h5>
                            </div>
                        </div>
                    </Link>
                    <Link to="westernvideo">
                        <div className="card text-center">
                            <Badge text="EP 02"
                                   style={{
                                       marginLeft: 12,
                                       padding: '0 3px',
                                       backgroundColor: '#e91e63',
                                       borderRadius: 2
                                   }}/>
                            <img className="card-img-top" src={am03} alt="am03"/>
                            <div className="card-body" id="card">
                                <h5 className="card-title">Title</h5>
                            </div>
                        </div>
                    </Link>
                    <div className="clearfix"></div>
                </div>
            </div>
        );
    }
}

export default USTV;
