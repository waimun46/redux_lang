import React, {Component} from 'react';
import {Badge} from 'antd-mobile';
import {Link} from 'react-router';
import ch01 from '../../../../images/ch01.jpg';
import ch02 from '../../../../images/ch02.jpg';
import ch03 from '../../../../images/ch03.jpg';
import ch04 from '../../../../images/ch04.jpg';

class ChinaTV extends Component {
    render() {
        return (
            <div className="ChinaTV">
                <div className="top_01">
                    <header>
                        <h4>Chinese</h4>
                        <Link to="chinese">
                            <span className="span_home">view more<i className="fal fa-angle-double-right"></i></span>
                        </Link>
                        <div className="clearfix"></div>
                    </header>

                    <Link to="chinesevideo">
                        <div className="card text-center">
                            <Badge text="EP 36"
                                   style={{
                                       marginLeft: 12,
                                       padding: '0 3px',
                                       backgroundColor: '#e91e63',
                                       borderRadius: 2
                                   }}/>
                            <img className="card-img-top" src={ch01} alt="ch01"/>
                            <div className="card-body" id="card">
                                <h5 className="card-title">Title</h5>
                            </div>
                        </div>
                    </Link>
                    <Link to="chinesevideo">
                        <div className="card text-center">
                            <Badge text="EP 22"
                                   style={{
                                       marginLeft: 12,
                                       padding: '0 3px',
                                       backgroundColor: '#e91e63',
                                       borderRadius: 2
                                   }}/>
                            <img className="card-img-top" src={ch02} alt="ch02"/>
                            <div className="card-body" id="card">
                                <h5 className="card-title">Title</h5>
                            </div>
                        </div>
                    </Link>
                    <Link to="chinesevideo">
                        <div className="card text-center">
                            <Badge text="EP 02"
                                   style={{
                                       marginLeft: 12,
                                       padding: '0 3px',
                                       backgroundColor: '#e91e63',
                                       borderRadius: 2
                                   }}/>
                            <img className="card-img-top" src={ch03} alt="ch03"/>
                            <div className="card-body" id="card">
                                <h5 className="card-title">Title</h5>
                            </div>
                        </div>
                    </Link>
                    <Link to="chinesevideo">
                        <div className="card text-center">
                            <Badge text="EP 12"
                                   style={{
                                       marginLeft: 12,
                                       padding: '0 3px',
                                       backgroundColor: '#e91e63',
                                       borderRadius: 2
                                   }}/>
                            <img className="card-img-top" src={ch04} alt="ch04"/>
                            <div className="card-body" id="card">
                                <h5 className="card-title">Title</h5>
                            </div>
                        </div>
                    </Link>
                    <div className="clearfix"></div>
                </div>
            </div>
        );
    }
}

export default ChinaTV;
