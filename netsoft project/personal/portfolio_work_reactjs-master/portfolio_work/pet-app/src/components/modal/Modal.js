import React, {Component} from 'react';
import {Link} from 'react-router';
import { Rate } from 'antd';
import {Tabs, Icon} from 'antd';
import cat from '../../images/cat.jpg'
import dog from '../../images/dog.png'
import pet from '../../images/petfood.png'
import pa2 from '../../images/pa2.jpg'

const TabPane = Tabs.TabPane;


class Modal extends Component {
    render() {
        return (
            <div id="Modal">
                <div id="myModal" className="modal">
                    <div className="modal-content" id="a1">
                        <div className="modal-header">
                            <span className="close">&times;</span>
                            <h2>American Bobtail</h2>
                        </div>
                        <div className="modal-body">
                            <div className="img_modal">
                                <img src={cat} alt=""/>
                            </div>
                            <div className="tab_cat">
                                <Tabs defaultActiveKey="1">
                                    <TabPane tab={<span><Icon type="bulb"/>Information</span>} key="1">
                                        <div className="tab_1">
                                            <i className="fas fa-volume-down"> </i>
                                            <h3 className="h31">Name</h3>
                                            <h3 className="h32">Meow</h3>
                                            <div className="clearfix"></div>
                                        </div>
                                        <div className="tab_1">
                                            <i className="fas fa-transgender"> </i>
                                            <h3 className="h31">Gender</h3>
                                            <h3 className="h32">Male</h3>
                                            <div className="clearfix"></div>
                                        </div>
                                        <div className="tab_1">
                                            <i className="fas fa-paw"> </i>
                                            <h3 className="h31">Age</h3>
                                            <h3 className="h32">2</h3>
                                            <div className="clearfix"></div>
                                        </div>
                                        <div className="clearfix"></div>

                                    </TabPane>
                                    <TabPane tab={<span><Icon type="hourglass"/>Story </span>} key="2">
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                            Lorem Ipsum has been the industry's standard dummy text ever since the
                                            1500s, when an unknown printer took a galley of type and scrambled it to
                                            make a type specimen book.</p>
                                    </TabPane>
                                </Tabs>
                            </div>
                        </div>
                        <Link to="home">
                        <div className="modal-footer">
                            <h3>Adopt <i className="fas fa-paw"> </i></h3>
                        </div>
                        </Link>
                    </div>


                    <div className="modal-content" id="a2">
                        <div className="modal-header">
                            <span className="close">&times;</span>
                            <h2>Dachshund Dog</h2>
                        </div>
                        <div className="modal-body">
                            <div className="img_modal">
                                <img src={dog} alt=""/>
                            </div>
                            <div className="tab_cat">
                                <Tabs defaultActiveKey="1">
                                    <TabPane tab={<span><Icon type="bulb"/>Information</span>} key="1">
                                        <div className="tab_1">
                                            <i className="fas fa-volume-down"> </i>
                                            <h3 className="h31">Name</h3>
                                            <h3 className="h32">Mimi</h3>
                                            <div className="clearfix"></div>
                                        </div>
                                        <div className="tab_1">
                                            <i className="fas fa-transgender"> </i>
                                            <h3 className="h31">Gender</h3>
                                            <h3 className="h32">Female</h3>
                                            <div className="clearfix"></div>
                                        </div>
                                        <div className="tab_1">
                                            <i className="fas fa-paw"> </i>
                                            <h3 className="h31">Age</h3>
                                            <h3 className="h32">7</h3>
                                            <div className="clearfix"></div>
                                        </div>
                                        <div className="clearfix"></div>

                                    </TabPane>
                                    <TabPane tab={<span><Icon type="hourglass"/>Story </span>} key="2">
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                            Lorem Ipsum has been the industry's standard dummy text ever since the
                                            1500s, when an unknown printer took a galley of type and scrambled it to
                                            make a type specimen book.</p>
                                    </TabPane>
                                </Tabs>
                            </div>
                        </div>
                        <Link to="home">
                        <div className="modal-footer">
                            <h3>Adopt <i className="fas fa-paw"> </i></h3>
                        </div>
                        </Link>
                    </div>



                    <div className="modal-content" id="a3">
                        <div className="modal-header">
                            <span className="close">&times;</span>
                            <h2>Black Gold</h2>
                        </div>
                        <div className="modal-body">
                            <div className="img_modal">
                                <img src={pet} alt=""/>
                            </div>
                            <div className="tab_cat">
                                <Tabs defaultActiveKey="1">
                                    <TabPane tab={<span><Icon type="bulb"/>Information</span>} key="1">
                                        <div className="tab_1">
                                            <i className="fas fa-heartbeat"></i>
                                            <h3 className="h31">Healthy</h3>
                                            <h3 className="h32 rate_sty" >
                                                <Rate allowHalf defaultValue={4} />
                                            </h3>
                                            <div className="clearfix"></div>
                                        </div>

                                        <div className="tab_1">
                                            <i className="fas fa-paw"> </i>
                                            <h3 className="h31">Age</h3>
                                            <h3 className="h32">3 - 15</h3>
                                            <div className="clearfix"></div>
                                        </div>
                                        <div className="clearfix"></div>

                                    </TabPane>
                                    <TabPane tab={<span><Icon type="hourglass"/>Story </span>} key="2">
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                            Lorem Ipsum has been the industry's standard dummy text ever since the
                                            1500s, when an unknown printer took a galley of type and scrambled it to
                                            make a type specimen book.</p>
                                    </TabPane>
                                </Tabs>
                            </div>
                        </div>
                        <Link to="home">
                            <div className="modal-footer">
                                <h3>Shop <i className="fas fa-paw"> </i></h3>
                            </div>
                        </Link>
                    </div>



                    <div className="modal-content" id="a4">
                        <div className="modal-header">
                            <span className="close">&times;</span>
                            <h2>Dog Bed</h2>
                        </div>
                        <div className="modal-body">
                            <div className="img_modal">
                                <img src={pa2} alt=""/>
                            </div>
                            <div className="tab_cat">
                                <Tabs defaultActiveKey="1">
                                    <TabPane tab={<span><Icon type="bulb"/>Information</span>} key="1">
                                        <div className="tab_1">
                                            <i className="fas fa-crosshairs"></i>
                                            <h3 className="h31">Size</h3>
                                            <h3 className="h32 " >
                                                S, M, L, XL
                                            </h3>
                                            <div className="clearfix"></div>
                                        </div>

                                        <div className="tab_1">
                                            <i className="fas fa-paw"> </i>
                                            <h3 className="h31">Age</h3>
                                            <h3 className="h32">3 - 15</h3>
                                            <div className="clearfix"></div>
                                        </div>
                                        <div className="clearfix"></div>

                                    </TabPane>
                                    <TabPane tab={<span><Icon type="hourglass"/>Story </span>} key="2">
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                            Lorem Ipsum has been the industry's standard dummy text ever since the
                                            1500s, when an unknown printer took a galley of type and scrambled it to
                                            make a type specimen book.</p>
                                    </TabPane>
                                </Tabs>
                            </div>
                        </div>
                        <Link to="home">
                            <div className="modal-footer">
                                <h3>Shop <i className="fas fa-paw"> </i></h3>
                            </div>
                        </Link>
                    </div>


                    
                </div>
            </div>
        );
    }
}

export default Modal;
