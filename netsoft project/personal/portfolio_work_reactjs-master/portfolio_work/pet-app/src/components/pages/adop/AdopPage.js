import React, {Component} from 'react';
import MenuTop from '../../menu/MenuTop'
import BottomMenu from '../../menu/BottomMenu'
import Video from './content/Video'
import Cat from './content/Cat'
import Dog from './content/Dog'
import Modal from '../../modal/Modal'


class AdopPage extends Component {

    render() {
        return (
            <main id="AdopPage">
                <MenuTop title="ADOPTION"/>
                <Video/>
                 <Cat/>
                <Dog/>
                <Modal/>
                <div className="clearfix"></div>
                <BottomMenu/>
            </main>
        );
    }
}

export default AdopPage;
