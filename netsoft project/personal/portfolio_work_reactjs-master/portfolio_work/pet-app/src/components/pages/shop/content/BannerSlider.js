import React, {Component} from 'react';
import s1 from '../../../../images/s5.jpg'
import s2 from '../../../../images/s2.jpg'
import s3 from '../../../../images/s3.jpg'

class BannerSlider extends Component {
    render() {
        return (
            <section id="BannerSlider">
                <div id="myCarousel" className="carousel slide" data-ride="carousel">
                    <ol className="carousel-indicators">
                        <li data-target="#myCarousel" data-slide-to="0" className="active"></li>
                        <li data-target="#myCarousel" data-slide-to="1"></li>
                        <li data-target="#myCarousel" data-slide-to="2"></li>
                    </ol>

                    <div className="carousel-inner">
                        <div className="item active">
                            <img src={s1} alt=""/>
                        </div>

                        <div className="item">
                            <img src={s2} alt=""/>
                        </div>

                        <div className="item">
                            <img src={s3} alt=""/>
                        </div>
                    </div>

                    <a className="left carousel-control" href="#myCarousel" data-slide="prev">
                        <span className="glyphicon glyphicon-chevron-left ar_width"></span>
                        <span className="sr-only">Previous</span>
                    </a>
                    <a className="right carousel-control" href="#myCarousel" data-slide="next">
                        <span className="glyphicon glyphicon-chevron-right ar_width"></span>
                        <span className="sr-only">Next</span>
                    </a>
                </div>
            </section>
        );
    }
}

export default BannerSlider;
