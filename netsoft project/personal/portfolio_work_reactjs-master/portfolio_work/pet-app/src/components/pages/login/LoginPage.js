import React, {Component} from 'react';
import {Modal} from 'antd';
import {browserHistory, Link} from 'react-router';
import logo from '../../../images/logo.png'


class LoginPage extends Component {

    state = {
        visible: false,
        confirmLoading: false,
    }

    showModal = () => {
        this.setState({
            visible: true,
        });
    }

    handleOk = () => {
        this.setState({
            confirmLoading: true,

        });
        setTimeout(() => {
            this.setState({
                visible: false,
                confirmLoading: false,
            });
        }, 2000);
        
        return  (
            browserHistory.push('home')
        )
    }

    handleCancel = () => {
        console.log('Clicked cancel button');
        this.setState({
            visible: false,
        });
    }


    render() {
        const {visible, confirmLoading} = this.state;
        return (
            <main id="LoginPage">
                <div className="login_logo">
                    <img src={logo} alt="logo"/>
                </div>

                <div className="login_form">
                    <div className="form-container">
                        <form>
                            <div className="form_sty">
                                <label>
                                    <i className="fas fa-user"></i>
                                    Username
                                </label>
                                <input type="text" placeholder="Enter Username"/>
                            </div>
                            <div className="form_sty">
                                <label>
                                    <i className="fas fa-lock-alt"></i>
                                   Password
                                </label>
                                <input type="password" placeholder="Enter Password"/>
                            </div>

                            <div className="login_btn">
                                <Link to="home">
                                    <button className="button">
                                        <span>Login</span>
                                    </button>
                                </Link>
                            </div>
                            <div className="signup_btn">
                                <button type="button" className="button" onClick={this.showModal}>
                                    <span>Sign up</span>
                                </button>
                                <Modal title="Sign up"
                                       visible={visible}
                                       onOk={this.handleOk}
                                       confirmLoading={confirmLoading}
                                       onCancel={this.handleCancel}
                                >
                                   <div className="signup_form">
                                       <div className="form_sty">
                                           <label>
                                               <i className="fas fa-user"></i>
                                               Enter Username
                                           </label>
                                           <input type="text" placeholder="Enter Username"/>
                                       </div>
                                       <div className="form_sty">
                                           <label>
                                               <i className="fas fa-envelope"></i>
                                               Enter Email
                                           </label>
                                           <input type="text" placeholder="Enter Email"/>
                                       </div>
                                       <div className="form_sty">
                                           <label>
                                               <i className="fas fa-mobile-alt"></i>
                                               Enter Phone Number
                                           </label>
                                           <input type="password" placeholder="Enter Phone Number"/>
                                       </div>
                                       <div className="form_sty">
                                           <label>
                                               <i className="fas fa-lock-alt"></i>
                                               Enter password
                                           </label>
                                           <input type="password" placeholder="Enter Password"/>
                                       </div>
                                   </div>
                                </Modal>
                            </div>

                            <div className="clearfix"></div>

                        </form>
                    </div>


                </div>

            </main>
        );
    }
}

export default LoginPage;
