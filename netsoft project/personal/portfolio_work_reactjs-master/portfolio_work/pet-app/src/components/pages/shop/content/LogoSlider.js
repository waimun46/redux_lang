import React, {Component} from 'react';
import {Card} from 'antd';
import OwlCarousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';

import f1 from '../../../../images/f1.png'
import f2 from '../../../../images/f2.png'
import f3 from '../../../../images/f3.png'
import f4 from '../../../../images/f4.png'
import f5 from '../../../../images/f5.png'
import f6 from '../../../../images/f6.png'

class LogoSlider extends Component {
    render() {
        return (
            <section id="LogoSlider">
                <Card title="- Brand -">
                    <OwlCarousel
                        className="owl-theme"
                        loop={true}
                        autoplay={true}
                        margin={7}
                        nav={true}
                        dots={false}
                    >
                        <div className="item">
                            <img src={f1} alt=""/>
                        </div>
                        <div className="item">
                            <img src={f2} alt=""/>
                        </div>
                        <div className="item">
                            <img src={f3} alt=""/>
                        </div>
                        <div className="item">
                            <img src={f4} alt=""/>
                        </div>
                        <div className="item">
                            <img src={f5} alt=""/>
                        </div>
                        <div className="item">
                            <img src={f6} alt=""/>
                        </div>
                    </OwlCarousel>
                </Card>
            </section>
        );
    }
}

export default LogoSlider;
