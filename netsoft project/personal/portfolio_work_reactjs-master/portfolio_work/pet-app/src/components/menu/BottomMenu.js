import React, {Component} from 'react';
import { browserHistory } from 'react-router';
import { Modal } from 'antd';

class BottomMenu extends Component {

    state = {
        visible: false,
        confirmLoading: false,
    }
    showModal = () => {
        this.setState({
            visible: true,
        });
    }

    handleOk = () => {
        this.setState({
            ModalText: 'The modal will be closed after two seconds',
            confirmLoading: true,
        });
        setTimeout(() => {
            this.setState({
                visible: false,
                confirmLoading: false,
            });
        }, 2000);
    }

    handleCancel = () => {
        console.log('Clicked cancel button');
        this.setState({
            visible: false,
        });
    }

    home(){
        browserHistory.push('home');
    }

    render() {
        const { visible, confirmLoading } = this.state;
        return (
            <div id="BottomMenu">
                <div className="wrapper">
                    <input id="triggerButton" className="triggerButton" type="checkbox" />
                    <label htmlFor="triggerButton"></label>
                    <div className="two fas fa-home" onClick={this.home}></div>
                    <div className="one fas fa-phone" onClick={this.showModal}></div>

                </div>
                <Modal title="Contact"
                       visible={visible}
                       onOk={this.handleOk}
                       confirmLoading={confirmLoading}
                       onCancel={this.handleCancel}
                >
                    <div className="map_sty">
                        <iframe title="map" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d254970.9059066768!2d101.54410037422775!3d3.115759005463715!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31cc4b5bcab74cdb%3A0x559cd1de7a0b24fe!2sPetsmore+Purchong+Jalan+Kenari!5e0!3m2!1sen!2smy!4v1531678677535" allowFullScreen></iframe>

                    </div>
                    <div className="add-sty">
                        <div className="ad1">
                            <p><i className="fas fa-map-marker-alt"></i>Bandar Puchong Jaya, 47100 Puchong, Selangor.</p>
                        </div>
                        <div className="ad2">
                            <p><i className="fas fa-phone-square"></i>03-8070 6309</p>
                        </div>
                    </div>
                </Modal>
            </div>
        );
    }
}

export default BottomMenu;
