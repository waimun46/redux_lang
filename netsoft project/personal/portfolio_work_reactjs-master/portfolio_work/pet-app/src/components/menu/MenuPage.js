import React, {Component} from 'react';
import {Link} from 'react-router';
import {Card} from 'antd';

import p1 from '../../images/p1.jpg'
import logo from '../../images/logo.png'


class MenuPage extends Component {
    render() {
        return (
            <div id="MenuPage">
                <div className="logo">
                    <img src={logo} alt=""/>
                </div>
                <div className="top_banner">
                    <img src={p1} alt=""/>
                </div>

                <div className="top_menu">
                    <div className="care_pa">
                        <Link to="shop">
                            <Card.Grid>
                                <i className="fas fa-shopping-cart"></i>
                                <p>Pet Shop</p>
                            </Card.Grid>
                        </Link>
                    </div>
                    <div className="care_pa">
                        <Link to="adopt">
                            <Card.Grid>
                                <i className="fas fa-heart"></i>
                                <p>Adoption</p>
                            </Card.Grid>
                        </Link>
                    </div>
                    <div className="care_pa">
                        <Link to="video">
                            <Card.Grid>
                                <i className="fab fa-youtube"></i>
                                <p>Videos</p>
                            </Card.Grid>
                        </Link>
                    </div>
                    <div className="care_pa ">
                        <Link to="/">
                            <Card.Grid>
                                <i className="fas fa-sign-out-alt"></i>
                                <p>Logout</p>
                            </Card.Grid>
                        </Link>
                    </div>
                    <div className="clearfix"></div>
                </div>
            </div>
        );
    }
}

export default MenuPage;
