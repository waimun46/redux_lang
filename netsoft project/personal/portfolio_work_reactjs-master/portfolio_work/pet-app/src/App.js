import React, {Component} from 'react';
import Routes from './routes';

import './App.css';
import './css/responsive.css';
import './css/font-fa/css/fontawesome-all-pro.css';
import './css/bootstrap.min.css';


class App extends Component {
    render() {
        return (
            <div className="App">
                <Routes/>
            </div>
        );
    }
}

export default App;
