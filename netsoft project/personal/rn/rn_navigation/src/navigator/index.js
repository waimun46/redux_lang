import React, { Component } from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createBottomTabNavigator, createMaterialTopTabNavigator } from 'react-navigation-tabs';
import ANT from 'react-native-vector-icons/AntDesign';
import HomeScreen from '../pages/home';
import BlogScreen from '../pages/blog';
import NewsScreen from '../pages/news'
import ProfileScreen from '../pages/profile'
import SearchPage from '../pages/content/search'


// const HomeTab = createMaterialTopTabNavigator({
//   Tab1: HomeScreen,
//   Tab2: HomeScreen02,
//   Tab3: HomeScreen03,
//   Tab4: HomeScreen04,
//   Tab5: HomeScreen05,
//   Tab6: HomeScreen06,
//   Tab7: HomeScreen07,
// }, {
//   tabBarOptions: {
//     scrollEnabled: true,
//     labelStyle: {
//       fontSize: 12,
//     },
//     tabStyle: {
//       width: Dimensions.get('window').width / 4,
//     },
//     style: {
//       backgroundColor: 'tomato',
//     },
//     indicatorStyle: {
//       backgroundColor: '#fff'
//     }
//   },
// });

// HomeTab.navigationOptions = {
//   header: null
// };

const HomeBottomTab = createBottomTabNavigator({

  Home: {
    screen: HomeScreen,
    navigationOptions: {
      tabBarLabel: 'Home',
      tabBarIcon: ({ tintColor }) => (
        <ANT name="pluscircle" size={30} color={tintColor} />
      )
    },
  },

  Blog: {
    screen: BlogScreen,
    navigationOptions: {
      tabBarLabel: 'Blog',
      tabBarIcon: ({ tintColor }) => (
        <ANT name="pluscircle" size={30} color={tintColor} />
      )
    },
  },
  News: {
    screen: NewsScreen,
    navigationOptions: {
      tabBarLabel: 'News',
      tabBarIcon: ({ tintColor }) => (
        <ANT name="pluscircle" size={30} color={tintColor} />
      )
    },
  },
  Profile: {
    screen: ProfileScreen,
    navigationOptions: {
      tabBarLabel: 'Profile',
      tabBarIcon: ({ tintColor }) => (
        <ANT name="pluscircle" size={30} color={tintColor} />
      )
    },
  },
}, {
    initialRouteName: 'Home',
    tabBarOptions: {
      activeTintColor: '#fb9800',
      inactiveTintColor: '#7e7b7b',
      showIcon: true,
      style: { height: 54, backgroundColor: '#fff', borderTopWidth: 0.5, borderTopColor: '#fb9800' },
      showLabel: true,
      labelStyle: {
        fontSize: 10,

      }
    }
  })

// /////////////////////////////////// ProductTopTabsStack /////////////////////////////////// 
// // const ProductTopTabsStack = createMaterialTopTabNavigator({
// //   lasterProduct: {
// //     screen: LasterProducts,

// //   },
// //   promotionProduct: {
// //     screen: PromotionProducts,
// //   }
// // }, {
// //   tabBarOptions: {
// //     labelStyle: {
// //       fontSize: 12,
// //       color: '#000',
// //       fontWeight: 'bold'
// //     },
  
// //     style: {
// //       backgroundColor: '#fff',
// //     },
// //   }
// // });

// // ProductTopTabsStack.navigationOptions = ({ navigation }) => {
// //   const { routeName } = navigation.state.routes[navigation.state.index];
// //   const headerTitle = routeName;
// //   return {
// //     headerTitle,
// //   };
// // };


// /////////////////////////////////// ProductScreenStack /////////////////////////////////// 
// const ProductScreenStack = createStackNavigator({
//   product: {
//     screen: ProductsScreen,
//     navigationOptions: ({ navigation, screenProps }) => ({
//       headerLeft: <DrawerButton navigation={navigation} />,
//       headerRight: <UserButton navigation={navigation} />,
//       title: "Our Products"
//     })
//   },
//   login: {
//     screen: LoginScreen,
//   },
//   signup: {
//     screen: SignupScreen,
//   },

// }, {
//     headerLayoutPreset: 'center',
//     defaultNavigationOptions: {
//       headerStyle: { backgroundColor: '#000'},
//       headerTintColor: '#fff',
//       headerTitleStyle: { fontWeight: 'bold'},
//       headerBackTitle: null,
//     }
//   });


const AppNavigator = createStackNavigator({

  Home: {
    screen: HomeBottomTab,
    navigationOptions: {
      title: 'Home',
      headerRight: (<ANT name="pluscircle" size={30} color='#fff' />),
      headerBackTitle: null, /// remove back btn title
    },
  },
  Search: {
    screen: SearchPage,
    navigationOptions: {
      title: 'Search',
    },
  },
}, {
    headerLayoutPreset: 'center', /// or left
    defaultNavigationOptions: {
      headerStyle: { backgroundColor: '#f4511e', },
      headerTintColor: '#fff',
      headerTitleStyle: { fontWeight: 'bold' },

    }
  })







export default createAppContainer(AppNavigator);