import React, { Component } from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';


class SearchPage extends Component {
  // static navigationOptions = {
  //   title: 'Search',
  // };

  render() {
    return (
      <View style={styles.container}>
        <Text style={{color: '#000'}}>SearchPage</Text>
      </View>
    )
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default SearchPage;