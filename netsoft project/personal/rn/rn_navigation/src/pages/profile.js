import React, { Component } from 'react';
import { StyleSheet, Text, View,Button } from 'react-native';


class ProfileScreen extends Component {
  static navigationOptions = {
    title: 'Profile',
  };

  render() {
    return (
      <View style={styles.container}>
        <Text style={{color: '#000'}}>ProfileScreen</Text>
        <Button title="blog" onPress={() => this.props.navigation.navigate('Blog')} />
      </View>
    )
  }

}



const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default ProfileScreen;