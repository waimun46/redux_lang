import React, { Component } from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';


class BlogScreen extends Component {
  static navigationOptions = {
    title: 'Blog',
  };

  render() {
    return (
      <View style={styles.container}>
        <Text style={{color: '#000'}}>BlogScreen</Text>
        <Button title="blog" onPress={() => this.props.navigation.navigate('Home')} />
      </View>
    )
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default BlogScreen;