// SELECTING ALL TEXT ELEMENTS(name="")
var username = document.forms['validateForm']['username'];
var email = document.forms['validateForm']['email'];
var password = document.forms['validateForm']['password'];
var password_confirm = document.forms['validateForm']['password_confirm'];
var isValid = true;

// SELECTING ALL ID(id="")
var userId = document.getElementById('user_id');
var emailId = document.getElementById('email_id');
var passwordId = document.getElementById('password_id');
var password_confirmId = document.getElementById('password_confirm_id');

// SELECTING ALL ERROR DISPLAY ELEMENTS(id="")
var user_error = document.getElementById('user_error');
var email_error = document.getElementById('email_error');
var password_error = document.getElementById('password_error');
var password_confirm_error = document.getElementById('password_confirm_error');
var password_confirm_error_check = document.getElementById('password_confirm_error_check');

// SETTING ALL EVENT LISTENERS
username.addEventListener('blur', userVerify, true);
email.addEventListener('blur', emailVerify, true);
password.addEventListener('blur', passwordVerify, true);
password_confirm.addEventListener('blur', confirmVerify, true);



function ValidateSubmit() {
    // validation username
    if(username.value == null || username.value == ""){
        username.style.border = "1px solid red";
        userId.style.color = "red";
        user_error.style.visibility = "visible";
        user_error.textContent = "Please fill up your username.";
        isValid = false;
    }
    else {
        user_error.style.visibility = "hidden";
    }

    // validation email
    if(email.value == null || email.value == ""){
        email.style.border = "1px solid red";
        emailId.style.color = "red";
        email_error.style.visibility = "visible";
        email_error.textContent = "Please fill up your email.";
        isValid = false;
    }
    else {
        email_error.style.visibility = "hidden";
    }

    // validation password
    if(password.value == null || password.value == ""){
        password.style.border = "1px solid red";
        passwordId.style.color = "red";
        password_error.style.visibility = "visible";
        password_error.textContent = "Please fill up your password.";
        isValid = false;
    }
    else {
        password_error.style.visibility = "hidden";
    }


    // validation password_confirm
    if(password_confirm.value == null || password_confirm.value == ""){
        password_confirm.style.border = "1px solid red";
        password_confirmId.style.color = "red";
        password_confirm_error.style.visibility = 'visible';
        password_confirm_error.textContent = "Please fill up your password confirm.";
        isValid = false;
    }
    else {
        password_confirm_error.textContent = "";
    }

    // check if the two passwords match
    if(password.value != password_confirm.value){
        password_confirm.style.border = "1px solid red";
        password_confirmId.style.color = "red";
        password_confirm_error_check.style.visibility = "visible";
        password_confirm_error_check.textContent = "The two passwords do not match.";
        isValid = false;

        if(password_confirm.value == null || password_confirm.value == ""){
            password_confirm_error_check.textContent = "";
        }
    }
    else {
        password_confirm_error_check.textContent = "";
    }

    return isValid;
}


// event handler functions
function userVerify() {
    if(username.value != ""){
        username.style.border = "1px solid #76bb76";
        userId.style.color = "#76bb76"
        user_error.textContent = "";
        return true;
    }
}

function emailVerify() {
    if(email.value != ""){
        email.style.border = "1px solid #76bb76";
        emailId.style.color = "#76bb76"
        email_error.textContent = "";
        return true;
    }
}

function passwordVerify() {
    if(password.value != ""){
        password.style.border = "1px solid #76bb76";
        passwordId.style.color = "#76bb76";
        password_error.textContent = "";
        return true;
    }
}

function confirmVerify() {
    if(password_confirm.value != ""){
        password_confirm.style.border = "1px solid #76bb76";
        password_confirmId.style.color = "#76bb76";
        password_confirm_error.textContent = "";
        return true;
    }
}


