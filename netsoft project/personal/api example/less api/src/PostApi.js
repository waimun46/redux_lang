export function PostApi(type) {
    let Url = "https://jsonplaceholder.typicode.com/";

    return new Promise((resolve, reject) => {

        fetch(Url+type, {
            method: 'GET'
        })
            .then((res) => res.json())
            .then((resJson) => {
                //console.log(resJson);
                resolve(resJson);
            })
            .catch((error) => {
                console.log(error);
                reject(error)
            })

    })

}