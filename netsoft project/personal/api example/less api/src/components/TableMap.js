import React, {Component} from 'react';
import {PostApi} from '../PostApi'

class TableMap extends Component {

    constructor(props){
        super(props);
        this.state = {
            data: []
        }
    }

    componentDidMount(){

        PostApi('albums')
            .then((fetchData) => {
                //console.log(fetchData);
                this.setState({
                    data:fetchData
                })
            })

    }

    render() {
        return (
            <div className="TableMap">
                <table>
                    <thead>
                        <tr>
                            <th>userId</th>
                            <th>id</th>
                            <th>title</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            this.state.data.map((postData, key) =>
                                <tr>
                                    <td>{postData.userId}</td>
                                    <td>{postData.id}</td>
                                    <td>{postData.title}</td>
                                </tr>
                            )
                        }
                    </tbody>
                </table>
            </div>
        );
    }
}

export default TableMap;
