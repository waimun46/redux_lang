import React from 'react';
import ReactDOM from 'react-dom';
import {Route, Router, browserHistory,IndexRoute} from 'react-router';
import App from './App';
import HomeIndex from './components/HomeIndex';
import TableMap from './components/TableMap';
import PaginTable from './components/PaginTable';
import AntTable from './components/AntTable';


ReactDOM.render(
    <Router onUpdate={() => window.scrollTo(0, 0)} history={browserHistory}>
        <Route path="/" component={App}>
            <IndexRoute component={HomeIndex}/>
            <Route path="/tablemap" component={TableMap}/>
            <Route path="/pagintable" component={PaginTable}/>
            <Route path="/anttable" component={AntTable}/>
        </Route>
    </Router>
    ,
    document.getElementById('root'));
