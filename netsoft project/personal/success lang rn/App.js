import React, { Component } from 'react';
import { Platform, Button, StyleSheet, Text, View } from 'react-native';
// 1. 导入相关的文件
import { connect } from 'react-redux'
import { increaseCount } from './action_creators'

import RNLanguages from 'react-native-languages';
import i18n from './i18n';

import en from './translations/en.json'
import zh from './translations/zh.json'
import { AsyncStorage } from "react-native"

import RNRestart from "react-native-restart"

import Home from './home'

import {
  createSwitchNavigator,
  createAppContainer,
  createBottomTabNavigator
} from 'react-navigation';



class Profile extends Component {
  render() {
    return (
      <View>
        <Home />
      </View>
    );
  }
}

class Layout extends Component {
  constructor(props) {
    super(props);
  }

 
  change = async (language) => {
    console.info("== changing language to:" + language)
    await AsyncStorage.setItem("language", language)
    RNRestart.Restart()
  }
  render() {
    const {navigation} = this.props;

  
    return (
      <View>
        <Text> 
         {global.t('title')}
 

        </Text>

        <Button
          title="zh"
          onPress={() => this.change("en")}
        >
        </Button>
        <Button
          title="en"
          onPress={() => this.change('zh')}
        >
        </Button>

        <Button
          title="home"
          onPress={() => this.props.navigation.navigate('profile')}
        >
        </Button>


        <Text>
          count: {this.props.count}
        </Text>
      </View>
    );
  }
}



const AppSwitchNavigator = createSwitchNavigator({
  layout: { screen: Layout },
  profile: { screen: Profile },


});

const AppContainer = createAppContainer(AppSwitchNavigator);


class App extends Component {



  render() {

    return (
      <AppContainer />
    );
  }
}


export default connect()(App)
