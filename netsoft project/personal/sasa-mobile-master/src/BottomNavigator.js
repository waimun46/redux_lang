import React from 'react';
import { Image } from 'react-native';
import { createBottomTabNavigator, createMaterialTopTabNavigator } from 'react-navigation';
import HomeScreen from './screens/TabBarScreen/HomeScreen';
import MyCartScreen from './screens/TabBarScreen/MyCartScreen';
import EvoucherInApp from './screens/TabBarScreen/EvoucherInApp';
import PromotionsList from './screens/TabBarScreen/PromotionsList';
import EvoucherInStore from './screens/TabBarScreen/EvoucherInStore';
import MapScreen from './screens/TabBarScreen/MapScreen'
import StoreLocationListView from './screens/TabBarScreen/StoreLocationListView';
import StoreLocationListView1 from './screens/TabBarScreen/StoreLocationListView1';
import Icon3 from 'react-native-vector-icons/MaterialCommunityIcons';
import { futuraPtMedium } from './styles/styleText'

const TabVoucher = createMaterialTopTabNavigator({
  EvoucherInApp: {
    screen: EvoucherInApp,
    navigationOptions: (navigation) => ({
      tabBarLabel: 'MOBILE APP',
      headerLeft: null,

    })
  },
  EvoucherInStore: {
    screen: EvoucherInStore,
    navigationOptions: (navigation) => ({
      tabBarLabel: 'IN STORE',
      labelStyle: {
        fontSize: 20,
      },
      headerLeft: null,
    })
  },
}, {
    tabBarOptions: {
      style: {
        backgroundColor: '#fff',
        height: 50,
        borderTopColor: 'transparent',
        borderTopWidth: 1,
        paddingRight: 10,
        paddingLeft: 10,
        borderTopWidth: 1,

      },
      labelStyle: { fontSize: 16, fontFamily: futuraPtMedium, },
      indicatorStyle: {
        backgroundColor: '#ff4da6', // color of the indicator
        height: 4,
      },
      activeTintColor: 'black',
      inactiveTintColor: 'gray',
      activeBackgroundColor: 'white',
      inactiveBackgroundColor: '#F6F6F6',
      tabBarPosition: "top",
    }
  })

const TabLocation = createMaterialTopTabNavigator({
  StoreLocationListView1: {
    screen: StoreLocationListView1,
    navigationOptions: (navigation) => ({
      tabBarLabel: 'LIST VIEW',
      headerLeft: null,

    })
  },
  MapScreen: {
    screen: MapScreen,
    navigationOptions: (navigation) => ({

      tabBarLabel: 'MAP VIEW',
      headerLeft: null,

    })
  },
}, {
    tabBarOptions: {
      style: {
        backgroundColor: '#fff',
        height: 50,
        borderTopColor: 'transparent',
        borderTopWidth: 1,
        paddingRight: 10,
        paddingLeft: 10,
        borderTopWidth: 1,
      },
      labelStyle: { fontSize: 16, fontFamily: futuraPtMedium },
      indicatorStyle: {
        backgroundColor: '#ff4da6', // color of the indicator
        height: 4,
      },
      activeTintColor: 'black',
      inactiveTintColor: 'gray',
      activeBackgroundColor: 'white',
      inactiveBackgroundColor: '#F6F6F6',
      tabBarPosition: "top",
    }
  })

export default BottomNavigator = createBottomTabNavigator({
  HomeScreen: {
    screen: HomeScreen,
    navigationOptions: (navigation) => ({
      title: 'Home',
      headerLeft: null,
      tabBarIcon: ({ tintColor }) => (
        <Icon3 name="home-outline" color='#757575' size={25}
        />
      )
    })
  },
  Evoucher: {
    screen: TabVoucher,
    navigationOptions: () => ({
      title: 'E-Voucher',
      tabBarIcon: ({ tintColor }) => (
        <Icon3 name="wallet-giftcard" color='#757575' size={25}
        />

      )
    }),
    StoreLocationListView: {
      screen: StoreLocationListView,
    },
  },
  PromotionsList: {
    screen: PromotionsList,
    navigationOptions: (navigation) => ({
      tabBarIcon: ({ tintColor }) => (
          <Icon3 name="tag-outline" color='#757575' size={25}
          />

      )
    })
  },
  MyCartScreen: {
    screen: MyCartScreen,
    EvoucherInStore: {
      screen: EvoucherInStore
    },
    navigationOptions: (navigation) => ({
      tabBarIcon: ({ tintColor }) => (
        <Icon3 name="cart-outline" color='#757575' size={25}
        />

      )
    })
  },
}, {
    tabBarOptions: {
      activeTintColor: '#ff4da6',
      inactiveTintColor: 'gray',
      activeBackgroundColor: 'white',
      inactiveBackgroundColor: '#F6F6F6',
      tabBarPosition: "top",
    }
  })
