import { StyleSheet, Dimensions } from 'react-native';
import { futuraPtMedium, futuraPtBook } from './styleText'

const dimWidth = Dimensions.get('window').width
const dimHeight = Dimensions.get('window').height
const buttonColor = '#ff4da6'
const textColor = '#fff'
const backgroundColor = '#fff'

const styles = StyleSheet.create({
    container: {
        height: dimHeight
    },
    textSort: {
        fontSize: 16, marginTop: 20, fontWeight: '300', fontFamily: futuraPtBook
    },
    textSortSelected: {
        fontSize: 16, marginTop: 20, fontFamily: futuraPtMedium, color: '#E4007C',
    },
    containerBackground: {
        backgroundColor: backgroundColor
    },
    textHeader: {
        color: '#111111',
        marginTop: 16,
        marginLeft: 10,
        fontSize: 16,
        fontFamily: futuraPtMedium
    },
    viewText: {
        marginTop: 6,
        marginBottom: 10,
        marginLeft: 10,
        flex: 1,
        flexDirection: 'column',

    },
    textHeading: {
        color: '#000',
        fontSize: 16,
        justifyContent: "center",
        alignItems: 'center',
        fontFamily: futuraPtMedium,
        marginTop: 5

    },


    textNormal: {
        color: '#C0C0C0',
        fontSize: 14,
        marginTop: 2,
        marginBottom: 2,
        justifyContent: "center",
        alignItems: 'center'
    },
    bottomLine: {
        backgroundColor: '#D3D3D3',
        height: 0.50,
        marginRight: 4,
        marginLeft: 4,
        marginTop: 10,
        marginBottom: 10
    },
    styleViewOne: {
        flex: 1, flexDirection: 'row'
    },
    styleTextRight: {
        textAlign: 'right', color: '#ff4da6', marginRight: 4
    },
    bottomLine: {
        backgroundColor: '#D3D3D3',
        height: 0.50,
        marginRight: 4,
        marginLeft: 4,
        marginTop: 10,
        marginBottom: 10
    },
    btnContainer: {
        marginTop: 10,
        marginBottom: 10,
        marginLeft: 20,
        marginRight: 20,
        height: 50
    },
    btnTouch: {
        backgroundColor: '#E4007C',
        justifyContent: "center",
        alignItems: 'center',
        height: 50
    },
    btnText: {
        color: 'white',
        fontSize: 20,
        fontFamily: futuraPtMedium
    },
    optionView: {
        flex: 1, flexDirection: 'row', marginTop: 1, marginBottom: 1,
    },
    viewTextTop: {

        marginTop: 1,
        marginBottom: 1,
        marginLeft: 10,

    },
    bottomLine: {
        backgroundColor: '#D3D3D3',
        height: 0.50,
        marginRight: 20,
        marginLeft: 20


    },
    Container1: {
        flex: 1,
        flexDirection: 'row',
        marginRight: 8

    },
    checkboxContainer1: {
        marginTop: 16,
        marginLeft: 10,
        marginRight: 10
    },
    checkboxStyle1: {
        backgroundColor: '#f2f2f2',
        color: '#900',
        borderRadius: 5,
        marginTop: 16,
        marginLeft: 10,
        marginRight: 10
    },
    terms1: {
        marginTop: 16,
        marginLeft: 10,
        marginRight: 30
    },
    Container2: {
        flex: 1,
        flexDirection: 'row',
        marginRight: 8
    },
    checkboxContainer2: {
        marginTop: 16,
        marginLeft: 10,
        marginRight: 10
    },
    checkboxStyle2: {
        backgroundColor: '#f2f2f2',
        color: '#900',
        borderRadius: 5,
        marginTop: 16,
        marginLeft: 10,
        marginRight: 10
    },
    terms2: {
        marginTop: 16,
        marginLeft: 10,
        marginRight: 30
    },



});
export default styles;
