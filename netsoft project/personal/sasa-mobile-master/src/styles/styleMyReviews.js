import {StyleSheet, Dimensions} from 'react-native';

const dimWidth= Dimensions.get('window').width
const dimHeight= Dimensions.get('window').height
const buttonColor='#ff4da6'
const textColor='#fff'
const backgroundColor='#fff'

const styles=StyleSheet.create({
    container:{
        height:dimHeight
    },
  containerBackground:{
        backgroundColor:backgroundColor
    },
    viewText:{
        marginTop: 4,
        marginBottom: 4,
        marginLeft:10
    },
    textHeading:{
        
        
       
         justifyContent: 'flex-end',
         alignItems: 'flex-end',
         color:'#000',
         fontSize:16,
    },
    textNormal:{
        
         
        fontSize:14,
        marginTop: 2,
       marginTop:10
    },
   
});
export default styles;