import {StyleSheet, Dimensions} from 'react-native';
import { futuraPtBook, futuraPtLight } from './styleText'

const dimWidth= Dimensions.get('window').width
const dimHeight= Dimensions.get('window').height

const buttonColor='#E4007C'
const textColor='#fff'
const backgroundColor='#fff'

const styles=StyleSheet.create({
    container:{
        height:dimHeight
    },

    fieldtext:{
     color:'#000',
     marginTop: 16,
     marginLeft: 10,
     marginRight: 10,
     fontSize: 16,
     justifyContent:"center",
     alignItems:'center',
     fontFamily:futuraPtLight
    },
    errorFieldtext:{
        color:'red',
        marginTop: 16,
        marginLeft: 10,
        marginRight: 10,
        fontSize: 18,
        justifyContent:"center",
        alignItems:'center',
        fontFamily:futuraPtBook
    },
    email:{
        color:'#737373',
        marginLeft: 10,
        marginRight: 10,
        marginTop: 4,
        justifyContent:"center",
        alignItems:'center',
        fontSize: 18,
        fontFamily:futuraPtLight
    },

    lineDown:{
        borderBottomColor:'#C0C0C0',
        borderBottomWidth:1,
        backgroundColor:'#000',
        height:0.5,
        marginLeft: 10,
        marginRight: 10,
        marginTop: 4
    },

    textError:{
        color:'red',
        fontSize:16,
        marginLeft:15,
        marginTop:10
    },

    viewChkMainOne:{
        flex: 1,
        flexDirection: 'row'
    },
    viewChkOne:{
        marginTop: 16,
        marginLeft: 10,
        marginRight: 10
    },
    chkOne:{
        backgroundColor: '#f2f2f2',
        color:'#900',
        borderRadius: 5
    },
    viewChkTxtOne:{
        marginTop: 16,
        marginLeft: 10,
        marginRight: 10
    },
    chkTextOne:{
        marginRight: 10
    },



    viewButton:{marginTop:20,marginLeft:20,marginRight:20,height:50,backgroundColor:buttonColor},
    textButton:{color:textColor,fontSize:18,},

  touch:{
      justifyContent:"center",alignItems:'center',height:50
  }



});
export default styles;
