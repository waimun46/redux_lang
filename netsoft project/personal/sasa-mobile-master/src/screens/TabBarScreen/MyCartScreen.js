
import React, { Component } from 'react';
import { TextInput, AsyncStorage, Text, View, Image, ScrollView, Picker, TouchableOpacity, FlatList, Dimensions, StatusBar, Alert } from 'react-native';
import { Container, Header, Content, Button, ListItem, List, Card, Left, Right, Title, Body, } from 'native-base';
import styles from '../../styles/styleMyCart';
import Icon1 from 'react-native-vector-icons/dist/Foundation';
import { futuraPtMedium, futuraPtBook } from '../../styles/styleText'

const dimWidth = Dimensions.get('window').width
const dimHeight = Dimensions.get('window').height

const backgroundColor = '#fff'
const buttonColor = '#ff4da6'
const textColor = '#fff'

export default class MyCartScreen extends Component {
  static navigationOptions = ({
    title: 'My Cart',
    headerLeft: null,
    headerStyle: {
      backgroundColor: 'black'
    },
    headerTintColor: '#fff',
    headerTitleStyle: {
      fontSize: 22,
      fontFamily: futuraPtMedium,
      fontWeight: '500'
    },
  })

  constructor(props) {
    super(props);
    this.state = {
      cartList: [],
      itemTotal: 0,
      discountTotal: 0,
      orderTotal: 0,

    }
  }

  _keyExtractor = (item, index) => item.id;

  componentDidMount = async () => {

    this.subs = [
      this.props.navigation.addListener('didFocus', () => this.componentDidFocus()),
    ];
  }

  componentDidFocus = async () => {
    this.subTotal()

  }

  componentWillUnmount() {
    this.subs.forEach(sub => sub.remove());
  }

  onCheckout = () => {

    if (this.state.cartList.length < 1) {
      return;
    }

    if (this.state.itemTotal < 25) {
      Alert.alert("Minimum Purchase of RM25 is required to checkout.")
      return;
    }

    for (var i = 0; i < this.state.cartList.length; i++) {
      if (this.state.cartList[i].amount === undefined || this.state.cartList[i].amount < 1) {
        Alert.alert("Please fill amount")
        return;
      }
      else if (this.state.cartList[i].amount > this.state.cartList[i].quantity) {
        Alert.alert(this.state.cartList[i].product_name + " Insufficient Stock")
        return;
      }
      else if (this.state.cartList[i].amount > 6) {
        Alert.alert(this.state.cartList[i].product_name + " Maximum Quantity Allowed Should be 6 Only")
        return;
      }
    }

    this.props.navigation.navigate('CheckoutScreen')
  }

  changeAmount = async (value, index) => {
    const newArray = [...this.state.cartList];
    newArray[index].amount = value;
    var subTotalProduct = value * (parseFloat(newArray[index].offer_price) == 0 ? newArray[index].price : newArray[index].offer_price);
    newArray[index].subTotal = parseFloat(subTotalProduct).toFixed(2)

    this.setState({ cartList: newArray });


    await AsyncStorage.setItem('cart', JSON.stringify(newArray))

    this.subTotal()
  }

  async subTotal() {
    var cartStorage = await AsyncStorage.getItem('cart')
    let cartList = JSON.parse(cartStorage);

    var total = 0
    var discountTotal = 0
    for (var i = 0; i < cartList.length; i++) {
      if (cartList[i].offer_price != 0) {
        var subTotal = total + parseFloat(cartList[i].offer_price) * (cartList[i].amount ? cartList[i].amount : 1);
      } else {
        var subTotal = total + parseFloat(cartList[i].price) * (cartList[i].amount ? cartList[i].amount : 1);
      }
      total = subTotal;
    }

    this.setState({
      cartList: cartList,
      itemTotal: parseFloat(total).toFixed(2),
      orderTotal: total - discountTotal,
    })

  }

  async deleteItem(index) {
    var array = [...this.state.cartList]; // make a separate copy of the array
    if (index !== -1) {
      array.splice(index, 1);
      this.setState({ cartList: array });
    }

    await AsyncStorage.setItem('cart', JSON.stringify(array))

    this.subTotal()
  }

  renderRow = ({ item, index }) => {

    return (
      <View style={styles.viewText}>
        <View style={styles.styleViewOne} >

          <TouchableOpacity onPress={() => this.props.navigation.navigate('ProductDetailsScreen', {
            item: item,
          })}>
            <View><Card style={{ padding: 4 }}><Image style={{ width: 70, height: 100 }} source={{ uri: item.image_url }} /></Card></View>
          </TouchableOpacity>

          <TouchableOpacity  style={{ flex: 1, flexDirection: 'column' }} onPress={() => this.props.navigation.navigate('ProductDetailsScreen', {
            item: item,
          })}>
            <View style={{ flex: 1, flexDirection: 'column' }}>
              <Text style={{ marginLeft: 4, marginTop: 8, fontSize: 14, fontFamily: futuraPtBook, fontWeight: '300', color: "#111111" }}>{item.brand}</Text>
              <Text style={{ marginLeft: 4, marginTop: 4, fontSize: 18, fontFamily: futuraPtMedium, fontWeight: '300', color: '#111111' }}>{item.product_name}  </Text>
              <Text style={{ marginLeft: 4, marginTop: 4, fontSize: 16, textDecorationLine: 'line-through', fontFamily: futuraPtBook, fontWeight: '300' }}>{(parseFloat(item.offer_price)) == 0 ? "" : "RM" + item.price} </Text>
              <Text style={{ marginLeft: 4, marginTop: 4, fontSize: 16, color: '#ff4da6', fontFamily: futuraPtBook, fontWeight: '300' }}>{(parseFloat(item.offer_price)) == 0 ? "RM" + item.price : "RM" + item.offer_price} </Text>
            </View>
          </TouchableOpacity>
          <View style={{ marginRight: 10, marginTop: 5 }}>
            <TouchableOpacity onPress={() => this.deleteItem(index)}>
              <Icon1 name="trash" size={25} color="grey" style={{ marginLeft: 30 }} />
            </TouchableOpacity>
            <View style={{ marginTop: 10, height: 35, width: 60, alignItems: 'flex-end', justifyContent: 'center' }} >
              <View style={{ flexDirection: 'row', }}>
                <TextInput
                  style={{ height: 40, width: 40, borderColor: 'black', borderWidth: 0.5, padding: 10, }}
                  keyboardType='numeric'
                  onChangeText={(text) => this.changeAmount(text, index)}
                  value={this.state.cartList[index].amount}
                  defaultValue="1" />
              </View>
            </View>
            <Text style={{ fontSize: 14, color: 'black', marginTop: 5, fontFamily: futuraPtMedium, fontWeight: '300' }}>
              RM {(this.state.cartList[index].subTotal ? this.state.cartList[index].subTotal : ((parseFloat(item.offer_price)) == 0 ? item.price : item.offer_price))}
            </Text>
          </View>

        </View>
        <View style={styles.bottomLine}></View>
      </View>
    )
  }

  render() {
    return (

      (this.state.cartList.length > 0) ?
        <ScrollView style={{ backgroundColor: backgroundColor }}>
          <StatusBar
            backgroundColor="black"
            barStyle="light-content"
          />

          <FlatList
            vertical
            data={this.state.cartList}
            renderItem={item => this.renderRow(item)}>
          </FlatList>

          <List>
            <View style={{ marginTop: 20, marginLeft: 20 }}>

              <Text style={{
                color: '#000',
                fontSize: 20,
                fontFamily: futuraPtMedium,
                fontWeight: '300',
                color: '#111111'
              }}>ORDER SUMMARY</Text>


            </View>


            <View style={{ flex: 1, flexDirection: 'row', marginLeft: 20, marginRight: 20, marginTop: 15, justifyContent: 'space-between', marginBottom: 20 }} >
              <Text style={styles.textHeading}>Item Total:</Text>
              <Text style={styles.textHeading}>RM{this.state.itemTotal}</Text>
            </View>

          </List>


          <View style={{ backgroundColor: '#EEEEEE' }}>

            <View style={styles.btnContainer}>
              <TouchableOpacity style={styles.btnTouch} onPress={this.onCheckout} >
                <Text style={styles.btnText}>CHECKOUT</Text>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
        :
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', margin: 20 }}>
          <View style={{ backgroundColor: "#E4007C", opacity: 0.3, height: 100, width: 100, borderRadius: 50, marginBottom: 20 }}>
          </View>
          <Text style={[styles.textHeading, { fontSize: 27 }]}>Your cart is empty!</Text>
          <Text style={[styles.textHeading, { textAlign: 'center' }]}>Not quite ready to buy? Save it here until you're ready.</Text>
          <View style={styles.btnContainer}>
            <TouchableOpacity style={{
              width: '100%',
              height: 45,
              borderColor: '#E4007C',
              borderWidth: 1,
              justifyContent: 'center',
              alignContent: 'center',
              alignItems: 'center',
              paddingHorizontal: 20
            }}
              onPress={() => this.props.navigation.navigate('HomeScreen')}
            >
              <Text style={[styles.btnText, { color: "#E4007C" }]}>START SHOPPING</Text>
            </TouchableOpacity>
          </View>
        </View>


    );
  }
}
