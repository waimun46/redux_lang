import React, { Component } from 'react';
import { Alert, FlatList, Text, View, Image, ScrollView, TextInput, TouchableOpacity, AsyncStorage, Dimensions } from 'react-native';
import { Container, Header, Content, Body, ListItem, List, Card, Left, Right, Icon } from 'native-base';

import MyWebView from 'react-native-webview-autoheight';

import Progressbar from 'react-native-loading-spinner-overlay';
import Service from '../../config/Service';
import CanonicalPath from '../../config/CanonicalPath';
import { futuraPtMedium } from '../../styles/styleText'

const backgroundColor = '#fff'

export default class CheckoutPaymentScreen extends Component {

    static navigationOptions = ({
        title: 'Payment',
        headerLeft: null,
        headerStyle: {
            backgroundColor: 'black'
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontSize: 22,
            fontFamily: futuraPtMedium,
            fontWeight: '500'
        },
    })


    constructor(props) {
        super(props);
        this.state = {
            order: "",
            post_url: '',
            firstTime: true,
        }
    }

    componentDidMount = async () => {

        this.setState({
            order: this.props.navigation.getParam('order'),
            post_url: this.props.navigation.getParam('post_url'),
        })
    }

    setStateAsync(state) {
        return new Promise((resolve) => {
            this.setState(state, resolve)
        });
    }

    onSuccess = async (filter, json) => {
        this.setStateAsync({ isLoading: false });

        switch (filter) {
            case "GET_PAYMENT_STATUS":
                if (json[0].status == 0) {

                    this.props.navigation.navigate('CheckoutPaymentFailure', { data: this.state.order })
                } else {
                    this.props.navigation.navigate('CheckoutPaymentSuccess', { data: this.state.order })
                }

                break
        }

    }

    onError = (filter, e) => {
        this.setStateAsync({ isLoading: false });

        alert(e)
    }

    async get_payment_status() {
        await this.setStateAsync({ isLoading: true });

        var data = await {
            "order_id": this.state.order.order_id
        }

        var params = await {
            contentType: "multipart/form-data",
            Accept: "application/json",
            keepAlive: true,
            method: 'GET',
            useToken: true,
            data: data,
            canonicalPath: CanonicalPath.GET_PAYMENT_STATUS
        };

        await Service.request(this.onSuccess, this.onError, "GET_PAYMENT_STATUS", params);
    }

    render() {
        return (
            <ScrollView style={{ backgroundColor: backgroundColor }}>

                <MyWebView
                    source={{ uri: this.state.post_url }}
                    startInLoadingState={true}
                    onNavigationStateChange={(e) => {
                        console.warn("current state is ", JSON.stringify(1, null, 2));
                        if (e.url.indexOf("https://sasa.com.my/test/_adminCP/api/paymentstatus.php") > -1) {
                            this.get_payment_status()
                        }
                    }}
                />
            </ScrollView>
        );
    }
}
