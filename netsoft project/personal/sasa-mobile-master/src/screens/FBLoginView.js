import React, { Component } from 'react';
import { StyleSheet, Text, View,TouchableOpacity,Image } from 'react-native';
import PropTypes from 'prop-types';
import  Icon  from 'react-native-vector-icons/FontAwesome';
import { futuraPtMedium } from '../styles/styleText'

/**
  Example FBLoginView class
  Please note:
  - this is not meant to be a full example but highlights what you have access to
  - If you use a touchable component, you will need to set the onPress event like below
**/
class FBLoginView extends Component {
  static contextTypes = {
    isLoggedIn: PropTypes.bool,
    login: PropTypes.func,
    logout: PropTypes.func,
    props: PropTypes.shape({})
	};

  constructor(props) {
      super(props);
    }

    render(){
        return (
          <View style={{  marginTop:10,
            marginLeft:20,
            marginRight:20,}}>
           <TouchableOpacity  style={{
               backgroundColor:'#5B7AB9',
    height:50,
    marginTop:10,
    justifyContent:"center",
    alignItems:'center',
    flexDirection:'row'}} onPress={() => {
                if(!this.context.isLoggedIn){
                  this.context.login()
                }else{
                  this.context.logout()
                }

              }}>

               <Image   style={{ height:30,
    width:30}}source={require('../assets/fb.png')}/>
                         <Text style={{color:'white',
    fontSize:20,
    fontFamily:futuraPtMedium,

    marginLeft:10,
    marginRight:10}}> CONTINUE WITH FACEBOOK</Text>

           </TouchableOpacity>
          </View>
      )
    }
}
module.exports = FBLoginView;
