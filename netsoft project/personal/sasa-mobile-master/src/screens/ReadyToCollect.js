
import { createStackNavigator, StackActions, NavigationActions } from 'react-navigation'; // Version can be specified in package.json
import { Col, Row, Grid } from "react-native-easy-grid";
import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Image, ScrollView, TextInput, TouchableOpacity, ImageBackground, Dimensions, Alert } from 'react-native';

import { Container, Header, Content, Button, ListItem, List, Card, Left, Right, Icon } from 'native-base';
import { futuraPtMedium, futuraPtBook } from '../styles/styleText'

const width = Dimensions.get('window').width
const height = Dimensions.get('window').height
const itemsArray = ['Order No', 'Date', 'Time', 'Total', 'Paid By']
const itemsArray1 = ['#0123', '10 July 2018', '12:00 PM', 'RM 18.80', 'Debit Card']
export default class ReadyToCollect extends Component {
    static navigationOptions = ({
        title: 'Bill No:#0123',
        headerLeft: null,
        headerStyle: {
            backgroundColor: 'black',

        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontSize: 22,
            fontFamily:futuraPtMedium,
            fontWeight:'500'
        },
    })
    constructor(props) {
        super(props);
        this.state = {
            isVisible: false
        }

    }

    render() {
        return (
            <ScrollView style={{ flex: 1, backgroundColor: "#fff" }}>
                <View style={styles.container}>
                    <Text style={styles.text}>ORDER SUMMARY</Text>
                    <View style={styles.conText1}>
                        <Text style={styles.text1}>Order No:</Text>
                        <Text style={styles.detailText}>  #0123</Text>
                    </View>
                    <View style={styles.conText1}>
                        <Text style={styles.text1}>                  Date:</Text>
                        <Text style={styles.detailText}>  10 July 2018</Text>
                    </View>
                    <View style={styles.conText1}>
                        <Text style={styles.text1}>              Time:</Text>
                        <Text style={styles.detailText}>  12:00 PM</Text>
                    </View>

                    <View style={styles.conText1}>
                        <Text style={styles.text1}>               Total</Text>
                        <Text style={styles.detailText}>    RM 18.80</Text>
                    </View>
                    <View style={styles.conText1}>
                        <Text style={styles.text1}>           Paid By:</Text>
                        <Text style={styles.detailText}>  Debit Card</Text>
                    </View>
                    <View style={styles.containerRight1} >


                        <View style={styles.colleTouch}  >
                            <Text style={{ color: '#fff', fontWeight: '300',fontFamily:futuraPtMedium }}>READY TO COLLECT</Text>
                        </View>
                    </View>

                </View>

                <Text style={{ marginTop: 30, marginLeft: 20, fontSize: 16, color: "#111111", fontWeight: '300',fontFamily:futuraPtMedium }}>2 ITEMS</Text>
                <View style={styles.styleViewOne} >


                    <View>
                        <Card style={{ padding: 4 }}><TouchableOpacity><Image style={{ width: 70, height: 100, }} source={require('../assets/pic1.png')}>
                        </Image></TouchableOpacity></Card></View>
                    <View style={{ flex: 1, flexDirection: 'column', marginLeft: 10, marginRight: 10 }}>
                        <Text style={{ marginLeft: 4, marginTop: 8, fontSize: 16,fontFamily:futuraPtBook,fontWeight:'300' ,color:'#000'}}>Silkygirl</Text>
                        <Text style={{ marginLeft: 4, marginTop: 4, fontSize: 18 ,fontFamily:futuraPtMedium ,color:'#000',fontWeight:'300'}}>Cyber Colors - Serum Lipstics  </Text>
                        <Text style={{ marginLeft: 4, marginTop: 4, fontSize: 16,color:'#000', textDecorationLine: 'line-through',fontFamily:futuraPtBook,fontWeight:'300' }}>RM12.30 </Text>
                        <Text style={{ marginLeft: 4, marginTop: 4, fontSize: 16, color: '#ff4da6' ,fontFamily:futuraPtBook,fontWeight:'300', }}>RM5.90 </Text>
                    </View>



                </View>
                <View style={styles.styleViewOne} >


                    <View><Card style={{ padding: 4 }}><TouchableOpacity><Image style={{ width: 70, height: 100, }} source={require('../assets/pic2.png')}></Image></TouchableOpacity>
                    </Card></View>
                    <View style={{ flex: 1, flexDirection: 'column', marginLeft: 10, marginRight: 10 }}>
                        <Text style={{ marginLeft: 4, marginTop: 8, fontSize: 14,fontFamily:futuraPtBook,fontWeight:'300' ,color:'#000' }}>Silkygirl</Text>
                        <Text style={{ marginLeft: 4, marginTop: 4, fontSize: 18,fontFamily:futuraPtMedium,color:'#000' ,fontWeight:'300'}}>CYBER STAR EYESHADOW  </Text>
                        <Text style={{ marginLeft: 4, marginTop: 4, fontSize: 16, color:'#000',textDecorationLine: 'line-through',fontFamily:futuraPtBook,fontWeight:'300' }}>RM14.60 </Text>
                        <Text style={{ marginLeft: 4, marginTop: 4, fontSize: 16, color: '#ff4da6',fontFamily:futuraPtBook,fontWeight:'300' }}>RM9.90 </Text>
                    </View>


                </View>



            </ScrollView>
        )
    }
}
const styles = StyleSheet.create({

    container: {
        justifyContent: 'center',
        alignItems: 'center',



    },
    text: {
        fontSize: 18,
        fontWeight: '300',
        color: '#111111',
        fontFamily:futuraPtMedium,
        marginTop:20
    },
    conText1: {
        flexDirection: 'row',
        justifyContent: 'center',
        marginTop: 10
    },
    text1: {
        fontSize: 16,
        color: '#000',
        fontFamily:futuraPtBook,
        fontWeight:'300'
    },
    detailText: {
        fontSize: 16,
        fontFamily:futuraPtBook,
        fontWeight:'300',
        color:'lightgrey'
    },
    containerRight1: {
        marginTop: 20,
        width: 150,

        height: 10
    },
    colleTouch: {
        backgroundColor: '#E4007C',
        justifyContent: "center",
        alignItems: 'center',
        height: 35
    },
    writeReview: {
        marginTop: 50,
        marginLeft: 20,
        marginRight: 20,

        height: 50,

    },
    writeTouch: {
        backgroundColor: '#E4007C',
        justifyContent: "center",
        alignItems: 'center',
        height: 50,
        marginBottom: 100,
        flexDirection: 'row'
    },
    styleViewOne: {
        flex: 1, flexDirection: 'row',
        backgroundColor: '#fff',
        marginTop: 20,
        marginLeft: 20,
        marginRight: 20

    },


})
