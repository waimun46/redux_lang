import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { HomeStackScreen, PromotionStackScreen } from './stack';

//////////////////////////// createBottomTabNavigator ///////////////////////////
const BottomTabs = createBottomTabNavigator();

//////////////////////////// BottomTabsNavigation ///////////////////////////
export const BottomTabsNavigation = () => {
  return (
    <BottomTabs.Navigator>
      <BottomTabs.Screen name="Home" component={HomeStackScreen} />
      <BottomTabs.Screen name="Promotions" component={PromotionStackScreen} />
    </BottomTabs.Navigator>
  )
}

