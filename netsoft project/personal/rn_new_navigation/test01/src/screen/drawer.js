import React from 'react';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { AboutUsStackScreen } from './stack';
import { BottomTabsNavigation } from './bottomTab';

//////////////////////////// createDrawerNavigator ///////////////////////////
const Drawer = createDrawerNavigator();

//////////////////////////// DrawerNavigation ///////////////////////////
export const DrawerNavigation = () => {
  return (
    <Drawer.Navigator>
      <Drawer.Screen name="Home" component={BottomTabsNavigation} />
      <Drawer.Screen name="AboutUs" component={AboutUsStackScreen} />
    </Drawer.Navigator>
  )
}

